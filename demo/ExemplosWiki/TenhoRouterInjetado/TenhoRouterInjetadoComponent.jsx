import React from './TenhoRouterInjetado'
import PropTypes from 'prop-types'

export const TenhoRouterInjetadoComponent = props => {
  return <div>{props.history.location.pathname}</div>
}

TenhoRouterInjetadoComponent.propTypes = {
  // router
  history: PropTypes.object.isRequired,
}
