import { withRouter } from 'react-router'

import { TenhoRouterInjetadoComponent } from './TenhoRouterInjetadoComponent'

export const TenhoRouterInjetado = withRouter(
  TenhoRouterInjetadoComponent,
)
