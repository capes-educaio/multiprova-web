import React from 'react'
import PropTypes from 'prop-types'

export const AlgumComponent = props => (
  <div>{props.algumState}</div>
)

AlgumComponent.propTypes = {
  algumState: PropTypes.any,
}
