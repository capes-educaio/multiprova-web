import { compose } from 'redux'
import { connect } from 'react-redux'

import { algumMapStateToProps } from './algumMapStateToProps'

import { AlgumComponent } from './ComReduxStateComponent'

export const Algum = compose(connect(algumMapStateToProps))(
  AlgumComponent,
)
