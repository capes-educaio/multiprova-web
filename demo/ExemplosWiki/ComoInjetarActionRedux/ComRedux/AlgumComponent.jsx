import React, { Component } from 'react'
import PropTypes from 'prop-types'

export class AlgumComponent extends Component {
  UNSAFE_componentWillMount = () => this.props.selectAlgumState('Outro valor')

  render() {
    return <div>Olá, Mundo!</div>
  }
}

AlgumComponent.propTypes = {
  selectAlgumState: PropTypes.any,
}
