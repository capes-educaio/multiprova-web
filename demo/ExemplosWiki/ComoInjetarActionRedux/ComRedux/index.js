import { compose } from 'redux'
import { connect } from 'react-redux'

import { algumMapStateToProps, algumMapDispatchToProps } from './AlgumRedux'

import { AlgumComponent } from './ComReduxStateComponent'

export const Algum = compose(
  connect(
    algumMapStateToProps,
    algumMapDispatchToProps,
  ),
)(AlgumComponent)
