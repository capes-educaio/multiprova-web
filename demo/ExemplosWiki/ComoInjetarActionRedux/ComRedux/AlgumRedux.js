import { bindActionCreators } from 'redux'
import { actions } from 'utils/actions'

export function algumMapStateToProps(state) {
  return {}
}

export function algumMapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      selectAlgumState: actions.select.algumState,
    },
    dispatch,
  )
}
