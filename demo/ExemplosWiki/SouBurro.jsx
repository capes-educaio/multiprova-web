import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'

import { withStrings } from 'localModules/strings'
import { souBurroStyle } from 'souBurroStyle'

export const SouBurroComponent = props => <div className={props.classes.burro}>{props.strings.nomeProjeto}</div>

SouBurroComponent.propTypes = {
  strings: PropTypes.object.isRequired,
}

export const SouBurro = withStyles(souBurroStyle)(withStrings(SouBurroComponent))
