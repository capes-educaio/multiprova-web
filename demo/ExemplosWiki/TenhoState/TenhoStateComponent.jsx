import React, { Component } from 'react'

export class TenhoStateComponent extends Component {
  constructor(props) {
    super(props)
    this.state = {
      algumState: true,
    }
  }

  render() {
    return (
      <div>
        {this.state.algumState
          ? 'Algum state é verdadeiro'
          : 'Algum state é falso'}
      </div>
    )
  }
}
