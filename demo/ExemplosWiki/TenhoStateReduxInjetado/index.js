import { connect } from 'react-redux'

import { TenhoStateReduxInjetadoComponent } from './TenhoStateReduxInjetadoComponent'
import { tenhoStateReduxInjetadoMapStateToProps } from './tenhoStateReduxInjetadoMapStateToProps'

export const TenhoStateReduxInjetado = connect(
  tenhoStateReduxInjetadoMapStateToProps,
)(TenhoStateReduxInjetadoComponent)
