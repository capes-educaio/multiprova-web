import { connect } from 'react-redux'

import { TenhoActionReduxInjetadaComponent } from './TenhoActionReduxInjetadaComponent'
import {
  tenhoActionReduxInjetadaMapStateToProps,
  tenhoActionReduxInjetadaMapDispatchToProps,
} from './TenhoActionReduxInjetadaRedux'

export const TenhoActionReduxInjetada = connect(
  tenhoActionReduxInjetadaMapStateToProps,
  tenhoActionReduxInjetadaMapDispatchToProps,
)(TenhoActionReduxInjetadaComponent)
