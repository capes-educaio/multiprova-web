import React from './react'
import PropTypes from 'prop-types'

export const TenhoActionReduxInjetadaComponent = props => {
  props.selectAlgoNoReduxStore('algo')
  return <div>Outra coisa</div>
}

TenhoActionReduxInjetadaComponent.propTypes = {
  // redux actions
  selectAlgoNoRedux: PropTypes.func.isRequired,
}
