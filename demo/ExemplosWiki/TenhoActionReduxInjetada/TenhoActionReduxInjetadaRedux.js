import { bindActionCreators } from 'redux'
import { actions } from 'utils/actions'

export function tenhoActionReduxInjetadaMapStateToProps(state) {
  return {}
}

export function tenhoActionReduxInjetadaMapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      selectAlgoNoReduxStore: actions.select.algoNoReduxStore,
    },
    dispatch,
  )
}
