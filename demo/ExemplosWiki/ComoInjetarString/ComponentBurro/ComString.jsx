import React from 'react'
import PropTypes from 'prop-types'
import { withStrings } from 'localModules/strings'

const ComStringComponent = props => (
  <div>{this.props.algumaString}</div>
)

ComStringComponent.propTypes = {
  strings: PropTypes.object.isRequired,
}

export const ComString = withStrings(ComStringComponent)
