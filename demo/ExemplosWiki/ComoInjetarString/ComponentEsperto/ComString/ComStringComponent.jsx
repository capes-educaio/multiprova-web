import React, { Component } from 'react'
import PropTypes from 'prop-types'

export class ComStringComponent extends Component {
  constructor(props) {
    super(props)
    this.state = {
      tenhoState: true,
    }
  }

  render() {
    return <div>{this.props.algumaString}</div>
  }
}

ComStringComponent.propTypes = {
  strings: PropTypes.object.isRequired,
}
