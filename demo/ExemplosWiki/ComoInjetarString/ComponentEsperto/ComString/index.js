import { compose } from 'redux'

import { withStrings } from 'localModules/strings'

import { ComStringComponent } from './ComStringComponent'

export const ComString = compose(withStrings)(
  ComStringComponent,
)
