# multiprova-web

Aplicação web (frontend) para o sistema Educaio da UFRN.

## Instruções para rodar

1.  Clone o repositório `$ git clone https://gitlab.com/capes-educaio/multiprova-web.git`
2.  Entre no diretório: `$ cd multiprova-web`
3.  Instale as dependências: `$ npm install`
4.  Garantir que o projeto está apontando para serviços de backend ativos. Há algumas opções diferentes para esse caso. Veja arquivo `src/api/url.js`
    - Rodar os microserviços. Para isso reporte-se a: [multiprova-api](https://gitlab.com/capes-educaio/multiprova-api), [multiprova-html2pdf](https://gitlab.com/capes-educaio/multiprova-html2pdf) e [multiprova-educaio-doc](https://gitlab.com/capes-educaio/multiprova-educaio-doc)
5.  Inicie o serviço através do comando `$ npm start`

## Informações importantes na wiki

- [Estilo](https://gitlab.com/tapioca/multiprova/multiprova3/wikis/estilo)
- [Estrutura de arquivos](https://gitlab.com/tapioca/multiprova/multiprova3/wikis/estrutura-de-arquivos)
- [Padrão de projeto - Strings](https://gitlab.com/tapioca/multiprova/multiprova3/wikis/strings)
- [Padrão de projeto - Redux](https://gitlab.com/tapioca/multiprova/multiprova3/wikis/redux)
- [Padrão de projeto - UrlState](https://gitlab.com/tapioca/multiprova/multiprova3/wikis/url-state)
- [Padrão de projeto - Chamadas API](https://gitlab.com/tapioca/multiprova/multiprova3/wikis/chamadas-api)
- [Padrão de projeto - JSS](https://gitlab.com/tapioca/multiprova/multiprova3/wikis/jss)
- [Extensões do vscode](https://gitlab.com/tapioca/multiprova/multiprova3/wikis/extensões-do-vscode)
