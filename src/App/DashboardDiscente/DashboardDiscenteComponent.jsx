import React, { Component } from 'react'

import { propTypes } from './propTypes'
import { defaultProps } from './defaultProps'
import { provaUtils } from 'localModules/provas'

import { Dashboard } from 'common/Dashboard'

import { notasUltimas10Provas } from './graficos'

export class DashboardDiscenteComponent extends Component {
  static propTypes = propTypes
  static defaultProps = defaultProps

  constructor(props) {
    super(props)
    this.state = {
      carregando: false,
      estatisticas: {
        provas: [],
      },
      cardData: [],
      graphData: [],
    }
  }

  setDashboardData = () => {
    const { estatisticas } = this.state
    const { strings, classes, browser } = this.props
    const isMobile = browser.lessThan.medium
    const cardData = [
      {
        gridXs: isMobile ? 12 : 4,
        titulo: strings.provasParaFazer({ value: estatisticas.provasParaFazer }),
        value: estatisticas.provasParaFazer,
        classes,
      },
      {
        gridXs: isMobile ? 12 : 4,
        titulo: strings.provasJaRespondidas({ value: estatisticas.provasJaRespondidas }),
        value: estatisticas.provasJaRespondidas,
        classes,
      },
      {
        gridXs: isMobile ? 12 : 4,
        titulo: strings.vistasDeProvaEmAberto({ value: estatisticas.provasComVistaHabilitada }),
        value: estatisticas.provasComVistaHabilitada,
        classes,
      },
    ]
    const graphData = [
      {
        gridXs: 12,
        option: notasUltimas10Provas.option({ dados: estatisticas, strings, isMobile }),
      },
    ]
    this.setState({ cardData, graphData })
  }

  setEstatisticasDiscente = () => {
    const { usuarioAtual } = this.props
    var e1 = provaUtils
      .getEstatisticasProvasDiscente(usuarioAtual.data.id)
      .then(res => {
        this.setState({ estatisticas: { ...this.state.estatisticas, ...res.data } })
      })
      .catch(err => {
        console.warn(err)
      })

    Promise.all([e1]).then(() => {
      this.setDashboardData()
      this.setState({ carregando: false })
    })
  }

  componentDidMount = () => {
    this.setEstatisticasDiscente()
  }

  render() {
    const { carregando, cardData, graphData } = this.state
    return <Dashboard loading={carregando} cardData={cardData} graphData={graphData} />
  }
}
