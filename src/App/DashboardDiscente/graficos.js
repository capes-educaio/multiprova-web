import { theme } from 'utils/theme.js'
import { arredondar } from 'utils/arredondar.js'

const percent = 100

const _criarObjetoParaGrafico = (acc, cur) => {
  acc.notaAluno.push(arredondar(cur.prova.notaProva, 2))
  acc.valor.push(arredondar(cur.prova.valor, 2))
  acc.titulo.push(cur.prova.titulo)
  acc.notaPercent.push(arredondar((percent * cur.prova.notaProva) / cur.prova.valor, 2))
  return acc
}

export const notasUltimas10Provas = {
  option: ({ dados, strings, isMobile }) => {
    const { ultimas10Provas } = dados

    const notas =
      ultimas10Provas !== undefined
        ? ultimas10Provas.reduce(_criarObjetoParaGrafico, { notaAluno: [], valor: [], titulo: [], notaPercent: [] })
        : 0

    const option = {
      title: {
        text: strings.evolucaoDaNota,
        left: 'center',
        textStyle: {
          fontSize: isMobile ? 12 : 20,
          fontWeight: '100',
        },
      },
      toolbox: {
        feature: {
          saveAsImage: { show: true, title: strings.salvarComoImagem },
        },
        left: '35px',
        type: 'png',
      },
      legend: {
        show: true,
        data: [strings.provasAluno],
        bottom: '2%',
        orient: 'horizontal',
        borderWidth: 1,
        borderColor: theme.palette.grafico.grey.main,
        borderRadius: 5,
        padding: 10,
      },
      grid: {
        bottom: '25%',
      },
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'cross',
        },
        formatter: data => {
          return `${strings.notaAluno}: ${notas.notaAluno[data[0].dataIndex]}<br>
              ${strings.valorDaProva}: ${notas.valor[data[0].dataIndex]}`
        },
      },
      xAxis: {
        name: strings.notaProva,
        data: notas.titulo,
        splitNumber: 10,
        axisLabel: {
          show: false,
        },
        splitLine: {
          show: false,
        },
      },
      yAxis: {
        type: 'value',
        scale: true,
        min: 0,
        max: 100,
        axisLabel: {
          formatter: val => {
            return val + '%'
          },
        },
        splitArea: {
          show: false,
        },
        name: strings.notaAluno,
        nameTextStyle: {
          padding: 20,
        },
        nameLocation: 'center',
      },
      series: [
        {
          name: strings.provasAluno,
          type: 'scatter',
          itemStyle: {
            color: theme.palette.secondary.dark,
          },
          label: {
            emphasis: {
              show: true,
              formatter: param => param.value + '%',
              position: 'top',
              textStyle: {
                color: theme.palette.secondary.contrastText,
                fontSize: 14,
              },
            },
          },
          data: notas.notaPercent,
        },
      ],
    }
    return option
  },
}
