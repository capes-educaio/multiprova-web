import { bindActionCreators } from 'redux'

export function mapStateToProps(state) {
  return {
    strings: state.strings,
    usuarioAtual: state.usuarioAtual,
    browser: state.browser,
  }
}

export function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch)
}
