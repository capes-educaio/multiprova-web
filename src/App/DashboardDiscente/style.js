export const style = theme => ({
  card: {
    boxShadow: 'none',
    margin: 4,
    borderRadius: 5,
  },
  titulo: {
    padding: '3px 10px 3px 10px',
    color: theme.palette.gelo,
    background: theme.palette.primary.main,
  },
  numero: {
    padding: '15px 0px 5px 10px',
    fontWeight: '100',
    color: theme.palette.primary.main,
  },
})
