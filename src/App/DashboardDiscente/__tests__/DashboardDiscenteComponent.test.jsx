import Enzyme from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import { ptBr } from 'utils/strings/ptBr'

import { DashboardDiscenteComponent } from '../DashboardDiscenteComponent'
import { mountTest } from 'localModules/testUtils/mountTest'

Enzyme.configure({ adapter: new Adapter() })

jest.mock('localModules/provas', () => ({
  provaUtils: {
    getEstatisticasProvasDiscente: jest.fn().mockResolvedValue({ estatisticas: {} }),
    then: jest.fn(),
  },
}))

const requiredProps = {
  usuarioAtual: {
    data: {
      id: 1,
    },
  },
  // style
  classes: {},
  // strings
  strings: ptBr,
}

const arrayOfProps = [requiredProps]

mountTest(DashboardDiscenteComponent, 'DashboardDiscenteComponent')(arrayOfProps)
