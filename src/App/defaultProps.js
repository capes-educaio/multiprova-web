import { appConfig } from 'appConfig'

const { modulosHabilitados, homePath } = appConfig

export const defaultProps = {
  modulosHabilitados,
  homePath,
}
