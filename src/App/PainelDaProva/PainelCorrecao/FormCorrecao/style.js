export const style = theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
    marginTop: 10,
  },
  card: {
    minWidth: 275,
    background: 'transparent',
    boxShadow: '0 0',
  },
  top16: {
    marginTop: '10px',
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    flexGrow: 1,
    color: theme.palette.primary.contrastText,
  },
  pos: {
    marginBottom: 12,
  },
  vspace: {
    height: '16px',
  },
  // Painel
  painel: {
    width: '100%',
    height: '100vh',
    marginTop: '5vh',
  },
  painelInterno: {
    marginLeft: '10%',
    width: '80%',
    height: '80vh',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  top: {
    height: '30px',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  bottom: {
    height: '30px',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  left: {
    width: '30px',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-around',
  },
  content: {
    width: 'calc(100% - 60px)',
    boder: '1px solid black',
  },
  toolbar: {},
  areaDeTrabalho: {
    margin: 0,
    padding: 0,
  },
  right: {
    width: '30px',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-around',
  },
  controle: {
    minWidth: 0,
  },
  margin: {
    margin: 0,
    minWidth: 0,
  },
  // Painel content
  conteudo: {
    padding: '16px',
    overflowY: 'scroll',
    height: 'calc(80vh - 94px)',
  },
  botaoSalvar: {
    background: theme.palette.primary.main,
    color: theme.palette.primary.contrastText,
  },
  divider: {
    margin: '5px 0 15px 0',
    height: '2px',
  },
  aviso: {
    display: 'flex',
    margin: '5px 0 5px 0',
  },
  icon: { margin: 0, color: theme.palette.amareloQueimado },
  textoAviso: {
    margin: '0 0 0 10px',
    color: theme.palette.amareloQueimado,
  },
  expectativaDeResposta: {
    margin: '5px 0 20px 0',
  },
})
