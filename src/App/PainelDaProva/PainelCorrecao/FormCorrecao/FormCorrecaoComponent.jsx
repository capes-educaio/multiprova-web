import React, { Component } from 'react'

import { Send, Warning } from '@material-ui/icons'
import { Typography } from '@material-ui/core'

import { SimpleReduxForm, CampoText, CampoSlate } from 'localModules/ReduxForm'

import { MpButton } from 'common/MpButton'
import { MpParser } from 'common/MpParser'
import { Subtitulo } from 'common/Subtitulo'

export class FormCorrecaoComponent extends Component {
  formRef

  setFormRef = ref => (this.formRef = ref)

  mostrarErros = () => {
    if (this.formRef && this.formRef.showError) this.formRef.showError()
  }

  render() {
    const {
      classes,
      strings,
      matrixDeCorrecao,
      indexParticipante,
      indexQuestao,
      salvarCorrecao,
      disabled,
      onValid,
      isValid,
    } = this.props
    const { questoes, status } = matrixDeCorrecao[indexParticipante]
    const questao = questoes[indexQuestao]

    const { setFormRef, mostrarErros } = this

    if (questao.observacao === undefined) questao.observacao = '<p></p>'
    if (questao.notaQuestao === undefined) questao.notaQuestao = 0

    const expectativaDeResposta = (
      <div className={classes.expectativaDeResposta}>
        <Subtitulo>{strings.expectativaDeResposta}</Subtitulo>
        <MpParser>{questao.expectativaDeResposta}</MpParser>
      </div>
    )

    return (
      <div>
        {questao.discursiva && expectativaDeResposta}
        {disabled && (
          <div className={classes.aviso}>
            <Warning className={classes.icon} width="24px" />
            <Typography variant="body1" className={classes.textoAviso}>
              {strings.necessarioDespublicar}
            </Typography>
          </div>
        )}
        <Subtitulo>{strings.observacoesDaCorrecao}</Subtitulo>
        <SimpleReduxForm
          id={`formNotaDaQuestao${indexParticipante}${indexQuestao}`}
          valor={questao}
          onValid={onValid}
          onRef={setFormRef}
        >
          <CampoSlate
            key={`observacao${indexParticipante}${indexQuestao}`}
            id="observacao"
            accessor="observacao"
            inputProps={{
              readOnly: status === 0 || disabled,
              valor: questao.observacao,
              rows: 4,
              placeholder: strings.observacoes,
            }}
          />
          <div className={classes.vspace} />
          <CampoText
            key={`notaQuestao${indexParticipante}${indexQuestao}`}
            id="notaQuestao"
            accessor="notaQuestao"
            valor={0}
            label={strings.notaDaQuestao}
            textFieldProps={{
              disabled: status === 0 || disabled,
              type: 'number',
              variant: 'outlined',
              inputProps: { min: 0, max: questao.valor, step: 0.01 },
            }}
            validar={value => {
              let sucesso = true,
                erros = []
              if (value < 0) {
                sucesso = false
                erros.push({ code: 'valor_invalido' })
              }
              return { sucesso, erros }
            }}
            mensagensDeErro={{ valor_invalido: strings.valorInvalido }}
          />
        </SimpleReduxForm>
        <div className={classes.top16}>
          <MpButton
            IconComponent={Send}
            id="testButtonSalvar"
            buttonProps={{ variant: 'contained', color: 'primary' }}
            classes={{ root: classes.botaoSalvar }}
            variant="material"
            disabled={status === 0 || disabled}
            onClick={() => {
              if (isValid) salvarCorrecao()
              else mostrarErros()
            }}
          >
            {strings.salvar}
          </MpButton>
        </div>
      </div>
    )
  }
}
