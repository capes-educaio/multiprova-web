import React, { Component } from 'react'

import { Card, CardContent, Typography, Button, AppBar, Toolbar, Paper, Fab } from '@material-ui/core'

import { KeyboardArrowUp, KeyboardArrowDown, KeyboardArrowLeft, KeyboardArrowRight, Close } from '@material-ui/icons'
import { QuestaoDiscente } from 'localModules/questoes/QuestaoDiscenteProva'
import { FormCorrecao } from './FormCorrecao'
import { provaUtils } from 'localModules/provas/ProvaUtils'
import { updateUrlState } from 'localModules/urlState'
import { Container } from 'common/Container'
import CircularProgress from '@material-ui/core/CircularProgress'
import { enumTipoQuestao } from 'utils/enumTipoQuestao'

import { Snackbar } from 'utils/Snackbar'
import { enumPainelTabs } from '../enumPainelTabs'

export class PainelCorrecaoComponent extends Component {
  constructor(props) {
    super(props)

    this._provaAtualId = this.props.match.params.id

    this.state = {
      indexParticipante: Number(this.props.match.params.indexParticipante),
      indexQuestao: Number(this.props.match.params.indexQuestao),
      snackbarIsOpen: false,
      isValid: true,
    }

    this.handlers = {
      ArrowLeft: this.handleBack,
      ArrowRight: this.handleNext,
      ArrowUp: this.handleUp,
      ArrowDown: this.handleDown,
    }
  }

  componentDidMount() {
    if (this.props.matrixDeCorrecao.length === 0) {
      this.carregarMatrixDeCorrecao()
    }
  }

  carregarMatrixDeCorrecao() {
    const usuarioAtualId = this.props.usuarioAtual.data.id
    const provaAtualId = this.props.match.params.id
    provaUtils.getProva({ usuarioAtualId, provaAtualId }).then(res => {
      const tipoProva = res.data.tipoProva
      provaUtils.getMatrixDeCorrecao({ provaAtualId, tipoProva })
    })
  }

  componentWillMount() {
    document.addEventListener('keydown', this.controllerPainel)
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this.controllerPainel)
  }

  get isNotasPublicadas() {
    const { matrixDeCorrecao } = this.props
    const { indexParticipante } = this.state
    return matrixDeCorrecao[indexParticipante] && matrixDeCorrecao[indexParticipante].notasPublicadas
      ? matrixDeCorrecao[indexParticipante].notasPublicadas
      : false
  }

  salvarCorrecao = () => {
    if (!this.isNotasPublicadas) {
      const { matrixDeCorrecao } = this.props
      const { indexParticipante, indexQuestao } = this.state
      const { questoes } = matrixDeCorrecao[indexParticipante]
      const questao = questoes[indexQuestao]
      const instanciaId = matrixDeCorrecao[indexParticipante].id
      const isBloco = questao.bloco !== undefined

      const dados = {
        observacao: questao.observacao,
        notaQuestao: parseFloat(questao.notaQuestao),
        questaoMatrizId: isBloco ? questao.questaoMatrizId : undefined,
      }

      const idQuestao = isBloco ? questao.bloco.questaoMatrizId : questao.questaoMatrizId
      provaUtils
        .updateCorrecao({ instanciaId, idQuestao, dados })
        .then(() => {
          this.setState({ snackbarIsOpen: true })
        })
        .catch(erro => console.error(erro))
    }
  }

  snackbarShoudClose = () => {
    this.setState({ snackbarIsOpen: false })
  }

  controllerPainel = event => {
    if (this.handlers[event.key] !== undefined) this.handlers[event.key]()
  }

  handleUp = () => {
    this.salvarCorrecao()
    const { indexParticipante } = this.state
    if (this.isPrimeiraAluno(indexParticipante)) {
      const ultimoParticipante = this.props.matrixDeCorrecao.length - 1
      this.setState({ indexParticipante: ultimoParticipante })
    } else this.setState({ indexParticipante: indexParticipante - 1 })
  }

  handleNext = () => {
    this.salvarCorrecao()
    const { indexParticipante, indexQuestao } = this.state
    if (this.isUltimaQuestao(indexParticipante, indexQuestao + 1)) this.setState({ indexQuestao: 0 })
    else this.setState({ indexQuestao: this.state.indexQuestao + 1 })
  }

  handleBack = () => {
    this.salvarCorrecao()
    const { indexParticipante, indexQuestao } = this.state
    if (this.isPrimeiraQuestao(indexQuestao)) {
      const { questoes } = this.props.matrixDeCorrecao[indexParticipante]
      const ultimaQuestao = questoes.length - 1
      this.setState({ indexQuestao: ultimaQuestao })
    } else this.setState({ indexQuestao: this.state.indexQuestao - 1 })
  }

  handleDown = () => {
    this.salvarCorrecao()
    const { indexParticipante } = this.state
    if (this.isUltimoAluno(indexParticipante + 1)) this.setState({ indexParticipante: 0 })
    else this.setState({ indexParticipante: indexParticipante + 1 })
  }

  isPrimeiraAluno = iParticipante => {
    if (iParticipante === 0) return true
    else return false
  }

  isUltimoAluno = iParticipante => {
    const maxV = this.props.matrixDeCorrecao.length - 1
    if (iParticipante > maxV) return true
    else return false
  }

  isPrimeiraQuestao = iQuestao => {
    if (iQuestao === 0) return true
    else return false
  }

  isUltimaQuestao = (iParticipante, iQuestao) => {
    const { questoes } = this.props.matrixDeCorrecao[iParticipante]
    const maxH = questoes.length - 1
    if (iQuestao > maxH) return true
    else return false
  }

  handleClose = () => {
    updateUrlState({
      pathname: `/prova/${this._provaAtualId}/painel`,
      hidden: { painelDeProva: enumPainelTabs.matrizDeCorrecao },
    })
  }

  onValid = isValid => {
    if (this.id !== undefined) this.setState({ isValid: true })
    else this.setState({ isValid })
  }

  _qtdQuestoes = questoes => {
    if (!questoes) return 0
    let qtdQuestoes = 0
    questoes.forEach(questao => {
      if (questao.tipo === enumTipoQuestao.tipoQuestaoBloco) qtdQuestoes += this._qtdQuestoes(questao.bloco.questoes)
      else qtdQuestoes += 1
    })
    return qtdQuestoes
  }

  render() {
    const { classes, strings, matrixDeCorrecao, carregando } = this.props
    const { indexParticipante, indexQuestao, snackbarIsOpen, isValid } = this.state
    const { handleBack, handleNext, handleUp, handleDown, handleClose, salvarCorrecao, isNotasPublicadas } = this

    if (matrixDeCorrecao.length < 1) return <div />

    const { questoes, matricula, nome, status, titulo } = matrixDeCorrecao[indexParticipante]
    let qtnQuestoes = this._qtdQuestoes(questoes)
    const questao = questoes[indexQuestao]

    if (questao.observacao === undefined) questao.observacao = '<p></p>'
    if (questao.notaQuestao === undefined) questao.notaQuestao = 0

    return (
      <Container>
        {carregando ? (
          <div className={classes.carregando}>
            <CircularProgress />
          </div>
        ) : null}
        {!carregando && (
          <Paper className={classes.paper}>
            <Card className={classes.card}>
              <CardContent>
                <div className={classes.painel}>
                  <div className={classes.top}>
                    <Button
                      size="large"
                      className={classes.margin}
                      onClick={() => {
                        if (isValid) handleUp()
                      }}
                    >
                      <KeyboardArrowUp />
                    </Button>
                  </div>
                  <div className={classes.painelInterno}>
                    <div className={classes.left}>
                      <Button
                        className={classes.margin}
                        onClick={() => {
                          if (isValid) handleBack()
                        }}
                      >
                        <KeyboardArrowLeft />
                      </Button>
                    </div>
                    <Card className={classes.content}>
                      <CardContent className={classes.areaDeTrabalho}>
                        <AppBar position="static">
                          <Toolbar className={classes.toolbar}>
                            <Typography variant="h6" className={classes.title}>
                              {strings.painelCorrecao}
                            </Typography>
                            <Fab size="small" color="primary" onClick={handleClose}>
                              <Close />
                            </Fab>
                          </Toolbar>
                        </AppBar>
                        <div className={classes.conteudo}>
                          <Typography variant="h6">{titulo}</Typography>
                          <Typography>
                            <strong>
                              {matricula}
                              {' - '}
                              {nome}
                            </strong>
                          </Typography>
                          <Typography>{`${Number(indexQuestao) + 1} de ${qtnQuestoes}`}</Typography>
                          <QuestaoDiscente
                            key={`${indexParticipante}${indexQuestao}`}
                            questaoValue={questao}
                            numeroDeQuestoes={qtnQuestoes}
                            questaoSelecionada={indexQuestao}
                            disabled={true}
                          />
                          <FormCorrecao
                            onValid={this.onValid}
                            isValid={isValid}
                            key={`form${indexParticipante}${indexQuestao}`}
                            strings={strings}
                            indexQuestao={indexQuestao}
                            indexParticipante={indexParticipante}
                            salvarCorrecao={salvarCorrecao}
                            disabled={isNotasPublicadas}
                          />
                          {status === 0 && (
                            <Typography variant="p" className={classes.warn}>
                              {strings.provaSendoRespondida}
                            </Typography>
                          )}
                        </div>
                      </CardContent>
                    </Card>
                    <div className={classes.right}>
                      <Button size="small" className={classes.margin}>
                        <KeyboardArrowRight
                          onClick={() => {
                            if (isValid) handleNext()
                          }}
                        />
                      </Button>
                    </div>
                  </div>
                  <div className={classes.bottom}>
                    <Button
                      size="small"
                      className={classes.margin}
                      onClick={() => {
                        if (isValid) handleDown()
                      }}
                    >
                      <KeyboardArrowDown />
                    </Button>
                  </div>
                </div>
              </CardContent>
            </Card>
          </Paper>
        )}
        <Snackbar
          variant="success"
          open={snackbarIsOpen}
          onClose={this.snackbarShoudClose}
          message={strings.asAlteracoesForamSalvas}
          autoHideDuration={3000}
        />
      </Container>
    )
  }
}
