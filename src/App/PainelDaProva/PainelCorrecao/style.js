export const style = theme => ({
  carregando: {
    display: 'flex',
    justifyContent: 'center',
    position: 'absolute',
    top: '50%',
    left: '50%',
  },
  paper: {
    flexGrow: 1,
    backgroundColor: 'transparent',
    marginTop: 10,
    boxShadow: '0 0',
  },
  card: {
    minWidth: 275,
    background: 'transparent',
    boxShadow: '0 0',
  },
  top16: {
    marginTop: '10px',
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    flexGrow: 1,
    color: theme.palette.primary.contrastText,
  },
  pos: {
    marginBottom: 12,
  },
  warn: {
    color: theme.palette.amareloQueimado,
    marginTop: '16px',
  },
  // Painel
  painel: {
    width: '100%',
  },
  painelInterno: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  top: {
    height: '30px',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  bottom: {
    height: '30px',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  left: {
    width: '30px',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-around',
  },
  content: {
    width: 'calc(100% - 60px)',
    boder: '1px solid black',
  },
  areaDeTrabalho: {
    margin: 0,
    padding: 0,
  },
  right: {
    width: '30px',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-around',
  },
  controle: {
    minWidth: 0,
  },
  margin: {
    margin: 0,
    minWidth: 0,
  },
  // Painel content
  conteudo: {
    padding: '20px',
  },
  botaoSalvar: {
    background: theme.palette.primary.main,
    color: theme.palette.primary.contrastText,
  },
})
