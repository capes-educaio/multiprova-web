import { bindActionCreators } from 'redux'
// import { actions } from 'utils/actions'

export function mapStateToProps(state) {
  return {
    strings: state.strings,
    matrixDeCorrecao: state.matrixDeCorrecao,
    usuarioAtual: state.usuarioAtual,
    carregando: state.carregando,
  }
}

export function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch)
}
