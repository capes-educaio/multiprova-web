import React, { Component } from 'react'
import { propTypes } from './propTypes'
import { Paper, IconButton, Tooltip, Button } from '@material-ui/core'
import { Table, TableCell, TableBody, TableHead, TableRow } from '@material-ui/core'
import DescriptionIcon from '@material-ui/icons/Description'
import { ChipSwitch } from './ChipSwitch'
import { setUrlState } from 'localModules/urlState'
import { GeradorPDF } from 'common/GeradorPDF'
import { enumTemplate } from 'utils/enumTemplate'
import { enumStatusInstanciamento } from 'utils/enumStatusInstanciamento'

export class ResumoDaAplicacaoComponent extends Component {
  static propTypes = propTypes
  _urlImpressao
  _geradorPDFRef = []

  constructor(props) {
    super(props)
    this.state = {
      carregando: false,
    }
    this._urlImpressao = '/instanciamentos/imprimir-instancias/'
  }

  convert = value =>
    Number(value)
      .toFixed(2)
      .replace('.', ',')

  openPainelDeCorrecao = indexParticipante => () => {
    const { provaId } = this.props
    setUrlState({ pathname: `/prova/${provaId}/correcao/${indexParticipante}/0` })
  }

  render() {
    const { classes, strings, resumo, dataTermino } = this.props

    let provaAplicada = false
    if (new Date(dataTermino) < new Date()) provaAplicada = true

    return (
      <div className={classes.root}>
        <Paper style={{ overflowX: 'auto' }}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>{strings.matricula}</TableCell>
                <TableCell className={classes.fullWidth}>{strings.nome}</TableCell>
                <TableCell align="center">{strings.status}</TableCell>
                <TableCell align="center">{strings.nota}</TableCell>
                <TableCell />
              </TableRow>
            </TableHead>
            <TableBody>
              {resumo.map(({ matricula, nome, status, nota, idNumero }, index) => (
                <TableRow key={`${matricula}${index}`}>
                  <TableCell align="left" onClick={this.openPainelDeCorrecao(index)}>
                    <Button className={classes.btnInk}>{matricula ? matricula : '-'}</Button>
                  </TableCell>
                  <TableCell align="left" className={classes.fullWidth} onClick={this.openPainelDeCorrecao(index)}>
                    <Button className={classes.btnInk}>{nome ? nome : strings.provaNaoAssociada(idNumero)}</Button>
                  </TableCell>
                  <TableCell align="center">
                    <ChipSwitch status={status} />
                  </TableCell>
                  <TableCell align="center">
                    {status === enumStatusInstanciamento.naoIniciada && !provaAplicada ? '-' : this.convert(nota)}
                  </TableCell>
                  <TableCell align="center">
                    <Tooltip placement="left" title={strings.baixar} aria-label={strings.baixar}>
                      <IconButton size="small">
                        <DescriptionIcon fontSize="small" onClick={() => this._geradorPDFRef[index].gerarPDF()} />
                      </IconButton>
                    </Tooltip>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
          {resumo.map(({ id, titulo }, index) => (
            <GeradorPDF
              key={id + index}
              stringGerandoPDF={strings.imprimindoProvas}
              url={this._urlImpressao + id}
              dados={{
                template: enumTemplate.provaGeralCandidato,
                titulo: titulo || 'Prova',
                usuarioId: this.props.usuarioAtual.data.id,
              }}
              onRef={geradorPDFRef => {
                this._geradorPDFRef.push(geradorPDFRef)
              }}
            />
          ))}
        </Paper>
      </div>
    )
  }
}
