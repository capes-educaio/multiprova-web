import { ResumoDaAplicacaoComponent } from './ResumoDaAplicacaoComponent'
import { compose } from 'redux'
import { connect } from 'react-redux'

import { withStyles } from '@material-ui/core/styles'

import { mapStateToProps, mapDispatchToProps } from './redux'
import { style } from './style'

export const ResumoDaAplicacao = compose(
  withStyles(style),
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(ResumoDaAplicacaoComponent)
