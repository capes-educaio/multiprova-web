import React from 'react'
import classnames from 'classnames'
import { propTypes } from './propTypes'
import { Chip } from '@material-ui/core'
import { enumStatusInstanciamento } from 'utils/enumStatusInstanciamento'

export const ChipSwitchComponent = ({ status, strings, classes }) => {
  switch (status) {
    case enumStatusInstanciamento.naoIniciada:
      return <Chip className={classnames(classes.chip, classes.naoIniciada)} size="small" label={strings.naoIniciada} />
    case enumStatusInstanciamento.emExecucao:
      return <Chip className={classnames(classes.chip, classes.emExecucao)} size="small" label={strings.emExecucao} />
    case enumStatusInstanciamento.concluida:
      return <Chip className={classnames(classes.chip, classes.concluido)} size="small" label={strings.concluida} />

    default:
      return null
  }
}

ChipSwitchComponent.propTypes = propTypes
