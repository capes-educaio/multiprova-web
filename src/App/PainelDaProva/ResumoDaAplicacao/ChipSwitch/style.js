export const style = theme => ({
  chip: { borderRadius: 5, color: theme.palette.gelo },
  emExecucao: { backgroundColor: theme.palette.mediumBlue },
  concluido: { backgroundColor: theme.palette.success.dark },
  naoIniciada: { backgroundColor: theme.palette.error.dark },
})
