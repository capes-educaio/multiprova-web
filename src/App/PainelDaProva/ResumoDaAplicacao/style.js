export const style = theme => ({
  root: { padding: 20 },
  fullWidth: { width: '100%' },
  tableRow: {
    padding: 0,
  },
  btnInk: {
    padding: '5',
    textTransform: 'initial',
    justifyContent: 'left',
    minWidth: 'auto',
    minHeight: 'auto',
  },
})
