import PropTypes from 'prop-types'

export const propTypes = {
  width: PropTypes.string.isRequired,
  location: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
