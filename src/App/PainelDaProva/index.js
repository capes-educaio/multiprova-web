import { PainelDaProvaComponent } from './PainelDaProvaComponent'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import withWidth from '@material-ui/core/withWidth'
import { withStyles } from '@material-ui/core/styles'

import { mapStateToProps, mapDispatchToProps } from './redux'
import { style } from './style'

export const PainelDaProva = compose(
  withStyles(style),
  withWidth(),
  withRouter,
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(PainelDaProvaComponent)
