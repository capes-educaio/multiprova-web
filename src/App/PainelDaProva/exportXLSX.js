import XLSX from 'xlsx'
import { saveAs } from 'file-saver'

const formatarAlunos = (strings, alunos) =>
  alunos.map(({ matricula, nome, nota }) => ({
    [strings.matricula]: matricula,
    [strings.nome]: nome,
    [strings.nota]: nota,
  }))

export const exportXLSX = ({ strings, resumo }) => {
  var worksheet = XLSX.utils.json_to_sheet(formatarAlunos(strings, resumo))
  var wb = XLSX.utils.book_new()
  XLSX.utils.book_append_sheet(wb, worksheet, strings.resumoDaAplicacao)

  var wbout = XLSX.write(wb, { bookType: 'xlsx', type: 'binary' })

  /* generate a download */
  function s2ab(s) {
    var buf = new ArrayBuffer(s.length)
    var view = new Uint8Array(buf)
    for (var i = 0; i !== s.length; ++i) view[i] = s.charCodeAt(i) & 0xff
    return buf
  }

  saveAs(new Blob([s2ab(wbout)], { type: 'application/octet-stream' }), `${strings.resumoDaAplicacao}.xlsx`)
}
