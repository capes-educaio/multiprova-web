export const style = theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
    boxShadow: 'none',
  },
  buttonIcon: {
    marginRight: 6,
  },
  selectContainer: { padding: 8, marginLeft: 15 },
  appBar: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    boxShadow: 'none',
  },
  toRight: {
    marginLeft: 'auto',
  },
  botaoVoltar: {
    marginRight: 15,
  },
  legenda: {
    marginLeft: 15,
  },
  tab: {
    background: theme.palette.primary.contrastText,
  },
  questionIconSuccess: { fontSize: 16, color: theme.palette.success.dark },
  questionIconError: { fontSize: 16, color: theme.palette.error.dark },
})
