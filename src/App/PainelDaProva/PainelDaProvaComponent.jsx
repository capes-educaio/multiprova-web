import React, { Component } from 'react'
import { propTypes } from './propTypes'

import { Paper, AppBar, Tabs, Tab, Button, FormControl, InputLabel, Select, MenuItem } from '@material-ui/core'
import Typography from '@material-ui/core/Typography'
import IconButton from '@material-ui/core/IconButton'
import Ok from '@material-ui/icons/CheckCircleOutline'
import Error from '@material-ui/icons/ErrorOutline'

import { ResumoDaAplicacao } from './ResumoDaAplicacao'
import { MatrizDeCorrecao } from './MatrizDeCorrecao'
import { Estatisticas } from 'common/Estatisticas'
import { updateUrlState, setUrlState } from 'localModules/urlState'
import { exportXLSX } from './exportXLSX'
import { CloudDownload } from '@material-ui/icons'
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos'
import { provaUtils } from 'localModules/provas/ProvaUtils'
import { enumPainelTabs } from './enumPainelTabs'

export class PainelDaProvaComponent extends Component {
  static propTypes = propTypes
  _prova
  state = {
    activeTab: this.props.location.state.hidden.painelDeProva || enumPainelTabs.resumoDaAplicacao,
    provaAplicada: false,
    isEstatisticas: false,
  }

  getData = value => {
    this.setState({ activeTab: value })
    if (value === enumPainelTabs.resumoDaAplicacao) {
      provaUtils.getResumo({ provaAtualId: this._provaAtualId })
    } else if (value === enumPainelTabs.matrizDeCorrecao) {
      provaUtils.getMatrixDeCorrecao({ provaAtualId: this._provaAtualId, tipoProva: this._prova.tipoProva })
    }
  }

  componentDidMount = () => {
    this._usuarioAtualId = this.props.usuarioAtual.data.id
    this._provaAtualId = this.props.match.params.id

    provaUtils.getProva({ usuarioAtualId: this._usuarioAtualId, provaAtualId: this._provaAtualId }).then(res => {
      this._prova = res.data
      const provaAplicada = res.data.status === provaUtils.enumStatusProva.aplicada
      const isEstatisticas = !!res.data.estatisticas
      this.setState({ provaAplicada, isEstatisticas })
      this.getData(this.state.activeTab)
    })
  }

  handleChange = (event, newValue) => {
    this.getData(newValue)
    this.setState({
      activeTab: newValue,
    })
    updateUrlState({ hidden: { painelDeProva: newValue } })
  }

  handleExportar = () => {
    const { strings, resumo } = this.props
    exportXLSX({ strings, resumo })
  }

  handleVoltar = () => {
    setUrlState({ pathname: '/provas' })
  }

  render() {
    const { classes, strings, resumo, matrixDeCorrecao, width } = this.props
    const { activeTab, provaAplicada, isEstatisticas } = this.state

    const dataTermino =
      this._prova && this._prova === 'virtual' ? this._prova.dadosAplicacao.virtual.dataTerminoProva : undefined
    return (
      <div>
        <AppBar className={classes.appBar} position="static" color="inherit">
          {width === 'xs' ? (
            <div className={classes.selectContainer}>
              <FormControl>
                <InputLabel>{strings.painelProva}</InputLabel>
                <Select value={activeTab} onChange={event => this.handleChange(event, event.target.value)}>
                  <MenuItem value={enumPainelTabs.resumoDaAplicacao}>{strings.resumoDaAplicacao}</MenuItem>
                  <MenuItem value={enumPainelTabs.matrizDeCorrecao}>{strings.matrizDeCorrecao}</MenuItem>
                  <MenuItem disabled={!(provaAplicada && isEstatisticas)} value={enumPainelTabs.estatisticas}>
                    {strings.estatisticas}
                  </MenuItem>
                </Select>
              </FormControl>
            </div>
          ) : (
            <Tabs value={activeTab} onChange={this.handleChange}>
              <Tab label={strings.resumoDaAplicacao} />
              <Tab label={strings.matrizDeCorrecao} />
              <Tab label={strings.estatisticas} disabled={!(provaAplicada && isEstatisticas)} />
            </Tabs>
          )}
          <Button className={classes.toRight} color="primary" size="small" onClick={this.handleExportar}>
            <CloudDownload className={classes.buttonIcon} fontSize="small" />
            {width === 'xs' ? '' : strings.exportarNotas}
          </Button>
          <Button className={classes.botaoVoltar} color="primary" size="small" onClick={this.handleVoltar}>
            <ArrowBackIosIcon fontSize="small" />
            {width === 'xs' ? '' : strings.voltar}
          </Button>
        </AppBar>
        <Paper className={classes.root}>
          {resumo.length > 0 !== undefined && activeTab === enumPainelTabs.resumoDaAplicacao && (
            <ResumoDaAplicacao provaId={this._provaAtualId} dataTermino={dataTermino} />
          )}
          {matrixDeCorrecao.length > 0 && activeTab === enumPainelTabs.matrizDeCorrecao && (
            <div>
              <MatrizDeCorrecao provaId={this._provaAtualId} />
              <div className={classes.legenda}>
                <Typography variant="caption">
                  <IconButton>
                    <Ok className={classes.questionIconSuccess} />
                  </IconButton>
                  {strings.questaoCorrigida}
                  <IconButton>
                    <Error className={classes.questionIconError} />
                  </IconButton>
                  {strings.questaoNaoCorrigida}
                </Typography>
              </div>
            </div>
          )}
        </Paper>
        {provaAplicada && isEstatisticas && activeTab === enumPainelTabs.estatisticas && (
          <Estatisticas prova={this._prova} />
        )}
      </div>
    )
  }
}
