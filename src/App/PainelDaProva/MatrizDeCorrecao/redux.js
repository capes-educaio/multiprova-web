import { bindActionCreators } from 'redux'

export function mapStateToProps(state) {
  return {
    strings: state.strings,
    matrixDeCorrecao: state.matrixDeCorrecao,
    usuarioAtual: state.usuarioAtual,
  }
}

export function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch)
}
