import PropTypes from 'prop-types'

export const propTypes = {
  // style
  classes: PropTypes.object.isRequired,
  //redux
  strings: PropTypes.object.isRequired,
}
