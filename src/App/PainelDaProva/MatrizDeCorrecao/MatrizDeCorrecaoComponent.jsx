import React, { Component } from 'react'
import { Table, TableBody, TableCell, TableHead, TableRow, Paper, Tooltip, IconButton } from '@material-ui/core'
import Ok from '@material-ui/icons/CheckCircleOutline'
import Download from '@material-ui/icons/Print'
import Error from '@material-ui/icons/ErrorOutline'
import { setUrlState } from 'localModules/urlState'
import { propTypes } from './propTypes'
import { GeradorPDF } from 'common/GeradorPDF'
import { enumTemplate } from 'utils/enumTemplate'
import { enumStatusInstanciamento } from 'utils/enumStatusInstanciamento'

export class MatrizDeCorrecaoComponent extends Component {
  static propTypes = propTypes
  _urlImpressao
  _geradorPDFRef = []

  constructor(props) {
    super(props)
    this._urlImpressao = '/instanciamentos/imprimir-instancias/'
  }

  componentWillMount() {
    document.addEventListener('keydown', this.controllerPainel)
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this.controllerPainel)
  }

  questionIcon = (prova, questao) => {
    const { classes } = this.props
    let status = false
    if (prova.status === enumStatusInstanciamento.concluida) {
      if (questao.notaQuestao >= 0) status = true
    } else if (prova.status === enumStatusInstanciamento.naoIniciada && prova.aplicacaoFinalizada) {
      return <Ok className={classes.questionIconSuccess} />
    }
    return status ? <Ok className={classes.questionIconSuccess} /> : <Error className={classes.questionIconError} />
  }

  openPainelDeCorrecao = (indexParticipante, indexQuestao) => () => {
    const { provaId } = this.props
    setUrlState({ pathname: `/prova/${provaId}/correcao/${indexParticipante}/${indexQuestao}` })
  }

  render() {
    const { classes, strings, matrixDeCorrecao } = this.props
    const { questionIcon, openPainelDeCorrecao } = this

    return (
      <div className={classes.root}>
        <Paper className={classes.paper}>
          <Table className={classes.table} size="small">
            <TableHead>
              <TableRow>
                <TableCell>{strings.matricula}</TableCell>
                {matrixDeCorrecao[0].questoes.map((q, i) => (
                  <TableCell key={'q-' + i} className={classes.tableRow} align="center">
                    {`Q${i + 1}`}
                  </TableCell>
                ))}
                <TableCell className={classes.tableRow} align="center">
                  {' '}
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {matrixDeCorrecao.map((prova, iParticipante) => (
                <TableRow key={`${prova.matricula}${iParticipante}`}>
                  <Tooltip placement="right" title={prova.nome} aria-label={prova.nome}>
                    <TableCell component="th" scope="row">
                      {prova.matricula ? prova.matricula : '-'}
                    </TableCell>
                  </Tooltip>
                  {prova.questoes.map((questao, iQuestao) => (
                    <TableCell
                      key={'qicon-' + iQuestao}
                      className={classes.tableRow}
                      align="center"
                      onClick={openPainelDeCorrecao(iParticipante, iQuestao)}
                    >
                      <IconButton>{questionIcon(prova, questao)}</IconButton>
                    </TableCell>
                  ))}
                  <TableCell className={classes.tableRow} align="center">
                    <IconButton onClick={() => this._geradorPDFRef[iParticipante].gerarPDF()}>
                      <Download />
                    </IconButton>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </Paper>
        {matrixDeCorrecao.map((row, iParticipante) => (
          <GeradorPDF
            key={row.id + iParticipante}
            stringGerandoPDF={strings.imprimindoProvas}
            url={this._urlImpressao + row.id}
            dados={{
              template: enumTemplate.provaGeralCandidato,
              titulo: row.titulo || 'Prova',
              usuarioId: this.props.usuarioAtual.data.id,
            }}
            onRef={geradorPDFRef => {
              this._geradorPDFRef.push(geradorPDFRef)
            }}
          />
        ))}
      </div>
    )
  }
}
