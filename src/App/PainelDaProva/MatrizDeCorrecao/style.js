export const style = theme => ({
  root: {
    padding: 20,
  },
  painelButtonIcon: { marginRight: 6 },
  paper: {
    width: '100%',
    overflowX: 'auto',
  },
  table: {
    minWidth: 650,
  },
  tableRow: {
    padding: 0,
  },
  questionIconSuccess: { fontSize: 16, color: theme.palette.success.dark },
  questionIconError: { fontSize: 16, color: theme.palette.error.dark },
})
