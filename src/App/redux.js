import { bindActionCreators } from 'redux'
import { resetSimpleStates } from 'utils/resetSimpleStates'
import { actions } from 'utils/actions'

export function mapStateToProps(state) {
  return {
    themeChosen: state.theme,
    strings: state.strings,
    usuarioAtual: state.usuarioAtual,
    notificacoes: state.notificacoes,
    messages: state.messages,
  }
}

export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      resetSimpleStates,
      addNotificacao: actions.push.notificacoes,
      removeMessage: actions.pop.messages,
    },
    dispatch,
  )
}
