import React, { Component } from 'react'

import MenuItem from '@material-ui/core/MenuItem'
import Select from '@material-ui/core/Select'
import QuestionAnswer from '@material-ui/icons/QuestionAnswer'
import Description from '@material-ui/icons/Description'

import { setUrlState } from 'localModules/urlState'

import { TituloDaPagina } from 'common/TituloDaPagina'
import { PaginaPaper } from 'common/PaginaPaper'
import { LixeiraQuestoes } from './LixeiraQuestoes'
import { LixeiraProvas } from './LixeiraProvas'
import { propTypes } from './propTypes'

export class LixeiraComponent extends Component {
  static propTypes = propTypes

  componentDidMount() {
    const { tipoLixeira } = this.props.match.params
    if (tipoLixeira !== 'questoes' && tipoLixeira !== 'provas') setUrlState({ pathname: '/lixeira/questoes' })
  }

  get lixeira() {
    const { tipoLixeira } = this.props.match.params
    switch (tipoLixeira) {
      case 'questoes':
        return <LixeiraQuestoes forceUpdate={metodo => (this.forceUpdateLixeiraQuestoes = metodo)} />
      case 'provas':
        return <LixeiraProvas />
      default:
        console.log('Tipo de lixeira inválida')
        return null
    }
  }

  UNSAFE_componentWillReceiveProps() {
    const { tipoLixeira } = this.props.match.params
    if (tipoLixeira === 'questoes') {
      this.forceUpdateLixeiraQuestoes()
    }
  }

  mudarTipoLixeira = event => {
    setUrlState({ pathname: `/lixeira/${event.target.value}` })
  }

  render() {
    const { strings, classes, match } = this.props
    const { tipoLixeira } = match.params
    return (
      <div>
        <PaginaPaper>
          <TituloDaPagina>
            {strings.lixeira}
            <Select value={tipoLixeira} onChange={this.mudarTipoLixeira} className={classes.select}>
              <MenuItem value="questoes">
                <QuestionAnswer />
                {strings.questoes}
              </MenuItem>
              <MenuItem value="provas">
                <Description />
                {strings.provas}
              </MenuItem>
            </Select>
          </TituloDaPagina>
        </PaginaPaper>
        {this.lixeira}
      </div>
    )
  }
}
