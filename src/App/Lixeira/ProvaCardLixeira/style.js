export const style = theme => ({
  menuIcon: {
    marginRight: '3px',
    color: theme.palette.primary.main,
  },
})
