import React, { Component } from 'react'

import MenuItem from '@material-ui/core/MenuItem'
import DeleteForever from '@material-ui/icons/DeleteForever'
import Undo from '@material-ui/icons/Undo'

import { post, del } from 'api'

import { ProvaCard } from 'common/ProvaCard'
import { MenuOpcoes } from 'common/MenuOpcoes'

export class ProvaCardLixeiraComponent extends Component {
  restaurar = () => {
    const { id } = this.props.instancia
    let url = `/provas/lixeira/restaurar/${id}`
    post(url).then(() => {
      this.atualizarPagina()
    })
  }

  excluirProvaDefinitivamente = () => {
    const { id } = this.props.instancia
    let url = `/provas/lixeira/${id}`
    del(url).then(() => {
      this.atualizarPagina()
    })
  }

  openDialogExcluir = () => {
    const { listaDialogPush, strings } = this.props
    const dialogConfig = {
      titulo: strings.excluirDefinitivamente,
      texto: strings.desejaExcluirProvaDefinitivamente,
      actions: [
        {
          texto: strings.ok,
          onClick: this.excluirProvaDefinitivamente,
        },
        {
          texto: strings.cancelar,
        },
      ],
    }
    listaDialogPush(dialogConfig)
  }

  atualizarPagina = () => this.props.atualizarCardDisplay()

  render() {
    const { classes, instancia, strings } = this.props
    const menuHeader = (
      <MenuOpcoes>
        <MenuItem onClick={this.restaurar}>
          <Undo color="action" className={classes.menuIcon} />
          <span>{strings.restaurar}</span>
        </MenuItem>
        <MenuItem id="item-menu-excluir" onClick={this.openDialogExcluir}>
          <DeleteForever color="action" className={classes.menuIcon} />
          <span>{strings.excluirDefinitivamente}</span>
        </MenuItem>
      </MenuOpcoes>
    )
    return <ProvaCard opcoes={menuHeader} instancia={instancia} />
  }
}
