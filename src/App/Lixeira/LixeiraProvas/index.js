import { compose } from 'redux'
import { connect } from 'react-redux'

import { LixeiraProvasComponent } from './LixeiraProvasComponent'
import { mapStateToProps } from './redux'

export const LixeiraProvas = compose(connect(mapStateToProps))(LixeiraProvasComponent)
