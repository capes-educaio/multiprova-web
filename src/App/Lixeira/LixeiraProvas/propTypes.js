import PropTypes from 'prop-types'

export const propTypes = {
  // strings
  strings: PropTypes.object.isRequired,
}
