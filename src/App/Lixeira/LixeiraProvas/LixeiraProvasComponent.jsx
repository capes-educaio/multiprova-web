import React, { Component } from 'react'

import { CardDisplay } from 'common/CardDisplay'

import { ProvaCardLixeira } from '../ProvaCardLixeira'
import { propTypes } from './propTypes'

export class LixeiraProvasComponent extends Component {
  static propTypes = propTypes

  render() {
    const { strings } = this.props
    return (
      <CardDisplay
        nadaCadastrado={strings.lixeiraProvasVazia}
        quantidadeUrl={'provas/lixeira/count'}
        fetchInstanciasUrl={'provas/lixeira'}
        CardComponent={ProvaCardLixeira}
        quantidadePorPaginaInicial={12}
        quantidadePorPaginaOpcoes={[12, 24, 48, 96]}
        numeroDeColunas={{
          extraSmall: 1,
          small: 1,
          medium: 1,
          large: 1,
          extraLarge: 1,
        }}
      />
    )
  }
}
