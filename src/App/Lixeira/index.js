import { compose } from 'redux'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'

import { mapStateToProps } from './redux'
import { LixeiraComponent } from './LixeiraComponent'
import { style } from './style'

export const Lixeira = compose(
  withStyles(style),
  connect(mapStateToProps),
)(LixeiraComponent)
