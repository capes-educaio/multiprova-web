export const style = theme => ({
  select: {
    color: theme.palette.primary.main,
    '& svg': {
      marginBottom: '-6px',
    },
  },
})
