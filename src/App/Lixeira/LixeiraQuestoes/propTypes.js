import PropTypes from 'prop-types'

export const propTypes = {
  // redux actions
  selectListaTags: PropTypes.func.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
}
