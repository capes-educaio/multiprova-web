import { LixeiraQuestoesComponent } from './LixeiraQuestoesComponent'
import { compose } from 'redux'
import { connect } from 'react-redux'

import { mapStateToProps, mapDispatchToProps } from './redux'

export const LixeiraQuestoes = compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(LixeiraQuestoesComponent)
