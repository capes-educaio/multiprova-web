import React, { Component } from 'react'

import { Busca } from 'common/Busca'
import { FormQuestaoFiltroPadrao } from 'common/FormQuestaoFiltroPadrao'
import { FormQuestaoFiltroExpandido } from 'common/FormQuestaoFiltroExpandido'
import { CardDisplay } from 'common/CardDisplay'

import { montarFiltroQuestao } from 'utils/montarFiltroQuestao'
import { setListaTags } from 'utils/compositeActions'

import { QuestaoCardLixeira } from '../QuestaoCardLixeira'
import { propTypes } from './propTypes'

export class LixeiraQuestoesComponent extends Component {
  static propTypes = propTypes

  constructor(props) {
    super(props)
    setListaTags()
  }

  UNSAFE_componentWillMount() {
    this.props.forceUpdate(this.forceUpdate)
  }

  componentWillUnmount = () => this.props.selectListaTags(null)

  render() {
    const { strings } = this.props
    return (
      <div>
        <Busca
          FormPadrao={FormQuestaoFiltroPadrao}
          FormExpandido={FormQuestaoFiltroExpandido}
          montarFiltro={montarFiltroQuestao}
        />
        <CardDisplay
          nadaCadastrado={strings.lixeiraQuestoesVazia}
          quantidadeUrl={'questoes/lixeira/count'}
          fetchInstanciasUrl={'questoes/lixeira/'}
          CardComponent={QuestaoCardLixeira}
          quantidadePorPaginaInicial={12}
          quantidadePorPaginaOpcoes={[12, 24, 48, 96]}
        />
      </div>
    )
  }
}
