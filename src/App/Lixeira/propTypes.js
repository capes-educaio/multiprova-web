import PropTypes from 'prop-types'

export const propTypes = {
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
