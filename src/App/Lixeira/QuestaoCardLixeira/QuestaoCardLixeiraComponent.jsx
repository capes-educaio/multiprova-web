import React, { Component } from 'react'

import MenuItem from '@material-ui/core/MenuItem'
import DeleteForever from '@material-ui/icons/DeleteForever'
import Undo from '@material-ui/icons/Undo'

import { post, del } from 'api'

import { QuestaoCard } from 'common/QuestaoCard'
import { MenuOpcoes } from 'common/MenuOpcoes'

export class QuestaoCardLixeiraComponent extends Component {
  restaurar = () => {
    const { id } = this.props.instancia
    let url = `/questoes/lixeira/restaurar/${id}`
    post(url).then(() => {
      this.atualizarPagina()
    })
  }

  excluirQuestaoDefinitivamente = () => {
    const { id } = this.props.instancia
    let url = `/questoes/lixeira/${id}`
    del(url).then(() => {
      this.atualizarPagina()
    })
  }

  openDialogExcluir = () => {
    const { listaDialogPush, strings } = this.props
    const dialogConfig = {
      titulo: strings.excluirDefinitivamente,
      texto: strings.desejaExcluirQuestaoDefinitivamente,
      actions: [
        {
          texto: strings.ok,
          onClick: this.excluirQuestaoDefinitivamente,
        },
        {
          texto: strings.cancelar,
        },
      ],
    }
    listaDialogPush(dialogConfig)
  }

  atualizarPagina = () => this.props.atualizarCardDisplay()

  render() {
    const { classes, instancia, strings } = this.props
    const menuHeader = (
      <MenuOpcoes>
        <MenuItem onClick={this.restaurar}>
          <Undo className={classes.menuIcon} />
          <span>{strings.restaurar}</span>
        </MenuItem>
        <MenuItem id="item-menu-excluir" onClick={this.openDialogExcluir}>
          <DeleteForever className={classes.menuIcon} />
          <span>{strings.excluirDefinitivamente}</span>
        </MenuItem>
      </MenuOpcoes>
    )
    return <QuestaoCard questao={instancia} opcoes={menuHeader} />
  }
}
