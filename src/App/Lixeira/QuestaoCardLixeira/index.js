import { QuestaoCardLixeiraComponent } from './QuestaoCardLixeiraComponent'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'

import { mapStateToProps, mapDispatchToProps } from './redux'
import { style } from './style'

export const QuestaoCardLixeira = compose(
  withStyles(style),
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(QuestaoCardLixeiraComponent)
