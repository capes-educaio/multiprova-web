import React, { Component } from 'react'

import CircularProgress from '@material-ui/core/CircularProgress'
import { Grid, Paper, Divider, Typography } from '@material-ui/core'

import { TituloDaPagina } from 'common/TituloDaPagina'
import { PaginaPaper } from 'common/PaginaPaper'

import { provaUtils } from 'localModules/provas/ProvaUtils'

import { propTypes } from './propTypes'
import { defaultProps } from './defaultProps'

import { HistogramaProvas, PizzaQuestoes, ProvasAplicadas } from './Graficos'
export class DashboardComponent extends Component {
  static propTypes = propTypes
  static defaultProps = defaultProps

  constructor(props) {
    super(props)
    this.state = {
      carregando: true,
      estatisticas: {},
    }
  }

  componentDidMount = () => {
    var p1 = provaUtils
      .estatisticasAcademicas()
      .then(res => {
        this.setState({ estatisticas: { ...this.state.estatisticas, ...res.data } })
      })
      .catch(err => {
        console.warn(err)
      })

    var p2 = provaUtils
      .estatisticasProvasAplicadas()
      .then(res => {
        this.setState({ estatisticas: { ...this.state.estatisticas, ...res.data } })
      })
      .catch(err => {
        console.warn(err)
      })

    var p3 = provaUtils
      .estatisticasQuestoes()
      .then(res => {
        this.setState({ estatisticas: { ...this.state.estatisticas, ...res.data } })
      })
      .catch(err => {
        console.warn(err)
      })

    Promise.all([p1, p2, p3]).then(() => {
      this.setState({ carregando: false })
    })
  }

  render() {
    const { classes, strings } = this.props
    const { carregando, estatisticas } = this.state

    if (carregando)
      return (
        <PaginaPaper>
          <TituloDaPagina>{strings.dashboard}</TituloDaPagina>
          <Divider />
          <div className={classes.carregando}>
            <CircularProgress />
          </div>
        </PaginaPaper>
      )
    return (
      <PaginaPaper>
        <TituloDaPagina>{strings.dashboard}</TituloDaPagina>
        <Divider />

        <Grid container spacing={8}>
          <Grid item xs={4}>
            <CustomCard classes={classes} titulo={strings.alunos} value={estatisticas.discentes}></CustomCard>
          </Grid>
          <Grid item xs={4}>
            <CustomCard classes={classes} titulo={strings.professores} value={estatisticas.docentes}></CustomCard>
          </Grid>
          <Grid item xs={4}>
            <CustomCard classes={classes} titulo={strings.qteTurmas} value={estatisticas.turmas}></CustomCard>
          </Grid>

          <Grid item xs={4}>
            <CustomCard
              classes={classes}
              titulo={strings.qteQuestoes}
              value={estatisticas.totalDeQuestoes}
            ></CustomCard>
          </Grid>
          <Grid item xs={4}>
            <CustomCard
              classes={classes}
              titulo={strings.qteAvaliacoes}
              value={estatisticas.totalDeProvas}
            ></CustomCard>
          </Grid>
          <Grid item xs={4}>
            <CustomCard
              classes={classes}
              titulo={strings.qteProvasAplicadas}
              value={estatisticas.provas.length}
            ></CustomCard>
          </Grid>

          <Grid item xs={12}>
            <Paper>
              <PizzaQuestoes classes={classes} strings={strings} dados={estatisticas} />
            </Paper>
          </Grid>

          <Grid item xs={6}>
            <Paper>
              <HistogramaProvas classes={classes} strings={strings} dados={estatisticas.provas} />
            </Paper>
          </Grid>

          <Grid item xs={6}>
            <Paper>
              <ProvasAplicadas classes={classes} strings={strings} dados={estatisticas.provas} />
            </Paper>
          </Grid>
        </Grid>
      </PaginaPaper>
    )
  }
}

const CustomCard = ({ classes, titulo, value }) => {
  return (
    <Paper className={classes.paper}>
      <Typography className={classes.titulo} variant="body2">
        {titulo}
      </Typography>
      <Divider />
      <Typography className={classes.numero} variant="h4">
        {value}
      </Typography>
    </Paper>
  )
}
