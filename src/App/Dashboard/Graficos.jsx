import React from 'react'

import ReactEcharts from 'echarts-for-react'

export const HistogramaProvas = ({ dados, classes, strings }) => (
  <ReactEcharts option={provas.option({ dados, strings })} className={classes.chart} notMerge lazyUpdate />
)

export const PizzaQuestoes = ({ dados, classes, strings }) => (
  <ReactEcharts option={questoes.option({ dados, strings })} className={classes.chart} notMerge lazyUpdate />
)

export const ProvasAplicadas = ({ dados, classes, strings }) => (
  <ReactEcharts option={provasAplicadas.option({ dados, strings })} className={classes.chart} notMerge lazyUpdate />
)

const provas = {
  option: ({ dados, strings }) => {
    return {
      title: {
        subtext: strings.provasAplicadasNoUltimos12Meses,
        left: 'center',
      },
      xAxis: {
        type: 'category',
        text: 'label',
        data: provas.dataAxisX(),
      },
      yAxis: {
        type: 'value',
      },
      series: [
        {
          data: provas.dataAxisY(dados),
          type: 'bar',
        },
      ],
    }
  },
  dataAxisX: () => {
    const hoje = new Date()
    const months = []
    const m = Number(hoje.getMonth()) + 12
    let index = 0
    while (index < 12) {
      months.push(MONTHS[(index + m) % 12])
      index++
    }
    return months.reverse()
  },
  dataAxisY: provas => {
    const _dados = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    for (let prova of provas) {
      const dataUltimaAlteracao = new Date(prova.dataUltimaAlteracao)
      _dados[Number(dataUltimaAlteracao.getMonth())]++
    }
    const hoje = new Date()
    const dados = []
    const m = Number(hoje.getMonth()) + 12
    let index = 0
    while (index < 12) {
      dados.push(_dados[(index + m) % 12])
      index++
    }
    return dados.reverse()
  },
}

const questoes = {
  option: ({ dados, strings }) => {
    const data = questoes.data({ dados, strings })
    return {
      title: {
        subtext: strings.quantidadeDeQuestoes,
        x: 'center',
      },
      tooltip: {
        trigger: 'item',
        formatter: '{a} <br/>{b} : {c} ({d}%)',
      },
      legend: {
        type: 'scroll',
        orient: 'vertical',
        right: 10,
        top: 20,
        bottom: 20,
        data: data.legendData,
        selected: data.selected,
      },
      series: [
        {
          name: strings.quantidadeDeQuestoes,
          type: 'pie',
          radius: '55%',
          center: ['40%', '50%'],
          data: data.seriesData,
          itemStyle: {
            emphasis: {
              shadowBlur: 10,
              shadowOffsetX: 0,
              shadowColor: 'rgba(0, 0, 0, 0.5)',
            },
          },
        },
      ],
    }
  },
  data: ({ dados, strings }) => {
    var legendData = [
      strings.vouf,
      strings.associacaoDeColunas,
      strings.multiplaEscolha,
      strings.redacao,
      strings.bloco,
      strings.discursiva,
    ]
    var seriesData = [
      {
        name: strings.vouf,
        value: dados.vouf,
      },
      {
        name: strings.associacaoDeColunas,
        value: dados.associacaoDeColunas,
      },
      {
        name: strings.multiplaEscolha,
        value: dados.multiplaEscolha,
      },
      {
        name: strings.redacao,
        value: dados.redacao,
      },
      {
        name: strings.bloco,
        value: dados.bloco,
      },
      {
        name: strings.discursiva,
        value: dados.dicursiva,
      },
    ]
    var selected = {
      vouf: true,
      associacaoDeColunas: true,
      multiplaEscolha: true,
      redacao: true,
      bloco: true,
      dicursiva: true,
    }

    return {
      legendData: legendData,
      seriesData: seriesData,
      selected: selected,
    }
  },
}

const provasAplicadas = {
  option: ({ dados, strings }) => {
    return {
      title: {
        subtext: strings.provasAplicadas,
        left: 'center',
      },
      xAxis: {
        type: 'category',
        data: ['Jan', 'Fev', 'Mar', 'Abr', 'Maio', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
      },
      yAxis: {
        type: 'value',
      },
      series: [
        {
          data: provasAplicadas.dataAxixY(dados),
          type: 'line',
        },
      ],
    }
  },
  dataAxixY: provas => {
    const dados = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    for (let prova of provas) {
      const dataUltimaAlteracao = new Date(prova.dataUltimaAlteracao)
      dados[Number(dataUltimaAlteracao.getMonth())]++
    }
    return dados
  },
}

const MONTHS = ['Jan', 'Fev', 'Mar', 'Abr', 'Maio', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez']
