export const style = theme => ({
  paper: {
    padding: 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  titulo: {
    padding: '5px 10px',
    color: theme.palette.primary.dark,
  },
  numero: {
    padding: '30px',
    textAlign: 'center',
    fontWeight: '100',
    color: theme.palette.primary.main,
  },
})
