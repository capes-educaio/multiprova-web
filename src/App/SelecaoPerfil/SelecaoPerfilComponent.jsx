import { Grid, Paper, Typography } from '@material-ui/core'
import classnames from 'classnames'
import { PaginaPaper } from 'common/PaginaPaper'
import { TituloDaPagina } from 'common/TituloDaPagina'
import React, { Component } from 'react'
import { mandarParaPaginaInicial } from 'utils/compositeActions/mandarParaPaginaInicial'
import { propTypes } from './propTypes'

export class SelecaoPerfilComponent extends Component {
  static propTypes = propTypes

  handleClickPerfil = perfil => {
    const { usuarioAtual, atualizarUsuarioAtual } = this.props
    const novoUsuario = usuarioAtual.setarPerfilAtual(perfil)
    atualizarUsuarioAtual(novoUsuario)
    mandarParaPaginaInicial(novoUsuario)
  }

  render() {
    const { strings, classes, usuarioAtual } = this.props
    const perfisDoUsuario = usuarioAtual.getPerfisList()
    return (
      <React.Fragment>
        <PaginaPaper>
          <TituloDaPagina>{strings.escolhaOPerfilDesejado}</TituloDaPagina>
        </PaginaPaper>
        <Grid container spacing={8}>
          {perfisDoUsuario.map((perfil, index) => (
            <Grid item xs={12} key={`perfil${index}`}>
              <Paper
                className={classnames(classes.paper, classes.hoverShadow)}
                onClick={() => this.handleClickPerfil(perfil)}
              >
                <Typography className={classes.titulo} variant="body2">
                  {perfil}
                </Typography>
              </Paper>
            </Grid>
          ))}
        </Grid>
      </React.Fragment>
    )
  }
}
