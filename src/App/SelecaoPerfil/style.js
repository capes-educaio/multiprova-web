export const style = theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
    boxShadow: 'none',
  },
  paper: {
    padding: 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
    minHeight: '3em',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hoverShadow: {
    '&:hover': {
      boxShadow: '0px 2px 10px grey',
      cursor: 'pointer',
    },
  },
})
