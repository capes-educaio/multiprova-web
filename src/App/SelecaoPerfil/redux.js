import { bindActionCreators } from 'redux'
import { actions } from 'utils/actions'

export function mapStateToProps(state) {
  return {
    strings: state.strings,
    usuarioAtual: state.usuarioAtual,
  }
}

export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      atualizarUsuarioAtual: actions.select.usuarioAtual,
    },
    dispatch,
  )
}
