import Enzyme, { mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

import { BarraSuperiorComponent } from '../BarraSuperiorComponent'

import React from 'react'

Enzyme.configure({ adapter: new Adapter() })

jest.mock('@material-ui/core/AppBar', () => props => <div>{props.children}</div>)
jest.mock('@material-ui/core/Toolbar', () => props => <div>{props.children}</div>)
jest.mock('@material-ui/core/Typography', () => props => <div>{props.children}</div>)
jest.mock('../SegundoMenu', () => ({
  SegundoMenu: props => <div>{props.children}</div>,
}))
jest.mock('../PrimeiroMenu', () => ({
  PrimeiroMenu: props => <div>{props.children}</div>,
}))

const defaultProps = {
  // style
  classes: {},
  // strings
  strings: {},
  // redux state
  usuarioAtual: {},
  browser: {
    greaterThan: {},
    lessThan: {},
  },
  // redux actions
  toggleMenuEsquerdoAberto: jest.fn(),
}

const mixinPropsSemUser = {
  usuarioAtual: null,
}

const mixinPropsComUser = {
  usuarioAtual: {},
}

const mixinPropsTelaPequena = {
  browser: {
    greaterThan: {
      small: false,
    },
    lessThan: {
      medium: true,
    },
  },
}

const mixinPropsTelaGrande = {
  browser: {
    greaterThan: {
      small: true,
    },
    lessThan: {
      medium: false,
    },
  },
}

describe('BarraSuperiorComponent unit tests', () => {
  test('Monta', () => {
    const component = mount(<BarraSuperiorComponent {...defaultProps} />)
    component.unmount()
  })
  describe('Com usuário logado', () => {
    const comUserProps = { ...defaultProps, ...mixinPropsComUser }
    test('Monta', () => {
      const component = mount(<BarraSuperiorComponent {...comUserProps} />)
      component.unmount()
    })
    test('Em tela pequena, renderiza BotaoMenuEsquerdo, não renderiza MenuSperiorMeio', () => {
      const props = { ...comUserProps, ...mixinPropsTelaPequena }
      const component = mount(<BarraSuperiorComponent {...props} />)
      component.unmount()
    })
    test('Em tela grande, não renderiza BotaoMenuEsquerdo, renderiza MenuSperiorMeio', () => {
      const props = { ...comUserProps, ...mixinPropsTelaGrande }
      const component = mount(<BarraSuperiorComponent {...props} />)
      component.unmount()
    })
  })
  test('Sem usuário logado, não renderiza: BotaoMenuEsquerdo, MenuSuperiorMeio, MenuSuperiorDireito', () => {
    const props = { ...defaultProps, ...mixinPropsSemUser }
    const component = mount(<BarraSuperiorComponent {...props} />)
    component.unmount()
  })
})
