export const style = theme => ({
  hidden: {
    opacity: '0',
  },
  toolbar: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    minHeight: '14px',
  },
  iconeEsquerdo: {
    marginRight: theme.spacing.unit,
  },
  icon: {
    margin: theme.spacing.unit * 2,
    color: theme.palette.primary.main,
  },
  rightIcon: {
    marginLeft: theme.spacing.unit,
  },
  button: {
    margin: theme.spacing.unit,
  },
  appBar: {
    boxShadow: '0px 1px 2px 0px rgba(0,0,0,0.70)',
  },
  appBarSegundoMenuHabilitado: {
    boxShadow: '0px 1px 1px 0px rgba(0,0,0,0.20)',
  },
  barraSuperior: {
    width: '100%',
    padding: '0 !important',
  },
})
