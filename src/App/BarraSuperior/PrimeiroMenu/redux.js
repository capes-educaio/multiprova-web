import { bindActionCreators } from 'redux'
import { actions } from 'utils/actions'

export function mapStateToProps(state) {
  return {
    themeChosen: state.theme,
    strings: state.strings,
    usuarioAtual: state.usuarioAtual,
    browser: state.browser,
    notificacoes: state.notificacoes,
  }
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      changeTheme: actions.select.theme,
      toggleMenuEsquerdoAberto: actions.toggle.menuEsquerdoAberto,
    },
    dispatch,
  )
}
