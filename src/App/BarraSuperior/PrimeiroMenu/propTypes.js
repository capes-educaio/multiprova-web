import PropTypes from 'prop-types'

export const propTypes = {
  width: PropTypes.string.isRequired,
  // redux state
  changeTheme: PropTypes.func.isRequired,
  themeChosen: PropTypes.string.isRequired,
  usuarioAtual: PropTypes.object,
  browser: PropTypes.object.isRequired,
  // redux actions
  toggleMenuEsquerdoAberto: PropTypes.func.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
