import React, { Component } from 'react'

import { Fab, Toolbar, Typography, IconButton, Badge, Tooltip } from '@material-ui/core'
import MenuIcon from '@material-ui/icons/Menu'
import NotificationsIcon from '@material-ui/icons/Notifications'
import OpacityIcon from '@material-ui/icons/Opacity'
import { NoteIcon } from 'localModules/icons/NoteIcon'
import { enumThemes } from 'localModules/theme'
import { BotaoComMenu } from 'common/BotaoComMenu'

import { mandarParaPaginaInicial } from 'utils/compositeActions/mandarParaPaginaInicial'

import { MenuListPerfil } from '../MenuListPerfil'
import { MenuListNotificacao } from '../MenuListNotificacao'

import { propTypes } from './propTypes'
import { defaultProps } from './defaultProps'

export class PrimeiroMenuComponent extends Component {
  static propTypes = propTypes
  static defaultProps = defaultProps

  _irParaPaginaInicial = () => {
    const { usuarioAtual } = this.props
    mandarParaPaginaInicial(usuarioAtual)
  }

  _countNotificacoesNaoLida = () => {
    const { notificacoes } = this.props
    if (!notificacoes) return 0
    const notificacoesNaoLida = notificacoes.filter(not => {
      return not.status === 'nova'
    })
    return notificacoesNaoLida.length
  }
  toogleHighContrast = () => {
    const { changeTheme, themeChosen } = this.props
    const newTheme = themeChosen === enumThemes.light ? enumThemes.highContrast : enumThemes.light
    changeTheme(newTheme)
  }
  render() {
    const { classes, strings, toggleMenuEsquerdoAberto, usuarioAtual, browser, modulosHabilitados, width, themeChosen } = this.props
    const qtnNotificacoesNaoLida = this._countNotificacoesNaoLida()
    const isDocente = usuarioAtual.isDocente()
    const colorIconNotebook = themeChosen === 'light' ? '#FFF' : '#000'

    return (
      <Toolbar className={classes.flex} role="navigation" aria-label={strings.primeiroMenu.titulo}>
        {browser.lessThan.medium && modulosHabilitados.menuNavegacao && (
          <div className={classes.flex}>
            <IconButton color="inherit" onClick={toggleMenuEsquerdoAberto}>
              <MenuIcon />
            </IconButton>
          </div>
        )}
        <div className={classes.flex}>
          <IconButton
            onClick={this._irParaPaginaInicial}
            className={classes.notebookIcon}
            color="inherit"
            aria-label="Home"
          >
            <NoteIcon icon="notebook" width="25" height="25" fill={colorIconNotebook} />
          </IconButton>
          <Typography variant="h6" color="inherit">
            {strings.nomeProjeto}
          </Typography>
        </div>

        <div className={classes.flex}>
          {width !== 'xs' && modulosHabilitados.altoContraste && (
            <Tooltip placement="left" title={strings.altoContraste} aria-label={strings.altoContraste}>
              <IconButton onClick={this.toogleHighContrast} className={classes.colorWhite}>
                <OpacityIcon />
              </IconButton>
            </Tooltip>
          )}
          {isDocente && modulosHabilitados.notificacoes && (
            <BotaoComMenu menuList={<MenuListNotificacao />}>
              <IconButton className={classes.colorWhite}>
                <Badge badgeContent={qtnNotificacoesNaoLida}>
                  <NotificationsIcon />
                </Badge>
              </IconButton>
            </BotaoComMenu>
          )}

          <BotaoComMenu menuList={<MenuListPerfil />}>
            <Fab
              id="abrir-menu-usuario"
              size="small"
              color="secondary"
              aria-label={strings.primeiroMenu.perfil}
              className={classes.letraUsuario}
            >
              {usuarioAtual.data.nome[0]}
            </Fab>
          </BotaoComMenu>
        </div>
      </Toolbar>
    )
  }
}
