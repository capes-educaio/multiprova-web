export const style = theme => ({
  flex: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  colorWhite: {
    color: theme.palette.primary.contrastText,
    marginRight: '10px',
  },
  menuItem: { padding: '20px 10px 20px 10px', margin: '0', color: '#293a4f' },
  letraUsuario: { fontSize: '16px' },
  btnNotificacao: {
    marginRight: '10px',
  },
})
