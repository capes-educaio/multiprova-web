import { mount } from 'enzyme'
import React from 'react'
import configureStore from 'redux-mock-store'
import { MemoryRouter } from 'react-router-dom'
import { Provider } from 'react-redux'
import { ptBr } from 'utils/strings/ptBr'

import { PrimeiroMenuComponent } from '../PrimeiroMenuComponent'

jest.mock('@material-ui/core/styles', () => ({
  withStyles: style => Component => Component,
}))
jest.mock('localModules/strings', () => ({
  withStrings: Component => Component,
  createStringsReducer: (languages, uid) => (state, action) => jest.fn(),
}))
jest.mock('@material-ui/core/Toolbar', () => props => <div>{props.children}</div>)
jest.mock('@material-ui/core/Typography', () => props => <div>{props.children}</div>)
jest.mock('@material-ui/core/IconButton', () => props => <div>{props.children}</div>)
jest.mock('@material-ui/core/Button', () => props => <div>{props.children}</div>)

jest.mock('common/BotaoComMenu', () => ({
  BotaoComMenu: props => <div>{props.children}</div>,
}))
jest.mock('../../MenuListPerfil', () => ({
  MenuListPerfil: props => <div>{props.children}</div>,
}))
jest.mock('utils/compositeActions/mandarParaPaginaInicial', () => ({
  mandarParaPaginaInicial: jest.fn(),
}))

const mockStore = configureStore()
const store = mockStore({
  usuarioAtual: {
    data: {},
    isDocente: () => false,
    isDiscente: () => false,
    isAdmin: () => false,
    isGestor: () => false,
  },
  strings: ptBr,
  classesPai: {
    flex: 'flex',
    segundaToobar: 'segundaToobar',
    menuItem: 'menuItem',
  },
})
let defaultProps = {
  usuarioAtual: {
    data: { nome: 'nome' },
    isDocente: () => false,
    isDiscente: () => false,
    isAdmin: () => false,
    isGestor: () => false,
  },
  browser: { lessThan: {} },
  // redux actions
  toggleMenuEsquerdoAberto: jest.fn(),
  // classes
  classes: {
    flex: 'flex',
    colorWhite: 'colorWhite',
    menuItem: 'menuItem',
    letraUsuario: 'letraUsuario',
  },
  // strings
  strings: {
    nomeProjeto: 'nomeProjeto',
    primeiroMenu: {
      titulo: 'titulo',
    },
  },
}

describe('PrimeiroMenuComponent unit tests', () => {
  test('Monta', () => {
    const component = mount(
      <MemoryRouter>
        <div>
          <Provider store={store}>
            <PrimeiroMenuComponent {...defaultProps} />
          </Provider>
        </div>
      </MemoryRouter>,
    )
    component.unmount() //
  })
})
