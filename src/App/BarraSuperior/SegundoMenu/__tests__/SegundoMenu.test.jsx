import { mount } from 'enzyme'
import { MemoryRouter } from 'react-router-dom'
import { Provider } from 'react-redux'

import configureStore from 'redux-mock-store'

import { ptBr } from 'utils/strings/ptBr'

import React from 'react'

import { SegundoMenuComponent } from '../SegundoMenuComponent'

jest.mock('@material-ui/core/styles', () => ({
  withStyles: style => Component => Component,
}))
jest.mock('localModules/strings', () => ({
  withStrings: Component => Component,
}))
jest.mock('localModules/urlState', () => ({
  setUrlState: jest.fn(),
}))

jest.mock('@material-ui/icons/Home', () => props => <div>{props.children}</div>)
jest.mock('@material-ui/icons/ViewList', () => props => <div>{props.children}</div>)
jest.mock('@material-ui/icons/AssignmentInd', () => props => <div>{props.children}</div>)
jest.mock('@material-ui/core/MenuItem', () => props => <div>{props.children}</div>)
jest.mock('@material-ui/core/Toolbar', () => props => <div>{props.children}</div>)

jest.mock('common/Container', () => ({
  Container: props => <div>{props.children}</div>,
}))

const mockStore = configureStore()

const store = mockStore({
  usuarioAtual: {
    data: {},
    isDocente: () => false,
    isDiscente: () => false,
    isAdmin: () => false,
    isGestor: () => false,
  },
  strings: ptBr,
  classesPai: {
    flex: 'flex',
    segundaToobar: 'segundaToobar',
    menuItem: 'menuItem',
  },
})

let defaultProps = {
  usuarioAtual: {
    data: {},
    isDocente: () => false,
    isDiscente: () => false,
    isAdmin: () => false,
    isGestor: () => false,
  },
  // classes
  classes: {
    flex: 'flex',
    segundaToobar: 'segundaToobar',
    menuItem: 'menuItem',
  },
  // strings
  strings: {},
}

describe('PrimeiroMenu unit tests', () => {
  let props
  test('Monta', () => {
    props = defaultProps
    const component = mount(
      <MemoryRouter>
        <div>
          <Provider store={store}>
            <SegundoMenuComponent {...props} />
          </Provider>
          ,
        </div>
      </MemoryRouter>,
    )
    component.unmount()
  })
  test('Monta docente', () => {
    props = {
      ...defaultProps,
      usuarioAtual: {
        data: {},
        isDocente: () => true,
        isDiscente: () => false,
        isAdmin: () => false,
        isGestor: () => false,
      },
    }
    const component = mount(
      <MemoryRouter>
        <div>
          <Provider store={store}>
            <SegundoMenuComponent {...props} />
          </Provider>
          ,
        </div>
      </MemoryRouter>,
    )
    component.unmount()
  })
  test('Monta discente', () => {
    props = {
      ...defaultProps,
      usuarioAtual: {
        data: {},
        isDocente: () => false,
        isDiscente: () => true,
        isAdmin: () => false,
        isGestor: () => false,
      },
    }
    const component = mount(
      <MemoryRouter>
        <div>
          <Provider store={store}>
            <SegundoMenuComponent {...props} />
          </Provider>
          ,
        </div>
      </MemoryRouter>,
    )
    component.unmount()
  })
  test('Monta admin', () => {
    props = {
      ...defaultProps,
      usuarioAtual: {
        data: {},
        isDocente: () => false,
        isDiscente: () => false,
        isAdmin: () => true,
        isGestor: () => false,
      },
    }
    const component = mount(
      <MemoryRouter>
        <div>
          <Provider store={store}>
            <SegundoMenuComponent {...props} />
          </Provider>
          ,
        </div>
      </MemoryRouter>,
    )
    component.unmount()
  })
})
