export const style = theme => ({
  flex: {
    display: 'flex',
    alignItems: 'center',
    width: '100%',
  },
  segundaToobar: {
    backgroundColor: theme.palette.primary.contrastText,
    paddingLeft: 'calc(100vw - 100%)',
  },
  menuItem: { padding: '20px', margin: '0', color: theme.palette.primary.dark },
})
