import React, { Component } from 'react'

import Toolbar from '@material-ui/core/Toolbar'

import { RotasMenuItens } from 'common/RotasMenuItens'
import { Container } from 'common/Container'

import { propTypes } from './propTypes'

export class SegundoMenuComponent extends Component {
  static propTypes = propTypes

  render() {
    const { classes } = this.props
    return (
      <Toolbar className={classes.segundaToobar} disableGutters>
        <Container tabindex="0">
          <div className={classes.flex}>
            <RotasMenuItens classesPai={classes} />
          </div>
        </Container>
      </Toolbar>
    )
  }
}
