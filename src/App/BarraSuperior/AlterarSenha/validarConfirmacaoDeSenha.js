export const validarConfirmacaoDeSenha = (value, values) => {
  const validacao = {
    sucesso: true,
    erros: [],
  }
  if (values.password !== value) {
    validacao.sucesso = false
    validacao.erros.push({ code: 'senhas_diferentes' })
  }
  return validacao
}
