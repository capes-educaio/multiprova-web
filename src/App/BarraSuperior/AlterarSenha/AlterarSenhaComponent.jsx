import React, { Component } from 'react'

import { Divider } from '@material-ui/core'
import SendIcon from '@material-ui/icons/Send'
import CancelIcon from '@material-ui/icons/Cancel'

import { PaginaPaper } from 'common/PaginaPaper'
import { TituloDaPagina } from 'common/TituloDaPagina'
import { Actions } from 'common/Actions'

import { usuarioProps } from 'localModules/multiprovaEntidades'
import { SimpleForm, CampoPassword } from 'localModules/simpleForm'

import { showMessage } from 'utils/Snackbar'
import { logar } from 'utils/compositeActions'
import { mandarParaPaginaInicial } from 'utils/compositeActions/mandarParaPaginaInicial'

import { post, get } from 'api'

import { propTypes } from './propTypes'

export class AlterarSenhaComponent extends Component {
  static propTypes = propTypes
  constructor(props) {
    super(props)
    this.state = {
      isValid: false,
      valor: {
        oldPassword: '',
        password: '',
        passwordConfirmation: '',
      },
      formRef: null,
    }
  }

  getValorInicialForm = () => {
    const valor = {
      oldPassword: '',
      password: '',
      passwordConfirmation: '',
    }
    this.setState({ valor })
  }

  logarUsuario = () => {
    const { usuarioAtual } = this.props
    const valor = this.state.valor
    let authToken, userData

    post('/usuarios/login', { email: usuarioAtual.data.email, password: valor.password })
      .then(response => {
        this.getValorInicialForm()
        authToken = response.data
        return get('/usuarios/' + authToken.userId, null, authToken.id)
      })
      .then(response => {
        userData = response.data
        logar({ userData, authToken })
      })
  }

  alterarSenha = () => {
    const { strings, usuarioAtual } = this.props
    const UsuarioId = usuarioAtual.data.id
    const valor = this.state.valor

    post(`/usuarios/${UsuarioId}/update-password`, valor)
      .then(() => {
        showMessage.success({ message: strings.senhaRedefinida })
        this.logarUsuario()
      })
      .catch(() => {
        showMessage.error({ message: strings.senhaAtualNaoConfere })
      })
  }

  onFormChange = valor => this.setState({ valor })

  onValid = isValid => {
    if (this.id !== undefined) this.setState({ isValid: true })
    else this.setState({ isValid })
  }

  _mostrarErros = () => {
    const { formRef } = this.state
    if (formRef) formRef.mostrarErros()
  }

  _irParaPaginaInicial = () => {
    const { usuarioAtual } = this.props
    mandarParaPaginaInicial(usuarioAtual)
  }

  render() {
    const { classes, strings } = this.props
    const { isValid, valor } = this.state
    const { onFormChange, onValid, setFormRef, alterarSenha, _mostrarErros, _irParaPaginaInicial } = this
    const actionsConfig = [
      {
        icone: SendIcon,
        texto: strings.salvar,
        buttonProps: {
          onClick: () => {
            if (isValid) alterarSenha()
            else _mostrarErros()
          },
        },
      },
      {
        icone: CancelIcon,
        texto: strings.cancelar,
        buttonProps: {
          onClick: () => _irParaPaginaInicial(),
        },
      },
    ]
    return (
      <PaginaPaper>
        <TituloDaPagina>{strings.alterarSenha}</TituloDaPagina>
        <Divider />
        <div className={classes.secao}>
          <SimpleForm onValid={onValid} onChange={onFormChange} valor={valor} onRef={setFormRef}>
            <CampoPassword
              id="oldPassword"
              accessor="oldPassword"
              label={strings.senhaAtual}
              required
              validar={usuarioProps.password.validar}
              textFieldProps={{
                fullWidth: true,
                name: 'oldPassword',
              }}
            />
            <CampoPassword
              id="password"
              accessor="password"
              label={strings.novaSenha}
              required
              validar={usuarioProps.password.validar}
              textFieldProps={{
                fullWidth: true,
                name: 'password',
              }}
            />
            <CampoPassword
              id="passwordConfirmation"
              accessor="passwordConfirmation"
              label={strings.confirmarNovaSenha}
              required
              textFieldProps={{
                fullWidth: true,
                name: 'passwordConfirmation',
              }}
            />
          </SimpleForm>
        </div>
        <Divider />
        <Actions config={actionsConfig} />
      </PaginaPaper>
    )
  }
}
