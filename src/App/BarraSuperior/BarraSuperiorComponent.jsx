import React, { Component } from 'react'

import AppBar from '@material-ui/core/AppBar'

import { SegundoMenu } from './SegundoMenu'
import { PrimeiroMenu } from './PrimeiroMenu'
import { defaultProps } from './defaultProps'

export class BarraSuperiorComponent extends Component {
  static defaultProps = defaultProps
  render() {
    const { browser, usuarioAtual, classes, modulosHabilitados } = this.props
    const appBarClassName = modulosHabilitados.menuNavegacao ? classes.appBarSegundoMenuHabilitado : classes.appBar
    if (usuarioAtual)
      return (
        <div className={classes.barraSuperior}>
          {browser.greaterThan.small && (
            <AppBar position="fixed" className={appBarClassName}>
              <PrimeiroMenu />
              {modulosHabilitados.menuNavegacao && <SegundoMenu />}
            </AppBar>
          )}
          <div className={browser.greaterThan.small ? classes.hidden : null}>
            <AppBar position="static" className={appBarClassName}>
              <PrimeiroMenu />
              {browser.greaterThan.small && modulosHabilitados.menuNavegacao && <SegundoMenu />}
            </AppBar>
          </div>
        </div>
      )
    return null
  }
}
