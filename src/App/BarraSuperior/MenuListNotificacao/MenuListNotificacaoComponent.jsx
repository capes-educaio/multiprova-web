import React, { Component } from 'react'
import MenuItem from '@material-ui/core/MenuItem'
import MenuList from '@material-ui/core/MenuList'
import Typography from '@material-ui/core/Typography'
import FiberManualRecord from '@material-ui/icons/FiberManualRecord'
import Button from '@material-ui/core/Button'
import { get, del } from 'api'
import { setUrlState } from 'localModules/urlState'
import { bancoDeQuestoes } from 'utils/enumBancoDeQuestoes'
import { dispatchToStore } from 'utils/compositeActions/dispatchToStore'
import { enumNotificacaoRecurso } from 'utils/enumNotificacaoRecurso'
import { propTypes } from './propTypes'

export class MenuListNotificacaoComponent extends Component {
  static propTypes = propTypes

  _openNotificacao = notificacao => () => {
    const { updateNotificacao, updateQuestaoId } = this.props
    const { rota } = notificacao
    const { recurso } = rota
    dispatchToStore('select', 'carregando', true)
    if (recurso === enumNotificacaoRecurso.questaoCedida) {
      get(`notificacoes/${notificacao.id}/marcar-lida`).then(res => {
        setUrlState({ pathname: '/questoes/compartilhadas' })
        updateNotificacao(res.data.notificacao)
        updateQuestaoId(rota.id)
        dispatchToStore('select', 'tipoBancoQuestoes', bancoDeQuestoes.compartilhadas)
      })
    }
  }

  _removeAllNotificacoes = () => {
    const { usuarioAtual, removeAllNotificacoes } = this.props
    const { id } = usuarioAtual.data
    del(`usuarios/${id}/notificacoes`).then(() => {
      removeAllNotificacoes([])
    })
  }

  _sortNotificacoesByStatus = () => {
    const { notificacoes } = this.props
    const notificacoesNaoLida = notificacoes.filter(not => {
      return not.status === 'nova'
    })
    const notificacoesLida = notificacoes.filter(not => {
      return not.status === 'lida'
    })
    return [...notificacoesNaoLida, ...notificacoesLida]
  }

  render() {
    const { classes, strings } = this.props
    const { _openNotificacao, _removeAllNotificacoes } = this

    const notificacoes = this._sortNotificacoesByStatus()

    const notificacoesVazia = (
      <MenuItem className={classes.itemCenter}>
        <Typography variant="subtitle2" className={classes.text}>
          {strings.nenhumaNotificacao}
        </Typography>
      </MenuItem>
    )

    return (
      <MenuList className={classes.list}>
        <div className={classes.areaNotificacao}>
          {notificacoes.map((notificacao, i) => (
            <MenuItem key={`not-${i}`} className={classes.item} onClick={_openNotificacao(notificacao)}>
              <Typography
                variant="subtitle2"
                className={notificacao.status !== 'nova' ? classes.text : classes.textNaoLido}
              >
                <Button className={classes.btnWrapperNotificacaoNaoLida}>
                  <FiberManualRecord
                    className={
                      notificacao.status !== 'nova' ? classes.iconeNotificacaoLida : classes.iconeNotificacaoNaoLida
                    }
                  />
                </Button>
                {notificacao.texto}
              </Typography>
            </MenuItem>
          ))}
        </div>
        {!notificacoes.length ? (
          notificacoesVazia
        ) : (
          <MenuItem className={classes.itemCenter} onClick={_removeAllNotificacoes}>
            <Typography variant="subtitle2" className={classes.text}>
              {strings.excluirNotificacoes}
            </Typography>
          </MenuItem>
        )}
      </MenuList>
    )
  }
}
