export const style = theme => ({
  list: { padding: '0px' },
  item: {
    paddingTop: '0px',
    paddingBottom: '0px',
    maxWidth: '300px',
    height: 'auto',
  },
  itemCenter: {
    paddingTop: '4px',
    paddingBottom: '4px',
    maxWidth: '300px',
    height: 'auto',
    justifyContent: 'center',
    background: theme.palette.fundo.cinza,
  },
  areaNotificacao: {
    maxHeight: '500px',
    overflowY: 'scroll',
  },
  btnWrapperNotificacaoNaoLida: {
    minWidth: '30px',
  },
  iconeNotificacaoNaoLida: {
    fontSize: '0.8em',
    color: theme.palette.steelBlue,
  },
  iconeNotificacaoLida: {
    fontSize: '0.8em',
    color: theme.palette.cinzaDusty,
  },
  linkMarcarComoLida: {
    fontSize: '0.5em',
    marginLeft: '30px',
    marginTop: '-10px',
  },
  link: {
    textDecoration: 'none',
  },
  icon: {
    color: theme.palette.primary.main,
  },
  text: {
    color: theme.palette.cinzaDusty,
    whiteSpace: 'initial',
  },
  textNaoLido: {
    color: theme.palette.primary.main,
    whiteSpace: 'initial',
  },
})
