import { bindActionCreators } from 'redux'
import { actions } from 'utils/actions'

export function mapStateToProps(state) {
  return {
    strings: state.strings,
    usuarioAtual: state.usuarioAtual,
    notificacoes: state.notificacoes,
    questaoId: state.questaoId,
  }
}

export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      updateNotificacao: actions.updateItem.notificacoes,
      removeAllNotificacoes: actions.select.notificacoes,
      updateQuestaoId: actions.select.questaoId,
    },
    dispatch,
  )
}
