import PropTypes from 'prop-types'

export const propTypes = {
  // style
  classes: PropTypes.object.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
  // redux state
  usuarioAtual: PropTypes.object,
  browser: PropTypes.object.isRequired,
  // redux actions
  toggleMenuEsquerdoAberto: PropTypes.func.isRequired,
}
