import PropTypes from 'prop-types'

export const propTypes = {
  width: PropTypes.string.isRequired,
  // redux state
  strings: PropTypes.object.isRequired,
  changeTheme: PropTypes.func.isRequired,
  themeChosen: PropTypes.string.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
