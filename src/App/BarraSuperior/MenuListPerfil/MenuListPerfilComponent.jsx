import React, { Component } from 'react'
import MenuItem from '@material-ui/core/MenuItem'
import MenuList from '@material-ui/core/MenuList'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import Typography from '@material-ui/core/Typography'

import { Group } from '@material-ui/icons'
import { VpnKey } from '@material-ui/icons'
import { ExitToApp } from '@material-ui/icons'
import { Delete } from '@material-ui/icons'
import { Opacity } from '@material-ui/icons'
import { Help } from '@material-ui/icons'
import TagsIcon from 'localModules/icons/TagsIcon'

import { setUrlState } from 'localModules/urlState'
import { enumThemes } from 'localModules/theme'
import { deslogar } from 'utils/compositeActions'

import { propTypes } from './propTypes'
import { defaultProps } from './defaultProps'

export class MenuListPerfilComponent extends Component {
  static propTypes = propTypes
  static defaultProps = defaultProps

  toogleHighContrast = () => {
    const { changeTheme, themeChosen } = this.props
    const newTheme = themeChosen === enumThemes.light ? enumThemes.highContrast : enumThemes.light
    changeTheme(newTheme)
  }

  gotToTutorial = () => {
    const { usuarioAtual } = this.props
    console.log(window.location.host)

    let [host, porta] = window.location.host.split(':')
    if (host === 'localhost' && String(porta) === '3001') {
      porta = ':3030'
    }

    if (usuarioAtual.isDocente()) {
      window.open(`${window.location.protocol}//manual.${host}${porta}/?target=docente`)
    } else if (usuarioAtual.isDiscente()) {
      window.open(`${window.location.protocol}//manual.${host}${porta}/?target=discente`)
    } else if (usuarioAtual.isGestor()) {
      window.open(`${window.location.protocol}//manual.${host}${porta}/?target=gestor`)
    } else if (usuarioAtual.isAdmin()) {
      window.open(`${window.location.protocol}//manual.${window.location.host}/?target=admin`)
    }
  }

  render() {
    const { classes, strings, modulosHabilitados, usuarioAtual, width } = this.props
    const isDocente = usuarioAtual.isDocente()
    const temMaisDeUmPerfil = usuarioAtual.temMaisDeUmPerfil()
    const { gotToTutorial } = this

    return (
      <MenuList className={classes.list}>
        {width === 'xs' && modulosHabilitados.altoContraste && (
          <MenuItem className={classes.item} onClick={this.toogleHighContrast}>
            <ListItemIcon>
              <Opacity className={classes.icon} />
            </ListItemIcon>
            <Typography variant="subtitle1" className={classes.text}>
              {strings.altoContraste}
            </Typography>
          </MenuItem>
        )}
        {temMaisDeUmPerfil && (
          <MenuItem className={classes.item} onClick={() => setUrlState({ pathname: '/mudar-perfil' })}>
            <ListItemIcon>
              <Group className={classes.icon}></Group>
            </ListItemIcon>
            <Typography variant="subtitle1" className={classes.text}>
              {strings.trocarPerfil}
            </Typography>
          </MenuItem>
        )}
        <MenuItem
          id="alterarSenha"
          className={classes.item}
          onClick={() => setUrlState({ pathname: '/alterar-senha' })}
        >
          <ListItemIcon>
            <VpnKey className={classes.icon} />
          </ListItemIcon>
          <Typography variant="subtitle1" className={classes.text}>
            {strings.alterarSenha}
          </Typography>
        </MenuItem>
        {modulosHabilitados.lixeira && isDocente && (
          <MenuItem className={classes.item} onClick={() => setUrlState({ pathname: '/lixeira/questoes' })}>
            <ListItemIcon>
              <Delete className={classes.icon} />
            </ListItemIcon>
            <Typography variant="subtitle1" className={classes.text}>
              {strings.lixeira}
            </Typography>
          </MenuItem>
        )}
        {modulosHabilitados.tags && isDocente && (
          <MenuItem className={classes.item} onClick={() => setUrlState({ pathname: '/tags' })}>
            <ListItemIcon>
              <TagsIcon className={classes.icon}></TagsIcon>
            </ListItemIcon>
            <Typography variant="subtitle1" className={classes.text}>
              {strings.minhasTags}
            </Typography>
          </MenuItem>
        )}
        <MenuItem id="manual" className={classes.item} onClick={gotToTutorial}>
          <ListItemIcon>
            <Help className={classes.icon} />
          </ListItemIcon>
          <Typography variant="subtitle1" className={classes.text}>
            {strings.tutorial}
          </Typography>
        </MenuItem>
        <MenuItem id="deslogar" className={classes.item} onClick={deslogar}>
          <ListItemIcon>
            <ExitToApp className={classes.icon} />
          </ListItemIcon>
          <Typography variant="subtitle1" className={classes.text}>
            {strings.sair}
          </Typography>
        </MenuItem>
      </MenuList>
    )
  }
}
