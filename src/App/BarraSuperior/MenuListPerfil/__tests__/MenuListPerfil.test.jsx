import Enzyme, { mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import { Provider } from 'react-redux'
import configureStore from 'redux-mock-store'
import { ptBr } from 'utils/strings/ptBr'

import React from 'react'

import { MenuListPerfilComponent } from '../MenuListPerfilComponent'

Enzyme.configure({ adapter: new Adapter() })

jest.mock('@material-ui/core/styles', () => ({
  withStyles: style => Component => Component,
  withTheme: style => Component => Component,
}))
jest.mock('@material-ui/core/MenuItem', () => props => <div>{props.children}</div>)
jest.mock('@material-ui/core/MenuList', () => props => <div>{props.children}</div>)
jest.mock('@material-ui/icons/Delete', () => props => <div>{props.children}</div>)
jest.mock('utils/compositeActions', () => ({
  deslogar: jest.fn(),
}))

const mockStore = configureStore()

const store = mockStore({
  usuarioAtual: {
    data: {},
    isDocente: () => false,
    isDiscente: () => false,
    isAdmin: () => false,
    isGestor: () => false,
    temMaisDeUmPerfil: () => false,
  },
  strings: ptBr,
  classesPai: {
    flex: 'flex',
    segundaToobar: 'segundaToobar',
    menuItem: 'menuItem',
  },
})

let defaultProps = {
  usuarioAtual: {
    data: {},
    isDocente: () => false,
    isDiscente: () => false,
    isAdmin: () => false,
    isGestor: () => false,
    temMaisDeUmPerfil: () => false,
  },
  classes: {},
  strings: {
    minhaConta: 'minhaConta',
    lixeira: 'lixeira',
    sair: 'sair',
  },
}

describe('MenuListPerfilComponent unit tests', () => {
  test('Monta', () => {
    const component = mount(
      <Provider store={store}>
        <MenuListPerfilComponent {...defaultProps} />{' '}
      </Provider>,
    )
    component.unmount()
  })
})
