import { appConfig } from 'appConfig'

const { modulosHabilitados } = appConfig

export const defaultProps = {
  modulosHabilitados,
}
