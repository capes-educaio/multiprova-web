export const style = theme => ({
  list: { padding: '8px 0px' },
  item: { paddingTop: '4px', paddingBottom: '4px' },
  icon: { color: theme.palette.primary.main },
  text: { color: theme.palette.primary.main },
})
