import { bindActionCreators } from 'redux'
import { actions } from 'utils/actions'

export function mapStateToProps(state) {
  return {
    strings: state.strings,
    usuarioAtual: state.usuarioAtual,
    themeChosen: state.theme,
  }
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      changeTheme: actions.select.theme,
    },
    dispatch,
  )
}
