import { MenuListPerfilComponent } from './MenuListPerfilComponent'
import { compose } from 'redux'
import { connect } from 'react-redux'
import withWidth from '@material-ui/core/withWidth'
import { withStyles } from '@material-ui/core/styles'

import { mapStateToProps, mapDispatchToProps } from './redux'
import { style } from './style'

export const MenuListPerfil = compose(
  withStyles(style),
  withWidth(),
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(MenuListPerfilComponent)
