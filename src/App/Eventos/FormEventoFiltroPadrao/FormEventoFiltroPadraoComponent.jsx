import React, { Component } from 'react'

import MenuItem from '@material-ui/core/MenuItem'
import Select from '@material-ui/core/Select'

import { propTypes } from './propTypes'

export class FormEventoFiltroPadraoComponent extends Component {
  static propTypes = propTypes

  constructor(props) {
    super(props)
    this.state = {
      valor: '',
    }
  }

  componentDidMount = () => {
    const { onRef } = this.props
    if (onRef) onRef(this)
    this.limparFiltro()
  }

  componentWillUnmount = () => {
    const { onRef } = this.props
    if (onRef) onRef(null)
  }

  limparFiltro = () => {
    const { onFormFiltroChange } = this.props
    onFormFiltroChange({
      tipoEvento: '',
      username: '',
      dataSuperior: '',
      dataInferior: '',
    })
  }

  onChangeForm = event => {
    const valor = event.target.value
    const { onFormFiltroChange } = this.props
    const tipoEvento = valor
    onFormFiltroChange({ tipoEvento })
  }

  render() {
    const { valorDoForm, strings, classes } = this.props
    const { tipoEvento } = valorDoForm

    return (
      <Select
        value={tipoEvento ? tipoEvento : ''}
        onChange={this.onChangeForm}
        fullWidth
        displayEmpty
        className={classes.root}
      >
        <MenuItem value="" disabled selected>
          Pesquisar por tipo de evento
        </MenuItem>
        <MenuItem value="criacao">{strings.criacao}</MenuItem>
        <MenuItem value="atualizacao">{strings.atualizacao}</MenuItem>
        <MenuItem value="exclusao">{strings.exclusao}</MenuItem>
      </Select>
    )
  }
}
