import React from 'react'

import { TituloDaPagina } from 'common/TituloDaPagina'
import { PaginaPaper } from 'common/PaginaPaper'
import { Busca } from 'common/Busca'
import { CardDisplay } from 'common/CardDisplay'

import { FormEventoFiltroPadrao } from './FormEventoFiltroPadrao'
import { FormEventoFiltroExpandido } from './FormEventoFiltroExpandido'
import { montarFiltroEvento } from './montarFiltroEvento'
import { EventoCard } from './EventoCard'
import { propTypes } from './propTypes'

export class EventosComponent extends React.Component {
  static propTypes = propTypes

  render() {
    const { strings } = this.props
    return (
      <div>
        <PaginaPaper>
          <TituloDaPagina>{strings.eventos}</TituloDaPagina>
        </PaginaPaper>
        <Busca
          FormPadrao={FormEventoFiltroPadrao}
          FormExpandido={FormEventoFiltroExpandido}
          montarFiltro={montarFiltroEvento}
        />
        <CardDisplay
          nadaCadastrado={strings.naoEncontrouEvento}
          quantidadeUrl="/eventos/count"
          fetchInstanciasUrl="/eventos"
          CardComponent={EventoCard}
          quantidadePorPaginaInicial={10}
          quantidadePorPaginaOpcoes={[10, 20, 50, 100]}
          numeroDeColunas={{
            extraSmall: 1,
            small: 1,
            medium: 1,
            large: 1,
            extraLarge: 1,
          }}
          order="data DESC"
        />
      </div>
    )
  }
}
