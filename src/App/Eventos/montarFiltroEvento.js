export const montarFiltroEvento = valorDoForm => {
  const { tipoEvento, username } = valorDoForm
  let { dataInferior, dataSuperior } = valorDoForm
  dataInferior = dataInferior ? dataInferior + ':00.000Z' : '2018-01-01T00:00:00.000Z'
  dataSuperior = dataSuperior ? dataSuperior + ':00.000Z' : new Date().toISOString()
  const where = {
    and: [
      tipoEvento ? { tipoEvento } : {},
      username ? { username } : {},
      { data: { gt: dataInferior } },
      { data: { lt: dataSuperior } },
    ],
  }
  return { where }
}
