export const style = theme => ({
  listItem: {
    '& > :first-child': {
      marginBottom: 5,
    },
  },
  card: {
    position: 'relative',
    padding: 20,
    backgroundColor: theme.palette.gelo,
  },
  expand: {
    transform: 'rotate(0deg)',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
    marginLeft: 'auto',
    position: 'absolute',
    bottom: 0,
    right: 0,
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  instancia: {
    padding: '2%',
    '& .string-value': {
      wordBreak: 'break-all',
    },
  },
  infoEvento: {
    display: 'flex',
  },
  labels: {
    display: 'inline-block',
  },
  valores: {
    display: 'inline-block',
    marginLeft: 10,
  },
})
