import React, { Component } from 'react'
import classnames from 'classnames'
import ReactJson from 'react-json-view'

import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'
import Collapse from '@material-ui/core/Collapse'
import IconButton from '@material-ui/core/IconButton'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'

import { propTypes } from './propTypes'

export class EventoCardComponent extends Component {
  static propTypes = propTypes
  state = {
    expandido: false,
  }

  organizaData = dataString => {
    if (!dataString) {
      return 'DD-MM-AAAA HH:MM:SS'
    } else {
      let hora = dataString.substring(11, 19)
      let dataSoComDiaMesAno = dataString.substring(0, 10)
      let partesDaData = dataSoComDiaMesAno.split('-')
      return `${partesDaData[2]}-${partesDaData[1]}-${partesDaData[0]} ${hora}`
    }
  }

  get nomeDoEvento() {
    const { strings, instancia } = this.props
    const modelo = this.tipoModelo
    switch (instancia.tipoEvento) {
      case 'criacao':
        return `${strings.criacao} ${strings.de} ${modelo}`
      case 'atualizacao':
        return `${strings.atualizacao} ${strings.de} ${modelo}`
      case 'exclusao':
        return `${strings.exclusao} ${strings.de} ${modelo}`
      default:
        return strings.tipoEventoNCadastrado
    }
  }

  get tipoModelo() {
    const { strings, instancia } = this.props
    switch (instancia.tipoModelo) {
      case 'questao':
        return strings.questaoMin
      case 'usuario':
        return strings.usuarioMin
      case 'prova':
        return strings.provaMin
      default:
        return strings.tipoModeloNCadastrado
    }
  }

  get infoEvento() {
    const { strings, instancia, classes } = this.props
    const labels = ['username', 'conexao', 'dataHora']
    const valores = [instancia.username, instancia.conexao, this.organizaData(instancia.data)]
    return (
      <div className={classes.infoEvento}>
        <div className={classes.labels}>
          {labels.map((label, index) => (
            <Typography key={index}>{strings[label]}:</Typography>
          ))}
        </div>
        <div className={classes.valores}>
          {valores.map((valor, index) => (
            <Typography key={index}>{valor}</Typography>
          ))}
        </div>
      </div>
    )
  }

  toggleExpandido = () => {
    this.setState({ expandido: !this.state.expandido })
  }

  render() {
    const { classes, instancia } = this.props
    const { expandido } = this.state
    return (
      <Paper className={classes.card} key={instancia.id}>
        <div className={classes.listItem}>
          <Typography variant="subtitle1">{this.nomeDoEvento}</Typography>
          {this.infoEvento}
        </div>
        <IconButton
          size="small"
          className={classnames(classes.expand, {
            [classes.expandOpen]: expandido,
          })}
          onClick={this.toggleExpandido}
          aria-expanded={expandido}
        >
          <ExpandMoreIcon />
        </IconButton>
        <Collapse in={expandido} timeout="auto" unmountOnExit>
          <div className={classes.instancia}>
            <ReactJson
              src={instancia.instancia}
              name="instancia"
              collapseStringsAfterLength={64}
              displayObjectSize={false}
              displayDataTypes={false}
              collapsed={1}
            />
          </div>
        </Collapse>
      </Paper>
    )
  }
}
