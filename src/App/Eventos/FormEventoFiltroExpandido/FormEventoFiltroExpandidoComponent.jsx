import React from 'react'

import TextField from '@material-ui/core/TextField'
import MenuItem from '@material-ui/core/MenuItem'
import Select from '@material-ui/core/Select'
import FormControl from '@material-ui/core/FormControl'
import InputLabel from '@material-ui/core/InputLabel'

import { propTypes } from './propTypes'

export class FormEventoFiltroExpandidoComponent extends React.Component {
  static propTypes = propTypes

  handleChange = campo => event => {
    event.preventDefault()
    const { onFormFiltroChange, valorDoForm } = this.props
    const valor = event.target.value
    valorDoForm[campo] = valor
    onFormFiltroChange(valorDoForm)
  }

  render() {
    const { strings, valorDoForm, handleKeyPress, classes, children } = this.props
    const { tipoEvento, username, dataInferior, dataSuperior } = valorDoForm

    return (
      <div>
        <TextField
          value={username}
          label={strings.username}
          onChange={this.handleChange('username')}
          onKeyPress={handleKeyPress}
          fullWidth
        />
        <TextField
          className={classes.dataInferior}
          label={strings.dataInferiorPesquisa}
          type="datetime-local"
          onChange={this.handleChange('dataInferior')}
          defaultValue={dataInferior}
          InputLabelProps={{
            shrink: true,
          }}
        />
        <TextField
          label={strings.dataSuperiorPesquisa}
          type="datetime-local"
          onChange={this.handleChange('dataSuperior')}
          defaultValue={dataSuperior}
          InputLabelProps={{
            shrink: true,
          }}
        />
        <FormControl className={classes.formControl} fullWidth>
          <InputLabel htmlFor="tipo-evento">{strings.tipoEvento}</InputLabel>
          <Select
            value={tipoEvento ? tipoEvento : ''}
            onChange={this.handleChange('tipoEvento')}
            inputProps={{
              id: 'tipo-evento',
            }}
          >
            <MenuItem value="">
              <em>Nenhum</em>
            </MenuItem>
            <MenuItem value="criacao">{strings.criacao}</MenuItem>
            <MenuItem value="atualizacao">{strings.atualizacao}</MenuItem>
            <MenuItem value="exclusao">{strings.exclusao}</MenuItem>
          </Select>
        </FormControl>
        {children}
      </div>
    )
  }
}
