import PropTypes from 'prop-types'

export const propTypes = {
  valorDoForm: PropTypes.object,
  onFormFiltroChange: PropTypes.func.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
