export const style = theme => ({
  app: {
    [theme.breakpoints.down('sm')]: {
      marginLeft: '0',
    },
  },
})
