import { mount } from 'enzyme'

import React from 'react'

import { MemoryRouter } from 'react-router-dom'
import { Provider } from 'react-redux'

import configureStore from 'redux-mock-store'

import { ptBr } from 'utils/strings/ptBr'

import { MenuEsquerdoComponent } from '../MenuEsquerdoComponent'

jest.mock('@material-ui/core/SwipeableDrawer', () => props => <div>{props.children}</div>)
jest.mock('@material-ui/icons/Home', () => props => <div>{props.children}</div>)
jest.mock('@material-ui/icons/ViewList', () => props => <div>{props.children}</div>)
jest.mock('@material-ui/icons/AssignmentInd', () => props => <div>{props.children}</div>)
jest.mock('@material-ui/core/MenuItem', () => props => <div>{props.children}</div>)
jest.mock('@material-ui/core/Toolbar', () => props => <div>{props.children}</div>)

const mockStore = configureStore()

const store = mockStore({
  usuarioAtual: {
    data: {},
    isDocente: () => false,
    isDiscente: () => false,
    isAdmin: () => false,
    isGestor: () => false,
  },
  strings: ptBr,
  classesPai: {
    flex: 'flex',
    segundaToobar: 'segundaToobar',
    menuItem: 'menuItem',
  },
})

const defaultProps = {
  // redux state
  usuarioAtual: {
    isDocente: jest.fn(),
    isAdmin: jest.fn(),
  },
  strings: {},
  menuEsquerdoAberto: false,
  // redux actions
  selectMenuEsquerdoAberto: jest.fn(),
  // style
  classes: {},
}

const mixinPropsMenuEsquerdoAbertoTrue = {
  menuEsquerdoAberto: true,
}

const mixinPropsMenuEsquerdoAbertoFalse = {
  menuEsquerdoAberto: false,
}

describe('MenuEsquerdoComponent unit tests', () => {
  test('Monta', () => {
    const component = mount(
      <MemoryRouter>
        <div>
          <Provider store={store}>
            <MenuEsquerdoComponent {...defaultProps} />
          </Provider>
          ,
        </div>
      </MemoryRouter>,
    )
    component.unmount()
  })
  test('Com menuEsquerdoAberto true, SwipeableDrawer props.open é true', () => {
    const props = { ...defaultProps, ...mixinPropsMenuEsquerdoAbertoTrue }
    const component = mount(
      <MemoryRouter>
        <div>
          <Provider store={store}>
            <MenuEsquerdoComponent {...props} />
          </Provider>
          ,
        </div>
      </MemoryRouter>,
    )
    component.unmount()
  })
  test('Com menuEsquerdoAberto false, SwipeableDrawer pros.open é false', () => {
    const props = { ...defaultProps, ...mixinPropsMenuEsquerdoAbertoFalse }
    const component = mount(
      <MemoryRouter>
        <div>
          <Provider store={store}>
            <MenuEsquerdoComponent {...props} />
          </Provider>
          ,
        </div>
      </MemoryRouter>,
    )
    component.unmount()
  })
  test('Métodos de fechar e abrir menu implementados', () => {
    const componentPai = mount(
      <MemoryRouter>
        <div>
          <Provider store={store}>
            <MenuEsquerdoComponent {...defaultProps} />
          </Provider>
          ,
        </div>
      </MemoryRouter>,
    )
    const component = componentPai.find('MenuEsquerdoComponent')
    component.instance().fecharMenuEsquerdo()
    component.instance().abrirMenuEsquerdo()
    expect(defaultProps.selectMenuEsquerdoAberto).toBeCalledTimes(2)
    componentPai.unmount()
  })
})
