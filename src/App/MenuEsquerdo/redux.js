import { bindActionCreators } from 'redux'
import { actions } from 'utils/actions'

export function mapStateToProps(state) {
  return {
    menuEsquerdoAberto: state.menuEsquerdoAberto,
    usuarioAtual: state.usuarioAtual,
    strings: state.strings,
  }
}

export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      selectMenuEsquerdoAberto: actions.select.menuEsquerdoAberto,
    },
    dispatch,
  )
}
