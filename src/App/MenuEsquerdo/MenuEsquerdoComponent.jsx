import React, { Component } from 'react'

// import SwipeableDrawer from '@material-ui/core/SwipeableDrawer'
import { SwipeableDrawer } from '@material-ui/core'

import { RotasMenuItens } from 'common/RotasMenuItens'

import { propTypes } from './propTypes'

export class MenuEsquerdoComponent extends Component {
  static propTypes = propTypes

  fecharMenuEsquerdo = () => this.props.selectMenuEsquerdoAberto(false)

  abrirMenuEsquerdo = () => this.props.selectMenuEsquerdoAberto(true)

  render() {
    const { classes, menuEsquerdoAberto, usuarioAtual } = this.props
    if (usuarioAtual)
      return (
        <SwipeableDrawer open={menuEsquerdoAberto} onClose={this.fecharMenuEsquerdo} onOpen={this.abrirMenuEsquerdo}>
          <div tabIndex={0} role="button" onClick={this.fecharMenuEsquerdo} onKeyDown={this.fecharMenuEsquerdo}>
            <RotasMenuItens classesPai={classes} />
          </div>
        </SwipeableDrawer>
      )
    return null
  }
}
