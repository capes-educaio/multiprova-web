import PropTypes from 'prop-types'

export const propTypes = {
  // redux state
  menuEsquerdoAberto: PropTypes.bool,
  usuarioAtual: PropTypes.object,
  // redux actions
  selectMenuEsquerdoAberto: PropTypes.func,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
