import React, { Component } from 'react'
import Button from '@material-ui/core/Button'
import MenuItem from '@material-ui/core/MenuItem'
import Select from '@material-ui/core/Select'
import InputLabel from '@material-ui/core/InputLabel'
import FormControl from '@material-ui/core/FormControl'

import { TituloDaPagina } from 'common/TituloDaPagina'
import { PaginaPaper } from 'common/PaginaPaper'
import { CardDisplay } from 'common/CardDisplay'
import { Busca } from 'common/Busca'

import { setUrlState } from 'localModules/urlState'

import { dispatchToStore } from 'utils/compositeActions/dispatchToStore'
import { enumFiltroPorTempo } from 'utils/enumFiltroPorTempo'

import { CardGerenciamentoTurmas } from './CardGerenciamentoTurmas'
import { propTypes } from './propTypes'

import { FormFiltroPadrao } from './FormFiltroPadrao'
import { FormFiltroExpandido } from './FormFiltroExpandido'

export class GerenciamentoTurmasComponent extends Component {
  static propTypes = propTypes
  cardDisplay
  buscaRef

  _goToAdd = () =>
    setUrlState({
      pathname: '/turma',
    })

  getFiltro = ano => {
    const timestampAtual = new Date()
    const anoAtual = timestampAtual.getFullYear()

    if (ano === anoAtual) return enumFiltroPorTempo.anoAtual
    else return enumFiltroPorTempo.anoAnterior
  }

  _updateBancoDeTurmasStore = () => {
    const { inputsDoFiltro } = this.props.location.state.hidden
    if (inputsDoFiltro && inputsDoFiltro.ano) {
      dispatchToStore('select', 'tipoBancoTurmas', this.getFiltro(inputsDoFiltro.ano))
    } else dispatchToStore('select', 'tipoBancoTurmas', enumFiltroPorTempo.todas)
  }

  componentDidMount = () => {
    this._updateBancoDeTurmasStore()
  }

  get rota() {
    const idUsuario = this.props.usuarioAtual.data.id
    return {
      back: {
        get: `/usuarios/${idUsuario}/turmas`,
        count: `usuarios/${idUsuario}/turmas/count`,
      },
    }
  }

  handleChange = event => {
    event.preventDefault()
    const rotas = this.rota
    if (rotas) {
      this.buscaRef.filtroRef.handleChangeFiltroTempo(event.target.value)
      this.buscaRef.onClickPesquisar()
    }
    dispatchToStore('select', 'tipoBancoTurmas', event.target.value)
  }

  montarFiltro = valorDoForm => {
    const { nome, ano, periodo } = valorDoForm
    let where = {
      nome: nome !== '' ? { regexp: `.*${nome}.*` } : undefined,
      ano: Number(ano) !== 0 ? Number(ano) : undefined,
      periodo: Number(periodo) !== 0 ? Number(periodo) : undefined,
    }
    const include = {}
    return {
      include,
      where,
    }
  }

  selectBancoDeTurmas = () => {
    const { strings, classes } = this.props

    return (
      <FormControl>
        <InputLabel>{strings.filtroPorTempo}</InputLabel>
        <Select
          className={classes.widthSelect}
          inputProps={{
            name: 'tipoVisualizacao',
          }}
          value={this.props.tipoBancoTurmas}
          onChange={this.handleChange}
        >
          <MenuItem value={enumFiltroPorTempo.todas}>{strings.todas}</MenuItem>
          <MenuItem value={enumFiltroPorTempo.anoAtual}>{strings.anoAtual}</MenuItem>
          <MenuItem value={enumFiltroPorTempo.anoAnterior}>{strings.anoAnterior}</MenuItem>
        </Select>
      </FormControl>
    )
  }

  render() {
    const { strings } = this.props
    const { montarFiltro } = this

    return (
      <div>
        <PaginaPaper>
          <TituloDaPagina>
            {this.selectBancoDeTurmas()}
            <Button onClick={this._goToAdd} variant="contained" mini color="primary" aria-label="add">
              {strings.criar}
            </Button>
          </TituloDaPagina>
        </PaginaPaper>
        <Busca
          FormPadrao={FormFiltroPadrao}
          FormExpandido={FormFiltroExpandido}
          montarFiltro={montarFiltro}
          onRef={buscaRef => {
            this.buscaRef = buscaRef
          }}
        />
        <CardDisplay
          CardComponent={CardGerenciamentoTurmas}
          numeroDeColunas={{
            extraSmall: 1,
            small: 1,
            medium: 1,
            large: 1,
            extraLarge: 1,
          }}
          quantidadeUrl={this.rota.back.count}
          fetchInstanciasUrl={this.rota.back.get}
          nadaCadastrado={strings.naoEncontrouTurma}
          order={['ano DESC', 'periodo DESC', 'nome ASC']}
        />
      </div>
    )
  }
}
