import React, { Component } from 'react'

import MenuItem from '@material-ui/core/MenuItem'
import DeleteIcon from '@material-ui/icons/Delete'
import EditIcon from '@material-ui/icons/Edit'

import { updateUrlState } from 'localModules/urlState'

import { TurmaCard } from 'common/TurmaCard'
import { Opcoes } from 'common/Opcoes'

import { openDialogExcluir } from 'utils/compositeActions/openDialogExcluir'

import { propTypes } from './propTypes'

export class CardGerenciamentoTurmasComponent extends Component {
  static propTypes = propTypes
  _opcoesRef

  _rejectExcluir = e => {
    console.error('Erro não tratado')
    console.error(e)
  }

  _openDialogExcluir = event => {
    this._opcoesRef && this._opcoesRef.close(event)

    const { atualizarCardDisplay, strings, instancia } = this.props
    const titulo = strings.excluir
    const texto = strings.desejaExcluirTurma
    const url = `/turmas/${instancia.id}`
    openDialogExcluir({ texto, titulo, url, resolve: () => atualizarCardDisplay(), reject: this._rejectExcluir })
  }

  _goToEdit = event => {
    this._opcoesRef && this._opcoesRef.close(event)
    const { instancia } = this.props
    const pathname = `/turma/${instancia.id}`
    updateUrlState({ pathname })
  }

  render() {
    const { instancia, strings } = this.props
    const opcoes = (
      <Opcoes onRef={ref => (this._opcoesRef = ref)}>
        <MenuItem id="item-menu-editar" onClick={this._goToEdit}>
          <EditIcon color="primary" />
          {strings.editar}
        </MenuItem>
        <MenuItem id="item-menu-excluir" onClick={this._openDialogExcluir}>
          <DeleteIcon color="primary" />
          {strings.excluir}
        </MenuItem>
      </Opcoes>
    )
    return <TurmaCard onClick={this._goToEdit} opcoes={opcoes} instancia={instancia} />
  }
}
