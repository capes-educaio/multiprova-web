export const style = theme => ({
  containerSeletores: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  blocoSeletor: {
    flexBasis: 200,
    flexGrow: 1,
    width: '100%',
    margin: 5,
  },
})
