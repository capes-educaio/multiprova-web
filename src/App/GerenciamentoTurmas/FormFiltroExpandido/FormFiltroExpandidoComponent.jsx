import React from 'react'

import TextField from '@material-ui/core/TextField'

import { propTypes } from './propTypes'

export class FormFiltroExpandidoComponent extends React.Component {
  static propTypes = propTypes

  constructor(props) {
    super(props)
    this.state = {
      ano: '',
      periodo: '',
    }
  }

  handleChange = campo => event => {
    event.preventDefault()
    this.alterarValorDoForm(campo, event.target.value)
  }

  alterarValorDoForm = (campo, valor) => {
    const { onFormFiltroChange, valorDoForm } = this.props
    const value = valor.value
    valorDoForm[campo] = value ? value : valor
    onFormFiltroChange(valorDoForm)
    this.setState({ [campo]: valor })
  }

  render() {
    const { strings, valorDoForm, handleKeyPress, classes } = this.props
    let { ano, periodo } = { ...this.state, ...valorDoForm }

    return (
      <div>
        <div className={classes.containerSeletores}>
          <div className={classes.blocoSeletor}>
            <TextField
              type="number"
              label={strings.ano}
              value={ano}
              onKeyPress={handleKeyPress}
              onChange={this.handleChange('ano')}
              fullWidth
            />
          </div>
          <div className={classes.blocoSeletor}>
            <TextField
              type="number"
              label={strings.periodo}
              value={periodo}
              onKeyPress={handleKeyPress}
              onChange={this.handleChange('periodo')}
              fullWidth
            />
          </div>
        </div>
        <div className={classes.botoes}>{this.props.children}</div>
      </div>
    )
  }
}
