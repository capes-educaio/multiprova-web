import React from 'react'

import TextField from '@material-ui/core/TextField'

import { enumFiltroPorTempo } from 'utils/enumFiltroPorTempo'

import { propTypes } from './propTypes'

export class FormFiltroPadraoComponent extends React.Component {
  static propTypes = propTypes

  constructor(props) {
    super(props)
    this.state = {
      nome: '',
    }
  }

  componentDidMount = () => {
    const { onRef } = this.props
    if (onRef) onRef(this)
    this.limparFiltro()
  }

  componentWillUnmount = () => {
    const { onRef } = this.props
    if (onRef) onRef(null)
  }

  handleChange = campo => event => {
    event.preventDefault()
    this.alterarValorDoForm(campo, event.target.value)
  }

  getAnoAtual = () => {
    const timestampAtual = new Date()
    const ano = timestampAtual.getFullYear()
    this.alterarValorDoForm('ano', ano)
  }

  getAnoAnterior = () => {
    const timestampAtual = new Date()
    const ano = timestampAtual.getFullYear()
    this.alterarValorDoForm('ano', ano - 1)
  }

  getTodasAsTurmas = () => {
    this.alterarValorDoForm('ano', '')
  }

  handleChangeFiltroTempo = event => {
    switch (event) {
      case enumFiltroPorTempo.todas:
        this.getTodasAsTurmas()
        break
      case enumFiltroPorTempo.anoAtual:
        this.getAnoAtual()
        break
      case enumFiltroPorTempo.anoAnterior:
        this.getAnoAnterior()
        break
      default:
        this.getTodasAsTurmas()
        break
    }
  }

  alterarValorDoForm = (campo, valor) => {
    const { onFormFiltroChange, valorDoForm } = this.props
    valorDoForm[campo] = valor
    onFormFiltroChange(valorDoForm)
    this.setState({ [campo]: valor })
  }

  limparFiltro = () => {
    const { onFormFiltroChange } = this.props
    const filtroLimpo = {
      nome: '',
      ano: '',
      periodo: '',
    }
    onFormFiltroChange(filtroLimpo)
  }

  render() {
    const { handleKeyPress, strings, valorDoForm } = this.props
    let { nome } = { ...this.state, ...valorDoForm }

    return (
      <TextField
        placeholder={strings.nome}
        value={nome}
        onChange={this.handleChange('nome')}
        onKeyPress={handleKeyPress}
        fullWidth
      />
    )
  }
}
