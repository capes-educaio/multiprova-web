import PropTypes from 'prop-types'

export const propTypes = {
  valorDoForm: PropTypes.object,
  onFormFiltroChange: PropTypes.func.isRequired,
  onkeyPressAction: PropTypes.func,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
