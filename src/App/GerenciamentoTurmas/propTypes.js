import PropTypes from 'prop-types'

export const propTypes = {
  // redux state
  usuarioAtual: PropTypes.object.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
