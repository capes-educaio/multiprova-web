import React, { Component } from 'react'

import Paper from '@material-ui/core/Paper'
import Button from '@material-ui/core/Button'
import FormHelperText from '@material-ui/core/FormHelperText'

import { post } from 'api'
import { showMessage } from 'utils/Snackbar'
import { ResgatarSenhaForm } from './ResgatarSenhaForm'
import { propTypes } from './propTypes'

export class ResgatarSenhaComponent extends Component {
  static propTypes = propTypes

  constructor(props) {
    super(props)
    this.state = {
      value: {
        password: '',
        passwordConfirmation: '',
      },
      isValid: false,
      erroLoginText: '',
      mostrarErroNoForm: false,
    }
  }

  _handleSubmit = () => {
    const {
      strings,
      history,
      match: {
        params: { hashSenha },
      },
    } = this.props
    const { isValid, value } = this.state
    const { password, passwordConfirmation } = value
    if (password !== passwordConfirmation) {
      this.setState({ mostrarErroNoForm: true, erroLoginText: strings.senhasDiferentes })
    } else if (isValid) {
      post('/usuarios/update-forgotten-password', { password, passwordConfirmation, hashSenha })
        .then(() => {
          this.setState({ mostrarErroNoForm: false, erroLoginText: '' })
          showMessage.success({ message: strings.senhaRedefinida })
          history.push('/login')
        })
        .catch(({ response = {} }) => {
          const { data = {} } = response
          const { error = {} } = data
          const { message = '' } = error
          this.setState({ mostrarErroNoForm: true, erroLoginText: message })
        })
    } else this._mostrarErros()
  }

  _onValid = isValid => this.setState({ isValid })

  _onChange = value => this.setState({ value })

  _mostrarErros = () => {
    const { formRefs } = this.props
    if (formRefs.formLogin && formRefs.formLogin.showError) formRefs.formLogin.showError()
    else console.warn('Algum problema ao buscar a ref do form de login.')
  }

  render() {
    const { classes, strings } = this.props
    const { isValid, mostrarErroNoForm, erroLoginText } = this.state
    return (
      <div className={classes.background}>
        <div className={classes.loginContainerOut}>
          <div>
            <div className={classes.formBox} dir="x">
              <div className={classes.divMenor}>{strings.nomeProjeto}</div>
              <Paper className={classes.paperPadding} elevation={2}>
                <div className={classes.helperText}>
                  <FormHelperText error={Boolean(mostrarErroNoForm)} tabIndex="1">
                    {mostrarErroNoForm ? erroLoginText : strings.legendaFormEsqueciSenha}
                  </FormHelperText>
                </div>
                <ResgatarSenhaForm
                  valor={this.state.value}
                  onValid={this._onValid}
                  onChange={this._onChange}
                  handleLogin={this._handleSubmit}
                />
                <div className={classes.buttonContainer}>
                  <Button
                    className={classes.button}
                    color="primary"
                    onClick={isValid ? this._handleSubmit : this._mostrarErros}
                  >
                    {strings.resgatar}
                  </Button>
                </div>
              </Paper>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
