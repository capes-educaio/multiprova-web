import React, { Component } from 'react'

import { usuarioProps } from 'localModules/multiprovaEntidades'

import { SimpleReduxForm, CampoPassword } from 'localModules/ReduxForm'

import { propTypes } from './propTypes'

export class ResgatarSenhaFormComponent extends Component {
  static propTypes = propTypes

  render() {
    const { strings, valor, onChange, onValid, handleLogin } = this.props
    return (
      <SimpleReduxForm id="formResgatarSenha" valor={valor} onChange={onChange} onValid={onValid}>
        <CampoPassword
          id="formResgatar__password"
          accessor="password"
          required
          label={strings.senha}
          validar={usuarioProps.password.validar}
          onEnter={handleLogin}
          textFieldProps={{
            autoFocus: true,
            fullWidth: true,
          }}
        />
        <CampoPassword
          id="formResgatar__passwordConfirmation"
          accessor="passwordConfirmation"
          required
          label={strings.confirmarSenha}
          validar={usuarioProps.password.validar}
          onEnter={handleLogin}
          textFieldProps={{
            fullWidth: true,
          }}
        />
      </SimpleReduxForm>
    )
  }
}
