import PropTypes from 'prop-types'

export const propTypes = {
  valor: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired,
  onValid: PropTypes.func.isRequired,
  handleLogin: PropTypes.func.isRequired,
  // redux state
  refs: PropTypes.object.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
