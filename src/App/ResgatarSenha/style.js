export const style = theme => ({
  background: {
    backgroundColor: '#000000',
    backgroundImage: 'url(imagemLogin.jpg)',
    backgroundSize: 'cover',
    minHeight: '100vh',
    display: 'flex',
    overflow: 'auto',
    justifyContent: 'center',
    alignItems: 'center',
    fontFamily: 'Roboto, sans-serif',
  },
  loginContainerOut: {
    backgroundColor: 'rgba(0, 0, 0, 0.8)',
    minWidth: '100%',
    minHeight: '100vh',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    fontFamily: 'Roboto, sans-serif',
  },
  formBox: {
    [theme.breakpoints.down('sm')]: {
      margin: '0px',
    },
    [theme.breakpoints.up('sm')]: {
      margin: '0px 2px 4px 2px',
    },
    display: 'flex',
    justifyContent: 'center',
  },
  paperPadding: {
    padding: '80px 20px 5px 20px',
    borderRadius: '5px',
    width: 280,
  },
  tituloPaper: {
    margin: '0px 2px 4px 2px',
    textAlign: 'center',
  },
  divMenor: {
    width: 290,
    height: 110,
    background: 'linear-gradient(180deg, #3e66b8, #2282ab);',
    position: 'absolute',
    marginTop: -40,
    borderRadius: 5,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    fontWeight: 'bold',
    fontSize: 26,
    color: 'white',
    fontFamily: 'Montserrat, sans-serif',
  },
  buttonContainer: {
    padding: '0',
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
  },
  button: {
    margin: '10px 0',
  },
  helperText: { justifyContent: 'center', display: 'flex', margin: '0 0 5px 0' },
})
