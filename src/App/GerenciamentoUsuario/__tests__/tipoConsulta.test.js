import { isNumber, isEmail, isName } from '../montarFiltroUsuario'

describe('Tipo consulta', () => {
  test('email', () => {
    expect(isEmail('talison@gmail.com')).toBe(true)
    expect(isEmail('talison@g.c')).toBe(false)
    expect(isEmail('@g.c')).toBe(false)
    expect(isEmail('francisco')).toBe(false)
  })

  test('matricula', () => {
    expect(isNumber('talison')).toBe(false)
    expect(isNumber('125361426')).toBe(true)
  })

  test('nome', () => {
    expect(isName('talison')).toBe(true)
    expect(isName('talison@gmail.com')).toBe(false)
    expect(isName('3123123')).toBe(false)
  })
})
