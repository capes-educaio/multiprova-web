import React, { Component } from 'react'
import MenuItem from '@material-ui/core/MenuItem'
import MenuList from '@material-ui/core/MenuList'

import { propTypes } from './propTypes'
import { setUrlState } from 'localModules/urlState'

export class MenuListAddUsuarioComponent extends Component {
  static propTypes = propTypes

  render() {
    const { strings } = this.props
    return (
      <MenuList>
        <MenuItem id="usuario-novo" onClick={() => setUrlState({ pathname: '/usuario/novo' })}>
          {strings.usuario}
        </MenuItem>
        <MenuItem id="usuario-novo" onClick={() => setUrlState({ pathname: '/discentes' })}>
          {strings.discentesEmLote}
        </MenuItem>
      </MenuList>
    )
  }
}
