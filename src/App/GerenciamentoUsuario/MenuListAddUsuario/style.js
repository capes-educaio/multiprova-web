export const style = theme => ({
  icon: { margin: 0 },
  itemText: { color: theme.palette.defaultLink },
})
