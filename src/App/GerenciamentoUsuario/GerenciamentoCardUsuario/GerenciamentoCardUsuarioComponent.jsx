import React, { Component } from 'react'

import MenuItem from '@material-ui/core/MenuItem'
import DeleteIcon from '@material-ui/icons/Delete'
import EditIcon from '@material-ui/icons/Edit'

import { setUrlState } from 'localModules/urlState'

import { GerenciamentoCard } from 'common/GerenciamentoCard'
import { UsuarioCard } from '../UsuarioCard'
import { Opcoes } from 'common/Opcoes'

import { propTypes } from './propTypes'

export class GerenciamentoCardUsuarioComponent extends Component {
  static propTypes = propTypes

  render() {
    const { instancia, atualizarCardDisplay, strings } = this.props
    const pathname = `/usuario/${instancia.id}`
    return (
      <GerenciamentoCard
        opcoes={
          <Opcoes>
            <MenuItem onClick={() => setUrlState({ pathname })}>
              <EditIcon color="primary" />
              {strings.editar}
            </MenuItem>
            <MenuItem id="item-menu-excluir">
              <DeleteIcon color="primary" />
              {strings.excluir}
            </MenuItem>
          </Opcoes>
        }
        instancia={instancia}
        atualizarCardDisplay={atualizarCardDisplay}
        rotaDaInstanciaFront="/usuario"
        rotaDaInstanciaBack="/usuarios"
        stringDesejaExcluirInstancia={strings.desejaExcluirUsuario}
        CardComponent={UsuarioCard}
      />
    )
  }
}
