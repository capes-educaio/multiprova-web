export const style = theme => ({
  cardTop: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  cardHeader: {
    padding: '10px 0 10px 20px',
  },
  cardSubHeader: {
    padding: '5px',
  },
  cardTopEsquerda: {
    display: 'flex',
    alignItems: 'center',
  },
  cardContent: {
    wordWrap: 'break-word',
    padding: '0px 20px 10px',
  },
  titulo: {
    display: 'flex',
    alignItems: 'center',
  },
  name: { color: theme.palette.primary.main, fontWeight: 500 },
  matricula: { marginLeft: 4 },
})
