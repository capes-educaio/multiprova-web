import React, { Component } from 'react'

import Card from '@material-ui/core/Card'
import Typography from '@material-ui/core/Typography'

import { propTypes } from './propTypes'

export class UsuarioCardComponent extends Component {
  static propTypes = propTypes

  getPerfil(permissao) {
    switch (permissao) {
      case 1:
        return 'ADMIN'
      case 2:
        return 'DOCENTE'
      case 3:
        return 'DISCENTE'
      case 4:
        return 'SUPER'
      case 5:
        return 'GESTOR'
      default:
        return undefined
    }
  }

  render() {
    const { classes, instancia, opcoes } = this.props

    return (
      <div key={instancia.id}>
        <Card>
          <div className={classes.cardTop}>
            <div className={classes.cardTopEsquerda}>
              {instancia.permissoes &&
                instancia.permissoes.map((p, index) => {
                  return (
                    <Typography
                      key={index}
                      className={index === 0 ? classes.cardHeader : classes.cardSubHeader}
                      variant="caption"
                    >
                      {this.getPerfil(p)}
                    </Typography>
                  )
                })}
              <Typography className={classes.cardHeader} variant="caption">
                ({instancia.email})
              </Typography>
            </div>
            <div>{opcoes}</div>
          </div>
          <div className={classes.cardContent}>
            <div className={classes.titulo}>
              <Typography className={classes.name}>{instancia.nome}</Typography>
              {!!instancia.matricula && (
                <Typography className={classes.matricula}>
                  {'- '}
                  {instancia.matricula}
                </Typography>
              )}
            </div>
          </div>
        </Card>
      </div>
    )
  }
}
