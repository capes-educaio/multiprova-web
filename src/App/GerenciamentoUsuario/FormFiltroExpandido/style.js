export const style = theme => ({
  formControl: {
    minWidth: 120,
  },
  botoes: {
    marginTop: '5px',
    marginBottom: '5px',
  },
})
