import React from 'react'

import { Select, MenuItem, InputLabel, FormControl } from '@material-ui/core'

import { propTypes } from './propTypes'

import { enumTipoUsuario } from '../enumTipoUsuario'

export class FormFiltroExpandidoComponent extends React.Component {
  static propTypes = propTypes

  constructor(props) {
    super(props)
    this.state = {
      perfil: 0,
    }
  }

  handleChange = campo => event => {
    event.preventDefault()
    this.alterarValorDoForm(campo, event.target.value)
  }

  alterarValorDoForm = (campo, valor) => {
    const { onFormFiltroChange, valorDoForm } = this.props
    const value = valor.value
    valorDoForm[campo] = value ? value : valor
    onFormFiltroChange(valorDoForm)
    this.setState({ [campo]: valor })
  }

  render() {
    const { strings, classes, valorDoForm } = this.props
    let { perfil } = { ...this.state, ...valorDoForm }

    return (
      <div>
        <FormControl fullWidth>
          <InputLabel htmlFor="perfil">{strings.perfil}</InputLabel>
          <Select
            inputProps={{
              name: 'perfil',
              placeholder: strings.perfil,
              id: 'perfil',
            }}
            value={perfil}
            onChange={this.handleChange('perfil')}
          >
            <MenuItem value={0}>{strings.perfilVazio}</MenuItem>
            <MenuItem value={enumTipoUsuario.discente}>{strings.discente}</MenuItem>
            <MenuItem value={enumTipoUsuario.docente}>{strings.docente}</MenuItem>
            <MenuItem value={enumTipoUsuario.admin}>{strings.admin}</MenuItem>
            <MenuItem value={enumTipoUsuario.super}>{strings.super}</MenuItem>
            <MenuItem value={enumTipoUsuario.gestor}>{strings.gestor}</MenuItem>
          </Select>
        </FormControl>
        <div className={classes.botoes}>{this.props.children}</div>
      </div>
    )
  }
}
