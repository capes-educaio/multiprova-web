export const montarFiltroUsuario = valorDoForm => {
  const { value, perfil } = valorDoForm

  return {
    whereParams: {
      value,
      roleId: perfil,
    },
  }
}

export const isName = valor => {
  if (!isNumber(valor) && !isEmail(valor)) return true
  return false
}

export const isNumber = valor => {
  return /^\d+$/.test(valor)
}

export const isEmail = valor => {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  return re.test(valor.toLowerCase())
}
