export const enumTipoUsuario = {
  admin: 1,
  docente: 2,
  discente: 3,
  super: 4,
  gestor: 5,
}
