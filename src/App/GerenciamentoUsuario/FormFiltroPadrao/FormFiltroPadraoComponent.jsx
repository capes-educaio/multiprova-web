import React, { Component } from 'react'

import TextField from '@material-ui/core/TextField'

import { propTypes } from './propTypes'

export class FormFiltroPadraoComponent extends Component {
  static propTypes = propTypes

  constructor(props) {
    super(props)
    this.state = {
      value: '',
    }
  }

  componentDidMount = () => {
    const { onRef } = this.props
    if (onRef) onRef(this)
    this.limparFiltro()
  }

  componentWillUnmount = () => {
    const { onRef } = this.props
    if (onRef) onRef(null)
  }

  handleChange = campo => event => {
    event.preventDefault()
    this.alterarValorDoForm(campo, event.target.value)
  }

  alterarValorDoForm = (campo, valor) => {
    const { onFormFiltroChange, valorDoForm } = this.props
    valorDoForm[campo] = valor
    onFormFiltroChange(valorDoForm)
    this.setState({ [campo]: valor })
  }

  limparFiltro = () => {
    const { onFormFiltroChange } = this.props
    onFormFiltroChange({
      value: '',
      perfil: 0,
    })
  }

  render() {
    const { valorDoForm, strings, handleKeyPress, classes } = this.props
    let { value } = { ...this.state, ...valorDoForm }

    return (
      <TextField
        id="value"
        placeholder={strings.valueFilterUsuario}
        value={value ? value : ''}
        className={classes.input}
        onChange={this.handleChange('value')}
        onKeyPress={handleKeyPress}
        margin="normal"
        fullWidth
      />
    )
  }
}
