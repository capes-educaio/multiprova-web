import React, { Component } from 'react'

import { TituloDaPagina } from 'common/TituloDaPagina'
import { PaginaPaper } from 'common/PaginaPaper'
import { Busca } from 'common/Busca'
import { CardDisplay } from 'common/CardDisplay'
import Button from '@material-ui/core/Button'

import { FormFiltroPadrao } from './FormFiltroPadrao'
import { FormFiltroExpandido } from './FormFiltroExpandido'
import { montarFiltroUsuario } from './montarFiltroUsuario'
import { GerenciamentoCardUsuario } from './GerenciamentoCardUsuario'
import { propTypes } from './propTypes'

import { BotaoComMenu } from 'common/BotaoComMenu'
import { MenuListAddUsuario } from './MenuListAddUsuario'

export class GerenciamentoUsuarioComponent extends Component {
  static propTypes = propTypes

  render() {
    const { strings } = this.props
    return (
      <div>
        <PaginaPaper>
          <TituloDaPagina>
            {strings.usuarios}
            <BotaoComMenu menuList={<MenuListAddUsuario />}>
              <Button id="usuario-questao" variant="contained" color="primary" aria-label="add">
                {strings.criar}
              </Button>
            </BotaoComMenu>
          </TituloDaPagina>
        </PaginaPaper>
        <Busca FormPadrao={FormFiltroPadrao} FormExpandido={FormFiltroExpandido} montarFiltro={montarFiltroUsuario} />
        <CardDisplay
          CardComponent={GerenciamentoCardUsuario}
          quantidadeUrl="/usuarios/usuarios-by-roles/count"
          fetchInstanciasUrl="/usuarios/usuarios-by-roles"
          nadaCadastrado={strings.naoEncontrouUsuario}
          quantidadePorPaginaInicial={10}
          quantidadePorPaginaOpcoes={[10, 20, 50, 100]}
          order={['nome ASC', 'email ASC', 'matricula ASC']}
          numeroDeColunas={{
            extraSmall: 1,
            small: 1,
            medium: 1,
            large: 1,
            extraLarge: 1,
          }}
        />
      </div>
    )
  }
}
