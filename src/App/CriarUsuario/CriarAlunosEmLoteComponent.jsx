import React, { Component } from 'react'

import { PaginaPaper } from 'common/PaginaPaper'
import { TituloDaPagina } from 'common/TituloDaPagina'
import Divider from '@material-ui/core/Divider'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'

import IconButton from '@material-ui/core/IconButton'
import Typography from '@material-ui/core/Typography'
import CircularProgress from '@material-ui/core/CircularProgress'

import Paper from '@material-ui/core/Paper'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'

import Send from '@material-ui/icons/Send'
import Delete from '@material-ui/icons/Delete'
import Cancel from '@material-ui/icons/Cancel'
import { setUrlState } from 'localModules/urlState'

import { usuarioProps } from 'localModules/multiprovaEntidades'

import { post } from 'api'

import { isName, isNumber, isEmail } from '../GerenciamentoUsuario/montarFiltroUsuario'

export class CriarAlunosEmLoteComponent extends Component {
  constructor(props) {
    super(props)
    this.state = {
      discentes: '',
      discentesSalvoComSucesso: [],
      discentesComErro: [],
      carregando: false,
      isValid: true,
      messageErro: '',
    }
  }

  salvarDiscente = async discente => {
    discente.matricula = discente.matricula ? Number(discente.matricula) : undefined
    discente.redirectUrl = this.props.urlParaEnviarEmail
    if (discente.erros) delete discente.erros
    return await post('/usuarios/aluno', discente)
  }

  usernameExist = username => {
    return post('/usuarios/username-existe', { value: username })
  }

  emailExist = email => {
    return post('/usuarios/email-existe', { value: email })
  }

  validarDiscente = async discente => {
    const { strings } = this.props
    const erros = {}
    const r1 = await this.usernameExist(discente.nome)
    const r2 = await this.emailExist(discente.email)
    if (discente.nome && r1.data.existe) erros.nome = { isValid: false, message: strings.usernameJaExiste }
    if (discente.email && r2.data.existe) erros.email = { isValid: false, message: strings.emailJaExiste }
    if (!usuarioProps.email.validar(discente.email).sucesso)
      erros.email = { isValid: false, message: strings.emailInvalido }

    return erros
  }

  isListaValida = lista => {
    const { strings } = this.props

    if (lista[0] === '') {
      this.setState({ isValid: false, messageErro: strings.listaVazia })
      return false
    }

    for (let item of lista) {
      const dados = item.split(';')
      if (dados.length > 3) {
        this.setState({ isValid: false, messageErro: strings.discentesListaInvalida })
        return false
      }
    }

    this.setState({ isValid: true })
    return true
  }

  salvarDiscentes = () => {
    const { discentesSalvoComSucesso, discentesComErro, discentes } = this.state
    const lista = discentes.split('\n')

    this.setState({ carregando: true })
    if (this.isListaValida(lista)) {
      for (let dados of lista) {
        const isEmptyLine = dados.length === 0
        if (isEmptyLine) continue
        const discente = this.extrairDadosDoDiscente(dados)
        this.salvarDiscente(discente)
          .then(res => {
            discentesSalvoComSucesso.push(res.data)
            this.setState({ discentesSalvoComSucesso, discentes: '', carregando: false })
          })
          .catch(async () => {
            const erros = await this.validarDiscente(discente)
            discente.erros = { ...erros }
            discentesComErro.push(discente)
            this.setState({ discentesComErro, discentes: '', carregando: false })
          })
      }
    } else {
      this.setState({ carregando: false })
    }
  }

  salvarDiscenteFromTableErro = index => () => {
    const { discentesComErro, discentesSalvoComSucesso } = this.state
    const discente = discentesComErro[index]
    this.salvarDiscente(discente)
      .then(res => {
        discentesComErro.splice(index, 1)
        discentesSalvoComSucesso.push(res.data)
        this.setState({ discentesComErro, discentesSalvoComSucesso })
      })
      .catch(async () => {
        const erros = await this.validarDiscente(discente)
        discente.erros = { ...erros }
        discentesComErro[index].erros = erros
        this.setState({ discentesComErro })
      })
  }

  extrairDadosDoDiscente = dadosBruto => {
    const dados = dadosBruto.split(';')
    let discente = {}
    if (dados !== undefined) {
      for (let valor of dados) {
        const atributo = this.obterAtributo(valor)
        if (atributo !== undefined) {
          discente[atributo] = valor
        }
      }
    }

    return discente
  }

  obterAtributo = valor => {
    if (isNumber(valor)) return 'matricula'
    if (isEmail(valor)) return 'email'
    if (isName(valor)) return 'nome'
    return undefined
  }

  handleChange = field => event => {
    this.setState({ [field]: event.target.value })
  }

  handleChangeOnTable = (field, index) => event => {
    const { discentesComErro } = this.state
    discentesComErro[index][field] = event.target.value
    this.setState({ discentesComErro })
  }

  excluirDaListaComErros = index => () => {
    const { discentesComErro } = this.state
    discentesComErro.splice(index, 1)
    this.setState({ discentesComErro })
  }

  render() {
    const { classes, strings } = this.props
    const {
      handleChange,
      handleChangeOnTable,
      salvarDiscentes,
      salvarDiscenteFromTableErro,
      excluirDaListaComErros,
    } = this

    const { discentes, discentesSalvoComSucesso, discentesComErro, carregando, isValid, messageErro } = this.state

    return (
      <PaginaPaper>
        <TituloDaPagina>{strings.cadastrarDiscentes}</TituloDaPagina>
        <Divider />
        <div className={classes.secao}>
          <TextField
            id="discentes"
            label={strings.discentes}
            placeholder={strings.discentesLista}
            multiline
            rows="4"
            value={discentes}
            fullWidth
            onChange={handleChange('discentes')}
            margin="normal"
            variant="outlined"
            error={!isValid}
            helperText={!isValid ? messageErro : ''}
          />
          <div>
            <Button onClick={salvarDiscentes} variant="text">
              <Send className={classes.leftIcon} /> {strings.cadastrar}
            </Button>
            <Button
              onClick={() => {
                setUrlState({ pathname: '/usuarios' })
              }}
              className={classes.botaoSecundario}
              variant="text"
            >
              <Cancel className={classes.leftIcon} />
              {strings.sair}
            </Button>
          </div>

          {discentesSalvoComSucesso.length > 0 && (
            <div>
              <Typography variant="subtitle2" className={classes.secao}>
                {strings.listaDeDiscentesSalvoComSucesso}
              </Typography>
              <Paper className={classes.tabelaSucesso}>
                <Table>
                  <TableHead>
                    <TableRow>
                      <TableCell>{strings.matricula}</TableCell>
                      <TableCell>{strings.nome}</TableCell>
                      <TableCell>{strings.email}</TableCell>
                    </TableRow>
                  </TableHead>

                  <TableBody>
                    {discentesSalvoComSucesso.map((discente, index) => (
                      <TableRow key={index}>
                        <TableCell>{discente.matricula !== undefined ? discente.matricula : '-'}</TableCell>
                        <TableCell>{discente.nome}</TableCell>
                        <TableCell>{discente.email}</TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </Paper>
            </div>
          )}

          {discentesComErro.length > 0 && (
            <div>
              <Typography variant="subtitle2" className={classes.secao}>
                {strings.listaDeDiscentesComErros}
              </Typography>
              <Paper className={classes.tabelaDeErros}>
                <Table>
                  <TableHead>
                    <TableRow>
                      <TableCell>{strings.matricula}</TableCell>
                      <TableCell>{strings.nome}</TableCell>
                      <TableCell>{strings.email}</TableCell>
                      <TableCell></TableCell>
                    </TableRow>
                  </TableHead>

                  <TableBody>
                    {discentesComErro.map((discente, index) => (
                      <TableRow key={index}>
                        <TableCell>
                          <TextField
                            onChange={handleChangeOnTable('matricula', index)}
                            label={strings.matricula}
                            value={!discente.matricula ? '' : discente.matricula}
                            fullWidth
                            margin="dense"
                          />
                        </TableCell>
                        <TableCell>
                          <TextField
                            onChange={handleChangeOnTable('nome', index)}
                            value={!discente.nome ? '' : discente.nome}
                            error={discente.nome && discente.erros.nome && !discente.erros.nome.isValid}
                            label={
                              discente.nome && discente.erros.nome && !discente.erros.nome.isValid
                                ? discente.erros.nome.message
                                : strings.nome
                            }
                            fullWidth
                            margin="dense"
                          />
                        </TableCell>
                        <TableCell>
                          <TextField
                            onChange={handleChangeOnTable('email', index)}
                            value={!discente.email ? '' : discente.email}
                            error={discente.email && discente.erros.email && !discente.erros.email.isValid}
                            label={
                              discente.email && discente.erros.email && !discente.erros.email.isValid
                                ? discente.erros.email.message
                                : strings.email
                            }
                            fullWidth
                            margin="dense"
                          />
                        </TableCell>
                        <TableCell align="right">
                          <div>
                            <IconButton fontSize="small" onClick={excluirDaListaComErros(index)}>
                              <Delete />
                            </IconButton>
                            <IconButton fontSize="small" onClick={salvarDiscenteFromTableErro(index)}>
                              <Send />
                            </IconButton>
                          </div>
                        </TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </Paper>
            </div>
          )}
        </div>
        {carregando && (
          <div className={classes.loading}>
            <CircularProgress />
          </div>
        )}
      </PaginaPaper>
    )
  }
}
