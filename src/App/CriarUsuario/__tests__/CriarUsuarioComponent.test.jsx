import React from 'react'
import { CriarUsuarioComponent } from '../CriarUsuarioComponent'
import { mount } from 'enzyme'
import { post } from 'api'

jest.mock('@material-ui/icons/Help', () => props => <div>{props.children}</div>)
jest.mock('@material-ui/icons/PersonAdd', () => props => <div>{props.children}</div>)
jest.mock('@material-ui/core/Divider', () => props => <div>{props.children}</div>)
jest.mock('@material-ui/core/DialogTitle', () => props => <div>{props.children}</div>)
jest.mock('@material-ui/core/DialogContentText', () => props => <div>{props.children}</div>)
jest.mock('@material-ui/core/DialogActions', () => props => <div>{props.children}</div>)
jest.mock('@material-ui/core/DialogContent', () => props => <div>{props.children}</div>)
jest.mock('@material-ui/core/Dialog', () => props => <div>{props.children}</div>)
jest.mock('@material-ui/core/Button', () => props => <div>{props.children}</div>)

jest.mock('common/TituloDaPagina', () => ({
  TituloDaPagina: props => <div>{props.children}</div>,
}))
jest.mock('../UsuarioForm', () => ({
  UsuarioForm: props => <div>{props.children}</div>,
}))
jest.mock('common/PaginaPaper', () => ({
  PaginaPaper: props => <div>{props.children}</div>,
}))
jest.mock('common/Actions', () => ({
  Actions: props => <div>{props.children}</div>,
}))

let defaultProps = {
  // redux
  usuarioAtual: {},
  // style
  classes: {},
  // strings
  strings: {},
  formLimpo: {
    nome: '',
    instituicao: '',
    username: '',
    email: '',
    passwords: {
      password: '',
      confirmPassword: '',
    },
    permissoes: {
      admin: false,
      docente: false,
      discente: false,
      gestor: false,
    },
  },
  history: {
    location: {
      pathname: '/usuario/novo',
    },
  },
}

jest.mock('api', () => ({
  post: jest.fn().mockResolvedValue({}),
  get: jest.fn().mockResolvedValue({}),
  put: jest.fn().mockResolvedValue({}),
}))

describe('checando modificação do JSON de dados do usuário feitas no método salvarUsuario ', () => {
  it('Usuário Docente', () => {
    const exemploValuesProps = {
      nome: 'Docente Teste',
      instituicao: 'Instituição',
      username: 'testeDocente',
      email: 'docente@teste.com',
      passwords: {
        password: '12345678',
        confirmPassword: '12345678',
      },
      permissoes: {
        docente: true,
        discente: false,
      },
    }

    const valuesPropsPost = {
      nome: 'Docente Teste',
      instituicao: 'Instituição',
      username: 'testeDocente',
      email: 'docente@teste.com',
      password: '12345678',
      permissoes: [2],
    }
    const wrapper = mount(<CriarUsuarioComponent {...defaultProps} />)
    const instance = wrapper.instance()
    instance.setState({ valor: exemploValuesProps })
    instance._salvarUsuario()
    expect(post).toHaveBeenCalledWith('/usuarios/', valuesPropsPost)
  })

  it('Usuário Discente', () => {
    const exemploValuesProps = {
      nome: 'Discente Teste',
      instituicao: 'Instituição',
      username: 'testeDiscente',
      email: 'discente@teste.com',
      passwords: {
        password: '12345678',
        confirmPassword: '12345678',
      },
      permissoes: {
        docente: false,
        discente: true,
      },
    }

    const valuesPropsPost = {
      nome: 'Discente Teste',
      instituicao: 'Instituição',
      username: 'testeDiscente',
      email: 'discente@teste.com',
      password: '12345678',
      permissoes: [3],
    }
    const wrapper = mount(<CriarUsuarioComponent {...defaultProps} />)
    const instance = wrapper.instance()
    instance.setState({ valor: exemploValuesProps })
    instance._salvarUsuario()
    expect(post).toHaveBeenCalledWith('/usuarios/', valuesPropsPost)
  })

  it('Usuário Docente e Discente', () => {
    const exemploValuesProps = {
      nome: 'Docente e Discente Teste',
      instituicao: 'Instituição',
      username: 'testeDocenteEDiscente',
      email: 'docente_discente@teste.com',
      passwords: {
        password: '12345678',
        confirmPassword: '12345678',
      },
      permissoes: {
        docente: true,
        discente: true,
      },
    }

    const valuesPropsPost = {
      nome: 'Docente e Discente Teste',
      instituicao: 'Instituição',
      username: 'testeDocenteEDiscente',
      email: 'docente_discente@teste.com',
      password: '12345678',
      permissoes: [2, 3],
    }
    const wrapper = mount(<CriarUsuarioComponent {...defaultProps} />)
    const instance = wrapper.instance()
    instance.setState({ valor: exemploValuesProps })
    instance._salvarUsuario()
    expect(post).toHaveBeenCalledWith('/usuarios/', valuesPropsPost)
  })
})

describe('checando mudança de estado em salvarUsuarioSucesso ', () => {
  it('Deve abrir o dialog e setar states', () => {
    const response = {
      data: {
        email: 'email@email.com',
      },
    }
    const wrapper = mount(<CriarUsuarioComponent {...defaultProps} />)
    const instance = wrapper.instance()
    instance._salvarUsuarioSucesso(response)
    expect(instance.state.valor.email).toBe('')
    expect(instance.state.valor).toEqual(defaultProps.formLimpo)
  })
})

describe('checando mudança de estado em salvarUsuarioFalha ', () => {
  it('Deve abrir o dialog e setar states', () => {
    const wrapper = mount(<CriarUsuarioComponent {...defaultProps} />)
    const instance = wrapper.instance()
    instance._salvarUsuarioFalha()
    expect(instance.state.valor).toEqual(defaultProps.formLimpo)
  })
})

describe('checando onValid', () => {
  it('Deve mudar o estado do isValid para true', () => {
    const wrapper = mount(<CriarUsuarioComponent {...defaultProps} />)
    const instance = wrapper.instance()
    instance.onValid(true)
    expect(instance.state.isValid).toBeTruthy()
  })
  it('Deve mudar o estado do isValid para false', () => {
    const wrapper = mount(<CriarUsuarioComponent {...defaultProps} />)
    const instance = wrapper.instance()
    instance.onValid(false)
    expect(instance.state.isValid).toBeFalsy()
  })
})

describe('Checando onFormChange', () => {
  const exemploForm = {
    nome: 'Teste',
    instituicao: 'TESTE',
    username: 'testandoFORM',
    email: 'teste@form.com',
    passwords: {
      password: '12345678',
      confirmPassword: '12345678',
    },
    permissoes: {
      docente: true,
      discente: true,
    },
  }

  it('Deve mudar os campos do formulário', () => {
    const wrapper = mount(<CriarUsuarioComponent {...defaultProps} />)
    const instance = wrapper.instance()
    instance.onFormChange(exemploForm)
    expect(instance.state.valor).toBe(exemploForm)
  })
})
