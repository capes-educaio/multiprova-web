export const validarConfirmacaoDeSenha = valor => value => {
  const validacao = {
    sucesso: true,
    erros: [],
  }
  if (valor.passwords.password !== value) {
    validacao.sucesso = false
    validacao.erros.push({ code: 'senhas_diferentes' })
  }
  return validacao
}
