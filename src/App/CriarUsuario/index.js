import { CriarUsuarioComponent } from './CriarUsuarioComponent'
import { CriarAlunosEmLoteComponent } from './CriarAlunosEmLoteComponent'
import { compose } from 'redux'
import { connect } from 'react-redux'
// import { withRouter } from 'react-router'
import { withStyles } from '@material-ui/core/styles'

import { mapStateToProps, mapDispatchToProps } from './redux'
import { style } from './style'

export const CriarUsuario = compose(
  withStyles(style),
  // withRouter,
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(CriarUsuarioComponent)

export const CriarAlunosEmLote = compose(
  withStyles(style),
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(CriarAlunosEmLoteComponent)
