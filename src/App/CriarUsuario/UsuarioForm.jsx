import React from 'react'

import { post } from 'api'

import { usuarioProps } from 'localModules/multiprovaEntidades'

import { SimpleForm, CampoText, CampoObjeto, CampoPassword, CampoBool } from 'localModules/simpleForm'

import { validarConfirmacaoDeSenha } from './validarConfirmacaoDeSenha'
import { validarPermissoes } from './validarPermissoes'

export const UsuarioForm = ({ onValid, onFormChange, valor, setFormRef, strings }) => (
  <SimpleForm onValid={onValid} onChange={onFormChange} initialValue={valor} onRef={setFormRef}>
    <CampoText
      accessor="email"
      required
      validar={async value => {
        let { sucesso, erros } = usuarioProps.email.validar(value)
        if (erros.length < 1) {
          const { data } = await post('/usuarios/email-existe', { value })
          if (data.existe) {
            sucesso = false
            erros.push({ code: 'ja_existe' })
          }
        }
        return { sucesso, erros }
      }}
      mensagensDeErro={{ email_invalido: strings.erroEmail, ja_existe: strings.emailJaExiste }}
      label={strings.email}
      fullWidth
      async
    />
    <CampoText
      accessor="username"
      validar={async value => {
        let { sucesso, erros } = { sucesso: true, erros: [] }
        if (erros.length < 1) {
          const { data } = await post('/usuarios/username-existe', { value })
          if (data.existe) {
            sucesso = false
            erros.push({ code: 'ja_existe' })
          }
        }
        return { sucesso, erros }
      }}
      mensagensDeErro={{ ja_existe: strings.usernameJaExiste }}
      label={strings.usuario}
      fullWidth
      async
    />
    <CampoText accessor="nome" required label={strings.nome} fullWidth />
    <CampoText accessor="instituicao" label={strings.instituicao} fullWidth />
    <CampoObjeto accessor="passwords">
      <CampoPassword
        accessor="password"
        required
        validar={usuarioProps.password.validar}
        label={strings.senha}
        fullWidth
      />
      <CampoPassword
        accessor="confirmPassword"
        required
        validar={validarConfirmacaoDeSenha(valor)}
        mensagensDeErro={{ senhas_diferentes: strings.senhasDiferentes }}
        label={strings.confirmarSenha}
        fullWidth
      />
    </CampoObjeto>
    <CampoObjeto
      accessor="permissoes"
      label={strings.perfil}
      required
      helperText={strings.permissoesHelperText}
      validar={validarPermissoes}
      mensagensDeErro={{
        pelos_menos_uma_permissao: strings.permissoesErro,
      }}
    >
      <div>
        <CampoBool accessor="docente" label={strings.professor} />
      </div>
      <div>
        <CampoBool accessor="discente" label={strings.aluno} />
      </div>
      <div>
        <CampoBool accessor="gestor" label={strings.gestor} />
      </div>
      <div>
        <CampoBool accessor="admin" label={strings.admin} />
      </div>
    </CampoObjeto>
  </SimpleForm>
)
