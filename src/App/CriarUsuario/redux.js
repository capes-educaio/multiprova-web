import { bindActionCreators } from 'redux'
// import { actions } from 'utils/actions'

export function mapStateToProps(state) {
  return {
    strings: state.strings,
    usuarioAtual: state.usuarioAtual,
    urlParaEnviarEmail: state.urlParaEnviarEmail,
  }
}

export function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch)
}
