export const validarPermissoes = value => {
  const validacao = {
    sucesso: true,
    erros: [],
  }
  if (!value.docente && !value.discente && !value.gestor) {
    validacao.sucesso = false
    validacao.erros.push({ code: 'pelos_menos_uma_permissao' })
  }
  return validacao
}
