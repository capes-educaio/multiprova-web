import React from 'react'

import Divider from '@material-ui/core/Divider'
import CircularProgress from '@material-ui/core/CircularProgress'
import { showMessage } from 'utils/Snackbar'

import { post, get, patch } from 'api'
import { PaginaPaper } from 'common/PaginaPaper'
import { TituloDaPagina } from 'common/TituloDaPagina'
import { UsuarioForm } from './UsuarioForm'
import { getUsuarioFormConfig } from './getUsuarioFormConfig'
import { propTypes } from './propTypes'
import { setUrlState } from 'localModules/urlState'
import Button from '@material-ui/core/Button'
import Cancel from '@material-ui/icons/Cancel'
import Send from '@material-ui/icons/Send'

const PERMISSOES_ENUM = {
  1: 'admin',
  2: 'docente',
  3: 'discente',
  5: 'gestor',
}

export class CriarUsuarioComponent extends React.Component {
  static propTypes = propTypes
  id = undefined

  constructor(props) {
    super(props)
    this.state = {
      isValid: false,
      valor: getUsuarioFormConfig(),
      formRef: null,
      carregando: true,
    }
  }

  componentDidMount = () => {
    this.id = this.props.history.location.pathname.split('/')[2]
    if (this.id === 'novo') this.id = undefined

    if (this.id) {
      get(`/usuarios/${this.id}`).then(res => {
        let _permissoes = {}
        for (let permissao of res.data.permissoes) {
          _permissoes[PERMISSOES_ENUM[permissao]] = true
        }

        this.setState({
          carregando: false,
          valor: {
            ...res.data,
            ...{
              passwords: {
                password: '',
                confirmPassword: '',
              },
            },
            ...{
              permissoes: _permissoes,
            },
          },
        })
      })
    } else this.setState({ carregando: false })
  }

  setFormRef = formRef => this.setState({ formRef })

  onValid = isValid => {
    if (this.id !== undefined) this.setState({ isValid: true })
    else this.setState({ isValid })
  }

  onFormChange = valor => this.setState({ valor })

  _salvarUsuario = () => {
    const { valor } = this.state
    const data = { ...this.state.valor }
    delete data.passwords
    data.permissoes = []
    if (valor.permissoes.admin) data.permissoes.push(1)
    if (valor.permissoes.docente) data.permissoes.push(2)
    if (valor.permissoes.discente) data.permissoes.push(3)
    if (valor.permissoes.gestor) data.permissoes.push(5)

    if (!this.id) {
      data.password = valor.passwords.password
    } else if (valor.passwords.password !== '') {
      data.password = valor.passwords.password
    }

    if (!this.id) {
      post('/usuarios/', data)
        .then(this._salvarUsuarioSucesso)
        .catch(this._salvarUsuarioFalha)
    } else {
      patch(`/usuarios/${this.id}`, data)
        .then(this._salvarUsuarioSucesso)
        .catch(this._salvarUsuarioFalha)
    }
  }

  _salvarUsuarioSucesso = response => {
    this.id = response.data.id
    setUrlState({ pathname: `/usuario/${this.id}` })
    this.setState({ valor: { ...this.state.valor }, ...{ id: this.id } })
    const { strings } = this.props
    showMessage.success({ message: !this.id ? strings.usuarioCriado : strings.usuarioEditado })
  }

  _salvarUsuarioFalha = () => {
    const { strings } = this.props
    showMessage.error({ message: strings.usuarioErro })
  }

  _mostrarErros = () => {
    const { formRef } = this.state
    if (formRef) formRef.showError()
  }

  render() {
    const { onFormChange, onValid, setFormRef, _salvarUsuario, _mostrarErros } = this
    const { valor, isValid, carregando } = this.state
    const { classes, strings } = this.props

    if (carregando)
      return (
        <div className={classes.loading}>
          <CircularProgress />
        </div>
      )

    return (
      <PaginaPaper>
        <TituloDaPagina>{!this.id ? strings.adicionarUsuario : strings.editarUsuario}</TituloDaPagina>
        <Divider />
        <div className={classes.secao}>
          {<UsuarioForm {...{ onFormChange, onValid, valor, setFormRef, strings }} />}
        </div>
        <Divider />

        <div className={classes.secao}>
          <Button
            onClick={() => {
              if (isValid) _salvarUsuario()
              else _mostrarErros()
            }}
          >
            <Send className={classes.leftIcon} /> {this.id ? strings.salvar : strings.cadastrar}
          </Button>
          <Button
            onClick={() => {
              setUrlState({ pathname: '/usuarios' })
            }}
            className={classes.botaoSecundario}
          >
            <Cancel className={classes.leftIcon} />
            {strings.sair}
          </Button>
        </div>
      </PaginaPaper>
    )
  }
}
