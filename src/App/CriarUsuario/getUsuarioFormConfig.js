export const getUsuarioFormConfig = () => ({
  id: undefined,
  nome: '',
  instituicao: '',
  username: '',
  email: '',
  passwords: {
    password: '',
    confirmPassword: '',
  },
  permissoes: {
    admin: false,
    docente: false,
    discente: false,
    gestor: false,
  },
})
