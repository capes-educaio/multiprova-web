import React from 'react'

import { InputTag } from 'common/InputTag'
import { setListaTags } from 'utils/compositeActions'
import { propTypes } from './propTypes'

export class FormFiltroPadraoComponent extends React.Component {
  static propTypes = propTypes

  componentDidMount = () => {
    setListaTags()
    const { onRef } = this.props
    if (onRef) onRef(this)
    this.limparFiltro()
  }

  componentWillUnmount = () => {
    const { onRef } = this.props
    if (onRef) onRef(null)
  }

  alterarValorDoForm = (campo, valor) => {
    const { onFormFiltroChange, valorDoForm } = this.props
    valorDoForm[campo] = valor
    onFormFiltroChange(valorDoForm)
    this.setState({ [campo]: valor })
  }

  limparFiltro = () => {
    const { onFormFiltroChange } = this.props
    const filtroLimpo = {
      tagsSelected: [],
    }
    onFormFiltroChange(filtroLimpo)
  }

  handleChangeTags = tags => {
    this.alterarValorDoForm('tagsSelected', tags)
  }

  render() {
    const { valorDoForm, handleKeyPress } = this.props
    let { tagsSelected } = valorDoForm

    if (tagsSelected === undefined) tagsSelected = []

    return <InputTag onChange={this.handleChangeTags} valor={tagsSelected} handleKeyPress={handleKeyPress} />
  }
}
