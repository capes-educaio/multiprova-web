import { CardDisplay } from 'common/CardDisplay'
import { PaginaPaper } from 'common/PaginaPaper'
import { TituloDaPagina } from 'common/TituloDaPagina'
import { rotasTag } from 'localModules/TagAssets/rotasTag'
import React, { Component } from 'react'
import { CardGerenciamentoTags } from './CardGerenciamentoTags'
import { propTypes } from './propTypes'
import { Busca } from 'common/Busca'
import { FormFiltroPadrao } from './FormFiltroPadrao'
import { FormFiltroExpandido } from './FormFiltroExpandido'

export class GerenciamentoTagsComponent extends Component {
  static propTypes = propTypes
  rotasTags

  componentWillMount = () => {
    const { usuarioAtual } = this.props
    this.rotasTags = rotasTag(usuarioAtual._data.id)
  }

  atualizarCardDisplay = props => this.cardDisplay && this.cardDisplay.fetchInstancias(props)

  montarFiltro = valorDoForm => {
    const { tagsSelected } = valorDoForm
    let where = {}
    if (tagsSelected.length > 0) {
      where.or = tagsSelected.map(tag => {
        return { nome: { regexp: `.*${tag.nome}.*` } }
      })
    }
    const include = {}
    return {
      include,
      where,
    }
  }

  render() {
    const { strings } = this.props

    const { montarFiltro } = this

    return (
      <div>
        <PaginaPaper>
          <TituloDaPagina>{strings.minhasTags}</TituloDaPagina>
        </PaginaPaper>
        <Busca FormPadrao={FormFiltroPadrao} FormExpandido={FormFiltroExpandido} montarFiltro={montarFiltro} />
        <CardDisplay
          nadaCadastrado={strings.voceNaoPossuiNenhumaTag}
          quantidadeUrl={this.rotasTags.back.count}
          fetchInstanciasUrl={this.rotasTags.back.find}
          CardComponent={CardGerenciamentoTags}
          numeroDeColunas={{
            extraSmall: 1,
            small: 2,
            medium: 3,
            large: 3,
            extraLarge: 3,
          }}
          quantidadePorPaginaInicial={12}
          quantidadePorPaginaOpcoes={[12, 24, 48, 96]}
          onRef={ref => (this.cardDisplay = ref)}
          contexto={{
            rotas: this.rotasTags,
            atualizarCardDisplay: this.atualizarCardDisplay,
          }}
        />
      </div>
    )
  }
}
