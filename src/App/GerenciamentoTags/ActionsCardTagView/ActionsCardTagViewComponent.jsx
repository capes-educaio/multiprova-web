import React, { Component } from 'react'
import EditOutlinedIcon from '@material-ui/icons/EditOutlined'
import { MpIconButton } from 'common/MpIconButton'
import DeleteOutlinedIcon from '@material-ui/icons/DeleteOutlined'

export class ActionsCardTagViewComponent extends Component {
  get actions() {
    const { strings, handleClickEditarTag, handleClickExcluirTag } = this.props
    let opcoesArray = [
      {
        IconComponent: EditOutlinedIcon,
        onClick: handleClickEditarTag,
        tooltip: strings.editar,
        key: 'editar' + this._id,
      },

      {
        IconComponent: DeleteOutlinedIcon,
        onClick: handleClickExcluirTag,
        tooltip: strings.excluir,
        key: 'excluir' + this._id,
      },
    ]
    return opcoesArray
  }

  render() {
    return this.actions.map(({ IconComponent, onClick, tooltip, iconProps, key }) => (
      <MpIconButton {...{ IconComponent, onClick, tooltip, iconProps, key }} round />
    ))
  }
}
