import React from 'react'

import { propTypes } from './propTypes'

export class FormFiltroExpandidoComponent extends React.Component {
  static propTypes = propTypes

  render() {
    const { classes } = this.props

    return (
      <div>
        <div className={classes.botoes}>{this.props.children}</div>
      </div>
    )
  }
}
