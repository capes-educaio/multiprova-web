export const style = theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
    boxShadow: 'none',
  },
  buttonIcon: {
    marginRight: 6,
  },
  appBar: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    boxShadow: 'none',
  },
  toRight: {
    marginLeft: 'auto',
    marginRight: '15px',
  },
  tab: {
    background: theme.palette.primary.contrastText,
  },
})
