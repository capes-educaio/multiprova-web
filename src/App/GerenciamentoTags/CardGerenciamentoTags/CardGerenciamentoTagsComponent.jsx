import { TagCard } from 'common/TagCard'
import React, { Component } from 'react'
import { ActionsCardTagView } from '../ActionsCardTagView'
import { ActionsCardTagEdit } from '../ActionsCardTagEdit'
import { AlertaDeConfirmacao } from 'common/AlertaDeConfirmacao'

import { deleteTag } from 'localModules/TagAssets/chamadasApiTags'
import { patchTag } from 'localModules/TagAssets/chamadasApiTags'

export class CardGerenciamentoTagsComponent extends Component {
  state = { modoEdicao: false, nomeDigitado: this.props.instancia.nome, alertaExcluirAberto: false }

  get actions() {
    return this.state.modoEdicao ? (
      <ActionsCardTagEdit
        handleClickConfirm={this.handleClickConfirmEdit}
        handleClickCancel={this.handleClickCancelEdit}
      />
    ) : (
      <ActionsCardTagView
        handleClickEditarTag={this.handleClickEditarTag}
        handleClickExcluirTag={this.handleClickExcluirTag}
      />
    )
  }

  componentDidUpdate(prevProps) {
    if (prevProps.instancia !== this.props.instancia) {
      this.updateNomeDigitado(this.props.abaAtual)
    }
  }

  updateNomeDigitado = () => {
    this.setState({ nomeDigitado: this.props.instancia.nome })
  }

  atualizarPagina = async () => await this.props.atualizarCardDisplay()

  handleChangeNomeDigitado = nomeDigitado => {
    this.setState({ nomeDigitado })
  }

  handleClickEditarTag = () => {
    this.setState({ modoEdicao: true })
  }
  handleClickExcluirTag = async () => {
    this.setState({ alertaExcluirAberto: true })
  }

  handleClickConfirmEdit = async () => {
    await patchTag(this.props.instancia.id, { nome: this.state.nomeDigitado })
    this.setState({ modoEdicao: false })
    this.atualizarPagina()
  }
  handleClickCancelEdit = () => {
    this.setState({ modoEdicao: false })
  }

  handleCloseAlertaConfirmacaoExcluir = async confirma => {
    this.closeAlerta()
    if (confirma) {
      await deleteTag(this.props.instancia.id)
      this.atualizarPagina()
    }
  }
  closeAlerta = () => this.setState({ alertaExcluirAberto: false })

  render() {
    const { instancia, strings } = this.props
    const { modoEdicao, nomeDigitado, alertaExcluirAberto } = this.state
    return (
      <React.Fragment>
        <TagCard
          instancia={instancia}
          opcoes={this.actions}
          modoEdicao={modoEdicao}
          handleChangeNomeDigitado={this.handleChangeNomeDigitado}
          nomeDigitado={nomeDigitado}
        />
        <AlertaDeConfirmacao
          alertaAberto={alertaExcluirAberto}
          descricaoSelected={strings.temCertezaQueDesejaExcluirATag}
          handleClose={this.handleCloseAlertaConfirmacaoExcluir}
          stringNomeInstancia={strings.excluirTag}
        />
      </React.Fragment>
    )
  }
}
