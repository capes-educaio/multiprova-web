import React, { Component } from 'react'
import { MpIconButton } from 'common/MpIconButton'
import DoneIcon from '@material-ui/icons/Done'
import CancelIcon from '@material-ui/icons/Cancel'

export class ActionsCardTagEditComponent extends Component {
  get actions() {
    const { strings, handleClickConfirm, handleClickCancel } = this.props
    let opcoesArray = [
      {
        IconComponent: DoneIcon,
        onClick: handleClickConfirm,
        tooltip: strings.salvar,
        key: 'editar' + this._id,
      },

      {
        IconComponent: CancelIcon,
        onClick: handleClickCancel,
        tooltip: strings.cancelar,
        key: 'excluir' + this._id,
      },
    ]
    return opcoesArray
  }

  render() {
    return this.actions.map(({ IconComponent, onClick, tooltip, iconProps, key }) => (
      <MpIconButton {...{ IconComponent, onClick, tooltip, iconProps, key }} round />
    ))
  }
}
