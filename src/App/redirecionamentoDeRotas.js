import { get } from 'api'

import { setUrlState, setRetornoLogin } from 'localModules/urlState'

import { cookies } from 'utils/cookies'
import { logar } from 'utils/compositeActions'

const recuperarSessaoPassadaComCookies = async (usuarioAtual, authToken) => {
  if (typeof authToken === 'object' && !usuarioAtual) {
    const { data: userData } = await get('/usuarios/' + authToken.userId, null, authToken.id)
    logar({ authToken, userData })
  }
}

const mandarNaoAutenticadosParaLogin = (history, authToken) => {
  const { pathname } = history.location
  if (
    typeof authToken !== 'object' &&
    pathname !== '/login' &&
    pathname !== '/esqueci-a-senha' &&
    !pathname.includes('resgatar')
  ) {
    setRetornoLogin()
    setUrlState({ pathname: '/login' })
  }
}

export const redirecionamentoDeRotas = (usuarioAtual, historyAtual, historyProxima) => {
  const authToken = cookies.get('authToken')
  const historyMaisRecente = historyProxima ? historyProxima : historyAtual
  mandarNaoAutenticadosParaLogin(historyMaisRecente, authToken)
  recuperarSessaoPassadaComCookies(usuarioAtual, authToken)
}
