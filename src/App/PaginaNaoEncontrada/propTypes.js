import PropTypes from 'prop-types'

export const propTypes = {
  // redux state
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
