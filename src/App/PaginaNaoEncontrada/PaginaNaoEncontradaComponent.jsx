import React, { Component } from 'react'

import { TituloDaPagina } from 'common/TituloDaPagina'
import { PaginaPaper } from 'common/PaginaPaper'

import { propTypes } from './propTypes'

export class PaginaNaoEncontradaComponent extends Component {
  static propTypes = propTypes

  instancia

  constructor(props) {
    super(props)
    const { match, strings } = props
    this.instancia = match.params.instancia
    this.state = {
      stringExibida: strings.paginaNaoEncontrada,
    }
  }

  componentDidMount = () => {
    this.verificaStringInstancia()
  }

  verificaStringInstancia = () => {
    const { strings } = this.props
    switch (this.instancia) {
      case 'questao':
        this.setState({ stringExibida: strings.questaoNaoEncontrada })
        break
      case 'prova':
        this.setState({ stringExibida: strings.provaNaoEncontrada })
        break
      default:
        break
    }
  }

  render() {
    const { stringExibida } = this.state
    return (
      <PaginaPaper>
        <TituloDaPagina>{stringExibida}</TituloDaPagina>
      </PaginaPaper>
    )
  }
}
