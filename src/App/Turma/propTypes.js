import PropTypes from 'prop-types'

export const propTypes = {
  // redux state
  usuarioAtual: PropTypes.object,
  // FormComponent: PropTypes.any.isRequired,
  valor: PropTypes.object,
  // style
  classes: PropTypes.object.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
}
