export const getInitialStore = () => ({
  valor: {
    id: '',
    nome: '',
    ano: '',
    periodo: '',
    numero: '',
    metadados: {
      professor: '',
    },
    alunos: [],
  },
})
