export const style = theme => ({
  carimbo: {
    textAlign: 'center',
    color: '#ccc',
    fontFamily: 'Arial, sans-serif',
  },
  iconeGrande: {
    fontSize: '4em',
  },
})
