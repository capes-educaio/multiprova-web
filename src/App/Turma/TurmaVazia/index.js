import { TurmaVaziaComponent } from './TurmaVaziaComponent'
import { withStyles } from '@material-ui/core/styles'

import { style } from './style'

export const TurmaVazia = withStyles(style)(TurmaVaziaComponent)
