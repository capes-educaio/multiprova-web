import React, { Component } from 'react'

import SupervisorAccountOutlined from '@material-ui/icons/SupervisorAccountOutlined'

export class TurmaVaziaComponent extends Component {
  render() {
    const { classes } = this.props
    return (
      <div className={classes.carimbo}>
        <div>
          <SupervisorAccountOutlined className={classes.iconeGrande} />
        </div>
        <div>Turma sem alunos</div>
      </div>
    )
  }
}
