import CircularProgress from '@material-ui/core/CircularProgress'
import Divider from '@material-ui/core/Divider'
import Grid from '@material-ui/core/Grid'
import { GroupAdd } from '@material-ui/icons'
import Cancel from '@material-ui/icons/Cancel'
import Send from '@material-ui/icons/Send'
import { get, post, put } from 'api'
import { AlertaDeConfirmacao } from 'common/AlertaDeConfirmacao'
import { MpButton } from 'common/MpButton'
import { PaginaPaper } from 'common/PaginaPaper'
import { Subtitulo } from 'common/Subtitulo'
import { TituloDaPagina } from 'common/TituloDaPagina'
import { CampoText } from 'localModules/ReduxForm/CampoText'
import { SimpleReduxForm } from 'localModules/ReduxForm/SimpleReduxForm'
import { setUrlState } from 'localModules/urlState'
import React, { Component } from 'react'
import { saoIguaisDeepEqual } from 'utils/compararObjetos'
import { Snackbar } from 'utils/Snackbar'
import { AlunosDialog } from './AlunosDialog'
import { propTypes } from './propTypes'
import { TabelaDeAlunos } from './TabelaDeAlunos'
import { TabelaDeAlunosPendentes } from './TabelaDeAlunosPendentes'

export class TurmaComponent extends Component {
  static propTypes = propTypes
  isValid = false
  valor
  formRefs
  _isEditar

  constructor(props) {
    super(props)
    this._isEditar = false
    this.state = {
      carregando: true,
      opensnackbar: false,
      message: '',
      snackbarVariant: null,
      valor: {},
      alertaSair: false,
      modalInserirAlunosAberto: false,
      valorFoiModificado: false,
      valorAntigo: {},
    }
  }

  componentDidMount = () => {
    const turmaId = this.props.history.location.pathname.split('/')[2]
    if (turmaId) this._fetchTurmaParaEdicao(`/turmas/${turmaId}`)
    else this._montarValorInicialVazio()
  }

  _fetchTurmaParaEdicao = url => {
    get(url)
      .then(res => {
        this._montarValorInicialEditar(res.data)
      })
      .catch(err => {
        console.error(err)
      })
  }

  get existemAlunosPendentes() {
    return this.state.valor.alunosPendentes && this.state.valor.alunosPendentes.length > 0
  }

  _montarValorInicialVazio = () => {
    const { usuarioAtual } = this.props
    const nomeProfessor = usuarioAtual.data.nome

    this.setState({
      carregando: false,
      valor: {
        nome: '',
        ano: '',
        periodo: '',
        numero: '',
        professor: nomeProfessor,
        alunos: [],
        alunosPendentes: [],
        usuarioId: '',
      },
      valorAntigo: {
        nome: '',
        ano: '',
        periodo: '',
        numero: '',
        professor: nomeProfessor,
        alunos: [],
        alunosPendentes: [],
        usuarioId: '',
      },
    })
  }

  _montarValorInicialEditar = dados => {
    this._isEditar = true
    const { professor } = dados.metadados
    delete dados.metadados
    const valor = { ...dados, professor }
    const valorAntigo = JSON.parse(JSON.stringify(valor))
    this.setState({ carregando: false, valor, valorAntigo })
  }

  _handleSubmit = event => {
    event.preventDefault()
    const { _isEditar, _mostrarErros, _atualizarTurma, _salvarNovaTurma, isValid } = this
    const { valor } = this.state
    const turmaParaSalvar = Object.assign({}, valor)
    if (isValid) {
      this.setState({ valorFoiModificado: false })
      if (_isEditar) _atualizarTurma(turmaParaSalvar)
      else _salvarNovaTurma(turmaParaSalvar)
    } else _mostrarErros()
  }

  _mostrarErros = () => {
    this.formRefs.showError()
  }

  _salvarNovaTurma = dados => {
    post(`turmas`, this.formatarDadosAntesDeEnviar(dados)).then(() => {
      this.setState({
        opensnackbar: true,
        message: this.props.strings.asAlteracoesForamSalvas,
        snackbarVariant: 'success',
      })
    })
  }

  _atualizarTurma = dados => {
    const fk = dados.id
    delete dados.id
    put(`turmas/${fk}`, this.formatarDadosAntesDeEnviar(dados))
      .then(() => {
        this.setState({
          opensnackbar: true,
          message: this.props.strings.asAlteracoesForamSalvas,
          snackbarVariant: 'success',
        })
      })
      .catch(e => {
        this.setState({
          opensnackbar: true,
          message: this.props.strings.naoFoiPossivelSalvarATurma,
          snackbarVariant: 'error',
        })
      })
  }

  formatarDadosAntesDeEnviar = dados => {
    const { professor } = dados
    delete dados.professor
    dados.periodo = Number(dados.periodo)
    dados.ano = Number(dados.ano)
    return { ...dados, metadados: { professor } }
  }

  _checarSeAlunoJaExiste = async alunoForm => {
    const { email } = alunoForm
    const user = await get(`/usuarios/usuarios-discentes-by-email`, { email })
    let usuarioExistenteInfo = null
    const usuarioJaEstaCadastrado = user.data.length > 0

    if (usuarioJaEstaCadastrado) {
      usuarioExistenteInfo = this._montarAlunoFromRequest(user)
    }
    return { usuarioJaEstaCadastrado, usuarioExistenteInfo }
  }

  _montarAlunoFromRequest = user => {
    return {
      nome: user.data[0].nome,
      email: user.data[0].email,
      id: user.data[0].id,
      validado: user.data[0].validado,
      matricula: Number(user.data[0].matricula),
    }
  }

  _handleCriarAlunoAPartirDoForm = async alunoForm => {
    const { usuarioJaEstaCadastrado, usuarioExistenteInfo } = await this._checarSeAlunoJaExiste(alunoForm)
    if (usuarioJaEstaCadastrado) {
      if (this._checarSeAlunoJaPertenceATurma(usuarioExistenteInfo)) {
        this.setState({
          opensnackbar: true,
          message: this.props.strings.usuarioJaExistenteNaTurma,
          snackbarVariant: 'error',
        })
      } else {
        this.setState({
          opensnackbar: true,
          message: this.props.strings.usuarioJaExistente,
          snackbarVariant: 'error',
        })
        this._salvarAlunoNaTurma(usuarioExistenteInfo)
      }
    } else {
      try {
        alunoForm.redirectUrl = this.props.urlParaEnviarEmail
        const alunoCriadoComSucesso = await this._criarAluno(alunoForm)
        if (alunoCriadoComSucesso) {
          const aluno = await this._recuperarAlunoCriado(alunoForm.email)
          if (aluno) this._salvarAlunoNaTurma(aluno)
          else console.warn('Erro ao buscar aluno recém criado')
        } else console.warn('Erro ao criar usuário')
      } catch (e) {
        console.warn(e)
      }
    }
  }
  _recuperarAlunoCriado = async email => {
    const res = await get(`/usuarios/usuarios-discentes-by-email`, { email })
    if (res.data.length > 0) return this._montarAlunoFromRequest(res)
    else return null
  }

  _checarSeAlunoJaPertenceATurma = aluno => {
    const alunosDaTurma = this.state.valor.alunos
    return alunosDaTurma.some(alunoExistente => aluno.email === alunoExistente.email)
  }

  _salvarAlunoNaTurma = aluno => {
    const { _compararAlunos } = this
    const { valor } = this.state

    let alunos = [aluno, ...valor.alunos]
    alunos = alunos.sort(_compararAlunos)
    const nextState = { ...valor, alunos }
    this.setState({ valor: nextState })
  }

  _compararAlunos = (alunoA, alunoB) => {
    return alunoA.nome > alunoB.nome ? 1 : alunoB.nome > alunoA.nome ? -1 : 0
  }

  _handleClose = () => {
    this.setState({ opensnackbar: false })
  }
  _abrirModalAlunos = () => {
    this.setState({ modalInserirAlunosAberto: true })
  }
  _fecharModalAlunos = () => {
    this.setState({ modalInserirAlunosAberto: false })
  }

  _abrirAlunosModal = () => {
    this.setState({ modalInserirAlunosAberto: true })
  }

  _voltarParaPaginaDeOrigem = () => setUrlState({ pathname: '/turmas' })

  _handleOnValid = isValid => (this.isValid = isValid)

  handleClickSair = () => {
    if (this.state.valorFoiModificado) {
      this.setState({ alertaSair: true })
    } else {
      this._voltarParaPaginaDeOrigem()
    }
  }

  handleCloseSair = confirmaSair => {
    if (confirmaSair === true) this._voltarParaPaginaDeOrigem()
    this.setState({
      alertaSair: false,
    })
  }

  _handleAlunosGerenciados = data => {
    this.setState(prev => ({
      valor: {
        ...prev.valor,
        ...data,
      },
    }))
  }
  _fecharModalGerenciarAlunos = () => {
    this.setState({ modalInserirAlunosAberto: false })
  }

  _excluirAlunoPendente = index => {
    const { valor } = this.state
    const alunosPendentes = [...this.state.valor.alunosPendentes]
    alunosPendentes.splice(index, 1)
    valor.alunosPendentes = alunosPendentes
    this.setState({ valor })
  }
  _excluirAluno = index => {
    const { valor } = this.state
    const alunos = [...this.state.valor.alunos]
    alunos.splice(index, 1)
    valor.alunos = alunos
    this.setState({ valor })
  }

  _criarAluno = async alunoForm => {
    const aluno = { ...alunoForm, matricula: Number(alunoForm.matricula) }
    const res = await post('/usuarios/aluno', aluno)
    return res.status === 200
  }

  _handleValorChange = novoValor => {
    const { valor, valorAntigo, valorFoiModificado } = this.state
    if (!valorFoiModificado && !saoIguaisDeepEqual(valor, valorAntigo)) {
      this.setState({
        valorFoiModificado: true,
      })
    }
  }

  render() {
    const { strings, classes } = this.props
    const {
      carregando,
      opensnackbar,
      message,
      snackbarVariant,
      valor,
      alertaSair,
      modalInserirAlunosAberto,
    } = this.state
    const { _handleOnValid, _handleSubmit, _handleClose, handleClickSair, handleCloseSair } = this

    if (carregando)
      return (
        <div className={classes.carregando}>
          <CircularProgress />
        </div>
      )
    const FormComponent = (
      <SimpleReduxForm
        id="form"
        onRef={ref => (this.formRefs = ref)}
        valor={valor}
        wrapperProps={{ spacing: 8, container: true }}
        WrapperComponent={Grid}
        onValid={_handleOnValid}
        onChange={this._handleValorChange}
      >
        <CampoText
          id="nomeDaTurma"
          accessor="nome"
          label={strings.nome}
          required
          textFieldProps={{ fullWidth: true }}
          wrapperProps={{ xs: 12, item: true }}
          WrapperComponent={Grid}
        />
        <CampoText
          id="professor"
          accessor="professor"
          label={strings.professor}
          textFieldProps={{ fullWidth: true }}
          wrapperProps={{ xs: 12, item: true }}
          WrapperComponent={Grid}
        />
        <CampoText
          id="numero"
          accessor="numero"
          label={strings.numero}
          textFieldProps={{ fullWidth: true }}
          wrapperProps={{ xs: 4, item: true }}
          WrapperComponent={Grid}
        />
        <CampoText
          id="ano"
          accessor="ano"
          label={strings.ano}
          required
          textFieldProps={{
            fullWidth: true,
            type: 'number',
          }}
          wrapperProps={{ xs: 4, item: true }}
          WrapperComponent={Grid}
        />
        <CampoText
          id="periodo"
          accessor="periodo"
          label={strings.periodo}
          required
          textFieldProps={{
            fullWidth: true,
            type: 'number',
            inputProps: { min: 1, max: 4 },
          }}
          wrapperProps={{ xs: 4, item: true }}
          WrapperComponent={Grid}
          validar={value => {
            let sucesso = true,
              erros = []
            if (value < 1 || value > 4) {
              sucesso = false
              erros.push({ code: 'valor_invalido' })
            }
            return { sucesso, erros }
          }}
          mensagensDeErro={{ valor_invalido: strings.entre1e4 }}
        />
      </SimpleReduxForm>
    )

    return (
      <div>
        <PaginaPaper>
          <TituloDaPagina>{strings.cadastrarTurma}</TituloDaPagina>
          <Divider />
          <div className={classes.secao}>{FormComponent}</div>
          <div className={classes.secao}>
            <Grid xs={12} item>
              {this.existemAlunosPendentes && (
                <React.Fragment>
                  <Subtitulo>
                    {strings.alunosPendentes}
                    <span className={classes.naoCadastrados}>({strings.alunosNaoCadastrados})</span>
                  </Subtitulo>
                  <div className={classes.tabelaDeAlunos}>
                    <TabelaDeAlunosPendentes
                      salvarAluno={this._handleCriarAlunoAPartirDoForm}
                      valor={valor.alunosPendentes}
                      excluirAlunoPendente={this._excluirAlunoPendente}
                    ></TabelaDeAlunosPendentes>
                  </div>
                </React.Fragment>
              )}
              <div className={classes.marginTop}>
                <Subtitulo>{strings.getAlunoPlural({ numeroUsos: 2 })}</Subtitulo>
                <MpButton
                  IconComponent={GroupAdd}
                  variant="material"
                  buttonProps={{ variant: 'contained', color: 'primary' }}
                  marginRight
                  onClick={this._abrirAlunosModal}
                >
                  {strings.inserirAlunos}
                </MpButton>
                <div className={classes.tabelaDeAlunos}>
                  <TabelaDeAlunos valor={valor.alunos} excluirAluno={this._excluirAluno} />
                </div>
              </div>
            </Grid>

            <MpButton
              buttonProps={{ variant: 'contained', color: 'primary', type: 'submit' }}
              IconComponent={Send}
              onClick={_handleSubmit}
              variant="material"
            >
              {strings.salvar}
            </MpButton>
            <MpButton
              onClick={handleClickSair}
              className={classes.botaoSecundario}
              IconComponent={Cancel}
              variant="material"
            >
              {strings.sair}
            </MpButton>
          </div>
        </PaginaPaper>

        <Snackbar
          variant={snackbarVariant}
          open={opensnackbar}
          message={message}
          onClose={_handleClose}
          autoHideDuration={3000}
        />
        <AlertaDeConfirmacao
          alertaAberto={alertaSair}
          descricaoSelected={''}
          handleClose={handleCloseSair}
          stringNomeInstancia={strings.sairSemSalvarTurma}
        />
        {modalInserirAlunosAberto && (
          <AlunosDialog
            open={modalInserirAlunosAberto}
            onClose={this._fecharModalGerenciarAlunos}
            handleParticipantesGerenciados={this._handleAlunosGerenciados}
            usuariosJaSalvos={valor.alunos}
            usuariosPendentes={valor.alunosPendentes}
            abas={[
              { value: 'usuario', label: strings.buscaPorUsuario },
              { value: 'lote', label: strings.inserirEmLote },
            ]}
          />
        )}
      </div>
    )
  }
}
