export const style = theme => ({
  secao: {
    padding: '20px 20px 20px 20px',
  },
  marginTop: {
    marginTop: 20,
  },
  tabelaDeAlunos: {
    marginTop: 20,
    marginBottom: 20,
  },
  dialog: {
    fontFamily: 'Arial, sans-serif',
  },
  carregando: {
    display: 'flex',
    justifyContent: 'center',
    position: 'absolute',
    top: '50%',
    left: '50%',
  },
  botaoSecundario: { marginLeft: 10 },
  naoCadastrados: {
    paddingLeft: 8,
    fontSize: 12,
    color: 'grey',
    fontStyle: 'italic',
  },
})
