import PropTypes from 'prop-types'

export const propTypes = {
  strings: PropTypes.object.isRequired,
  salvarAluno: PropTypes.func.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
