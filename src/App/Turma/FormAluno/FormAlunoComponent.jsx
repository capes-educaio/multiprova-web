import React, { Component } from 'react'

import { propTypes } from './propTypes'
import { SimpleReduxForm } from 'localModules/ReduxForm/SimpleReduxForm'
import { CampoText } from 'localModules/ReduxForm/CampoText'
import Grid from '@material-ui/core/Grid'
import Add from '@material-ui/icons/Add'

import { MpButton } from 'common/MpButton'

export class FormAlunoComponent extends Component {
  static propTypes = propTypes
  VALOR_INICIAL_ALUNO_DEFAULT = { nome: '', matricula: '', email: '' }
  state = { valor: { ...this.VALOR_INICIAL_ALUNO_DEFAULT } }
  isValid = false
  formRef

  componentWillMount() {
    const { email = '', matricula } = this.props
    const emailString = email ? email : ''
    this.setState({
      valor: { ...this.state.valor, email: emailString, matricula },
    })
  }
  handleSubmit = () => {
    if (this.isValid) {
      this.props.salvarAluno(this.state.valor)
      this.formRef._updateValues({ ...this.VALOR_INICIAL_ALUNO_DEFAULT })
    } else this.formRef.showError()
  }

  handleOnValid = isValid => {
    this.isValid = isValid
  }

  render() {
    const { strings, classes, id, matricula, nome, email } = this.props
    const emailString = email ? email : ''

    const { valor } = this.state
    return (
      <fieldset>
        <legend className={classes.labelFildset}>{strings.cadastrarAluno}</legend>
        <SimpleReduxForm
          id={`form-aluno${id}`}
          valor={valor}
          onRef={ref => (this.formRef = ref)}
          wrapperProps={{ spacing: 8, container: true }}
          WrapperComponent={Grid}
          onValid={this.handleOnValid}
        >
          <CampoText
            id="matricula"
            accessor="matricula"
            label={strings.matricula}
            value={matricula}
            required
            textFieldProps={{
              fullWidth: true,
              type: 'number',
            }}
            wrapperProps={{ xs: 4, item: true }}
            WrapperComponent={Grid}
          />
          <CampoText
            id="nome"
            accessor="nome"
            label={strings.nome}
            value={nome}
            required
            textFieldProps={{ fullWidth: true, inputProps: { type: 'email', required: true } }}
            wrapperProps={{ xs: 4, item: true }}
            WrapperComponent={Grid}
          />
          <CampoText
            id="email"
            accessor="email"
            label={strings.email}
            value={emailString}
            required
            mensagensDeErro={{ email_invalido: strings.emailInvalido }}
            validar={value => {
              let sucesso = true
              const erros = []
              if (!value.match(/[\w]{1,}[\w.+-]{0,}@[\w-]{2,}([.][a-zA-Z]{2,}|[.][\w-]{2,}[.][a-zA-Z]{2,})/g)) {
                sucesso = false
                erros.push({ code: 'email_invalido' })
              }
              return { sucesso, erros }
            }}
            textFieldProps={{
              fullWidth: true,
            }}
            wrapperProps={{ xs: 4, item: true }}
            WrapperComponent={Grid}
          />
        </SimpleReduxForm>
        <MpButton
          buttonProps={{ color: 'primary' }}
          IconComponent={Add}
          onClick={this.handleSubmit}
          loadOnClick={false}
          variant="material"
        >
          {strings.criarUsuario}
        </MpButton>
      </fieldset>
    )
  }
}
