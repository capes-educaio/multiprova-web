export const style = theme => ({
  root: {},
  title: {
    display: 'flex',
    flexDirection: 'row ',
  },
  dialogContent: { overflowY: 'visible' },
  formControl: {
    display: 'flex',
    flexDirection: 'row ',
  },
  group: {
    flexDirection: 'row ',
  },
  buscaPorUsuariosInput: {
    display: 'flex',
    flexDirection: 'row ',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  TextField: {
    width: '100%',
  },
  loading: {
    minWidth: '20px',
  },
})
