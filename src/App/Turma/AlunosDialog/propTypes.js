import PropTypes from 'prop-types'

export const propTypes = {
  // lista de participantes que ja fazem parte da aplicação da prova
  usuariosJaSalvos: PropTypes.array,
  // funcao que recebe os usuarios selecionados
  handleUsuariosAdicionados: PropTypes.func.isRequired,
  // deve lidar com o fechamento do modal
  onClose: PropTypes.func.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
