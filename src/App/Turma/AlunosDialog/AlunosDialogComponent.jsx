import { TextField } from '@material-ui/core'
import { get } from 'api'
import { ListaUsuariosBusca } from 'common/ListaUsuariosBusca'
import React, { Component } from 'react'
import _ from 'lodash'

export class AlunosDialogComponent extends Component {
  state = {
    participantesDaBusca: [],
    participantesSelecionados: [],
    valorDigitadoBuscaUsuario: '',
    turmas: [],
    turmaId: '',
  }
  timerParaBuscaNaAPI = null

  componentWillMount = () => {
    this.props.ligarLoading()
    this.setState({
      participantesSelecionados: [...this.props.usuariosJaSalvos],
    })
    this.props.desligarLoading()
  }
  componentDidUpdate(prevProps) {
    if (prevProps.abaAtual !== this.props.abaAtual) {
      this.handleChangeAba(this.props.abaAtual)
    }
  }

  converterTextoParaAlunos = texto =>
    texto
      .trim()
      .split('\n')
      .map(aluno => {
        return aluno.trim()
      })

  _removerAlunosQueJaEstaoNaLista = (usuariosJaSalvos, alunos) => {
    if (!Array.isArray(usuariosJaSalvos) || !Array.isArray(alunos)) return {}

    for (let usuario of usuariosJaSalvos) {
      _.remove(alunos, aluno => aluno.email === usuario.email)
    }

    return { alunos }
  }

  verificarAlunos = async matriculaOuEmail => {
    const { handleParticipantesGerenciados, usuariosJaSalvos = [], usuariosPendentes = [] } = this.props
    const { data = {} } = await get('/usuarios/alunos-em-lote', { matriculaOuEmail })
    const { alunos = [], alunosPendentes = [] } = data
    this._removerAlunosQueJaEstaoNaLista(usuariosJaSalvos, alunos)
    this._removerAlunosQueJaEstaoNaLista(usuariosPendentes, alunosPendentes)

    handleParticipantesGerenciados({
      alunos: [...usuariosJaSalvos, ...alunos],
      alunosPendentes: [...usuariosPendentes, ...alunosPendentes],
    })
  }

  onClose = async () => {
    const { handleParticipantesGerenciados } = this.props
    if (this.props.abaAtual === 'lote') {
      await this.verificarAlunos(this.converterTextoParaAlunos(this.state.texto))
    } else handleParticipantesGerenciados({ alunos: this.state.participantesSelecionados })
  }

  handleChangeAba = novoValorAba => {
    this.props.ligarLoading()
    this.setState({ participantesDaBusca: [] })
    switch (novoValorAba) {
      case 'lote':
        this.props.desligarLoading()
        break
      case 'usuario':
        this.props.desligarLoading()
        break
      default:
        console.warn('Aba não válida')
        break
    }
  }

  buscarPorUsuario = input => {
    if (!!input) {
      get('/usuarios/usuarios-discentes', { value: input }).then(({ data = {} }) => {
        const participantesDaBusca = data.usuarios ? data.usuarios : []
        this.setState({
          participantesDaBusca,
        })
      })
    } else {
      this.setState({ participantesDaBusca: [] })
    }
  }

  handleChangeTextoUsuario = event => {
    const { value } = event.target

    clearTimeout(this.timerParaBuscaNaAPI)
    this.props.ligarLoading()
    this.setState({ valorDigitadoBuscaUsuario: value })

    this.timerParaBuscaNaAPI = setTimeout(() => {
      this.buscarPorUsuario(value)
      this.props.desligarLoading()
    }, 300)
  }
  marcarUsuario = usuario => {
    const novoAluno = {
      nome: usuario.nome,
      matricula: usuario.matricula,
      email: usuario.email,
      id: usuario.id,
      validado: usuario.validado,
    }
    this.setState(({ participantesSelecionados }) => ({
      participantesSelecionados: [...participantesSelecionados, novoAluno],
    }))
  }
  desmarcarUsuario = usuario => {
    const participantesSelecionados = this.state.participantesSelecionados.filter(
      participante => participante.id !== usuario.id,
    )
    this.setState({ participantesSelecionados })
  }
  render() {
    const { abaAtual, strings, classes } = this.props
    const { participantesDaBusca, valorDigitadoBuscaUsuario, participantesSelecionados, texto } = this.state

    return (
      <div>
        {abaAtual === 'lote' && (
          <TextField
            label={strings.digiteUmaMatriculaOuEmailPorLinha}
            rows={5}
            value={texto}
            autoFocus
            onChange={({ target: { value: texto } }) => this.setState({ texto })}
            fullWidth
            variant="outlined"
            multiline
          />
        )}
        {abaAtual === 'usuario' && (
          <React.Fragment>
            <div className={classes.buscaPorUsuariosInput}>
              <TextField
                label={strings.nomeMatriculaOuEmail}
                value={valorDigitadoBuscaUsuario}
                onChange={event => this.handleChangeTextoUsuario(event)}
                className={classes.TextField}
                autoFocus={true}
              />
            </div>
          </React.Fragment>
        )}
        <ListaUsuariosBusca
          participantes={participantesDaBusca}
          marcarUsuario={this.marcarUsuario}
          desmarcarUsuario={this.desmarcarUsuario}
          participantesSelecionados={participantesSelecionados}
        />
      </div>
    )
  }
}
