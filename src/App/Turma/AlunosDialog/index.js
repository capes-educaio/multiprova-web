import { AlunosDialogComponent } from './AlunosDialogComponent'
import { compose } from 'redux'
import { connect } from 'react-redux'

import { mapStateToProps, mapDispatchToProps } from './redux'
import { style } from './style'
import { withStyles } from '@material-ui/core/styles'
import { WithRadioTabsDialog } from 'common/WithRadioTabsDialog'

export const AlunosDialog = WithRadioTabsDialog(
  compose(
    connect(
      mapStateToProps,
      mapDispatchToProps,
      null,
      { forwardRef: true },
    ),
  )(withStyles(style)(AlunosDialogComponent)),
)
