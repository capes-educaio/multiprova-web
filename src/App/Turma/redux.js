import { bindActionCreators } from 'redux'

export function mapStateToProps(state) {
  return {
    usuarioAtual: state.usuarioAtual,
    strings: state.strings,
    formRefs: state.SimpleReduxForm__refs,
    urlParaEnviarEmail: state.urlParaEnviarEmail,
  }
}

export function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch)
}
