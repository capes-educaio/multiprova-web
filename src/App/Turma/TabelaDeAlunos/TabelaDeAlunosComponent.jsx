import React, { Component } from 'react'
import { propTypes } from './propTypes'
import classnames from 'classnames'

import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Paper from '@material-ui/core/Paper'
import Button from '@material-ui/core/Button'
import Delete from '@material-ui/icons/Delete'
import Tooltip from '@material-ui/core/Tooltip'

import { AlertaDeConfirmacao } from 'common/AlertaDeConfirmacao'
import { TurmaVazia } from '../TurmaVazia'

export class TabelaDeAlunosComponent extends Component {
  static propTypes = propTypes
  state = {
    alerta: false,
    index: 0,
  }

  _handleConfirmarExcluir = () => {
    this.props.excluirAluno(this.state.index)
  }
  _handleClickDeleteAluno = index => () => {
    this.setState({
      alerta: true,
      index,
    })
  }
  _handleCloseAlerta = () => this.setState({ alerta: false })

  _handleCloseExcluir = confirmaCancelar => {
    if (confirmaCancelar === true) this._handleConfirmarExcluir()
    this._handleCloseAlerta()
  }

  render() {
    const { valor, classes, strings } = this.props
    const { alerta } = this.state
    const turmaVazia = valor.length
    return (
      <div>
        {turmaVazia ? (
          <Paper className={classes.root}>
            <Table className={classes.table}>
              <TableHead>
                <TableRow>
                  <TableCell>{strings.matricula}</TableCell>
                  <TableCell>{strings.nome}</TableCell>
                  <TableCell>{strings.email}</TableCell>
                  <TableCell>{'  '}</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {valor.map((aluno, index) => (
                  <TableRow key={index}>
                    <TableCell className={classnames({ [classes.naoValidado]: !aluno.validado })}>
                      {aluno.matricula ? aluno.matricula : '-'}
                    </TableCell>
                    <TableCell className={classnames({ [classes.naoValidado]: !aluno.validado })}>
                      {aluno.nome}
                    </TableCell>
                    <TableCell className={classnames({ [classes.naoValidado]: !aluno.validado })}>
                      {aluno.email}
                    </TableCell>
                    <TableCell className={classes.alignRight}>
                      <Tooltip title={strings.removerAluno}>
                        <Button onClick={this._handleClickDeleteAluno(index)}>
                          <Delete />
                        </Button>
                      </Tooltip>
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
            <AlertaDeConfirmacao
              alertaAberto={alerta}
              stringNomeInstancia={strings.removerAluno}
              descricaoSelected={strings.confirmarExclusao}
              handleClose={this._handleCloseExcluir}
            />
          </Paper>
        ) : (
          <TurmaVazia />
        )}
      </div>
    )
  }
}
