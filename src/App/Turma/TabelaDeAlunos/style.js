export const style = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
  },
  alignRight: {
    textAlign: 'right',
  },
  naoValidado: {
    color: theme.palette.default.medium,
  },
})
