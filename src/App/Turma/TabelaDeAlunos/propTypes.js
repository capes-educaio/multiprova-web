import PropTypes from 'prop-types'

export const propTypes = {
  valor: PropTypes.array.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
