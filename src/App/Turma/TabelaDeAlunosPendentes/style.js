export const style = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
  },
  tableHead: {},
  button: {
    textAlign: 'right',
    minWidth: 40,
  },
  naoValidado: {
    color: theme.palette.default.medium,
  },
})
