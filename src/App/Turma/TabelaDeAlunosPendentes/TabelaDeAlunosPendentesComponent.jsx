import Button from '@material-ui/core/Button'
import Paper from '@material-ui/core/Paper'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Tooltip from '@material-ui/core/Tooltip'
import Delete from '@material-ui/icons/Delete'
import PersonAdd from '@material-ui/icons/PersonAdd'
import PersonAddDisabled from '@material-ui/icons/PersonAddDisabled'
import classnames from 'classnames'
import React, { Component } from 'react'
import { FormAluno } from '../FormAluno'
import { propTypes } from './propTypes'

export class TabelaDeAlunosPendentesComponent extends Component {
  static propTypes = propTypes

  state = {
    formAlunoAbertos: [],
  }

  componentDidMount() {
    this.setState({
      formAlunoAbertos: this.props.valor.map(() => false),
    })
  }

  abrirFormDeAluno = index => {
    const formAlunoAbertos = [...this.state.formAlunoAbertos]
    formAlunoAbertos[index] = !formAlunoAbertos[index]
    this.setState({ formAlunoAbertos })
  }
  handleCriarAluno = (index, ...args) => {
    const { excluirAlunoPendente, salvarAluno } = this.props
    const formAlunoAbertos = [...this.state.formAlunoAbertos]
    formAlunoAbertos[index] = false
    this.setState({ formAlunoAbertos })
    excluirAlunoPendente(index)
    salvarAluno(...args)
  }

  render() {
    const { valor, classes, strings, excluirAlunoPendente } = this.props
    const { formAlunoAbertos } = this.state
    const turmaTemAlunosPendentes = valor.length
    return (
      <React.Fragment>
        {turmaTemAlunosPendentes ? (
          <Paper className={classes.root}>
            <Table className={classes.table}>
              <TableHead>
                <TableRow className={classes.tableHeadRow}>
                  <TableCell>{strings.matricula}</TableCell>
                  <TableCell>{strings.email}</TableCell>
                  <TableCell> {'  '} </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {valor.map((aluno, index) => (
                  <React.Fragment key={`alunoPendente${index}`}>
                    <TableRow>
                      <TableCell className={classnames({ [classes.naoValidado]: !aluno.validado })}>
                        {aluno.matricula ? aluno.matricula : '-'}
                      </TableCell>
                      <TableCell className={classnames({ [classes.naoValidado]: !aluno.validado })}>
                        {aluno.email ? aluno.email : '-'}
                      </TableCell>
                      <TableCell className={classes.button}>
                        <Tooltip
                          title={formAlunoAbertos[index] ? strings.cancelarCadastroDeUsuario : strings.cadastrarUsuario}
                        >
                          <Button onClick={() => this.abrirFormDeAluno(index)}>
                            {formAlunoAbertos[index] ? <PersonAddDisabled /> : <PersonAdd />}
                          </Button>
                        </Tooltip>
                        <Tooltip title={strings.removerAluno}>
                          <Button onClick={() => excluirAlunoPendente(index)}>
                            <Delete />
                          </Button>
                        </Tooltip>
                      </TableCell>
                    </TableRow>
                    {formAlunoAbertos[index] && (
                      <TableRow>
                        <TableCell colSpan={3}>
                          <FormAluno
                            salvarAluno={args => this.handleCriarAluno(index, args)}
                            id={index}
                            matricula={aluno.matricula}
                            email={aluno.email}
                          />
                        </TableCell>
                      </TableRow>
                    )}
                  </React.Fragment>
                ))}
              </TableBody>
            </Table>
          </Paper>
        ) : null}
      </React.Fragment>
    )
  }
}
