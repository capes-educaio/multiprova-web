import React, { Component } from 'react'
import { Switch, Route, Redirect } from 'react-router'

import { validadeUrlState } from 'localModules/urlState'
import { openDialogAviso } from 'utils/compositeActions/openDialogAviso'

import { Dialog } from 'common/Dialog'
import { UrlProvider } from 'common/UrlProvider'
import { Snackbar } from 'utils/Snackbar'
import { StringsContext } from 'utils/strings/context'

import { redirecionamentoDeRotas } from './redirecionamentoDeRotas'
import { propTypes } from './propTypes'
import { Display } from './Display'
import { Login } from './Login'
import { ResgatarSenha } from './ResgatarSenha'
import { EsqueceuASenha } from './EsqueceuASenha'
import { MenuEsquerdo } from './MenuEsquerdo'
import { PaginaNaoEncontrada } from './PaginaNaoEncontrada'
import { dispatchToStore } from 'utils/compositeActions'

import { defaultProps } from './defaultProps'
import {
  rotasAdmin,
  rotasDocente,
  rotasGestor,
  rotasProvasSemDisplay,
  rotasDiscenteSemDisplay,
  rotasDiscente,
} from './rotasRoot'

import { enumThemes } from 'localModules/theme'
import { MuiThemeProvider } from '@material-ui/core/styles'
import { theme } from 'utils/theme'
import { contrastTheme } from 'utils/contrastTheme'

export class AppComponent extends Component {
  static propTypes = propTypes
  static defaultProps = defaultProps
  isUrlStateValid

  constructor(props) {
    super(props)
    document.title = props.strings.nomeProjeto
    const { usuarioAtual, history, resetSimpleStates } = props
    resetSimpleStates()
    redirecionamentoDeRotas(usuarioAtual, history)
    this.isUrlStateValid = validadeUrlState(history)

    window.addEventListener('resize', this.resize.bind(this))
    this.resize()
  }

  resize() {
    dispatchToStore('select', 'isMobile', this.isMobile)
  }

  get isMobile() {
    return window.innerWidth <= 760
  }

  get _userHomePath() {
    const { usuarioAtual, homePath, strings } = this.props
    if (usuarioAtual.isDocente()) return homePath.docente
    else if (usuarioAtual.isDiscente()) return homePath.discente
    else if (usuarioAtual.isGestor()) return homePath.gestor
    else if (usuarioAtual.isAdmin()) return homePath.admin
    else {
      openDialogAviso({
        texto: `${strings.falhaInesperada} id: 4b14c27c-3c7a-41df-a836-277aa3652ca9`,
        titulo: strings.erro,
      })
      return null
    }
  }

  selectTheme = () => {
    const { themeChosen } = this.props
    switch (themeChosen) {
      case enumThemes.light:
        return theme
      case enumThemes.highContrast:
        return contrastTheme
      default:
        return theme
    }
  }

  UNSAFE_componentWillReceiveProps = novosProps => {
    const { history } = this.props
    redirecionamentoDeRotas(novosProps.usuarioAtual, history, novosProps.history)
    this.isUrlStateValid = validadeUrlState(history)
  }

  render() {
    const { classes, usuarioAtual, modulosHabilitados, messages, strings, location } = this.props
    const hideDialog =
      location.pathname.split('/')[1] === 'prova' &&
      location.pathname.split('/')[2] !== 'aplicar-edit' &&
      location.pathname.split('/')[2] !== 'aplicar'

    if (!this.isUrlStateValid) return <div />
    return (
      <MuiThemeProvider theme={this.selectTheme()}>
        <StringsContext.Provider value={strings}>
          <UrlProvider>
            <div className={classes.app}>
              {usuarioAtual && modulosHabilitados.menuNavegacao && <MenuEsquerdo />}
              <Switch>
                {usuarioAtual && usuarioAtual.isDiscente() && rotasDiscenteSemDisplay()}
                {usuarioAtual && usuarioAtual.isDocente() && rotasProvasSemDisplay()}
                <Route exact path="/login" component={Login} />
                <Route exact path="/esqueci-a-senha" component={EsqueceuASenha} />
                <Route exact path="/resgatar/:hashSenha" component={ResgatarSenha} />
                <Route exact path="/resgatar/" component={ResgatarSenha} />
                {usuarioAtual && (
                  <Display>
                    <div>
                      <Switch>
                        <Route exact path="/" render={() => <Redirect to={this._userHomePath} />} />
                        {usuarioAtual.isDocente() && rotasDocente()}
                        {usuarioAtual.isDiscente() && rotasDiscente()}
                        {usuarioAtual.isAdmin() && rotasAdmin()}
                        {usuarioAtual.isGestor() && rotasGestor()}
                        <Route path="/" component={PaginaNaoEncontrada} />
                      </Switch>
                    </div>
                  </Display>
                )}
              </Switch>
              {hideDialog || <Dialog />}
              <Snackbar
                open={messages.length > 0 ? true : false}
                message={messages.length > 0 ? messages[0].message : undefined}
                variant={messages.length > 0 ? messages[0].variant : undefined}
                onClose={this.props.removeMessage}
              />
            </div>
          </UrlProvider>
        </StringsContext.Provider>
      </MuiThemeProvider>
    )
  }
}
