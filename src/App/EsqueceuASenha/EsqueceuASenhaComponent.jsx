import React, { Component } from 'react'

import { Paper, Typography, Button, FormHelperText } from '@material-ui/core'

import { post } from 'api'

import { showMessage } from 'utils/Snackbar'
import { EsqueceuASenhaForm } from './EsqueceuASenhaForm'
import { propTypes } from './propTypes'

export class EsqueceuASenhaComponent extends Component {
  static propTypes = propTypes

  constructor(props) {
    super(props)
    this.state = {
      value: {
        email: '',
      },
      isValid: false,
      infoText: '',
      mostrarErroNoForm: false,
    }
  }

  _handleSubmit = () => {
    const { strings } = this.props
    const { isValid, value } = this.state
    const redirect_url = window.location.origin + '/resgatar'
    if (isValid) {
      post('/usuarios/forgot-password', { ...value, redirect_url })
        .then(() => {
          this.setState({ mostrarErroNoForm: false, infoText: '' })
          showMessage.success({ message: strings.emailComRedefinicaoEnviado })
        })
        .catch(({ response = {} }) => {
          const { data = {} } = response
          const { error = {} } = data
          const { name } = error
          this.setState({ mostrarErroNoForm: true, infoText: strings.erros[name] })
        })
    } else this._mostrarErros()
  }

  _onValid = isValid => this.setState({ isValid })

  _onChange = value => this.setState({ value })

  _mostrarErros = () => {
    const { formRefs } = this.props
    if (formRefs.formLogin && formRefs.formLogin.showError) formRefs.formLogin.showError()
    else console.warn('Algum problema ao buscar a ref do form de login.')
  }

  render() {
    const { classes, strings, history } = this.props
    const { isValid, mostrarErroNoForm, infoText } = this.state
    return (
      <div className={classes.background}>
        <div className={classes.loginContainerOut}>
          <div>
            <div className={classes.formBox} dir="x">
              <div className={classes.divMenor}>{strings.nomeProjeto}</div>
              <Paper className={classes.paperPadding} elevation={2}>
                <div className={classes.helperText}>
                  <FormHelperText error={Boolean(mostrarErroNoForm)} tabIndex="1">
                    {mostrarErroNoForm ? infoText : strings.legendaFormEsqueciSenha}
                  </FormHelperText>
                </div>
                <EsqueceuASenhaForm
                  title={strings.titleFormRedefinirSenha}
                  valor={this.state.value}
                  onValid={this._onValid}
                  onChange={this._onChange}
                  handleLogin={this._handleSubmit}
                />
                <div className={classes.buttonContainer}>
                  <Button
                    className={classes.button}
                    color="primary"
                    onClick={isValid ? this._handleSubmit : this._mostrarErros}
                  >
                    {strings.resgatar}
                  </Button>
                </div>
                <div className={classes.link}>
                  <a className={classes.linkA} href="#login" onClick={() => history.push('/login')}>
                    <Typography align="center">{strings.irParaLogin}</Typography>
                  </a>
                </div>
              </Paper>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
