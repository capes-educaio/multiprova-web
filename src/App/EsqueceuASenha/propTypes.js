import PropTypes from 'prop-types'

export const propTypes = {
  match: PropTypes.object.isRequired,
  // redux state
  formRefs: PropTypes.object.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
