import React, { Component } from 'react'

import { usuarioProps } from 'localModules/multiprovaEntidades'

import InputAdornment from '@material-ui/core/InputAdornment'
import EmailIcon from '@material-ui/icons/Email'

import { SimpleReduxForm, CampoText } from 'localModules/ReduxForm'

import { propTypes } from './propTypes'

export class EsqueceuASenhaFormComponent extends Component {
  static propTypes = propTypes

  render() {
    const { strings, valor, onChange, onValid, classes, handleLogin, title } = this.props
    return (
      <SimpleReduxForm id="formEsqueceuASenha" valor={valor} onChange={onChange} onValid={onValid} title={title}>
        <CampoText
          id="formEsqueceuASenha__email"
          accessor="email"
          label={strings.email}
          required
          validar={usuarioProps.email.validar}
          onEnter={handleLogin}
          textFieldProps={{
            autoComplete: 'email',
            fullWidth: true,
            InputProps: {
              endAdornment: (
                <InputAdornment position="end" className={classes.emailAdornment}>
                  <EmailIcon className={classes.icon} />
                </InputAdornment>
              ),
            },
          }}
          mensagensDeErro={{
            email_invalido: 'Email inválido.',
          }}
        />
      </SimpleReduxForm>
    )
  }
}
