export const style = theme => ({
  emailAdornment: { color: 'rgba(0, 0, 0, 0.5)' },
  icon: {
    height: 20,
  },
  iconButton: {
    color: 'rgba(0, 0, 0, 0.5)',
    '& :hover': {
      color: 'rgba(0, 0, 0, 1)',
    },
  },
})
