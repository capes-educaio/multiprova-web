import React from 'react'
import { Route } from 'react-router-dom'

import { appConfig } from 'appConfig'

import { ProvaEditar } from 'localModules/provas/ProvaEditar'
import { ProvaVer } from 'localModules/provas/ProvaVer'
import { ProvaAplicacaoCriar } from 'localModules/provas/ProvaAplicacaoCriar'
import { ProvaAplicacaoEdit } from 'localModules/provas/ProvaAplicacaoEdit'

import { rotasTag } from 'localModules/TagAssets/rotasTag'

import { Questao } from 'localModules/questoes/Questao'
import { CriarUsuario, CriarAlunosEmLote } from './CriarUsuario'
import { GerenciamentoQuestoes } from 'localModules/questoes/GerenciamentoQuestoes'
import { GerenciamentoProvas } from 'localModules/provas/GerenciamentoProvas'
import { Eventos } from './Eventos'
import { Lixeira } from './Lixeira'
import { GerenciamentoProvasDiscentes } from 'localModules/provas/GerenciamentoProvasDiscentes'
import { ResolucaoProva } from './ResolucaoProva'
import { GerenciamentoTurmas } from './GerenciamentoTurmas'
import { GerenciamentoUsuario } from './GerenciamentoUsuario'
import { Turma } from './Turma'
import { PainelDaProva } from './PainelDaProva'
import { PainelCorrecao } from './PainelDaProva/PainelCorrecao'
import { DashboardGestor } from './DashboardGestor'
import { DashboardDocente } from './DashboardDocente'
import { DashboardDiscente } from './DashboardDiscente'
import { AlterarSenha } from './BarraSuperior/AlterarSenha'
import { GerenciamentoTags } from './GerenciamentoTags'
import { SelecaoPerfil } from './SelecaoPerfil'
import { EstatisticasPage } from './Estatisticas'

const rotasQuestoes = () => {
  if (appConfig.modulosHabilitados.questoes)
    return [
      <Route exact path="/questoes" component={GerenciamentoQuestoes} key="GerenciamentoQuestoes" tabindex="0" />,
      <Route
        exact
        path="/questoes/minhas"
        component={GerenciamentoQuestoes}
        key="GerenciamentoQuestoes"
        tabindex="0"
      />,
      <Route
        exact
        path="/questoes/publicas"
        component={GerenciamentoQuestoes}
        key="GerenciamentoQuestoes"
        tabindex="0"
      />,
      <Route
        exact
        path="/questoes/compartilhadas"
        component={GerenciamentoQuestoes}
        key="GerenciamentoQuestoes"
        tabindex="0"
      />,
      <Route exact path="/questao/:id" component={Questao} key="QuestaoEdicao" tabindex="0" />,
      <Route exact path="/questao/compartilhada/:id" component={Questao} key="QuestaoEdicao" tabindex="0" />,
      <Route exact path="/questao" component={Questao} key="QuestaoCriacao" tabindex="0" />,
    ]
  else return []
}

const rotasProvas = () => {
  if (appConfig.modulosHabilitados.provas)
    return [
      <Route exact path="/prova/:id/painel" component={PainelDaProva} key="PainelDaProva" tabindex="0" />,
      <Route exact path="/prova" component={ProvaEditar} key="ProvaCriacao" tabindex="0" />,
      <Route exact path="/prova/:id" component={ProvaEditar} key="ProvaEdicao" tabindex="0" />,
      <Route exact path="/prova/view/:id" component={ProvaVer} key="ProvaVer" tabindex="0" />,
      <Route exact path="/prova/aplicar/:id" component={ProvaAplicacaoCriar} key="ProvaAplicacaoCriar" tabindex="0" />,
      <Route
        exact
        path="/prova/aplicar-edit/:id"
        component={ProvaAplicacaoEdit}
        key="ProvaAplicacaoEdit"
        tabindex="0"
      />,
      <Route exact path="/provas" component={GerenciamentoProvas} key="GerenciamentoProvas" tabindex="0" />,
    ]
  else return []
}

export const rotasProvasSemDisplay = () => {
  if (appConfig.modulosHabilitados.provas) {
    return [
      <Route
        exact
        path="/prova/:id/correcao/:indexParticipante/:indexQuestao"
        component={PainelCorrecao}
        key="PainelCorrecao"
        tabindex="0"
      />,
    ]
  }
  return []
}

const rotasTurmas = () => {
  if (appConfig.modulosHabilitados.turmas)
    return [
      <Route exact path="/turmas" component={GerenciamentoTurmas} key="GerenciamentoTurmas" tabindex="0" />,
      <Route exact path="/turma" component={Turma} key="TurmaCriacao" tabindex="0" />,
      <Route exact path="/turma/:id" component={Turma} key="TurmaEdicao" tabindex="0" />,
    ]
  else return []
}

const rotasGerenciamentoUsuario = () => {
  if (appConfig.modulosHabilitados.gerenciamentoUsuarios)
    return [
      <Route exact path="/usuarios" component={GerenciamentoUsuario} key="GerenciamentoUsuario" />,
      <Route exact path="/usuario/:id" component={CriarUsuario} key="CriarUsuario" />,
      <Route exact path="/discentes" component={CriarAlunosEmLote} key="CriarAlunosEmLote" />,
    ]
  else return []
}

const rotasEventos = () => {
  if (appConfig.modulosHabilitados.eventos)
    return [<Route exact path="/eventos" component={Eventos} key="Eventos" tabindex="0" />]
  else return []
}

const rotasProvasDiscente = () => {
  if (appConfig.modulosHabilitados.provasDiscente)
    return [
      <Route
        exact
        path="/provas"
        component={GerenciamentoProvasDiscentes}
        key="GerenciamentoProvasDiscentes"
        tabindex="0"
      />,
      <Route
        exact
        path="/provas/abertas"
        component={GerenciamentoProvasDiscentes}
        key="GerenciamentoProvasDiscentes"
        tabindex="0"
      />,
      <Route
        exact
        path="/provas/agendadas"
        component={GerenciamentoProvasDiscentes}
        key="GerenciamentoProvasDiscentes"
        tabindex="0"
      />,
      <Route
        exact
        path="/provas/finalizadas"
        component={GerenciamentoProvasDiscentes}
        key="GerenciamentoProvasDiscentes"
        tabindex="0"
      />,
    ]
  else return []
}

const rotasProvasDiscenteSemDisplay = () => {
  if (appConfig.modulosHabilitados.provasDiscente)
    return [
      <Route exact path="/prova/:id" component={ResolucaoProva} key="ResolucaoProva" tabindex="0" />,
      <Route exact path="/prova/:id/estatisticas" component={EstatisticasPage} key="EstatisticasPage" tabindex="0" />,
    ]
  else return []
}

const rotasLixeira = () => {
  if (appConfig.modulosHabilitados.lixeira)
    return [<Route exact path="/lixeira/:tipoLixeira" component={Lixeira} key="Lixeira" tabindex="0" />]
  else return []
}
const rotasTags = () => {
  if (appConfig.modulosHabilitados.tags)
    return [<Route exact path={rotasTag().front.gerenciamento} component={GerenciamentoTags} key="Tags" />]
  else return []
}
const rotasMudarPerfil = () => {
  if (appConfig.modulosHabilitados.mudarPerfil)
    return [<Route exact path={'/mudar-perfil'} component={SelecaoPerfil} key="mudar-perfil" />]
  else return []
}
const rotasDashoboardDocente = () => {
  if (appConfig.modulosHabilitados.dashboard)
    return [<Route exact path="/dashboard" component={DashboardDocente} key="DashboardDocente" tabindex="0" />]
  else return []
}
const rotasDashoboardDiscente = () => {
  if (appConfig.modulosHabilitados.dashboard)
    return [<Route exact path="/dashboard" component={DashboardDiscente} key="DashboardDiscente" tabindex="0" />]
  else return []
}
const rotasDashoboardGestor = () => {
  if (appConfig.modulosHabilitados.dashboard)
    return [<Route exact path="/dashboard" component={DashboardGestor} key="DashboardGestor" tabindex="0" />]
  else return []
}

const rotasAlterarSenha = () => {
  if (appConfig.modulosHabilitados.alterarSenha)
    return [<Route exact path="/alterar-senha" component={AlterarSenha} key="AlterarSenha" />]
  else return []
}

export const rotasGestor = () => [...rotasDashoboardGestor(), ...rotasAlterarSenha(), ...rotasMudarPerfil()]

export const rotasDiscente = () => [
  ...rotasDashoboardDiscente(),
  ...rotasProvasDiscente(),
  ...rotasAlterarSenha(),
  ...rotasMudarPerfil(),
]

export const rotasDiscenteSemDisplay = () => rotasProvasDiscenteSemDisplay()

export const rotasDocente = () => [
  ...rotasDashoboardDocente(),
  ...rotasQuestoes(),
  ...rotasProvas(),
  ...rotasTurmas(),
  ...rotasLixeira(),
  ...rotasAlterarSenha(),
  ...rotasTags(),
  ...rotasMudarPerfil(),
]

export const rotasAdmin = () => [
  ...rotasGerenciamentoUsuario(),
  ...rotasEventos(),
  ...rotasAlterarSenha(),
  ...rotasMudarPerfil(),
]
