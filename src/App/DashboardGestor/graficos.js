import { theme } from 'utils/theme.js'

export const provas = {
  option: ({ dados, strings, isMobile }) => {
    return {
      title: {
        text: strings.provasAplicadasNoUltimos12Meses,
        left: 'center',
        textStyle: {
          fontSize: isMobile ? 12 : 19,
          fontWeight: '100',
        },
      },
      toolbox: {
        feature: {
          saveAsImage: { show: true, title: strings.salvarComoImagem },
        },
        left: '35px',
        type: 'png',
        top: isMobile ? 'auto' : '7%',
      },
      xAxis: {
        type: 'category',
        text: 'label',
        data: provas.dataAxisX(),
        axisLabel: {
          rotate: 40,
          margin: 10,
          show: true,
        },
      },
      yAxis: {
        type: 'value',
        maxInterval: 5,
      },
      tooltip: {
        trigger: 'item',
        formatter: '{b} : {c}',
      },
      series: [
        {
          data: provas.dataAxisY(dados),
          type: 'bar',
          itemStyle: {
            emphasis: {
              shadowBlur: 10,
              shadowOffsetX: 0,
              shadowColor: theme.palette.default.dark,
            },
            color: theme.palette.secondary.dark,
          },
        },
      ],
    }
  },
  dataAxisX: () => {
    const hoje = new Date()
    const months = []
    const m = Number(hoje.getMonth()) + 13

    for (let index = 0; index < 12; index++) {
      const indexFinal = (index + m) % 12
      months.push(MONTHS[indexFinal])
    }
    return months
  },
  dataAxisY: provas => {
    const _dados = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    for (let prova of provas) {
      const dataUltimaAlteracao = new Date(prova.dataUltimaAlteracao)
      _dados[Number(dataUltimaAlteracao.getMonth())]++
    }
    const hoje = new Date()
    const dados = []
    const m = Number(hoje.getMonth()) + 13

    for (let index = 0; index < 12; index++) {
      const indexFinal = (index + m) % 12
      dados.push(_dados[indexFinal])
    }
    return dados
  },
}

export const questoes = {
  option: ({ dados, strings, isMobile }) => {
    const data = questoes.data({ dados, strings })
    return {
      title: {
        text: strings.quantidadeDeQuestoes,
        x: 'center',
        textStyle: {
          fontSize: isMobile ? 12 : 20,
          fontWeight: '100',
        },
      },
      toolbox: {
        feature: {
          saveAsImage: { show: true, title: strings.salvarComoImagem },
        },
        left: '35px',
        type: 'png',
      },
      tooltip: {
        trigger: 'item',
        formatter: '{a} <br/>{b} : {c} ({d}%)',
      },
      legend: {
        type: 'scroll',
        orient: isMobile ? 'horizontal' : 'vertical',
        left: isMobile ? '1%' : 'auto',
        right: isMobile ? '1%' : '10%',
        top: isMobile ? 'auto' : '25%',
        bottom: isMobile ? '5%' : 'auto',
        data: data.legendData,
        selected: data.selected,
        padding: 10,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: theme.palette.grafico.grey.main,
      },
      series: [
        {
          name: strings.quantidadeDeQuestoes,
          type: 'pie',
          radius: isMobile ? '50%' : '55%',
          center: isMobile ? ['50%', '45%'] : ['40%', '50%'],
          data: data.seriesData,
          label: {
            formatter: values => {
              return values.data.name.split(' ').join('\n')
            },
          },

          itemStyle: {
            emphasis: {
              shadowBlur: 10,
              shadowOffsetX: 0,
              shadowColor: theme.palette.default.dark,
            },
          },
        },
      ],
    }
  },
  data: ({ dados, strings }) => {
    var legendData = [
      strings.vouf,
      strings.associacaoDeColunas,
      strings.multiplaEscolha,
      strings.redacao,
      strings.bloco,
      strings.discursiva,
    ]
    var seriesData = [
      {
        name: strings.vouf,
        value: dados.vouf,
        itemStyle: {
          color: theme.palette.defaultGrey,
        },
      },
      {
        name: strings.associacaoDeColunas,
        value: dados.associacaoDeColunas,
        itemStyle: {
          color: theme.palette.secondary.main,
        },
      },
      {
        name: strings.multiplaEscolha,
        value: dados.multiplaEscolha,
        itemStyle: {
          color: theme.palette.secondary.dark,
        },
      },
      {
        name: strings.redacao,
        value: dados.redacao,
        itemStyle: {
          color: theme.palette.default.light,
        },
      },
      {
        name: strings.bloco,
        value: dados.bloco,
        itemStyle: {
          color: theme.palette.default.main,
        },
      },
      {
        name: strings.discursiva,
        value: dados.discursiva,
        itemStyle: {
          color: theme.palette.default.dark,
        },
      },
    ]
    var selected = {
      vouf: true,
      associacaoDeColunas: true,
      multiplaEscolha: true,
      redacao: true,
      bloco: true,
      dicursiva: true,
    }

    return {
      legendData: legendData,
      seriesData: seriesData,
      selected: selected,
    }
  },
}

export const provasAplicadas = {
  option: ({ dados, strings, isMobile }) => {
    let dadosFinais = _countAsProvasDeCadaMes(dados)
    dadosFinais = _countAcumulativoDasProvas(dadosFinais)
    return {
      title: {
        text: strings.provasAplicadas,
        left: 'center',
        textStyle: {
          fontSize: isMobile ? 12 : 19,
          fontWeight: '100',
        },
      },
      xAxis: {
        type: 'category',
        text: 'label',
        data: provasAplicadas.dataAxisX(dadosFinais),
        axisLabel: {
          rotate: 40,
          margin: 10,
          show: true,
        },
      },
      yAxis: {
        type: 'value',
        maxInterval: 10,
      },
      toolbox: {
        feature: {
          saveAsImage: { show: true, title: strings.salvarComoImagem },
        },
        left: '35px',
        type: 'png',
      },
      tooltip: {
        trigger: 'item',
        formatter: '{b} : {c}',
      },
      series: [
        {
          data: provasAplicadas.dataAxixY(dadosFinais),
          type: 'line',
          itemStyle: {
            emphasis: {
              shadowBlur: 10,
              shadowOffsetX: 0,
              shadowColor: theme.palette.default.dark,
            },
            color: theme.palette.primary.dark,
          },
        },
      ],
    }
  },
  dataAxisX: dados => {
    const labels = []
    for (let dado of dados) labels.push(dado.mes)
    return labels
  },
  dataAxixY: dados => {
    const valor = []
    for (let dado of dados) valor.push(dado.valor)
    return valor
  },
}

const _ordenaOArray = meses => {
  const hoje = new Date()
  const dados = []
  const m = Number(hoje.getMonth()) + 13

  for (let index = 0; index < 12; index++) {
    const indexFinal = (index + m) % 12
    dados.push({ mes: MONTHS[indexFinal], valor: meses[indexFinal] })
  }
  return dados
}

const _countAcumulativoDasProvas = dados => {
  const arrayOrdenado = _ordenaOArray(dados)
  for (let i = 0; i < arrayOrdenado.length; i++)
    arrayOrdenado[i].valor = i !== 0 ? arrayOrdenado[i - 1].valor + arrayOrdenado[i].valor : arrayOrdenado[i].valor
  return arrayOrdenado
}

const _countAsProvasDeCadaMes = provas => {
  const dados = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  for (let prova of provas) {
    const dataUltimaAlteracao = new Date(prova.dataUltimaAlteracao)
    dados[Number(dataUltimaAlteracao.getMonth())]++
  }
  return dados
}

const MONTHS = ['Jan', 'Fev', 'Mar', 'Abr', 'Maio', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez']
