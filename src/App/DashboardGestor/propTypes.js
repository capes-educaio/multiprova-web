import PropTypes from 'prop-types'

// const { objectOf, shape, string, func } = PropTypes

export const propTypes = {
  browser: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
}
