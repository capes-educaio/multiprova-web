import React, { Component } from 'react'

import { provaUtils } from 'localModules/provas/ProvaUtils'

import { propTypes } from './propTypes'
import { defaultProps } from './defaultProps'

import { provas, provasAplicadas, questoes } from './graficos'

import { Dashboard } from 'common/Dashboard'

export class DashboardGestorComponent extends Component {
  static propTypes = propTypes
  static defaultProps = defaultProps

  constructor(props) {
    super(props)
    this.state = {
      carregando: true,
      estatisticas: {
        provas: [],
      },
      cardData: [],
      graphData: [],
    }
  }

  setDashboardData = () => {
    const { estatisticas } = this.state
    const { strings, classes, browser } = this.props
    const isMobile = browser.lessThan.medium

    const cardData = [
      {
        gridXs: isMobile ? 6 : 4,
        titulo: strings.alunos,
        value: estatisticas.discentes,
        classes,
      },
      {
        gridXs: isMobile ? 6 : 4,
        titulo: strings.professores,
        value: estatisticas.docentes,
        classes,
      },
      {
        gridXs: isMobile ? 6 : 4,
        titulo: strings.qteTurmas,
        value: estatisticas.turmas,
        classes,
      },
      {
        gridXs: isMobile ? 6 : 4,
        titulo: strings.qteQuestoes,
        value: estatisticas.totalDeQuestoes,
        classes,
      },
      {
        gridXs: isMobile ? 6 : 4,
        titulo: strings.qteAvaliacoes,
        value: estatisticas.totalDeProvas,
        classes,
      },
      {
        gridXs: isMobile ? 6 : 4,
        titulo: strings.qteProvasAplicadas,
        value: estatisticas.provas.length,
        classes,
      },
    ]

    const graphData = [
      {
        gridXs: 12,
        option: questoes.option({ dados: estatisticas, strings, isMobile }),
      },
      {
        gridXs: isMobile ? 12 : 6,
        option: provas.option({ dados: estatisticas.provas, strings, isMobile }),
      },
      {
        gridXs: isMobile ? 12 : 6,
        option: provasAplicadas.option({ dados: estatisticas.provas, strings, isMobile }),
      },
    ]

    this.setState({ cardData, graphData })
  }

  componentDidMount = () => {
    var p1 = provaUtils
      .estatisticasAcademicas()
      .then(res => {
        this.setState({ estatisticas: { ...this.state.estatisticas, ...res.data } })
      })
      .catch(err => {
        console.warn(err)
      })

    var p2 = provaUtils
      .estatisticasProvasAplicadas()
      .then(res => {
        this.setState({ estatisticas: { ...this.state.estatisticas, ...res.data } })
      })
      .catch(err => {
        console.warn(err)
      })

    var p3 = provaUtils
      .estatisticasQuestoes()
      .then(res => {
        this.setState({ estatisticas: { ...this.state.estatisticas, ...res.data } })
      })
      .catch(err => {
        console.warn(err)
      })

    Promise.all([p1, p2, p3]).then(() => {
      this.setDashboardData()
      this.setState({ carregando: false })
    })
  }

  render() {
    const { carregando, cardData, graphData } = this.state
    return <Dashboard loading={carregando} cardData={cardData} graphData={graphData} />
  }
}
