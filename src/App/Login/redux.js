import { bindActionCreators } from 'redux'
// import { actions } from 'utils/actions'

export function mapStateToProps(state) {
  return {
    strings: state.strings,
    formRefs: state.SimpleReduxForm__refs,
  }
}

export function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch)
}
