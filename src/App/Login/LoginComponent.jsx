import React, { Component } from 'react'

import Paper from '@material-ui/core/Paper'
import Button from '@material-ui/core/Button'
import FormHelperText from '@material-ui/core/FormHelperText'

import { post, get } from 'api'

import { logar } from 'utils/compositeActions'

import { LoginForm } from './LoginForm'
import { propTypes } from './propTypes'
import { Typography } from '@material-ui/core'

export class LoginComponent extends Component {
  static propTypes = propTypes

  constructor(props) {
    super(props)
    this.state = {
      value: {
        email: '',
        password: '',
      },
      isValid: false,
      erroLoginText: '',
      mostrarErroLogin: false,
    }
  }

  _handleLogin = () => {
    const { strings } = this.props
    const { isValid } = this.state
    let authToken, userData
    if (isValid) {
      post('/usuarios/login', this.state.value)
        .then(response => {
          authToken = response.data
          return get('/usuarios/' + authToken.userId, null, authToken.id)
        })
        .then(response => {
          userData = response.data
          logar({ userData, authToken, redirecionar: true })
        })
        .catch(error => {
          const { response } = error
          const data = response ? response.data : null
          const dataError = data ? data.error : null
          const code = dataError ? dataError.code : null
          const mostrarErroLogin = true
          const erroLoginText = strings.erros[code]
          this.setState({ mostrarErroLogin, erroLoginText })
        })
    } else this._mostrarErros()
  }

  _onValid = isValid => this.setState({ isValid })

  _onChange = value => this.setState({ value })

  _mostrarErros = () => {
    const { formRefs } = this.props
    if (formRefs.formLogin && formRefs.formLogin.showError) formRefs.formLogin.showError()
    else console.warn('Algum problema ao buscar a ref do form de login.')
  }

  render() {
    const { classes, strings, history } = this.props
    const { isValid, mostrarErroLogin, erroLoginText } = this.state
    return (
      <div className={classes.background}>
        <div className={classes.loginContainerOut}>
          <div>
            <div className={classes.formBox} dir="x">
              <div className={classes.divMenor} tabIndex="1">{strings.nomeProjeto}</div>
              <Paper className={classes.paperPadding} elevation={2}>
                <div className={classes.helperText}>
                  <FormHelperText error={Boolean(mostrarErroLogin)} tabIndex="2">
                    {mostrarErroLogin ? erroLoginText : strings.acesseMultiprova}
                  </FormHelperText>
                </div>
                <LoginForm
                  title={strings.titleFormLogin}
                  valor={this.state.value}
                  onValid={this._onValid}
                  onChange={this._onChange}
                  handleLogin={this._handleLogin}
                />
                <div className={classes.buttonContainer}>
                  <Button
                    className={classes.button}
                    id="entrar"
                    color="primary"
                    onClick={isValid ? this._handleLogin : this._mostrarErros}
                  >
                    {strings.entrar}
                  </Button>
                </div>
                <div className={classes.link}>
                  <a className={classes.linkA} href="#esqueci-a-senha" onClick={() => history.push('/esqueci-a-senha')}>
                    <Typography align="center">{strings.esqueceuASenha}</Typography>
                  </a>
                </div>
              </Paper>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
