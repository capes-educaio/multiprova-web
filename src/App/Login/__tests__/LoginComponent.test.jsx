import { ptBr } from 'utils/strings/ptBr'

import { LoginComponent } from '../LoginComponent'
import { mountTest } from 'localModules/testUtils/mountTest'

const requiredProps = {
  // redux state
  formRefs: { formLogin: { showError: jest.fn() } },
  // style
  classes: {},
  // strings
  strings: ptBr,
}

const arrayOfProps = [requiredProps]

mountTest(LoginComponent, 'LoginComponent')(arrayOfProps)
