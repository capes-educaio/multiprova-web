import React, { Component } from 'react'

import { usuarioProps } from 'localModules/multiprovaEntidades'

import InputAdornment from '@material-ui/core/InputAdornment'
import EmailIcon from '@material-ui/icons/Email'

import { SimpleReduxForm, CampoText, CampoPassword } from 'localModules/ReduxForm'

import { propTypes } from './propTypes'

export class LoginFormComponent extends Component {
  static propTypes = propTypes

  render() {
    const { strings, valor, onChange, onValid, classes, handleLogin, refs, title } = this.props
    return (
      <SimpleReduxForm id="formLogin" valor={valor} onChange={onChange} onValid={onValid} title={title}>
        <CampoText
          id="formLogin__email"
          accessor="email"
          label={strings.email}
          required
          validar={usuarioProps.email.validar}
          onEnter={refs['formLogin__password'] ? refs['formLogin__password'].focus : null}
          textFieldProps={{
            // autoFocus: true,
            autoComplete: 'email',
            fullWidth: true,
            name: 'formLogin__email',
            InputProps: {
              endAdornment: (
                <InputAdornment position="end" className={classes.emailAdornment}>
                  <EmailIcon className={classes.icon} />
                </InputAdornment>
              ),
            },
          }}
          mensagensDeErro={{
            email_invalido: 'Email inválido.',
          }}
        />
        <CampoPassword
          id="formLogin__password"
          accessor="password"
          required
          label={strings.senha}
          validar={usuarioProps.password.validar}
          onEnter={handleLogin}
          textFieldProps={{
            fullWidth: true,
            name: 'formLogin__password',
          }}
          mensagensDeErro={{
            email_invalido: 'Email inválido.',
          }}
        />
      </SimpleReduxForm>
    )
  }
}
