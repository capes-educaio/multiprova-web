import { ptBr } from 'utils/strings/ptBr'

import { LoginFormComponent } from '../LoginFormComponent'
import { mountTest } from 'localModules/testUtils/mountTest'

const requiredProps = {
  valor: {},
  onChange: jest.fn(),
  onValid: jest.fn(),
  handleLogin: jest.fn(),
  // redux state
  refs: {},
  // style
  classes: {},
  // strings
  strings: ptBr,
}

const arrayOfProps = [requiredProps]

mountTest(LoginFormComponent, 'LoginFormComponent')(arrayOfProps)
