import PropTypes from 'prop-types'

export const propTypes = {
  // redux state
  themeChosen: PropTypes.string,
  strings: PropTypes.object,
  usuarioAtual: PropTypes.object,
  // redux actions
  resetSimpleStates: PropTypes.func,
  // style
  classes: PropTypes.object.isRequired,
  // router
  history: PropTypes.object.isRequired,
}
