import React, { Component } from 'react'

import { Container } from 'common/Container'

import { BarraSuperior } from '../BarraSuperior'

import { propTypes } from './propTypes'

export class DisplayComponent extends Component {
  static propTypes = propTypes

  render() {
    const { classes, children } = this.props
    return (
      <div className={classes.display}>
        <div>
          <BarraSuperior /> <Container>{children}</Container>
        </div>
      </div>
    )
  }
}
