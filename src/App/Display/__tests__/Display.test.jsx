import Enzyme, { mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

import { DisplayComponent } from '../DisplayComponent'

import React from 'react'

Enzyme.configure({ adapter: new Adapter() })

jest.mock('common/Container', () => ({
  Container: props => <div>{props.children}</div>,
}))

jest.mock('../../BarraSuperior', () => ({
  BarraSuperior: props => <div>{props.children}</div>,
}))

const defaultProps = {
  classes: {
    display: '.display',
  },
  strings: {},
  children: <div>Conteúdo</div>,
}

describe('BotaoMenuEsquerdo unit test', () => {
  test('Monta', () => {
    const component = mount(<DisplayComponent {...defaultProps} />)
    component.unmount()
  })
  test('Children é aplicado.', () => {
    const component = mount(<DisplayComponent {...defaultProps} />)
    expect(component.contains(defaultProps.children)).toBeTruthy()
    component.unmount()
  })
})
