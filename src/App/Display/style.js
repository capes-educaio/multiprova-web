export const style = theme => ({
  display: {
    backgroundColor: theme.palette.fundo.cinza,
    '& > div': {
      [theme.breakpoints.up('md')]: {
        padding: '0 0 20px 0',
      },
    },
    '& header': {
      padding: '0 !important',
    },
    width: '100%',
  },
})
