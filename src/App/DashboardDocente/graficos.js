import { theme } from 'utils/theme.js'
import { appConfig } from 'appConfig'

const { tipoQuestoesHabilitados } = appConfig

export const nivelDeDificuldadeQuestoes = {
  option: ({ dados, strings, isMobile }) => {
    const data = nivelDeDificuldadeQuestoes.data({ dados, strings })
    return {
      title: {
        text: strings.dificuldadeDasQuestoes,
        x: 'center',
        textStyle: {
          fontSize: isMobile ? 12 : 20,
          fontWeight: '100',
        },
      },
      toolbox: {
        feature: {
          saveAsImage: { show: true, title: strings.salvarComoImagem },
        },
        left: '35px',
        type: 'png',
      },
      tooltip: {
        trigger: 'item',
        formatter: '{a} <br/>{b} : {c} ({d}%)',
      },
      legend: {
        type: 'scroll',
        orient: isMobile ? 'horizontal' : 'vertical',
        top: isMobile ? 'auto' : '25%',
        bottom: isMobile ? '5%' : 'auto',
        left: isMobile ? 'center' : '60%',
        data: data.legendData,
        padding: 10,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: theme.palette.grafico.grey.main,
      },
      series: [
        {
          name: strings.dificuldadeDasQuestoes,
          type: 'pie',
          radius: '50%',
          center: isMobile ? ['50%', '45%'] : ['32%', '55%'],
          data: data.seriesData,
          itemStyle: {
            emphasis: {
              shadowBlur: 10,
              shadowOffsetX: 0,
              shadowColor: theme.palette.default.dark,
            },
          },
        },
      ],
    }
  },
  data: ({ dados, strings }) => {
    var legendData = [strings.facil, strings.medio, strings.dificil]
    var seriesData = [
      {
        name: strings.facil,
        value: dados.facil,
        itemStyle: {
          color: theme.palette.secondary.main,
        },
      },
      {
        name: strings.medio,
        value: dados.medio,
        itemStyle: {
          color: theme.palette.default.main,
        },
      },
      {
        name: strings.dificil,
        value: dados.dificil,
        itemStyle: {
          color: theme.palette.default.dark,
        },
      },
    ]

    return {
      legendData: legendData,
      seriesData: seriesData,
    }
  },
}

export const tipoDasQuestoes = {
  option: ({ dados, strings, isMobile }) => {
    const data = tipoDasQuestoes.data({ dados, strings })
    return {
      title: {
        text: strings.tiposDasQuestoes,
        x: 'center',
        textStyle: {
          fontSize: isMobile ? 12 : 20,
          fontWeight: '100',
        },
      },
      toolbox: {
        feature: {
          saveAsImage: { show: true, title: strings.salvarComoImagem },
        },
        left: '35px',
        type: 'png',
      },
      tooltip: {
        trigger: 'item',
        formatter: '{a} <br/>{b} : {c} ({d}%)',
      },
      legend: {
        type: 'scroll',
        orient: isMobile ? 'horizontal' : 'vertical',
        top: isMobile ? 'auto' : '25%',
        bottom: isMobile ? '5%' : 'auto',
        left: isMobile ? '1%' : 'auto',
        right: isMobile ? '1%' : '2%',
        data: data.legendData,
        padding: 10,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: theme.palette.grafico.grey.main,
        textStyle: {
          fontSize: isMobile ? 12 : 10,
        },
      },
      series: [
        {
          name: strings.tiposDasQuestoes,
          type: 'pie',
          radius: '50%',
          center: isMobile ? ['50%', '45%'] : ['30%', '55%'],
          data: data.seriesData,

          label: {
            formatter: values => {
              return values.data.name.split(' ').join('\n')
            },
            fontSize: isMobile ? 12 : 10,
          },

          itemStyle: {
            emphasis: {
              shadowBlur: 10,
              shadowOffsetX: 0,
              shadowColor: theme.palette.default.dark,
            },
          },
        },
      ],
    }
  },
  data: ({ dados, strings }) => {
    var legendData = []
    var seriesData = []
    let itemMap = {
      vouf: {
        name: strings.vouf,
        value: dados.vouf,
        itemStyle: {
          color: theme.palette.defaultGrey,
        },
      },
      associacaoDeColunas: {
        name: strings.associacaoDeColunas,
        value: dados.associacaoDeColunas,
        itemStyle: {
          color: theme.palette.secondary.main,
        },
      },
      multiplaEscolha: {
        name: strings.multiplaEscolha,
        value: dados.multiplaEscolha,
        itemStyle: {
          color: theme.palette.secondary.dark,
        },
      },
      redacao: {
        name: strings.redacao,
        value: dados.redacao,
        itemStyle: {
          color: theme.palette.default.light,
        },
      },
      bloco: {
        name: strings.bloco,
        value: dados.bloco,
        itemStyle: {
          color: theme.palette.default.main,
        },
      },
      discursiva: {
        name: strings.discursiva,
        value: dados.discursiva,
        itemStyle: {
          color: theme.palette.default.dark,
        },
      },
    }

    Object.keys(itemMap).map(
      tipo => tipoQuestoesHabilitados[tipo] && legendData.push(strings[tipo]) && seriesData.push(itemMap[tipo]),
    )

    return {
      legendData: legendData,
      seriesData: seriesData,
    }
  },
}

export const acumuladoDeProvas = {
  option: ({ dados, strings, isMobile }) => {
    return {
      title: {
        text: strings.acumuladoDeProvasIndividuais,
        left: 'center',
        textStyle: {
          fontSize: isMobile ? 12 : 20,
          fontWeight: '100',
        },
      },
      toolbox: {
        feature: {
          saveAsImage: { show: true, title: strings.salvarComoImagem },
        },
        left: '35px',
        top: isMobile ? '7%' : 'auto',
        type: 'png',
      },
      xAxis: {
        type: 'category',
        text: 'label',
        data: acumuladoDeProvas.dataAxisX(dados),
        axisLabel: {
          rotate: isMobile ? 40 : 0,
          margin: 10,
          show: true,
        },
      },
      yAxis: {
        type: 'value',
        maxInterval: 10,
      },
      tooltip: {
        trigger: 'item',
        formatter: '{b} : {c}',
      },
      series: [
        {
          data: acumuladoDeProvas.dataAxisY(dados),
          type: 'bar',
          label: {
            formatter: values => {
              return values.data.name.split(' ').join('\n')
            },
          },
          itemStyle: {
            emphasis: {
              shadowBlur: 10,
              shadowOffsetX: 0,
              shadowColor: theme.palette.default.dark,
            },
            color: theme.palette.secondary.dark,
          },
        },
      ],
    }
  },
  dataAxisX: dados => {
    const labels = []
    if (dados !== undefined && dados.acumulado !== undefined) {
      for (let dado of dados.acumulado) labels.push(dado.mes)
    }
    return labels
  },
  dataAxisY: dados => {
    const acumulado = []
    if (dados !== undefined && dados.acumulado !== undefined) {
      for (let dado of dados.acumulado) acumulado.push(dado.valor)
    }
    return acumulado
  },
}
