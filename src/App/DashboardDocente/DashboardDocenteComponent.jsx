import React, { Component } from 'react'

import { Dashboard } from 'common/Dashboard'
import { questaoUtils } from 'localModules/questoes/utils/QuestaoUtils'
import { provaUtils } from 'localModules/provas'
import { tipoDasQuestoes, nivelDeDificuldadeQuestoes, acumuladoDeProvas } from './graficos'

import { propTypes } from './propTypes'
import { defaultProps } from './defaultProps'

export class DashboardDocenteComponent extends Component {
  static propTypes = propTypes
  static defaultProps = defaultProps

  constructor(props) {
    super(props)
    this.state = {
      carregando: true,
      estatisticas: {
        questoes: [],
      },
      cardData: [],
      graphData: [],
    }
  }

  setDashboardData = () => {
    const { estatisticas } = this.state
    const { strings, classes, browser } = this.props
    const isMobile = browser.lessThan.medium

    const cardData = [
      {
        gridXs: isMobile ? 6 : 4,
        titulo: strings.questoesCriadas({ value: estatisticas.totalDeQuestoes }),
        value: estatisticas.totalDeQuestoes,
        classes,
      },
      {
        gridXs: isMobile ? 6 : 4,
        titulo: strings.questoesValidadas({ value: estatisticas.questoesValidadas }),
        value: estatisticas.questoesValidadas,
        classes,
      },
      {
        gridXs: isMobile ? 6 : 4,
        titulo: strings.provasIndividuaisAplicadas({ value: estatisticas.totalDeInstanciasConcluidas }),
        value: estatisticas.totalDeInstanciasConcluidas,
        classes,
      },
      {
        gridXs: isMobile ? 6 : 4,
        titulo: strings.provasEmAplicacao({ value: estatisticas.provasEmAplicacao }),
        value: estatisticas.provasEmAplicacao,
        classes,
      },
      {
        gridXs: isMobile ? 6 : 4,
        titulo: strings.provasParaCorrigir({ value: estatisticas.totalDeInstanciasParaCorrigir }),
        value: estatisticas.totalDeInstanciasParaCorrigir,
        classes,
      },
      {
        gridXs: isMobile ? 6 : 4,
        titulo: strings.vistasDeProvaEmAberto({ value: estatisticas.provasComVistaHabilitadas }),
        value: estatisticas.provasComVistaHabilitadas,
        classes,
      },
    ]
    const graphData = [
      {
        gridXs: isMobile ? 12 : 6,
        option: tipoDasQuestoes.option({ dados: estatisticas, strings, isMobile }),
      },
      {
        gridXs: isMobile ? 12 : 6,
        option: nivelDeDificuldadeQuestoes.option({ dados: estatisticas, strings, isMobile }),
      },
      {
        gridXs: 12,
        option: acumuladoDeProvas.option({ dados: estatisticas, strings, isMobile }),
      },
    ]

    this.setState({ cardData, graphData })
  }

  componentDidMount = () => {
    const { usuarioAtual } = this.props
    var e1 = questaoUtils
      .getEstatisticasQuestoes(usuarioAtual.data.id)
      .then(res => {
        this.setState({ estatisticas: { ...this.state.estatisticas, ...res.data } })
      })
      .catch(err => {
        console.warn(err)
      })
    var e2 = provaUtils
      .getEstatisticasInstancias(usuarioAtual.data.id)
      .then(res => {
        this.setState({ estatisticas: { ...this.state.estatisticas, ...res.data } })
      })
      .catch(err => {
        console.warn(err)
      })
    var e3 = provaUtils
      .getEstatisticasProvas(usuarioAtual.data.id)
      .then(res => {
        this.setState({ estatisticas: { ...this.state.estatisticas, ...res.data } })
      })
      .catch(err => {
        console.warn(err)
      })

    var acumuladoDasInstancias = provaUtils
      .getAcumuladoDasInstancias(usuarioAtual.data.id)
      .then(res => {
        this.setState({ estatisticas: { ...this.state.estatisticas, ...res.data } })
      })
      .catch(err => {
        console.warn(err)
      })

    Promise.all([e1, e2, e3, acumuladoDasInstancias]).then(() => {
      this.setDashboardData()
      this.setState({ carregando: false })
    })
  }

  render() {
    const { carregando, cardData, graphData } = this.state

    return <Dashboard loading={carregando} cardData={cardData} graphData={graphData} />
  }
}
