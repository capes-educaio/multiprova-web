import { bindActionCreators } from 'redux'

export function mapStateToProps(state) {
  return {
    strings: state.strings,
    prova: state.prova,
  }
}

export function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch)
}
