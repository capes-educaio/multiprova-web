import React, { Component } from 'react'
import { Estatisticas } from 'common/Estatisticas'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import IconButton from '@material-ui/core/IconButton'
import ArrowBackIos from '@material-ui/icons/ArrowBackIos'
import { setUrlState } from 'localModules/urlState'

export class EstatisticasPageComponent extends Component {
  componentDidMount() {
    const { prova } = this.props
    if (prova === null) {
      setUrlState({ pathname: `/provas/finalizadas` })
    }
  }

  back = () => {
    setUrlState({ pathname: `/provas/finalizadas` })
  }

  render() {
    const { classes, strings, prova } = this.props

    return (
      <div className={classes.root}>
        <AppBar position="static">
          <Toolbar>
            <IconButton
              edge="start"
              onClick={this.back}
              className={classes.menuButton}
              color="inherit"
              aria-label="menu"
            >
              <ArrowBackIos />
            </IconButton>
            <Typography color="inherit" variant="h6" className={classes.title}>
              {strings.estatisticas}
            </Typography>
          </Toolbar>
        </AppBar>
        {prova !== null && <Estatisticas />}
      </div>
    )
  }
}
