import PropTypes from 'prop-types'

export const propTypes = {
  indexNavegacao: PropTypes.number.isRequired,
  select: PropTypes.func.isRequired,
  numeroDeQuestoes: PropTypes.number.isRequired,
  // style
  classes: PropTypes.object.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
}
