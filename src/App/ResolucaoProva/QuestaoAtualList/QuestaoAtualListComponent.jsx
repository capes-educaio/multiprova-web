import React from 'react'

import Select from '@material-ui/core/Select'
import MenuItem from '@material-ui/core/MenuItem'
import OutlinedInput from '@material-ui/core/OutlinedInput'

import { propTypes } from './propTypes'

export const QuestaoAtualListComponent = ({ classes, indexNavegacao, select, numeroDeQuestoes }) => (
  <Select
    className={classes.select}
    value={indexNavegacao}
    onChange={e => select(e.target.value)()}
    input={<OutlinedInput name="questao" id="questao" labelWidth={0} />}
    autoWidth={true}
  >
    {[...Array(numeroDeQuestoes)].map((x, index) => (
      <MenuItem key={index + 1} value={index + 1}>
        {index + 1}
      </MenuItem>
    ))}
  </Select>
)

QuestaoAtualListComponent.propTypes = propTypes
