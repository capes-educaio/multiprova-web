import { compose } from 'redux'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'

import { withStyles } from '@material-ui/core/styles'

import { style } from './style'
import { mapStateToProps, mapDispatchToProps } from './redux'
import { ResolucaoProvaComponent } from './ResolucaoProvaComponent'

export const ResolucaoProva = compose(
  withStyles(style),
  withRouter,
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(ResolucaoProvaComponent)
