import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import { MemoryRouter } from 'react-router-dom'
import { Provider } from 'react-redux'
import { ResolucaoProvaComponent } from '../ResolucaoProvaComponent'
import prova from './sampleInstanciaSemGabarito'
import React from 'react'

import configureStore from 'redux-mock-store'

import { ptBr } from 'utils/strings/ptBr'

const mockStore = configureStore()

export const store = mockStore({
  usuarioAtual: {
    data: {
      id: 1,
    },
  },
  strings: ptBr,
  browser: {},
})

Enzyme.configure({ adapter: new Adapter() })

jest.mock('@material-ui/core/CircularProgress', () => props => <div>{props.children}</div>)
jest.mock('@material-ui/core/CardContent', () => props => <div>{props.children}</div>)
jest.mock('@material-ui/core/Snackbar', () => props => <div>{props.children}</div>)

jest.mock('utils/enumTipoQuestao', () => ({
  enumTipoQuestao: {
    tipoQuestaoBloco: 'bloco',
    tipoQuestaoDiscursiva: 'discursiva',
    tipoQuestaoMultiplaEscolha: 'multiplaEscolha',
    tipoQuestaoVerdadeiroOuFalso: 'vouf',
    tipoQuestaoAssociacaoDeColunas: 'associacaoDeColunas',
    tipoQuestaoRedacao: 'redacao',
  },
}))

jest.mock('localModules/questoes/QuestaoDiscenteProva/RespostaMultiplaEscolhaDiscente', () => ({
  RespostaMultiplaEscolhaDiscente: props => <div>{props.children}</div>,
}))
jest.mock('localModules/questoes/QuestaoDiscenteProva/RespostaDiscursivaDiscente', () => ({
  RespostaDiscursivaDiscente: props => <div>{props.children}</div>,
}))
jest.mock('localModules/questoes/QuestaoDiscenteProva/RespostaAssociacaoDeColunasDiscente', () => ({
  RespostaAssociacaoDeColunasDiscente: props => <div>{props.children}</div>,
}))
jest.mock('localModules/questoes/QuestaoDiscenteProva/RespostaVouFDiscente', () => ({
  RespostaVouFDiscente: props => <div>{props.children}</div>,
}))
jest.mock('localModules/questoes/QuestaoDiscenteProva/CabecalhoBloco', () => ({
  CabecalhoBloco: props => <div>{props.children}</div>,
}))
jest.mock('common/SnackbarContent', () => ({
  SnackbarContent: props => <div>{props.children}</div>,
}))

jest.mock('common/MpParser', () => ({
  MpParser: props => <div>{props.children}</div>,
}))

jest.mock('api', () => ({
  get: jest.fn().mockResolvedValue({}),
}))

const defaultProps = {
  classes: {},
  instanciaProva: {},
  questaoValue: {},
  questaoSelecionada: 1,
  history: {},
  location: {},
  strings: ptBr,
  usuarioAtual: {
    data: {
      id: 1,
    },
  },
}

describe('ResolucaoProvaComponent unit tests', () => {
  test('Monta', () => {
    console.log('console log ta aqui: ', prova)
    shallow(
      <MemoryRouter>
        <div>
          <Provider store={store}>
            <ResolucaoProvaComponent {...defaultProps} />
          </Provider>
          ,
        </div>
      </MemoryRouter>,
    )
  })
})
