import PropTypes from 'prop-types'

export const propTypes = {
  provaValue: PropTypes.object.isRequired,
  children: PropTypes.node.isRequired,
  duracao: PropTypes.number.isRequired,
  // style
  classes: PropTypes.object.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
}
