const secaoComum = theme => ({
  border: `1px solid ${theme.palette.cinzaDusty}`,
  borderRadius: '5px',
  padding: '20px',
})

const textoComum = theme => ({
  fontWeight: '300',

  color: theme.palette.primary.main,
})

export const style = theme => ({
  content: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  secaoSup: {
    ...secaoComum(theme),
    maxWidth: '450px',
    margin: '80px 0 80px 0',
  },
  secaoInf: {
    ...secaoComum(theme),
    margin: '0 0 80px 0',
  },
  texto: {
    ...textoComum(theme),
    fontSize: '18px',
  },
  textoLinks: {
    ...textoComum(theme),
    fontSize: '16px',
    maxWidth: '480px',
  },
})
