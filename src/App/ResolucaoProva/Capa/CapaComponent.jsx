import React from 'react'

import Typography from '@material-ui/core/Typography'

import { propTypes } from './propTypes'

export class CapaComponent extends React.Component {
  static propTypes = propTypes

  render() {
    const { classes, provaValue, strings, duracao, children } = this.props
    const { tema, numeroDeQuestoesNaProva } = provaValue
    const nomeProfessor = provaValue.nomeProfessor !== '' && strings.professor + ': ' + provaValue.nomeProfessor

    return (
      <div className={classes.content} role="contentinfo" tabIndex="0">
        <div className={classes.secaoSup}>
          <Typography className={classes.texto} align="center" gutterBottom>
            {tema}
          </Typography>
          <Typography className={classes.texto} align="center" gutterBottom>
            {nomeProfessor}
          </Typography>
          <Typography className={classes.texto} align="center" gutterBottom>
            {strings.quantidadeDeQuestoes}: {numeroDeQuestoesNaProva}
          </Typography>
          <Typography className={classes.texto} align="center" gutterBottom>
            {strings.tempoTotal}: {duracao} {strings.minutos}
          </Typography>
        </div>
        <Typography className={classes.textoLinks} align="left">
          {strings.linkParaQuestoes}:
        </Typography>
        <div className={classes.secaoInf}>{children}</div>
      </div>
    )
  }
}
