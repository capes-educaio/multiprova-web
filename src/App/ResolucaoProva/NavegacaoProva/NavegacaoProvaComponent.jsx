import React from 'react'
import IconButton from '@material-ui/core/IconButton'
import ArrowLeft from '@material-ui/icons/ArrowLeft'
import ArrowRight from '@material-ui/icons/ArrowRight'
import { propTypes } from './propTypes'
import classnames from 'classnames'

export class NavegacaoProvaComponent extends React.Component {
  static propTypes = propTypes
  qtdQuestoesNevegacaoRodape = 5

  constructor(props) {
    super(props)
    this.state = {
      indexNavegacaoRodape: Math.floor((props.indexNavegacao - 1) / this.qtdQuestoesNevegacaoRodape),
      numLinhasNavegacaoRodape: Math.ceil(props.questoes.length / this.qtdQuestoesNevegacaoRodape),
    }
  }

  componentWillReceiveProps = newProps => {
    const { indexNavegacao } = this.props
    if (indexNavegacao !== newProps.indexNavegacao) {
      const indexNavegacaoRodape = Math.floor((newProps.indexNavegacao - 1) / this.qtdQuestoesNevegacaoRodape)
      this.setState({ indexNavegacaoRodape })
    }
  }

  _selectNavegacaoRodape = numero => {
    const { numLinhasNavegacaoRodape } = this.state
    let destino = numero
    if (numero < 0 || numero === numLinhasNavegacaoRodape) destino = numLinhasNavegacaoRodape
    else if (numero > numLinhasNavegacaoRodape) destino = 0

    this.setState({ indexNavegacaoRodape: destino, numLinhasNavegacaoRodape })
  }

  render() {
    const { indexNavegacaoRodape, numLinhasNavegacaoRodape } = this.state
    const { classes, strings, select, questoes, isMobile } = this.props

    const indexVoltar = indexNavegacaoRodape - 1
    const indexAvancar = indexNavegacaoRodape + 1

    if (isMobile) {
      return (
        <div className={classes.navegacaoMobile}>
          <div ref={'capa'}>
            <IconButton onClick={select(0)} aria-label={'capa'} className={classes.botaoQuestaoSelecionado}>
              <span className={classes.numeroComum}>{strings.capa.toUpperCase()}</span>
            </IconButton>
          </div>
          {questoes.map((questao, i) => {
            const numQuestao = i + 1
            return (
              <div key={i} ref={'questao' + numQuestao}>
                <IconButton
                  onClick={select(numQuestao)}
                  aria-label={'questao' + numQuestao}
                  className={classnames(
                    classes.botaoQuestaoSelecionado,
                    classes.numeroComum,
                    !questao.resolucaoIniciada && classes.resolucaoNaoIniciada,
                    questao.resolucaoIniciada && classes.resolucaoIniciada,
                  )}
                >
                  <span className={classes.numeroQuestao}>{numQuestao}</span>
                </IconButton>
              </div>
            )
          })}
        </div>
      )
    }

    return (
      <div className={classes.arrowsContainer} tabIndex="0">
        <IconButton
          onClick={() => this._selectNavegacaoRodape(indexVoltar)}
          className={classes.botaoQuestaoSelecionado}
          aria-label={'questão anterior'}
          disabled={indexVoltar < 0 && true}
        >
          <ArrowLeft />
        </IconButton>
        <div className={classes.container} ref="containerQuestoes">
          <div ref={'capa'}>
            <IconButton onClick={select(0)} aria-label={'capa'} className={classes.botaoQuestaoSelecionado}>
              <span className={classes.numeroComum}>{strings.capa.toUpperCase()}</span>
            </IconButton>
          </div>
          {questoes.map((questao, i) => {
            const numQuestao = i + 1
            const menorNumeroDaLinha = i < indexNavegacaoRodape * this.qtdQuestoesNevegacaoRodape
            const maiorNumeroDaLinha = numQuestao > (indexNavegacaoRodape + 1) * this.qtdQuestoesNevegacaoRodape
            if (menorNumeroDaLinha || maiorNumeroDaLinha) return null
            return (
              <div key={i} ref={'questao' + numQuestao}>
                <IconButton
                  onClick={select(numQuestao)}
                  aria-label={'questao' + numQuestao}
                  className={classnames(
                    classes.botaoQuestaoSelecionado,
                    classes.numeroComum,
                    !questao.resolucaoIniciada && classes.resolucaoNaoIniciada,
                    questao.resolucaoIniciada && classes.resolucaoIniciada,
                  )}
                >
                  <span className={classes.numeroQuestao}>{numQuestao}</span>
                </IconButton>
              </div>
            )
          })}
        </div>
        <IconButton
          onClick={() => this._selectNavegacaoRodape(indexAvancar)}
          className={classes.botaoQuestaoSelecionado}
          aria-label={'próxima questão'}
          disabled={indexAvancar >= numLinhasNavegacaoRodape && true}
        >
          <ArrowRight />
        </IconButton>
      </div>
    )
  }
}
