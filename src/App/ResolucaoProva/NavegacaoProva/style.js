export const style = theme => ({
  arrowsContainer: {
    padding: 4,
    margin: 'auto',
    maxWidth: '100%',
    width: '300px',
    display: 'flex',
    boxSizing: 'border-box',
  },
  container: {
    '*::-webkit-scrollbar-thumb': {
      background: '#888',
    },
    maxWidth: '100%',
    width: 'fit-content',
    display: 'flex',
    boxSizing: 'border-box',
    alignItems: 'center',
  },

  botaoQuestaoSelecionado: {
    '&:hover': {
      color: theme.palette.text.primary,
    },
    borderRadius: '50%',
    padding: '5px',
    color: theme.palette.primary.cinzaDusty,
  },

  numeroComum: {
    color: theme.palette.primary.main,
    fontSize: 18,
    minWidth: 32,
  },

  resolucaoIniciada: {
    fontWeight: '100',
  },
  resolucaoNaoIniciada: {
    color: theme.palette.defaultLink,
    fontWeight: 'bold',
  },

  navegacaoMobile: {
    display: 'flex',
    overflow: 'scroll',
    scrollbarWidth: 'none',
    scrollBehavior: 'smooth',
    paddingBottom: '1em',
    paddingLeft: '1em',
    paddingRight: '1em',
  },
})
