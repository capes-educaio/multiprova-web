import PropTypes from 'prop-types'

export const propTypes = {
  handleEncerrarProva: PropTypes.func.isRequired,
  indexNavegacao: PropTypes.number.isRequired,
  numeroDeQuestoes: PropTypes.number.isRequired,
  navegacao: PropTypes.node.isRequired,
  select: PropTypes.func.isRequired,
  // style
  classes: PropTypes.object.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
}
