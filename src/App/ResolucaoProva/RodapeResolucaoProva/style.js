const capaComum = theme => ({
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center',
  flexDirection: 'row',
  padding: '10px 10px',
  minHeight: '42px',
})

export const style = theme => ({
  container: {
    ...capaComum(theme),
    justifyContent: 'space-between',
  },

  containerCapa: {
    ...capaComum(theme),
    justifyContent: 'flex-end',
  },

  paginaAtual: {
    fontSize: '14px',
    display: 'flex',
  },

  indexNavegacao: {
    display: 'inline-block',
    border: `1px solid ${theme.palette.darkGray}`,
    verticalAlign: 'middle',
    width: '36px',
    borderRadius: '50%',
  },

  navegacao: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    paddingRight: '30px',
  },

  navegacaoMobile: {
    paddingTop: '1em',
    paddingBottom: '1em',
  },

  navegacaoMobileCapa: {
    padding: '1em',
    textAlign: 'right',
  },

  navegacaoMobileLeftRigth: {
    display: 'flex',
    justifyContent: 'space-around',
    paddingBottom: '1em',
  },
})
