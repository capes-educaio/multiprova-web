import React, { Fragment } from 'react'

import Typography from '@material-ui/core/Typography'
import ArrowForwardIcon from '@material-ui/icons/ArrowForward'
import ArrowBackIcon from '@material-ui/icons/ArrowBack'
import Button from '@material-ui/core/Button'

import { propTypes } from './propTypes'
import { QuestaoAtualList } from '../QuestaoAtualList'

export class RodapeResolucaoProvaComponent extends React.Component {
  static propTypes = propTypes

  get navegacao() {
    const { indexNavegacao, numeroDeQuestoes, strings, select } = this.props
    const prev = indexNavegacao - 1
    const next = indexNavegacao + 1

    let buttonLabelNext = null
    if (prev < 0) {
      buttonLabelNext = strings.questoes
    } else if (next > numeroDeQuestoes) {
      buttonLabelNext = strings.capa
    } else {
      buttonLabelNext = strings.proxima
    }

    const navegacao = {
      prev,
      next,
      ariaLabelPrev: prev <= 0 ? 'capa' : `questao${prev}`,
      ariaLabelNext: next > numeroDeQuestoes ? 'capa' : `questao${next}`,
      lastPage: next > numeroDeQuestoes,
      buttonLabelPrev: prev === 0 ? strings.capa : strings.anterior,
      buttonLabelNext,
      funcNavegacaoFinal: next > numeroDeQuestoes ? select(0) : select(next),
    }

    return navegacao
  }

  render() {
    const { classes, strings, navegacao, indexNavegacao, select, numeroDeQuestoes, isMobile } = this.props
    const isCapa = indexNavegacao === 0

    return (
      <div>
        {isMobile && (
          <div>
            {!isCapa && (
              <div>
                <div className={classes.navegacaoMobile}>{navegacao}</div>
                <div className={classes.navegacaoMobileLeftRigth}>
                  <Button
                    className={classes.button}
                    onClick={select(this.navegacao.prev)}
                    aria-label={this.navegacao.ariaLabelPrev}
                  >
                    <ArrowBackIcon /> {this.navegacao.buttonLabelPrev}
                  </Button>

                  <Typography className={classes.paginaAtual} align="center" variant="overline">
                    <QuestaoAtualList
                      indexNavegacao={indexNavegacao}
                      select={select}
                      numeroDeQuestoes={numeroDeQuestoes}
                    />
                    {strings.de} {numeroDeQuestoes}
                  </Typography>

                  <Button
                    className={classes.button}
                    onClick={this.navegacao.funcNavegacaoFinal}
                    aria-label={this.navegacao.ariaLabelNext}
                  >
                    {this.navegacao.buttonLabelNext} {!this.navegacao.lastPage && <ArrowForwardIcon />}
                  </Button>
                </div>
              </div>
            )}
            {isCapa && (
              <div className={classes.navegacaoMobileCapa}>
                <Button
                  className={classes.button}
                  onClick={this.navegacao.funcNavegacaoFinal}
                  aria-label={this.navegacao.ariaLabelNext}
                >
                  {this.navegacao.buttonLabelNext} {!this.navegacao.lastPage && <ArrowForwardIcon />}
                </Button>
              </div>
            )}
          </div>
        )}
        {!isMobile && (
          <div className={isCapa ? classes.containerCapa : classes.container}>
            {!isCapa && (
              <Fragment>
                <Button
                  className={classes.button}
                  onClick={select(this.navegacao.prev)}
                  aria-label={this.navegacao.ariaLabelPrev}
                >
                  <ArrowBackIcon /> {this.navegacao.buttonLabelPrev}
                </Button>

                <Typography className={classes.paginaAtual} align="center" variant="overline">
                  {strings.questao}
                  <QuestaoAtualList
                    indexNavegacao={indexNavegacao}
                    select={select}
                    numeroDeQuestoes={numeroDeQuestoes}
                  />
                  {strings.de} {numeroDeQuestoes}
                </Typography>

                <div className={classes.navegacao}>{navegacao}</div>
              </Fragment>
            )}
            <Button
              className={classes.button}
              onClick={this.navegacao.funcNavegacaoFinal}
              aria-label={this.navegacao.ariaLabelNext}
            >
              {this.navegacao.buttonLabelNext} {!this.navegacao.lastPage && <ArrowForwardIcon />}
            </Button>
          </div>
        )}
      </div>
    )
  }
}
