const lineComum = theme => ({
  display: 'flex',
  flexDirection: 'row',
  alignItems: 'center',
})

export const style = theme => ({
  container: {
    maxWidth: '480px',
    width: 'fit-content',
    boxSizing: 'border-box',
    display: 'flex',
    flexWrap: 'wrap',
  },
  line: {
    ...lineComum(theme),
    justifyContent: 'space-between',
  },
  lineNotFull: {
    ...lineComum(theme),
    justifyContent: 'flex-start',
  },
  botao: {
    '&:hover': {
      color: theme.palette.text.primary,
    },
    color: theme.palette.primary.cinzaDusty,
  },

  numeroComum: {
    color: theme.palette.primary.main,
    fontSize: 18,
    minWidth: 24,
  },

  resolucaoIniciada: {
    fontWeight: '100',
  },
  resolucaoNaoIniciada: {
    color: theme.palette.defaultLink,
    fontWeight: 'bold',
  },
})
