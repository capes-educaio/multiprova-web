import PropTypes from 'prop-types'

export const propTypes = {
  // style
  classes: PropTypes.object.isRequired,
  // strings
  strings: PropTypes.object.isRequired,

  select: PropTypes.func.isRequired,
  selected: PropTypes.number.isRequired,
  questoes: PropTypes.array.isRequired,
}
