import { compose } from 'redux'
import { connect } from 'react-redux'

import { withStyles } from '@material-ui/core/styles'

import { mapStateToProps, mapDispatchToProps } from './redux'
import { style } from './style'
import { PainelNavegacaoProvaComponent } from './PainelNavegacaoProvaComponent'

export const PainelNavegacaoProva = compose(
  withStyles(style),
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(PainelNavegacaoProvaComponent)
