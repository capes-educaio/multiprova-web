import React, { Component } from 'react'
import IconButton from '@material-ui/core/IconButton'
import Tooltip from '@material-ui/core/Tooltip'
import { propTypes } from './propTypes'
import classnames from 'classnames'

export class PainelNavegacaoProvaComponent extends Component {
  static propTypes = propTypes

  render() {
    const { classes, selected, select, strings, questoes } = this.props

    return (
      <div className={classes.container} ref="containerQuestoes" tabIndex="0">
        {questoes.map((item, index) => (
          <div key={index + 1} ref={'questao' + index + 1}>
            <Tooltip title={item.resolucaoIniciada ? strings.jaIniciada : strings.naoIniciada}>
              <IconButton onClick={select(index + 1)} aria-label={'questao' + index + 1} className={classes.botao}>
                <span
                  className={classnames(
                    classes.numeroComum,
                    selected !== index + 1 && !item.resolucaoIniciada && classes.resolucaoNaoIniciada,
                    selected !== index + 1 && item.resolucaoIniciada && classes.resolucaoIniciada,
                    selected === index + 1 && classes.botaoQuestaoSelecionado,
                  )}
                >
                  {index + 1}
                </span>
              </IconButton>
            </Tooltip>
          </div>
        ))}
      </div>
    )
  }
}
