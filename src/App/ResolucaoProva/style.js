export const style = theme => ({
  carregando: {
    display: 'flex',
    justifyContent: 'center',
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -20,
    marginLeft: -20,
  },
  paper: {
    padding: 8,
  },
  divider: {
    height: '5px',
    backgroundColor: theme.palette.fundo.cinza,
  },
  conteudo: {
    padding: '20px',
  },
})
