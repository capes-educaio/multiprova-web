import React from 'react'

import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'

import { propTypes } from './propTypes'
import { Cronometro } from 'common/Cronometro'
import logo from 'localModules/icons/logo-capes.svg'

export class CabecalhoResolucaoComponent extends React.Component {
  static propTypes = propTypes

  render() {
    const { classes, provaValue, strings, tempoRestanteDinamico, handleEncerrarProva, readMode, isMobile } = this.props
    const { instituicao, titulo } = provaValue

    if (isMobile) {
      return (
        <div>
          <div className={classes.disposicaoCabecalho} role="contentinfo" tabIndex="0">
            <div className={classes.logo} tabIndex="0">
              <img className={classes.imagem} src={logo} alt="logo" aria-label={strings.logoTexto} />
              {/* <Typography className={classes.logoTexto}>{strings.logoTexto}</Typography> */}
            </div>
            <Typography className={classes.logoTexto}>{strings.logoTexto}</Typography>
            <Button variant="contained" className={classes.button} onClick={handleEncerrarProva}>
              {readMode ? strings.encerrarVista : strings.concluirProva}
            </Button>
          </div>

          <div className={classes.disposicaoCabecalho}>
            <div className={classes.textoContainer} tabIndex="0">
              <Typography className={classes.instituicao} align="left" variant="h6" role="heading" aria-level="1">
                {instituicao}
              </Typography>
              <Typography className={classes.titulo} align="left" variant="h5" role="heading" aria-level="2">
                {titulo}
              </Typography>
            </div>

            {!readMode && (
              <div className={classes.cronometro} tabIndex="0">
                <Typography className={classes.tempoTexto} variant="body2" gutterBottom>
                  {strings.tempoRestante}:
                </Typography>
                <Cronometro tempo={tempoRestanteDinamico} />
              </div>
            )}
          </div>
        </div>
      )
    }

    return (
      <div className={classes.disposicaoCabecalho} role="contentinfo" tabIndex="0">
        <div className={classes.logo} tabIndex="0">
          <img className={classes.imagem} src={logo} alt="logo" aria-label={strings.logoTexto} />
          <Typography className={classes.logoTexto}>{strings.logoTexto}</Typography>
        </div>
        <div className={classes.textoContainer} tabIndex="0">
          <Typography className={classes.instituicao} align="left" variant="h6" role="heading" aria-level="1">
            {instituicao}
          </Typography>
          <Typography className={classes.titulo} align="left" variant="h5" role="heading" aria-level="2">
            {titulo}
          </Typography>
        </div>
        {!readMode && (
          <div className={classes.cronometro} tabIndex="0">
            <Typography className={classes.tempoTexto} variant="body2" gutterBottom>
              {strings.tempoRestante}:
            </Typography>
            <Cronometro tempo={tempoRestanteDinamico} />
          </div>
        )}
        <Button variant="contained" className={classes.button} onClick={handleEncerrarProva}>
          {readMode ? strings.encerrarVista : strings.concluirProva}
        </Button>
      </div>
    )
  }
}
