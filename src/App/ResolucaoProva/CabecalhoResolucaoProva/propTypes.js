import PropTypes from 'prop-types'

export const propTypes = {
  provaValue: PropTypes.object.isRequired,
  handleEncerrarProva: PropTypes.func.isRequired,
  tempoRestanteDinamico: PropTypes.number,
  // style
  classes: PropTypes.object.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
}
