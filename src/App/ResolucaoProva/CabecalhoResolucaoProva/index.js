import { compose } from 'redux'
import { connect } from 'react-redux'

import { withStyles } from '@material-ui/core/styles'

import { mapStateToProps, mapDispatchToProps } from './redux'
import { style } from './style'
import { CabecalhoResolucaoComponent } from './CabecalhoResolucaoComponent'

export const CabecalhoResolucao = compose(
  withStyles(style),
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(CabecalhoResolucaoComponent)
