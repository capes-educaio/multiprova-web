export const style = theme => ({
  disposicaoCabecalho: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    padding: '10px 0',
  },
  textoContainer: {
    flexGrow: 1,
    paddingLeft: '30px',
  },
  instituicao: {
    fontWeight: '300',
    fontSize: '18px',
    color: theme.palette.primary.main,
  },

  titulo: {
    fontSize: '18px',
    color: theme.palette.primary.main,
    paddingBottom: '5px',
  },

  logo: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    paddingLeft: '20px',
  },
  imagem: {
    width: '60px',
    paddingBottom: '5px',
  },
  logoTexto: {
    letterSpacing: '1px',
    fontSize: '14px',
  },
  tempoTexto: {
    color: theme.palette.primary.main,
  },

  cronometro: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    paddingRight: '30px',
  },
  button: {
    marginRight: '20px',
  },
  '@media only screen and (max-width: 600px)': {
    disposicaoCabecalho: {
      display: 'flex',
      justifyContent: 'space-around',
      padding: 0,
    },
    logo: {
      paddingLeft: 0,
    },
    imagem: {
      width: '40px',
      padding: '5px',
    },
    button: {
      marginRight: 0,
    },
    logoTexto: {
      letterSpacing: '0px',
      fontSize: '.8em',
    },
    textoContainer: {
      paddingLeft: '15px',
    },
  },
})
