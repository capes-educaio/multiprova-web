import PropTypes from 'prop-types'

export const propTypes = {
  // redux state
  usuarioAtual: PropTypes.object.isRequired,
  instanciaProva: PropTypes.object,
  instanciaProvaReadOnly: PropTypes.object,
  // redux actions
  // router
  history: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
}
