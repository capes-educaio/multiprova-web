import { bindActionCreators } from 'redux'

export function mapStateToProps(state) {
  return {
    usuarioAtual: state.usuarioAtual,
    instanciaProva: state.instanciaProva,
    instanciaProvaReadOnly: state.instanciaProvaReadOnly,
    strings: state.strings,
  }
}

export function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch)
}
