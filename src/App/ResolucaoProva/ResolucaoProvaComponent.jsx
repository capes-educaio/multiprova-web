import React, { Component } from 'react'

import { propTypes } from './propTypes'

import CircularProgress from '@material-ui/core/CircularProgress'
import Divider from '@material-ui/core/Divider'

import { Container } from 'common/Container'
import { PaginaPaper } from 'common/PaginaPaper'
import { PainelNavegacaoProva } from './PainelNavegacaoProva'
import { Alerta } from 'common/Alerta'
import { AlertaDeConfirmacao } from 'common/AlertaDeConfirmacao'

import { get } from 'api'

import { CabecalhoResolucao } from './CabecalhoResolucaoProva'
import { NavegacaoProva } from './NavegacaoProva'
import { Capa } from './Capa'
import { QuestaoDiscente } from 'localModules/questoes/QuestaoDiscenteProva'
import { RodapeResolucaoProva } from './RodapeResolucaoProva'

import { verificarSeQuestaoJaFoiRespondida } from 'utils/verificarSeQuestaoJaFoiRespondida'
import { calculaTempoRestanteDaProva } from 'utils/calculaTempoRestanteDaProva'
import { enumTipoQuestao } from 'utils/enumTipoQuestao'

import { setUrlState, getUrlState } from 'localModules/urlState'
import { dispatchToStore } from 'utils/compositeActions'

export class ResolucaoProvaComponent extends Component {
  static propTypes = propTypes
  interval = null
  inicioDaResolucao
  questoes

  constructor(props) {
    super(props)
    this.tempoRestanteDaProva = 0
    this.state = {
      indexNavegacao: 0,
      alertaAberto: false,
      alertaDeConfirmacaoAberto: false,
      carregando: true,
    }
  }

  componentWillMount() {
    const readMode = this.props.instanciaProvaReadOnly.readMode
    if (!this.props.instanciaProva) this._carregarProva()
    else {
      if (readMode) {
        this._carregarVistaDeProvas()
      } else {
        this._verificarTempoEcarregarInicioDeResolucao()
      }
      this.setState({ readMode })
    }
  }

  componentWillUnmount() {
    if (this.interval) clearInterval(this.interval)
  }

  _janelaDeExecucaoEncerrada = () => {}

  _verificarTempoEcarregarInicioDeResolucao = () => {
    const { duracaoDaProva } = this.props.instanciaProva.virtual
    const acabouOTempo = duracaoDaProva <= 0
    if (acabouOTempo) this._redirectToListaDeProvas()
    else {
      this._carregarInicioDeResolucao()
    }
  }

  _carregarProva = () => {
    const urlState = getUrlState()
    const pathname = urlState.pathname.split('/')

    if (pathname.length > 2) {
      const id = pathname[2]
      get(`instanciamentos/${id}`)
        .then(res => {
          dispatchToStore('update', 'instanciaProva', res.data)

          const { prova } = res.data
          if (prova.vistaProvaHabilitada !== undefined && prova.vistaProvaHabilitada === true) {
            this._carregarVistaDeProvas()
            this.setState({ readMode: true })
          } else {
            this._carregarInicioDeResolucao()
          }
        })
        .catch(console.warn)
    }
  }

  _carregarInicioDeResolucao = () => {
    const { id, virtual } = this.props.instanciaProva
    this.tempoRestante(virtual.duracaoDaProva)
    get(`instanciamentos/${id}/iniciar-resolucao`)
      .then(
        get(`instanciamentos/${id}/obter-instancia-sem-gabarito`)
          .then(res => {
            this.questoes = res.data.data.prova.grupos.reduce(this.getQuestoesDaProvaProcessadas, [])
            this.setState({ carregando: false })
          })
          .catch(err => {
            console.error(err)
          }),
      )
      .catch(console.warn)
  }

  _carregarVistaDeProvas = () => {
    const { id } = this.props.instanciaProva
    get(`instanciamentos/${id}/vista-de-prova`)
      .then(res => {
        this.questoes = res.data.prova.grupos.reduce(this.getQuestoesDaProvaProcessadas, [])
        this.setState({ carregando: false })
      })
      .catch(err => {
        console.error(err)
      })
  }

  _redirectToListaDeProvas = () => {
    setUrlState({ pathname: '/provas/finalizadas' })
  }

  finalizarProva = isToRedirect => {
    const { id } = this.props.instanciaProva
    get(`instanciamentos/${id}/finalizar-resolucao`)
      .then(() => {
        if (isToRedirect) {
          setUrlState({
            pathname: '/provas/finalizadas',
            hidden: { desabilitaBotaoIniciarProva: true, desabilitaBotaoFinalizarProva: true },
          })
        }
      })
      .catch(error => {
        console.warn(error)
      })
  }

  abrirAvisoCancelar = () => {
    this.setState({ alertaDeConfirmacaoAberto: true })
  }
  handleBotaoEncerrarProva = () => {
    this.state.readMode ? this._redirectToListaDeProvas() : this.abrirAvisoCancelar()
  }

  handleCloseConfirmar = button => {
    if (button === true) this.finalizarProva(true)
    else this.setState({ alertaDeConfirmacaoAberto: false })
  }

  handleClose = button => {
    if (button === true) this._redirectToListaDeProvas()
    else this.setState({ alertaAberto: false })
  }

  _getNumeroPrimeiraEUltimaQuestoesBloco = questao => {
    const { bloco } = questao
    const { questoes } = bloco
    if (questoes)
      return {
        inicial: questoes[0].numeroNaInstancia,
        final: questoes[questoes.length - 1].numeroNaInstancia,
      }
    return {
      inicial: 0,
      final: 0,
    }
  }

  _substituirPalavrasChavePorNumerosNoEnunciadodoBloco = questao => {
    const { inicial, final } = this._getNumeroPrimeiraEUltimaQuestoesBloco(questao)
    questao.enunciado = questao.enunciado.replace('#inicial', inicial)
    questao.enunciado = questao.enunciado.replace('#final', final)
    questao.bloco.fraseInicial = questao.bloco.fraseInicial.replace('#inicial', inicial)
    questao.bloco.fraseInicial = questao.bloco.fraseInicial.replace('#final', final)
    return questao
  }

  getQuestaoDoGrupoProcessada = (grupoIndex, spreadObject) => (acc, questao, questaoIndex) => {
    if (questao.tipo !== enumTipoQuestao.tipoQuestaoBloco)
      return [...acc, { ...questao, grupoIndex, questaoIndex, ...spreadObject }]

    questao = this._substituirPalavrasChavePorNumerosNoEnunciadodoBloco(questao)

    const {
      bloco: { fraseInicial = '' },
    } = questao
    return [
      ...acc,
      ...questao.bloco.questoes.reduce(
        this.getQuestaoDoGrupoProcessada(grupoIndex, {
          bloco: {
            questaoIndex,
            questaoMatrizId: questao.questaoMatrizId,
            enunciado: questao.enunciado,
            fraseInicial,
          },
        }),
        [],
      ),
    ]
  }

  getQuestoesDaProvaProcessadas = (todasQuestoesDaProvaProcessadas, grupo, grupoIndex) => [
    ...todasQuestoesDaProvaProcessadas,
    ...grupo.questoes.reduce(this.getQuestaoDoGrupoProcessada(grupoIndex), []),
  ]

  select = numero => () => {
    let destino = numero
    if (numero < 0 || numero === this.questoes.length) destino = this.questoes.length
    else if (numero > this.questoes.length) destino = 0
    this.setState({ indexNavegacao: destino })
  }

  _getDateTimeServidor = async () => {
    return await get('/usuarios/datetime-servidor')
  }

  tempoRestante = async duracao => {
    try {
      const { data } = await this._getDateTimeServidor()
      const dateTimeServidor = data.datetime
      const prova = this.props.instanciaProva.virtual
      const dataIniciouResolucao = prova.dataIniciouResolucao ? new Date(prova.dataIniciouResolucao) : dateTimeServidor
      this.tempoRestanteDaProva = calculaTempoRestanteDaProva(dateTimeServidor, dataIniciouResolucao, duracao).timestamp
      this.interval = setInterval(this.atualizarTempo, 1000)
      this.inicioDaResolucao = new Date(dateTimeServidor).getTime()
      this.setState({ tempoRestanteDinamico: this.tempoRestanteDaProva })
    } catch (error) {
      console.warn(error)
    }
  }

  atualizarTempo = async () => {
    const { data } = await this._getDateTimeServidor()
    const dateTimeServidor = new Date(data.datetime)
    const tempoRestanteDinamico = this.tempoRestanteDaProva - (dateTimeServidor.getTime() - this.inicioDaResolucao)
    this.setState({ tempoRestanteDinamico })

    const dataTerminoProva = new Date(this.props.instanciaProva.virtual.dataTerminoProva)
    const acabouOTempo = tempoRestanteDinamico <= 0
    const fechouAJanela = dateTimeServidor >= dataTerminoProva
    if (acabouOTempo || fechouAJanela) {
      clearInterval(this.interval)
      this.setState({ alertaAberto: true })
      this.finalizarProva(false)
    }
  }

  render() {
    const {
      indexNavegacao,
      tempoRestanteDinamico,
      alertaAberto,
      alertaDeConfirmacaoAberto,
      readMode,
      carregando,
    } = this.state
    const { classes, strings, instanciaProva } = this.props
    if (carregando)
      return (
        <div className={classes.carregando}>
          <CircularProgress />
        </div>
      )

    const navegacao = (
      <div>
        <NavegacaoProva
          indexNavegacao={indexNavegacao}
          select={this.select}
          questoes={this.questoes.map(questao => ({
            resolucaoIniciada: verificarSeQuestaoJaFoiRespondida(questao),
          }))}
        />
      </div>
    )

    return (
      <Container role="main">
        <PaginaPaper className={classes.paper}>
          <CabecalhoResolucao
            provaValue={instanciaProva.prova}
            tempoRestanteDinamico={tempoRestanteDinamico}
            handleEncerrarProva={this.handleBotaoEncerrarProva}
            readMode={readMode}
          />
          <Divider className={classes.divider} />
          {indexNavegacao < 1 && (
            <Capa provaValue={instanciaProva.prova} duracao={instanciaProva.virtual.duracaoDaProva}>
              <PainelNavegacaoProva
                select={this.select}
                selected={indexNavegacao}
                questoes={this.questoes.map(questao => ({
                  resolucaoIniciada: verificarSeQuestaoJaFoiRespondida(questao),
                }))}
              />
            </Capa>
          )}
          {indexNavegacao > 0 && (
            <div className={classes.conteudo}>
              <QuestaoDiscente
                disabled={readMode}
                readMode={readMode}
                questaoSelecionada={indexNavegacao - 1}
                numeroDeQuestoes={this.questoes.length}
                questaoValue={this.questoes[indexNavegacao - 1]}
              />
            </div>
          )}
          <Divider className={classes.divider} />
          <RodapeResolucaoProva
            navegacao={navegacao}
            indexNavegacao={indexNavegacao}
            select={this.select}
            numeroDeQuestoes={this.questoes.length}
            handleEncerrarProva={this.abrirAvisoCancelar}
          />
        </PaginaPaper>
        <AlertaDeConfirmacao
          alertaAberto={alertaDeConfirmacaoAberto}
          handleClose={this.handleCloseConfirmar}
          stringNomeInstancia={strings.finalizarProva}
          descricaoSelected={strings.confirmacaoFinalizarProva}
        />
        <Alerta aberto={alertaAberto} handleClose={this.handleClose} titulo={strings.duracaoDaProvaAcabou} />
      </Container>
    )
  }
}
