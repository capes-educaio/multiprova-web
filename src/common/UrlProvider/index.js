import { UrlComponent } from './UrlComponent'
import { compose } from 'redux'
import { withRouter } from 'react-router'

export const UrlProvider = compose(withRouter)(UrlComponent)
