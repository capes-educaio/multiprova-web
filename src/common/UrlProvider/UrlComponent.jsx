import React, { Component } from 'react'
import qs from 'qs'

import { initiateContextState } from 'utils/context'
import { UrlContext } from './context'

export class UrlComponent extends Component {
  _locationStack = []

  constructor(props) {
    super(props)
    const { location } = props
    if (location) this._locationStack.unshift(location)
    const contextInitialValue = {
      hidden: location.state.hidden,
      string: location.state.string,
      pathname: location.pathname,
      replace: this.replace,
      set: this._set,
      update: this._update,
      goBackTo: this._goBackTo,
      goBackFrom: this._goBackFrom,
    }
    initiateContextState(this, contextInitialValue)
  }

  componentDidUpdate(prevProps) {
    if (this.props.location && this.props.location.key !== prevProps.location.key) {
      this._locationStack.unshift(this.props.location)
    }
  }

  _set = ({ pathname, hidden, string }) => {
    const { history } = this.props
    const newUrl = {
      pathname: pathname ? pathname : history.location.pathname,
      state: {
        hidden: hidden,
        string: string,
      },
    }
    if (hidden === null || hidden === undefined) newUrl.state.hidden = {}
    if (string === null || string === undefined) newUrl.state.string = {}
    else newUrl.search = qs.stringify(newUrl.state.string)
    history.push(newUrl)
  }

  _update = ({ pathname, hidden, string }) => {
    const { history } = this.props
    let { state } = history.location
    if (!state) state = {}
    const newUrl = {
      pathname: history.location.pathname,
      state: {
        hidden: state.hidden,
        string: state.string,
      },
    }
    if (pathname) newUrl.pathname = pathname
    if (hidden) newUrl.state.hidden = { ...state.hidden, ...hidden }
    if (string) {
      newUrl.state.string = { ...state.string, ...string }
      newUrl.search = qs.stringify(newUrl.state.string)
    }
    history.push(newUrl)
  }

  _replace = ({ pathname, hidden, string }) => {
    const { history } = this.props
    const newUrl = {
      state: {
        hidden: hidden,
        string: string,
      },
    }
    if (pathname) newUrl.pathname = pathname
    if (hidden === null || hidden === undefined) newUrl.state.hidden = {}
    if (string === null || string === undefined) newUrl.state.string = {}
    else newUrl.search = qs.stringify(newUrl.state.string)
    history.replace(newUrl)
  }

  _goBackTo = (goingTo, defaultPage) => {
    const regex = new RegExp(goingTo)
    const location = this._locationStack.find(location => location && regex.test(location.pathname))
    if (!location) this._set(defaultPage)
    const { pathname, state } = location
    this._set({ pathname, hidden: state.hidden, string: state.string })
  }

  _goBackFrom = (goingOutOf, defaultPage) => {
    const regex = new RegExp(goingOutOf)
    const location = this._locationStack.find(location => location && !regex.test(location.pathname))
    if (!location) return this._set(defaultPage)
    const { pathname, state } = location
    return this._set({ pathname, hidden: state.hidden, string: state.string })
  }

  render() {
    const { location } = this.props
    const { context } = this.state
    if (location.state.hidden) context.hidden = location.state.hidden
    else context.hidden = {}
    if (location.state.string) context.string = location.state.string
    else context.string = {}
    context.pathname = location.pathname
    return <UrlContext.Provider value={context}>{this.props.children}</UrlContext.Provider>
  }
}
