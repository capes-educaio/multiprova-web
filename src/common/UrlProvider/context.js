import React from 'react'
import { withContext } from 'utils/context'

export const UrlContext = React.createContext({
  hidden: null,
  string: null,
  pathname: null,
  replace: null,
  set: null,
  update: null,
})

export const withUrlContext = withContext(UrlContext, 'urlContext')
