import PropTypes from 'prop-types'

export const propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
