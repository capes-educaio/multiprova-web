import React, { Component } from 'react'

import classnames from 'classnames'
import { Paper } from '@material-ui/core'
import { propTypes } from './propTypes'

export class MpActionBarComponent extends Component {
  static propTypes = propTypes

  render() {
    const { children, classes, className } = this.props
    return (
      <Paper className={classnames(classes.root, { [className]: className })}>
        <div className={classes.filling} />
        {children}
      </Paper>
    )
  }
}
