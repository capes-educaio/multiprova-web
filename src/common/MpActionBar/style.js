export const style = theme => ({
  root: {
    boxShadow: 'none',
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'flex-end',
    '& svg': {
      fill: theme.palette.primary.main,
    },
  },
  filling: {
    [theme.breakpoints.up(480)]: {
      flexGrow: 1,
    },
  },
})
