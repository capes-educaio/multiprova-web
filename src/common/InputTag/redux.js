import { bindActionCreators } from 'redux'
import { actions } from 'utils/actions'

export function mapStateToProps(state) {
  return {
    listaTags: state.listaTags,
    strings: state.strings,
  }
}

export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      listaTagsPush: actions.push.listaTags,
    },
    dispatch,
  )
}
