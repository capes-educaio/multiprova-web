import PropTypes from 'prop-types'

const tagType = PropTypes.shape({
  nome: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  isNew: PropTypes.bool,
})

export const propTypes = {
  valorInicial: PropTypes.array,
  valor: PropTypes.array,
  onChange: PropTypes.func,
  handleKeyPress: PropTypes.func,
  // style
  classes: PropTypes.object.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
  // redux state
  listaTags: PropTypes.arrayOf(tagType),
  // redux actions
  listaTagsPush: PropTypes.func.isRequired,
}
