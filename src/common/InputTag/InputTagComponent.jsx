import React from 'react'
import { getInputTagCreateOption } from './getInputTagCreateOption'
import { propTypes } from './propTypes'
import { CreatableSelect } from 'common/Autosuggest'

export class InputTagComponent extends React.Component {
  static propTypes = propTypes

  constructor(props) {
    super(props)
    const { getTagsConfig } = this
    const { listaTags } = props
    const tagsConfiguradas = listaTags ? getTagsConfig(listaTags) : []
    const valor = props.valor ? getTagsConfig(props.valor) : getTagsConfig(props.valorInicial)
    this.state = {
      tagsConfiguradas,
      single: null,
      newOption: [],
      inputValue: '',
      todasTagsDoUsuario: [],
      valor,
    }
    if (props.getClear) props.getClear(this._clear)
  }

  _listaTagsIguais = (antes, depois) => {
    if (antes === depois) return true

    if (!antes || !depois) return false

    const listaTagsAntes = antes.map(tag => tag.id)
    const listaTagsDepois = depois.map(tag => tag.id)
    return (
      listaTagsAntes.length === listaTagsDepois.length &&
      listaTagsAntes.sort().every(function(value, index) {
        return value === listaTagsDepois.sort()[index]
      })
    )
  }

  _atualizarTagComALista = lista => {
    const { valor } = this.state
    for (let i = 0; i < valor.length; i += 1) {
      lista.forEach(tag => {
        if (valor[i].nome === tag.nome) {
          valor[i].id = tag.id
          valor[i].value = tag.id
          valor[i].isNew = false
        }
      })
      this.setState({ valor })
    }
  }

  UNSAFE_componentWillReceiveProps = newProps => {
    const { configuraTagESalvaNoEstado } = this
    const { listaTags, valor } = this.props

    if (!this._listaTagsIguais(listaTags, newProps.listaTags)) {
      configuraTagESalvaNoEstado('tagsConfiguradas', newProps.listaTags)
      this._atualizarTagComALista(newProps.listaTags)
    }
    if (!this._listaTagsIguais(valor, newProps.valor)) configuraTagESalvaNoEstado('valor', newProps.valor)
  }

  componentWillUnmount = () => {
    if (this.props.getClear) this.props.getClear(null)
  }

  _clear = () => this.setState({ valor: [] })

  configuraTagESalvaNoEstado = (nomeDoCampoDoState, tags) => {
    const tagConfig = this.getTagsConfig(tags)
    this.setState({
      [nomeDoCampoDoState]: tagConfig,
    })
  }

  getTagConfig = tag => ({
    value: tag.id,
    label: tag.nome,
    ...tag,
  })

  getTagsConfig = tagsToConfig => {
    const { getTagConfig } = this
    let tagsConfiguradas = []
    if (tagsToConfig) {
      tagsConfiguradas = tagsToConfig.map(getTagConfig)
    }
    return tagsConfiguradas
  }

  getTagEnvio = tag => ({ id: tag.id, nome: tag.nome, isNew: Boolean(tag.isNew) })

  getTagsEnvio = tagsConfiguradas => tagsConfiguradas.map(this.getTagEnvio)

  handleChangeTagsConfiguradas = valor => {
    const { avisarMudancaPorOnChange } = this
    this.setState(
      {
        valor,
      },
      avisarMudancaPorOnChange,
    )
  }

  avisarMudancaPorOnChange = () => {
    const { getTagsEnvio } = this
    const { onChange } = this.props
    const { valor } = this.state
    const tagsEnvio = getTagsEnvio(valor)
    onChange(tagsEnvio)
  }

  handleCreate = inputValue => {
    const { handleChangeTagsConfiguradas } = this
    const { listaTagsPush } = this.props
    const { valor } = this.state
    const { novaTag, novaTagConfigurada } = getInputTagCreateOption(inputValue)
    const novasTagsConfiguradas = [...valor, novaTagConfigurada]
    listaTagsPush(novaTag)
    handleChangeTagsConfiguradas(novasTagsConfiguradas)
  }

  formatCreate = inputValue => {
    return (
      <p>
        {this.props.strings.inserir} "{inputValue}"
      </p>
    )
  }

  render() {
    const { handleChangeTagsConfiguradas, handleCreate, formatCreate } = this
    const { strings, listaTags, placeholder, handleKeyPress } = this.props
    const { tagsConfiguradas, valor } = this.state
    const todasTagsDoUsuario = tagsConfiguradas
    const temListaTags = Boolean(listaTags)
    return (
      <CreatableSelect
        options={todasTagsDoUsuario}
        value={valor}
        onKeyDown={handleKeyPress}
        noOptionsMessage={() => strings.insiraTag}
        onCreateOption={handleCreate}
        onChange={handleChangeTagsConfiguradas}
        placeholder={placeholder ? placeholder : strings.pesquisarPorTags}
        noOptionsMessageCSS={strings.naoExiste}
        formatCreateLabel={formatCreate}
        isClearable
        isMulti
        compo
        isLoading={!temListaTags}
        loadingMessage={() => strings.carregando}
      />
    )
  }
}
