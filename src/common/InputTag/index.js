import { compose } from 'redux'
import { withStyles } from '@material-ui/core/styles'
import { connect } from 'react-redux'

import { InputTagComponent } from './InputTagComponent'
import { style } from './style'
import { mapStateToProps, mapDispatchToProps } from './redux'

export const InputTag = compose(
  withStyles(style),
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(InputTagComponent)
