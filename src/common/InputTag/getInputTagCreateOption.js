import uid from 'uuid/v4'

export const getInputTagCreateOption = label => {
  const novaTag = {
    nome: label,
    id: uid(),
    isNew: true,
  }
  const novaTagConfigurada = {
    label,
    value: novaTag.id,
    ...novaTag,
  }
  return { novaTag, novaTagConfigurada }
}
