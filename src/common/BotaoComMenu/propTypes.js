import PropTypes from 'prop-types'

export const propTypes = {
  propsPopover: PropTypes.object,
  children: PropTypes.any,
  menuList: PropTypes.object,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
