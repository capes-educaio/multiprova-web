import React, { Component } from 'react'
import classnames from 'classnames'

import Popover from '@material-ui/core/Popover'

import { propTypes } from './propTypes'

export class BotaoComMenuComponent extends Component {
  static propTypes = propTypes

  constructor(props) {
    super(props)
    this.state = {
      anchorMenu: null,
      temMenu: true,
    }
  }

  UNSAFE_componentWillMount() {
    if (this.props.temMenu === false) {
      this.setState({
        temMenu: this.props.temMenu,
      })
    }
  }

  abrirMenu = event => {
    if (this.state.temMenu) {
      this.setState({
        anchorMenu: event.currentTarget,
      })
    }
  }

  fecharMenu = event => {
    this.setState({
      anchorMenu: null,
    })
  }

  render() {
    const { children, menuList, propsPopover, classes } = this.props
    const { anchorMenu } = this.state
    return (
      <div className={classnames(this.props.menuItem, classes.classeBotao)}>
        <div className={classnames(this.props.menuItem, classes.classeBotao)} onClick={this.abrirMenu}>
          {children}
        </div>
        <Popover
          open={Boolean(anchorMenu)}
          anchorEl={anchorMenu}
          onClick={this.fecharMenu}
          onClose={this.fecharMenu}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'right',
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'right',
          }}
          {...propsPopover}
        >
          {menuList}
        </Popover>
      </div>
    )
  }
}
