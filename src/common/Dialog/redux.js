import { bindActionCreators } from 'redux'
import { actions } from 'utils/actions'

export function mapStateToProps(state) {
  return {
    strings: state.strings,
    listaDialog: state.listaDialog,
  }
}

export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      listaDialogShift: actions.shift.listaDialog,
    },
    dispatch,
  )
}
