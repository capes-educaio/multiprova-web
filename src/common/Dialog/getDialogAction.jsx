import React from 'react'
import Button from '@material-ui/core/Button'

export const getDialogAction = closeDialog => (actionConfig, index) => {
  const { onClick, autoFocus, id } = actionConfig
  const onClickPersonalizado = event => {
    event.stopPropagation()
    if (onClick) onClick()
    closeDialog()
  }
  return (
    <Button id={id} key={index} onClick={onClickPersonalizado} color="primary" autoFocus={autoFocus || false}>
      {actionConfig.texto}
    </Button>
  )
}
