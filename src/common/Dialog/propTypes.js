import PropTypes from 'prop-types'

export const propTypes = {
  // redux state
  listaDialog: PropTypes.array.isRequired,
  // redux actions
  listaDialogShift: PropTypes.func.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
