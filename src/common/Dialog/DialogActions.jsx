import React from 'react'

import MaterialDialogActions from '@material-ui/core/DialogActions'

import { getDialogAction } from './getDialogAction'

export const DialogActions = ({ config, closeDialog }) => (
  <MaterialDialogActions>{config.map(getDialogAction(closeDialog))}</MaterialDialogActions>
)
