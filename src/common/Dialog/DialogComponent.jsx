import React from 'react'
import PropTypes from 'prop-types'

import Dialog from '@material-ui/core/Dialog'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import Slide from '@material-ui/core/Slide'

import { DialogActions } from './DialogActions'

export class DialogComponent extends React.Component {
  static propTypes = {
    // redux state
    listaDialog: PropTypes.array.isRequired,
    // redux actions
    listaDialogShift: PropTypes.func.isRequired,
    // classes
    classes: PropTypes.object.isRequired,
  }

  transitionComponent = props => <Slide direction="up" {...props} />

  render() {
    const { listaDialog, listaDialogShift, classes } = this.props
    const dialogAtual = listaDialog[0] ? listaDialog[0] : {}
    const { titulo, texto, actions } = dialogAtual
    const BackdropProps = { classes: { root: classes.backdropRoot } }
    return (
      <Dialog
        id="dialog"
        open={listaDialog.length > 0}
        TransitionComponent={this.transitionComponent}
        keepMounted
        onClose={listaDialogShift}
        BackdropProps={BackdropProps}
        PaperProps={{ elevation: 2 }}
      >
        {titulo && <DialogTitle>{titulo}</DialogTitle>}
        {texto && (
          <DialogContent className={classes.dialogContent}>
            <DialogContentText>{texto}</DialogContentText>
          </DialogContent>
        )}
        {actions && <DialogActions config={actions} closeDialog={listaDialogShift} />}
      </Dialog>
    )
  }
}
