export const style = theme => ({
  backdropRoot: {
    opacity: '0.2 !important',
  },
  dialogContent: {
    textAlign: 'justify',
  },
})
