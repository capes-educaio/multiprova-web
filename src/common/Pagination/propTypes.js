import PropTypes from 'prop-types'

export const propTypes = {
  quantidadeTotal: PropTypes.number.isRequired,
  onChangePagina: PropTypes.func,
  onChangeQuantidadePorPagina: PropTypes.func,
  quantidadePorPaginaInicial: PropTypes.number,
  paginaInicial: PropTypes.number,
  quantidadePorPaginaOpcoes: PropTypes.array,
  paginaAtual: PropTypes.number,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
