import React, { Component } from 'react'

import Table from '@material-ui/core/Table'
import TableFooter from '@material-ui/core/TableFooter'
import TablePagination from '@material-ui/core/TablePagination'
import TableRow from '@material-ui/core/TableRow'

import { propTypes } from './propTypes'

export class PaginationComponent extends Component {
  static propTypes = propTypes

  constructor(props) {
    super(props)
    this.state = {
      quantidadePorPagina: props.quantidadePorPaginaInicial || 10,
    }
  }

  onChangePagina = (e, numeroDaPagina) => {
    const { onChangePagina } = this.props
    if (onChangePagina) onChangePagina(numeroDaPagina)
  }

  onChangeQuantidadePorPagina = e => {
    const { onChangeQuantidadePorPagina } = this.props
    const quantidadePorPagina = e.target.value
    this.setState({ quantidadePorPagina })
    if (onChangeQuantidadePorPagina) this.props.onChangeQuantidadePorPagina(quantidadePorPagina)
  }

  labelDisplayedRows = ({ from, to, count }) => `${from}-${to} ${this.props.strings.de} ${count}`

  render() {
    const { classes, strings, quantidadeTotal, quantidadePorPaginaOpcoes, paginaAtual } = this.props
    const { quantidadePorPagina } = this.state
    return (
      <Table>
        <TableFooter>
          <TableRow>
            <TablePagination
              classes={{ toolbar: classes.toolbar }}
              labelRowsPerPage={strings.porPagina}
              count={quantidadeTotal}
              rowsPerPage={quantidadePorPagina}
              page={paginaAtual === null ? 0 : paginaAtual}
              onChangePage={this.onChangePagina}
              onChangeRowsPerPage={this.onChangeQuantidadePorPagina}
              rowsPerPageOptions={quantidadePorPaginaOpcoes || [10, 20, 50, 100]}
              labelDisplayedRows={this.labelDisplayedRows}
            />
          </TableRow>
        </TableFooter>
      </Table>
    )
  }
}
