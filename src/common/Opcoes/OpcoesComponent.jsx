import React, { Component } from 'react'
import uid from 'uuid/v4'

import IconButton from '@material-ui/core/IconButton'
import Menu from '@material-ui/core/Menu'

import MoreVertIcon from '@material-ui/icons/MoreVert'
import { propTypes } from './propTypes'

export class OpcoesComponent extends Component {
  static propTypes = propTypes

  static defaultProps = {
    icon: <MoreVertIcon style={{ fontSize: 20 }} />,
    anchorOrigin: { vertical: 'top', horizontal: 'right' },
    transformOrigin: { vertical: 'top', horizontal: 'right' },
  }

  _id = uid()

  constructor(props) {
    super(props)
    this.state = {
      anchorEl: null,
    }
    if (props.onRef) props.onRef(this)
  }

  componentWillUnmount = () => {
    if (this.props.onRef) this.props.onRef(null)
  }

  close = event => {
    event && event.stopPropagation()
    this.setState({ anchorEl: null })
  }

  _handleClickMenu = event => {
    event.stopPropagation()
    const { onClick } = this.props
    this.setState({ anchorEl: event.currentTarget })
    if (onClick) onClick()
  }

  render() {
    const { children, icon, anchorOrigin, transformOrigin } = this.props
    const { anchorEl } = this.state
    return [
      <IconButton key={'icon' + this._id} onClick={this._handleClickMenu}>
        {icon}
      </IconButton>,
      <Menu
        key={'menu' + this._id}
        anchorEl={anchorEl}
        getContentAnchorEl={null}
        anchorOrigin={anchorOrigin}
        transformOrigin={transformOrigin}
        open={Boolean(anchorEl)}
        onClose={this.close}
      >
        {children}
      </Menu>,
    ]
  }
}
