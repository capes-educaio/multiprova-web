import PropTypes from 'prop-types'

export const propTypes = {
  children: PropTypes.node.isRequired,
  icon: PropTypes.node.isRequired,
  anchorOrigin: PropTypes.object.isRequired,
  transformOrigin: PropTypes.object.isRequired,
  onClick: PropTypes.func,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
