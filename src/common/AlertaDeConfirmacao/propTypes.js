import PropTypes from 'prop-types'

export const propTypes = {
  alertaAberto: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  descricaoSelected: PropTypes.string,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
