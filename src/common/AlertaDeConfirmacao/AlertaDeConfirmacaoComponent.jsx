import React, { Component } from 'react'

import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import { propTypes } from './propTypes'

export class AlertaDeConfirmacaoComponent extends Component {
  static propTypes = propTypes

  render() {
    const { classes, alertaAberto, handleClose, descricaoSelected, stringNomeInstancia, strings } = this.props
    return (
      <Dialog
        role="dialog"
        aria-labelledby="dialogTitleConfimation"
        aria-describedby="dialogContentConfimation"
        disableBackdropClick
        disableEscapeKeyDown
        open={alertaAberto}
        onClose={handleClose}
        classes={{ root: classes.root, paper: classes.paper }}
      >
        <DialogTitle id="dialogTitleConfimation">{stringNomeInstancia}</DialogTitle>
        {!!descricaoSelected && (
          <DialogContent>
            <DialogContentText id="dialogContentConfimation">{descricaoSelected}</DialogContentText>
          </DialogContent>
        )}
        <DialogActions>
          <Button tabIndex="1" id="sim-confirmar" onClick={() => handleClose(true)}>
            {strings.sim}
          </Button>
          <Button tabIndex="2" id="nao-confirmar" onClick={() => handleClose(false)} autoFocus>
            {strings.nao}
          </Button>
        </DialogActions>
      </Dialog>
    )
  }
}
