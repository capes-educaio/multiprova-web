export const style = theme => ({
  root: {
    padding: '20px 20px 20px 20px',
  },
  paper: {
    border: `1px solid ${theme.palette.defaultText}`,
  },
})
