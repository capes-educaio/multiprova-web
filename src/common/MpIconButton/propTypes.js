import PropTypes from 'prop-types'

export const propTypes = {
  //define se as bordas tem um leve arredondamento, estilo multiprova.
  round: PropTypes.bool,
  IconComponent: PropTypes.any.isRequired,
  onClick: PropTypes.func,
  loadOnClick: PropTypes.bool,
  disabled: PropTypes.bool,
  color: PropTypes.oneOf(['steelBlue', 'fundoCinza', 'darkBlue']),
  className: PropTypes.string,
  tooltip: PropTypes.node,
  tooltipProps: PropTypes.object,
  iconProps: PropTypes.object,
  onRef: PropTypes.func,
  // aplica esses props no Button do materialUI
  buttonProps: PropTypes.object,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
  // theme
  theme: PropTypes.object.isRequired,
}
