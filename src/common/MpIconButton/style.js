export const style = theme => ({
  root: {
    borderRadius: 0,
    padding: 5,
    fontWeight: 300,
    backgroundColor: theme.palette.gelo,
    color: theme.palette.cinzaEscuro,
    width: 34,
    height: 34,
    '&:hover': {
      backgroundColor: theme.palette.gelo,
      filter: 'brightness(85%)',
    },
  },
  round: { borderRadius: 5 },
  icon: {
    color: theme.palette.cinzaEscuro,
    fill: theme.palette.cinzaEscuro,
  },
  steelBlueRoot: {
    backgroundColor: theme.palette.steelBlue,
    color: theme.palette.steelBlueContrast,
    '&:hover': {
      backgroundColor: theme.palette.primary.main,
    },
  },
  darkBlueRoot: {
    backgroundColor: theme.palette.darkBlue,
    color: theme.palette.steelBlueContrast,
    '&:hover': {
      backgroundColor: theme.palette.steelBlue,
      filter: 'brightness(100%)',
    },
  },
  darkBlueIcon: {
    color: theme.palette.common.white,
    fill: theme.palette.common.white,
  },
  steelBlueIcon: {
    color: theme.palette.steelBlueContrast,
    fill: theme.palette.steelBlueContrast,
  },
  fundoCinzaRoot: {
    backgroundColor: theme.palette.fundo.cinza,
    '&:hover': {
      backgroundColor: theme.palette.fundo.cinza,
    },
  },
  fundoCinzaIcon: {
    color: theme.palette.cinzaDusty,
    fill: theme.palette.cinzaDusty,
  },
})
