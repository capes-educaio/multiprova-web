import React, { Component } from 'react'
import classnames from 'classnames'

import IconButton from '@material-ui/core/IconButton'
import CircularProgress from '@material-ui/core/CircularProgress'

import { MpButtonProvider } from 'common/MpButtonProvider'

import { propTypes } from './propTypes'

export class MpIconButtonComponent extends Component {
  static propTypes = propTypes

  render() {
    const {
      loadOnClick,
      round,
      IconComponent,
      classes,
      color,
      className,
      iconProps,
      tooltip,
      tooltipProps,
      onRef,
      buttonProps: buttonPropsNaoTratado,
      onClick: onClickNaoTratado,
      disabled,
      id,
    } = this.props
    const iconClassName = classnames(classes.icon, {
      [classes.steelBlueIcon]: color === 'steelBlue',
      [classes.fundoCinzaIcon]: color === 'fundoCinza',
      [classes.darkBlueIcon]: color === 'darkBlue',
    })
    return (
      <MpButtonProvider
        buttonProps={buttonPropsNaoTratado}
        onClick={onClickNaoTratado}
        tooltip={tooltip}
        tooltipProps={tooltipProps}
        onRef={onRef}
        loadOnClick={loadOnClick}
      >
        {({ onClick, buttonProps, loading }) => {
          return (
            <IconButton
              classes={{
                root: classnames(classes.root, {
                  [classes.round]: round,
                  [classes.steelBlueRoot]: color === 'steelBlue',
                  [classes.fundoCinzaRoot]: color === 'fundoCinza',
                  [classes.darkBlueRoot]: color === 'darkBlue',
                  [className]: Boolean(className),
                }),
              }}
              onClick={onClick}
              disabled={disabled || loading}
              id={id}
              {...buttonProps}
            >
              {IconComponent && !loading && <IconComponent className={iconClassName} {...iconProps} />}
              {loading && <CircularProgress color="primary" size={20} className={iconClassName} />}
            </IconButton>
          )
        }}
      </MpButtonProvider>
    )
  }
}
