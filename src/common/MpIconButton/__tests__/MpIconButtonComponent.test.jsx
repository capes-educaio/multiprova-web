import React from 'react'

import { MpIconButtonComponent } from '../MpIconButtonComponent'
import { mountTest } from 'localModules/testUtils/mountTest'

const requiredProps = {
  IconComponent: () => <div />,
  // style
  classes: {},
  // strings
  strings: {},
  // theme
  theme: { palette: {} },
}

const optionalProps = {
  color: 'steelBlue',
  onClick: jest.fn(),
}

const arrayOfProps = [requiredProps, { ...requiredProps, ...optionalProps }]

mountTest(MpIconButtonComponent, 'MpIconButtonComponent')(arrayOfProps)
