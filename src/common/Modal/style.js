export const style = theme => ({
  paper: {
    backgroundColor: 'rgb(240, 240, 247)',
    padding: '20px',
    width: 'fit-content',

    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-end',
  },

  botaoFechar: {
    marginTop: '10px',
    backgroundColor: theme.palette.gelo,
  },
})
