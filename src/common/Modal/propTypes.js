import PropTypes from 'prop-types'

export const propTypes = {
  children: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  fecharModal: PropTypes.func,
  open: PropTypes.bool,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
