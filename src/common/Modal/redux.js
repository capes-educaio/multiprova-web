import { bindActionCreators } from 'redux'
// import { actions } from 'utils/actions'

export function mapStateToProps(state) {
  return {
    strings: state.strings,
    isMobile: state.isMobile,
  }
}

export function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch)
}
