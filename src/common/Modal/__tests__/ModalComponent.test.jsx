import React from 'react'
import Enzyme, { mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import { ptBr } from 'utils/strings/ptBr'

import { ModalComponent } from '../ModalComponent'
import { mountTest } from 'localModules/testUtils/mountTest'

Enzyme.configure({ adapter: new Adapter() })

const requiredProps = {
  open: true,
  fecharModal: jest.fn(),
  // style
  classes: {},
  // strings
  strings: ptBr,
}

const arrayOfProps = [requiredProps]

mountTest(ModalComponent, 'ModalComponent')(arrayOfProps)

describe(`Testando botões`, () => {
  it(`button fecharModal`, () => {
    const wrapper = mount(<ModalComponent {...requiredProps} />)
    const botao = wrapper.find(`button#fecharModal`)
    botao.simulate('click')
    expect(requiredProps.fecharModal).toHaveBeenCalled()
    wrapper.unmount()
  })
})
