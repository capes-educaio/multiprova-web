import React, { Component } from 'react'

import Paper from '@material-ui/core/Paper'
import Dialog from '@material-ui/core/Dialog'
import Button from '@material-ui/core/Button'
import CancelIcon from '@material-ui/icons/Cancel'

import { propTypes } from './propTypes'

export class ModalComponent extends Component {
  static propTypes = propTypes

  render() {
    const { classes, children, open, strings, fecharModal, isMobile } = this.props
    return (
      <Dialog fullScreen={isMobile} open={open} maxWidth="lg">
        <Paper className={classes.paper} elevation={4}>
          {children}
          {fecharModal && (
            <Button
              className={classes.botaoFechar}
              color="default"
              onClick={fecharModal}
              id="fecharModal"
              aria-label="close"
            >
              <CancelIcon />
              {strings.fechar}
            </Button>
          )}
        </Paper>
      </Dialog>
    )
  }
}
