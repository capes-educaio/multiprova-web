import PropTypes from 'prop-types'

export const propTypes = {
  tema: PropTypes.string.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
