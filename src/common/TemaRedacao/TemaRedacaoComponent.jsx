import React, { Component } from 'react'

import Typography from '@material-ui/core/Typography'

import { MpParser } from 'common/MpParser'

import { propTypes } from './propTypes'

export class TemaRedacaoComponent extends Component {
  static propTypes = propTypes

  render() {
    const { tema, strings, classes } = this.props
    return (
      <div className={classes.temaWrapper}>
        <Typography className={classes.tema}>{strings.tema}</Typography>
        <MpParser>{tema}</MpParser>
      </div>
    )
  }
}
