import PropTypes from 'prop-types'

export const propTypes = {
  onChange: PropTypes.func.isRequired,
  onFocus: PropTypes.func,
  onBlur: PropTypes.func,
  required: PropTypes.bool,
  children: PropTypes.any,
  actions: PropTypes.array,
  default: PropTypes.any,
  grande: PropTypes.bool,
  // config.props
  label: PropTypes.string,
  decorarCorreta: PropTypes.bool,
  inline: PropTypes.bool,
  error: PropTypes.bool,
  helperText: PropTypes.string,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
