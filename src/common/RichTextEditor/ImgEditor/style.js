export const style = () => ({
  editor: {
    display: 'inline-flex',
    flexDirection: 'column',
    backgroundColor: 'white',
    zIndex: 1,
    border: '1px solid lightgray',
    borderRadius: '5px',
    boxShadow: '1px 1px 10px lightgray',
    maxWidth: 500,
  },
  botaoUpload: {
    marginTop: '8px',
  },

  inputContainer: {
    padding: '0px 0px 5px 5px',
    display: 'flex',
  },
})
