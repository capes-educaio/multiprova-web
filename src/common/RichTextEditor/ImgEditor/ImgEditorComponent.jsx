import React, { Component } from 'react'
import ReactFileReader from 'react-file-reader'

import Cancel from '@material-ui/icons/Cancel'
import { IconButton, ButtonBase, Button } from '@material-ui/core/'

import { propTypes } from './propTypes'

export class ImgEditorComponent extends Component {
  static propTypes = propTypes

  isEdition = false

  constructor(props) {
    super(props)
    this.ref = React.createRef()
  }

  sendImg = files => {
    const image = files.base64
    const { onChange, onInsert, onCancel } = this.props
    if (this.isEdition) onChange(image)
    else if (image === '') onCancel()
    else onInsert(image)
    onCancel()
  }

  render() {
    const { classes, onCancel, strings } = this.props
    return (
      <div ref={this.ref} className={classes.editor}>
        <div className={classes.inputContainer}>
          <ReactFileReader handleFiles={this.sendImg} base64={true}>
            <ButtonBase type="file" aria-label="Escolher Imagem" />
            <Button color="primary" className={classes.botaoUpload}>
              {strings.escolherImg}
            </Button>
          </ReactFileReader>
          <IconButton onClick={onCancel} aria-label="Cancelar">
            <Cancel />
          </IconButton>
        </div>
      </div>
    )
  }
}
