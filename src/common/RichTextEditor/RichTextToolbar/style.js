const toolbar = theme => ({
  boxShadow: '0px 2px 2px lightgrey',
  borderTop: `5px solid ${theme.palette.primary.main}`,
  borderRadius: '4px 4px 0px 0px',
})

export const style = theme => ({
  controls: {
    display: 'flex',
    fontSize: '14px',
    userSelect: 'none',
    alignItems: 'center',
  },
  verticalRuler: {
    borderLeft: `1px solid #ddd`,
    height: '20px',
    margin: '0 8px',
  },
  toolbar: {
    ...toolbar(theme),
    position: 'absolute',
    backgroundColor: 'white',
    border: '1px solid #ddd',
    zIndex: 1,
    borderRadius: '5px',
    paddingTop: '2px',
    top: '-35px',
    right: '20px',
    borderTop: `5px solid ${theme.palette.primary.main}`,
    boxShadow: 'none',
  },
  toolbarInline: {
    ...toolbar(theme),
    width: '100%',
  },
  toolbarGroup: {
    padding: '3px 0 0 0',
  },
})
