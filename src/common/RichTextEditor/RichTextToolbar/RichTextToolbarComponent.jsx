import React, { Component } from 'react'

import { StyleButton } from '../StyleButton'
import { blockTypes } from '../blockTypes'
import { estilosInline } from '../estilosInline'

import { propTypes } from './propTypes'

export class RichTextToolbarComponent extends Component {
  static propTypes = propTypes

  render() {
    const {
      classes,
      inline,
      currentStyle,
      currentBlock,
      onToggleBlockType,
      onToggleInlineStyle,
      isHover,
      isActive,
      actions,
    } = this.props
    const mostrarToolBar = inline || (isHover || isActive)
    if (mostrarToolBar)
      return (
        <div className={inline ? classes.toolbarInline : classes.toolbar}>
          <div className={classes.controls}>
            <div className={classes.toolbarGroup}>
              {estilosInline.map(({ style, icon }, index) => (
                <StyleButton
                  key={index}
                  active={currentStyle.has(style)}
                  icon={icon}
                  onToggle={onToggleInlineStyle}
                  style={style}
                />
              ))}
              {blockTypes.map(({ block, icon }, index) => (
                <StyleButton
                  key={index}
                  active={currentBlock === block}
                  icon={icon}
                  onToggle={onToggleBlockType}
                  block={block}
                />
              ))}
            </div>
            {actions && <div className={classes.verticalRuler} />}
            {actions && (
              <div className={classes.toolbarGroup}>
                {actions.map((action, index) => (
                  <StyleButton key={index} icon={action.icon} onClick={action.onClick} />
                ))}
              </div>
            )}
          </div>
        </div>
      )
    else return null
  }
}
