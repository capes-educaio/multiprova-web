import PropTypes from 'prop-types'

export const propTypes = {
  onToggleInlineStyle: PropTypes.func.isRequired,
  onToggleBlockType: PropTypes.func.isRequired,
  currentStyle: PropTypes.any,
  currentBlock: PropTypes.any,
  inline: PropTypes.bool,
  isHover: PropTypes.bool,
  isActive: PropTypes.bool,
  actions: PropTypes.array,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
