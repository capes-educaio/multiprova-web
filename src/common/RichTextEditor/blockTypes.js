import React from 'react'
import { Functions, AddAPhoto } from '@material-ui/icons/'

import { TEX_BLOCK, IMG_BLOCK } from 'utils/RichText'

export const blockTypes = [{ icon: <AddAPhoto />, block: IMG_BLOCK }, { icon: <Functions />, block: TEX_BLOCK }]
