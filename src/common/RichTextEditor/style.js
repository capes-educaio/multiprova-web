const root = theme => ({
  position: 'relative',
  margin: '15px 0 0 0',
  backgroundColor: 'white',
  border: '1px solid #ddd',
  borderRadius: '4px 4px 2px 2px',
})

const areaDeTexto = {
  display: 'flex',
  justifyContent: 'space-between',
  padding: '10px',
  cursor: 'text',
  '& .DraftEditor-root': {
    alignSelf: 'center',
    width: '100%',
    overflow: 'hidden',
  },
  '& .DraftEditor-root .public-DraftEditor-content': {
    wordWrap: 'break-word',
  },
  '& .DraftEditor-root .katex-display': {
    cursor: 'pointer',
    userSelect: 'none',
    '&:hover': {
      backgroundColor: 'rgb(245,245,245)',
    },
  },
  '& .DraftEditor-root figure': {
    margin: 0,
    textAlign: 'center',
  },
}

const areaDeTextoGrande = {
  minHeight: 60,
  '& .DraftEditor-root .public-DraftEditor-content': {
    minHeight: 60,
    wordWrap: 'break-word',
  },
}

const cssLabel = {
  margin: '4px 0 0 10px',
  zIndex: 1,
  cursor: 'text',
  '&$cssFocused': {
    cursor: 'default',
    margin: '0 0 0 0',
  },
}

export const style = theme => ({
  formControl: {
    width: '100%',
  },
  root: root(theme),
  rootDecoradaCorreta: {
    ...root(theme),
    borderColor: theme.palette.decorarCorreta,
  },
  areaDeTexto,
  areaDeTextoDecoradaCorreta: {
    ...areaDeTexto,
    '& .DraftEditor-root': {
      alignSelf: 'center',
      width: `calc(100% - 24px)`,
      overflow: 'hidden',
    },
  },
  areaDeTextoGrande: {
    ...areaDeTexto,
    ...areaDeTextoGrande,
  },
  cssLabel,
  cssLabelInline: {
    ...cssLabel,
    margin: '38px 0 0 10px',
  },
  cssFocused: {},
  textBoxIcon: {
    alignSelf: 'center',
    color: theme.palette.decorarCorreta,
  },
  mathEditor: {
    display: 'flex',
    justifyContent: 'center',
    marginTop: '10px',
  },
})
