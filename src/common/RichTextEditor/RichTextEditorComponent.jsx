import React, { Component } from 'react'
import { Editor, EditorState, RichUtils, Modifier, AtomicBlockUtils } from 'draft-js'

import Typography from '@material-ui/core/Typography'
import FormControl from '@material-ui/core/FormControl'
import InputLabel from '@material-ui/core/InputLabel'
import FormHelperText from '@material-ui/core/FormHelperText'
import Collapse from '@material-ui/core/Collapse'

import DoneIcon from '@material-ui/icons/Done'

import { AtomicBlock } from 'common/AtomicBlock'

import { languages } from 'utils/strings'
import { TYPE_TEX, TEX_BLOCK, IMG_BLOCK, TYPE_IMG } from 'utils/RichText'
import { draftParaHtml, htmlParaDraft } from 'utils/conversoesDraft'

import { RichTextToolbar } from './RichTextToolbar'
import { MathEditor } from './MathEditor'
import { ImgEditor } from './ImgEditor'
import { propTypes } from './propTypes'

export class RichTextEditorComponent extends Component {
  static defaultProps = {
    tabCharacter: '    ',
  }

  static propTypes = propTypes

  static emptyStateContent = JSON.stringify(draftParaHtml(EditorState.createEmpty().getCurrentContent()))

  constructor(props) {
    super(props)
    this.state = {
      editorState: props.default
        ? EditorState.createWithContent(htmlParaDraft(props.default))
        : EditorState.createEmpty(),
      isHover: false,
      isActive: false,
      showMathEditor: false,
      showImgEditor: false,
      entityKey: null,
      entityContent: null,
    }
  }

  editor = React.createRef()

  componentDidMount = () => {
    this.editor.current.addEventListener('mouseenter', () => {
      this.setState({ isHover: true })
    })
    this.editor.current.addEventListener('mouseleave', () => {
      this.setState({ isHover: false })
    })
    const input = this.editor.current.querySelector('[contenteditable=true]')
    input.addEventListener('focus', () => {
      this.setState({ isActive: true })
    })
    input.addEventListener('blur', () => {
      this.setState({ isActive: false })
    })
  }

  onChange = editorState => {
    const valueFormatoDraft = editorState.getCurrentContent()
    const valueFormatoHtml = draftParaHtml(valueFormatoDraft)
    this.props.onChange(valueFormatoHtml)
    this.setState({ editorState })
  }

  focusText = () => {
    this.refs.text.focus()
  }

  toggleInlineStyle = inlineStyle => {
    const { editorState } = this.state
    const newEditorState = RichUtils.toggleInlineStyle(editorState, inlineStyle)
    this.onChange(newEditorState)
  }

  toggleBlockType = blockType => {
    if (this.isAtomicBlock(blockType)) return
    const { editorState } = this.state
    const newEditorState = RichUtils.toggleBlockType(editorState, blockType)
    this.onChange(newEditorState)
  }

  isAtomicBlock = blockType => {
    switch (blockType) {
      case TEX_BLOCK:
        this.toggleMathEditor()
        return true
      case IMG_BLOCK:
        this.toggleImgEditor()
        return true
      default:
        return false
    }
  }
  toggleImgEditor = () => {
    const show = !this.state.showImgEditor
    this.setState({ showImgEditor: show })
    if (!show) {
      this.setState({ entityKey: null, entityContent: null })
    }
  }

  toggleMathEditor = () => {
    const show = !this.state.showMathEditor
    this.setState({ showMathEditor: show })
    if (!show) {
      this.setState({ entityKey: null, entityContent: null })
    }
  }

  hasContent = () => {
    const { editorState } = this.state
    if (editorState) {
      const { getCurrentContent } = editorState
      if (getCurrentContent) {
        const currentContent = JSON.stringify(draftParaHtml(editorState.getCurrentContent()))
        return RichTextEditorComponent.emptyStateContent !== currentContent
      } else console.error('Tem algo de errado com o estado draft, ele está sem getCurrentContent.')
    }
    return false
  }

  onTab = e => {
    e.preventDefault()
    const { tabCharacter } = this.props
    const { editorState } = this.state
    let newContentState = Modifier.replaceText(
      editorState.getCurrentContent(),
      editorState.getSelection(),
      tabCharacter,
    )
    const newEditorState = EditorState.push(editorState, newContentState, 'insert-characters')
    this.onChange(newEditorState)
  }

  handleKeyCommand = (command, editorState) => {
    const newState = RichUtils.handleKeyCommand(editorState, command)
    if (newState) {
      this.onChange(newState)
      return 'handled'
    }
    return 'not-handled'
  }

  blockRenderer = block => {
    if (block.getType() === 'atomic') {
      return {
        component: AtomicBlock,
        editable: false,
        props: { onClick: this.onClickAtomic, isStatic: false },
      }
    }
    return null
  }

  onClickAtomic = (type, entityKey, entityContent) => {
    this.setState({ entityKey, entityContent })
    switch (type) {
      case TYPE_TEX:
        if (!this.state.showMathEditor) {
          this.toggleMathEditor()
        }
        break
      case TYPE_IMG:
        if (!this.state.showImgEditor) {
          this.toggleImgEditor()
        }
        break
      default:
        this.logUnknownBlock()
    }
  }

  onChangeAtomic = type => content => {
    const { editorState } = this.state
    const contentState = editorState.getCurrentContent()
    const { entityKey } = this.state
    const newEntityData = contentState.replaceEntityData(entityKey, { content })
    const newEditorState = EditorState.createWithContent(newEntityData)
    this.onChange(newEditorState)
    this.handleChangeAtomic(type)
  }

  handleChangeAtomic = blockType => {
    switch (blockType) {
      case TEX_BLOCK:
        this.toggleMathEditor()
        break
      case IMG_BLOCK:
        this.toggleImgEditor()
        break
      default:
        this.logUnknownBlock()
    }
    this.setState({ entityKey: null, entityContent: null })
  }

  onInsertAtomic = type => content => {
    const { editorState } = this.state
    const contentState = editorState.getCurrentContent()
    const contentStateWithEntity = contentState.createEntity(type, 'IMMUTABLE', { content })
    const entityKey = contentStateWithEntity.getLastCreatedEntityKey()
    const newEditorState = EditorState.set(editorState, { currentContent: contentStateWithEntity })
    const editorStateWithAtomicBlock = AtomicBlockUtils.insertAtomicBlock(newEditorState, entityKey, ' ')
    this.onChange(editorStateWithAtomicBlock)
    this.handleInsertAtomic(type)
  }

  handleInsertAtomic = type => {
    switch (type) {
      case TYPE_TEX:
        this.toggleMathEditor()
        break
      case TYPE_IMG:
        this.toggleImgEditor()
        break
      default:
        this.logUnknownBlock()
    }
  }

  logUnknownBlock = () => {
    console.error('Tipo de bloco desconhecido')
  }

  render() {
    const { editorState, isHover, isActive, showMathEditor, showImgEditor, entityKey, entityContent } = this.state
    const {
      classes,
      children,
      required,
      inline,
      label,
      actions,
      decorarCorreta,
      onFocus,
      onBlur,
      helperText,
      error,
      grande,
    } = this.props
    const hasContent = this.hasContent()
    let areaDeTextoClass
    if (grande) areaDeTextoClass = classes.areaDeTextoGrande
    else if (decorarCorreta) areaDeTextoClass = classes.areaDeTextoDecoradaCorreta
    else areaDeTextoClass = classes.areaDeTexto
    return (
      <FormControl className={classes.formControl}>
        <InputLabel
          onClick={hasContent || isActive ? null : this.focusText}
          required={required}
          shrink={hasContent || isActive}
          focused={hasContent || isActive}
          FormLabelClasses={{
            root: inline ? classes.cssLabelInline : classes.cssLabel,
            focused: classes.cssFocused,
          }}
        >
          {label}
        </InputLabel>
        <div
          onKeyPress={this.onKeyPress}
          className={decorarCorreta ? classes.rootDecoradaCorreta : classes.root}
          ref={this.editor}
        >
          {children || null}
          <RichTextToolbar
            actions={actions}
            inline={inline}
            isHover={isHover && label !== languages.ptBr.novaAlternativa}
            isActive={isActive}
            onToggleInlineStyle={this.toggleInlineStyle}
            onToggleBlockType={this.toggleBlockType}
            currentStyle={editorState.getCurrentInlineStyle()}
            currentBlock={RichUtils.getCurrentBlockType(editorState)}
          />
          <Typography className={areaDeTextoClass} headlineMapping={{ body2: 'div' }}>
            <Editor
              className={classes.editor}
              editorState={editorState}
              onChange={this.onChange}
              ref="text"
              onTab={this.onTab}
              onFocus={onFocus}
              onBlur={onBlur}
              onClick={this.focusText}
              blockRendererFn={this.blockRenderer}
              handleKeyCommand={this.handleKeyCommand}
            />
            {decorarCorreta && <DoneIcon className={classes.textBoxIcon} />}
          </Typography>
        </div>
        <Collapse in={showMathEditor} timeout="auto" unmountOnExit>
          <div className={classes.mathEditor}>
            <MathEditor
              tex={entityKey ? entityContent : ''}
              onChange={this.onChangeAtomic(TEX_BLOCK)}
              onInsert={this.onInsertAtomic(TYPE_TEX)}
              onCancel={this.toggleMathEditor}
            />
          </div>
        </Collapse>
        <Collapse in={showImgEditor} timeout="auto" unmountOnExit>
          <div className={classes.mathEditor}>
            <ImgEditor
              img={entityKey ? entityContent : ''}
              onChange={this.onChangeAtomic(IMG_BLOCK)}
              onInsert={this.onInsertAtomic(TYPE_IMG)}
              onCancel={this.toggleImgEditor}
            />
          </div>
        </Collapse>
        {helperText && <FormHelperText error={error}>{helperText}</FormHelperText>}
      </FormControl>
    )
  }
}
