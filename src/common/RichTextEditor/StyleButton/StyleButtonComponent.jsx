import React, { Component } from 'react'

import { propTypes } from './propTypes'

export class StyleButtonComponent extends Component {
  static propTypes = propTypes

  onToggle = e => {
    const { onToggle, onClick, style, block } = this.props
    e.preventDefault()
    if (onToggle) {
      const type = style ? style : block
      onToggle(type)
    }
    if (onClick) onClick()
  }

  render() {
    const { classes, active } = this.props
    return (
      <span className={active ? classes.activeButton : classes.buttonDefault} onMouseDown={this.onToggle}>
        {this.props.icon}
      </span>
    )
  }
}
