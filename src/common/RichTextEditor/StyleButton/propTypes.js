import PropTypes from 'prop-types'

export const propTypes = {
  icon: PropTypes.any.isRequired,
  onToggle: PropTypes.func,
  active: PropTypes.bool,
  style: PropTypes.any,
  block: PropTypes.any,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
