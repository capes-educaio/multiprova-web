import React, { Component } from 'react'
import { InlineMath } from 'react-katex'

import Send from '@material-ui/icons/Send'
import Cancel from '@material-ui/icons/Cancel'
import IconButton from '@material-ui/core/IconButton'

import { Tabs } from 'common/Tabs'

import texCommands from 'utils/texCommands'

import { propTypes } from './propTypes'

export class MathEditorComponent extends Component {
  static propTypes = propTypes

  mathField
  isEdition = false

  constructor(props) {
    super(props)
    this.ref = React.createRef()
  }

  componentDidMount = () => {
    this.createMathField()
  }

  createMathField = () => {
    const { classes } = this.props
    const input = this.ref.current.querySelector('.' + classes.mathEditorInput)
    const that = this
    const config = {
      spaceBehavesLikeTab: false,
      handlers: {
        enter() {
          that.sendTex()
        },
      },
    }

    const MQ = window.MathQuill.getInterface(2)
    const mathField = MQ.MathField(input, config)
    const { tex } = this.props
    if (tex) {
      mathField.latex(tex)
      this.isEdition = true
    }
    this.mathField = mathField
  }

  command = ({ type, tex, keystrokes }) => () => {
    const { mathField } = this
    switch (type) {
      case 'cmd':
        mathField.cmd(tex)
        break
      case 'write':
        mathField.write(tex)
        break
      case 'typed':
        mathField.typedText(tex)
        break
      default:
        console.error('Comando mathQuill desconhecido')
    }
    if (keystrokes) {
      keystrokes.forEach(k => mathField.keystroke(k))
    }
    mathField.focus()
  }

  sendTex = () => {
    const tex = this.mathField.latex()
    const { onChange, onInsert, onCancel } = this.props
    if (this.isEdition) onChange(tex)
    else if (tex === '') onCancel()
    else onInsert(tex)
  }

  render() {
    const { classes, onCancel } = this.props
    const commandTabs = texCommands.map((commandTypes, index) => (
      <div key={index}>
        {commandTypes.map((command, index) => (
          <div key={index} className={classes.command} onClick={this.command(command)}>
            <InlineMath math={command.icon} />
          </div>
        ))}
      </div>
    ))
    return (
      <div ref={this.ref} className={classes.mathEditor}>
        <Tabs labels={['Básicos', 'Cálculo', 'Símbolos']}>{commandTabs}</Tabs>
        <div className={classes.inputContainer}>
          <span className={classes.mathEditorInput} />
          <IconButton onClick={this.sendTex} aria-label="Salvar">
            <Send />
          </IconButton>
          <IconButton onClick={onCancel} aria-label="Cancelar">
            <Cancel />
          </IconButton>
        </div>
      </div>
    )
  }
}
