import PropTypes from 'prop-types'

export const propTypes = {
  variant: PropTypes.oneOf(['material', 'mp']),
  IconComponent: PropTypes.any,
  onClick: PropTypes.func,
  loadOnClick: PropTypes.bool,
  loading: PropTypes.bool,
  disabled: PropTypes.bool,
  color: PropTypes.oneOf(['steelBlue']),
  marginLeft: PropTypes.bool,
  marginRight: PropTypes.bool,
  className: PropTypes.string,
  id: PropTypes.string,
  tooltip: PropTypes.node,
  tooltipProps: PropTypes.object,
  onRef: PropTypes.func,
  defaultIcon: PropTypes.oneOf(['salvar', 'cancelar']),
  // aplica esses props no Button do materialUI
  buttonProps: PropTypes.object,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
  // theme
  theme: PropTypes.object.isRequired,
}
