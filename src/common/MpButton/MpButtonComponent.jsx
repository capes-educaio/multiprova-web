import React, { Component } from 'react'
import classnames from 'classnames'

import Button from '@material-ui/core/Button'
import LinearProgress from '@material-ui/core/LinearProgress'
import SendIcon from '@material-ui/icons/Send'
import CancelIcon from '@material-ui/icons/Cancel'

import { MpButtonProvider } from 'common/MpButtonProvider'

import { propTypes } from './propTypes'
import { defaultProps } from './defaultProps'

export class MpButtonComponent extends Component {
  static propTypes = propTypes
  static defaultProps = defaultProps

  get _allVariantProps() {
    const { classes, className, marginLeft, marginRight, color, responsiveGrow } = this.props
    return {
      material: {
        classNameButton: classnames(classes.root, {
          [classes.marginLeft]: marginLeft,
          [classes.marginRight]: marginRight,
          [className]: Boolean(className),
          [classes.responsiveGrow]: responsiveGrow,
        }),
        classNameIcon: classes.icon,
      },
      mp: {
        classNameButton: classnames(classes.root, classes.rootMp, {
          [classes.marginLeft]: marginLeft,
          [classes.marginRight]: marginRight,
          [classes.steelBlueRoot]: color === 'steelBlue',
          [className]: Boolean(className),
          [classes.responsiveGrow]: responsiveGrow,
        }),
        classNameIcon: classnames(classes.icon, classes.iconMp, {
          [classes.steelBlueIcon]: color === 'steelBlue',
        }),
      },
    }
  }

  get _variantProps() {
    const variantProps = this._allVariantProps[this.props.variant]
    return variantProps ? variantProps : {}
  }

  get _classNameButton() {
    return this._variantProps.classNameButton
  }

  get _classNameIcon() {
    return this._variantProps.classNameIcon
  }

  get _defaultIcon() {
    switch (this.props.defaultIcon) {
      case 'salvar':
        return SendIcon
      case 'cancelar':
        return CancelIcon
      default:
        return null
    }
  }

  render() {
    let {
      id,
      children,
      classes,
      tooltip,
      tooltipProps,
      onRef,
      IconComponent,
      loadOnClick,
      defaultIcon,
      iconProps,
      disabled,
    } = this.props
    IconComponent = defaultIcon ? this._defaultIcon : IconComponent
    const buttonPropsNaoTratado = this.props.buttonProps
    const onClickNaoTratado = this.props.onClick
    const loadingFromProps = this.props.loading
    return (
      <MpButtonProvider
        buttonProps={buttonPropsNaoTratado}
        onClick={onClickNaoTratado}
        tooltip={tooltip}
        tooltipProps={tooltipProps}
        onRef={onRef}
        loadOnClick={loadOnClick}
      >
        {({ onClick, buttonProps, loading }) => {
          const isLoading = typeof loadingFromProps === 'boolean' ? loadingFromProps : loading
          return (
            <Button
              id={id}
              className={classnames(this._classNameButton, {
                'nao-esta-carregando': !isLoading,
                carregando: isLoading,
              })}
              classes={{ label: classes.label }}
              disabled={isLoading || disabled}
              onClick={isLoading || disabled ? null : onClick}
              {...buttonProps}
            >
              <div
                className={classnames(classes.content, {
                  [classes.contentLoading]: isLoading,
                })}
              >
                {IconComponent && <IconComponent className={this._classNameIcon} {...iconProps} />}
                {children}
              </div>
              {isLoading && <LinearProgress className={classes.linearProgress} />}
            </Button>
          )
        }}
      </MpButtonProvider>
    )
  }
}
