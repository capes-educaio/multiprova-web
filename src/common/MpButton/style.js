export const style = theme => ({
  root: {
    padding: 0,
  },
  rootMp: {
    textTransform: 'none',
    borderRadius: 0,
    fontWeight: 300,
    '&:hover': {
      filter: 'brightness(85%)',
    },
  },
  label: {
    flexWrap: 'wrap',
  },
  content: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    padding: '6px 8px',
  },
  contentLoading: {
    padding: '6px 8px 2px 8px',
  },
  linearProgress: {
    width: '100%',
    flexGrow: 1,
  },
  marginLeft: {
    marginLeft: 2,
  },
  marginRight: {
    marginRight: 10,
  },
  steelBlueRoot: {
    backgroundColor: theme.palette.steelBlue,
    color: theme.palette.steelBlueContrast,
    '&:hover': {
      backgroundColor: theme.palette.primary.main,
    },
  },
  icon: {
    marginRight: 5,
  },
  iconMp: {
    color: theme.palette.cinzaEscuro,
    fill: theme.palette.cinzaEscuro,
  },
  steelBlueIcon: {
    color: theme.palette.steelBlueContrast,
    fill: theme.palette.steelBlueContrast,
  },
  responsiveGrow: {
    [theme.breakpoints.down(480)]: {
      flexGrow: 1,
    },
  },
})
