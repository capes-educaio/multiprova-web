import React from 'react'

import { MpButtonComponent } from '../MpButtonComponent'
import { mountTest } from 'localModules/testUtils/mountTest'

const requiredProps = {
  // style
  classes: {},
  // strings
  strings: {},
  // theme
  theme: { palette: {} },
}

const optionalProps = {
  IconComponent: () => <div />,
  color: 'steelBlue',
}

const arrayOfProps = [requiredProps, { ...requiredProps, ...optionalProps }]

mountTest(MpButtonComponent, 'MpButtonComponent')(arrayOfProps)
