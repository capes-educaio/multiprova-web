export const style = theme => ({
  card: {
    width: '200px',
  },
  titulo: {
    padding: '5px 10px',
    color: theme.palette.primary.dark,
  },
  numero: {
    padding: '30px',
    textAlign: 'center',
    fontWeight: '100',
    color: theme.palette.primary.main,
  },
})
