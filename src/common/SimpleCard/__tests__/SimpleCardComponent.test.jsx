import { ptBr } from 'utils/strings/ptBr'

import { SimpleCardComponent } from '../SimpleCardComponent'
import { mountTest } from 'localModules/testUtils/mountTest'

const requiredProps = {
  titulo: '',
  number: '',
  browser: {
    lessThan: {
      medium: {},
    },
  },
  // style
  classes: {},
  // strings
  strings: ptBr,
}

const arrayOfProps = [requiredProps]

mountTest(SimpleCardComponent, 'SimpleCardComponent')(arrayOfProps)
