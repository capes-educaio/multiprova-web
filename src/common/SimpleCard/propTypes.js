import PropTypes from 'prop-types'

export const propTypes = {
  titulo: PropTypes.string.isRequired,
  numero: PropTypes.oneOfType([PropTypes.string.isRequired]),
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
