import React from 'react'
import Card from '@material-ui/core/Card'
import { Typography } from '@material-ui/core'

export function SimpleCardComponent({ classes, customClasses, titulo, numero, browser }) {
  const usedClasses = customClasses ? customClasses : classes
  const isMobile = browser.lessThan.medium
  return (
    <Card className={usedClasses.card}>
      <Typography className={usedClasses.numero} variant={isMobile ? 'h5' : 'h4'}>
        {numero && numero.toString().replace('.', ',')}
      </Typography>
      <Typography className={usedClasses.titulo} variant={isMobile ? 'body2' : 'body1'}>
        {titulo}
      </Typography>
    </Card>
  )
}
