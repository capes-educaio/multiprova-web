import React from 'react'
import { shallow } from 'enzyme'
import { get, post } from 'api'

import { cookies } from 'utils/cookies'
import { baseUrl } from 'api/url'

import { GeradorPDFComponent } from '../GeradorPDFComponent'

jest.mock('api', () => ({
  get: jest.fn(),
  post: jest.fn().mockResolvedValue({}),
}))

const impressaoId = 1
const idInterval = 1

const defaultProps = {
  match: {
    params: {
      id: '0',
    },
  },
  onRef: jest.fn(),
  salvarNoBanco: jest.fn(),
  dados: {},
  url: `/provas/imprimir/`,
  classes: {},
  strings: {},
}

describe('Testando <GeradorPDFComponent />', () => {
  const wrapper = shallow(<GeradorPDFComponent {...defaultProps} />)

  it('componentWillUnmount e componentDidMount - Carrega o componente', () => {
    expect(wrapper.length).toEqual(1)
    jest.clearAllMocks()
  })

  it('componentWillUnmount e componentDidMount - Chama onRef passando a classe como referência', () => {
    const spy = wrapper.instance().props.onRef
    wrapper.instance().componentDidMount()
    expect(spy).toHaveBeenCalledWith(wrapper.instance())
    jest.clearAllMocks()
    spy.mockRestore()
  })

  it('componentWillUnmount e componentDidMount - Chama onRef passando null', () => {
    const spy = wrapper.instance().props.onRef
    wrapper.instance().componentWillUnmount()
    expect(spy).toHaveBeenCalledWith(null)
    jest.clearAllMocks()
    spy.mockRestore()
  })

  it('componentWillUnmount e componentDidMount - Não chama onRef', () => {
    wrapper.setProps({ onRef: null })
    wrapper.instance().componentWillUnmount()
    wrapper.instance().componentDidMount()
    expect(wrapper.instance().props.onRef).toBeNull()
    jest.clearAllMocks()
  })

  it('abrirAvisoSalvar - Seta o estado para abrir o componente de alerta salvar prova', () => {
    wrapper.setState({ alertaSalvarAberto: false })
    expect(wrapper.instance().state.alertaSalvarAberto).not.toBeTruthy()
    expect(wrapper.instance().state.alertaSalvarAberto).toBeFalsy()
    wrapper.instance().abrirAvisoSalvar()
    expect(wrapper.instance().state.alertaSalvarAberto).toBeTruthy()
    expect(wrapper.instance().state.alertaSalvarAberto).not.toBeFalsy()
  })

  it('fecharAvisoSalvar - Fecha o aviso de salvar prova. Se a resposta passada for true, chama salvarEGerarPDF', () => {
    wrapper.setState({ alertaSalvarAberto: true })
    expect(wrapper.instance().state.alertaSalvarAberto).toBeTruthy()
    expect(wrapper.instance().state.alertaSalvarAberto).not.toBeFalsy()
    const spy = jest.spyOn(wrapper.instance(), 'salvarEGerarPDF')
    wrapper.instance().fecharAvisoSalvar(false)
    expect(wrapper.instance().state.alertaSalvarAberto).not.toBeTruthy()
    expect(wrapper.instance().state.alertaSalvarAberto).toBeFalsy()
    expect(spy).not.toHaveBeenCalled()
    wrapper.instance().fecharAvisoSalvar(true)
    expect(spy).toHaveBeenCalled()
    jest.clearAllMocks()
    spy.mockRestore()
  })

  it('salvarEGerarPDF - Chama a função de salvar a prova no banco passando o flag (se foi emitido por geração de PDF) e manda gerar PDF', () => {
    const spysalvarNoBanco = wrapper.instance().props.salvarNoBanco
    const spygerarPDF = jest.spyOn(wrapper.instance(), 'gerarPDF')
    wrapper.instance().salvarEGerarPDF()
    expect(spysalvarNoBanco).toHaveBeenCalledWith({}, true)
    expect(spygerarPDF).toHaveBeenCalled()
    jest.clearAllMocks()
    spysalvarNoBanco.mockRestore()
    spygerarPDF.mockRestore()
  })

  it('gerarPDF - Emite requisição para gerar a prova e redireciona para prepararParaChecarStatusPDF', async () => {
    const spy = jest.spyOn(wrapper.instance(), 'prepararParaChecarStatusPDF')
    await wrapper.instance().gerarPDF()
    expect(post).toHaveBeenCalledWith(defaultProps.url, defaultProps.dados)
    expect(spy).toHaveBeenCalled()
    jest.clearAllMocks()
    spy.mockRestore()
  })

  it('gerarPDF - Simulando um erro', async () => {
    const erro = new TypeError()
    post.mockRejectedValueOnce(erro)
    const spy = jest.spyOn(wrapper.instance(), 'prepararParaChecarStatusPDF')
    await wrapper.instance().gerarPDF()
    expect(post).toHaveBeenCalledWith(defaultProps.url, defaultProps.dados)
    expect(spy).not.toHaveBeenCalled()
    expect(wrapper.instance().state.erro.type).toEqual(erro.type)
    jest.clearAllMocks()
    spy.mockRestore()
  })

  it('prepararParaChecarStatusPDF - seta os estados iniciais do pdf para GERANDO PDF e 0 (tentativas) e seta a função checarStatus para ser chamada num determinado intervalo', () => {
    jest.useFakeTimers()
    const spysetPDFState = jest.spyOn(wrapper.instance(), 'setPDFState')
    const response = {
      data: {
        impressaoId,
      },
    }
    const status = 'GERANDO PDF'
    const tentativas = 0
    const INTERVALO_DE_TENTATIVAS = 2000

    wrapper.instance().prepararParaChecarStatusPDF(response)
    expect(wrapper.instance().state.alertaGerandoPDFAberto).toBeTruthy()
    expect(spysetPDFState).toHaveBeenCalledWith({ status, tentativas })
    expect(setInterval).toHaveBeenCalledTimes(1)
    expect(setInterval).toHaveBeenLastCalledWith(expect.any(Function), INTERVALO_DE_TENTATIVAS)

    jest.clearAllMocks()
    spysetPDFState.mockRestore()
  })

  it('checarStatusPDF - Requisita ao back o status do pdf', async () => {
    const spy = jest.spyOn(wrapper.instance(), 'prepararParaDownloadPDF')
    const url = `/impressao/${impressaoId}`
    get.mockResolvedValueOnce({})
    await wrapper.instance().checarStatusPDF(impressaoId, idInterval)
    expect(get).toHaveBeenCalledWith(url)
    expect(spy).toHaveBeenCalledWith({}, idInterval)
    jest.clearAllMocks()
    spy.mockRestore()
  })

  it('checarStatusPDF - Simulando um erro', async () => {
    const erro = new TypeError()
    get.mockRejectedValueOnce(erro)
    const spy = jest.spyOn(wrapper.instance(), 'prepararParaDownloadPDF')
    const url = `/impressao/${impressaoId}`

    await wrapper.instance().checarStatusPDF(impressaoId, idInterval)
    expect(get).toHaveBeenCalledWith(url)
    expect(spy).not.toHaveBeenCalled()
    expect(wrapper.instance().state.erro.type).toEqual(erro.type)
    jest.clearAllMocks()
    spy.mockRestore()
  })

  it('prepararParaDownloadPDF - donwload, processando e erro', () => {
    get.mockResolvedValueOnce({})
    wrapper.instance().downloadPDF = jest.fn()
    const spydownloadPDF = jest.spyOn(wrapper.instance(), 'downloadPDF')
    const spyincrementaTentativasVerificarPDF = jest.spyOn(wrapper.instance(), 'incrementaTentativasVerificarPDF')
    const spysetPDFErro = jest.spyOn(wrapper.instance(), 'setPDFErro')

    let responsePRONTO = {
      data: {
        status: 'PRONTO',
      },
    }
    wrapper.instance().prepararParaDownloadPDF(responsePRONTO, idInterval)
    expect(spydownloadPDF).toHaveBeenCalledWith(responsePRONTO.data, idInterval)
    expect(spyincrementaTentativasVerificarPDF).not.toHaveBeenCalledWith(idInterval)
    expect(spysetPDFErro).not.toHaveBeenCalledWith(idInterval)
    jest.clearAllMocks()

    let responseERROR = {
      data: {
        status: 'ERROR',
      },
    }
    wrapper.instance().prepararParaDownloadPDF(responseERROR, idInterval)
    expect(spydownloadPDF).not.toHaveBeenCalledWith(responseERROR.data, idInterval)
    expect(spyincrementaTentativasVerificarPDF).not.toHaveBeenCalledWith(idInterval)
    expect(spysetPDFErro).toHaveBeenCalledWith(idInterval)
    jest.clearAllMocks()

    let response = {
      data: {
        status: '',
      },
    }
    wrapper.instance().prepararParaDownloadPDF(response, idInterval, impressaoId)
    expect(spydownloadPDF).not.toHaveBeenCalledWith(impressaoId, idInterval)
    expect(spyincrementaTentativasVerificarPDF).toHaveBeenCalledWith(idInterval)
    expect(spysetPDFErro).not.toHaveBeenCalledWith(idInterval)
    jest.clearAllMocks()
  })

  it('incrementaTentativasVerificarPDF - tentativas menor que o máximo e depois, maior que o máximo', () => {
    const wrapper = shallow(<GeradorPDFComponent {...defaultProps} />)
    const spySetPDFErro = jest.spyOn(wrapper.instance(), 'setPDFErro')
    const spySetPDFState = jest.spyOn(wrapper.instance(), 'setPDFState')

    let pdf = {}
    pdf.tentativas = 1

    wrapper.setState({ pdf })
    wrapper.instance().incrementaTentativasVerificarPDF(idInterval)
    pdf.tentativas++
    expect(spySetPDFErro).not.toHaveBeenCalled()
    expect(spySetPDFState).toHaveBeenCalledWith(pdf)

    jest.clearAllMocks()

    pdf.tentativas = 51
    wrapper.setState({ pdf })
    wrapper.instance().incrementaTentativasVerificarPDF(idInterval)
    expect(spySetPDFState).not.toHaveBeenCalledWith(pdf)
    expect(spySetPDFErro).toHaveBeenCalledWith(idInterval)

    jest.clearAllMocks()
    spySetPDFState.mockRestore()
    spySetPDFErro.mockRestore()
  })

  it('setPDFErro - seta estado de erro e zera as tentativas de buscar o PDF e para o checador', () => {
    jest.useFakeTimers()
    const spy = jest.spyOn(wrapper.instance(), 'setPDFState')
    wrapper.instance().setPDFErro(idInterval)
    const status = 'ERROR'
    const tentativas = 0
    expect(spy).toHaveBeenCalledWith({ status, tentativas })
    expect(clearInterval).toHaveBeenCalledTimes(1)
    expect(clearInterval).toHaveBeenLastCalledWith(idInterval)
    jest.clearAllMocks()
  })

  it('setPDFState - seta estado apenas mudando o que foi passado', () => {
    let pdf = {}
    pdf.tentativas = 1
    pdf.status = 'PRONTO'
    pdf.link = 'www.g.com'
    wrapper.setState({ pdf })
    const status = 'ERRO'
    const tentativas = 0
    wrapper.instance().setPDFState({ status, tentativas })
    const RESULTADO_ESPERADO_LINK_IGUAL = {
      status,
      tentativas,
      link: pdf.link,
    }
    const link_alterado = ''
    const NAO_DEVE_MUDAR_LINK = {
      status,
      tentativas,
      link_alterado,
    }
    expect(wrapper.instance().state.pdf).toEqual(RESULTADO_ESPERADO_LINK_IGUAL)
    expect(wrapper.instance().state.pdf).not.toEqual(NAO_DEVE_MUDAR_LINK)
    const PASSANDO_NULL = null
    expect(wrapper.instance().setPDFState(PASSANDO_NULL)).toBeFalsy()

    jest.clearAllMocks()
  })

  it('handleCloseFecharPDF - fecha o alerta de geração do PDF', () => {
    wrapper.setState({ alertaGerandoPDFAberto: true })
    expect(wrapper.instance().state.alertaGerandoPDFAberto).toBeTruthy()
    wrapper.instance().handleCloseFecharPDF(true)
    expect(wrapper.instance().state.alertaGerandoPDFAberto).toBeFalsy()

    wrapper.setState({ alertaGerandoPDFAberto: true })
    expect(wrapper.instance().state.alertaGerandoPDFAberto).toBeTruthy()
    wrapper.instance().handleCloseFecharPDF(false)
    expect(wrapper.instance().state.alertaGerandoPDFAberto).toBeTruthy()

    jest.clearAllMocks()
  })
})
describe('downloadPDF', () => {
  it(' - baixa o PDF', () => {
    const wrapper = shallow(<GeradorPDFComponent {...defaultProps} />)
    jest.useFakeTimers()
    const impressao = {
      status: 'PRONTO',
      id: impressaoId,
    }

    const authToken = cookies ? cookies.get('authToken') : undefined
    const tokenId = authToken ? authToken.id : undefined
    const url = baseUrl + `/impressao/download/${impressaoId}/?access_token=${tokenId}`
    const spy = jest.spyOn(wrapper.instance(), 'setPDFState')
    wrapper.instance().downloadPDF(impressao, idInterval)
    expect(spy).toHaveBeenCalledWith({ status: impressao.status, link: url })
    expect(clearInterval).toHaveBeenCalledTimes(1)
    expect(clearInterval).toHaveBeenLastCalledWith(idInterval)
    jest.clearAllMocks()
    spy.mockRestore()
  })
})
