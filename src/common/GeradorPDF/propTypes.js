import PropTypes from 'prop-types'

export const propTypes = {
  onRef: PropTypes.func,
  salvarNoBanco: PropTypes.func,
  stringsSalvarAntesDeImprimir: PropTypes.string,
  url: PropTypes.string.isRequired,
  dados: PropTypes.object,
  stringGerandoPDF: PropTypes.string,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
