import React, { Component } from 'react'
import { cookies } from 'utils/cookies'
import { baseUrl } from 'api/url'
import { get, post } from 'api'
import { AlertaGerandoPDF } from 'common/AlertaGerandoPDF'

import { propTypes } from './propTypes'

export class GeradorPDFComponent extends Component {
  static propTypes = propTypes

  constructor(props) {
    super(props)
    this.state = {
      alertaGerandoPDFAberto: false,
      alertaSalvarAberto: false,
      pdf: {
        status: '',
        tentativas: 0,
        link: '',
      },
    }
  }

  componentDidMount = () => {
    const { onRef } = this.props
    if (onRef) onRef(this)
  }

  componentWillUnmount = () => {
    const { onRef } = this.props
    if (onRef) onRef(null)
  }

  abrirAvisoSalvar = () => {
    this.setState({ alertaSalvarAberto: true })
  }

  fecharAvisoSalvar = resposta => {
    if (resposta === true) {
      this.salvarEGerarPDF()
    }
    this.setState({
      alertaSalvarAberto: false,
    })
  }

  salvarEGerarPDF = () => {
    const avisoDeGeracaoDePDF = true
    this.props.salvarNoBanco({}, avisoDeGeracaoDePDF)
    this.gerarPDF()
  }

  gerarPDF = () => {
    const { url, dados } = this.props
    post(url, dados)
      .then(this.prepararParaChecarStatusPDF)
      .catch(e => this.setState({ erro: e, alertaGerandoPDFAberto: false }))
  }

  prepararParaChecarStatusPDF = response => {
    const { data } = response
    const { impressaoId } = data
    const { checarStatusPDF } = this
    const INTERVALO_VERIFICACAO_PDF = 2000
    const status = 'GERANDO PDF'
    const tentativas = 0
    this.setState({ alertaGerandoPDFAberto: true })
    this.setPDFState({ status, tentativas })

    const idInterval = setInterval(function() {
      checarStatusPDF(impressaoId, idInterval)
    }, INTERVALO_VERIFICACAO_PDF)
  }

  checarStatusPDF = (impressaoId, idInterval) => {
    const url = `/impressao/${impressaoId}`
    get(url)
      .then(response => this.prepararParaDownloadPDF(response, idInterval))
      .catch(e => this.setState({ erro: e, alertaGerandoPDFAberto: false }))
  }

  prepararParaDownloadPDF = (response, idInterval) => {
    const { data } = response
    const { status } = data
    const PRONTO = 'PRONTO'
    const ERROR = 'ERROR'
    switch (status) {
      case PRONTO:
        this.downloadPDF(data, idInterval)
        break
      case ERROR:
        this.setPDFErro(idInterval)
        break
      default:
        this.incrementaTentativasVerificarPDF(idInterval)
        break
    }
  }

  incrementaTentativasVerificarPDF = idInterval => {
    let { pdf } = this.state
    let { tentativas } = pdf
    const QTD_MAXIMA_TENTATIVAS = 50
    if (tentativas > QTD_MAXIMA_TENTATIVAS) {
      this.setPDFErro(idInterval)
    } else {
      tentativas++
      pdf.tentativas = tentativas
      this.setPDFState(pdf)
    }
  }

  setPDFErro = idInterval => {
    const status = 'ERROR'
    const tentativas = 0
    this.setPDFState({ status, tentativas })
    clearInterval(idInterval)
  }

  setPDFState = pdfState => {
    if (!pdfState) return

    const pdf = {
      ...this.state.pdf,
      ...pdfState,
    }

    this.setState({ pdf })
  }

  handleCloseFecharPDF = confirmaFechar => {
    if (confirmaFechar === true)
      this.setState({
        alertaGerandoPDFAberto: false,
      })
  }

  downloadPDF = (impressao, idInterval) => {
    const { id, titulo, status } = impressao
    let { dataUltimaAlteracao } = impressao
    const url = this.getUrlImpressao(id)
    dataUltimaAlteracao = dataUltimaAlteracao ? dataUltimaAlteracao.substring(0, 10) : null
    const nomeArquivo = titulo + ' - ' + dataUltimaAlteracao + '.pdf'

    let link = document.createElement('a')
    link.href = url
    link.target = 'blank'
    link.setAttribute('download', nomeArquivo)
    document.body.appendChild(link)
    link.click()
    this.setPDFState({ status, link: url })
    clearInterval(idInterval)
  }

  getUrlImpressao = idImpressao => {
    const authToken = cookies ? cookies.get('authToken') : undefined
    const tokenId = authToken ? authToken.id : undefined
    return baseUrl + `/impressao/download/${idImpressao}/?access_token=${tokenId}`
  }

  render() {
    const { alertaGerandoPDFAberto, pdf, alertaSalvarAberto } = this.state
    const { stringsSalvarAntesDeImprimir, stringGerandoPDF } = this.props
    return (
      <div>
        <AlertaGerandoPDF
          stringGerandoPDF={stringGerandoPDF}
          stringsSalvarAntesDeImprimir={stringsSalvarAntesDeImprimir}
          alertaAberto={alertaGerandoPDFAberto}
          pdf={pdf}
          handleCloseCancelar={this.handleCloseFecharPDF}
          alertaSalvarAberto={alertaSalvarAberto}
          fecharAvisoSalvar={this.fecharAvisoSalvar}
        />
      </div>
    )
  }
}
