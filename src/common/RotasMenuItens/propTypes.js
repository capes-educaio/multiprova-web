import PropTypes from 'prop-types'

export const propTypes = {
  children: PropTypes.any,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classesPai: PropTypes.object.isRequired,
}
