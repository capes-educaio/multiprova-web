import React, { Component, Fragment } from 'react'

import { appConfig } from 'appConfig'

import MenuItem from '@material-ui/core/MenuItem'
import Feedback from '@material-ui/icons/Feedback'
import AssignmentInd from '@material-ui/icons/AssignmentInd'
import QuestionAnswer from '@material-ui/icons/QuestionAnswer'
import Description from '@material-ui/icons/Description'
import Group from '@material-ui/icons/Group'

import { setUrlState } from 'localModules/urlState'

import { propTypes } from './propTypes'

const MenuDashboard = ({ className, children }) => {
  if (appConfig.modulosHabilitados.dashboard)
    return (
      <MenuItem onClick={() => setUrlState({ pathname: '/dashboard' })} className={className}>
        {children}
      </MenuItem>
    )
  else return null
}

const MenuQuestoes = ({ className, children }) => {
  if (appConfig.modulosHabilitados.questoes)
    return (
      <MenuItem id="btnQuestoes" className={className} onClick={() => setUrlState({ pathname: '/questoes' })}>
        {children}
      </MenuItem>
    )
  else return null
}

const MenuProvas = ({ className, children }) => {
  if (appConfig.modulosHabilitados.provas)
    return (
      <MenuItem id="btnProvasDocente" onClick={() => setUrlState({ pathname: '/provas' })} className={className}>
        {children}
      </MenuItem>
    )
  else return null
}

const MenuTurmas = ({ className, children }) => {
  if (appConfig.modulosHabilitados.turmas)
    return (
      <MenuItem onClick={() => setUrlState({ pathname: '/turmas' })} className={className}>
        {children}
      </MenuItem>
    )
  else return null
}

const MenuGerenciamentoUsuarios = ({ className, children }) => {
  if (appConfig.modulosHabilitados.gerenciamentoUsuarios)
    return (
      <MenuItem onClick={() => setUrlState({ pathname: '/usuarios' })} className={className}>
        {children}
      </MenuItem>
    )
  else return null
}

const MenuEventos = ({ className, children }) => {
  if (appConfig.modulosHabilitados.eventos)
    return (
      <MenuItem onClick={() => setUrlState({ pathname: '/eventos' })} className={className}>
        {children}
      </MenuItem>
    )
  else return null
}

const MenuProvasDiscente = ({ className, children }) => {
  if (appConfig.modulosHabilitados.provasDiscente)
    return (
      <MenuItem id="btnProvasDiscente" onClick={() => setUrlState({ pathname: '/provas' })} className={className}>
        {children}
      </MenuItem>
    )
  else return null
}

export class RotasMenuItensComponent extends Component {
  static propTypes = propTypes

  render() {
    const { usuarioAtual, classesPai, strings } = this.props
    return (
      <div className={classesPai.flex}>
        {usuarioAtual.isDocente() && (
          <Fragment>
            <MenuDashboard className={classesPai.menuItem}>
              <Description className={classesPai.rightIcon} />
              {strings.dashboard}
            </MenuDashboard>
            <MenuQuestoes className={classesPai.menuItem}>
              <QuestionAnswer className={classesPai.rightIcon} />
              {strings.questoes}
            </MenuQuestoes>
            <MenuProvas className={classesPai.menuItem}>
              <Description className={classesPai.rightIcon} />
              {strings.provas}
            </MenuProvas>
            <MenuTurmas className={classesPai.menuItem}>
              <Group className={classesPai.rightIcon} />
              {strings.turmas}
            </MenuTurmas>
          </Fragment>
        )}
        {usuarioAtual.isAdmin() && (
          <Fragment>
            <MenuGerenciamentoUsuarios className={classesPai.menuItem}>
              <AssignmentInd className={classesPai.rightIcons} />
              {strings.usuarios}
            </MenuGerenciamentoUsuarios>
            <MenuEventos className={classesPai.menuItem}>
              <Feedback className={classesPai.rightIcon} />
              {strings.eventos}
            </MenuEventos>
          </Fragment>
        )}
        {usuarioAtual.isDiscente() && (
          <Fragment>
            <MenuDashboard className={classesPai.menuItem}>
              <Description className={classesPai.rightIcon} />
              {strings.dashboard}
            </MenuDashboard>
            <MenuProvasDiscente className={classesPai.menuItem}>
              <Description className={classesPai.rightIcon} />
              {strings.provas}
            </MenuProvasDiscente>
          </Fragment>
        )}
        {usuarioAtual.isGestor() && (
          <MenuDashboard className={classesPai.menuItem}>
            <Description className={classesPai.rightIcon} />
            {strings.dashboard}
          </MenuDashboard>
        )}
      </div>
    )
  }
}
