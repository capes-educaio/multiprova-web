import React from 'react'

import { InputTag } from 'common/InputTag'

import { propTypes } from './propTypes'

export class FormQuestaoFiltroPadraoComponent extends React.Component {
  static propTypes = propTypes

  componentDidMount = () => {
    const { onRef } = this.props
    if (onRef) onRef(this)
    this.limparFiltro()
  }

  componentWillUnmount = () => {
    const { onRef } = this.props
    if (onRef) onRef(null)
  }

  handleChange = campo => event => {
    event.preventDefault()
    this.alterarValorDoForm(campo, event.target.value)
  }

  alterarValorDoForm = (campo, valor) => {
    const { onFormFiltroChange, valorDoForm } = this.props
    valorDoForm[campo] = valor
    onFormFiltroChange(valorDoForm)
    this.setState({ [campo]: valor })
  }

  limparFiltro = () => {
    const { onFormFiltroChange, valorDoFormInicial } = this.props
    const valorInicialDificuldade = 0
    const valorInicialAnoEscolar = 0
    const valorInicialTexto = ''
    const valorInicialStatus = 0
    const valorInicialTagsSelected = []
    const valorInicialTipo = 0
    const filtroLimpo = {
      dificuldade: valorInicialDificuldade,
      anoEscolar: valorInicialAnoEscolar,
      textoFiltro: valorInicialTexto,
      tagsSelected: valorInicialTagsSelected,
      status: valorInicialStatus,
      tipo: valorInicialTipo,
    }
    onFormFiltroChange(!!valorDoFormInicial ? valorDoFormInicial : filtroLimpo)
  }

  handleChangeTags = tags => {
    this.alterarValorDoForm('tagsSelected', tags)
  }

  render() {
    const { valorDoForm, handleKeyPress } = this.props
    let { tagsSelected } = valorDoForm

    if (tagsSelected === undefined) tagsSelected = []

    return <InputTag onChange={this.handleChangeTags} valor={tagsSelected} handleKeyPress={handleKeyPress} />
  }
}
