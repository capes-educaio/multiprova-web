import PropTypes from 'prop-types'

export const propTypes = {
  className: PropTypes.string,
  config: PropTypes.array.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
