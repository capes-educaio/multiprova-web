export const style = theme => ({
  actionsWrapper: {
    display: 'flex',
    padding: '20px 20px 20px 20px',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  actionIcone: {
    margin: '0 5px 0 0',
  },
})
