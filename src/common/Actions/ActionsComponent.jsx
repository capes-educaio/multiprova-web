import React, { Component } from 'react'
import Button from '@material-ui/core/Button'
import IconButton from '@material-ui/core/IconButton'
import Tooltip from '@material-ui/core/Tooltip'

import { propTypes } from './propTypes'

export class ActionsComponent extends Component {
  static propTypes = propTypes

  render() {
    const { classes, config, className } = this.props
    const actions = config.map((actionConfig, index) => (
      <div className={classes.action} key={index}>
        {actionConfig.texto && (
          <Button {...actionConfig.buttonProps}>
            {actionConfig.icone ? <actionConfig.icone className={classes.actionIcone} /> : null}
            {actionConfig.texto}
          </Button>
        )}
        {!actionConfig.texto && (
          <TooltipWrap {...actionConfig.tooltip}>
            <IconButton {...actionConfig.buttonProps}>
              <actionConfig.icone />
            </IconButton>
          </TooltipWrap>
        )}
      </div>
    ))
    return <div className={className || classes.actionsWrapper}>{actions}</div>
  }
}

function TooltipWrap(props) {
  const { title, children } = props
  if (title !== undefined) return <Tooltip title={title}>{children}</Tooltip>
  return children
}
