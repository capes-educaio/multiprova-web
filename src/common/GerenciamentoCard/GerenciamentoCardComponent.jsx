import React, { Component } from 'react'

import MenuItem from '@material-ui/core/MenuItem'

import Delete from '@material-ui/icons/Delete'
import DeleteForever from '@material-ui/icons/DeleteForever'
import EditIcon from '@material-ui/icons/Edit'

import { updateUrlState } from 'localModules/urlState'

import { ProvaCard } from 'common/ProvaCard'
import { Opcoes } from 'common/Opcoes'

import { del } from 'api'

import { propTypes } from './propTypes'

export class GerenciamentoCardComponent extends Component {
  static propTypes = propTypes
  _opcoesRef

  _goToEdit = id => () => {
    updateUrlState({
      pathname: `${this.props.rotaDaInstanciaFront}/${id}`,
      hidden: { activeStep: this.props.passoInicialEdicao, isOpenModal: false, idsCardsSelecionados: [] },
    })
  }

  _excluirInstancia = id => () => {
    const { rotaDaInstanciaBack, atualizarCardDisplay } = this.props
    let url = `${rotaDaInstanciaBack}/${id}`
    del(url).then(response => {
      this.handleCloseMenu()
      atualizarCardDisplay()
    })
  }

  _openDialogExcluir = id => () => {
    this.handleCloseMenu()
    const { listaDialogPush, strings, stringDesejaExcluirInstancia } = this.props
    const dialogConfig = {
      titulo: strings.excluir,
      texto: stringDesejaExcluirInstancia,
      actions: [
        {
          texto: strings.ok,
          onClick: this._excluirInstancia(id),
        },
        {
          texto: strings.cancelar,
        },
      ],
    }
    listaDialogPush(dialogConfig)
  }

  handleCloseMenu = () => {
    if (this._opcoesRef) this._opcoesRef.close()
  }

  render() {
    const { classes, instancia, strings, CardComponent, goToEdit, children } = this.props
    if (children) {
      return children({
        goToEdit: this._goToEdit,
        goToView: this._goToView,
        openDialogExcluir: this._openDialogExcluir,
      })
    }
    return (
      <CardComponent
        opcoes={
          <Opcoes onRef={ref => (this._opcoesRef = ref)}>
            <MenuItem onClick={goToEdit ? goToEdit(instancia.id) : this._goToEdit(instancia.id)}>
              <EditIcon className={classes.menuIcon} />
              <span>{strings.editar}</span>
            </MenuItem>
            <MenuItem id="item-menu-excluir" onClick={this._openDialogExcluir(instancia.id)}>
              {CardComponent === ProvaCard ? (
                <Delete className={classes.menuIcon} />
              ) : (
                <DeleteForever className={classes.menuIcon} />
              )}
              <span>{strings.excluir}</span>
            </MenuItem>
          </Opcoes>
        }
        instancia={instancia}
        titulo={instancia.titulo}
        exibirDataUltimaAlteracao
      />
    )
  }
}
