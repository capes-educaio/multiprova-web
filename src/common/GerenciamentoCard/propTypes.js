import PropTypes from 'prop-types'

export const propTypes = {
  rotaDaInstanciaBack: PropTypes.string.isRequired,
  rotaDaInstanciaFront: PropTypes.string.isRequired,
  stringDesejaExcluirInstancia: PropTypes.string.isRequired,
  atualizarCardDisplay: PropTypes.any.isRequired,
  children: PropTypes.func,
  instancia: PropTypes.object,
  CardComponent: PropTypes.any,
  goToEdit: PropTypes.func,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
