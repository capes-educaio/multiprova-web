import React from 'react'
import { shallow } from 'enzyme'

import { GerenciamentoCardComponent } from '../GerenciamentoCardComponent'

const defaultProps = {
  instancia: {},
  rotaDaInstanciaBack: 'mock',
  rotaDaInstanciaFront: 'mock',
  stringDesejaExcluirInstancia: 'mock',
  atualizarCardDisplay: jest.fn(),
  CardComponent: () => <div />,
  // strings
  strings: {},
  // style
  classes: {},
}

describe('GerenciamentoCardComponent', () => {
  it('monta', () => {
    shallow(<GerenciamentoCardComponent {...defaultProps} />)
  })
})
