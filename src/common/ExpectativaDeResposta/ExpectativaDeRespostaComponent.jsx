import React, { Component } from 'react'

import Typography from '@material-ui/core/Typography'

import { MpParser } from 'common/MpParser'

import { propTypes } from './propTypes'

export class ExpectativaDeRespostaComponent extends Component {
  static propTypes = propTypes

  render() {
    const { expectativaDeResposta, strings, classes } = this.props
    return (
      <div className={classes.expectativaWrapper}>
        <Typography className={classes.expectativaDeResposta}>{strings.expectativaDeResposta}</Typography>
        <MpParser>{expectativaDeResposta}</MpParser>
      </div>
    )
  }
}
