import PropTypes from 'prop-types'

export const propTypes = {
  expectativaDeResposta: PropTypes.string.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
