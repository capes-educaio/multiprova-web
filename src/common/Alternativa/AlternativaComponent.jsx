import React, { Component } from 'react'

import Typography from '@material-ui/core/Typography'

import { MpParser } from 'common/MpParser'

import { propTypes } from './propTypes'

export class AlternativaComponent extends Component {
  static propTypes = propTypes

  render() {
    const { alternativa, classes } = this.props
    const letra = alternativa.letra || alternativa.letraNaInstancia
    return (
      <div className={classes.alternativa}>
        <Typography className={classes.letraNaInstancia}>{letra.toUpperCase()})</Typography>
        <MpParser typographyProps={{ classes: { root: classes.texto } }}>{alternativa.texto}</MpParser>
      </div>
    )
  }
}
