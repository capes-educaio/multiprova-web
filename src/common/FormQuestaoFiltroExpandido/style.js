export const style = theme => ({
  root: {},
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120,
  },
  containerSeletores: {
    margin: '5px -5px',
    display: 'flex',
    flexWrap: 'wrap',
  },
  blocoSeletor: {
    flexBasis: 200,
    flexGrow: 1,
    width: '100%',
    margin: 5,
  },
  pesquisar: {
    marginTop: '-20px',
    marginBottom: '8px',
  },
})
