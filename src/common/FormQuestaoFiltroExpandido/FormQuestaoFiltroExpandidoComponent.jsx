import React from 'react'

import TextField from '@material-ui/core/TextField'
import Input from '@material-ui/core/Input'
import InputLabel from '@material-ui/core/InputLabel'
import FormControl from '@material-ui/core/FormControl'
import MenuItem from '@material-ui/core/MenuItem'
import Select from '@material-ui/core/Select'
import { enumStatusQuestao } from 'utils/enumStatusQuestao'
import { enumTipoQuestao } from 'utils/enumTipoQuestao'
import { enumAnoEscolar } from 'utils/enumAnoEscolar'
import { InputAutocomplete } from './../InputAutocomplete'

import { setListaElaboradores } from 'utils/compositeActions'

import { propTypes } from './propTypes'
import { appConfig, educaio } from 'appConfig'

export class FormQuestaoFiltroExpandidoComponent extends React.Component {
  static propTypes = propTypes

  componentDidMount = () => {
    setListaElaboradores()
  }

  handleChange = campo => event => {
    event.preventDefault()

    this.alterarValorDoForm(campo, event.target.value)
    this.setState({ [campo]: event.target.value })
  }

  handleChangeTags = tags => {
    this.alterarValorDoForm('tagsSelected', tags)
  }

  alterarValorDoForm = (campo, valor) => {
    const { onFormFiltroChange, valorDoForm } = this.props
    const value = valor.value
    valorDoForm[campo] = value ? value : valor
    onFormFiltroChange(valorDoForm)
  }

  render() {
    const {
      strings,
      valorDoForm,
      handleKeyPress,
      classes,
      listaElaboradores,
      tipoQuestoesHabilitados = appConfig.tipoQuestoesHabilitados,
    } = this.props
    const { textoFiltro } = valorDoForm
    let { dificuldade, status, anoEscolar, tipo, elaborador } = valorDoForm

    const isEducaio = appConfig.strings.nomeProjeto === educaio.strings.nomeProjeto

    return (
      <div>
        <div className={classes.pesquisar}>
          <TextField
            className={classes.textField}
            id="busca"
            value={textoFiltro}
            label={strings.buscarPorTexto}
            onChange={this.handleChange('textoFiltro')}
            onKeyPress={handleKeyPress}
            fullWidth
          />
        </div>
        <div className={classes.containerSeletores}>
          <div className={classes.blocoSeletor}>
            <FormControl fullWidth>
              <InputLabel shrink> {strings.dificuldade} </InputLabel>
              <Select
                fullWidth
                value={dificuldade ? dificuldade : 0}
                onKeyPress={handleKeyPress}
                onChange={this.handleChange('dificuldade')}
                input={<Input name="Dificuldade" id="dificuldade" />}
              >
                <MenuItem value={0}>{strings.todas}</MenuItem>
                <MenuItem value={1}>{strings.facil}</MenuItem>
                <MenuItem value={2}>{strings.medio}</MenuItem>
                <MenuItem value={3}>{strings.dificil}</MenuItem>
              </Select>
            </FormControl>
          </div>
          {!isEducaio && <div className={classes.blocoSeletor}>
            <FormControl fullWidth>
              <InputLabel shrink> {strings.statusQuestao} </InputLabel>
              <Select
                value={status ? status : 0}
                fullWidth
                onKeyPress={handleKeyPress}
                onChange={this.handleChange('status')}
                input={<Input name="Status" id="status" />}
              >
                <MenuItem value={0}>{strings.todos}</MenuItem>
                {Object.keys(enumStatusQuestao).map(status => (
                  <MenuItem key={enumStatusQuestao[status].id} value={enumStatusQuestao[status].id}>
                    {enumStatusQuestao[status].texto}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
          </div>}
          <div className={classes.blocoSeletor}>
            <FormControl fullWidth>
              <InputLabel shrink>{strings.anoEscolar}</InputLabel>
              <Select
                value={anoEscolar ? anoEscolar : 0}
                fullWidth
                onKeyPress={handleKeyPress}
                onChange={this.handleChange('anoEscolar')}
                input={<Input name="anoEscolar" id="anoEscolar" />}
              >
                <MenuItem value={0}>{strings.todos}</MenuItem>
                {Object.keys(enumAnoEscolar).map(ano => (
                  <MenuItem key={ano} value={enumAnoEscolar[ano]}>
                    {strings[ano]}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
          </div>
          <div className={classes.blocoSeletor}>
            <FormControl fullWidth>
              <InputLabel shrink> {strings.tipoQuestao} </InputLabel>
              <Select
                value={tipo ? tipo : 0}
                fullWidth
                onKeyPress={handleKeyPress}
                displayEmpty
                onChange={this.handleChange('tipo')}
                input={<Input name={strings.tipoQuestao} id="tipo" />}
              >
                <MenuItem value={0}>{strings.todas}</MenuItem>
                {Object.keys(enumTipoQuestao).map(key => {
                  const tipoQuestao = enumTipoQuestao[key]
                  if (tipoQuestoesHabilitados[tipoQuestao]) {
                    return (
                      <MenuItem key={key} value={tipoQuestao}>
                        {strings[tipoQuestao]}
                      </MenuItem>
                    )
                  } else {
                    return null
                  }
                })}
              </Select>
            </FormControl>
          </div>
          {listaElaboradores && !isEducaio && (
            <div className={classes.blocoSeletor}>
              <InputAutocomplete
                label={'Elaborador'}
                placeholder={'Buscar por elaborador'}
                lista={listaElaboradores}
                handleChange={this.handleChange}
                acessor={'elaborador'}
                value={elaborador}
              />
            </div>
          )}
        </div>
        <div className={classes.botoes}>{this.props.children}</div>
      </div>
    )
  }
}
