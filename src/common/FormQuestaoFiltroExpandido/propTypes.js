import PropTypes from 'prop-types'

export const propTypes = {
  valorDoForm: PropTypes.object,
  onFormFiltroChange: PropTypes.func.isRequired,
  onkeyPressAction: PropTypes.func,
  tipoQuestoesHabilitados: PropTypes.object,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
  listaElaboradores: PropTypes.array,
}
