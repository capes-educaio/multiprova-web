import React from 'react'

import TextField from '@material-ui/core/TextField'
import Input from '@material-ui/core/Input'
import InputLabel from '@material-ui/core/InputLabel'
import FormControl from '@material-ui/core/FormControl'
import MenuItem from '@material-ui/core/MenuItem'
import Select from '@material-ui/core/Select'
import { enumTipoQuestao } from 'utils/enumTipoQuestao'
import { enumAnoEscolar } from 'utils/enumAnoEscolar'
import { provaUtils } from 'localModules/provas/ProvaUtils'
import { propTypes } from './propTypes'
import { appConfig } from 'appConfig'

export class FormQuestaoSelecaoAutomaticaExpandidoComponent extends React.Component {
  static propTypes = propTypes

  handleChange = campo => event => {
    event.preventDefault()
    this.alterarValorDoForm(campo, event.target.value)
  }

  alterarValorDoForm = (campo, valor) => {
    const { onFormFiltroChange, valorDoForm } = this.props
    valorDoForm[campo] = valor
    onFormFiltroChange(valorDoForm)
  }

  render() {
    const {
      strings,
      valorDoForm,
      handleKeyPress,
      classes,
      tipoQuestoesHabilitados = {
        ...appConfig.tipoQuestoesHabilitados,
        bloco: !provaUtils.isProvaDinamica,
      },
    } = this.props
    let { dificuldade, quantidadeDeQuestoes, anoEscolar, tipo } = valorDoForm
    return (
      <div>
        <div className={classes.containerSeletores}>
          <div className={classes.blocoSeletor}>
            <FormControl fullWidth>
              <InputLabel shrink> {strings.dificuldade} </InputLabel>
              <Select
                fullWidth
                value={dificuldade ? dificuldade : 0}
                onKeyPress={handleKeyPress}
                onChange={this.handleChange('dificuldade')}
                input={<Input name="Dificuldade" id="dificuldade" />}
              >
                <MenuItem value={0}>{strings.todas}</MenuItem>
                <MenuItem value={1}>{strings.facil}</MenuItem>
                <MenuItem value={2}>{strings.medio}</MenuItem>
                <MenuItem value={3}>{strings.dificil}</MenuItem>
              </Select>
            </FormControl>
          </div>
          <div className={classes.blocoSeletor}>
            <FormControl fullWidth>
              <InputLabel shrink>{strings.anoEscolar}</InputLabel>
              <Select
                value={anoEscolar ? anoEscolar : 0}
                fullWidth
                onKeyPress={handleKeyPress}
                onChange={this.handleChange('anoEscolar')}
                input={<Input name="anoEscolar" id="anoEscolar" />}
              >
                <MenuItem value={0}>{strings.todos}</MenuItem>
                {Object.keys(enumAnoEscolar).map(ano => (
                  <MenuItem key={ano} value={enumAnoEscolar[ano]}>
                    {strings[ano]}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
          </div>
          <div className={classes.blocoSeletor}>
            <FormControl fullWidth>
              <InputLabel shrink> {strings.tipoQuestao} </InputLabel>
              <Select
                value={tipo ? tipo : 0}
                fullWidth
                onKeyPress={handleKeyPress}
                displayEmpty
                onChange={this.handleChange('tipo')}
                input={<Input name={strings.tipoQuestao} id="tipo" />}
              >
                {!provaUtils.isProvaDinamica && <MenuItem value={0}>{strings.todas}</MenuItem>}
                {Object.keys(enumTipoQuestao).map(key => {
                  const tipo = enumTipoQuestao[key]
                  if (tipoQuestoesHabilitados[tipo]) {
                    return (
                      <MenuItem key={tipo} value={tipo}>
                        {strings[key]}
                      </MenuItem>
                    )
                  } else {
                    return null
                  }
                })}
              </Select>
            </FormControl>
          </div>
          <div className={classes.blocoSeletor}>
            <FormControl fullWidth>
              <TextField
                type="number"
                className={classes.textField}
                id="quantidadeDeQuestoes"
                value={quantidadeDeQuestoes ? quantidadeDeQuestoes : 1}
                label={strings.selecionarQuantasQuestoes}
                onChange={this.handleChange('quantidadeDeQuestoes')}
                onKeyPress={handleKeyPress}
                margin="normal"
                fullWidth
                inputProps={{ min: '0' }}
              />
            </FormControl>
          </div>
        </div>
        <div className={classes.botoes}>{this.props.children}</div>
      </div>
    )
  }
}
