import React, { Component, Fragment } from 'react'
import Button from '@material-ui/core/Button'
import SwipeableViews from 'react-swipeable-views'
import { bindKeyboard } from 'react-swipeable-views-utils'

import { MobileStepper } from '@material-ui/core'

import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft'
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight'
import { propTypes } from './propTypes'

const BindKeyboardSwipeableViews = bindKeyboard(SwipeableViews)

export class SwipeableContainerComponent extends Component {
  static propTypes = propTypes

  constructor(props) {
    super(props)
    this.state = {
      activeStep: 0,
    }
  }

  handleNext = () => {
    this.setState(prevState => ({
      activeStep: prevState.activeStep + 1,
    }))
  }

  handleBack = () => {
    this.setState(prevState => ({
      activeStep: prevState.activeStep - 1,
    }))
  }

  handleStepChange = activeStep => {
    this.setState({ activeStep })
  }

  render() {
    const { classes, theme, children } = this.props
    const { activeStep } = this.state
    const maxSteps = children.length

    return (
      <Fragment>
        <BindKeyboardSwipeableViews
          axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
          index={activeStep}
          onChangeIndex={this.handleStepChange}
          enableMouseEvents
        >
          {children.map(item => item)}
        </BindKeyboardSwipeableViews>
        <MobileStepper
          className={classes.mobileStepper}
          steps={maxSteps}
          position="static"
          activeStep={activeStep}
          nextButton={
            <Button size="small" id="next" onClick={this.handleNext} disabled={activeStep === maxSteps - 1}>
              {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
            </Button>
          }
          backButton={
            <Button size="small" id="back" onClick={this.handleBack} disabled={activeStep === 0}>
              {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
            </Button>
          }
        />
      </Fragment>
    )
  }
}
