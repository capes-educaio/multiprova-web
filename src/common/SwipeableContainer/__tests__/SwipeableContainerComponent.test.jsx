import React from 'react'
import Enzyme, { mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import { ptBr } from 'utils/strings/ptBr'

import { SwipeableContainerComponent } from '../SwipeableContainerComponent'
import { mountTest } from 'localModules/testUtils/mountTest'

Enzyme.configure({ adapter: new Adapter() })

const requiredProps = {
  children: [<div key="1" />, <div key="2" />, <div key="3" />],

  theme: {
    direction: '',
  },
  // style
  classes: {},
  // strings
  strings: ptBr,
}

const arrayOfProps = [requiredProps]

mountTest(SwipeableContainerComponent, 'SwipeableContainerComponent')(arrayOfProps)

describe(`Testando botões`, () => {
  const wrapper = mount(<SwipeableContainerComponent {...requiredProps} />)
  it(`button handleNext`, () => {
    const spy = jest.spyOn(wrapper.instance(), 'handleNext')
    wrapper.instance().forceUpdate()
    const botao = wrapper.find(`button#next`)
    botao.simulate('click', spy)
    expect(spy).toHaveBeenCalled()
  })
  it(`button handleBack`, () => {
    const spy = jest.spyOn(wrapper.instance(), 'handleBack')
    wrapper.instance().forceUpdate()
    const botao = wrapper.find(`button#back`)
    botao.simulate('click')
    expect(spy).toHaveBeenCalled()
    wrapper.unmount()
  })
})
