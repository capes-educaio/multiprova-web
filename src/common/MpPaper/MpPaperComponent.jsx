import React, { Component } from 'react'
import classnames from 'classnames'
import { Paper } from '@material-ui/core'
import { propTypes } from './propTypes'

export class MpPaperComponent extends Component {
  static propTypes = propTypes

  render() {
    const { classes, children, className } = this.props
    return <Paper className={classnames(classes.root, { [className]: className })}>{children}</Paper>
  }
}
