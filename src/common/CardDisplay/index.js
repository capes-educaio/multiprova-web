import { CardDisplayComponent } from './CardDisplayComponent'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import { withStyles } from '@material-ui/core/styles'

import { withFiltragemContext } from 'common/Filtragem/context'

import { mapStateToProps, mapDispatchToProps } from './redux'
import { style } from './style'

export const CardDisplay = compose(
  withStyles(style),
  withRouter,
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withFiltragemContext,
)(CardDisplayComponent)
