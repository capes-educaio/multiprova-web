import React, { Component } from 'react'
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'

import CircularProgress from '@material-ui/core/CircularProgress'
import { bancoDeQuestoes } from 'utils/enumBancoDeQuestoes'

import { get } from 'api'
import { Pagination } from 'common/Pagination'

import { Coluna } from './Coluna'
import { propTypes } from './propTypes'

export class CardDisplayComponent extends Component {
  static defaultProps = {
    numeroDeColunas: {
      extraSmall: 1,
      small: 1,
      medium: 2,
      large: 3,
      extraLarge: 3,
    },
    quantidadePorPaginaInicial: 10,
    paginaInicial: 0,
    quantidadePorPaginaOpcoes: [10, 20, 50, 100],
    order: ['dataUltimaAlteracao DESC'],
  }

  static propTypes = propTypes

  refsCards = {}

  constructor(props) {
    super(props)
    this.state = {
      carregando: true,
      instancias: [],
      matrizInstancias: null,
      numeroDeColunas: null,
      numeroDeLinhas: null,
      quantidadeTotal: null,
      quantidadePorPagina: props.quantidadePorPaginaInicial,
      cardsInicialmenteExpandidos: props.cardsInicialmenteExpandidos,
      paginaAtual: 0,
    }
    this._rootRef = React.createRef()
  }

  componentDidMount = async () => {
    const { onRef } = this.props
    if (onRef) onRef(this)
    await this.fetchInstancias()
  }

  UNSAFE_componentWillReceiveProps = async proximo => {
    const { browser, location } = this.props
    if (browser.mediaType !== proximo.browser.mediaType) {
      this.setNumeroDeColunas(proximo)
    }
    if (proximo.location.state.hidden.filtro !== location.state.hidden.filtro) {
      await this.fetchInstancias(proximo, true)
    }
  }

  componentWillUnmount = () => {
    const { onRef } = this.props
    if (onRef) onRef(null)
  }

  onChangeQuantidadePorPagina = quantidadePorPagina => {
    this.setState({ quantidadePorPagina }, this.fetchInstancias)
  }

  onChangePagina = paginaAtual => {
    this.setState({ paginaAtual }, this.fetchInstancias)
    this.scrollToMyRef()
  }

  getQuantidade = props => {
    let { quantidadeUrl, location } = props ? props : this.props
    if (!quantidadeUrl) quantidadeUrl = this.props.quantidadeUrl
    if (!location) location = this.props.location
    const { filtro } = location.state.hidden
    get(quantidadeUrl, filtro).then(response => {
      const data = response.data
      if (data) {
        const quantidadeTotal = data.count
        if (typeof quantidadeTotal === 'number') {
          this.setState({ quantidadeTotal })
        }
      }
    })
  }

  get _where() {
    const { filtragemContext, location } = this.props
    const { filtro } = location.state.hidden
    let where = {}
    if (filtro && filtro.where) where = { ...where, ...filtro.where }
    if (filtragemContext) {
      const contextWhere = filtragemContext.montarFiltro()
      if (contextWhere) where = { ...where, ...contextWhere }
    }
    return where
  }

  fetchInstancias = async (props = {}, resetPaginaAtual) => {
    this.setState({ carregando: true })
    this.getQuantidade(props)
    const {
      fetchInstanciasUrl = this.props.fetchInstanciasUrl,
      order = this.props.order,
      location = this.props.location,
      bancoDeQuestoesOrigem = this.props.bancoDeQuestoesOrigem,
    } = props
    const { quantidadePorPagina, paginaAtual } = this.state
    const { filtro } = location.state.hidden
    const options = {
      filter: {
        order,
        limit: quantidadePorPagina,
        skip: resetPaginaAtual ? 0 : paginaAtual * quantidadePorPagina,
        where: this._where,
        ...filtro,
      },
    }
    const response = await get(fetchInstanciasUrl, options)
    const instancias = this.adapterQuestoes(bancoDeQuestoesOrigem, response.data)
    this.setState({ instancias, carregando: false, paginaAtual }, () => this.setNumeroDeColunas())
  }

  _fetchQuantidadeEmbed = async () => { 
    const { data } = await get(this.props.fetchInstanciasUrl, { filter: { where: this._where } })
    return data.length
  }

  refreshEmbed = async ({ pagina } = {}) => {
    this.setState({ carregando: true })
    const { quantidadePorPagina } = this.state
    const { fetchInstanciasUrl, order } = this.props
    let irParaPagina = typeof pagina === 'number' ? pagina : this.state.paginaAtual
    const filter = {
      order,
      limit: quantidadePorPagina,
      skip: irParaPagina * quantidadePorPagina,
      where: this._where,
    }
    const { data: instancias } = await get(fetchInstanciasUrl, { filter })
    const quantidadeTotal = await this._fetchQuantidadeEmbed()
    this.setState({ instancias, carregando: false, paginaAtual: irParaPagina, quantidadeTotal }, () =>
      this.setNumeroDeColunas(),
    )
  }

  adapterQuestoes = (bancoDeQuestoesOrigem, dados) => {
    if (bancoDeQuestoesOrigem) {
      switch (bancoDeQuestoesOrigem) {
        case bancoDeQuestoes.compartilhadas:
          return dados.map(item => {
            const questao = { ...item.questao, id: item.id, questaoId: item.questao.id }
            return questao
          })
        default:
          return dados
      }
    }
    return dados
  }

  setNumeroDeLinhas = () => {
    const { numeroDeColunas, instancias } = this.state
    const maxQuantidadePorPagina = this.state.quantidadePorPagina
    const quantidadeAtual = instancias.length
    const numeroDeLinhasPadrao = maxQuantidadePorPagina / numeroDeColunas
    if (quantidadeAtual < maxQuantidadePorPagina) {
      let numeroDeLinhas = quantidadeAtual / numeroDeColunas
      const numeroDeLinhasArredondado = ~~numeroDeLinhas
      if (numeroDeLinhas > numeroDeLinhasArredondado) {
        numeroDeLinhas = numeroDeLinhasArredondado + 1
      }
      this.setState({ numeroDeLinhas }, () => this.setMatrizInstancias(instancias))
    } else {
      this.setState(
        {
          numeroDeLinhas: numeroDeLinhasPadrao,
        },
        () => this.setMatrizInstancias(instancias),
      )
    }
  }

  setNumeroDeColunas = props => {
    let numeroDeColunas, browser
    if (props) {
      numeroDeColunas = props.numeroDeColunas
      browser = props.browser
    } else {
      numeroDeColunas = this.props.numeroDeColunas
      browser = this.props.browser
    }
    if (typeof numeroDeColunas === 'number') {
      this.setState({ numeroDeColunas }, () => this.setNumeroDeLinhas())
    } else {
      const numeroColunasResponsivo = numeroDeColunas[browser.mediaType]
      if (numeroColunasResponsivo)
        this.setState({ numeroDeColunas: numeroColunasResponsivo }, () => this.setNumeroDeLinhas())
      else {
        console.error(
          `props.numeroDeColunas deve conter especificação para todos os tipos de media ou ser um número. Ex.
        {
          extraSmall: 1,
          small: 1,
          medium: 2,
          large: 2,
          extraLarge: 3,
        }`,
          numeroColunasResponsivo,
        )
        this.setState({ numeroDeColunas: 1 }, () => this.setNumeroDeLinhas())
      }
    }
  }

  setMatrizInstancias = instanciasParam => {
    const { numeroDeLinhas, numeroDeColunas } = this.state
    const instancias = [...instanciasParam]
    const matrizInstancias = []
    for (let coluna = 0; coluna < numeroDeColunas; coluna++) {
      matrizInstancias.push([])
    }
    for (let linha = 0; linha < numeroDeLinhas; linha++) {
      for (let coluna = 0; coluna < numeroDeColunas; coluna++) {
        const questao = instancias.shift()
        if (questao) {
          questao.open = false
          matrizInstancias[coluna][linha] = questao
        }
      }
    }
    this.setState({ matrizInstancias })
  }

  contrairTodas = () => {
    this.setState({ cardsInicialmenteExpandidos: false })
    for (let key in this.refsCards) {
      const refCard = this.refsCards[key]
      if (refCard && refCard.contrair) refCard.contrair()
    }
  }

  expandirTodas = () => {
    this.setState({ cardsInicialmenteExpandidos: true })
    for (let key in this.refsCards) {
      const refCard = this.refsCards[key]
      if (refCard && refCard.expandir) refCard.expandir()
    }
  }

  scrollToMyRef = () => this._rootRef && window.scrollTo({ behavior: 'smooth', top: this._rootRef.current.offsetTop })

  render() {
    const { matrizInstancias, quantidadeTotal, carregando, paginaAtual } = this.state
    const {
      classes,
      nadaCadastrado,
      quantidadePorPaginaInicial,
      quantidadePorPaginaOpcoes,
      selecionavel,
      CardComponent,
      contexto,
      cardComponentProps,
    } = this.props
    const className = classes.principal + (selecionavel ? ' selecionavel' : '')
    return (
      <div className={className} ref={this._rootRef} role="list">
        {carregando ? (
          <div className={classes.carregando}>
            <CircularProgress />
          </div>
        ) : null}
        {!carregando && matrizInstancias ? (
          <div className={classes.colunas}>
            {matrizInstancias.map((coluna, indexColuna) => (
              <Coluna
                key={indexColuna}
                coluna={coluna}
                indexColuna={indexColuna}
                CardComponent={CardComponent}
                contexto={contexto}
                quantidadeColunas={matrizInstancias.length}
                atualizarCardDisplay={this.fetchInstancias}
                cardComponentProps={cardComponentProps}
              />
            ))}
          </div>
        ) : null}
        {quantidadeTotal < 1 && !carregando && (
          <Paper className={classes.nadaCadastrado}>
            <Typography>{nadaCadastrado}</Typography>
          </Paper>
        )}
        {quantidadeTotal > 0 && (
          <Paper className={classes.pagination}>
            <Pagination
              paginaAtual={paginaAtual}
              quantidadeTotal={quantidadeTotal}
              quantidadePorPaginaInicial={quantidadePorPaginaInicial}
              onChangePagina={this.onChangePagina}
              onChangeQuantidadePorPagina={this.onChangeQuantidadePorPagina}
              quantidadePorPaginaOpcoes={quantidadePorPaginaOpcoes}
            />
          </Paper>
        )}
      </div>
    )
  }
}
