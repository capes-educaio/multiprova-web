import PropTypes from 'prop-types'

export const propTypes = {
  quantidadeUrl: PropTypes.string.isRequired,
  fetchInstanciasUrl: PropTypes.string.isRequired,
  nadaCadastrado: PropTypes.string.isRequired,
  numeroDeColunas: PropTypes.object,
  bancoDeQuestoes: PropTypes.string,
  quantidadePorPaginaInicial: PropTypes.number,
  paginaInicial: PropTypes.number,
  quantidadePorPaginaOpcoes: PropTypes.array,
  cardsInicialmenteExpandidos: PropTypes.bool,
  order: PropTypes.array,
  onRef: PropTypes.func,
  CardComponent: PropTypes.any.isRequired,
  contexto: PropTypes.object,
  // redux state
  browser: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
  // router
  history: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
}
