export const style = theme => ({
  principal: {
    [theme.breakpoints.up('md')]: {
      margin: 0,
    },
  },
  carregando: {
    display: 'flex',
    justifyContent: 'center',
    margin: '100px 0',
  },
  colunas: {
    display: 'flex',
    width: '100%',
    [theme.breakpoints.up('md')]: {
      margin: '10px 0 0 0',
    },
    margin: '1px 0',
  },
  pagination: {
    margin: '0',
  },
  nadaCadastrado: {
    padding: '20px 20px 20px 20px',
    margin: '0 0 20px 0',
  },
})
