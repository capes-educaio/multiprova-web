import { bindActionCreators } from 'redux'
// import { actions } from 'utils/actions'

export function mapStateToProps(state) {
  return {
    browser: state.browser,
  }
}

export function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch)
}
