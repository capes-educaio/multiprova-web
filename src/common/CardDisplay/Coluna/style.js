export const style = theme => ({
  card: {
    margin: '0 0 10px 0',
  },
  colunaInner: {
    // backgroundColor: 'rgb(240, 240, 247)',
    display: 'flex',
    flexDirection: 'column',
  },
  colunaOutter: {
    width: '1%',
    flex: '1 1 auto',
    margin: '0 10px 0 0',
  },
  colunaOutterLast: {
    width: '1%',
    flex: '1 1 auto',
    margin: '0',
  },
})
