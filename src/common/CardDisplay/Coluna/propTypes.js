import PropTypes from 'prop-types'

export const propTypes = {
  coluna: PropTypes.arrayOf(PropTypes.object).isRequired,
  indexColuna: PropTypes.number.isRequired,
  CardComponent: PropTypes.any.isRequired,
  atualizarCardDisplay: PropTypes.func.isRequired,
  quantidadeColunas: PropTypes.number.isRequired,
  contexto: PropTypes.object,
  // redux state
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
