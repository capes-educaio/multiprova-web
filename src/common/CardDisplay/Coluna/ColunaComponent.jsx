import React, { Component } from 'react'

import { propTypes } from './propTypes'

export class ColunaComponent extends Component {
  static propTypes = propTypes

  render() {
    const {
      coluna,
      indexColuna,
      CardComponent,
      classes,
      contexto,
      quantidadeColunas,
      atualizarCardDisplay,
      cardComponentProps,
    } = this.props
    return (
      <div className={indexColuna + 1 === quantidadeColunas ? classes.colunaOutterLast : classes.colunaOutter}>
        <div className={classes.colunaInner}>
          {coluna.map(instancia => (
            <div key={instancia.id} className={classes.card}>
              <CardComponent
                instancia={instancia}
                contexto={contexto}
                atualizarCardDisplay={atualizarCardDisplay}
                {...cardComponentProps}
              />
            </div>
          ))}
        </div>
      </div>
    )
  }
}
