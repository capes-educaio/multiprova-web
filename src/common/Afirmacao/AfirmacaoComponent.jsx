import React, { Component } from 'react'
import classnames from 'classnames'

import Typography from '@material-ui/core/Typography'
import { MpParser } from 'common/MpParser'

import { propTypes } from './propTypes'
import { defaultProps } from './defaultProps'

export class AfirmacaoComponent extends Component {
  static propTypes = propTypes
  static defaultProps = defaultProps

  render() {
    const { afirmacao, mostrarResposta, classes } = this.props
    return (
      <div className={classes.afirmacao}>
        <Typography className={classes.letra}>
          {'('}
          <span className={classnames({ [classes.espaco]: !mostrarResposta })}>
            {mostrarResposta && afirmacao.letra.toUpperCase()}
          </span>
          {')'}
        </Typography>
        <MpParser>{afirmacao.texto}</MpParser>
      </div>
    )
  }
}
