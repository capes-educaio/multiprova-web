const widthLetra = '25px'

export const style = theme => ({
  afirmacao: {
    display: 'flex',
    alignItems: 'flex-start',
  },
  letra: {
    minWidth: widthLetra,
  },
  texto: {
    width: `calc(100% - ${widthLetra})`,
    marginTop: '2px',
  },
  espaco: {
    marginLeft: '10px',
  },
})
