import React, { Component } from 'react'

import IconButton from '@material-ui/core/IconButton'

import CloseIcon from '@material-ui/icons/Close'

import { DeleteIcon } from 'localModules/icons/DeleteIcon'

import { MpIconButton } from 'common/MpIconButton'

import { propTypes } from './propTypes'

export class BotaoExcluirComponent extends Component {
  static propTypes = propTypes

  openDialogExcluir = () => {
    const { listaDialogPush, strings, stringExcluir, instancia, excluir } = this.props
    const dialogConfig = {
      titulo: strings.excluir,
      texto: stringExcluir,
      actions: [
        {
          texto: strings.ok,
          onClick: excluir(instancia),
        },
        {
          texto: strings.cancelar,
        },
      ],
    }
    listaDialogPush(dialogConfig)
  }

  render() {
    const { variant, buttonClass, color, buttonProps } = this.props
    if (variant === 'mp') {
      return (
        <MpIconButton
          color={color ? color : 'fundoCinza'}
          IconComponent={DeleteIcon}
          className={buttonClass}
          onClick={this.openDialogExcluir}
          {...buttonProps}
        />
      )
    } else {
      return (
        <IconButton className={buttonClass} onClick={this.openDialogExcluir} {...buttonProps}>
          <CloseIcon style={{ fontSize: 20 }} />
        </IconButton>
      )
    }
  }
}
