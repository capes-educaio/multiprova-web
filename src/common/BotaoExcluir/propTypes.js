import PropTypes from 'prop-types'

export const propTypes = {
  instancia: PropTypes.any,
  excluir: PropTypes.func.isRequired,
  children: PropTypes.func,
  buttonProps: PropTypes.object,
  // redux actions
  listaDialogPush: PropTypes.func.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
