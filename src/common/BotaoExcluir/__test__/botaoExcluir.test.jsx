import React from 'react'
import { mount } from 'enzyme'
import configureStore from 'redux-mock-store'

import { ptBr } from 'utils/strings/ptBr'
import { enumTipoQuestao } from 'utils/enumTipoQuestao'
import { BotaoExcluirComponent } from '../BotaoExcluirComponent'

jest.mock('utils/actions', () => ({
  actions: { push: jest.fn() },
}))

const questao = {
  id: '75cc4eff-6d71-4d55-821a-4ff6b8653695',
  dataCadastro: '2018-07-25T14:48:55.348Z',
  dataUltimaAlteracao: '2018-07-25T14:48:55.348Z',
  enunciado: '',
  dificuldade: 2,
  tipo: enumTipoQuestao.tipoQuestaoMultiplaEscolha,
  multiplaEscolha: {
    respostaCerta: 'a',
    alternativas: [
      {
        texto: '<p>150 reais</p>',
        letra: 'a',
      },
      {
        texto: '<p>450 reais</p>',
        letra: 'b',
      },
    ],
  },
}

const mockStore = configureStore()
const store = mockStore({
  strings: ptBr,
  listaDialogPush: jest.fn(),
})

const defaultProps = {
  strings: ptBr,
  classes: {},
  instancia: questao,
  stringExcluir: ptBr.desejaExcluirQuestaoDaProva,
  excluir: jest.fn(),
  listaDialogPush: jest.fn(),
}

describe('Testando <BotaoExcluir /> renderização e montagem', () => {
  it('Render e mount', () => {
    const wrapper = mount(<BotaoExcluirComponent store={store} {...defaultProps} />)
    expect(wrapper.length).toEqual(1)
    jest.clearAllMocks()
  })
})

describe('Testando <BotaoExcluir /> openDialogExcluirQuestaoDaProva', () => {
  it('Testa se openDialogExcluirQuestaoDaProva chama listaDialogPush com parametro passado ', () => {
    const wrapperParent = mount(<BotaoExcluirComponent store={store} {...defaultProps} />)
    const wrapper = wrapperParent.find('BotaoExcluirComponent')
    const spy = wrapper.instance().props.listaDialogPush
    const RESULTADO_ESPERADO = {
      titulo: defaultProps.strings.excluir,
      texto: defaultProps.strings.desejaExcluirQuestaoDaProva,
      actions: [
        {
          texto: defaultProps.strings.ok,
          onClick: defaultProps.excluir(questao),
        },
        {
          texto: defaultProps.strings.cancelar,
        },
      ],
    }
    wrapper.instance().openDialogExcluir()
    expect(spy).toHaveBeenCalledWith(RESULTADO_ESPERADO)
    jest.clearAllMocks()
  })

  it('Testa se openDialogExcluirQuestaoDaProva é chamada pelo clique do botão', () => {
    const wrapperParent = mount(<BotaoExcluirComponent store={store} {...defaultProps} />)
    const wrapper = wrapperParent.find('BotaoExcluirComponent')
    const spy = jest.spyOn(wrapper.instance(), 'openDialogExcluir')
    wrapper.instance().forceUpdate()
    const botao = wrapper.find('button')
    botao.at(0).simulate('click', spy)
    expect(spy).toHaveBeenCalled()
    jest.clearAllMocks()
  })
})
