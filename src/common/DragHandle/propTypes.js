import PropTypes from 'prop-types'

export const propTypes = {
  dragHandleProps: PropTypes.object,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
