import React, { Component } from 'react'

import DragIndicatorIcon from '@material-ui/icons/DragIndicator'

import { propTypes } from './propTypes'

export class DragHandleComponent extends Component {
  static propTypes = propTypes

  render() {
    const { dragHandleProps } = this.props
    return (
      <div>
        {dragHandleProps ? (
          <div {...dragHandleProps} style={{ margin: '0 5px' }}>
            <DragIndicatorIcon color="disabled" />
          </div>
        ) : (
          <DragIndicatorIcon color="disabled" style={{ margin: '0 5px' }} />
        )}
      </div>
    )
  }
}
