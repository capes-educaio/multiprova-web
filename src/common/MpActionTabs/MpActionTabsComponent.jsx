import React, { Component } from 'react'

import AppBar from '@material-ui/core/AppBar'

import { propTypes } from './propTypes'

export class MpActionTabsComponent extends Component {
  static propTypes = propTypes

  render() {
    const { mpTab, children, classes, appBarProps } = this.props

    return (
      <div>
        <AppBar {...appBarProps} position="static" color="default" elevation={0} className={classes.appBar}>
          {mpTab}
        </AppBar>
        {children}
      </div>
    )
  }
}
