import PropTypes from 'prop-types'

export const propTypes = {
  mpTab: PropTypes.node.isRequired,
  children: PropTypes.node.isRequired,
  appBarProps: PropTypes.object,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
