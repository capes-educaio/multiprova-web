import PropTypes from 'prop-types'

export const propTypes = {
  children: PropTypes.string,
  typographyProps: PropTypes.object,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
