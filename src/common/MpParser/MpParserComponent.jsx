import React, { Component } from 'react'

import Typography from '@material-ui/core/Typography'

import { MpEditor } from '../MpEditor'

import { propTypes } from './propTypes'

export class MpParserComponent extends Component {
  static propTypes = propTypes

  render() {
    const typographyProps = {
      headlineMapping: { body2: 'span' },
      align: 'justify',
      ...this.props.typographyProps,
    }
    return (
      <Typography {...typographyProps}>
        <MpEditor html={this.props.children} readOnly />
      </Typography>
    )
  }
}
