import React, { Component } from 'react'

import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'
import Chip from '@material-ui/core/Chip'

import { propTypes } from './propTypes'

export class TurmaCardComponent extends Component {
  static propTypes = propTypes

  alunoMinuscula(quantidade) {
    const { strings } = this.props
    return (
      strings
        .getAlunoPlural({ numeroUsos: quantidade })
        .charAt(0)
        .toLowerCase() + strings.getAlunoPlural({ numeroUsos: quantidade }).slice(1)
    )
  }
  get labelNumeroDeAlunos() {
    const { instancia } = this.props
    return instancia.alunos.length + ' ' + this.alunoMinuscula(instancia.alunos.length)
  }

  render() {
    const { classes, strings, instancia, opcoes, onClick } = this.props
    return (
      <Paper id={instancia.id} className={classes.cardClick} onClick={onClick}>
        <div className={classes.cardTop}>
          <div className={classes.cardTopEsquerda}>
            <Typography className={classes.cardHeader} variant="caption">
              {this.labelNumeroDeAlunos}
            </Typography>
          </div>
          <div>{opcoes}</div>
        </div>
        <div className={classes.cardContent}>
          <div className={classes.titulo}>
            <Typography className={classes.tituloTurma} variant="subtitle1">
              {instancia.nome}
            </Typography> 
          </div>
        </div>
        <div className={classes.rodape}>
          {[
            { label: strings.numero + ': ' + instancia.numero },
            { label: strings.periodo + ': ' + instancia.ano + '.' + instancia.periodo },
          ].map(({ label }, index) => (
            <Chip key={index} label={label} className={classes.chip} variant="outlined" color="primary" />
          ))}
        </div>
      </Paper>
    )
  }
}
