export const style = theme => ({
  cardTop: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  cardHeader: {
    padding: '10px 24px 10px 20px',
  },
  cardTopEsquerda: {
    display: 'flex',
    flexDirection: 'column',
    alignSelf: 'flex-end',
    alignItems: 'left',
  },
  cardContent: {
    wordWrap: 'break-word',
    padding: '0px 20px 10px',
  },
  titulo: {
    display: 'flex',
    justifyContent: 'space-between',
    '& > :last-child': {
      flexShrink: '0',
      width: 'fit-content',
    },
  },
  tituloTurma: {
    fontWeight: 550,
    color: theme.palette.primary.main,
    fontSize: 20,
    lineHeight: 'normal',
    textAlign: 'left',
  },
  rodape: {
    margin: '0 20px 10px',
  },
  chip: {
    margin: '0 10px 10px 0',
  },
  cardClick: {
    cursor: 'pointer',
    borderRadius: 0,
    padding: '8px 0px 0px',
  },
})
