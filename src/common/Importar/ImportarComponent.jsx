import React, { Component } from 'react'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogContent from '@material-ui/core/DialogContent'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'

import { MpButton } from 'common/MpButton'
import ImportIcon from 'localModules/icons/ImportIcon'
import { post } from 'api'

import { propTypes } from './propTypes'

export class ImportarComponent extends Component {
  static propTypes = propTypes
  constructor(props) {
    super(props)
    this.state = {
      openDialogErro: false,
      nomesArquivosComErro: [],
    }
  }

  gerenciarArquivoimportado = async (arquivo, nomeArquivo) => {
    const { processarArquivo, aposImportacao, pularSalvamento } = this.props
    let dados = arquivo
    try {
      if (processarArquivo) {
        dados = await processarArquivo(arquivo)
      }
      if (!pularSalvamento) {
        ;({ data: dados } = await this.salvarArquivo(dados))
      }
      if (aposImportacao) await aposImportacao(dados)
      return dados
    } catch (erro) {
      this.erroSalvamento(nomeArquivo, erro)
      return null
    }
  }

  salvarArquivo = arquivo => post(this.props.url, arquivo)

  importarQuestao = async event => {
    this.setState({ nomesArquivosComErro: [] })
    const input = event.target
    const arquivos = input.files
    const importacoes = []
    Array.from(arquivos).forEach(arquivo => {
      const reader = new FileReader()
      reader.onload = () => {
        const arquivoJson = reader.result
        importacoes.push(this.gerenciarArquivoimportado(arquivoJson, arquivo.name))
      }
      reader.readAsText(arquivo)
    })
    await Promise.all(importacoes)
    if (this.props.onEndImportacoes) this.props.onEndImportacoes(importacoes)
  }

  handleClose = () => {
    this.setState({ openDialogErro: false, nomesArquivosComErro: [] })
  }

  erroSalvamento = (nomeArquivo, erro) => {
    const { nomesArquivosComErro } = this.state
    nomesArquivosComErro.push(nomeArquivo)
    this.setState({ openDialogErro: true, nomesArquivosComErro: [...nomesArquivosComErro] })
    if (erro) {
      console.error(erro)
    }
  }

  render() {
    const { strings, classes, iconProps } = this.props
    const { nomesArquivosComErro } = this.state
    return (
      <React.Fragment>
        <label htmlFor="importarArquivos" className={classes.label}>
          <MpButton
            className={classes.botaoImportar}
            classes={{ icon: classes.icone }}
            buttonProps={{ variant: 'contained', component: 'span' }}
            IconComponent={ImportIcon}
            variant="material"
            iconProps={iconProps ? iconProps : null}
          >
            <input
              accept="application/json"
              className={classes.input}
              id="importarArquivos"
              type="file"
              multiple
              onChange={this.importarQuestao}
              value=""
            />
            {strings.importar}
          </MpButton>
        </label>
        <Dialog open={this.state.openDialogErro} onClose={this.handleClose}>
          <DialogTitle>
            {strings.erroImportacao}
            {nomesArquivosComErro.map((nome, i) => {
              const ultimoNome = nomesArquivosComErro.length - 1
              return ultimoNome === i ? ` ${nome}.` : `${nome},`
            })}
          </DialogTitle>
          <DialogContent>
            <DialogContentText>{strings.conferirArquivoTentarNovamente}</DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              {strings.ok}
            </Button>
          </DialogActions>
        </Dialog>
      </React.Fragment>
    )
  }
}
