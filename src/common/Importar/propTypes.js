import PropTypes from 'prop-types'

export const propTypes = {
  processarArquivo: PropTypes.func,
  url: PropTypes.string.isRequired,
  aposImportacao: PropTypes.func,
  pularSalvamento: PropTypes.bool,
  onEndImportacoes: PropTypes.func,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
