import PropTypes from 'prop-types'

export const propTypes = {
  conteudoDasTabs: PropTypes.arrayOf(PropTypes.node),
  tabIndex: PropTypes.number.isRequired,
  handleChange: PropTypes.func.isRequired,
  outrosPropsSwipeable: PropTypes.object,
  outrosPropsTypography: PropTypes.object,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
}
