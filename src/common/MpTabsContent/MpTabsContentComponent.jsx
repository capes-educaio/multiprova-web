import React, { Component } from 'react'

import SwipeableViews from 'react-swipeable-views'
import Typography from '@material-ui/core/Typography'

import { propTypes } from './propTypes'

export class MpTabsContentComponent extends Component {
  static propTypes = propTypes

  render() {
    const {
      conteudoDasTabs,
      theme,
      tabIndex,
      handleChange,
      classes,
      outrosPropsSwipeable,
      outrosPropsTypography,
    } = this.props
    return (
      <SwipeableViews
        {...outrosPropsSwipeable}
        axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
        index={tabIndex}
        onChangeIndex={handleChange}
      >
        {conteudoDasTabs.map((item, index) => (
          <Typography
            {...outrosPropsTypography}
            key={index}
            component="div"
            dir={theme.direction}
            className={classes.conteudo}
          >
            {item}
          </Typography>
        ))}
      </SwipeableViews>
    )
  }
}
