import { MpButtonGroupComponent } from '../MpButtonGroupComponent'
import { mountTest } from 'localModules/testUtils/mountTest'

const requiredProps = {
  // style
  classes: {},
  // strings
  strings: {},
  // theme
  theme: { palette: {} },
}

const optionalProps = {}

const arrayOfProps = [requiredProps, { ...requiredProps, ...optionalProps }]

mountTest(MpButtonGroupComponent, 'MpButtonGroupComponent')(arrayOfProps)
