import React, { Component } from 'react'
import classnames from 'classnames'

import { propTypes } from './propTypes'

export class MpButtonGroupComponent extends Component {
  static propTypes = propTypes

  get children() {
    if (Array.isArray(this.props.children)) return this.removeBooleanFalse(this.props.children)
    else return [this.removeBooleanFalse(this.props.children)]
  }

  removeBooleanFalse = children => {
    if (Array.isArray(this.props.children)) return children.filter(obj => obj)
    else return children
  }

  render() {
    const { classes, className } = this.props
    return (
      <div
        className={classnames(classes.root, {
          [className]: Boolean(className),
        })}
      >
        {this.children.map((button, index) => {
          if (index === 0) return button
          return [<div className={classes.verticalDivider} key={'verticalDivider' + index} />, button]
        })}
      </div>
    )
  }
}
