import PropTypes from 'prop-types'

export const propTypes = {
  children: PropTypes.node,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
  // theme
  theme: PropTypes.object.isRequired,
}
