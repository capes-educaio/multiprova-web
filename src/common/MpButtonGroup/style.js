export const style = theme => ({
  root: {
    display: 'flex',
    borderRadius: 5,
    overflow: 'hidden',
    maxHeight: 34,
  },
  verticalDivider: {
    padding: '0 1px 0 0',
    backgroundColor: theme.palette.cinzaAlto,
  },
})
