import React, { Component } from 'react'
import { Editor, EditorState } from 'draft-js'

import { htmlParaDraft } from 'utils/conversoesDraft'

import { blockRenderer } from './blockRenderer'
import { propTypes } from './propTypes'

export class StaticRichTextComponent extends Component {
  static propTypes = propTypes

  render() {
    const { html, classes } = this.props
    const editorState = EditorState.createWithContent(htmlParaDraft(html))
    return (
      <div className={classes.root}>
        <Editor editorState={editorState} blockRendererFn={blockRenderer} readOnly />
      </div>
    )
  }
}
