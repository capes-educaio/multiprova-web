export const style = () => ({
  root: {
    '& .DraftEditor-root': {
      overflow: 'hidden',
      '& figure': {
        margin: 0,
        textAlign: 'center',
      },
    },
  },
})
