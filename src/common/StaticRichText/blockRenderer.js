import { AtomicBlock } from 'common/AtomicBlock'

export const blockRenderer = block => {
  if (block.getType() === 'atomic') {
    return {
      component: AtomicBlock,
      editable: false,
      props: { isStatic: true },
    }
  }
  return null
}
