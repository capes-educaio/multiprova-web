import PropTypes from 'prop-types'

export const propTypes = {
  html: PropTypes.string.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
