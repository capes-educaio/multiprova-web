import PropTypes from 'prop-types'

export const propTypes = {
  // lista de usuarios a ser listados
  participantes: PropTypes.array.isRequired,
  // lista de usuario que foram selecionados
  participantesSelecionados: PropTypes.array,
  // recebe o objeto do usuario selecionado
  marcarUsuario: PropTypes.func,
  // recebe o objeto do usuario selecionado
  desmarcarUsuario: PropTypes.func,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
