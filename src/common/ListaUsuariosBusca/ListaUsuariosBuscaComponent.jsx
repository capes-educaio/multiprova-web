import { Person } from '@material-ui/icons'
import { MpLista } from 'common/MpLista'
import React, { Component } from 'react'
import { propTypes } from './propTypes'

export class ListaUsuariosBuscaComponent extends Component {
  static propTypes = propTypes

  state = { checkboxes: [] }

  static getDerivedStateFromProps = nextProps => {
    const { participantesSelecionados } = nextProps
    const checkboxes = nextProps.participantes.map(participante => {
      if (!participantesSelecionados.map(part => part.id).includes(participante.id)) {
        return false
      } else return true
    })

    return { checkboxes }
  }
  get actionsDaListaDeUsuarios() {
    return []
  }
  handleChangeCheckbox = (event, newCheckedValue, conteudo, index) => {
    const { checkboxes } = this.state
    const { marcarUsuario, participantes, desmarcarUsuario } = this.props
    checkboxes[index] = newCheckedValue
    if (newCheckedValue) marcarUsuario(participantes[index])
    else desmarcarUsuario(participantes[index])
    this.setState({ checkboxes })
  }

  render() {
    const { classes, participantes } = this.props
    const { checkboxes } = this.state
    const icon = <Person className={classes.icon} />
    return participantes.length > 0 ? (
      <MpLista
        checkboxEnabled
        lista={participantes.map(el =>
          el.matricula ? `${el.matricula} - ${el.nome} (${el.email})` : `${el.nome} (${el.email})`,
        )}
        actions={this.actionsDaListaDeUsuarios}
        className={classes.root}
        checkboxes={checkboxes}
        handleChangeCheckbox={this.handleChangeCheckbox}
        icon={icon}
      />
    ) : null
  }
}
