import { fade } from '@material-ui/core/styles/colorManipulator'

export const style = theme => ({
  item: {
    backgroundColor: theme.palette.gelo,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: '3px 6px',

    marginTop: '5px',
  },
  content: {
    display: 'flex',
    alignItems: 'center',
  },
  actions: {
    fill: theme.palette.cinzaEscuro,
    marginRight: 10,
    fontSize: 15,
    borderRadius: '5px',
    '& svg': {
      fill: theme.palette.cinzaDusty,
    },
  },
  icon: { fill: theme.palette.cinzaEscuro, marginRight: 10, fontSize: 20 },

  root: {
    padding: '10px',
    marginBottom: '10px',
  },
  list: {
    padding: '10px',
    width: '100%',
    backgroundColor: fade(theme.palette.common.black, 0.05),
  },
})
