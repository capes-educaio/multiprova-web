import React, { Component } from 'react'
import Tooltip from '@material-ui/core/Tooltip'

import { propTypes } from './propTypes'

export class TooltipParaButtonComponent extends Component {
  static propTypes = propTypes

  constructor(props) {
    super(props)
    if (props.onRef) props.onRef(this)
    this.state = { tooltipOpen: false }
  }

  componentWillUnmount = () => {
    if (this.props.onRef) this.props.onRef(null)
  }

  open = () => this.setState({ tooltipOpen: true })

  close = () => this.setState({ tooltipOpen: false })

  _onClick = e => {
    const { tooltip, tooltipProps } = this.props
    e.stopPropagation()
    if (tooltip || tooltipProps) this.close()
    if (this.props.onClick) this.props.onClick(e)
  }

  render() {
    const { children, tooltipProps, title } = this.props
    const { tooltipOpen } = this.state
    return (
      <Tooltip
        open={tooltipOpen}
        onOpen={this.open}
        onClose={this.close}
        title={title}
        disableFocusListener
        disableTouchListener
        {...tooltipProps}
      >
        {children}
      </Tooltip>
    )
  }
}
