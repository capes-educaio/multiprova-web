export const style = theme => ({
  paper: {
    margin: '10px 0px',
  },
  disableTopMargin: {
    backgroundColor: theme.palette.gelo,
    margin: '0 0 20px 0',
  },
  actions: {
    padding: 20,
  },
  '@media only screen and (max-width:430px)': {
    paper: {
      margin: 0,
    },
  },
})
