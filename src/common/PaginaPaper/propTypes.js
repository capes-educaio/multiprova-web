import PropTypes from 'prop-types'

export const propTypes = {
  variant: PropTypes.oneOf(['mp', 'material']),
  conteudo: PropTypes.node,
  children: PropTypes.node,
  disableTopMargin: PropTypes.bool,
  titulo: PropTypes.node,
  secao: PropTypes.bool,
  actions: PropTypes.node,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
