import React, { Component, Fragment } from 'react'

import MaterialPaper from '@material-ui/core/Paper'
import Divider from '@material-ui/core/Divider'

import { MpPaper } from 'common/MpPaper'
import { PaginaSecao } from 'common/PaginaSecao'
import { TituloDaPagina } from 'common/TituloDaPagina'
import { MpActionBar } from 'common/MpActionBar'

import { propTypes } from './propTypes'
import { defaultProps } from './defaultProps'

export class PaginaPaperComponent extends Component {
  static propTypes = propTypes
  static defaultProps = defaultProps

  render() {
    const { classes, children, disableTopMargin, titulo, secao, actions, variant } = this.props
    const conteudo = this.props.conteudo ? this.props.conteudo : children
    let corpo
    if (secao) corpo = <PaginaSecao>{conteudo}</PaginaSecao>
    else corpo = conteudo
    const mapVariantToPaper = { mp: MpPaper, material: MaterialPaper }
    const Paper = mapVariantToPaper[variant]
    return (
      <Fragment>
        <Paper className={disableTopMargin ? classes.disableTopMargin : classes.paper} elevation={1}>
          {titulo && (
            <div>
              <TituloDaPagina>{titulo}</TituloDaPagina>
              <Divider />
            </div>
          )}
          {corpo}
          {actions && variant === 'material' && (
            <Fragment>
              <Divider />
              <div className={classes.actions}>{actions}</div>
            </Fragment>
          )}
        </Paper>
        {variant === 'mp' && <MpActionBar>{actions}</MpActionBar>}
      </Fragment>
    )
  }
}
