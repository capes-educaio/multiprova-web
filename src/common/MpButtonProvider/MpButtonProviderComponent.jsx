import React, { Component } from 'react'

import { TooltipParaButton } from 'common/TooltipParaButton'

import { propTypes } from './propTypes'

export class MpButtonProviderComponent extends Component {
  static propTypes = propTypes
  _tooltipRef

  startLoad = () => this.setState({ loading: true })

  endLoad = () => this.setState({ loading: false })

  constructor(props) {
    super(props)
    if (props.onRef) props.onRef(this)
    this.state = { loading: false }
  }

  componentWillUnmount = () => {
    if (this.props.onRef) this.props.onRef(null)
  }

  get _buttonProps() {
    const buttonProps = { ...this.props.buttonProps }
    const propsInvalidos = ['onClick', 'children']
    propsInvalidos.forEach(propsKey => {
      if (buttonProps[propsKey]) {
        delete buttonProps[propsKey]
        console.warn(`O ${propsKey} deve ser passado nas props do MpButton.`)
      }
    })
    return buttonProps
  }

  _onClick = e => {
    const { loadOnClick } = this.props
    e.stopPropagation()
    if (loadOnClick) this.startLoad()
    if (this._tooltipRef) this._tooltipRef.close()
    if (this.props.onClick) this.props.onClick(e)
  }

  render() {
    const { children, tooltip, tooltipProps } = this.props
    const { loading } = this.state
    const button = children({ buttonProps: this._buttonProps, onClick: this._onClick, loading })
    if (tooltip || tooltipProps)
      return (
        <TooltipParaButton onRef={ref => (this._tooltipRef = ref)} title={tooltip} {...tooltipProps}>
          <div>{button}</div>
        </TooltipParaButton>
      )
    return button
  }
}
