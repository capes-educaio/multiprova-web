import PropTypes from 'prop-types'

export const propTypes = {
  children: PropTypes.func.isRequired,
  onClick: PropTypes.func,
  loadOnClick: PropTypes.bool,
  tooltip: PropTypes.string,
  tooltipProps: PropTypes.object,
  onRef: PropTypes.func,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
