import PropTypes from 'prop-types'

export const propTypes = {
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,

  salvarNoBanco: PropTypes.func,
  irPassoAnterior: PropTypes.func,
  abrirAvisoCancelar: PropTypes.func,
  children: PropTypes.any,
}
