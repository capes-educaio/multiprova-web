export const style = {
  form: { padding: '20px' },
  botaoSalvar: {
    margin: '20px 0 20px 20px',
  },
  botaoAdd: {
    position: 'sticky',
    bottom: 10,
    left: 9999,
    margin: '0 20px 0 0',
  },
}
