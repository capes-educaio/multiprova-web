import React, { Component } from 'react'

import Button from '@material-ui/core/Button'
import SendIcon from '@material-ui/icons/Send'
import CancelIcon from '@material-ui/icons/Cancel'
import ArrowBackIcon from '@material-ui/icons/ArrowBack'

import { propTypes } from './propTypes'

export class BotoesInferioresComponent extends Component {
  static propTypes = propTypes
  render() {
    const {
      classes,
      strings,
      salvarNoBanco,
      irPassoAnterior,
      abrirAvisoCancelar,
      children,
      labelPassoAnterior,
    } = this.props

    return (
      <span>
        <Button
          id="testButtonSalvar"
          onClick={salvarNoBanco}
          classes={{ root: classes.botaoSalvar }}
          variant="contained"
          color="primary"
        >
          <SendIcon />
          {strings.salvar}
        </Button>
        <Button onClick={irPassoAnterior}>
          <ArrowBackIcon />
          {labelPassoAnterior ? labelPassoAnterior : strings.editarCabecalho}
        </Button>
        <Button onClick={abrirAvisoCancelar}>
          <CancelIcon />
          {strings.cancelar}
        </Button>
        {children}
      </span>
    )
  }
}
