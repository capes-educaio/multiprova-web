import React, { Component } from 'react'

import Typography from '@material-ui/core/Typography'

import { propTypes } from './propTypes'

export class CampoViewComponent extends Component {
  static propTypes = propTypes

  render() {
    const { label, children, className, labelTypographyProps, childrenTypographyProps, htmlChildren } = this.props
    const htmlChildrenProps = htmlChildren ? { headlineMapping: { body2: 'span' } } : {}
    return (
      <div className={className}>
        <Typography variant="caption" {...labelTypographyProps}>
          {label}
        </Typography>
        <Typography {...htmlChildrenProps} {...childrenTypographyProps}>
          {children}
        </Typography>
      </div>
    )
  }
}
