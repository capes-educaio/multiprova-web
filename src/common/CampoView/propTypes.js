import PropTypes from 'prop-types'

export const propTypes = {
  label: PropTypes.node.isRequired,
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
  labelTypographyProps: PropTypes.object,
  childrenTypographyProps: PropTypes.object,
  htmlChildren: PropTypes.bool,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
