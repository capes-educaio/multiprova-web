import React, { Component } from 'react'

import { Editor } from 'multiprova-editor'
import { uploadFile } from 'utils/uploadFile'

export class MpEditorComponent extends Component {
  render() {
    const props = { ...this.props }
    props.defaultFontSize = 10
    return <Editor {...props} upload={uploadFile} />
  }
}
