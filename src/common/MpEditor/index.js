import { connect } from 'react-redux'

import { mapStateToProps, mapDispatchToProps } from './redux'
import { MpEditorComponent } from './MpEditorComponent'

export const MpEditor = connect(
  mapStateToProps,
  mapDispatchToProps,
)(MpEditorComponent)
