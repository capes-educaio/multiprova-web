import PropTypes from 'prop-types'

export const propTypes = {
  // tempo total
  tempo: PropTypes.number.isRequired,
  // style
  classes: PropTypes.object.isRequired,
  //quando acabar
  quandoFinalizar: PropTypes.func,
  // redux
  strings: PropTypes.object.isRequired,
}
