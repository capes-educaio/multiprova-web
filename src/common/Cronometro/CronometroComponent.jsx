import React from 'react'
import { propTypes } from './propTypes'
import Typography from '@material-ui/core/Typography'

export const CronometroComponent = ({ classes, tempo, strings }) => {
  const seconds = parseInt((tempo / 1000) % 60)
  const minutes = parseInt((tempo / 1000 - seconds) / 60) % 60
  const hours = parseInt(tempo / 3600000)
  return (
    <Typography className={classes.component} role="timer">
      {tempo > 0
        ? hours > 0
          ? hours + 'h ' + minutes + 'm'
          : minutes + 'm ' + seconds + 's'
        : strings.tempoFinalizado}
    </Typography>
  )
}
CronometroComponent.propTypes = propTypes
