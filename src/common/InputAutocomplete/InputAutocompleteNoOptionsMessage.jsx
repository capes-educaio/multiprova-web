import React from 'react'
import Typography from '@material-ui/core/Typography'

export function InputAutocompleteNoOptionsMessage(props) {
  return (
    <Typography
      color="textSecondary"
      className={props.selectProps.classes.noOptionsMessage}
      {...props.innerProps}
    >
      {props.children}
    </Typography>
  )
}