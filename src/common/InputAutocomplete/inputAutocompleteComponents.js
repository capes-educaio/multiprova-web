import { InputAutocompleteNoOptionsMessage } from './InputAutocompleteNoOptionsMessage'
import { InputAutocompleteControlComponent } from './InputAutocompleteControlComponent'
import { InputAutocompleteOptionComponent } from './InputAutocompleteOptionComponent'
import { InputAutocompletePlaceholderComponent } from './InputAutocompletePlaceholderComponent'
import { InputAutocompleteSingleValueComponent } from './InputAutocompleteSingleValueComponent'
import { InputAutocompleteValueContainerComponent } from './InputAutocompleteValueContainerComponent'
import { InputAutocompleteMultiValueComponent } from './InputAutocompleteMultiValueComponent'



export const inputAutocompleteComponents = {
  NoOptionsMessage: InputAutocompleteNoOptionsMessage,
  Control: InputAutocompleteControlComponent,
  Option: InputAutocompleteOptionComponent,
  Placeholder: InputAutocompletePlaceholderComponent,
  SingleValue: InputAutocompleteSingleValueComponent,
  ValueContainer: InputAutocompleteValueContainerComponent,
  MultiValue: InputAutocompleteMultiValueComponent,
}
