import React from 'react'
import Paper from '@material-ui/core/Paper'

export function InputAutocompleteMenuComponent(props) {
  return (
    <Paper square className={props.selectProps.classes.paper} {...props.innerProps}>
      {props.children}
    </Paper>
  )
}