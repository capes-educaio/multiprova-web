import PropTypes from 'prop-types'

export const propTypes = {
  classes: PropTypes.object.isRequired,
  strings: PropTypes.object.isRequired,
  lista: PropTypes.object.isRequired,
  placeholder: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
}
