import React from 'react'

export function InputAutocompleteInputComponent({ inputRef, ...props }) {
  return <div ref={inputRef} {...props} />
}