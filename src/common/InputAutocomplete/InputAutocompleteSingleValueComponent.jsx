import React from 'react'
import Typography from '@material-ui/core/Typography'

export function InputAutocompleteSingleValueComponent(props) {
  return (
    <Typography className={props.selectProps.classes.singleValue} {...props.innerProps}>
      {props.children}
    </Typography>
  )
}