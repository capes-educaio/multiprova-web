import React from 'react'
import TextField from '@material-ui/core/TextField'
import { InputAutocompleteInputComponent } from './InputAutocompleteInputComponent'

export function InputAutocompleteControlComponent(props) {
  return (
    <TextField
      fullWidth
      InputProps={{
        InputAutocompleteInputComponent,
        inputProps: {
          className: props.selectProps.classes.input,
          inputRef: props.innerRef,
          children: props.children,
          ...props.innerProps,
        },
      }}
      {...props.selectProps.textFieldProps}
    />
  )
}