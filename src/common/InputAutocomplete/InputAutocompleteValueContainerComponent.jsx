import React from 'react'

export function InputAutocompleteValueContainerComponent(props) {
  return <div className={props.selectProps.classes.valueContainer}>{props.children}</div>
}