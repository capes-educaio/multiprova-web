import React from 'react'
import PropTypes from 'prop-types'
import { BlockMath } from 'react-katex'

export function TexOutput({ content, onClick }) {
  return (
    <span onClick={onClick}>
      <BlockMath math={content} />
    </span>
  )
}

TexOutput.propTypes = {
  content: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
}
