import React from 'react'
import PropTypes from 'prop-types'

import { TexOutput } from './TexOutput'
import { ImageOutput } from './ImageOutput'
import { TYPE_TEX, TYPE_IMG } from 'utils/RichText/constantes'

export function AtomicBlockComponent({ contentState, block, blockProps }) {
  const entityKey = block.getEntityAt(0)
  const entity = contentState.getEntity(entityKey)
  const type = entity.getType()
  const { content } = entity.getData()
  const onClick = () => {
    const { isStatic, onClick } = blockProps
    if (!isStatic) onClick(type, entityKey, content)
  }
  const props = { content, onClick }
  switch (type) {
    case TYPE_TEX:
      return <TexOutput {...props} />
    case TYPE_IMG:
      return <ImageOutput {...props} />
    default:
      console.error('Tipo de bloco desconhecido')
      return false
  }
}

AtomicBlockComponent.propTypes = {
  contentState: PropTypes.object.isRequired,
  block: PropTypes.object.isRequired,
  blockProps: PropTypes.object.isRequired,
}
