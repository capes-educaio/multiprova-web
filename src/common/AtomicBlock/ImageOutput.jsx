import React from 'react'
import PropTypes from 'prop-types'

export function ImageOutput({ content, onClick }) {
  return (
    <div onClick={onClick}>
      <img src={content} alt="" />
    </div>
  )
}

ImageOutput.propTypes = {
  content: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
}
