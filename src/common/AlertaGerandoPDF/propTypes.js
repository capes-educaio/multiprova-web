import PropTypes from 'prop-types'

export const propTypes = {
  alertaAberto: PropTypes.bool.isRequired,
  stringsSalvarAntesDeImprimir: PropTypes.string,
  stringGerandoPDF: PropTypes.string.isRequired,
  pdf: PropTypes.object.isRequired,
  handleCloseCancelar: PropTypes.func.isRequired,
  alertaSalvarAberto: PropTypes.bool.isRequired,
  fecharAvisoSalvar: PropTypes.func.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
