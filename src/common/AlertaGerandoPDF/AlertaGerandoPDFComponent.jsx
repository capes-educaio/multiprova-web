import React, { Component } from 'react'

import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import Button from '@material-ui/core/Button'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'

import { AlertaDeConfirmacao } from 'common/AlertaDeConfirmacao'

import { propTypes } from './propTypes'

export class AlertaGerandoPDFComponent extends Component {
  static propTypes = propTypes

  render() {
    const {
      classes,
      alertaAberto,
      strings,
      pdf,
      handleCloseCancelar,
      alertaSalvarAberto,
      fecharAvisoSalvar,
      stringsSalvarAntesDeImprimir,
      stringGerandoPDF,
    } = this.props
    const GERANDO_PDF = 'GERANDO PDF'
    const ERROR = 'ERROR'
    const PRONTO = 'PRONTO'
    const { status } = pdf
    return (
      <div>
        <AlertaDeConfirmacao
          alertaAberto={alertaSalvarAberto}
          handleClose={fecharAvisoSalvar}
          descricaoSelected={stringsSalvarAntesDeImprimir}
          stringNomeInstancia={strings.gerarPdf}
        />
        {status === ERROR && !alertaSalvarAberto ? (
          <Dialog open={alertaAberto} onClose={handleCloseCancelar}>
            <DialogContent>
              <DialogContentText>{strings.erroPDF || ''}</DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={() => handleCloseCancelar(true)} color="primary">
                {strings.fechar}
              </Button>
            </DialogActions>
          </Dialog>
        ) : null}
        {status === GERANDO_PDF && !alertaSalvarAberto ? (
          <div className={classes.secao}>
            <Dialog open={alertaAberto}>
              <DialogContent>
                <DialogContentText>{stringGerandoPDF || ''}</DialogContentText>
              </DialogContent>
            </Dialog>
          </div>
        ) : null}
        {status === PRONTO && !alertaSalvarAberto ? (
          <Dialog open={alertaAberto} onClose={handleCloseCancelar}>
            <DialogContent>
              <DialogContentText>{strings.linkParaDownload || ''}</DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={() => handleCloseCancelar(true)} color="primary">
                {strings.fechar}
              </Button>
            </DialogActions>
          </Dialog>
        ) : null}
      </div>
    )
  }
}
