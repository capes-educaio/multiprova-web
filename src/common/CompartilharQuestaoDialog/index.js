import { CompartilharQuestaoDialogComponent } from './CompartilharQuestaoDialogComponent'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'

import { mapStateToProps, mapDispatchToProps } from './redux'
import { style } from './style'

export const CompartilharQuestaoDialog = compose(
  withStyles(style),
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(CompartilharQuestaoDialogComponent)
