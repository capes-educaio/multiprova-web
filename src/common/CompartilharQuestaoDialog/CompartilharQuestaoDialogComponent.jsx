import React, { Component } from 'react'
import { Dialog, DialogActions, DialogContent, DialogTitle, DialogContentText } from '@material-ui/core'
import { MpButton } from 'common/MpButton'
import { Select } from 'common/Autosuggest'
import { get } from 'api'
import { showMessage } from 'utils/Snackbar'

export class CompartilharQuestaoDialogComponent extends Component {
  state = {
    selectedUsers: [],
    options: [],
    loading: false,
  }
  searchUsers = input => {
    const { selectedUsers } = this.state
    if (!!input) {
      this.setState({ loading: true })
      get('/usuarios/usuarios-docente', {
        value: input,
        usuariosJaSelecionados: [this.props.usuarioAtual.data.id, ...selectedUsers.map(({ value }) => value)],
      }).then(({ data = {} }) => {
        const { usuarios = [] } = data
        this.setState({ loading: false, options: usuarios.map(({ email: label, id: value }) => ({ label, value })) })
      })
    } else this.setState({ options: [] })
  }
  share = async () => {
    const { selectedUsers } = this.state
    const { id, onClose, strings } = this.props
    await Promise.all(
      selectedUsers.map(({ value: destinatarioId }) => get(`/questoes/${id}/compartilhar`, { destinatarioId })),
    )
    showMessage.success({ message: strings.questaoCompartilhadaComSucesso })
    onClose()
  }
  render() {
    const { open = false, strings, onClose, classes } = this.props
    if (!open) return null
    const { selectedUsers, options, loading } = this.state
    return (
      <Dialog
        open={open}
        onClose={onClose}
        aria-labelledby="form-dialog-title"
        classes={{ paper: classes.dialogContent }}
      >
        <DialogTitle id="form-dialog-title">{strings.compartilharQuestao}</DialogTitle>
        <DialogContent className={classes.dialogContent}>
          <DialogContentText>{strings.selecioneOsUsuarios}:</DialogContentText>
          <Select
            filterOption={() => true}
            autoFocus={true}
            onInputChange={this.searchUsers}
            options={options}
            value={selectedUsers}
            onChange={selectedUsers => this.setState({ selectedUsers })}
            placeholder={strings.emailOuNome}
            isMulti
            isLoading={loading}
            loadingMessage={() => strings.carregando}
          />
        </DialogContent>
        <DialogActions>
          <MpButton onClick={onClose} variant="material">
            {strings.cancelar}
          </MpButton>
          <MpButton
            buttonProps={{ disabled: selectedUsers.length === 0, variant: 'contained', color: 'primary' }}
            onClick={this.share}
            variant="material"
          >
            {strings.compartilhar}
          </MpButton>
        </DialogActions>
      </Dialog>
    )
  }
}
