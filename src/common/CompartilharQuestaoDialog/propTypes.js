import PropTypes from 'prop-types'

export const propTypes = {
  id: PropTypes.string.isRequired,
  onClose: PropTypes.func.isRequired,
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
