import React from 'react'
import { Paper } from '@material-ui/core'
import { Search, Add, Clear, Save } from '@material-ui/icons'
import { Actions } from 'common/Actions'
import { propTypes } from './propTypes'
import { provaUtils } from 'localModules/provas/ProvaUtils'
import { enumTipoQuestao } from 'utils/enumTipoQuestao'

export class SelecaoAutomaticaBuscaComponent extends React.Component {
  static propTypes = propTypes

  filtroRef

  constructor(props) {
    super(props)
    this.state = {
      anchorEl: null,
      valorDoForm: this.valorDoFormInicial,
      expanded: false,
      disableButton: false,
    }
  }

  get _tagNames() {
    const { criterios, listaTags } = this.props
    if (criterios.tagIds) return criterios.tagIds.map(id => ({ id, nome: listaTags.find(tag => tag.id === id).nome }))
    return []
  }

  get valorDoFormInicial() {
    const { criterios } = this.props
    const valor = { quantidadeDeQuestoes: 1 }
    if (provaUtils.isProvaDinamica) valor.tipo = enumTipoQuestao.tipoQuestaoMultiplaEscolha
    if (!criterios) return valor
    if (criterios.quantidade) valor.quantidadeDeQuestoes = criterios.quantidade
    if (criterios.tipo) valor.tipo = criterios.tipo
    if (criterios.anoEscolar) valor.anoEscolar = criterios.anoEscolar
    if (criterios.dificuldade) valor.dificuldade = criterios.dificuldade
    if (criterios.tagIds) valor.tagsSelected = this._tagNames

    return valor
  }

  onFormFiltroChange = valorDoForm => {
    this.setState({ valorDoForm })
  }

  onClickInserir = () => {
    const { valorDoForm } = this.state
    this.setState({ disableButton: true }, () => {
      this.props.adicionarQuestoes(valorDoForm, res => {
        if (!res) this.setState({ disableButton: false })
      })
    })
  }

  onClickClear = async () => {
    if (this.filtroRef) {
      await this.filtroRef.limparFiltro()
    }
  }

  handleKeyPress = event => {
    if (event.key === 'Enter') {
      setTimeout(() => {
        this.onClickInserir()
      }, 350)
    }
  }

  render() {
    const { valorDoForm, disableButton } = this.state
    const { onFormFiltroChange, handleKeyPress } = this
    const { FormExpandido, FormPadrao, strings, classes, editMode = false } = this.props

    const actionsConfig = [
      {
        icone: Search,
        texto: '',
        buttonProps: {
          onClick: disableButton ? () => {} : this.onClickInserir,
          disabled: disableButton,
        },
      },
    ]
    const actionsConfigExpandido = [
      {
        icone: editMode ? Save : Add,
        texto: editMode ? strings.salvar : strings.adicionar,
        buttonProps: {
          onClick: disableButton ? () => {} : this.onClickInserir,
          disabled: disableButton,
        },
      },
    ]

    const actionsConfigClearExpandido = [
      {
        icone: Clear,
        texto: strings.cancelar,
        buttonProps: {
          onClick: this.props.onCancelar,
        },
      },
    ]
    const actionsConfigClear = [
      {
        icone: Clear,
        texto: '',
        buttonProps: {
          onClick: this.onClickClear,
        },
      },
    ]
    return (
      <div>
        <Paper className={classes.paper}>
          <div className={classes.formPadrao}>
            <Actions config={actionsConfig} className={classes.actions} />
            <span className={classes.inputTag}>
              <FormPadrao
                valorDoFormInicial={this.valorDoFormInicial}
                valorDoForm={valorDoForm}
                onFormFiltroChange={onFormFiltroChange}
                handleKeyPress={handleKeyPress}
                onRef={filtroRef => {
                  this.filtroRef = filtroRef
                }}
              />
            </span>

            <Actions config={actionsConfigClear} className={classes.actions} />
          </div>
          <div className={classes.visibleCollapse}>
            <div className={classes.formExpandido}>
              <FormExpandido
                valorDoForm={valorDoForm}
                onFormFiltroChange={onFormFiltroChange}
                handleKeyPress={handleKeyPress}
              >
                <div className={classes.botoes}>
                  <Actions config={actionsConfigExpandido} />
                  <Actions config={actionsConfigClearExpandido} />
                </div>
              </FormExpandido>
            </div>
          </div>
        </Paper>
      </div>
    )
  }
}
