import PropTypes from 'prop-types'

export const propTypes = {
  editMode: PropTypes.bool,
  onCancelar: PropTypes.func.isRequired,
  adicionarQuestoes: PropTypes.func.isRequired,
  FormPadrao: PropTypes.any,
  FormExpandido: PropTypes.any,
  montarFiltro: PropTypes.func,
  criterios: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  // style
  classes: PropTypes.object,
  // strings
  strings: PropTypes.object.isRequired,
  // redux state
  usuarioAtual: PropTypes.object.isRequired,
  // redux actions
  selectFiltro: PropTypes.func.isRequired,
  // router
  history: PropTypes.object.isRequired,
}
