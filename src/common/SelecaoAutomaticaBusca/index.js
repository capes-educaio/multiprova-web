import { SelecaoAutomaticaBuscaComponent } from './SelecaoAutomaticaBuscaComponent'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import { withStyles } from '@material-ui/core/styles'

import { mapStateToProps, mapDispatchToProps } from './redux'
import { style } from './style'

export const SelecaoAutomaticaBusca = compose(
  withStyles(style),
  withRouter,
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(SelecaoAutomaticaBuscaComponent)
