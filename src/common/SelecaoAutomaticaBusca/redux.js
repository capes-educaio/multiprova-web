import { bindActionCreators } from 'redux'
import { actions } from 'utils/actions'

export function mapStateToProps(state) {
  return {
    strings: state.strings,
    usuarioAtual: state.usuarioAtual,
    listaTags: state.listaTags,
  }
}

export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      selectFiltro: actions.select.filtro,
    },
    dispatch,
  )
}
