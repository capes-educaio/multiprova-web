import React, { Component } from 'react'

import CircularProgress from '@material-ui/core/CircularProgress'

import { propTypes } from './propTypes'

export class PaginaSecaoComponent extends Component {
  static propTypes = propTypes

  render() {
    const { classes, children, carregando } = this.props
    if (carregando)
      return (
        <div className={classes.carregando}>
          <CircularProgress />
        </div>
      )
    return <div className={classes.secao}>{children}</div>
  }
}
