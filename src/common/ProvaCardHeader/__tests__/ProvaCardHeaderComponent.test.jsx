import React from 'react'
import { shallow } from 'enzyme'
import { ptBr } from 'utils/strings/ptBr'

import { ProvaCardHeaderComponent } from '../ProvaCardHeaderComponent'

const provaComId = {
  id: '123456789',
  descricao: 'desc',
  titulo: 'titulo',
  dataUltimaAlteracao: '2018-02-01',
  grupos: [],
}

const provaSemId = {
  descricao: 'desc',
  titulo: 'titulo',
  grupos: [],
}

const defaultProps = {
  classes: {},
  strings: ptBr,
  instancia: provaComId,
  arrastavel: true,
  opcoes: [],
  history: { push: jest.fn() },
  location: { pathname: '' },
}

const defaultPropsSemId = {
  ...defaultProps,
  instancia: provaSemId,
}

describe('Testando <ProvaCardHeaderComponent />', () => {
  let wrapper
  beforeEach(() => {
    wrapper = shallow(<ProvaCardHeaderComponent {...defaultProps} />)
  })

  it('Carrega o componente', () => {
    expect(wrapper.length).toEqual(1)
  })

  const wrapperSemId = shallow(<ProvaCardHeaderComponent {...defaultPropsSemId} />)

  it('Carrega o componente', () => {
    expect(wrapperSemId.length).toEqual(1)
  })
})
