export const style = theme => ({
  cardTop: {
    display: 'flex',
    justifyContent: 'space-between',
    height: 30,
  },
  cardHeader: {
    padding: '10px 24px 10px 20px',
    display: 'flex',
    alignItems: 'center',
  },
  cardTopEsquerda: {
    display: 'flex',
    alignItems: 'center',
  },
  cardHeaderMenu: {
    marginRight: '10px',
  },
})
