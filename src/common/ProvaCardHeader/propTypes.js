import PropTypes from 'prop-types'

export const propTypes = {
  instancia: PropTypes.object.isRequired,
  status: PropTypes.object,
  strings: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired,
}
