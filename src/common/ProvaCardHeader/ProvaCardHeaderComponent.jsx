import React, { Component, Fragment } from 'react'

import Typography from '@material-ui/core/Typography'
import Tooltip from '@material-ui/core/Tooltip'
import Shuffle from '@material-ui/icons/Shuffle'
import Chip from '@material-ui/core/Chip'

import { provaUtils } from 'localModules/provas'

import { DragHandle } from 'common/DragHandle'
import { enumTipoProva } from 'utils/enumTipoProva'
import { enumTipoQuestao } from 'utils/enumTipoQuestao'

import { defaultProps } from './defaultProps'
import { propTypes } from './propTypes'

export class ProvaCardHeaderComponent extends Component {
  static defaultProps = defaultProps

  static propTypes = propTypes

  get questoesMinuscula() {
    const { strings } = this.props
    return strings.questoes.charAt(0).toLowerCase() + strings.questoes.slice(1)
  }

  quantidadeQuestoesNoGrupo(grupo) {
    const { instancia } = this.props
    return grupo.questoes.reduce((quantidade, questaoNoGrupo) => {
      const questao = instancia.questoes.find(questao => questao.id === questaoNoGrupo.questaoId)
      switch (questao ? questao.tipo : '') {
        case enumTipoQuestao.tipoQuestaoMultiplaEscolha:
          return quantidade + 1
        case enumTipoQuestao.tipoQuestaoVerdadeiroOuFalso:
          return quantidade + 1
        case enumTipoQuestao.tipoQuestaoDiscursiva:
          return quantidade + 1
        case enumTipoQuestao.tipoQuestaoAssociacaoDeColunas:
          return quantidade + 1
        case enumTipoQuestao.tipoQuestaoBloco:
          const questoes = questao.bloco ? questao.bloco.questoes : []
          return quantidade + questoes.length
        default:
          return quantidade
      }
    }, 0)
  }

  quantidadeQuestoes() {
    const { instancia } = this.props
    let quantidade = 0
    if (instancia.tipoProva === enumTipoProva.dinamica) {
      !!instancia.dinamica &&
        instancia.dinamica.forEach(din => {
          if (din.criterios.quantidade) quantidade += din.criterios.quantidade
        })
    } else {
      instancia.grupos.forEach(grupo => {
        quantidade += this.quantidadeQuestoesNoGrupo(grupo)
      })
    }

    return quantidade
  }

  get quantidadeQuestoesString() {
    const { strings, instancia } = this.props
    const quantidade = this.quantidadeQuestoes()
    let strDinamica = ''
    if (instancia.tipoProva === enumTipoProva.dinamica && quantidade === 1) strDinamica = strings.dinamicaMinuscula
    else if (instancia.tipoProva === enumTipoProva.dinamica && quantidade > 1) strDinamica = strings.dinamicas

    switch (quantidade) {
      case 0:
        return strings.naoEncontrouQuestaoNoCard
      case 1:
        return strings.umaQuestao + ' ' + strDinamica
      default:
        return quantidade + ' ' + this.questoesMinuscula + ' ' + strDinamica
    }
  }

  render() {
    const { classes, arrastavel, opcoes, strings } = this.props
    const prova = this.props.instancia
    const { status } = prova

    return (
      <div className={classes.cardTop}>
        <div className={classes.cardTopEsquerda}>
          {arrastavel && <DragHandle />}
          <Typography className={classes.cardHeader} variant="caption">
            {status ? (
              <Fragment>
                <Tooltip title={status} disableFocusListener>
                  <Chip label={provaUtils.enumIdStatusProva[status]} />
                </Tooltip>
                &nbsp;
                {prova.tipoProva === enumTipoProva.dinamica && (
                  <Tooltip title={strings.prova + ' ' + strings.dinamicaMinuscula} disableFocusListener>
                    <Shuffle color="primary" width="24px" />
                  </Tooltip>
                )}
              </Fragment>
            ) : (
              ''
            )}
          </Typography>
          {<Typography variant="caption">{this.quantidadeQuestoesString}</Typography>}
        </div>
        <div className={classes.cardHeaderMenu}>{opcoes}</div>
      </div>
    )
  }
}
