export const style = theme => ({
  allCardContent: {
    display: 'flex',
    flexDirection: 'row',
    //margin: '0 ',
    justifyContent: 'space-between',
  },
  cardTop: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  cardHeader: {
    padding: '10px 24px 10px 20px',
  },
  cardTopEsquerda: {
    display: 'flex',
    alignItems: 'center',
  },
  cardContent: {
    wordWrap: 'break-word',
    padding: '0px 20px 10px',
  },
  expand: {
    transform: 'rotate(0deg)',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
    marginLeft: 'auto',
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  actions: {
    display: 'flex',
    alignSelf: 'flex-end',
  },
  cardExpandedContent: {
    wordWrap: 'break-word',
    padding: '5px 20px 20px 20px',
    '& > *': {
      margin: '-15px 0',
    },
  },
  cardFlex: {
    display: 'flex',
    justifyContent: 'space-between',
    marginTop: '-10px',
  },
  titulo: {
    display: 'flex',
    justifyContent: 'space-between',
    '& > :last-child': {
      flexShrink: '0',
      width: 'fit-content',
    },
    margin: '10px 0',
  },
  conteudoMinimizado: {
    padding: '0 10px',
    maxHeight: '72px',
    position: 'relative',
    textOverflow: 'ellipsis',
    '&:after': {
      content: '""',
      position: 'absolute',
      top: '32px',
      right: '0',
      width: '100%',
      height: '40px',
      background: `-webkit-linear-gradient(transparent, ${theme.palette.fundo.cinza})`,
    },
    '& p': {
      margin: '10px 0 0 0',
    },
  },
  rodape: {
    margin: '0 20px 5px',
  },
  nota: {
    //margin: '15px 25px 18px 0',
    margin: '40px 25px 0px 0',
    //textAlign: 'right',
    textAlign: 'center',
    //backgroundColor: theme.palette.steelBlueContrast,
    //padding: '2px 10px 0px 10px',
    //padding: '0px 10px 0px 10px',
    boxShadow: 'none',
    //border: 'solid',
    //borderColor: theme.palette.primary.main,
    //borderWidth: '1px',
    //borderRadius: '30px',
  },
  notaTitulo: {
    color: theme.palette.cinzaDusty,
    fontSize: 10,
  },
  notaNumero: {
    fontSize: 18,
    color: theme.palette.primary.main,
  },
  chip: {
    margin: '0 10px 10px 0',
  },
})
