import React from 'react'
import { shallow } from 'enzyme'
import { ptBr } from 'utils/strings/ptBr'

import { ProvaDiscenteCardComponent } from '../ProvaDiscenteCardComponent'

const provaInstanciadaComId = {
  id: '123456789',
  descricao: 'desc',
  titulo: 'titulo',
  prova: {
    grupos: [],
  },
  virtual: {
    dataInicio: new Date(),
  },
}

const provaInstanciadaSemId = {
  descricao: 'desc',
  titulo: 'titulo',
  prova: {
    grupos: [],
  },
  virtual: {
    dataInicio: new Date(),
  },
}

const defaultProps = {
  // style
  classes: {},
  // strings
  strings: ptBr,
  instancia: provaInstanciadaComId,
  arrastavel: true,
  history: { push: jest.fn() },
}

const defaultPropsSemId = {
  // style
  classes: {},
  // strings
  strings: ptBr,
  instancia: provaInstanciadaSemId,
  arrastavel: true,
  history: { push: jest.fn() },
}

describe('Testando <ProvaDiscenteCardComponent />', () => {
  let wrapper
  beforeEach(() => {
    wrapper = shallow(<ProvaDiscenteCardComponent {...defaultProps} />)
  })

  it('Carrega o componente', () => {
    expect(wrapper.length).toEqual(1)
  })

  const wrapperSemId = shallow(<ProvaDiscenteCardComponent {...defaultPropsSemId} />)

  it('Carrega o componente', () => {
    expect(wrapperSemId.length).toEqual(1)
  })
})
