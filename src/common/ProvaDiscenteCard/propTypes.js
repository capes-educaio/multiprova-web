import PropTypes from 'prop-types'

export const propTypes = {
  instancia: PropTypes.object.isRequired,
  arrastavel: PropTypes.bool,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
