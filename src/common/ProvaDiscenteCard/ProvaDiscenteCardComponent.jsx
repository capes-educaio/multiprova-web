import { Button, Card, CardActions, Typography } from '@material-ui/core'
import { get } from 'api'
import { AlertaDeConfirmacao } from 'common/AlertaDeConfirmacao'
import { Modal } from 'common/Modal'
import { setUrlState } from 'localModules/urlState'
import React, { Component } from 'react'
import { calculaTempoRestanteDaProva } from 'utils/calculaTempoRestanteDaProva'
import { dispatchToStore } from 'utils/compositeActions'
import { enumStatusInstanciamento } from 'utils/enumStatusInstanciamento'
import { ptBr } from 'utils/strings/ptBr'
import { propTypes } from './propTypes'
import { showMessage } from 'utils/Snackbar'
import { Estatisticas } from 'common/Estatisticas'
import CardContent from '@material-ui/core/CardContent'
export class ProvaDiscenteCardComponent extends Component {
  static propTypes = propTypes

  constructor(props) {
    super(props)
    this.state = {
      alertaDeConfirmacaoAberto: false,
      botaoFinalizarProvaHabilitado: false,
      botaoIniciarProvaHabilitado: false,
      isOpenModal: false,
    }
  }
  async componentWillMount() {
    const isPossivelIniciarProva = await this.isPossivelIniciarProva()
    if (isPossivelIniciarProva && !(await this.provaIniciada())) this.setState({ botaoIniciarProvaHabilitado: true })
    else if (isPossivelIniciarProva && (await this.provaIniciada()) && !(await this.provaTerminada()))
      this.setState({ botaoIniciarProvaHabilitado: true, botaoFinalizarProvaHabilitado: true })
  }

  _getDateTimeServidor = async () => {
    return await get('/usuarios/datetime-servidor')
  }

  _abrirModal = () => this.setState({ isOpenModal: true })

  _abrirEstatisticas = () => {
    const { instancia, strings, isMobile } = this.props
    dispatchToStore('update', 'prova', instancia.prova)

    if (instancia.prova && instancia.prova.estatisticas) {
      if (isMobile) {
        setUrlState({ pathname: `/prova/${instancia.id}/estatisticas` })
      } else {
        this.setState({ isOpenModal: true })
      }
    } else {
      showMessage.error({ message: strings.estatisticasVazia })
    }
  }

  _fecharModal = () => this.setState({ isOpenModal: false })

  questoesMinuscula() {
    const { strings } = this.props
    return strings.questoes.charAt(0).toLowerCase() + strings.questoes.slice(1)
  }
  get labelNumeroDeQuestoes() {
    const { instancia } = this.props
    const provaInstanciada = instancia
    return `Prova com ${provaInstanciada.prova.numeroDeQuestoesNaProva}  ${this.questoesMinuscula()}`
  }
  get labelDuracaoDaProva() {
    const { instancia, strings } = this.props
    if (typeof instancia.virtual != 'undefined') {
      return `${instancia.virtual.duracaoDaProva} ${strings.minutos}`
    } else return ''
  }
  get labelDataInicioProva() {
    const { instancia } = this.props
    return this.dataToStringExtenso(instancia.virtual.dataInicioProva)
  }
  get labelDataTerminoProva() {
    const { instancia } = this.props
    return this.dataToStringExtenso(instancia.virtual.dataTerminoProva)
  }
  get labelTempoRestanteDaProva() {
    const { strings } = this.props
    let tempo = this.calculaTempoRestanteDeProva()
    return tempo.horas > 0
      ? `${tempo.horas} ${strings.horas} e ${tempo.minutos} ${strings.minutos}`
      : `${tempo.minutos} ${strings.minutos}`
  }
  get labelNotaProva() {
    const { instancia } = this.props
    if (instancia.notasPublicadas && instancia.notaProva !== undefined)
      return instancia.notaProva.toFixed(2).replace('.', ',')
    return false
  }
  async calculaTempoRestanteDeProva() {
    const { instancia } = this.props
    const provaInstanciada = instancia
    if (provaInstanciada.virtual.dataIniciouResolucao) {
      const { data } = await this._getDateTimeServidor()
      let dataAtual = new Date(data.datetime)
      let duracaoProva = provaInstanciada.virtual.duracaoDaProva
      let inicioProva = provaInstanciada.virtual.dataIniciouResolucao
      let tempo = calculaTempoRestanteDaProva(dataAtual, inicioProva, duracaoProva)
      if (tempo.timestamp > 0) return tempo
    }
    return 0
  }
  dataToString(dataEntrada) {
    let data = new Date(dataEntrada)
    const segundos = data.getSeconds() >= 10 ? `${data.getSeconds()}` : `0${data.getSeconds()}`
    const minutos = data.getMinutes() >= 10 ? `${data.getMinutes()}` : `0${data.getMinutes()}`
    const horas = data.getHours() >= 10 ? `${data.getHours()}` : `0${data.getHours()}`
    let dia = data.getDay() < 10 ? `0${data.getDay()}` : data.getDay()
    let mes = data.getMonth() < 10 ? `0${data.getMonth()}` : data.getMonth()
    return `${dia}/${mes}/${data.getFullYear()} (${horas}:${minutos}:${segundos})`
  }
  dataToStringExtenso(dataEntrada) {
    let data = new Date(dataEntrada)
    let dia = ['Domingo', 'Segunda-feira', 'Terça-feira', 'Quarta-feira', 'Quinta-feira', 'Sexta-feira', 'Sábado'][
      data.getDay()
    ]
    let mes = [
      'Janeiro',
      'Fevereiro',
      'Março',
      'Abril',
      'Maio',
      'Junho',
      'Julho',
      'Agosto',
      'Setembro',
      'Outubro',
      'Novembro',
      'Dezembro',
    ][data.getMonth()]

    const segundos = data.getSeconds() >= 10 ? `${data.getSeconds()}` : `0${data.getSeconds()}`
    const minutos = data.getMinutes() >= 10 ? `${data.getMinutes()}` : `0${data.getMinutes()}`
    const horas = data.getHours() >= 10 ? `${data.getHours()}` : `0${data.getHours()}`

    return `${dia}, ${data.getDate()} de ${mes} de ${data.getFullYear()} às ${horas}:${minutos}:${segundos} (horário de Brasília)`
  }
  formaJanelaDeTempoDeAplicacao() {
    const { instancia } = this.props
    const strings = ptBr
    const inicio = this.dataToString(instancia.virtual.dataInicioProva)
    const fim = this.dataToString(instancia.virtual.dataTerminoProva)
    return strings.getJanelaTempoProva({ inicio, fim })
  }
  exibirBotaoIniciarProva() {
    let tempo = this.calculaTempoRestanteDeProva()
    if (tempo.timestamp === 0) return false
    else return true
  }
  get isDepoisDaDataTermino() {
    return (async () => {
      const provaInstanciada = this.props.instancia
      const { data } = await this._getDateTimeServidor()
      let agora = new Date(data.datetime)
      if (provaInstanciada.virtual && typeof provaInstanciada.virtual.dataTerminoProva !== undefined)
        return new Date(provaInstanciada.virtual.dataTerminoProva) < agora
      else return false
    })()
  }
  async provaTerminada() {
    const provaInstanciada = this.props.instancia
    if (
      provaInstanciada.virtual &&
      (typeof provaInstanciada.virtual.dataTerminouResolucao != 'undefined' || (await this.isDepoisDaDataTermino))
    ) {
      return true
    } else return false
  }
  async provaIniciada() {
    const provaInstanciada = this.props.instancia
    if (
      provaInstanciada.virtual &&
      (typeof provaInstanciada.virtual.dataIniciouResolucao != 'undefined' || (await this.isDepoisDaDataTermino))
    ) {
      return true
    } else return false
  }
  get exibirChipTempoRestante() {
    return this.provaIniciada() && !this.provaTerminada() && this.exibirBotaoIniciarProva()
  }
  get instanciaDaProvaConcluida() {
    return this.props.instancia.status === enumStatusInstanciamento.concluida
  }
  abrirAvisoCancelar = () => this.setState({ alertaDeConfirmacaoAberto: true })
  handleCloseCancelar = confirmaCancelar => {
    if (confirmaCancelar === true) {
      this.finalizarProva()
      this.setState({
        botaoFinalizarProvaHabilitado: false,
        botaoIniciarProvaHabilitado: false,
      })
    }
    this.setState({ alertaDeConfirmacaoAberto: false })
  }
  finalizarProva = () => {
    const { instancia } = this.props
    const provaInstanciada = instancia
    get(`instanciamentos/${provaInstanciada.id}/finalizar-resolucao`).catch(console.warn)
  }

  iniciarProva = async instancia => {
    const { strings } = this.props

    const { data } = await this._getDateTimeServidor()
    let agora = new Date(data.datetime)

    let dataTerminoProva = new Date(instancia.virtual.dataTerminoProva)
    if (agora < dataTerminoProva) {
      setUrlState({ pathname: `/prova/${instancia.id}` })
      dispatchToStore('update', 'instanciaProva', instancia)
    } else {
      showMessage.error({ message: strings.foraDoPeriodoDeAplicacao })
    }
  }

  visualizarProva = instancia => {
    dispatchToStore('update', 'instanciaProva', instancia)
    dispatchToStore('update', 'instanciaProvaReadOnly', { readMode: true })
    setUrlState({ pathname: `/prova/${instancia.id}` })
  }

  isPossivelIniciarProva = async () => {
    const instanciaId = this.props.instancia.id
    let isPossivelIniciar
    await get(`instanciamentos/${instanciaId}/esta-no-intervalo-de-aplicacao`).then(response => {
      isPossivelIniciar = response.data
    })
    return isPossivelIniciar
  }

  render() {
    const { classes, instancia, strings } = this.props
    const {
      alertaDeConfirmacaoAberto,
      botaoFinalizarProvaHabilitado,
      botaoIniciarProvaHabilitado,
      isOpenModal,
    } = this.state
    const provaInstanciada = instancia
    const { notasPublicadas } = provaInstanciada
    const { vistaProvaHabilitada } = provaInstanciada.prova
    return (
      <Card key={provaInstanciada.id} tabIndex="0" role="listitem">
        <div className={classes.allCardContent}>
          <div>
            <div className={classes.cardContent}>
              <div className={classes.titulo}>
                <Typography variant="h6" color="primary" align="justify">
                  {provaInstanciada.prova.titulo}
                </Typography>
              </div>
              <Typography>{provaInstanciada.prova.descricao}</Typography>
              <Typography>
                {strings.gerenciamentoProvasDiscentes.tema} {provaInstanciada.prova.tema}
              </Typography>
              <Typography>
                {strings.gerenciamentoProvasDiscentes.professor} {provaInstanciada.prova.nomeProfessor}
              </Typography>
              <Typography>
                {strings.gerenciamentoProvasDiscentes.dataInicio} {this.labelDataInicioProva}
              </Typography>
              <Typography>
                {strings.gerenciamentoProvasDiscentes.dataTermino} {this.labelDataTerminoProva}
              </Typography>
              <Typography>
                {strings.gerenciamentoProvasDiscentes.duracao} {this.labelDuracaoDaProva}
              </Typography>
            </div>
          </div>
          <Card className={classes.nota}>
            <CardContent>
              <Typography variant="body2" className={classes.notaTitulo}>
                {strings.nota}
              </Typography>
              {this.labelNotaProva ? (
                <Typography variant="body2" className={classes.notaNumero}>
                  {this.labelNotaProva}
                </Typography>
              ) : (
                <Typography variant="body2" className={classes.notaNumero}>
                  -
                </Typography>
              )}
            </CardContent>
          </Card>
        </div>
        <CardActions>
          <Button
            disabled={!botaoIniciarProvaHabilitado}
            size="small"
            color="default"
            onClick={() => this.iniciarProva(provaInstanciada)}
          >
            {provaInstanciada.virtual && provaInstanciada.virtual.dataIniciouResolucao
              ? strings.continuarProva
              : strings.iniciarProva}
          </Button>
          <Button
            disabled={!botaoFinalizarProvaHabilitado}
            size="small"
            color="default"
            onClick={() => this.abrirAvisoCancelar()}
          >
            {strings.finalizarProva}
          </Button>
          {vistaProvaHabilitada && this.instanciaDaProvaConcluida && (
            <Button size="small" color="default" onClick={() => this.visualizarProva(provaInstanciada)}>
              {strings.vistaDeProva}
            </Button>
          )}
          {notasPublicadas && this.instanciaDaProvaConcluida && (
            <Button size="small" color="default" onClick={this._abrirEstatisticas}>
              {strings.estatisticas}
            </Button>
          )}
        </CardActions>
        <Modal open={isOpenModal} fecharModal={this._fecharModal}>
          <Estatisticas />
        </Modal>
        {alertaDeConfirmacaoAberto && (
          <AlertaDeConfirmacao
            alertaAberto={alertaDeConfirmacaoAberto}
            handleClose={this.handleCloseCancelar}
            stringNomeInstancia={strings.finalizarProva}
            descricaoSelected={strings.confirmacaoFinalizarProva}
          />
        )}
      </Card>
    )
  }
}
