export const style = theme => ({
  cardEscuro: {
    backgroundColor: theme.palette.fundo.amarelo,
    border: `2px solid ${theme.palette.primary.light}`,
  },
  expand: {
    transform: 'rotate(0deg)',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
    marginLeft: 'auto',
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  actions: {
    display: 'flex',
    alignSelf: 'flex-end',
    flexGrow: '1',
  },
  tag: {
    display: 'flex',
    paddingLeft: '20px',
    flexGrow: '1',
  },
  rodape: {
    display: 'flex',
    alignItems: 'center',
  },

  cardExpandedContent: {
    wordWrap: 'break-word',
    padding: '5px 20px 20px 20px',
  },
  cardContent: {
    textAlign: 'justify',
    display: 'flex',
    justifyContent: 'space-between',
    wordWrap: 'break-word',
    padding: '0px 20px 0px 20px',
    '& > *': {
      width: '100%',
      overflow: 'hidden',
    },
  },
  cardAltura: {
    maxHeight: '200px',
    overflow: 'hidden',
    position: 'relative',
    '&:after': {
      content: '""',
      position: 'absolute',
      zIndex: 1,
      top: '160px',
      right: '0',
      width: '100%',
      height: '40px',
      background: `-webkit-linear-gradient(transparent, ${theme.palette.gelo})`,
    },
  },
})
