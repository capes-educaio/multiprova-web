import PropTypes from 'prop-types'

export const propTypes = {
  handleExpandirEContrair: PropTypes.func.isRequired,
  questao: PropTypes.object.isRequired,
  strings: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired,
  opcoes: PropTypes.object,
}
