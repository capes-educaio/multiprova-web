import React, { Component } from 'react'
import classnames from 'classnames'

import CardContent from '@material-ui/core/CardContent'
import Collapse from '@material-ui/core/Collapse'
import IconButton from '@material-ui/core/IconButton'

import ExpandMoreIcon from '@material-ui/icons/ExpandMore'

import { Alternativa } from 'common/Alternativa'
import { MpParser } from 'common/MpParser'
import { QuestaoCardHeader } from 'common/QuestaoCard/QuestaoCardHeader'
import { DisplayTags } from 'common/DisplayTags'

import { propTypes } from './propTypes'

export class QuestaoMultiplaEscolhaCardComponent extends Component {
  static propTypes = propTypes

  render() {
    const { classes, questao, opcoes, estaExpandido, handleExpandirEContrair } = this.props

    return (
      <div>
        <QuestaoCardHeader questao={questao} opcoes={opcoes} />
        <div className={classnames(classes.cardContent, { [classes.cardAltura]: !estaExpandido })}>
          <MpParser>{questao.enunciado}</MpParser>
        </div>
        <div className={classes.rodape}>
          <div className={classes.tag}>
            <DisplayTags>{questao.tagIds}</DisplayTags>
          </div>
          <div className={classes.actions}>
            <IconButton
              size="small"
              className={classnames(classes.expand, {
                [classes.expandOpen]: estaExpandido,
              })}
              onClick={handleExpandirEContrair}
              aria-expanded={estaExpandido}
              aria-label="Show more"
            >
              <ExpandMoreIcon />
            </IconButton>
          </div>
        </div>
        <Collapse in={estaExpandido} timeout="auto" unmountOnExit>
          <CardContent className={classes.cardExpandedContent}>
            {questao.multiplaEscolha.alternativas.map(alternativa => (
              <Alternativa key={alternativa.letra} alternativa={alternativa} />
            ))}
          </CardContent>
        </Collapse>
      </div>
    )
  }
}
