import PropTypes from 'prop-types'

export const propTypes = {
  handleExpandirEContrair: PropTypes.func.isRequired,
  strings: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired,
  opcoes: PropTypes.object,
}
