import React from 'react'
import { QuestaoAssociacaoDeColunasCard } from '../index'
import Enzyme from 'enzyme'
import { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import { unwrap } from '@material-ui/core/test-utils'
import { enumTipoQuestao } from 'utils/enumTipoQuestao'

Enzyme.configure({ adapter: new Adapter() })

const usuarioAtual = {
  data: {
    id: 1,
  },
}

const questao = {
  id: '75cc4eff-6d71-4d55-821a-4ff6b8653695',
  dataCadastro: '2019-01-08T16:35:00.180Z',
  dataUltimaAlteracao: '2019-01-09T19:35:00.180Z',
  enunciado:
    'Considerando as principais operações de calculo diferencial e integral, relacione as afirmações apresentadas no lado diretio com os termos mostrados no lado esquerdo, apresentados abaixo.',
  tipo: enumTipoQuestao.tipoQuestaoAssociacaoDeColunas,
  dificuldade: 1,
  associacaoDeColunas: {
    colunaA: [
      {
        rotulo: '1',
        conteudo: 'Derivada',
      },
      {
        rotulo: '2',
        conteudo: 'Integral',
      },
      {
        rotulo: '3',
        conteudo: 'Gradiente',
      },
      {
        rotulo: '4',
        conteudo: 'Rotacional',
      },
      {
        rotulo: '5',
        conteudo: 'Laplaciano',
      },
    ],
    colunaB: [
      {
        rotulo: '5',
        conteudo: 'Divergente do gradiente de uma função',
      },
      {
        rotulo: '1',
        conteudo: 'Taxa de variação instantânea da função.',
      },
      {
        rotulo: '3',
        conteudo: 'Determinar a direção de máximo crescimento.',
      },
      {
        rotulo: '2',
        conteudo: 'Calcular a área de uma curva qualquer.',
      },
      {
        rotulo: '4',
        conteudo:
          'Calcular, em uma superfície infinitesimal, o quanto os vetores de um campo vetorial se afastam ou se aproximam de um vetor normal a esta superfície.',
      },
    ],
  },
  status: {
    id: 'el',
    texto: 'Em elaboração',
  },
}

const defaultProps = {
  handleExpandirEContrair: jest.fn(),
  // style
  classes: {},
  // strings
  strings: {
    naoEncontrouQuestao: '',
    facil: '',
    medio: '',
    dificil: '',
    editar: '',
    excluir: '',
    removerQuestao: '',
    desejaExcluirQuestao: '',
    nao: '',
    sim: '',
  },
  // redux state
  usuarioAtual,
  // redux actions
  questao,
  componentArrastavel: <div />,
  history: { push: jest.fn() },
}

const ComponentNaked = unwrap(QuestaoAssociacaoDeColunasCard)

describe('Testando <QuestaoAssociacaoDeColunasCardComponent />', () => {
  it('Carrega o componente', () => {
    const wrapper = shallow(<ComponentNaked {...defaultProps} />)
    expect(wrapper.length).toEqual(1)
    jest.clearAllMocks()
  })
})

describe('Testando <QuestaoAssociacaoDeColunasCardComponent /> handleExpandirEContrair', () => {
  it('Testa se a função handleExpandirEContrair que troca o valor da variável estaExpandido do state ', () => {
    const wrapper = shallow(<ComponentNaked {...defaultProps} />)
    const event = { stopPropagation: jest.fn() }
    wrapper.instance().props.handleExpandirEContrair(event)
    expect(wrapper.props('estaExpandido')).toBeTruthy()
    jest.clearAllMocks()
  })
})
