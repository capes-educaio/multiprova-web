import PropTypes from 'prop-types'

export const propTypes = {
  questao: PropTypes.object.isRequired,
  opcoes: PropTypes.object,
  status: PropTypes.object,

  location: PropTypes.object.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
