import React, { Component } from 'react'

import Typography from '@material-ui/core/Typography'
import Tooltip from '@material-ui/core/Tooltip'
import Public from '@material-ui/icons/Public'
import Check from '@material-ui/icons/Check'
import Clear from '@material-ui/icons/Clear'

import { PercentageCircle } from 'common/PercentageCircle'

import { Modal } from 'common/Modal'

import { getDificuldadeProps } from 'utils/getDificuldadeProps'
import { getTipoIconEString } from 'utils/getTipoIconEString'

import { defaultProps } from './defaultProps'
import { propTypes } from './propTypes'

import { GraficoPorcentAcertoQuestao } from '../GraficoPorcentAcertoQuestao'
import { enumSeloQuestao } from 'utils/enumSeloQuestao'

import { provaUtils } from 'localModules/provas/ProvaUtils'
export class QuestaoCardHeaderComponent extends Component {
  static defaultProps = defaultProps

  static propTypes = propTypes

  constructor(props) {
    super(props)
    this.state = {
      open: false,
      isOpenModal: false,
    }
  }

  _abrirModal = questao => async () => {
    if (questao.estatisticas) {
      const usos = questao.estatisticas.usos
      if (usos) {
        for (let uso of usos) {
          var response = await provaUtils.findById(uso.idProva)
          uso.prova = response.data
        }
      }
      this.setState({ isOpenModal: true })
    }
  }

  _fecharModal = () => this.setState({ isOpenModal: false })

  handleTooltipClose = () => {
    this.setState({ open: false })
  }

  handleTooltipOpen = () => {
    this.setState({ open: true })
  }

  render() {
    const { questao, classes, opcoes, strings, subheader } = this.props
    let { isOpenModal } = this.state

    const percent = 100
    const { estatisticas, selo } = questao
    const resumo = estatisticas ? estatisticas.resumo : null
    const acertos = resumo ? Math.round(percent * resumo.acertos) : 0
    const numeroUsos = resumo ? resumo.numeroUsos : 0

    const { dificuldadeString, imagem } = getDificuldadeProps(questao.dificuldade, strings)
    const { tipoString, TipoIcon } = getTipoIconEString(questao.tipo, strings)

    return (
      <div className={classes.cardTop}>
        <div className={classes.cardTopEsquerda}>
          <div
            className={classes.porcentagem}
            onMouseOver={this.handleTooltipOpen}
            onMouseLeave={this.handleTooltipClose}
          >
            <Tooltip
              title={strings.getNumAplicacoes(numeroUsos)}
              open={this.state.open}
              onClose={this.handleTooltipClose}
            >
              <PercentageCircle percent={acertos} onClick={this._abrirModal(questao)} />
            </Tooltip>
          </div>

          {subheader && (
            <Typography className={classes.cardHeader} variant="caption">
              {subheader}
            </Typography>
          )}
          {tipoString && (
            <div className={classes.tipoQuestao}>
              <Tooltip title={tipoString}>
                <TipoIcon color="primary" width="35px" fontSize="large" />
              </Tooltip>
            </div>
          )}
          {questao.dificuldade && (
            <div className={classes.linha}>
              <Tooltip title={dificuldadeString}>
                <img src={imagem} alt="" />
              </Tooltip>
            </div>
          )}
          {questao.publico && (
            <div className={classes.linha}>
              <Tooltip title={strings.questaoPublica}>
                <Public color="primary" />
              </Tooltip>
            </div>
          )}
          {selo && selo === enumSeloQuestao.validada ? (
            <div className={classes.validada}>
              <Tooltip title={strings.validada}>
                <Check />
              </Tooltip>
            </div>
          ) : (
            <div className={classes.naoValidada}>
              <Tooltip title={strings.naoValidada}>
                <Clear />
              </Tooltip>
            </div>
          )}
        </div>
        <div id="opcoes-card-questao">{opcoes}</div>
        <Modal open={isOpenModal} fecharModal={this._fecharModal}>
          <GraficoPorcentAcertoQuestao questao={questao} />
        </Modal>
      </div>
    )
  }
}
