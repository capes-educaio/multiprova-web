const bolaStyle = theme => ({
  display: 'flex',
  backgroundColor: theme.palette.gelo,
  border: '2px solid #bdbdbd',
  borderRadius: '50%',
  width: '8px',
  height: '8px',
  position: 'relative',
  top: '-11px',
})

export const style = theme => ({
  cardTop: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  cardHeader: {
    padding: 10,
  },
  cardTopEsquerda: {
    display: 'flex',
    alignItems: 'center',
  },
  porcentagem: {
    paddingLeft: '15px',
  },
  validada: {
    margin: 3,
    color: theme.palette.success.dark,
  },
  naoValidada: {
    margin: 3,
    color: theme.palette.warning.dark,
  },
  slider: {
    width: '30px',
    margin: '0 10px 0 0',
    height: '10px',
    '& :hover': {
      cursor: 'pointer',
    },
  },
  bolaFacil: {
    ...bolaStyle(theme),
    right: '0px',
  },
  bolaMedio: {
    ...bolaStyle(theme),
    left: '9px',
  },
  bolaDificil: {
    ...bolaStyle(theme),
    right: '-18px',
  },
  linha: {
    margin: 4,
  },
  tipoQuestao: {
    margin: '0 6px',
  },
})
