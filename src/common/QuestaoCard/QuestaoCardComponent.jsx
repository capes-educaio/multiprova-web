import React, { Component } from 'react'
import Card from '@material-ui/core/Card'
import { QuestaoMultiplaEscolhaCard } from './QuestaoMultiplaEscolhaCard'
import { QuestaoAssociacaoDeColunasCard } from './QuestaoAssociacaoDeColunasCard'
import { QuestaoBlocoCard } from './QuestaoBlocoCard'
import { QuestaoDiscursivaCard } from './QuestaoDiscursivaCard'
import { QuestaoRedacaoCard } from './QuestaoRedacaoCard'
import { QuestaoVOuFCard } from './QuestaoVOuFCard'

import { propTypes } from './propTypes'
export class QuestaoCardComponent extends Component {
  static defaultProps = {
    estaInicialmenteExpandido: false,
  }

  static propTypes = propTypes

  constructor(props) {
    super(props)
    this.state = {
      estaExpandido: props.estaInicialmenteExpandido,
      dificuldadeTextoVisivel: false,
    }
  }

  componentDidMount = () => {
    const { onRef } = this.props
    if (onRef) onRef(this)
  }

  componentWillUnmount = () => {
    const { onRef } = this.props
    if (onRef) onRef(null)
  }

  expandir = event => {
    if (event) event.stopPropagation()
    this.setState({ estaExpandido: true })
  }

  contrair = event => {
    if (event) event.stopPropagation()
    this.setState({ estaExpandido: false })
  }

  handleExpandirEContrair = event => {
    if (event) {
      event.stopPropagation()
    }
    const { estaExpandido } = this.state
    this.setState({ estaExpandido: !estaExpandido })
  }

  resetQuestaoId = () => {
    const { updateQuestaoId } = this.props
    updateQuestaoId(null)
  }

  render() {
    const { classes, questao, questaoId, opcoes, id } = this.props
    const { estaExpandido } = this.state
    const wasQuestaoNotificada = questaoId && questaoId === questao.id

    const mapa = {
      multiplaEscolha: (
        <QuestaoMultiplaEscolhaCard
          classes={classes}
          questao={questao}
          estaExpandido={estaExpandido}
          opcoes={opcoes}
          handleExpandirEContrair={this.handleExpandirEContrair}
        />
      ),
      bloco: (
        <QuestaoBlocoCard
          classes={classes}
          questao={questao}
          estaExpandido={estaExpandido}
          opcoes={opcoes}
          handleExpandirEContrair={this.handleExpandirEContrair}
        />
      ),
      vouf: (
        <QuestaoVOuFCard
          classes={classes}
          questao={questao}
          estaExpandido={estaExpandido}
          opcoes={opcoes}
          handleExpandirEContrair={this.handleExpandirEContrair}
        />
      ),
      discursiva: (
        <QuestaoDiscursivaCard
          classes={classes}
          questao={questao}
          estaExpandido={estaExpandido}
          opcoes={opcoes}
          handleExpandirEContrair={this.handleExpandirEContrair}
        />
      ),
      associacaoDeColunas: (
        <QuestaoAssociacaoDeColunasCard
          classes={classes}
          questao={questao}
          estaExpandido={estaExpandido}
          opcoes={opcoes}
          handleExpandirEContrair={this.handleExpandirEContrair}
        />
      ),
      redacao: (
        <QuestaoRedacaoCard
          classes={classes}
          questao={questao}
          estaExpandido={estaExpandido}
          opcoes={opcoes}
          handleExpandirEContrair={this.handleExpandirEContrair}
        />
      ),
    }
    return (
      <div>
        <Card id={id} className={wasQuestaoNotificada ? classes.cardEscuro : ''} onClick={this.resetQuestaoId}>
          {questao && mapa[questao.tipo]}
        </Card>
      </div>
    )
  }
}
