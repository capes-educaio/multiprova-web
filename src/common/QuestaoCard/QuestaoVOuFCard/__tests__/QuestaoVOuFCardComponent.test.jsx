import React from 'react'
import { QuestaoVOuFCard } from '../index'
import Enzyme from 'enzyme'
import { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import { unwrap } from '@material-ui/core/test-utils'
import { enumTipoQuestao } from 'utils/enumTipoQuestao'

Enzyme.configure({ adapter: new Adapter() })

const usuarioAtual = {
  data: {
    id: 1,
  },
}

const questao = {
  id: '75cc4eff-6d71-4d55-821a-4ff6b8653695',
  dataCadastro: '2018-07-25T14:48:55.348Z',
  dataUltimaAlteracao: '2018-07-25T14:48:55.348Z',
  enunciado: '<p>Qual a metade de 2+2?</p>',
  dificuldade: 2,
  tipo: enumTipoQuestao.tipoQuestaoVerdadeiroOuFalso,
  vouf: {
    afirmacoes: [
      {
        texto: 'A soma vale 4',
        letra: 'V',
      },
      {
        texto: 'A soma vale 5',
        letra: 'F',
      },
      {
        texto: 'A soma vale 6',
        letra: 'F',
      },
    ],
  },
}

const defaultProps = {
  handleExpandirEContrair: jest.fn(),
  // style
  classes: {},
  // strings
  strings: {
    naoEncontrouQuestao: '',
    facil: '',
    medio: '',
    dificil: '',
    editar: '',
    excluir: '',
    removerQuestao: '',
    desejaExcluirQuestao: '',
    nao: '',
    sim: '',
  },
  // redux state
  usuarioAtual,
  // redux actions
  questao,
  enunciado: questao.enunciado,
  componentArrastavel: <div />,
  history: { push: jest.fn() },
}

const ComponentNaked = unwrap(QuestaoVOuFCard)

describe('Testando <QuestaoVOuFCard />', () => {
  it('Carrega o componente', () => {
    const wrapper = shallow(<ComponentNaked {...defaultProps} />)
    expect(wrapper.length).toEqual(1)
    jest.clearAllMocks()
  })
})

describe('Testando <QuestaoVOuFCard /> handleExpandirEContrair', () => {
  it('Testa se a função handleExpandirEContrair que troca o valor da variável estaExpandido do state ', () => {
    const wrapper = shallow(<ComponentNaked {...defaultProps} />)
    const event = { stopPropagation: jest.fn() }
    wrapper.instance().props.handleExpandirEContrair(event)
    expect(wrapper.props('estaExpandido')).toBeTruthy()
    jest.clearAllMocks()
  })
})
