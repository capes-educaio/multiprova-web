import { QuestaoCardComponent } from './QuestaoCardComponent'
import { withStyles } from '@material-ui/core/styles'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { mapStateToProps, mapDispatchToProps } from './redux'
import { style } from './style'

export const QuestaoCard = compose(
  withStyles(style),
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(QuestaoCardComponent)
