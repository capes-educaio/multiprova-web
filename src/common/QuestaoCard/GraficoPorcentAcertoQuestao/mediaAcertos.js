import { theme } from 'utils/theme.js'
import { isEmpty } from 'utils/isEmpty'
import { arredondar } from 'utils/arredondar.js'

const percent = 100

const getUltimasAplicacoes = usos => {
  if (!usos) return []
  let _usos = []
  const ultimas10Aplicacoes = []

  if (usos.length > 10) _usos = usos.slice(0, 10)
  else _usos = usos

  for (let uso of _usos) {
    ultimas10Aplicacoes.push(arredondar(uso.acertos * percent, 2))
  }

  return ultimas10Aplicacoes
}

// const retaConstante = (valor, eixoX) => {
//   const reta = []
//   for (var i = 0; i < eixoX; i++) {
//     reta.push(valor)
//   }
//   return reta
// }

const dataAxisX = usos => {
  const data = ['', '', '', '', '', '', '', '', '', '']
  let index = 0
  while (index < 10 && index < usos.length) {
    const uso = usos[index]
    data[index] = uso.prova.titulo
    index++
  }
  return data
}

const mediaAcertos = {
  getOption: ({ questao, strings }) => {
    const { estatisticas } = questao
    const ultimas10Aplicacoes = !isEmpty(estatisticas) ? getUltimasAplicacoes(estatisticas.usos) : []
    // const resumo = !isEmpty(estatisticas) ? estatisticas.resumo : {}
    // const acertos10 = !isEmpty(resumo) ? arredondar(resumo.acertos10 * percent, 2) : 0

    const option = {
      title: {
        text: strings.mediaAcertosTitulo,
        left: 'center',
      },
      toolbox: {
        feature: {
          saveAsImage: { show: true, title: strings.salvarComoImagem },
        },
        left: '35px',
        type: 'png',
      },
      legend: {
        show: true,
        data: [strings.mediaAcertosUmaAvaliacao, strings.mediaAcertoUltimas10],
        bottom: '2%',
        orient: 'horizontal',
        borderWidth: 1,
        borderColor: theme.palette.grafico.grey.main,
        borderRadius: 5,
        padding: 10,
      },
      grid: {
        bottom: '25%',
      },
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'cross',
        },
      },
      xAxis: {
        data: estatisticas !== undefined ? dataAxisX(estatisticas.usos) : [],
        splitNumber: 10,
        axisLabel: {
          show: false,
        },
        splitLine: {
          show: false,
        },
      },
      yAxis: {
        type: 'value',
        scale: true,
        max: 100,
        min: 0,
        splitArea: {
          show: false,
        },
        name: strings.mediaAcertosEixoY,
        nameTextStyle: {
          padding: 20,
        },
        nameLocation: 'center',
      },
      series: [
        {
          name: strings.mediaAcertosUmaAvaliacao,
          type: 'scatter',
          itemStyle: {
            color: theme.palette.secondary.dark,
          },
          label: {
            emphasis: {
              show: true,
              position: 'left',
              textStyle: {
                color: theme.palette.secondary.contrastText,
                fontSize: 14,
              },
            },
          },
          data: ultimas10Aplicacoes,
        },
      ],
    }
    return option
  },
}
export default mediaAcertos
