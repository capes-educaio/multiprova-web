import React from 'react'

import { propTypes } from './propTypes'
import ReactEcharts from 'echarts-for-react'
import mediaAcertos from './mediaAcertos'

import { Paper } from '@material-ui/core'

export const GraficoPorcentAcertoQuestaoComponent = ({ questao, fecharModal, classes, strings }) => {
  return (
    <Paper className={classes.grafico} elevation={0}>
      <ReactEcharts
        option={mediaAcertos.getOption({ questao, strings })}
        className={classes.chart}
        notMerge
        lazyUpdate
      />
    </Paper>
  )
}

GraficoPorcentAcertoQuestaoComponent.propTypes = propTypes
