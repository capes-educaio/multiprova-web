export const style = theme => ({
  grafico: {
    paddingTop: theme.spacing.unit,
    paddingBottom: theme.spacing.unit * 2,
  },

  chart: { width: '600px', paddingLeft: '0px', paddingRight: '0px' },
})
