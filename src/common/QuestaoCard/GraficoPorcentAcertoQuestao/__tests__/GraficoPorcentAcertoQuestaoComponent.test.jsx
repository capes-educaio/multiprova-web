import Enzyme from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import { ptBr } from 'utils/strings/ptBr'

import { GraficoPorcentAcertoQuestaoComponent } from '../GraficoPorcentAcertoQuestaoComponent'
import { mountTest } from 'localModules/testUtils/mountTest'

Enzyme.configure({ adapter: new Adapter() })

const requiredProps = {
  questao: {},
  // style
  classes: {},
  // strings
  strings: ptBr,
}

const arrayOfProps = [requiredProps]

mountTest(GraficoPorcentAcertoQuestaoComponent, 'GraficoPorcentAcertoQuestaoComponent')(arrayOfProps)
