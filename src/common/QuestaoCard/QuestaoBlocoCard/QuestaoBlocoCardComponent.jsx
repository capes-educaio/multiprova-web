import React, { Component } from 'react'
import classnames from 'classnames'
import SwipeableViews from 'react-swipeable-views'

import CardContent from '@material-ui/core/CardContent'
import Collapse from '@material-ui/core/Collapse'
import IconButton from '@material-ui/core/IconButton'
import MobileStepper from '@material-ui/core/MobileStepper'
import Button from '@material-ui/core/Button'

import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft'
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight'

import { Alternativa } from 'common/Alternativa'
import { Afirmacao } from 'common/Afirmacao'
import { ExpectativaDeResposta } from 'common/ExpectativaDeResposta'
import { ItemAssociacao } from 'common/ItemAssociacao'
import { MpParser } from 'common/MpParser'
import { QuestaoCardHeader } from 'common/QuestaoCard/QuestaoCardHeader'
import { DisplayTags } from 'common/DisplayTags'

import { enumTipoQuestao } from 'utils/enumTipoQuestao'

import { setActiveStepInicialByFiltro } from './setActiveStepInicial'
import { propTypes } from './propTypes'

export class QuestaoBlocoCardComponent extends Component {
  static propTypes = propTypes

  constructor(props) {
    super(props)
    this.state = {
      activeStep: setActiveStepInicialByFiltro(props),
    }
  }

  hasSubquestoes = () => {
    const { questao } = this.props
    const { bloco } = questao
    const subquestoes = bloco ? bloco.questoes : null
    if (!subquestoes) return false
    if (subquestoes.length < 1) return false
    return true
  }

  handleNext = () => {
    if (this.hasSubquestoes()) {
      this.setState(prevState => ({
        activeStep: prevState.activeStep + 1,
      }))
    }
  }

  handleBack = () => {
    if (this.hasSubquestoes()) {
      this.setState(prevState => ({
        activeStep: prevState.activeStep - 1,
      }))
    }
  }

  handleStepChange = activeStep => {
    this.setState({ activeStep })
  }

  getHeaderDeCadaSubQuestao = subquestoes => {
    const { opcoes, questao } = this.props
    return subquestoes.map((subquestao, index) => {
      return (
        <QuestaoCardHeader
          key={index}
          questao={{ ...questao, ...{ dificuldade: subquestao.dificuldade } }}
          opcoes={opcoes}
        />
      )
    })
  }

  getHeaderDaQuestaoBloco = () => {
    const { opcoes, questao } = this.props
    const { status } = questao
    return <QuestaoCardHeader questao={questao} opcoes={opcoes} status={status} />
  }

  getTagNomesDeCadaSubQuestao = subquestoes => {
    return subquestoes.map((subquestao, index) => <DisplayTags key={index}>{subquestao.tagIds}</DisplayTags>)
  }

  render() {
    const { classes, questao, theme, handleExpandirEContrair, estaExpandido } = this.props
    const { activeStep } = this.state
    const { bloco } = questao
    let subquestoes = bloco ? bloco.questoes : []
    const maxSteps = subquestoes.length
    let headerSubquestao = []
    let tagSubquestao = []
    if (this.hasSubquestoes()) {
      headerSubquestao = this.getHeaderDeCadaSubQuestao(subquestoes)
      tagSubquestao = this.getTagNomesDeCadaSubQuestao(subquestoes)
    } else {
      headerSubquestao.push(this.getHeaderDaQuestaoBloco())
    }

    return (
      <div>
        {headerSubquestao[this.state.activeStep]}

        <SwipeableViews
          axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
          index={this.state.activeStep}
          onChangeIndex={this.handleStepChange}
          enableMouseEvents
        >
          {subquestoes.map((subquestao, index) => (
            <div className={classnames(classes.cardContent, { [classes.cardAltura]: !estaExpandido })} key={index}>
              <MpParser key={index}>{subquestao.enunciado}</MpParser>
            </div>
          ))}
        </SwipeableViews>
        <div className={classes.rodape}>
          <div className={classes.tag}>{tagSubquestao[this.state.activeStep]}</div>
          <div className={classes.actions}>
            <IconButton
              size="small"
              className={classnames(classes.expand, {
                [classes.expandOpen]: estaExpandido,
              })}
              onClick={handleExpandirEContrair}
              aria-expanded={estaExpandido}
              aria-label="Show more"
            >
              <ExpandMoreIcon />
            </IconButton>
          </div>
        </div>

        <Collapse in={estaExpandido} timeout="auto" unmountOnExit>
          <SwipeableViews
            axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
            index={this.state.activeStep}
            onChangeIndex={this.handleStepChange}
            enableMouseEvents
          >
            {subquestoes.map((subquestao, index) => (
              <CardContent key={index} className={classes.cardExpandedContent}>
                {subquestao.tipo === enumTipoQuestao.tipoQuestaoMultiplaEscolha &&
                  subquestao.multiplaEscolha.alternativas.map((alternativa, idx) => (
                    <Alternativa key={idx} alternativa={alternativa} />
                  ))}
                {subquestao.tipo === enumTipoQuestao.tipoQuestaoVerdadeiroOuFalso &&
                  subquestao.vouf.afirmacoes.map((afirmacao, idx) => <Afirmacao key={idx} afirmacao={afirmacao} />)}
                {subquestao.tipo === enumTipoQuestao.tipoQuestaoDiscursiva && (
                  <ExpectativaDeResposta expectativaDeResposta={subquestao.discursiva.expectativaDeResposta} />
                )}
                {subquestao.tipo === enumTipoQuestao.tipoQuestaoAssociacaoDeColunas && (
                  <div>
                    {subquestao.associacaoDeColunas.colunaA.map((item, idx) => (
                      <ItemAssociacao key={idx} item={item} />
                    ))}
                    <p />
                    {subquestao.associacaoDeColunas.colunaB.map((item, idx) => (
                      <ItemAssociacao key={idx} item={item} />
                    ))}
                  </div>
                )}
              </CardContent>
            ))}
          </SwipeableViews>
        </Collapse>
        <MobileStepper
          steps={maxSteps}
          position="static"
          activeStep={activeStep}
          nextButton={
            <Button size="small" onClick={this.handleNext} disabled={activeStep === maxSteps - 1}>
              {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
            </Button>
          }
          backButton={
            <Button size="small" onClick={this.handleBack} disabled={activeStep === 0}>
              {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
            </Button>
          }
        />
      </div>
    )
  }
}
