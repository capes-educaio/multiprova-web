import PropTypes from 'prop-types'

export const propTypes = {
  questao: PropTypes.object.isRequired,
  opcoes: PropTypes.object,
  // redux state
  filtro: PropTypes.object,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
