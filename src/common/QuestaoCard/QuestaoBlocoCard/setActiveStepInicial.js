import { enumTipoQuestao } from 'utils/enumTipoQuestao'

const filtradoPorTag = (subquestao, filtro) => {
  const { tagsSelected } = filtro
  const { tagIds } = subquestao
  if (tagsSelected && tagIds) {
    for (let i = 0; i < tagsSelected.length; i++) {
      const encontrouTagNaSubquestao = Boolean(tagIds.find(id => id === tagsSelected[i].id))
      if (encontrouTagNaSubquestao) return true
    }
  }
  return false
}

const filtradoPorDificuldade = (subquestao, filtro) => {
  if (subquestao.dificuldade === filtro.dificuldade) return true
  return false
}

const filtradoPorAnoEscolar = (subquestao, filtro) => {
  if (subquestao.anoEscolar === filtro.anoEscolar) return true
  return false
}

const filtradoPorTextoNoEnunciado = (subquestao, filtro) => {
  if (subquestao.enunciado.search(filtro.textoFiltro) > 0) return true
  return false
}

const filtradoPorTextoNasAlternativas = (subquestao, filtro) => {
  if (subquestao.tipo === enumTipoQuestao.tipoQuestaoMultiplaEscolha && filtro.textoFiltro !== '')
    return subquestao.multiplaEscolha.alternativas.some(alternativa =>
      alternativa.texto ? alternativa.texto.includes(filtro.textoFiltro) : false,
    )
  return false
}

const filtradoPorTextoNasAfirmacoes = (subquestao, filtro) => {
  if (subquestao.tipo === enumTipoQuestao.tipoQuestaoVerdadeiroOuFalso)
    return subquestao.vouf.afirmacoes.some(afirmacao => afirmacao.texto.includes(filtro.textoFiltro))
  return false
}

const filtradoPorTextoNaDiscursiva = (subquestao, filtro) => {
  if (subquestao.tipo === enumTipoQuestao.tipoQuestaoDiscursiva)
    return subquestao.discursiva.expectativaDeResposta.includes(filtro.textoFiltro)
  return false
}

const getSubquestaoFiltradaIndex = (subquestoes, filtro) => {
  let novoActiveStep = null
  for (let index = 0; index < subquestoes.length; index++) {
    if (filtradoPorTag(subquestoes[index], filtro)) {
      return index
    }
    if (novoActiveStep === null) {
      if (filtradoPorDificuldade(subquestoes[index], filtro)) {
        novoActiveStep = index
      }
      if (filtradoPorTextoNoEnunciado(subquestoes[index], filtro)) {
        novoActiveStep = index
      }
      if (filtradoPorTextoNasAlternativas(subquestoes[index], filtro)) {
        novoActiveStep = index
      }
      if (filtradoPorTextoNasAfirmacoes(subquestoes[index], filtro)) {
        novoActiveStep = index
      }
      if (filtradoPorTextoNaDiscursiva(subquestoes[index], filtro)) {
        novoActiveStep = index
      }
      if (filtradoPorAnoEscolar(subquestoes[index], filtro)) {
        novoActiveStep = index
      }
    }
  }
  return novoActiveStep
}

export const setActiveStepInicialByFiltro = props => {
  const { filtro, questao } = props
  const questaoBloco = questao ? questao.bloco : []
  const subquestoes = questaoBloco ? questaoBloco.questoes : []
  let novoActiveStep = null
  if (subquestoes && filtro) {
    novoActiveStep = getSubquestaoFiltradaIndex(subquestoes, filtro)
  }
  novoActiveStep = novoActiveStep ? novoActiveStep : 0
  return novoActiveStep
}
