import PropTypes from 'prop-types'

export const propTypes = {
  questao: PropTypes.object.isRequired,
  strings: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired,
  opcoes: PropTypes.object,
}
