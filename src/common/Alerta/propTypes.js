import PropTypes from 'prop-types'

export const propTypes = {
  aberto: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  descricao: PropTypes.string,
  titulo: PropTypes.string,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
