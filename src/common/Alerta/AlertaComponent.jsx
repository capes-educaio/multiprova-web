import React, { Component } from 'react'

import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import { propTypes } from './propTypes'

export class AlertaComponent extends Component {
  static propTypes = propTypes

  render() {
    const { classes, aberto, handleClose, descricao, titulo, strings } = this.props
    return (
      <Dialog
        role="dialog"
        aria-labelledby="dialogTitleConfimation"
        aria-describedby="dialogContentConfimation"
        disableBackdropClick
        disableEscapeKeyDown
        open={aberto}
        onClose={handleClose}
        classes={{ root: classes.secao }}
      >
        <DialogTitle id="dialogTitleConfimation">{titulo}</DialogTitle>
        {!!descricao && (
          <DialogContent>
            <DialogContentText id="dialogContentConfimation">{descricao}</DialogContentText>
          </DialogContent>
        )}
        <DialogActions>
          <Button tabIndex="1" id="ok" onClick={() => handleClose(true)}>
            {strings.ok}
          </Button>
        </DialogActions>
      </Dialog>
    )
  }
}
