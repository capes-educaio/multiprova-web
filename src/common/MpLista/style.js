import { fade } from '@material-ui/core/styles/colorManipulator'

export const style = theme => ({
  item: {
    backgroundColor: theme.palette.defaultBackground || theme.palette.gelo,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: '2px 6px',
    marginTop: '5px',
  },
  content: {
    display: 'flex',
    alignItems: 'center',
  },
  actions: {
    fill: theme.palette.cinzaEscuro,
    marginRight: 10,
    fontSize: 15,
    borderRadius: '5px',
    '& svg': {
      fill: theme.palette.cinzaDusty,
    },
  },
  root: {
    '& > *': { marginTop: 5 },
    padding: '10px',
    flexDirection: 'column',
    display: 'flex',
    backgroundColor: fade(theme.palette.common.black, 0.05),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.black, 0.1),
    },
  },
  rootDisabled: {
    '& > *': { marginTop: 5 },
    padding: '10px',
    flexDirection: 'column',
    display: 'flex',
    backgroundColor: fade(theme.palette.common.black, 0.05),
  },
})
