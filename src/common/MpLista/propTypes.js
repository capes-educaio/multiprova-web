import PropTypes from 'prop-types'

export const propTypes = {
  // lista de itens a ser listados
  lista: PropTypes.array.isRequired,
  // lista de botoes que ficarao na direita de cada elemento da lista. Cada botao deve vir com um onClick que recebe (item,index)
  actions: PropTypes.array,
  // icone de cada item
  icon: PropTypes.object,
  // ao usar checkboxes, deve especificar handleChangeCheckbox(event, newCheckedValue, conteudo, index)
  checkboxesEnabled: PropTypes.bool,
  checkboxes: PropTypes.arrayOf(PropTypes.bool),
  handleChangeCheckbox: PropTypes.func,
  disabled: PropTypes.bool,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
