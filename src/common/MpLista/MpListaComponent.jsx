import { Checkbox, Typography } from '@material-ui/core'
import { MpIconButton } from 'common/MpIconButton'
import React, { Component } from 'react'
import { propTypes } from './propTypes'

export class MpListaComponent extends Component {
  static propTypes = propTypes

  componentDidMount = () => {
    if (this.props.checkboxEnabled && !Array.isArray(this.props.checkboxes)) {
      console.warn('chackbox habilitado na lista, mas nao foi passado array de checkboxes')
    }
  }

  render() {
    const {
      classes,
      checkboxEnabled,
      checkboxes = [],
      handleChangeCheckbox,
      actions,
      lista,
      disabled,
      icon,
    } = this.props
    return (
      <div className={disabled ? classes.rootDisabled : classes.root}>
        {lista.map((item, index) => (
          <div key={index} className={classes.item}>
            <div className={classes.content}>
              {checkboxEnabled && (
                <Checkbox
                  checked={checkboxes[index]}
                  onChange={(event, newCheckedValue) => handleChangeCheckbox(event, newCheckedValue, item, index)}
                  value={`checkbox${index}`}
                  inputProps={{
                    'aria-label': 'primary checkbox',
                  }}
                />
              )}
              {!!icon && icon}
              <Typography>{item}</Typography>
            </div>
            <div className={classes.actions}>
              {actions &&
                actions.map((button, bIndex) => {
                  const buttonProps = button.props
                  return (
                    <MpIconButton
                      key={'button' + bIndex}
                      buttonProps={{ size: 'small' }}
                      className={classes.actions}
                      {...buttonProps}
                      onClick={() => {
                        if (button.props.onClick) button.props.onClick(item, index)
                      }}
                    />
                  )
                })}
            </div>
          </div>
        ))}
      </div>
    )
  }
}
