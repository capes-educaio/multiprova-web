import React, { Component } from 'react'
import classnames from 'classnames'

import Typography from '@material-ui/core/Typography'

import { MpParser } from 'common/MpParser'

import { propTypes } from './propTypes'
import { defaultProps } from './defaultProps'

export class ItemAssociacaoComponent extends Component {
  static propTypes = propTypes
  static defaultProps = defaultProps

  render() {
    const { item, mostrarResposta, classes } = this.props
    return (
      <div className={classes.item}>
        <Typography className={classes.rotulo}>
          {'('}
          <span className={classnames({ [classes.espaco]: !mostrarResposta })}>
            {mostrarResposta && item.rotulo}
          </span>
          {')'}
        </Typography>
        <MpParser>{item.conteudo}</MpParser>
      </div>
    )
  }
}
