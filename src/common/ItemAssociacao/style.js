const widthLetra = '25px'

export const style = theme => ({
  item: {
    display: 'flex',
    alignItems: 'flex-start',
  },
  rotulo: {
    minWidth: widthLetra,
  },
  espaco: {
    marginLeft: '10px',
  },
})
