import PropTypes from 'prop-types'

export const propTypes = {
  item: PropTypes.object.isRequired,
  mostrarResposta: PropTypes.bool,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
