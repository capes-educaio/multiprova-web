import React, { Component } from 'react'

import Typography from '@material-ui/core/Typography'

import { propTypes } from './propTypes'

export class TituloDaPaginaComponent extends Component {
  static propTypes = propTypes

  render() {
    const { classes, children } = this.props
    return (
      <div className={classes.secao} role="heading" aria-level="1">
        <Typography variant="h6" className={classes.titulo} color="primary">
          {children}
        </Typography>
      </div>
    )
  }
}
