export const style = theme => ({
  secao: {
    padding: '20px 20px 20px 20px',
  },
  titulo: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    [theme.breakpoints.down('xs')]: {},
  },
})
