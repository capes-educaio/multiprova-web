import React, { Component } from 'react'
import IconButton from '@material-ui/core/IconButton'
import Menu from '@material-ui/core/Menu'

import MoreVertIcon from '@material-ui/icons/MoreVert'

export class MenuOpcoes extends Component {
  state = {
    anchorEl: null,
  }

  onClickIcon = event => {
    this.setState({ anchorEl: event.currentTarget })
  }

  fecharMenu = () => {
    this.setState({ anchorEl: null })
  }

  render() {
    const { anchorEl } = this.state
    return (
      <div>
        <IconButton onClick={this.onClickIcon}>
          <MoreVertIcon />
        </IconButton>
        <Menu
          anchorEl={anchorEl}
          getContentAnchorEl={null}
          anchorOrigin={{ vertical: 'center', horizontal: 'center' }}
          transformOrigin={{ vertical: 'top', horizontal: 'right' }}
          open={Boolean(anchorEl)}
          onClose={this.fecharMenu}
          MenuListProps={{
            onClick: this.fecharMenu,
          }}
          children={this.props.children}
        />
      </div>
    )
  }
}
