import { FiltragemComponent } from './FiltragemComponent'
import { compose } from 'redux'
import { withStyles } from '@material-ui/core/styles'

import { style } from './style'

export const Filtragem = compose(withStyles(style))(FiltragemComponent)
