import React from 'react'
import { withContext } from 'utils/context'

export const FiltragemContext = React.createContext()
export const withFiltragemContext = withContext(FiltragemContext, 'filtragemContext')
