import React, { Component, Fragment } from 'react'

import Paper from '@material-ui/core/Paper'
import Collapse from '@material-ui/core/Collapse'
import Divider from '@material-ui/core/Divider'

import IconButton from '@material-ui/core/IconButton'
import Search from '@material-ui/icons/Search'
import Clear from '@material-ui/icons/Clear'

import { ExpandMoreIcon } from 'localModules/icons/ExpandMoreIcon'

import { SimpleForm } from 'localModules/simpleForm'
import { SimpleFormContext } from 'localModules/simpleForm/SimpleForm/context'

import { MpButton } from 'common/MpButton'

import { propTypes } from './propTypes'

export class FiltroComponent extends Component {
  static propTypes = propTypes

  _toggleExpandir = () => {
    const { filtragemContext, onToggleExpandir } = this.props
    filtragemContext.updateContext({ estaExpandida: !filtragemContext.estaExpandida })
    if (onToggleExpandir) onToggleExpandir(!filtragemContext.estaExpandida)
  }

  _search = () => {
    const { onSearch, filtragemContext } = this.props
    if (onSearch) onSearch(filtragemContext.formValue)
  }

  _clear = clearFields => async () => {
    const { onSearch, filtragemContext, onClear } = this.props
    clearFields()
    await filtragemContext.updateContext({ formValue: null })
    if (onSearch) onSearch({})
    if (onClear) onClear()
  }

  render() {
    let {
      classes,
      strings,
      filtragemContext,
      campos,
      camposExpandidos,
      paperProps,
      estaExpandido,
      ...otherProps
    } = this.props
    estaExpandido = typeof expandido === 'boolean' ? estaExpandido : filtragemContext.estaExpandida
    return (
      <Paper {...paperProps}>
        <SimpleForm onChange={value => filtragemContext.updateContext({ formValue: value })} {...otherProps}>
          <SimpleFormContext.Consumer>
            {({ clearFields }) => (
              <Fragment>
                <div className={classes.minimizado}>
                  <IconButton onClick={this._search}>
                    <Search />
                  </IconButton>
                  <div className={classes.campos}>{campos}</div>
                  <IconButton onClick={this._clear(clearFields)}>
                    <Clear />
                  </IconButton>
                  {camposExpandidos && (
                    <IconButton onClick={this._toggleExpandir}>
                      <ExpandMoreIcon estaExpandido={estaExpandido} />
                    </IconButton>
                  )}
                </div>
                <Collapse in={estaExpandido}>
                  <Divider />
                  <div className={classes.expandido}>
                    {camposExpandidos}
                    <div className={classes.expandidoActions}>
                      <MpButton
                        IconComponent={Search}
                        variant="material"
                        className={classes.pesquisar}
                        onClick={this._search}
                      >
                        {strings.pesquisar}
                      </MpButton>
                      <MpButton
                        IconComponent={Clear}
                        variant="material"
                        className={classes.limpar}
                        onClick={this._clear(clearFields)}
                      >
                        {strings.limparFiltro}
                      </MpButton>
                    </div>
                  </div>
                </Collapse>
              </Fragment>
            )}
          </SimpleFormContext.Consumer>
        </SimpleForm>
      </Paper>
    )
  }
}
