import { FiltroComponent } from './FiltroComponent'
import { compose } from 'redux'
import { withStyles } from '@material-ui/core/styles'

import { withStringsContext } from 'utils/strings/context'

import { withFiltragemContext } from '../context'

import { style } from './style'

export const Filtro = compose(
  withStyles(style),
  withFiltragemContext,
  withStringsContext,
)(FiltroComponent)
