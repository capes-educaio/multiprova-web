import PropTypes from 'prop-types'

export const propTypes = {
  campos: PropTypes.node,
  camposExpandidos: PropTypes.node,
  paperProps: PropTypes.object,
  estaExpandido: PropTypes.bool,
  onSearch: PropTypes.func,
  onClear: PropTypes.func,
  // style
  classes: PropTypes.object.isRequired,
}
