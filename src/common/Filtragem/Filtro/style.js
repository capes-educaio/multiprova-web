export const style = theme => ({
  minimizado: { padding: 10, display: 'flex', alignItems: 'center', '& *': { zIndex: 2 } },
  expandido: { padding: '0 20px 10px 20px' },
  campos: { flexGrow: 1 },
  expandidoActions: { display: 'flex' },
  pesquisar: { marginRight: 10 },
  limpar: {},
})
