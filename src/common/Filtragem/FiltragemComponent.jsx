import React, { Component } from 'react'

import { initiateContextState } from 'utils/context'
import { FiltragemContext } from './context'

import { propTypes } from './propTypes'

export class FiltragemComponent extends Component {
  static propTypes = propTypes
  static defaultProps = { estaExpandidaInicial: false }

  constructor(props) {
    super(props)
    const contextInitialValue = {
      formValue: null,
      estaExpandida: props.estaExpandidaInicial,
      montarFiltro: this._montarFiltro,
    }
    initiateContextState(this, contextInitialValue)
  }

  _montarFiltro = () => {
    const { montarFiltro } = this.props
    if (montarFiltro) return montarFiltro(this.state.context.formValue)
    else return this.state.context.formValue
  }

  render() {
    const { children } = this.props
    return <FiltragemContext.Provider value={this.state.context}>{children}</FiltragemContext.Provider>
  }
}
