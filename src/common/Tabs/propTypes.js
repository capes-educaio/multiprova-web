import PropTypes from 'prop-types'

export const propTypes = {
  labels: PropTypes.arrayOf(PropTypes.string),
  containers: PropTypes.arrayOf(PropTypes.node),
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
}
