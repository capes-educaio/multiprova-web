import PropTypes from 'prop-types'

export const propTypes = {
  dados: PropTypes.object.isRequired,
  tipo: PropTypes.string.isRequired,
  onFechar: PropTypes.func.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
