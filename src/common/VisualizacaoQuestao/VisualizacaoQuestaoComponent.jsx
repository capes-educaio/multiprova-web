import React, { Component } from 'react'

import CircularProgress from '@material-ui/core/CircularProgress'

import { Pdf } from 'common/Pdf'
import { getUrlDownload } from 'utils/getUrlDownload'
import { post } from 'api'

import { propTypes } from './propTypes'

export class VisualizacaoQuestaoComponent extends Component {
  static defaultProps = { tipoTemplate: 'geral' }
  static propTypes = propTypes

  urlVisualizacao

  state = {
    estaCarregando: true,
    urlVisualizacao: null,
  }

  componentDidMount = () => {
    this._getUrlVisualizacao()
  }

  get _dadosParaPreview() {
    const dados = { ...this.props.dados }
    const atributosParaDeletar = ['elaborador', 'dificuldade', 'tagsVirtuais', 'status', 'anoEscolar', 'publico']
    atributosParaDeletar.forEach(atributo => delete dados[atributo])
    return dados
  }

  _getUrlVisualizacao = async () => {
    try {
      const { data } = await post('questoes/preview', {
        dados: this._dadosParaPreview,
        tipoTemplate: this.props.tipoTemplate,
      })
      const urlVisualizacao = await getUrlDownload(data.impressaoId)
      this.setState({ estaCarregando: false, urlVisualizacao })
    } catch (error) {
      this.props.onFechar()
    }
  }

  render() {
    const { classes } = this.props
    const { estaCarregando, urlVisualizacao } = this.state
    return <div className={classes.root}>{estaCarregando ? <CircularProgress /> : <Pdf file={urlVisualizacao} />}</div>
  }
}
