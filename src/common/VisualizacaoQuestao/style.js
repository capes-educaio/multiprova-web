export const style = theme => ({
  root: {
    backgroundColor: 'white',
    padding: '20px 0',
    '& > *': {
      maxWidth: '80%',
      display: 'block',
      margin: 'auto',
    },
  },
  html2Pdf: {
    marginLeft: '20px',
  },
})
