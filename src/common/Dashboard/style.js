export const style = theme => ({
  root: {
    paddingTop: 10,
  },
  loading: {
    display: 'flex',
    justifyContent: 'center',
    paddingTop: 50,
  },
  paper: {
    margin: 4,
    padding: '20px 0px',
    boxShadow: 'none',
    borderRadius: 5,
  },
})
