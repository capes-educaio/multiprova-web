import React, { Fragment } from 'react'

import ReactEcharts from 'echarts-for-react'

import CircularProgress from '@material-ui/core/CircularProgress'
import { Grid, Paper } from '@material-ui/core'

import { SimpleCard } from 'common/SimpleCard'

import { propTypes } from './propTypes'
import { defaultProps } from './defaultProps'

export const DashboardComponent = ({ classes, strings, loading, cardData, graphData }) => (
  <Fragment>
    {loading ? (
      <div className={classes.loading}>
        <CircularProgress />
      </div>
    ) : (
      <Grid container spacing={8} className={classes.root}>
        {cardData &&
          cardData.map((card, index) => (
            <Grid item xs={card.gridXs} key={index}>
              <SimpleCard customClasses={card.classes} titulo={card.titulo} numero={card.value}></SimpleCard>
            </Grid>
          ))}
        {graphData &&
          graphData.map((graph, index) => (
            <Grid item xs={graph.gridXs} key={index}>
              <Paper className={classes.paper}>
                <ReactEcharts option={graph.option} className={classes.chart} notMerge lazyUpdate />
              </Paper>
            </Grid>
          ))}
      </Grid>
    )}
  </Fragment>
)

DashboardComponent.propTypes = propTypes
DashboardComponent.defaultProps = defaultProps
