import { MpTabsComponent } from './MpTabsComponent'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'

import { mapStateToProps, mapDispatchToProps } from './redux'
import { style } from './style'

export const MpTabs = compose(
  withStyles(style, { withTheme: true }),
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(MpTabsComponent)
