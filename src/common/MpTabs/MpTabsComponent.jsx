import React, { Component } from 'react'

import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import { propTypes } from './propTypes'

export class MpTabsComponent extends Component {
  static propTypes = propTypes

  render() {
    const { tabs, tabIndex, handleChange, classes, outrosPropsTabs, outrosPropsTab, isValid } = this.props
    return (
      <Tabs
        {...outrosPropsTabs}
        className={classes.root}
        classes={{ text: classes.tabsText, indicator: classes.tabsIndicator }}
        value={tabIndex}
        onChange={(event, tabIndex) => handleChange(tabIndex)}
        textColor="inherit"
        variant="scrollable"
        scrollButtons="off"
      >
        {tabs.map((item, index) => (
          <Tab
            {...outrosPropsTab}
            classes={{ root: classes.botoesMenu, selected: classes.tabSelected }}
            style={{ marginLeft: index === 0 ? '0px' : '1px' }}
            key={index}
            id={item.id}
            label={item.label}
            icon={item.icon}
            disabled={!isValid || tabIndex === index}
            onClick={item.onClick}
          />
        ))}
      </Tabs>
    )
  }
}
