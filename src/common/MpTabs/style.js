const TAMANHO_DA_BARRA_DE_MENU = 690

export const style = theme => ({
  root: {
    backgroundColor: theme.palette.steelBlue,
    height: '40px',
    minHeight: '40px',
    '& > div': {
      display: 'flex',
      [theme.breakpoints.up(TAMANHO_DA_BARRA_DE_MENU)]: {
        width: 'fit-content',
      },
      backgroundColor: '#ffffff',
    },
  },
  botoesMenu: {
    backgroundColor: theme.palette.darkBlue,
    color: theme.palette.gelo,
    textTransform: 'none',
    padding: '5px 10px 5px 10px',
    fontFamily: 'Arial',
    minHeight: '40px',
    minWidth: 'fit-content',
    opacity: 1,
    '&:hover': {
      height: '40px',
      backgroundColor: theme.palette.steelBlue,
      fontWeight: theme.typography.fontWeightMedium,
      opacity: 0.8,
    },
    '& > span': {
      display: 'flex',
      flexDirection: 'row',
    },
    '& > span > span': {
      padding: '5px',
    },
    '&$tabSelected': {
      backgroundColor: theme.palette.primary.main,
      opacity: 1,
    },
  },
  tabSelected: {},
  tabsRoot: {
    height: '40px',
    minHeight: '40px',
  },
  tabsIndicator: {
    backgroundColor: '#33bb43',
  },
})
