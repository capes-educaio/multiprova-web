import PropTypes from 'prop-types'

export const propTypes = {
  tabs: PropTypes.arrayOf(PropTypes.object),
  tabIndex: PropTypes.number.isRequired,
  outrosPropsTabs: PropTypes.object,
  outrosPropsTab: PropTypes.object,
  isValid: PropTypes.bool,
  handleChange: PropTypes.func.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
}
