import React from 'react'
import { withContext } from 'utils/context'

export const TreeViewContext = React.createContext()
export const withTreeViewContext = withContext(TreeViewContext, 'treeViewContext')
