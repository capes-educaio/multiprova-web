import React, { Component } from 'react'

import { initiateContextState } from 'utils/context'

import { propTypes } from './propTypes'
import { TreeViewContext } from './context'

export class TreeViewComponent extends Component {
  static propTypes = propTypes

  constructor(props) {
    super(props)
    initiateContextState(this, {
      dicionarioTreeItems: {},
      expandirTodos: this._expandirTodos,
      contrairTodos: this._contrairTodos,
    })
  }

  _expandirTodos = () => {
    const { dicionarioTreeItems } = this._contextValue
    Object.keys(dicionarioTreeItems).forEach(id => dicionarioTreeItems[id].expandir())
  }

  _contrairTodos = () => {
    const { dicionarioTreeItems } = this._contextValue
    Object.keys(dicionarioTreeItems).forEach(id => dicionarioTreeItems[id].contrair())
  }

  render() {
    const { children } = this.props
    return <TreeViewContext.Provider value={this.state.context}>{children}</TreeViewContext.Provider>
  }
}
