import React, { Component } from 'react'

import { TreeItemExpandBar } from '../TreeItemExpandBar'
import { TreeItemContent } from '../TreeItemContent'

import { propTypes } from './propTypes'

export class TreeItemBottomComponent extends Component {
  static propTypes = propTypes

  render() {
    const { classes, children } = this.props
    return (
      <div className={classes.root}>
        <TreeItemExpandBar />
        <TreeItemContent>{children}</TreeItemContent>
      </div>
    )
  }
}
