import { TreeItemComponent } from './TreeItemComponent'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'

import { withTreeViewContext } from '../TreeView/context'

import { mapStateToProps, mapDispatchToProps } from './redux'
import { style } from './style'

export const TreeItem = compose(
  withStyles(style),
  // withRouter,
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withTreeViewContext,
)(TreeItemComponent)
