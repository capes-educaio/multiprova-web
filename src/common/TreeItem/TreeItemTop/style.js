export const style = theme => ({
  root: { display: 'flex', alignItems: 'center' },
  pointer: { cursor: 'pointer' },
  button: { padding: 2, marginRight: 5 },
  expand: {
    transform: 'rotate(0deg)',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
})
