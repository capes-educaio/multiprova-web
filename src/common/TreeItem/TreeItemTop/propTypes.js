import PropTypes from 'prop-types'

export const propTypes = {
  expandOnTitle: PropTypes.bool,
  disableArrow: PropTypes.bool,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
