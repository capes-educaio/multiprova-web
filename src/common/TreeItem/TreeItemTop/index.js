import { TreeItemTopComponent } from './TreeItemTopComponent'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'

import { withTreeItemContext } from '../context'

import { mapStateToProps, mapDispatchToProps } from './redux'
import { style } from './style'

export const TreeItemTop = compose(
  withStyles(style),
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withTreeItemContext,
)(TreeItemTopComponent)
