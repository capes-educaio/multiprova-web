import React, { Component } from 'react'
import classnames from 'classnames'

import IconButton from '@material-ui/core/IconButton'
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown'

import { propTypes } from './propTypes'

export class TreeItemTopComponent extends Component {
  static propTypes = propTypes

  _toggleExpandido = () => {
    const { treeItemContext } = this.props
    treeItemContext.updateContext({ expandido: !treeItemContext.expandido })
  }

  render() {
    const { classes, children, expandOnTitle, treeItemContext, disableArrow } = this.props
    return (
      <div className={classes.root}>
        {disableArrow || (
          <IconButton classes={{ root: classes.button }} onClick={this._toggleExpandido}>
            <KeyboardArrowDownIcon
              className={classnames(classes.expand, { [classes.expandOpen]: treeItemContext.expandido })}
            />
          </IconButton>
        )}
        <div
          className={classnames({ [classes.pointer]: expandOnTitle })}
          onClick={expandOnTitle ? this._toggleExpandido : null}
        >
          {children}
        </div>
      </div>
    )
  }
}
