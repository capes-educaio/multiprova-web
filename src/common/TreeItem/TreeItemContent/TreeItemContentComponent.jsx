import React, { Component } from 'react'
import classnames from 'classnames'

import { propTypes } from './propTypes'

export class TreeItemContentComponent extends Component {
  static propTypes = propTypes

  render() {
    const { classes, children, treeItemContext } = this.props
    return <div className={classnames(classes.root, { [classes.hidden]: !treeItemContext.expandido })}>{children}</div>
  }
}
