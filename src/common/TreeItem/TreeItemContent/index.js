import { TreeItemContentComponent } from './TreeItemContentComponent'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'

import { withTreeItemContext } from '../context'

import { mapStateToProps, mapDispatchToProps } from './redux'
import { style } from './style'

export const TreeItemContent = compose(
  withStyles(style),
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withTreeItemContext,
)(TreeItemContentComponent)
