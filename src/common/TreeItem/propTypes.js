import PropTypes from 'prop-types'

export const propTypes = {
  title: PropTypes.node,
  children: PropTypes.node,
  expandOnTitle: PropTypes.bool,
  disableArrow: PropTypes.bool,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
