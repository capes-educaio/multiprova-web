import React from 'react'
import { withContext } from 'utils/context'

export const TreeItemContext = React.createContext()
export const withTreeItemContext = withContext(TreeItemContext, 'treeItemContext')
