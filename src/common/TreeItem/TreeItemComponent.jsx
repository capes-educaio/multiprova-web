import React, { Component } from 'react'
import uid from 'uuid/v4'

import { initiateContextState } from 'utils/context'

import { TreeItemTop } from './TreeItemTop'
import { TreeItemBottom } from './TreeItemBottom'

import { TreeItemContext } from './context'
import { propTypes } from './propTypes'

export class TreeItemComponent extends Component {
  static propTypes = propTypes
  _id = uid()

  constructor(props) {
    super(props)
    initiateContextState(this, { expandido: true })
  }

  componentDidMount = () => this.props.treeViewContext && this._iniciarNoTreeView()

  componentWillUnmount = () => this.props.treeViewContext && this._retirarNoTreeView()

  get _expandOnTitle() {
    const { expandOnTitle, treeViewContext } = this.props
    return expandOnTitle || (treeViewContext && treeViewContext.expandOnTitle)
  }

  get _disableArrow() {
    const { disableArrow, treeViewContext } = this.props
    return disableArrow || (treeViewContext && treeViewContext.disableArrow)
  }

  _iniciarNoTreeView = () => {
    const { updateContext, dicionarioTreeItems } = this.props.treeViewContext
    dicionarioTreeItems[this._id] = { expandir: this._expandir, contrair: this._contrair }
    updateContext({ dicionarioTreeItems })
  }

  _retirarNoTreeView = () => {
    const { updateContext, dicionarioTreeItems } = this.props.treeViewContext
    delete dicionarioTreeItems[this._id]
    updateContext({ dicionarioTreeItems })
  }

  _expandir = () => this._updateContext({ expandido: true })

  _contrair = () => this._updateContext({ expandido: false })

  render() {
    const { title, children } = this.props
    return (
      <TreeItemContext.Provider value={this.state.context}>
        <TreeItemTop disableArrow={this._disableArrow} expandOnTitle={this._expandOnTitle}>
          {title}
        </TreeItemTop>
        <TreeItemBottom>{children}</TreeItemBottom>
      </TreeItemContext.Provider>
    )
  }
}
