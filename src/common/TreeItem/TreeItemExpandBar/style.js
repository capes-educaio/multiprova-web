export const style = theme => ({
  root: {
    width: 28,
    marginRight: 5,
    borderRadius: 20,
    '&:hover': {
      backgroundColor: theme.palette.gelo,
      filter: 'brightness(85%)',
    },
  },
})
