import React, { Component } from 'react'

import { propTypes } from './propTypes'

export class TreeItemExpandBarComponent extends Component {
  static propTypes = propTypes

  render() {
    const { classes, treeItemContext } = this.props
    return <div className={classes.root} onClick={() => treeItemContext.updateContext({ expandido: false })} />
  }
}
