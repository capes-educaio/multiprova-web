import { AutosuggestOptions } from './AutosuggestOptions'
import { AutosuggestNoOptionMessage } from './AutosuggestNoOptionMessage'
import { AutosuggestPlaceholder } from './AutosuggestPlaceholder'
import { AutosuggestSingleValue } from './AutosuggestSingleValue'
import { AutosuggestMultiValue } from './AutosuggestMultiValue'
import { AutosuggestValueContainer } from './AutosuggestValueContainer'

export const AutosuggestComponents = {
  Option: AutosuggestOptions,
  NoOptionsMessage: AutosuggestNoOptionMessage,
  Placeholder: AutosuggestPlaceholder,
  SingleValue: AutosuggestSingleValue,
  MultiValue: AutosuggestMultiValue,
  ValueContainer: AutosuggestValueContainer,
  DropdownIndicator: null,
}
