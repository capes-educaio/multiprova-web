import { compose } from 'redux'
import { withStyles } from '@material-ui/core/styles'

import { CreatableSelectComponent } from './CreatableSelectComponent'
import { SelectComponent } from './SelectComponent'
import { style } from './style'

export const CreatableSelect = compose(withStyles(style))(CreatableSelectComponent)

export const Select = compose(withStyles(style))(SelectComponent)
