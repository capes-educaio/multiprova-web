import React, { Component } from 'react'
import CreatableSelect from 'react-select/lib/Creatable'
import { AutosuggestComponents } from './AutosuggestComponents'

export class CreatableSelectComponent extends Component {
  static propTypes = { ...CreatableSelect.propTypes }
  render() {
    const { components, isClearable, ...props } = this.props
    const { placeholder } = this.props
    const spreadComponents = components ? { ...AutosuggestComponents, ...components } : AutosuggestComponents
    return (
      <CreatableSelect
        isClearable={isClearable || !!placeholder}
        components={placeholder ? spreadComponents : { ...spreadComponents, DropdownIndicator: null }}
        {...props}
      />
    )
  }
}
