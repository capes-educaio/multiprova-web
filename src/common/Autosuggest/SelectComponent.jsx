import React, { Component } from 'react'
import Select from 'react-select'
import { AutosuggestComponents } from './AutosuggestComponents'

export class SelectComponent extends Component {
  static propTypes = { ...Select.propTypes }
  render() {
    const { components, isClearable, ...props } = this.props
    const { placeholder } = this.props
    const spreadComponents = components ? { ...AutosuggestComponents, ...components } : AutosuggestComponents
    return (
      <Select
        isClearable={isClearable || !!placeholder}
        components={placeholder ? spreadComponents : { ...spreadComponents, DropdownIndicator: null }}
        {...props}
      />
    )
  }
}
