import React from 'react'

export function AutosuggestInputComponent({ inputRef, ...props }) {
  return <div ref={inputRef} {...props} />
}
