import React from 'react'

export function AutosuggestValueContainer(props) {
  return <div className={props.selectProps.classes.valueContainer}>{props.children}</div>
}
