import PropTypes from 'prop-types'

export const propTypes = {
  className: PropTypes.string,
  onClick: PropTypes.func,
  header: PropTypes.node,
  opcoes: PropTypes.node,
  content: PropTypes.node,
  children: PropTypes.node,
  footer: PropTypes.node,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
