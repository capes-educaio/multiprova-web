export const style = theme => ({
  root: {
    margin: '0 10px',
    display: 'flex',
    alignItems: 'center',
    flexGrow: 1,
  },
})
