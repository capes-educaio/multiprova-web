import React, { Component } from 'react'
import classnames from 'classnames'

import { propTypes } from './propTypes'

export class MpCardHeaderComponent extends Component {
  static propTypes = propTypes

  render() {
    const { classes, className, children, onClick } = this.props
    return (
      <div onClick={onClick} className={classnames(classes.root, { [className]: className })}>
        {children}
      </div>
    )
  }
}
