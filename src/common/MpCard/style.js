export const style = theme => ({
  clickable: { cursor: 'pointer' },
  root: { padding: 10 },
  cardTop: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: '0 0 10px 0',
  },
  header: {},
  opcoes: {},
  content: {},
  footer: {},
})
