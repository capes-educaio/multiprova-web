import React, { Component } from 'react'
import classnames from 'classnames'

import Paper from '@material-ui/core/Paper'

import { MpCardContent } from './MpCardContent'
import { MpCardFooter } from './MpCardFooter'
import { MpCardHeader } from './MpCardHeader'

import { propTypes } from './propTypes'

export class MpCardComponent extends Component {
  static propTypes = propTypes

  render() {
    const { classes, className, header, opcoes, content, children, footer, onClick } = this.props
    return (
      <Paper
        onClick={onClick}
        classes={{ root: classnames(classes.root, { [className]: className, [classes.clickable]: onClick }) }}
      >
        {(header || opcoes) && (
          <div className={classes.cardTop}>
            {header && <MpCardHeader className={classes.header}>{header}</MpCardHeader>}
            {opcoes && <div className={classnames(classes.opcoes, 'opcoes')}>{opcoes}</div>}
          </div>
        )}
        {content && <MpCardContent className={classes.content}>{content}</MpCardContent>}
        {children}
        {footer && <MpCardFooter className={classes.footer}>{footer}</MpCardFooter>}
      </Paper>
    )
  }
}
