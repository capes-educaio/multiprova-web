import PropTypes from 'prop-types'

export const propTypes = {
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.instanceOf(Date)]).isRequired,
  inputName: PropTypes.string.isRequired,
  inputLabel: PropTypes.string.isRequired,
  onFormChangeData: PropTypes.func.isRequired,
  flatClass: PropTypes.string.isRequired,
  labelClass: PropTypes.string.isRequired,
  otherFlatpickrProps: PropTypes.object,
  onClose: PropTypes.func,
  onFocus: PropTypes.func,
  disabled: PropTypes.bool,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
}
