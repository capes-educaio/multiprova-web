import React from 'react'
import Flatpickr from 'react-flatpickr'
import { Portuguese } from 'flatpickr/dist/l10n/pt'
import { propTypes } from './propTypes'
import CalendarioIcon from 'localModules/icons/CalendarioIcon'
import classNames from 'classnames'

import 'flatpickr/dist/flatpickr.min.css'

export const CalendarioInputComponent = ({
  classes,
  theme,
  value,
  inputName,
  inputLabel,
  onFormChangeData,
  flatClass,
  labelClass,
  onClose,
  onFocus,
  disabled,
  otherFlatpickrProps,
}) => (
  <Flatpickr
    data-enable-time
    value={value}
    options={{
      locale: Portuguese,
      time_24hr: true,
      dateFormat: 'd/m/Y H:i',
      position: 'above',
      wrap: true,
      ...otherFlatpickrProps,
    }}
    onChange={date => onFormChangeData(date)}
    onClose={date => onClose(date)}
  >
    <div className={classNames(disabled ? classes.disabled + ' ' + flatClass : flatClass)}>
      <label className={classNames(disabled ? classes.disabled + ' ' + labelClass : labelClass)} htmlFor={inputName}>
        {inputLabel}
      </label>
      <div className={classes.calendar}>
        <input
          id={inputName}
          type="text"
          className={classes.input}
          onFocus={onFocus}
          disabled={disabled}
          required
          data-input
        />
        <button className={classes.button} type="button" data-toggle>
          <CalendarioIcon height="24" width="24" SVGcolor={theme.palette.cinza700} />
        </button>
      </div>
    </div>
  </Flatpickr>
)

CalendarioInputComponent.propTypes = propTypes
