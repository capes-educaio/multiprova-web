import { compose } from 'redux'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'
// import { withRouter } from 'react-router'

import { ImageInputComponent } from './ImageInputComponent'
import { style } from './style'
import { mapStateToProps, mapDispatchToProps } from './redux'

export const ImageInput = compose(
  withStyles(style),
  // withRouter,
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(ImageInputComponent)
