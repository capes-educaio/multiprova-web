import PropTypes from 'prop-types'

export const propTypes = {
  value: PropTypes.string,
  onChange: PropTypes.func,
  imageName: PropTypes.string,
  previewLabel: PropTypes.string,
  label: PropTypes.string.isRequired,
  convertToHeight: PropTypes.number,
  fullWidth: PropTypes.bool,
  loading: PropTypes.bool,
  // style
  classes: PropTypes.object.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
  // router
  // history: PropTypes.object.isRequired,
  // location: PropTypes.object.isRequired,
  // match: PropTypes.object.isRequired,
}
