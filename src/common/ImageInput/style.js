export const style = theme => ({
  container: {
    display: 'flex',
    alignItems: 'center',
  },
  fullWidthContainer: {
    display: 'flex',
    alignItems: 'center',
    width: '100%',
  },
  fullWidth: {
    width: '100%',
  },
  text: {
    margin: '0 10px 0 0',
  },
  input: {
    display: 'none',
  },
  labelShrink: {
    margin: '0 !important',
  },
  labelRoot: {
    margin: '0 0 0 30px',
  },
  previewLabel: {
    margin: '10px 0 0 0',
  },
  imagePaper: {
    margin: '0 0 10px 0',
  },
  button: {
    cursor: 'pointer !important',
  },
  closeIcon: {
    color: theme.palette.primary.main,
    '& :hover': {
      color: theme.palette.secondary.main,
    },
  },
})
