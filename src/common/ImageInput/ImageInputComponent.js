import React, { Component } from 'react'
import uid from 'uuid/v4'

import CircularProgress from '@material-ui/core/CircularProgress'
import FormControl from '@material-ui/core/FormControl'
import InputLabel from '@material-ui/core/InputLabel'
import Input from '@material-ui/core/Input'
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'

import AddPhotoAlternateIcon from '@material-ui/icons/AddPhotoAlternate'
import CloseIcon from '@material-ui/icons/Close'

import { propTypes } from './proptypes'
import { defaultProps } from './defaultProps'

export class ImageInputComponent extends Component {
  static propTypes = propTypes

  _fileInputRef

  static defaultProps = defaultProps

  constructor(props) {
    super(props)
    this.state = {
      imageName: props.imageName,
      value: props.value ? props.value : null,
      width: 0,
      height: 0,
    }
  }

  componentDidMount = () => {
    const { value } = this.props
    if (value) {
      const image = new Image()
      image.onload = () => this._setImageDimentions(image)
      image.src = value
      const extension = value.substring('data:image/'.length, value.indexOf(';base64'))
      const imageName = uid() + '.' + extension
      this.setState({ imageName })
    }
  }

  _setImageDimentions = image => {
    const { width, height } = this._getDimentions(image)
    this.setState({ height, width })
  }

  _simulateClick = () => {
    if (this._fileInputRef) {
      this._fileInputRef.click()
    }
  }

  _onChangeImage = e => {
    const file = e.target.files[0]
    if (!file) return
    const imageName = file.name
    const canvas = document.createElement('canvas')
    const ctx = canvas.getContext('2d')
    const image = new Image()
    image.onload = () => this._drawImage({ canvas, ctx, image, imageName })
    image.src = URL.createObjectURL(file)
    return
  }

  _drawImage = ({ canvas, ctx, image, imageName }) => {
    const { onChange } = this.props
    const { width, height } = this._getDimentions(image)
    canvas.width = width
    canvas.height = height
    ctx.drawImage(image, 0, 0, width, height)
    const base64Image = canvas.toDataURL()
    this.setState({ height, width, imageName, value: base64Image })
    if (onChange) onChange(base64Image)
  }

  _getDimentions = image => {
    const { convertToHeight } = this.props
    let width = image.naturalWidth
    let height = image.naturalHeight
    if (convertToHeight) {
      const scale = convertToHeight / image.naturalHeight
      width = image.naturalWidth * scale
      height = convertToHeight
    }
    return { width, height }
  }

  _clearInput = e => {
    e.stopPropagation()
    const { onChange } = this.props
    this.setState({ imageName: '', value: null })
    if (onChange) onChange('')
  }

  render() {
    let { loading, classes, previewLabel, strings, label, fullWidth, disableImageMargin } = this.props
    const { imageName, value, height, width } = this.state
    const shrink = Boolean(imageName)
    return (
      <div className={fullWidth ? classes.container : classes.fullWidth}>
        {loading && <CircularProgress className={classes.text} />}
        <input
          ref={ref => (this._fileInputRef = ref)}
          accept="image/*"
          className={classes.input}
          id="raised-button-file"
          type="file"
          onChange={this._onChangeImage}
        />
        <FormControl className={fullWidth ? classes.fullWidth : null}>
          <InputLabel classes={{ root: classes.labelRoot, shrink: classes.labelShrink }} shrink={shrink}>
            {label}
          </InputLabel>
          <Input
            startAdornment={<AddPhotoAlternateIcon color="primary" />}
            endAdornment={
              value ? (
                <span className={classes.closeIcon} onClick={this._clearInput}>
                  <CloseIcon />
                </span>
              ) : null
            }
            onClick={this._simulateClick}
            readOnly
            value={imageName}
            classes={{ root: classes.button, input: classes.button }}
          />
          {value && (
            <Typography variant="caption" className={classes.previewLabel}>
              {previewLabel ? previewLabel : strings.preview}
            </Typography>
          )}
          {value && (
            <Paper style={{ height, width }} className={disableImageMargin ? null : classes.imagePaper}>
              <img src={value} alt="uploadImage" height={height} width={width} />
            </Paper>
          )}
        </FormControl>
      </div>
    )
  }
}
