import React from 'react'
import classnames from 'classnames'
import { Paper } from '@material-ui/core'
import Divider from '@material-ui/core/Divider'
import Collapse from '@material-ui/core/Collapse'
import IconButton from '@material-ui/core/IconButton'
import Search from '@material-ui/icons/Search'
import Clear from '@material-ui/icons/Clear'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import { updateUrlState } from 'localModules/urlState'
import { Actions } from 'common/Actions'
import { propTypes } from './propTypes'
import { isEmpty } from 'utils/isEmpty'

export class BuscaComponent extends React.Component {
  static propTypes = propTypes

  filtroRef

  constructor(props) {
    super(props)
    this.state = {
      anchorEl: null,
      valorDoForm: {
        textoFiltro: '',
        dificuldade: 0,
        status: 0,
        elaborador: '',
      },
      expanded: false,
    }
  }

  onFormFiltroChange = async valorDoForm => {
    await this.setState({ valorDoForm })
  }

  _updateBuscaState = () => {
    const { inputsDoFiltro } = this.props.location.state.hidden
    if (!isEmpty(inputsDoFiltro)) this.setState({ valorDoForm: inputsDoFiltro })
  }

  componentDidMount = () => {
    this._updateBuscaState()
    const { onRef } = this.props
    if (onRef) onRef(this)
  }

  componentWillUnmount = () => {
    const { onRef } = this.props
    if (onRef) onRef(null)
  }

  handleClose = () => {
    this.setState({
      anchorEl: null,
    })
  }

  handleClickOpen = event => {
    event.preventDefault()
    this.setState({
      anchorEl: event.currentTarget,
    })
  }

  onClickPesquisar = async () => {
    const { montarFiltro, selectFiltro } = this.props
    const { valorDoForm } = this.state

    if (this.filtroRef) {
      const filtro = montarFiltro(valorDoForm)
      selectFiltro(valorDoForm)
      await updateUrlState({ hidden: { filtro, inputsDoFiltro: valorDoForm } })
    }
  }

  onClickClear = async () => {
    if (this.filtroRef) {
      await this.filtroRef.limparFiltro()
    }
    this.onClickPesquisar()
    updateUrlState({ hidden: { filtro: {}, inputsDoFiltro: {} } })
  }

  handleKeyPress = event => {
    if (event.key === 'Enter') {
      setTimeout(() => {
        this.onClickPesquisar()
      }, 350)
    }
  }

  handleExpandClick = () => {
    this.setState(state => ({ expanded: !state.expanded }))
  }

  render() {
    const { valorDoForm, expanded } = this.state
    const { onFormFiltroChange, handleKeyPress, handleExpandClick } = this
    const { FormPadrao, FormExpandido, strings, classes } = this.props
    const actionsConfig = [
      {
        icone: Search,
        texto: '',
        buttonProps: {
          onClick: this.onClickPesquisar,
        },
      },
    ]
    const actionsConfigExpandido = [
      {
        icone: Search,
        texto: strings.pesquisar,
        buttonProps: {
          onClick: this.onClickPesquisar,
        },
      },
    ]

    const actionsConfigClear = [
      {
        icone: Clear,
        tooltip: { title: strings.limparFiltro },
        buttonProps: {
          onClick: this.onClickClear,
        },
      },
    ]
    const actionsConfigClearExpandido = [
      {
        icone: Clear,
        texto: strings.limparFiltro,
        buttonProps: {
          onClick: this.onClickClear,
        },
      },
    ]
    return (
      <Paper>
        <div className={classes.formPadrao}>
          <Actions config={actionsConfig} className={classes.actions} />
          <span className={classes.inputTag}>
            <FormPadrao
              valorDoForm={valorDoForm}
              onFormFiltroChange={onFormFiltroChange}
              handleKeyPress={handleKeyPress}
              onRef={filtroRef => {
                this.filtroRef = filtroRef
              }}
            />
          </span>
          <Actions config={actionsConfigClear} className={classes.actions} />

          <IconButton
            className={classnames(classes.expand, {
              [classes.expandOpen]: expanded,
            })}
            onClick={handleExpandClick}
            aria-expanded={expanded}
            aria-label="Show more"
          >
            <ExpandMoreIcon />
          </IconButton>
        </div>

        <Divider />

        <Collapse in={expanded}>
          <div className={classes.visibleCollapse}>
            <div className={classes.formExpandido}>
              <FormExpandido
                valorDoForm={valorDoForm}
                onFormFiltroChange={onFormFiltroChange}
                handleKeyPress={handleKeyPress}
              >
                <div className={classes.botoes}>
                  <Actions config={actionsConfigExpandido} />
                  <Actions config={actionsConfigClearExpandido} />
                </div>
              </FormExpandido>
            </div>
          </div>
        </Collapse>
      </Paper>
    )
  }
}
