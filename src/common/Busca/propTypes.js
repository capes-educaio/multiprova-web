import PropTypes from 'prop-types'

export const propTypes = {
  FormPadrao: PropTypes.any,
  FormExpandido: PropTypes.any,
  montarFiltro: PropTypes.func,
  // style
  classes: PropTypes.object,
  // strings
  strings: PropTypes.object.isRequired,
  // redux state
  usuarioAtual: PropTypes.object.isRequired,
  // redux actions
  selectFiltro: PropTypes.func.isRequired,
  // router
  history: PropTypes.object.isRequired,
}
