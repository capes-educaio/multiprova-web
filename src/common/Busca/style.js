export const style = theme => ({
  inputTag: {
    marginTop: '5px',
  },
  visibleCollapse: {
    marginTop: '-10px',
  },
  formPadrao: {
    boxSizing: 'border-box',
    width: '100%',
    marginTop: '10px',
    paddingBottom: '5px',
    paddingTop: '5px',
    display: 'flex',
    '&  > span': {
      width: '100%',
    },
  },
  formExpandido: {
    paddingLeft: '20px',
    paddingRight: '20px',
    marginTop: '10px',
    paddingBottom: '5px',
    paddingTop: '10px',
  },
  expand: {
    transform: 'rotate(0deg)',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
    margin: 'auto 0 auto auto',
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  actions: {},
  botoes: {
    display: 'flex',
    '& > *': {
      padding: 0,
    },
  },
})
