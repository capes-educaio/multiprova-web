import React from 'react'
import { mount } from 'enzyme'

import { updateUrlState } from 'localModules/urlState'

import { BuscaComponent } from '../BuscaComponent'

jest.mock('@material-ui/icons/Search', () => props => <div>{props.children}</div>)
jest.mock('@material-ui/icons/Clear', () => props => <div>{props.children}</div>)
jest.mock('@material-ui/icons/ExpandMore', () => props => <div>{props.children}</div>)

jest.mock('common/Actions', () => ({
  Actions: props => <div>{props.children}</div>,
}))

jest.mock('@material-ui/core', () => ({
  Popover: props => <div>{props.children}</div>,
  Paper: props => <div>{props.children}</div>,
}))

jest.mock('localModules/urlState', () => ({
  updateUrlState: jest.fn(),
}))

jest.mock('common/PaginaPaper', () => ({
  PaginaPaper: props => <div>{props.children}</div>,
}))

let componenteGenerico = props => <div>{props.children}</div>

let defaultProps = {
  FormPadrao: componenteGenerico,
  FormExpandido: componenteGenerico,
  montarFiltro: jest.fn(),
  selectFiltro: jest.fn(),
  classes: {},
  strings: {},
  usuarioAtual: {},
  urlState: {},
  history: {},
  location: { state: { hidden: '' } },
}
const wrapper = mount(<BuscaComponent {...defaultProps} />)
const instance = wrapper.instance()

describe('Checando se o estado é atualizado', () => {
  it('chama o onFormFiltroChange()', () => {
    const valorDoFormTeste = {
      texto: 'vetor',
      dificuldade: 1,
      tags: ['matematica', 'algebra'],
    }
    instance.onFormFiltroChange(valorDoFormTeste)
    expect(instance.state.valorDoForm).toBe(valorDoFormTeste)
  })

  it('chama o handleClose', () => {
    instance.handleClose()
    expect(instance.state.anchorEl).toBe(null)
  })

  it('chama o handleClickOpen', () => {
    const eventTest = {
      preventDefault: jest.fn(),
      currentTarget: 1,
    }
    instance.handleClickOpen(eventTest)
    expect(instance.state.anchorEl).toBe(1)
  })
})

describe('Teste do handleKeyPress()', () => {
  const spy = jest.spyOn(instance, 'onClickPesquisar')

  it('checa se Pesquisa quando o Enter é pressionado', () => {
    const eventKeyEnter = {
      key: 'Enter',
    }
    instance.handleKeyPress(eventKeyEnter)
    setTimeout(() => {
      expect(spy).toHaveBeenCalled()
      spy.mockRestore()
    }, 500)
  })
  it('checa se não pesquisa quando outra tecla é pressionada', () => {
    const eventKeySpace = {
      key: 'Space',
    }
    instance.handleKeyPress(eventKeySpace)
    setTimeout(() => {
      expect(spy).not.toHaveBeenCalled()
      spy.mockRestore()
    }, 500)
  })
})

describe('Verificando o onClickPesquisar', () => {
  const valorDoFormTeste = {
    texto: 'vetor',
    dificuldade: 1,
    tags: ['matematica', 'algebra'],
  }
  it('não deve entrar no if - filtroRef undefined', () => {
    instance.filtroRef = null
    instance.onClickPesquisar()
    expect(instance.props.montarFiltro).not.toBeCalledWith(valorDoFormTeste)
    expect(instance.props.selectFiltro).not.toBeCalledWith(valorDoFormTeste)
    expect(updateUrlState).not.toBeCalledWith({ filtro: instance.props.montarFiltro(valorDoFormTeste) })
  })
})
