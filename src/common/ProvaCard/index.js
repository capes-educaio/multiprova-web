import { ProvaCardComponent } from './ProvaCardComponent'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { withUrlContext } from 'common/UrlProvider/context'
import { withStyles } from '@material-ui/core/styles'

import { mapStateToProps, mapDispatchToProps } from './redux'
import { style } from './style'

export const ProvaCard = compose(
  withStyles(style),
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withUrlContext,
)(ProvaCardComponent)
