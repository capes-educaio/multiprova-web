export const style = theme => ({
  root: {
    borderLeft: `6px solid ${theme.palette.primary.main}`,
    [theme.breakpoints.down('sm')]: {
      borderLeft: `0px solid ${theme.palette.primary.main}`,
    },
    borderRadius: 0,
    padding: '18px 0px 5px 0px',
  },
  icon: { fill: theme.palette.cinzaEscuro, fontSize: 20, marginRight: 10 },
  cardContent: {
    wordWrap: 'break-word',
    padding: '7px 20px 0px',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  expand: {
    transform: 'rotate(0deg)',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
    marginLeft: 'auto',
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  actions: {
    display: 'flex',
    alignSelf: 'flex-end',
  },
  cardExpandedContent: {
    wordWrap: 'break-word',
    padding: '5px 20px 20px 20px',
    '& > *': {
      margin: '-15px 0',
    },
  },
  cardFlex: {
    display: 'flex',
    justifyContent: 'space-between',
    marginTop: '-10px',
  },
  titulo: {
    cursor: 'pointer',
    display: 'flex',
    justifyContent: 'space-between',
    '& > :last-child': {
      flexShrink: '0',
      width: 'fit-content',
    },
  },
  conteudoMinimizado: {
    padding: '0 10px',
    maxHeight: '72px',
    position: 'relative',
    textOverflow: 'ellipsis',
    '&:after': {
      content: '""',
      position: 'absolute',
      top: '32px',
      right: '0',
      width: '100%',
      height: '40px',
      background: `-webkit-linear-gradient(transparent, ${theme.palette.fundo.cinza})`,
    },
    '& p': {
      margin: '10px 0 0 0',
    },
  },
  rodape: {
    margin: '0px 25px 0px 0px',
    padding: '0 0 2px 0',
    textAlign: 'right',
  },
  tituloProva: {
    fontWeight: 500,
    color: theme.palette.primary.main,
    fontSize: 20,
    lineHeight: 'normal',
  },
  descricaoProva: {
    fontWeight: 450,
    fontSize: 15,
    color: theme.palette.primary.main,
    marginTop: 5,
    lineHeight: 'normal',
  },
  tituloDescricaoContainer: {
    marginBottom: 0,
    marginRight: 5,
  },
  qtdContainer: {
    display: 'flex',
    flexDirection: 'column',
  },
  '@media screen and (min-width:430px)': {
    qtdContainer: {
      paddingRight: 10,
    },
  },
  qtdItem: {
    display: 'flex',
    flexDirection: 'row',
  },
})
