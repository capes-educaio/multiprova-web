import PropTypes from 'prop-types'

export const propTypes = {
  onClick: PropTypes.func,
  instancia: PropTypes.object.isRequired,
  arrastavel: PropTypes.bool,
  exibirDataUltimaAlteracao: PropTypes.bool,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
