import React, { Component } from 'react'

import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'

import { ProvaCardHeader } from 'common/ProvaCardHeader'

import { propTypes } from './propTypes'

export class ProvaCardComponent extends Component {
  static propTypes = propTypes

  get dataUltimaAlteracao() {
    const { instancia, strings } = this.props
    const prova = instancia
    let dataSoComDiaMesAno = prova.dataUltimaAlteracao.substring(0, 10)
    let partesDaData = dataSoComDiaMesAno.split('-')

    return strings.ultimaAlteracaoProva + `${partesDaData[2]}-${partesDaData[1]}-${partesDaData[0]}`
  }

  getQuantidadeDeQuestoes = () => {
    const prova = this.props.instancia
    let quantidadeDeQuestoes = 0
    prova.questoes.forEach(questao => {
      if (questao.bloco) quantidadeDeQuestoes += questao.bloco.questoes.length
      else quantidadeDeQuestoes += 1
    })
    return quantidadeDeQuestoes
  }

  render() {
    const { classes, exibirDataUltimaAlteracao, arrastavel, opcoes, onClick, id } = this.props

    const prova = this.props.instancia
    return (
      <Paper id={id} onClick={() => onClick && onClick()} className={classes.root}>
        <ProvaCardHeader arrastavel={arrastavel} opcoes={opcoes} instancia={prova} />
        <div className={classes.cardContent}>
          <div className={classes.tituloDescricaoContainer}>
            <div className={classes.titulo}>
              <Typography variant="subtitle1" align="left" className={classes.tituloProva}>
                {prova.titulo}
              </Typography>
            </div>
            <Typography align="left" variant="body2" className={classes.descricaoProva}>
              {prova.descricao}
            </Typography>
          </div>
        </div>
        <div className={classes.rodape}>
          {exibirDataUltimaAlteracao && <Typography variant="caption">{this.dataUltimaAlteracao}</Typography>}
        </div>
      </Paper>
    )
  }
}
