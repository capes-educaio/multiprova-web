import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'
import React, { Component } from 'react'
import { propTypes } from './propTypes'
import { defaultProps } from './defaultProps'
import TextField from '@material-ui/core/TextField'

export class TagCardComponent extends Component {
  static propTypes = propTypes
  static defaultProps = defaultProps
  render() {
    const {
      classes,
      id,
      instancia: tag,
      opcoes,
      modoEdicao,
      strings,
      nomeDigitado,
      handleChangeNomeDigitado,
    } = this.props
    return (
      <div>
        <Paper id={id} className={classes.card}>
          <div className={classes.cardContent}>
            {modoEdicao ? (
              <TextField
                className={classes.editor}
                autoFocus={true}
                id="tagForm"
                label={strings.editarTag}
                rows="4"
                value={nomeDigitado}
                fullWidth
                onChange={event => handleChangeNomeDigitado(event.target.value)}
                margin="normal"
                error={!!!nomeDigitado}
                helperText={!!nomeDigitado ? '' : strings.nomeNaoPodeSerVazio}
              />
            ) : (
              <div className={classes.chip}>
                <Typography>{tag.nome}</Typography>
              </div>
            )}

            <div className={classes.actions}>{opcoes}</div>
          </div>
        </Paper>
      </div>
    )
  }
}
