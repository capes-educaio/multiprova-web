export const style = theme => ({
  card: {
    padding: '12px 0px 12px 0px',
  },
  icon: { fill: theme.palette.cinzaEscuro, fontSize: 20, marginRight: 10 },
  cardContent: {
    wordWrap: 'break-word',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: '0 8px',
  },
  actions: {
    display: 'flex',
    marginLeft: '8px',
  },
  editor: {
    margin: '0, 5px',
  },
  chip: {
    display: 'inline-block',
    background: theme.palette.cinzaAlto,
    padding: '3px 10px',
    borderRadius: '32px',
    fontSize: '13px',
    textAlign: 'center',
  },
})
