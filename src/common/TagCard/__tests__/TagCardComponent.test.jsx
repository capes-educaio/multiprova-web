import React from 'react'
import { shallow } from 'enzyme'
import { ptBr } from 'utils/strings/ptBr'

import { TagCardComponent } from '../TagCardComponent'

const tagComNome = {
  nome: 'Matemática',
}

const executaAlgo = { executaAlgo: jest.fn() }

const defaultProps = {
  // style
  classes: {},
  // strings
  strings: ptBr,
  instancia: tagComNome,
  opcoes: <button onClick={executaAlgo} />,
  history: { push: jest.fn() },
  location: { pathname: '' },
}

describe('Testando <TagCardCompoennt />', () => {
  let wrapper
  beforeEach(() => {
    wrapper = shallow(<TagCardComponent {...defaultProps} />)
  })

  it('Carrega o componente', () => {
    expect(wrapper.length).toEqual(1)
  })
})
