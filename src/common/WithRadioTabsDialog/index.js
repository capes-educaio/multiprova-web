import { connect } from 'react-redux'
import { compose } from 'redux'
import { mapDispatchToProps, mapStateToProps } from './redux'
import { WithRadioTabsDialogComponent } from './WithRadioTabsDialogComponent'

export const WithRadioTabsDialog = compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
    null,
    { forwardRef: true },
  ),
  WithRadioTabsDialogComponent,
)
