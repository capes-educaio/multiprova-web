import {
  CircularProgress,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControl,
  FormControlLabel,
  Radio,
  RadioGroup,
} from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'
import { MpButton } from 'common/MpButton'
import React from 'react'
import { style } from './style'

export const WithRadioTabsDialogComponent = WrappedComponent => {
  return withStyles(style)(
    class extends React.Component {
      state = {
        buscaIsLoading: false,
        abaAtual: this.props.abas[0].value,
      }

      confirmarFecharModal = () => {
        const { onClose } = this.props
        if (this.child.onClose) {
          this.child.onClose()
        }
        onClose()
      }
      handleChangeRadio = async event => {
        this.setState({
          abaAtual: event.target.value,
        })
      }
      ligarLoading = () => {
        this.setState({ buscaIsLoading: true })
      }
      desligarLoading = () => {
        this.setState({ buscaIsLoading: false })
      }

      render() {
        const { abas, open = false, strings, onClose, classes, ...passThroughProps } = this.props
        const { abaAtual, buscaIsLoading } = this.state

        return (
          <Dialog
            disableBackdropClick
            disableEscapeKeyDown
            fullWidth={true}
            maxWidth={'lg'}
            open={open}
            onClose={onClose}
            aria-labelledby="form-dialog-title"
            className={classes.root}
          >
            <div>
              <DialogTitle id="form-dialog-title" className={classes.title} disableTypography>
                <FormControl component="fieldset" className={classes.formControl}>
                  <RadioGroup
                    name="tipo de busca"
                    className={classes.group}
                    value={abaAtual}
                    onChange={this.handleChangeRadio}
                  >
                    {abas.map((aba, index) => (
                      <FormControlLabel
                        value={aba.value}
                        control={<Radio color="primary"></Radio>}
                        label={aba.label}
                        key={index}
                      />
                    ))}
                  </RadioGroup>
                </FormControl>
                {buscaIsLoading && <CircularProgress size={30} />}
              </DialogTitle>
            </div>
            <DialogContent className={classes.dialogContent}>
              <WrappedComponent
                {...passThroughProps}
                abaAtual={abaAtual}
                ligarLoading={this.ligarLoading}
                desligarLoading={this.desligarLoading}
                innerRef={ref => {
                  this.child = ref
                }}
              />
            </DialogContent>
            <DialogActions>
              <MpButton onClick={onClose} variant="material">
                {strings.cancelar}
              </MpButton>
              <MpButton
                buttonProps={{ variant: 'contained', color: 'primary' }}
                onClick={this.confirmarFecharModal}
                variant="material"
              >
                {strings.confirmar}
              </MpButton>
            </DialogActions>
          </Dialog>
        )
      }
    },
  )
}
