import PropTypes from 'prop-types'

export const propTypes = {
  //open que será passado para o dialog
  open: PropTypes.bool.isRequired,
  //array de objetos na forma {value, label}
  abaAtual: PropTypes.array.isRequired,
  // deve lidar com o fechamento do modal
  onClose: PropTypes.func.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
