import PropTypes from 'prop-types'

export const propTypes = {
  children: PropTypes.arrayOf(PropTypes.string).isRequired,
  //redux
  listaTags: PropTypes.array,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
