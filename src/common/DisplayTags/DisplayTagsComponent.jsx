import React, { Component } from 'react'
import Chip from '@material-ui/core/Chip'

import { propTypes } from './propTypes'

export class DisplayTagsComponent extends Component {
  static propTypes = propTypes

  get nomeTagsUsuario() {
    const { children, listaTags } = this.props
    let nomeTagsUsuario = []
    listaTags &&
      children.forEach(id => {
        const tagEncontrado = listaTags.find(tag => tag.id === id)
        if (tagEncontrado) {
          nomeTagsUsuario.push(tagEncontrado.nome)
        }
      })
    return nomeTagsUsuario
  }

  render() {
    const { classes } = this.props
    return (
      <div className={classes.container}>
        {this.nomeTagsUsuario.map((tagNome, index) => (
          <Chip className={classes.chip} label={tagNome} key={index} />
        ))}
      </div>
    )
  }
}
