import React from 'react'
import { shallow } from 'enzyme'
import { ptBr } from 'utils/strings/ptBr'

import { DisplayTagsComponent } from '../DisplayTagsComponent'

const listaTags = [
  {
    id: '1',
    nome: 'tag1',
  },
  {
    id: '2',
    nome: 'tag2',
  },
]

const tagIds = ['1', '3']

const tagIdsSemTagsIguais = ['4', '3']

const defaultProps = {
  children: tagIds,
  //redux
  listaTags,
  // style
  classes: {},
  // strings
  strings: ptBr,
  tagIds,
}

const defaultPropsSemTagsNaLista = {
  // style
  classes: {},
  // strings
  strings: ptBr,
  listaTags,
  children: tagIdsSemTagsIguais,
}

describe('Testando <DisplayTagsComponent />', () => {
  let wrapper = shallow(<DisplayTagsComponent {...defaultProps} />)

  it('Carrega o componente', () => {
    expect(wrapper.length).toEqual(1)
  })

  it('nomeTagsUsuario vai retornar o nome dos tags encontrados conforme os props', () => {
    const RESULTADO_ESPERADO = [listaTags[0].nome]
    const RESULTADO_NAO_ESPERADO = [listaTags[0].nome, listaTags[1].nome]
    expect(wrapper.instance().nomeTagsUsuario).toEqual(RESULTADO_ESPERADO)
    expect(wrapper.instance().nomeTagsUsuario).not.toEqual(RESULTADO_NAO_ESPERADO)
    wrapper.unmount()
  })

  it('nomeTagsUsuario vai retornar vazio, pois não tem tags correspondentes na lista', () => {
    wrapper = shallow(<DisplayTagsComponent {...defaultPropsSemTagsNaLista} />)
    const RESULTADO_ESPERADO = []
    expect(wrapper.instance().nomeTagsUsuario).toEqual(RESULTADO_ESPERADO)
    expect(wrapper.instance().nomeTagsUsuario.length).not.toBeGreaterThan(0)
  })
})
