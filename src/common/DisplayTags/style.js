export const style = theme => ({
  chip: {
    height: '25px',
    margin: '3px 3px 0 0',
  },
  container: {
    margin: '0 0 10px 0',
  },
})
