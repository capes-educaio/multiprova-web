export const style = theme => ({
  container: {
    margin: '0 auto',
    [theme.breakpoints.up('md')]: {
      width: 960,
    },
  },
})
