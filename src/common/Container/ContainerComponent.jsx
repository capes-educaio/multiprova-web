import React, { Component } from 'react'

import { propTypes } from './propTypes'

export class ContainerComponent extends Component {
  static propTypes = propTypes

  render() {
    const { classes, children } = this.props
    return <div className={classes.container}>{children}</div>
  }
}
