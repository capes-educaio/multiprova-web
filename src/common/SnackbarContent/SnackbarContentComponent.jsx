import React from 'react'
import classNames from 'classnames'
import CheckCircleIcon from '@material-ui/icons/CheckCircle'
import ErrorIcon from '@material-ui/icons/Error'
import InfoIcon from '@material-ui/icons/Info'
import CloseIcon from '@material-ui/icons/Close'
import AutoRenew from '@material-ui/icons/Autorenew'
import IconButton from '@material-ui/core/IconButton'
import { propTypes } from './propTypes'
import SnackbarContent from '@material-ui/core/SnackbarContent'
import WarningIcon from '@material-ui/icons/Warning'

const variantIcon = {
  success: CheckCircleIcon,
  warning: WarningIcon,
  error: ErrorIcon,
  info: InfoIcon,
}

export const SnackbarContentComponent = props => {
  const { classes, className, message, onClose, variant, onTryAgain, tryAgain, showIcon, ...other } = props
  const Icon = variantIcon[variant]

  return (
    <SnackbarContent
      role="alert"
      className={classNames(classes[variant], className)}
      aria-label={message}
      aria-describedby="client-snackbar"
      message={
        <span id="client-snackbar" className={classes.message}>
          {showIcon && <Icon className={classNames(classes.icon, classes.iconVariant)} />}
          {message}
        </span>
      }
      action={[
        tryAgain ? (
          <IconButton key="tryAgain" aria-label="Tentar novamente" color="inherit" onClick={onTryAgain}>
            <AutoRenew className={classes.icon} />
          </IconButton>
        ) : (
          <IconButton key="close" aria-label="Fechar" color="inherit" onClick={onClose}>
            <CloseIcon className={classes.icon} />
          </IconButton>
        ),
      ]}
      {...other}
    />
  )
}

SnackbarContentComponent.propTypes = propTypes
