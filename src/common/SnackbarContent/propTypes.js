import PropTypes from 'prop-types'

export const propTypes = {
  strings: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired,
  showIcon: PropTypes.bool,
  tryAgain: PropTypes.bool,
  onTryAgain: (props, propName) => {
    if (props['tryAgain'] === true && (props[propName] === undefined || typeof props[propName] !== 'function'))
      return new Error('Por favor, forneça a propriedade função chamada: ' + propName)
  },
  className: PropTypes.string,
  message: PropTypes.node,
  onClose: PropTypes.func,
  variant: PropTypes.oneOf(['success', 'warning', 'error', 'info']).isRequired,
}
