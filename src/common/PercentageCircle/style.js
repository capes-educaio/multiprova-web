export const style = theme => ({
  circle: {
    overflow: 'hidden',
    position: 'relative',
    backgroundColor: theme.palette.minimumBlue,
    cursor: 'pointer',
  },
  leftWrap: {
    overflow: 'hidden',
    position: 'absolute',
    top: 0,
  },
  rightWrap: {
    overflow: 'hidden',
    position: 'absolute',
    top: 0,
  },
  loader: {
    position: 'absolute',
    left: 0,
    top: 0,
    borderRadius: 1000,
    transformOrigin: '0 50%',
    backgroundColor: theme.palette.mediumBlue,
  },
  loader2: {
    position: 'absolute',
    left: 0,
    top: 0,
    borderRadius: 1000,
    transformOrigin: '100% 50%',
    backgroundColor: theme.palette.mediumBlue,
  },

  innerCircle: {
    position: 'relative',
    textAlign: 'center',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: theme.palette.gelo,
  },
  text: {
    color: theme.palette.darkBlue,
    fontFamily: '"Roboto", "Helvetica", "Arial", "sans-serif"',
    cursor: 'pointer',
  },
  textFont: {
    fontSize: '10px',
  },
  textFont100: {
    fontSize: '8px',
  },
})
