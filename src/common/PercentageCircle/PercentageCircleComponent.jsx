import React, { Component } from 'react'
import { propTypes, defaultProps } from './propTypes'
import classnames from 'classnames'

export class PercentageCircleComponent extends Component {
  static propTypes = propTypes
  static defaultProps = defaultProps
  constructor(props) {
    super(props)
    const percent = props.percent
    let leftTransformerDegree = '0deg'
    let rightTransformerDegree = '0deg'
    if (percent >= 50) {
      rightTransformerDegree = '180deg'
      leftTransformerDegree = (percent - 50) * 3.6 + 'deg'
    } else {
      rightTransformerDegree = percent * 3.6 + 'deg'
      leftTransformerDegree = '0deg'
    }
    this.state = {
      percent: this.props.percent,
      borderWidth: this.props.borderWidth < 2 || !this.props.borderWidth ? 2 : this.props.borderWidth,
      leftTransformerDegree: leftTransformerDegree,
      rightTransformerDegree: rightTransformerDegree,
    }
  }
  render() {
    const { classes, onClick, radius, borderWidth, textStyle, children, percent } = this.props
    return (
      <div
        onClick={onClick}
        className={classes.circle}
        style={{
          width: radius * 2,
          height: radius * 2,
          borderRadius: radius,
        }}
      >
        <div
          className={classes.leftWrap}
          style={{
            width: radius,
            height: radius * 2,
            left: 0,
          }}
        >
          <div
            className={classes.loader}
            id="id1"
            style={{
              left: radius,
              width: radius,
              height: radius * 2,
              borderTopLeftRadius: 0,
              borderBottomLeftRadius: 0,
              transform: 'rotate(' + this.state.leftTransformerDegree + ')',
            }}
          />
        </div>
        <div
          className={classes.rightWrap}
          style={{
            width: radius,
            height: radius * 2,
            left: radius,
          }}
        >
          <div
            className={classes.loader2}
            id="id2"
            style={{
              left: -radius,
              width: radius,
              height: radius * 2,
              borderTopRightRadius: 0,
              borderBottomRightRadius: 0,
              transform: 'rotate(' + this.state.rightTransformerDegree + ')',
            }}
          />
        </div>
        <div
          className={classes.innerCircle}
          style={{
            left: borderWidth,
            top: borderWidth,
            width: (radius - borderWidth) * 2,
            height: (radius - borderWidth) * 2,
            borderRadius: radius - borderWidth,
          }}
        >
          {children ? (
            children
          ) : (
            <span
              className={classnames(
                percent >= 100 && classes.textFont100,
                percent < 100 && classes.textFont,
                !textStyle && classes.text,
                textStyle,
              )}
            >
              {percent}%
            </span>
          )}
        </div>
      </div>
    )
  }
}
