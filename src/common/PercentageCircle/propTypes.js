import PropTypes from 'prop-types'

export const propTypes = {
  radius: PropTypes.number,
  percent: PropTypes.number,
  borderWidth: PropTypes.number,
  textStyle: PropTypes.string,
  onClick: PropTypes.func,

  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}

export const defaultProps = {
  radius: 14,
  percent: 0,
  borderWidth: 4,
  disabled: false,
  textStyle: '',
  onClick: () => {},
}

/**
 * Options
Props 	Type 	Example 	Description
percent 	Number 	30 	the percent you need
radius 	Number 	20 	how large the circle is
borderWidth 	Number(default 2) 	5 	the width of percentage progress bar
textStyle 	Array 	{fontSize: 24, color: 'red'} 	define the style of the text which in the circle
children 	jsx 	<Text>123</Text> 	define the children component in the circle
 */
