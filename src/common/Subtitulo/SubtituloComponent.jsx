import React, { Component } from 'react'
import classnames from 'classnames'

import Divider from '@material-ui/core/Divider'
import Typography from '@material-ui/core/Typography'

import { propTypes } from './propTypes'

export class SubtituloComponent extends Component {
  static propTypes = propTypes

  render() {
    const { classes, children, marginTop } = this.props
    return (
      <div className={classnames(classes.root, { [classes.marginTop]: marginTop })}>
        <Typography className={classes.typography} color="primary" variant="subtitle2">
          {children}
        </Typography>
        <Divider className={classes.divider} />
      </div>
    )
  }
}
