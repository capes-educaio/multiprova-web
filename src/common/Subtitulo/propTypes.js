import PropTypes from 'prop-types'

export const propTypes = {
  children: PropTypes.node,
  marginTop: PropTypes.bool,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
