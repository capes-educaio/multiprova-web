import { ptBr } from 'utils/strings/ptBr'

import { EstatisticasComponent } from '../EstatisticasComponent'
import { mountTest } from 'localModules/testUtils/mountTest'

jest.mock('localModules/provas/ProvaUtils', () => ({
  provaUtils: {
    isProvaDinamica: () => jest.fn(),
    provaTipo: 'convencional',
  },
}))

const requiredProps = {
  prova: {},
  // redux state
  browser: {
    lessThan: {},
  },
  urlState: {
    hidden: {
      provaTipo: 'convencional',
    },
  },
  // style
  classes: {},
  // strings
  strings: ptBr,
}

const arrayOfProps = [requiredProps]

mountTest(EstatisticasComponent, 'EstatisticasComponent')(arrayOfProps)
