import PropTypes from 'prop-types'

export const propTypes = {
  prova: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
  strings: PropTypes.object.isRequired,
}
