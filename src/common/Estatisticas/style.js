export const style = theme => ({
  root: {
    maxWidth: '900px',
    minWidth: '900px',
  },
  paper: {
    margin: '20px 4px 20px 4px',
    padding: '20px 0px',
    boxShadow: 'none',
    borderRadius: '5',
  },
  rotulos: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    paddingBottom: '30px',
  },
  card: {
    boxShadow: 'none',
    margin: 4,
    borderRadius: 5,
  },
  titulo: {
    padding: '3px 10px 3px 10px',
    color: theme.palette.gelo,
    background: theme.palette.primary.main,
  },
  numero: {
    padding: '15px 0px 5px 10px',
    fontWeight: '100',
    color: theme.palette.primary.main,
  },
})
