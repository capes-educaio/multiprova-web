import React, { Component, Fragment } from 'react'
import { Paper } from '@material-ui/core'

import { provaUtils } from 'localModules/provas/ProvaUtils'

import { Dashboard } from 'common/Dashboard'
import { SwipeableContainer } from 'common/SwipeableContainer'

import { enumTipoQuestao } from 'utils/enumTipoQuestao'

import { propTypes } from './propTypes'
import { GraficoMultiplaEscolha } from './GraficoMultiplaEscolha'
import { GraficoVouF } from './GraficoVouF'
import { GraficoAssociacaoColunas } from './GraficoAssociacaoColunas'
import { GraficoAcertosPorQuestao } from './GraficoAcertosPorQuestao'
import { GraficoHistogramaProva } from './GraficoHistogramaProva'

export class EstatisticasComponent extends Component {
  static propTypes = propTypes
  constructor(props) {
    super(props)
    this.state = {
      carregando: true,
      cardData: [],
    }
  }

  setDashboardData = () => {
    const { classes, strings, prova, browser } = this.props
    const { notaProva, estatisticas } = prova
    if (notaProva || notaProva === 0) estatisticas.notaProva = notaProva
    const { media, desvioPadrao } = estatisticas ? estatisticas.gerais : 0

    const isMobile = browser.lessThan.medium

    const cardData = [
      {
        gridXs: isMobile ? 12 : 6,
        titulo: strings.mediaDaTurma,
        value: Number(media).toFixed(2),
        classes,
      },
      {
        gridXs: isMobile ? 12 : 6,
        titulo: strings.desvioPadraoDaTurma,
        value: Number(desvioPadrao).toFixed(2),
        classes,
      },
    ]
    if (notaProva || notaProva === 0) {
      cardData.push({
        gridXs: isMobile ? 12 : 4,
        titulo: strings.notaAluno,
        value: notaProva || notaProva === 0 ? notaProva : '-',
        classes,
      })
      cardData.forEach(el => (el.gridXs = isMobile ? 12 : 4))
      cardData.reverse()
    }
    this.setState({ cardData })
  }

  componentDidMount = () => {
    this.setDashboardData()
    this.setState({ carregando: false })
  }

  render() {
    const { classes, prova, browser } = this.props
    const { notaProva, estatisticas } = prova
    const { cardData, carregando } = this.state
    const isMobile = browser.lessThan.medium

    if (notaProva || notaProva === 0) estatisticas.notaProva = notaProva

    const painel = (
      <div>
        <Dashboard loading={carregando} cardData={cardData} />
        <Paper className={classes.paper}>
          <GraficoHistogramaProva estatisticas={estatisticas} />
        </Paper>
        {!provaUtils.isProvaDinamica && (
          <Fragment>
            <Paper className={classes.paper}>
              <GraficoAcertosPorQuestao estatisticas={estatisticas} />
            </Paper>
            <Paper className={classes.paper}>
              <SwipeableContainer>
                {estatisticas.graficosPorQuestao.map(item => {
                  if (enumTipoQuestao.tipoQuestaoMultiplaEscolha === item.tipo)
                    return <GraficoMultiplaEscolha multiplaEscolha={item} isMobile={isMobile} key={item.questao} />
                  if (enumTipoQuestao.tipoQuestaoAssociacaoDeColunas === item.tipo)
                    return <GraficoAssociacaoColunas associacaoColunas={item} isMobile={isMobile} key={item.questao} />
                  if (enumTipoQuestao.tipoQuestaoVerdadeiroOuFalso === item.tipo)
                    return <GraficoVouF vouf={item} isMobile={isMobile} key={item.questao} />
                  return null
                })}
              </SwipeableContainer>
            </Paper>
          </Fragment>
        )}
      </div>
    )
    return (
      <div>
        {notaProva || notaProva === 0 ? <div className={!isMobile ? classes.root : undefined}>{painel}</div> : painel}
      </div>
    )
  }
}
