import React from 'react'

import { propTypes } from './propTypes'
import ReactEcharts from 'echarts-for-react'
import marcacoesVouF from './marcacoesVouF'

export const GraficoVouFComponent = ({ vouf, classes, strings, isMobile }) => (
  <ReactEcharts
    option={marcacoesVouF.getOption({ vouf, strings, isMobile })}
    className={classes.chart}
    notMerge
    lazyUpdate
  />
)

GraficoVouFComponent.propTypes = propTypes
