import { ptBr } from 'utils/strings/ptBr'

import { GraficoVouFComponent } from '../GraficoVouFComponent'
import { mountTest } from 'localModules/testUtils/mountTest'

const requiredProps = {
  vouf: {},
  // style
  classes: {},
  // strings
  strings: ptBr,
}

const arrayOfProps = [requiredProps]

mountTest(GraficoVouFComponent, 'GraficoVouFComponent')(arrayOfProps)
