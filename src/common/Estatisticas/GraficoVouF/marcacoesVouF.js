import { theme } from 'utils/theme.js'
import quadrado from 'localModules/icons/QuadradoIcon.svg'
import { isEmpty } from 'utils/isEmpty'

const marcacoesVouF = {
  getOption: ({ vouf, strings, isMobile }) => {
    const percent = 100
    const questao = !isEmpty(vouf) ? vouf.questao : 0
    const dados = !isEmpty(vouf)
      ? vouf.marcacoes.reduce(
        (acc, cur) => {
          if (!cur) return acc
          const alguemMarcou = cur.v !== 0 || cur.f !== 0
          return {
            afirmacao: [...acc.afirmacao, cur.afirmacao],
            v: [
              ...acc.v,
              {
                name: cur.afirmacao,
                value: cur.v,
                percent: alguemMarcou ? Math.round(percent * (cur.v / (cur.v + cur.f))) : 0,
                itemStyle: alguemMarcou &&
                    cur.resposta === 'V' && {
                  barBorderColor: theme.palette.grafico.grey.transparent,
                  barBorderWidth: 7,
                },
              },
            ],
            f: [
              ...acc.f,
              {
                name: cur.afirmacao,
                value: cur.f,
                percent: alguemMarcou ? Math.round(percent * (cur.f / (cur.v + cur.f))) : 0,
                itemStyle: alguemMarcou &&
                    cur.resposta === 'F' && {
                  barBorderColor: theme.palette.grafico.grey.transparent,
                  barBorderWidth: 7,
                },
              },
            ],
          }
        },
        { afirmacao: [], f: [], v: [] },
      )
      : []

    const option = {
      title: {
        text: strings.estatisticasMarcacao(questao),
        left: 'center',
        top: 10,
        textStyle: {
          fontWeight: '100',
        },
      },

      legend: [
        {
          show: true,
          data: [
            strings.verdadeiroMinificado,
            strings.falsoMinificado,
            { name: strings.alternativaCorreta, icon: `image://${quadrado}` },
          ],
          borderColor: theme.palette.grafico.grey.main,
          bottom: '2%',
          orient: 'horizontal',
          borderWidth: 1,
          borderRadius: 5,
          padding: 10,
          itemGap: 15,
        },
      ],
      toolbox: {
        feature: {
          saveAsImage: { show: true, title: strings.salvarComoImagem },
        },
        top: isMobile ? '8%' : 'auto',
        left: isMobile ? '3%' : '35px',
        type: 'png',
      },
      tooltip: {
        trigger: 'item',
        axisPointer: {
          type: 'shadow',
        },
        formatter: value => strings.alunosResponderam({ value: value.data.value, percent: value.data.percent }),
      },
      grid: {
        top: '20%',
        bottom: '25%',
      },
      xAxis: {
        type: 'category',
        data: dados.afirmacao,
        name: strings.sentencas,
        nameLocation: 'center',
        nameGap: 25,
        splitLine: {
          show: false,
        },
      },
      yAxis: {
        type: 'value',
        splitArea: {
          show: false,
        },
        name: strings.numeroRespostas,
        nameTextStyle: {
          padding: 20,
        },
        nameLocation: 'center',
      },
      series: [
        {
          name: strings.verdadeiroMinificado,
          type: 'bar',
          stack: 'letra',
          zlevel: 1,
          barMaxWidth: 120,
          itemStyle: {
            color: theme.palette.primary.main,
          },
          label: {
            formatter: value => {
              if (value > 0) return value
              else return
            },

            normal: {
              show: true,
              distance: 7,
              formatter: value => `${value.data.value} (${value.data.percent}%)`,
              position: ['7%', -22],
            },
          },
          data: dados.v,
        },
        {
          name: strings.falsoMinificado,
          type: 'bar',
          stack: 'letra',
          barMaxWidth: 120,
          itemStyle: {
            color: theme.palette.secondary.dark,
          },
          label: {
            formatter: value => {
              if (value > 0) return value
              else return
            },

            normal: {
              show: true,
              formatter: value => `${value.data.value} (${value.data.percent}%)`,
              position: ['60%', -22],
            },
          },
          data: dados.f,
        },
        {
          name: strings.alternativaCorreta,
          type: 'bar',
          stack: 'letra',

          itemStyle: {
            color: theme.palette.grafico.grey.transparent,
          },
          data: [],
        },
      ],
    }

    return option
  },
}
export default marcacoesVouF
