import PropTypes from 'prop-types'

export const propTypes = {
  vouf: PropTypes.object.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
