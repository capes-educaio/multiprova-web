export const style = theme => ({
  container: {
    display: 'flex',
  },

  chart: { paddingLeft: '0px', paddingRight: '0px', minHeight: '400px' },
})
