import { theme } from 'utils/theme.js'
import { arredondar } from 'utils/arredondar.js'
import { isEmpty } from 'utils/isEmpty'

const mediaAcertos = {
  getOption: ({ estatisticas, strings }) => {
    const percent = 100,
      margem = 0.1
    const acertosQuestao = !isEmpty(estatisticas)
      ? estatisticas.acertosQuestao.map(item => [item.questao, arredondar(item.indice * percent, 2)])
      : []

    const option = {
      title: {
        text: strings.acertosPorQuestao,
        left: 'center',
        top: 10,
        textStyle: {
          fontWeight: '100',
        },
      },
      toolbox: {
        feature: {
          saveAsImage: { show: true, title: strings.salvarComoImagem },
        },
        left: '35px',
        type: 'png',
      },
      legend: {
        show: true,
        data: [strings.porcentagemAcertos],
        bottom: '2%',
        orient: 'horizontal',
        borderWidth: 1,
        borderColor: theme.palette.grafico.grey.main,
        borderRadius: 5,
        padding: 10,
      },
      tooltip: {
        trigger: 'item',
        axisPointer: {
          type: 'line',
        },
        formatter: value => {
          return strings.porcentagemDosAlunosAcertaram(value.data[1], value.data[0])
        },
      },
      grid: {
        bottom: '25%',
      },
      xAxis: {
        type: 'category',
        name: strings.numeroDasQuestoes,
        nameLocation: 'center',
        nameGap: 25,
        splitLine: {
          show: false,
        },
        max: parseInt(acertosQuestao.length + margem),
      },
      yAxis: {
        type: 'value',
        min: 0,
        max: 100,
        scale: true,
        splitArea: {
          show: false,
        },
        name: `${strings.porcentagemAcertos} (%)`,
        nameTextStyle: {
          padding: 20,
        },
        nameLocation: 'center',
      },
      series: [
        {
          name: strings.porcentagemAcertos,
          type: 'bar',
          itemStyle: {
            color: theme.palette.secondary.dark,
          },
          label: {
            formatter: value => {
              return value + '%'
            },
            normal: {
              formatter: value => {
                return `${value.data[1]}%`
              },
              show: true,
              position: 'top',
            },
            emphasis: {
              show: true,
              textStyle: {
                fontSize: 14,
              },
            },
          },
          data: acertosQuestao,
        },
      ],
    }

    return option
  },
}
export default mediaAcertos
