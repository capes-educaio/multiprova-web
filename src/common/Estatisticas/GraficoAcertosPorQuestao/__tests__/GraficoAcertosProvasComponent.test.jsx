import { ptBr } from 'utils/strings/ptBr'

import { GraficoAcertosPorQuestaoComponent } from '../GraficoAcertosPorQuestaoComponent'
import { mountTest } from 'localModules/testUtils/mountTest'

const requiredProps = {
  estatisticas: {},
  // style
  classes: {},
  // strings
  strings: ptBr,
}

const arrayOfProps = [requiredProps]

mountTest(GraficoAcertosPorQuestaoComponent, 'GraficoAcertosPorQuestaoComponent')(arrayOfProps)
