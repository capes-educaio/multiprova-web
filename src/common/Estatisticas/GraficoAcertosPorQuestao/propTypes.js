import PropTypes from 'prop-types'

export const propTypes = {
  // estatisticas: PropTypes.object.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
