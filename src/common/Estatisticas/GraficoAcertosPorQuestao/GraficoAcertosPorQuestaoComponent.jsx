import React from 'react'

import { propTypes } from './propTypes'
import ReactEcharts from 'echarts-for-react'
import acertosPorQuestao from './acertosPorQuestao'

export const GraficoAcertosPorQuestaoComponent = ({ estatisticas, classes, strings }) => (
  <ReactEcharts
    option={acertosPorQuestao.getOption({ estatisticas, strings })}
    className={classes.chart}
    notMerge
    lazyUpdate
  />
)

GraficoAcertosPorQuestaoComponent.propTypes = propTypes
