import { theme } from 'utils/theme.js'
import { isEmpty } from 'utils/isEmpty'

const getTamMaiorColuna = associacaoColunas => {
  if (isEmpty(associacaoColunas)) return {}

  const tamColB = associacaoColunas.colunaB.length
  const tamColA = associacaoColunas.colunaB[0].colunaA.length

  const objTam = {
    maiorColuna: tamColA > tamColB ? tamColA : tamColB,
    tamColA,
    tamColB,
  }
  return objTam
}

const marcacoesAssociacaoColunas = {
  getOption: ({ associacaoColunas, strings, isMobile }) => {
    const percent = 100
    const questao = !isEmpty(associacaoColunas) ? associacaoColunas.questao : 0

    const getEstatisticasMarcacoes = cur => {
      let quantidadeTotal = 0
      const dadosColunaA = cur.colunaA.map((item, index) => {
        const ninguemMarcou = item.quantidade === 0
        const respostaCerta = cur.rotuloResposta === item.rotulo
        quantidadeTotal += item.quantidade
        return {
          source: `${item.rotulo}a`,
          target: ninguemMarcou && !respostaCerta ? null : `${cur.rotulo}b`,
          value: item.quantidade,
          lineStyle: {
            width: respostaCerta ? 3 : 2,
            color: respostaCerta ? theme.palette.secondary.dark : theme.palette.grafico.grey.main,
            opacity: respostaCerta ? 0.7 : 0.6,
            curveness: 0.1,
          },

          label: {
            show: respostaCerta ? true : false,
            color: theme.palette.primary.dark,
            formatter: item => {
              return `${strings.acertos}: ${item.data.value} (${item.data.percent}%)`
            },
            position: 'end',
            align: 'center',
            verticalAlign: 'bottom',
            padding: 25,
            fontSize: 10,
          },
        }
      })
      return { dadosColunaA, quantidadeTotal }
    }

    const objTamColunas = getTamMaiorColuna(associacaoColunas)
    let calculoDaAltura = !isEmpty(objTamColunas) && 1250 / objTamColunas.maiorColuna

    let larguraDoGrafico = calculoDaAltura > 250 ? 'auto' : 600
    let alturaDoGrafico = calculoDaAltura > 250 ? 'auto' : calculoDaAltura

    let xColunaA = 0,
      xColunaB = 200,
      yColunaA = objTamColunas.maiorColuna === objTamColunas.tamColA ? 0 : 20,
      yColunaB = 0,
      espacoEntreItensA = 25,
      espacoEntreItensB = 25

    const dados = !isEmpty(associacaoColunas)
      ? associacaoColunas.colunaB.reduce(
        (acc, cur) => {
          const estatisticasMarcacao = getEstatisticasMarcacoes(cur)
          const data = {
            rotulosColuna: [],
            links: [],
          }
          for (let serie of estatisticasMarcacao.dadosColunaA) {
            const percentAlunos = Math.round(percent * (serie.value / estatisticasMarcacao.quantidadeTotal))
            serie.percent = percentAlunos

            data.links = [...data.links, ...acc.links, serie]

            const deveInserirRotulosColunaA = acc.rotulosColuna.findIndex(r => r.name === serie.source) === -1

            if (deveInserirRotulosColunaA) {
              data.rotulosColuna = [
                ...data.rotulosColuna,
                {
                  name: serie.source,
                  x: xColunaA,
                  y: yColunaA,
                  itemStyle: {
                    color: theme.palette.primary.light,
                  },
                },
              ]
              yColunaA += espacoEntreItensA
            }
          }
          data.rotulosColuna = [
            ...data.rotulosColuna,
            ...acc.rotulosColuna,
            {
              name: `${cur.rotulo}b`,
              x: xColunaB,
              y: yColunaB,
              itemStyle: {
                color: theme.palette.primary.dark,
              },
              label: {
                show: true,
              },
            },
          ]
          yColunaB += espacoEntreItensB
          return data
        },
        { rotulosColuna: [], links: [] },
      )
      : []
    const option = {
      title: {
        text: strings.estatisticasMarcacao(questao),
        left: 'center',
        top: 10,
        textStyle: {
          fontWeight: '100',
        },
      },

      legend: [
        {
          show: true,
          data: [
            { name: strings.colunaA, icon: 'pin' },
            { name: strings.colunaB, icon: 'pin' },
            { name: strings.alternativaCorreta, icon: 'line' },
          ],
          borderColor: theme.palette.grafico.grey.main,
          bottom: '2%',
          orient: 'horizontal',
          borderWidth: 1,
          borderRadius: 5,
          padding: [10, 10, 10],
          icon: 'roundRect',
          itemGap: 15,
        },
      ],

      toolbox: {
        feature: {
          saveAsImage: { show: true, title: strings.salvarComoImagem },
        },
        top: isMobile ? '8%' : 'auto',
        left: isMobile ? '3%' : '35px',
        type: 'png',
      },
      tooltip: {
        trigger: 'item',
        axisPointer: {
          type: 'shadow',
        },
        formatter: item => {
          const { value, percent, name } = item.data
          if (item.data.value >= 0) return strings.alunosResponderam({ value, percent })
          else return strings.itemColuna({ name })
        },
      },
      series: [
        {
          type: 'graph',
          layout: 'none',
          top: 90,
          height: alturaDoGrafico,
          width: larguraDoGrafico,
          symbolSize: 35,

          label: {
            normal: {
              show: true,
            },
          },
          edgeSymbol: ['circle', 'arrow'],
          edgeSymbolSize: [4, 10],
          edgeLabel: {
            normal: {
              textStyle: {
                fontSize: 14,
              },
            },
          },
          data: dados.rotulosColuna,
          links: dados.links,
        },
        {
          name: strings.colunaA,
          type: 'graph',
          color: theme.palette.primary.light,
        },
        {
          name: strings.colunaB,
          type: 'graph',
          color: theme.palette.primary.dark,
        },
        {
          name: strings.alternativaCorreta,
          type: 'graph',
          color: theme.palette.secondary.dark,
        },
      ],
    }

    return option
  },
}
export default marcacoesAssociacaoColunas
