import { ptBr } from 'utils/strings/ptBr'

import { GraficoAssociacaoColunasComponent } from '../GraficoAssociacaoColunasComponent'
import { mountTest } from 'localModules/testUtils/mountTest'

const requiredProps = {
  associacaoColunas: {
    questao: 3,
    tipo: 'associacaoDeColunas',
    colunaB: [
      {
        rotulo: 1,
        rotuloResposta: 2,
        colunaA: [
          {
            rotulo: 1,
            quantidade: 4,
          },
          {
            rotulo: 2,
            quantidade: 10,
          },
        ],
      },
      {
        rotulo: 2,
        rotuloResposta: 1,
        colunaA: [
          {
            rotulo: 1,
            quantidade: 9,
          },
          {
            rotulo: 2,
            quantidade: 4,
          },
        ],
      },
    ],
  },
  // style
  classes: {},
  // strings
  strings: ptBr,
}

const arrayOfProps = [requiredProps]

mountTest(GraficoAssociacaoColunasComponent, 'GraficoAssociacaoColunasComponent')(arrayOfProps)
