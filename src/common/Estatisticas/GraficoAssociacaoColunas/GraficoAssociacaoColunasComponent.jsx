import React from 'react'

import { propTypes } from './propTypes'
import ReactEcharts from 'echarts-for-react'
import marcacoesAssociacaoColunas from './marcacoesAssociacaoColunas'

export const GraficoAssociacaoColunasComponent = ({ associacaoColunas, classes, strings, isMobile }) => (
  <ReactEcharts
    option={marcacoesAssociacaoColunas.getOption({ associacaoColunas, strings, isMobile })}
    className={classes.chart}
    notMerge
    lazyUpdate
  />
)

GraficoAssociacaoColunasComponent.propTypes = propTypes
