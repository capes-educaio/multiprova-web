import { theme } from 'utils/theme.js'
import { isEmpty } from 'utils/isEmpty'

const mediaAcertos = {
  getOption: ({ multiplaEscolha, strings, isMobile }) => {
    const percent = 100
    const questao = !isEmpty(multiplaEscolha) ? multiplaEscolha.questao : 0

    const totalAlunos = !isEmpty(multiplaEscolha)
      ? multiplaEscolha.marcacoes.reduce((acumulado, proximo) => acumulado + proximo.quantidade, 0)
      : []

    let acertosPorAlternativa = !isEmpty(multiplaEscolha)
      ? multiplaEscolha.marcacoes.map(item => [
        item.letra,
        Math.round((item.quantidade / totalAlunos) * percent),
        item.quantidade,
      ])
      : []

    const alternativaCorreta = acertosPorAlternativa[0]
    acertosPorAlternativa = acertosPorAlternativa.slice(1)

    const option = {
      title: {
        text: strings.estatisticasMarcacao(questao),
        left: 'center',
        top: 10,
        textStyle: {
          fontWeight: '100',
        },
      },
      legend: {
        show: true,
        data: [strings.alternativaCorreta],
        borderColor: theme.palette.grafico.grey.main,
        bottom: '2%',
        orient: 'horizontal',
        borderWidth: 1,
        borderRadius: 5,
        padding: 10,
      },
      toolbox: {
        feature: {
          saveAsImage: { show: true, title: strings.salvarComoImagem },
        },
        top: isMobile ? '6%' : 'auto',
        left: isMobile ? '3%' : '35px',
        type: 'png',
      },
      tooltip: {
        trigger: 'item',
        axisPointer: {
          type: 'line',
        },
        formatter: value => strings.alunosResponderam({ value: value.data[1], percent: value.data[2] }),
      },
      grid: {
        bottom: '25%',
      },
      xAxis: {
        type: 'category',
        name: `${strings.itemNaMatriz} (${strings.quantidadeAlunosQuestao})`,
        nameLocation: 'center',
        nameGap: 25,
        splitLine: {
          show: false,
        },
      },
      yAxis: {
        type: 'value',
        min: 0,
        max: 100,
        scale: true,
        splitArea: {
          show: false,
        },
        name: `${strings.quantidadeMarcacoes} (%)`,
        nameTextStyle: {
          padding: 20,
        },
        nameLocation: 'center',
      },
      series: [
        {
          name: strings.alternativaCorreta,
          type: 'bar',
          data: [alternativaCorreta],
          stack: 'multiplaEscolha',
          barMaxWidth: 120,
          itemStyle: {
            color: theme.palette.secondary.dark,
          },
          label: {
            formatter: value => {
              return value + '%'
            },
            normal: {
              formatter: value => `${value.data[1]}% (${value.data[2]})`,
              show: true,
              position: 'top',
            },
            emphasis: {
              show: true,
              textStyle: {
                fontSize: 14,
              },
            },
          },
        },
        {
          name: strings.itemNaMatriz,
          type: 'bar',
          stack: 'multiplaEscolha',
          barMaxWidth: 120,
          itemStyle: {
            color: theme.palette.primary.dark,
          },
          label: {
            formatter: value => {
              return value + '%'
            },
            normal: {
              formatter: value => `${value.data[1]}% (${value.data[2]})`,
              show: true,
              position: 'top',
            },
            emphasis: {
              show: true,
              textStyle: {
                fontSize: 14,
              },
            },
          },
          data: acertosPorAlternativa,
        },
      ],
    }

    return option
  },
}
export default mediaAcertos
