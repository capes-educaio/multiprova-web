import { ptBr } from 'utils/strings/ptBr'

import { GraficoMultiplaEscolhaComponent } from '../GraficoMultiplaEscolhaComponent'
import { mountTest } from 'localModules/testUtils/mountTest'

const requiredProps = {
  multiplaEscolha: {},
  // style
  classes: {},
  // strings
  strings: ptBr,
}

const arrayOfProps = [requiredProps]

mountTest(GraficoMultiplaEscolhaComponent, 'GraficoMultiplaEscolhaComponent')(arrayOfProps)
