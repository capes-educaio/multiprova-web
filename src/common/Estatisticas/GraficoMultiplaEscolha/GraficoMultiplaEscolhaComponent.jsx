import React from 'react'

import { propTypes } from './propTypes'
import ReactEcharts from 'echarts-for-react'
import acertosPorAlternativa from './acertosPorAlternativa'

export const GraficoMultiplaEscolhaComponent = ({ multiplaEscolha, classes, strings, isMobile }) => (
  <ReactEcharts
    option={acertosPorAlternativa.getOption({ multiplaEscolha, strings, isMobile })}
    className={classes.chart}
    notMerge
    lazyUpdate
  />
)

GraficoMultiplaEscolhaComponent.propTypes = propTypes
