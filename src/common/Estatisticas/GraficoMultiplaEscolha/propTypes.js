import PropTypes from 'prop-types'

export const propTypes = {
  multiplaEscolha: PropTypes.object.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
