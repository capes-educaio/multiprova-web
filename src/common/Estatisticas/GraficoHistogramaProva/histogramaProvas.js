import _ from 'lodash'
import { theme } from 'utils/theme.js'
import { isEmpty } from 'utils/isEmpty'

const dadosFaixa = str => {
  let tot = str.split('-')
  const mediaFaixa = tot.reduce(function(prev, next) {
    return Math.round(parseFloat(prev) + parseFloat(next) / 2, 2)
  })

  return { mediaFaixa, min: parseFloat(tot[0]), max: parseFloat(tot[1]) }
}

const getFaixaDoAlunoNoGrafico = (histogramaNotas, notaProva) =>
  histogramaNotas.reduce(
    (prev, curr) => (Math.abs(curr[0] - notaProva) < Math.abs(prev[0] - notaProva) ? curr : prev),
    [0, 0],
  )

const mediaAcertos = {
  getOption: ({ estatisticas, strings }) => {
    const ultimaFaixa = 4
    const lowerBound = 0
    const upperBound = !isEmpty(estatisticas) ? dadosFaixa(estatisticas.histogramaNotas[ultimaFaixa].faixa).max + 1 : 10

    const resolucaoDoEixoX = upperBound * 2
    const comprimentoDaGaussiana = upperBound + 2

    const histogramaNotas = !isEmpty(estatisticas)
      ? estatisticas.histogramaNotas.map((nota, i) => [dadosFaixa(nota.faixa).mediaFaixa, nota.quantidade])
      : []
    let media = !isEmpty(estatisticas) ? estatisticas.gerais.media : 0
    let desvioPadrao = !isEmpty(estatisticas) ? estatisticas.gerais.desvioPadrao : 0

    const numeroDeFaixas = 5
    const mediaDasFaixasDoHistograma = !isEmpty(estatisticas)
      ? estatisticas.histogramaNotas.reduce((acumulado, atual) => acumulado + parseInt(atual.quantidade), 0) /
        numeroDeFaixas
      : 0

    const normalY = (x, media, desvioPadrao) =>
      mediaDasFaixasDoHistograma * Math.exp(-0.5 * Math.pow((x - media) / desvioPadrao, 2))
    const generatePoints = (lowerBound, upperBound) => {
      let min = lowerBound
      let max = upperBound
      let unit = (max - min) / 100
      return _.range(min, max, unit)
    }

    let points = generatePoints(lowerBound, comprimentoDaGaussiana)
    let ajusteGaussianoData = points.map(x => [x, normalY(x, media, desvioPadrao)])

    const notaProva = !isEmpty(estatisticas) && estatisticas.notaProva

    const closest = notaProva !== 0 ? getFaixaDoAlunoNoGrafico(histogramaNotas, notaProva) : histogramaNotas[0]

    const option = {
      title: {
        text: strings.histograma,
        left: 'center',
        top: 10,
        textStyle: {
          fontWeight: '100',
        },
      },
      toolbox: {
        feature: {
          saveAsImage: { show: true, title: strings.salvarComoImagem },
        },
        left: '35px',
        type: 'png',
      },
      legend: {
        show: true,
        data: [strings.mediaEvento, strings.ajusteGaussiano],
        bottom: '2%',
        orient: 'horizontal',
        borderWidth: 1,
        borderColor: theme.palette.grafico.grey.main,
        borderRadius: 5,
        padding: 10,
      },
      tooltip: {
        trigger: 'item',
        axisPointer: {
          type: 'line',
        },
      },
      grid: {
        bottom: '25%',
      },
      xAxis: {
        type: 'value',
        min: -1,
        max: upperBound + 3,
        name: strings.faixaDasNotas,
        nameLocation: 'center',
        nameGap: 25,
        splitNumber: resolucaoDoEixoX,
        axisLine: { onZero: false },
        axisLabel: {
          formatter: function(value) {
            const index = histogramaNotas.findIndex(x => {
              return x[0] === value
            })

            if (index >= 0) return !isEmpty(estatisticas) ? estatisticas.histogramaNotas[index].faixa : value
            return
          },
          show: true,
        },
        splitLine: {
          show: false,
        },
      },
      yAxis: {
        type: 'value',
        min: 0,
        scale: true,
        splitArea: {
          show: false,
        },
        name: strings.numeroAlunos,
        nameTextStyle: {
          padding: 20,
        },
        nameLocation: 'center',
        boundaryGap: [0, 1],
        axisLine: { onZero: false },
      },
      series: [
        {
          name: strings.mediaEvento,
          type: 'bar',
          itemStyle: {
            color: theme.palette.secondary.dark,
          },
          barWidth: '80px',
          label: {
            normal: {
              show: true,
              position: 'inside',
            },
            emphasis: {
              show: true,
              textStyle: {
                fontSize: 14,
              },
            },
          },
          data: histogramaNotas,
          markPoint: {
            data: [
              {
                symbol: notaProva || notaProva === 0 ? 'pin' : 'none',
                symbolSize: 50,
                coord: closest,
                value: notaProva,
                itemStyle: {
                  normal: { color: theme.palette.primary.main },
                },
              },
            ],
            tooltip: {
              formatter: function(param) {
                return `${strings.notaAluno}: ${param.value}`
              },
            },
          },
        },
        {
          name: strings.ajusteGaussiano,
          type: 'line',
          showSymbol: false,
          data: ajusteGaussianoData,
          itemStyle: {
            color: theme.palette.primary.main,
          },
          lineStyle: {
            type: 'solid',
          },

          markPoint: {
            itemStyle: {
              normal: {
                color: 'transparent',
              },
            },
          },
        },
      ],
    }

    return option
  },
}
export default mediaAcertos
