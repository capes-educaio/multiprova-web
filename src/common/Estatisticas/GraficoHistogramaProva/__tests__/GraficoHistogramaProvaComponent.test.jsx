import { ptBr } from 'utils/strings/ptBr'

import { GraficoHistogramaProvaComponent } from '../GraficoHistogramaProvaComponent'
import { mountTest } from 'localModules/testUtils/mountTest'

const requiredProps = {
  estatisticas: {},
  // style
  classes: {},
  // strings
  strings: ptBr,
}

const arrayOfProps = [requiredProps]

mountTest(GraficoHistogramaProvaComponent, 'GraficoHistogramaProvaComponent')(arrayOfProps)
