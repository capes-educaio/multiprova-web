import React from 'react'

import { propTypes } from './propTypes'
import ReactEcharts from 'echarts-for-react'
import histogramaProvas from './histogramaProvas'

export const GraficoHistogramaProvaComponent = ({ estatisticas, classes, strings }) => (
  <ReactEcharts
    option={histogramaProvas.getOption({ estatisticas, strings })}
    className={classes.chart}
    notMerge
    lazyUpdate
  />
)

GraficoHistogramaProvaComponent.propTypes = propTypes
