import React, { Component } from 'react'
import { Document, Page } from 'react-pdf'
import uid from 'uuid/v4'

import { propTypes } from './propTypes'

export class PdfComponent extends Component {
  static propTypes = propTypes

  state = { numPages: null }

  _onDocumentLoadSuccess = ({ numPages }) => this.setState({ numPages })

  _getColorIndicesForCoord = (x, y, width) => {
    var red = y * (width * 4) + x * 4
    return [red, red + 1, red + 2, red + 3]
  }

  _getColor = (imageData, xCoord, yCoord, canvasWidth) => {
    var colorIndices = this._getColorIndicesForCoord(xCoord, yCoord, canvasWidth)
    var redIndex = colorIndices[0]
    var greenIndex = colorIndices[1]
    var blueIndex = colorIndices[2]
    var alphaIndex = colorIndices[3]
    var redForCoord = imageData.data[redIndex]
    var greenForCoord = imageData.data[greenIndex]
    var blueForCoord = imageData.data[blueIndex]
    var alphaForCoord = imageData.data[alphaIndex]
    return [redForCoord, greenForCoord, blueForCoord, alphaForCoord]
  }

  _cropImageFromCanvas(canvas) {
    const paddingBottom = 70
    const ctx = canvas.getContext('2d')
    const imageData = ctx.getImageData(0, 0, canvas.width, canvas.height)
    let heightFinal = canvas.height
    for (let linha = 0; linha < canvas.height; linha++) {
      const pixelsColoridos = [...Array(canvas.width).keys()].reduce((numero, coluna) => {
        const cores = this._getColor(imageData, coluna, linha, canvas.width)
        const temCor = cores.some(cor => cor < 100)
        if (temCor) return numero + 1
        return numero
      })
      const linhaVazia = pixelsColoridos < 1
      if (linhaVazia && linha < heightFinal) heightFinal = linha
      else if (!linhaVazia && linha > heightFinal) heightFinal = canvas.height
    }
    var newWidth = canvas.width
    var newHeight = heightFinal + paddingBottom > canvas.height ? canvas.height : heightFinal + paddingBottom
    var cut = ctx.getImageData(0, 0, newWidth, newHeight)
    canvas.width = newWidth
    canvas.height = newHeight
    canvas.style.width = `${Math.floor(newWidth)}px`
    canvas.style.height = `${Math.floor(newHeight)}px`
    ctx.putImageData(cut, 0, 0)
  }

  _onRenderPage = pageId => () => {
    const page = document.getElementById(pageId)
    const canvases = page.getElementsByClassName('react-pdf__Page__canvas')
    for (let i = 0; i < canvases.length; i++) this._cropImageFromCanvas(canvases[i])
  }

  render() {
    const { classes, file } = this.props
    const { numPages } = this.state
    return (
      <Document file={file} onLoadSuccess={this._onDocumentLoadSuccess} className={classes.document}>
        {[...Array(numPages).keys()].map(index => {
          const pageId = uid()
          return (
            <div id={pageId} className="pdf-page" key={index}>
              <Page className={classes.page} trim pageNumber={index + 1} onRenderSuccess={this._onRenderPage(pageId)} />
            </div>
          )
        })}
      </Document>
    )
  }
}
