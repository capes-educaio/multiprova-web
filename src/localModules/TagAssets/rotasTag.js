export const rotasTag = (usuarioId = 'ERROR_NO_USER') => {
  return {
    front: {
      gerenciamento: '/tags',
    },
    back: {
      find: `/usuarios/${usuarioId}/tags`,
      get: `/usuarios/${usuarioId}/tags`,
      count: `/usuarios/${usuarioId}/tags/count`,
      update: '/tags/:tagId',
      delete: '/tags/:tagId',
    },
  }
}
