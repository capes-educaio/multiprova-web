import { del } from 'api'
import { patch } from 'api'
import { rotasTag } from './rotasTag'
export const deleteTag = async tagId => {
  const url = rotasTag().back.delete.replace(':tagId', tagId)
  const res = await del(url)
  return res
}
export const patchTag = async (tagId, changes) => {
  const url = rotasTag().back.update.replace(':tagId', tagId)
  const res = await patch(url, changes)
  return res
}
