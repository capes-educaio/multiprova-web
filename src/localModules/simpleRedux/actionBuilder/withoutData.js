export const withoutData = (operationKey, stateKey) => {
  return () => {
    return {
      type: operationKey,
      stateKey,
    }
  }
}
