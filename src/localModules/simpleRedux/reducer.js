import { getNewState } from './getNewState'

export const reducer = (stateKey, initial = null) => {
  return (currentState, action) => {
    if (currentState === undefined) currentState = initial
    return getNewState(currentState, action, stateKey)
  }
}
