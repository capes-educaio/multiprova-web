export const operations = {
  select: {
    usesData: true,
    getNewState: data => data,
  },
  toggle: {
    usesCurrentState: true,
    getNewState: currentState => {
      if (typeof currentState !== 'boolean') throw new Error('Essa operação só é válidas para valores booleanos.')
      return !currentState
    },
  },
  update: {
    usesData: true,
    usesCurrentState: true,
    getNewState: (currentState, data) => {
      return { ...currentState, ...data }
    },
  },
  deleteProp: {
    usesData: true,
    usesCurrentState: true,
    getNewState: (currentState, data) => {
      delete currentState[data]
      return { ...currentState }
    },
  },
  push: {
    usesData: true,
    usesCurrentState: true,
    getNewState: (currentState, data) => {
      if (!Array.isArray(currentState)) throw new Error('Essa operação só é válida para arrays.')
      currentState.push(data)
      return [...currentState]
    },
  },
  updateItem: {
    usesData: true,
    usesCurrentState: true,
    getNewState: (currentState, data) => {
      if (!Array.isArray(currentState)) throw new Error('Essa operação só é válida para arrays.')
      const index = currentState.findIndex(it => {
        return it.id === data.id
      })
      currentState[index] = { ...data }
      return [...currentState]
    },
  },
  removeItem: {
    usesData: true,
    usesCurrentState: true,
    getNewState: (currentState, data) => {
      if (!Array.isArray(currentState)) throw new Error('Essa operação só é válida para arrays.')
      const index = currentState.findIndex(it => {
        return it.id === data.id
      })
      currentState.splice(index, 1)
      return [...currentState]
    },
  },
  pop: {
    usesCurrentState: true,
    getNewState: currentState => {
      if (!Array.isArray(currentState)) throw new Error('Essa operação só é válida para arrays.')
      currentState.pop()
      return [...currentState]
    },
  },
  unshift: {
    usesCurrentState: true,
    usesData: true,
    getNewState: (currentState, data) => {
      if (!Array.isArray(currentState)) throw new Error('Essa operação só é válida para arrays.')
      currentState.unshift(data)
      return [...currentState]
    },
  },
  shift: {
    usesCurrentState: true,
    getNewState: currentState => {
      if (!Array.isArray(currentState)) throw new Error('Essa operação só é válida para arrays.')
      currentState.shift()
      return [...currentState]
    },
  },
  splice: {
    usesCurrentState: true,
    usesData: true,
    getNewState: (currentState, { index, howmany = 1, element }) => {
      if (!Array.isArray(currentState)) throw new Error('Essa operação só é válida para arrays.')
      currentState.splice(index, howmany, element)
      return [...currentState]
    },
  },
}
