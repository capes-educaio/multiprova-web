import { reducer } from './reducer'

export const createSimpleReducers = states => {
  const reducers = {}
  for (let stateKey in states) {
    reducers[stateKey] = reducer(stateKey, states[stateKey])
  }
  return reducers
}
