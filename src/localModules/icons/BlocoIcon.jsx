import React from 'react'
import { withTheme } from '@material-ui/core/styles'
const BlocoIcon = props => {
  const color = props.SVGcolor || props.theme.palette.primary.main
  const svgProps = { ...props }
  delete svgProps.SVGcolor
  return (
    <svg {...svgProps} viewBox="0 0 24 24">
      <rect width="24" height="24" fill="none" />
      <rect x="4" y="13" width="16" height="5" fill={color} />
      <rect x="4" y="6" width="16" height="5" fill={color} />
    </svg>
  )
}

export default withTheme()(BlocoIcon)
