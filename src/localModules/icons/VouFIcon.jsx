import React from 'react'
import { withTheme } from '@material-ui/core/styles'

const VouFIcon = props => {
  const color = props.SVGcolor || props.theme.palette.primary.main
  const svgProps = { ...props }
  delete svgProps.SVGcolor
  return (
    <svg {...svgProps} viewBox="0 0 24 24">
      <rect width="24" height="24" fill="none" />
      <polygon points="6 11 6 12 8 12 8 13 6 13 6 14 5 14 5 10 8 10 8 11 6 11" fill={color} />
      <polygon points="9 5 7 9 6 9 4 5 5 5 6.5 8 8 5 9 5" fill={color} />
      <polygon points="9 15 7 19 6 19 4 15 5 15 6.5 18 8 15 9 15" fill={color} />
      <rect x="10" y="6" width="10" height="2" fill={color} />
      <rect x="10" y="11" width="10" height="2" fill={color} />
      <rect x="10" y="16" width="10" height="2" fill={color} />
    </svg>
  )
}
export default withTheme()(VouFIcon)
