import PropTypes from 'prop-types'

export const propTypes = {
  on: PropTypes.bool,
  // style
  classes: PropTypes.object.isRequired,
}
