import React, { Component } from 'react'

import { PinIcon } from 'localModules/icons/PinIcon'
import { PinOffIcon } from 'localModules/icons/PinOffIcon'

import { propTypes } from './propTypes'

export class PinOnOffIconComponent extends Component {
  static propTypes = propTypes

  render() {
    const { on } = this.props
    const props = { ...this.props }
    delete props.on
    if (on) return <PinIcon {...props} />
    return <PinOffIcon {...props} />
  }
}
