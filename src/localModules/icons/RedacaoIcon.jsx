import React from 'react'
import { withTheme } from '@material-ui/core/styles'

const RedacaoIcon = props => {
  const color = props.SVGcolor || props.theme.palette.primary.main
  const svgProps = { ...props }
  delete svgProps.SVGcolor
  return (
    <svg {...svgProps} viewBox="0 0 24 24">
      <path
        d="M4,2.3V21.7a.31.31,0,0,0,.3.3H19.7a.31.31,0,0,0,.3-.3V2.3a.31.31,0,0,0-.3-.3H4.3A.31.31,0,0,0,4,2.3ZM18.7,21H5.3a.31.31,0,0,1-.3-.3V3.3A.31.31,0,0,1,5.3,3H18.7a.31.31,0,0,1,.3.3V20.7A.31.31,0,0,1,18.7,21Z"
        transform="translate(0)"
        fill={color}
      />
      <rect x="8" y="5" width="8" height="1" rx="0.3" ry="0.3" fill={color} />
      <rect x="7" y="8" width="10" height="1" rx="0.3" ry="0.3" fill={color} />
      <rect x="7" y="10" width="10" height="1" rx="0.3" ry="0.3" fill={color} />
      <rect x="7" y="12" width="10" height="1" rx="0.3" ry="0.3" fill={color} />
      <rect x="7" y="14" width="10" height="1" rx="0.3" ry="0.3" fill={color} />
      <rect x="7" y="16" width="10" height="1" rx="0.3" ry="0.3" fill={color} />
      <rect x="7" y="18" width="5" height="1" rx="0.3" ry="0.3" fill={color} />
      <rect width="24" height="24" fill="none" />
    </svg>
  )
}
export default withTheme()(RedacaoIcon)
