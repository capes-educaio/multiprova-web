import React from 'react'
import { SvgIcon } from '@material-ui/core'
import { withTheme } from '@material-ui/core/styles'

const ImportIcon = props => {
  return (
    <SvgIcon {...props}>
      <svg viewBox="0 0 16 20" height={props.height || '20'} width={props.width || '20'}>
        <g>
          <path
            d="M19,8H15v2h3V20H6V10H9V8H5A1,1,0,0,0,4,9V21a1,1,0,0,0,1,1H19a1,1,0,0,0,1-1V9A1,1,0,0,0,19,8Z"
            transform="translate(-4 -2)"
          />
          <polygon points="8 14 11 10 9 10 9 0 7 0 7 10 5 10 8 14" />
        </g>
      </svg>
    </SvgIcon>
  )
}

export default withTheme()(ImportIcon)
