import React from 'react'
import { withTheme } from '@material-ui/core/styles'

const ExportIcon = props => {
  const color = props.SVGcolor || props.theme.palette.darkGray
  return (
    <svg viewBox="0 0 16 20" height={props.height || '20'} width={props.width || '20'}>
      <g>
        <path
          d="M19,8H15v2h3V20H6V10H9V8H5A1,1,0,0,0,4,9V21a1,1,0,0,0,1,1H19a1,1,0,0,0,1-1V9A1,1,0,0,0,19,8Z"
          transform="translate(-4 -2)"
          fill={color}
        />
        <polygon points="7 14 9 14 9 4 11 4 8 0 5 4 7 4 7 14" fill={color} />
      </g>
    </svg>
  )
}

export default withTheme()(ExportIcon)
