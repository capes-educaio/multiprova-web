import React from 'react'

import { withTheme } from '@material-ui/core/styles'

const CancelarIconComponent = props => {
  const { color, theme } = props
  const svgProps = { ...props }
  delete svgProps.color
  return (
    <svg enableBackground="new 0 0 24 24" version="1.0" viewBox="0 0 24 24" height="20px" width="20px" {...svgProps}>
      <g>
        <path
          fill={color}
          d="M12,4c4.4,0,8,3.6,8,8s-3.6,8-8,8s-8-3.6-8-8S7.6,4,12,4 M12,2C6.5,2,2,6.5,2,12c0,5.5,4.5,10,10,10s10-4.5,10-10   C22,6.5,17.5,2,12,2L12,2z"
        />
      </g>
      <line
        stroke={theme.palette.cinzaEscuro}
        strokeMiterlimit="10"
        strokeWidth="2"
        x1="18.2"
        x2="5.8"
        y1="18.2"
        y2="5.8"
      />
    </svg>
  )
}

export const CancelarIcon = withTheme()(CancelarIconComponent)
