import React from 'react'
import { withTheme } from '@material-ui/core/styles'

const TrianguloIcon = props => {
  const color = props.SVGcolor || props.theme.palette.steelBlue
  return (
    <svg viewBox="0 0 800 800" height={props.height || '20'} width={props.width || '15'}>
      <path fill={color} d="M0.062 128.25L383.812 512 0.062 895.75V128.25z" />
    </svg>
  )
}

export default withTheme()(TrianguloIcon)
