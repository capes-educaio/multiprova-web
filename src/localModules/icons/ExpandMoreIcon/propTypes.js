import PropTypes from 'prop-types'

export const propTypes = {
  estaExpandido: PropTypes.bool,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
