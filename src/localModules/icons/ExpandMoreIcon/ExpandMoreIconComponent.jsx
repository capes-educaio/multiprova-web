import React, { Component } from 'react'
import classnames from 'classnames'

import ExpandMoreIcon from '@material-ui/icons/ExpandMore'

import { propTypes } from './propTypes'

export class ExpandMoreIconComponent extends Component {
  static propTypes = propTypes

  render() {
    const { classes, estaExpandido, className } = this.props
    return (
      <ExpandMoreIcon
        className={classnames(classes.expand, {
          [classes.expandOpen]: estaExpandido,
          [className]: className,
        })}
      />
    )
  }
}
