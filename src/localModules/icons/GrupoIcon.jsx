import React from 'react'

export const GrupoIcon = () => (
  <svg enableBackground="new 0 0 24 24" height="19px" id="Layer_1" version="1.1" viewBox="0 0 24 24" width="19px">
    <path d="M24,1c0-0.6-0.4-1-1-1H1C0.4,0,0,0.4,0,1v22c0,0.6,0.4,1,1,1h22c0.6,0,1-0.4,1-1V1z M2,2h13v6H2V2z M2,9h6v6H2V9z M2,22v-6  h13v6H2z M22,22h-6v-6h6V22z M22,15H9V9h13V15z M22,8h-6V2h6V8z" />
  </svg>
)
