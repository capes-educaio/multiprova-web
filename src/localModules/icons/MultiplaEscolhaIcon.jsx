import React from 'react'
import { withTheme } from '@material-ui/core/styles'
const MultiplaEscolhaIcon = props => {
  const color = props.SVGcolor || props.theme.palette.primary.main
  const svgProps = { ...props }
  delete svgProps.SVGcolor
  return (
    <svg {...svgProps} viewBox="0 0 24 24">
      <rect width="24" height="24" fill="none" />
      <rect x="9" y="6" width="11" height="2" fill={color} />
      <rect x="9" y="11" width="11" height="2" fill={color} />
      <rect x="9" y="16" width="11" height="2" fill={color} />
      <path
        d="M7.41,10.59A2,2,0,0,0,4,12a2,2,0,0,0,2,2,2,2,0,0,0,1.41-3.41ZM6,13a1,1,0,1,1,1-1A1,1,0,0,1,6,13Z"
        fill={color}
      />
      <path
        d="M7.41,15.59A2,2,0,0,0,4,17a2,2,0,0,0,2,2,2,2,0,0,0,1.41-3.41ZM6,18a1,1,0,1,1,1-1A1,1,0,0,1,6,18Z"
        fill={color}
      />
      <rect x="4" y="5" width="4" height="4" rx="2" ry="2" fill={color} />
    </svg>
  )
}

export default withTheme()(MultiplaEscolhaIcon)
