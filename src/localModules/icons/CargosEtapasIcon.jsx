import React from 'react'
import { withTheme } from '@material-ui/core/styles'
export const CargosEtapasIconComponent = props => {
  const color = props.SVGcolor || props.theme.palette.gelo
  const svgProps = { ...props }
  delete svgProps.SVGcolor
  return (
    <svg width="24" height="24" viewBox="0 0 30 30" {...svgProps}>
      <path
        fill={color}
        className="cls-1"
        d="M24,10h4a2,2,0,0,0,2-2V4a2,2,0,0,0-2-2H24a2,2,0,0,0-2,2V5H19a2,2,0,0,0-2,2v8H14V12a2,2,0,0,0-2-2H4a2,2,0,0,0-2,2v8a2,2,0,0,0,2,2h8a2,2,0,0,0,2-2V17h3v8a2,2,0,0,0,2,2h3v1a2,2,0,0,0,2,2h4a2,2,0,0,0,2-2V24a2,2,0,0,0-2-2H24a2,2,0,0,0-2,2v1H19V17h3v1a2,2,0,0,0,2,2h4a2,2,0,0,0,2-2V14a2,2,0,0,0-2-2H24a2,2,0,0,0-2,2v1H19V7h3V8A2,2,0,0,0,24,10ZM12,20H4V12h8Zm12,4h4v4H24Zm0-10h4v4H24ZM24,4h4V8H24Z"
      />
    </svg>
  )
}

export const CargosEtapasIcon = withTheme()(CargosEtapasIconComponent)
