import React from 'react'
import { withTheme } from '@material-ui/core/styles'
const AssociacaoDeColunasIcon = props => {
  const color = props.SVGcolor || props.theme.palette.primary.main
  const svgProps = { ...props }
  delete svgProps.SVGcolor
  return (
    <svg {...svgProps} viewBox="0 0 24 24">
      <rect width="24" height="24" fill="none" />
      <rect x="14" y="10" width="6" height="4" fill={color} />
      <polygon points="20 4 20 8 18 8 18 6 16 6 16 8 14 8 14 4 20 4" fill={color} />
      <polygon points="10 5 10 9 8 9 8 7 6 7 6 9 4 9 4 5 10 5" fill={color} />
      <polygon points="20 16 20 20 14 20 14 16 16 16 16 18 18 18 18 16 20 16" fill={color} />
      <polygon points="10 15 10 19 4 19 4 15 6 15 6 17 8 17 8 15 10 15" fill={color} />
      <rect x="10" y="17" width="4" height="1" fill={color} />
      <rect x="10" y="6" width="4" height="1" fill={color} />
    </svg>
  )
}

export default withTheme()(AssociacaoDeColunasIcon)
