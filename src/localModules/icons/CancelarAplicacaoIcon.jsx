import React from 'react'
import { withTheme } from '@material-ui/core/styles'
export const CancelarAplicacaoIconComponent = props => {
  const color = props.SVGcolor || props.theme.palette.primary.main
  const svgProps = { ...props }
  delete svgProps.SVGcolor
  return (
    <svg width="24" height="24" viewBox="0 0 24 24" {...svgProps}>
      <path fill="none" d="M0,0H24V24H0Z" />
      <path fill={color} d="M14,2H6a1.9,1.9,0,0,0-1.34.53L14.13,12H16v1.87l4,4V8ZM13,9V3.5L18.5,9Z" />
      <path
        fill={color}
        d="M12.71,12,4.1,3.39l-1-1-.71.71L4,4.72V20a2,2,0,0,0,2,2H18a2,2,0,0,0,1.9-1.39l1,1,.71-.71ZM8,12h3.29l2,2H8Zm8,6H8V16h7.29l.71.71Z"
      />
    </svg>
  )
}

export const CancelarAplicacaoIcon = withTheme()(CancelarAplicacaoIconComponent)
