import { Entidade } from '../Entidade'
import { usuarioProps } from './usuarioProps'
import { enumRoleUsuario } from './enumRoleUsuaio'
import { enumPerfisUsuario } from './enumPerfisUsuario'

export class Usuario extends Entidade {
  constructor(data, operationType, perfil) {
    super(operationType)
    this.construirFront(usuarioProps, data)
    this.setarRoleInicial(perfil)
  }

  isAdmin() {
    this.throwIfInvalid()
    return this.roleAtual === enumRoleUsuario.ADMIN
  }

  isDocente() {
    this.throwIfInvalid()
    return this.roleAtual === enumRoleUsuario.DOCENTE
  }
  isDiscente() {
    this.throwIfInvalid()
    return this.roleAtual === enumRoleUsuario.DISCENTE
  }

  isGestor() {
    this.throwIfInvalid()
    return this.roleAtual === enumRoleUsuario.GESTOR
  }

  temMaisDeUmPerfil() {
    return this.data.permissoes.length > 1
  }

  setarComoAdmin() {
    if (!this.data.permissoes.includes(enumRoleUsuario.ADMIN)) {
      console.error('Usuario não tem permissão de ADMIN')
      return
    }
    return new Usuario(this.data, null, enumRoleUsuario.ADMIN)
  }
  setarComoGestor() {
    if (!this.data.permissoes.includes(enumRoleUsuario.GESTOR)) {
      console.error('Usuario não tem permissão de GESTOR')
      return
    }
    return new Usuario(this.data, null, enumRoleUsuario.GESTOR)
  }
  setarComoDocente() {
    if (!this.data.permissoes.includes(enumRoleUsuario.DOCENTE)) {
      console.error('Usuario não tem permissão de DOCENTE')
      return
    }
    return new Usuario(this.data, null, enumRoleUsuario.DOCENTE)
  }
  setarComoDiscente() {
    if (!this.data.permissoes.includes(enumRoleUsuario.DISCENTE)) {
      console.error('Usuario não tem permissão de DISCENTE')
      return
    }
    return new Usuario(this.data, null, enumRoleUsuario.DISCENTE)
  }

  adicionarRoleDocente() {
    if (!this.isDocente()) this.data.permissoes.push(enumRoleUsuario.DOCENTE)
    else console.error('Tentando setar como docente usuário que já é docente.')
  }

  adicionarRoleAdmin() {
    if (!this.isAdmin()) this.data.permissoes.push(enumRoleUsuario.ADMIN)
    else console.error('Tentando setar como admin usuário que já é admin.')
  }
  adicionarRoleDiscente() {
    if (!this.isDiscente()) this.data.permissoes.push(enumRoleUsuario.DISCENTE)
    else console.error('Tentando setar como discente usuário que já é discente.')
  }

  setarRoleInicial(role) {
    this.roleAtual = role ? role : this.getRoleAutomatica()
  }

  getRoleAutomatica() {
    if (this.data.permissoes.includes(enumRoleUsuario.ADMIN)) return enumRoleUsuario.ADMIN
    else if (this.data.permissoes.includes(enumRoleUsuario.GESTOR)) return enumRoleUsuario.GESTOR
    else if (this.data.permissoes.includes(enumRoleUsuario.DOCENTE)) return enumRoleUsuario.DOCENTE
    else if (this.data.permissoes.includes(enumRoleUsuario.DISCENTE)) return enumRoleUsuario.DISCENTE
    else console.error('usuario nao tem nenhuma role reconhecida.')
  }
  getPerfisList() {
    const roles = this.data.permissoes
    return roles.map(role => this.mapRoleToPerfil(role))
  }

  mapRoleToPerfil(role) {
    if (typeof role !== 'number') {
      console.error('role deve ser um numero')
    }
    const perfilKey = Object.keys(enumRoleUsuario).find(key => enumRoleUsuario[key] === role)
    if (!perfilKey) {
      console.error('não achou um perfil a partir da role.')
    }
    return enumPerfisUsuario[perfilKey]
  }

  setarPerfilAtual(perfil) {
    switch (perfil) {
      case enumPerfisUsuario.ADMIN:
        return this.setarComoAdmin()
      case enumPerfisUsuario.GESTOR:
        return this.setarComoGestor()
      case enumPerfisUsuario.DOCENTE:
        return this.setarComoDocente()
      case enumPerfisUsuario.DISCENTE:
        return this.setarComoDiscente()
      default:
        console.error('perfil nao reconhecido')
        return new Usuario(this.data)
    }
  }
}
