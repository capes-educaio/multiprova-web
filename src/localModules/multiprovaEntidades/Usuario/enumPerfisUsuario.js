import { ptBr } from 'utils/strings/ptBr'

export const enumPerfisUsuario = {
  ADMIN: ptBr.admin,
  GESTOR: ptBr.gestor,
  DOCENTE: ptBr.docente,
  DISCENTE: ptBr.discente,
}
