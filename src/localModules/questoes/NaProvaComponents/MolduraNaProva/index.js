import { compose } from 'redux'
import { connect } from 'react-redux'
// import { withRouter } from 'react-router'

import { withStyles } from '@material-ui/core/styles'

import { mapStateToProps, mapDispatchToProps } from './redux'
import { MolduraNaProvaComponent } from './MolduraNaProvaComponent'
import { style } from './style'

export const MolduraNaProva = compose(
  // withRouter,
  withStyles(style),
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(MolduraNaProvaComponent)
