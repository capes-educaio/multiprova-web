import PropTypes from 'prop-types'

const { node, string, shape, object } = PropTypes

export const propTypes = {
  children: node,
  expandedContent: node,
  bottomContent: node,
  classNames: shape({ root: string, main: string, barraExpandir: string }),
  // style
  classes: object.isRequired,
}
