export const style = theme => {
  return {
    root: {
      boxShadow: 'none',
      borderStyle: 'solid',
      borderWidth: '5px 0 0 0',
      borderColor: theme.palette.fundo.cinza,
      display: 'flex',
      width: '100%',
    },
    main: {
      display: 'flex',
      flexDirection: 'column',
      width: '100%',
      padding: '10px 0 15px 0',
    },
    barraExpandir: {
      cursor: 'pointer',
      width: 6,
      '&:hover': {
        filter: 'brightness(85%)',
      },
    },
    bottom: { display: 'flex', marginTop: 6, justifyContent: 'flex-end', paddingLeft: 8, paddingRight: 8 },
  }
}
