import React, { Component } from 'react'
import classnames from 'classnames'

import { propTypes } from './propTypes'
import { defaultProps } from './defaultProps'
import { Paper, Collapse } from '@material-ui/core'

export class MolduraNaProvaComponent extends Component {
  static propTypes = propTypes
  static defaultProps = defaultProps

  render() {
    const {
      children,
      classNames,
      expandedContent,
      classes,
      estaExpandido,
      handleExpandirEContrair,
      bottomContent,
    } = this.props
    return (
      <Paper className={classnames(classes.root, { [classNames.root]: classNames.root })}>
        <div
          className={classnames(classes.barraExpandir, { [classNames.barraExpandir]: classNames.barraExpandir })}
          onClick={handleExpandirEContrair}
        />
        <div className={classnames(classes.main, { [classNames.main]: classNames.main })}>
          <div className={classnames({ [classNames.contraidoContent]: classNames.contraidoContent })}>{children}</div>
          {expandedContent && (
            <Collapse in={estaExpandido} timeout="auto">
              <div
                className={classnames(classes.expandedContent, {
                  [classNames.expandedContent]: classNames.expandedContent,
                })}
              >
                {expandedContent}
              </div>
            </Collapse>
          )}
          {!!bottomContent && <div className={classes.bottom}>{bottomContent}</div>}
        </div>
      </Paper>
    )
  }
}
