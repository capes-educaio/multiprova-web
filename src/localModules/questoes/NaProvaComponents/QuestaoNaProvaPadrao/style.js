export const style = theme => ({
  buttonGroupInNotTouch: {
    opacity: '0',
    transition: '0.3s',
    alignItems: 'right',
  },
  mostrarAcoesOnHover: {
    '&:hover $buttonGroupInNotTouch': {
      opacity: '1',
      maxWidth: '100%',
    },
  },
  '@media (pointer: coarse)': {
    buttonGroupInNotTouch: {
      opacity: '1',
      maxWidth: '100%',
    },
  },
  buttonGroupInNotTouchFixa: {
    maxWidth: '34px',
    opacity: '1',
  },
  barraExpandirQuestaoDeBloco: {
    width: 0,
  },
  moduraQuestaoDeBloco: {
    width: '100%',
    borderStyle: 'dotted',
    borderWidth: '1px 0 0 0',
    borderColor: theme.palette.common.black,
  },
  expandedContent: {
    padding: '10px 30px 0 30px',
  },
  expandedContentBloco: {
    wordWrap: 'break-word',
    display: 'flex',
    width: '100%',
    flexWrap: 'wrap',
  },
  mainBloco: {
    paddingBottom: 0,
  },
  fade: {
    maxHeight: '200px',
    overflow: 'hidden',
    position: 'relative',
    '&:after': {
      content: '""',
      position: 'absolute',
      zIndex: 1,
      top: '160px',
      right: '0',
      width: '100%',
      height: '40px',
      background: `-webkit-linear-gradient(transparent, ${theme.palette.gelo})`,
    },
  },
  flexCenter: { display: 'flex', alignItems: 'center' },
})
