import PropTypes from 'prop-types'

const { shape, string, oneOf } = PropTypes

export const propTypes = {
  questaoProcessada: PropTypes.object.isRequired,
  conteudoContraido: PropTypes.node,
  conteudoExpandido: PropTypes.node,
  actions: PropTypes.node,
  iconDisplay: PropTypes.node,
  classNames: shape({ barraExpandir: string.isRequired }).isRequired,
  dragHandleProps: PropTypes.object,
  isQuestaoDeBloco: PropTypes.bool,
  variant: oneOf(['view', 'edit']),
  // redux state
  mostrarPin: PropTypes.bool.isRequired,
  prova: PropTypes.object.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
