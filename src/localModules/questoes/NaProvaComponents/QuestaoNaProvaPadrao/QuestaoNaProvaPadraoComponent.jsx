import React, { Component } from 'react'
import classnames from 'classnames'
import { Typography, TextField } from '@material-ui/core'

import { isTouchDevice } from 'utils/isTouchDevice'
import { enumTipoSistemaDeNotasDeProva } from 'utils/enumTipoSistemaDeNotasDeProva'
import { enumTipoProva } from 'utils/enumTipoProva'
import { enumTipoQuestao } from 'utils/enumTipoQuestao'

import { HeaderNaProva } from '../HeaderNaProva'
import { MolduraNaProva } from '../MolduraNaProva'
import { ActionsNaProva } from '../ActionsNaProva'
import { TopNaProva } from '../TopNaProva'
import { ConteudoContraidoNaProva } from '../ConteudoContraidoNaProva'
import { arredondar } from 'utils/arredondar.js'

import { propTypes } from './propTypes'

export class QuestaoNaProvaPadraoComponent extends Component {
  static propTypes = propTypes
  get _isView() {
    return this.props.variant === 'view'
  }

  somaPesoQuestoes = (prev, current) => {
    if (current.tipo === enumTipoQuestao.tipoQuestaoBloco) {
      const { questoes = [] } = current.bloco
      return prev + questoes.reduce(this.somaPesoQuestoes, 0)
    }
    const { peso = 1 } = current
    return prev + Number(peso)
  }
  componentWillMount() {
    const { prova, questaoProcessada } = this.props
    if (prova.sistemaDeNotasDaProva === enumTipoSistemaDeNotasDeProva.valorEmQuestoes) {
      if (
        (questaoProcessada.changeValor && questaoProcessada.valor === undefined) ||
        questaoProcessada.valor === null
      ) {
        if (questaoProcessada.changeValor != null) questaoProcessada.changeValor(1)
      }
    }
  }

  renderValorOuPesoQuestao = () => {
    const { prova, questaoProcessada, strings, classes } = this.props

    if (questaoProcessada.tipo === enumTipoQuestao.tipoQuestaoBloco || prova.tipoProva === enumTipoProva.dinamica)
      return false

    if (prova.sistemaDeNotasDaProva === enumTipoSistemaDeNotasDeProva.valorEmProvaEPesosEmQuestoes) {
      const { grupos = [] } = prova
      if (!prova.valor) prova.valor = 10
      const todasAsQuestoes = grupos.reduce((prev, current) => [...prev, ...current.questoes], [])
      const somaPesos = todasAsQuestoes.reduce(this.somaPesoQuestoes, 0)
      const { peso = 1 } = questaoProcessada
      return (
        <Typography>
          {strings.valorDaQuestao}: {arredondar(prova.valor * (peso / somaPesos), 2).replace('.', ',')}
        </Typography>
      )
    }
    if (prova.sistemaDeNotasDaProva === enumTipoSistemaDeNotasDeProva.valorEmQuestoes) {
      if (this._isView)
        return (
          <Typography>
            {strings.valorDaQuestao}: {arredondar(questaoProcessada.valor, 2).replace('.', ',')}
          </Typography>
        )
      else
        return (
          <div className={classes.flexCenter}>
            <Typography>{strings.valorDaQuestao}: </Typography>
            <TextField
              onBlur={({ target: { value } }) => {
                if (!value || value < 0) questaoProcessada.changeValor(0)
              }}
              style={{ width: 70 }}
              value={questaoProcessada.valor}
              onChange={({ target: { value } }) => {
                questaoProcessada.changeValor(value)
              }}
              inputProps={{ min: 0, step: 0.01, style: { textAlign: 'center' } }}
              type="number"
            />
          </div>
        )
    }
    return false
  }
  render() {
    const {
      questaoProcessada,
      classes,
      dragHandleProps,
      conteudoExpandido,
      classNames,
      estaExpandido,
      variant,
    } = this.props
    const { conteudoContraido, mostrarPin, actions, handleExpandirEContrair } = this.props
    const { iconDisplay } = this.props
    const isTouch = isTouchDevice()
    const { fixa, isQuestaoDeBloco } = questaoProcessada
    const fixaPrimeiraAction = !isTouch && fixa && mostrarPin
    let molduraClassNames
    if (questaoProcessada.tipo === enumTipoQuestao.tipoQuestaoBloco)
      molduraClassNames = {
        barraExpandir: classNames.barraExpandir,
        contraidoContent: classes.mostrarAcoesOnHover,
        expandedContent: classes.expandedContentBloco,
        main: classes.mainBloco,
      }
    else if (isQuestaoDeBloco)
      molduraClassNames = {
        root: classes.moduraQuestaoDeBloco,
        barraExpandir: classes.barraExpandirQuestaoDeBloco,
        main: classes.mostrarAcoesOnHover,
        expandedContent: classes.expandedContent,
      }
    else
      molduraClassNames = {
        barraExpandir: classNames.barraExpandir,
        main: classes.mostrarAcoesOnHover,
        expandedContent: classes.expandedContent,
      }

    return (
      <MolduraNaProva
        onRef={ref => (this._molduraRef = ref)}
        classNames={molduraClassNames}
        expandedContent={conteudoExpandido}
        handleExpandirEContrair={handleExpandirEContrair}
        estaExpandido={estaExpandido}
        bottomContent={this.renderValorOuPesoQuestao()}
      >
        <TopNaProva>
          <HeaderNaProva variant={variant} dragHandleProps={dragHandleProps} questaoProcessada={questaoProcessada} />
          <ActionsNaProva
            className={classnames({
              [classes.buttonGroupInNotTouch]: !isTouch,
              [classes.buttonGroupInNotTouchFixa]: fixaPrimeiraAction,
            })}
            iconDisplay={iconDisplay}
          >
            {actions}
          </ActionsNaProva>
        </TopNaProva>
        <div className={classnames({ [classes.fade]: !estaExpandido })}>
          <ConteudoContraidoNaProva>{conteudoContraido}</ConteudoContraidoNaProva>
        </div>
      </MolduraNaProva>
    )
  }
}
