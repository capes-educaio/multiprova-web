import { compose } from 'redux'
import { withStyles } from '@material-ui/core/styles'

import { PesoQuestaoComponent } from './PesoQuestaoComponent'
import { style } from './style'

export const PesoQuestao = compose(withStyles(style))(PesoQuestaoComponent)
