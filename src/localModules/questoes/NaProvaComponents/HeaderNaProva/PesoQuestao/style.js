export const style = theme => ({
  pesoContainer: {
    display: 'flex',
    alignItems: 'center',
    marginLeft: 10,
    borderRadius: 3,
    border: `1px solid ${theme.palette.primary.main}`,
  },
  peso: {
    fontFamily: theme.typography.fontFamily,
    paddingLeft: 6,
    paddingRight: 6,
  },
  arrow: {
    backgroundColor: theme.palette.primary.main,
  },
  disabledArrow: { backgroundColor: theme.palette.cinzaEscuro },
  arrowIcon: { fill: 'white' },
})
