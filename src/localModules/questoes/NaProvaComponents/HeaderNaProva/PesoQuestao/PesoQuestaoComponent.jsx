import React from 'react'
import { ButtonBase } from '@material-ui/core'
import AddIcon from '@material-ui/icons/Add'
import RemoveIcon from '@material-ui/icons/Remove'
import { propTypes } from './propTypes'
import classnames from 'classnames'

export const PesoQuestaoComponent = ({ classes, value, step = 1, max, min, onChange }) => {
  const validNewValue = newValue => {
    if (max && newValue > max) return max
    if (min && newValue < min) return min
    return newValue
  }
  const disableLeft = min && value === min
  const disableRight = max && value === max
  return (
    <div className={classes.pesoContainer}>
      <ButtonBase
        disabled={disableLeft}
        onClick={() => onChange(validNewValue(value - step))}
        className={classnames(classes.arrow, disableLeft && classes.disabledArrow)}
      >
        <RemoveIcon fontSize="small" className={classes.arrowIcon} />
      </ButtonBase>
      <div className={classes.peso}>{value}</div>
      <ButtonBase
        disabled={disableRight}
        onClick={() => onChange(validNewValue(value + step))}
        className={classnames(classes.arrow, disableRight && classes.disabledArrow)}
      >
        <AddIcon fontSize="small" className={classes.arrowIcon} />
      </ButtonBase>
    </div>
  )
}
PesoQuestaoComponent.propTypes = propTypes
