import Proptypes from 'prop-types'

export const propTypes = {
  onChange: Proptypes.func.isRequired,
  value: Proptypes.number.isRequired,
  step: Proptypes.number,
  max: Proptypes.number,
  min: Proptypes.number,
  classes: Proptypes.object.isRequired,
}
