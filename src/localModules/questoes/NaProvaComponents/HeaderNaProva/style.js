export const style = theme => ({
  root: {
    flexGrow: 1,
    display: 'flex',
    alignItems: 'center',
  },
  noDragIcon: {
    marginLeft: 30,
  },
  dragIcon: {
    padding: '0 3px',
    fill: theme.palette.cinzaDusty,
  },
  cardHeader: {
    padding: 0,
  },
  content: {
    display: 'flex',
    alignItems: 'center',
  },
  subHeader: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  dificuldade: {
    padding: '3px 3px 0 10px',
  },
  peso: {
    padding: '0 3px 3px 0',
  },
  textoBloco: {
    paddingLeft: '10px',
    paddingRight: '10px',
    fontWeight: 'bold',
  },
  textoQuestoes: {
    paddingLeft: '10px',
    fontSize: 'smaller',
  },
  subHeaderQuestao: {
    display: 'flex',
    justifyContent: 'space-between',
    fontWeight: 'bold',
  },
  validada: {
    margin: 3,
    color: theme.palette.success.dark,
  },
  naoValidada: {
    margin: 3,
    color: theme.palette.warning.dark,
  },
})
