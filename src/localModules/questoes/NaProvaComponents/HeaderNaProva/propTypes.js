import Proptypes from 'prop-types'

export const propTypes = {
  questaoProcessada: Proptypes.object.isRequired,
  opcoes: Proptypes.object,
  prova: Proptypes.object.isRequired,
  mostrarPin: Proptypes.bool,
  variant: Proptypes.oneOf(['view', 'edit']),
  // strings
  strings: Proptypes.object.isRequired,
  // style
  classes: Proptypes.object.isRequired,
}
