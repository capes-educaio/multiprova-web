import React, { Component } from 'react'
import classnames from 'classnames'

import { Typography, Tooltip } from '@material-ui/core'
import { DragIndicator, Check, Clear } from '@material-ui/icons'

import { GrupoIcon } from 'localModules/icons'

import { enumTipoProva } from 'utils/enumTipoProva'
import { enumTipoQuestao } from 'utils/enumTipoQuestao'
import { enumTipoSistemaDeNotasDeProva } from 'utils/enumTipoSistemaDeNotasDeProva'
import { enumSeloQuestao } from 'utils/enumSeloQuestao'
import { getDificuldadeProps } from 'utils/getDificuldadeProps'

import { propTypes } from './propTypes'
import { PesoQuestao } from './PesoQuestao'

export class HeaderNaProvaComponent extends Component {
  static propTypes = propTypes

  constructor(props) {
    super(props)
    this.state = {
      dificuldadeTextoVisivel: false,
      open: false,
    }
  }
  _handleTooltipClose = () => {
    this.setState({ open: false })
  }

  _handleTooltipOpen = () => {
    this.setState({ open: true })
  }

  _getHeaderQuestao = () => {
    const { questaoProcessada, strings, classes } = this.props
    return <div className={classes.subHeaderQuestao}>{`${strings.questao} ${questaoProcessada.numeroNaProva}`}</div>
  }

  _getHeaderBloco = () => {
    const { questaoProcessada, strings, classes } = this.props
    const { numeroNaProva, numeroDoBloco } = questaoProcessada
    const { questoes } = questaoProcessada.bloco
    const tamanho = questoes.length
    return (
      <div className={classes.subHeader}>
        <GrupoIcon />
        <div className={classes.textoBloco}>
          {strings.getBlocoNumero({
            numero: numeroDoBloco,
          })}
        </div>
        {'-'}
        <div className={classes.textoQuestoes}>
          {strings.getIntervaloDeQuestoes({
            inicial: numeroNaProva,
            final: numeroNaProva + tamanho - 1,
          })}
        </div>
      </div>
    )
  }

  _getHeaderCriterio = () => {
    const { questaoProcessada, strings, classes } = this.props
    const { numPrimeiraQuestao, numUltimaQuestao } = questaoProcessada
    return (
      <div className={classes.subHeaderQuestao}>
        {strings.getIntervaloDeQuestoes({
          inicial: numPrimeiraQuestao,
          final: numUltimaQuestao,
        })}
      </div>
    )
  }

  _getHeader = () => {
    const { questaoProcessada } = this.props
    switch (questaoProcessada.tipo) {
      case enumTipoQuestao.tipoQuestaoBloco:
        return this._getHeaderBloco()
      case enumTipoQuestao.tipoQuestaoMultiplaEscolha:
        return this._getHeaderQuestao()
      case enumTipoQuestao.tipoQuestaoVerdadeiroOuFalso:
        return this._getHeaderQuestao()
      case enumTipoQuestao.tipoQuestaoDiscursiva:
        return this._getHeaderQuestao()
      case enumTipoQuestao.tipoQuestaoAssociacaoDeColunas:
        return this._getHeaderQuestao()
      case enumTipoQuestao.tipoQuestaoRedacao:
        return this._getHeaderQuestao()
      case 'criterio':
        return this._getHeaderCriterio()
      default:
        console.error('_getHeader, tipo de questão não identificado: ' + questaoProcessada.tipo)
        return ''
    }
  }

  render() {
    const { questaoProcessada, classes, strings, dragHandleProps, prova, variant } = this.props
    const { dificuldade, changePeso, selo } = questaoProcessada
    const { dificuldadeString, imagem } = getDificuldadeProps(dificuldade, strings)
    const subheader = this._getHeader()
    const isDragHandle = Boolean(dragHandleProps)

    const peso = questaoProcessada.peso !== undefined ? questaoProcessada.peso : 1
    return (
      <div className={classnames(classes.root, { [classes.noDragIcon]: !dragHandleProps })} {...dragHandleProps}>
        {isDragHandle && <DragIndicator className={classes.dragIcon} />}
        <div className={classes.content}>
          <Typography className={classes.cardHeader} variant="caption">
            {subheader}
          </Typography>
          {dificuldade && (
            <div
              className={classes.dificuldade}
              onClick={this._handleTooltipOpen}
              onMouseOver={this._handleTooltipOpen}
            >
              <Tooltip title={dificuldadeString} open={this.state.open} onClose={this._handleTooltipClose}>
                <img src={imagem} alt="" />
              </Tooltip>
            </div>
          )}
          {prova.tipoProva === enumTipoProva.convencional &&
            variant !== 'view' &&
            (selo && selo === enumSeloQuestao.validada ? (
              <div className={classes.validada}>
                <Tooltip title={strings.validada}>
                  <Check />
                </Tooltip>
              </div>
            ) : (
              <div className={classes.naoValidada}>
                <Tooltip title={strings.naoValidada}>
                  <Clear />
                </Tooltip>
              </div>
            ))}
          {variant !== 'view' &&
            questaoProcessada.tipo !== enumTipoQuestao.tipoQuestaoBloco &&
            prova.tipoProva === enumTipoProva.convencional &&
            prova.sistemaDeNotasDaProva === enumTipoSistemaDeNotasDeProva.valorEmProvaEPesosEmQuestoes && (
            <div className={classes.peso}>
              <PesoQuestao onChange={changePeso} value={peso} step={1} max={10} min={1} />
            </div>
          )}
        </div>
      </div>
    )
  }
}
