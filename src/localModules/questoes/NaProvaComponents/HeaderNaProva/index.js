import { compose } from 'redux'
import { withStyles } from '@material-ui/core/styles'
import { connect } from 'react-redux'

import { mapStateToProps, mapDispatchToProps } from './redux'
import { HeaderNaProvaComponent } from './HeaderNaProvaComponent'
import { style } from './style'

export const HeaderNaProva = compose(
  withStyles(style),
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(HeaderNaProvaComponent)
