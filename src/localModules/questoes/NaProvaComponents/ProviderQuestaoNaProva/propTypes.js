import Proptypes from 'prop-types'

export const propTypes = {
  children: Proptypes.func.isRequired,
  questaoProcessada: Proptypes.object.isRequired,
  // style
  classes: Proptypes.object.isRequired,
}
