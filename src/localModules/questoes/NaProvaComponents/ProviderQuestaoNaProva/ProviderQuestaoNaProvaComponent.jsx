import { Component } from 'react'

import { enumTipoQuestao } from 'utils/enumTipoQuestao'

import { propTypes } from './propTypes'

export class ProviderQuestaoNaProvaComponent extends Component {
  static propTypes = propTypes

  _isBloco = false

  constructor(props) {
    super(props)
    const { questaoProcessada } = props
    if (questaoProcessada.tipo === enumTipoQuestao.tipoQuestaoBloco) this._isBloco = true
    if (questaoProcessada.onRef) questaoProcessada.onRef(this)
    this.state = {
      estaExpandido: true,
      questoesDoBlocoRefs: {},
    }
  }

  _setQuestaoDoBlocoRefs = id => questaoRef => {
    const { questoesDoBlocoRefs } = this.state
    if (questaoRef) questoesDoBlocoRefs[id] = questaoRef
    else delete questoesDoBlocoRefs[id]
    this.setState({ questoesDoBlocoRefs })
  }

  componentWillUnmount = () => {
    const { questaoProcessada } = this.props
    if (questaoProcessada.onRef) questaoProcessada.onRef(null)
  }

  expandir = () => {
    if (this._isBloco) this._expandirTodasQuestoesDoBloco()
    this.setState({ estaExpandido: true })
  }

  contrair = () => {
    if (this._isBloco) this._contrairTodasQuestoesDoBloco()
    this.setState({ estaExpandido: false })
  }

  _getQuestoesDoBloco = () => {
    const { questaoProcessada } = this.props
    const { numeroNaProva } = questaoProcessada
    const { questoes } = questaoProcessada.bloco
    let numeroDaQuestaoNaProva = numeroNaProva
    return questoes.map(questao => {
      const questaoNoBlocoProcessada = { ...questao }
      questaoNoBlocoProcessada.numeroNaProva = numeroDaQuestaoNaProva
      questaoNoBlocoProcessada.onRef = this._setQuestaoDoBlocoRefs(questao.id)
      questaoNoBlocoProcessada.isQuestaoDeBloco = true
      numeroDaQuestaoNaProva++
      return questaoNoBlocoProcessada
    })
  }

  _handleExpandirEContrair = () => this.setState({ estaExpandido: !this.state.estaExpandido })

  _expandirTodasQuestoesDoBloco = () => {
    const { questoesDoBlocoRefs } = this.state
    for (let questaoId in questoesDoBlocoRefs) {
      const questaoRef = questoesDoBlocoRefs[questaoId]
      questaoRef.expandir()
    }
  }

  _contrairTodasQuestoesDoBloco = () => {
    const { questoesDoBlocoRefs } = this.state
    for (let questaoId in questoesDoBlocoRefs) {
      const questaoRef = questoesDoBlocoRefs[questaoId]
      questaoRef.contrair()
    }
  }

  render() {
    const { children } = this.props
    const { estaExpandido } = this.state
    return children({
      estaExpandido,
      questoesDoBloco: this._isBloco ? this._getQuestoesDoBloco() : null,
      expandir: this._expandir,
      contrair: this._contrair,
      handleExpandirEContrair: this._handleExpandirEContrair,
    })
  }
}
