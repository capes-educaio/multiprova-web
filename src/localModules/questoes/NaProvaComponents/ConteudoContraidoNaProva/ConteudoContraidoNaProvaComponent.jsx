import React, { Component } from 'react'
import classnames from 'classnames'

import { propTypes } from './propTypes'

export class ConteudoContraidoNaProvaComponent extends Component {
  static propTypes = propTypes

  render() {
    const { children, classes, className } = this.props
    return <div className={classnames(classes.root, { [className]: className })}>{children}</div>
  }
}
