import React, { Component } from 'react'
import classnames from 'classnames'
import { MpButtonGroup } from 'common/MpButtonGroup'

import { propTypes } from './propTypes'
import { defaultProps } from './defaultProps'

export class ActionsNaProvaComponent extends Component {
  static propTypes = propTypes
  static defaultProps = defaultProps

  render() {
    const { classes, className, children, iconDisplay } = this.props
    if (iconDisplay)
      return (
        <div
          className={classnames(classes.root, {
            [className]: className,
          })}
        >
          {iconDisplay}
          <MpButtonGroup>{children}</MpButtonGroup>
        </div>
      )
    return (
      <MpButtonGroup
        className={classnames(classes.root, {
          [className]: className,
        })}
      >
        {children}
      </MpButtonGroup>
    )
  }
}
