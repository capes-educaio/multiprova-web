import React, { Component } from 'react'

import { QuestaoNaProvaPadrao, ProviderActionsComunsNaProva, ProviderQuestaoNaProva } from '../../NaProvaComponents'

import { propTypes } from './propTypes'

export class QuestaoSingularNaProvaComponent extends Component {
  static propTypes = propTypes

  get _isEdit() {
    return this.props.variant === 'edit'
  }

  get _isView() {
    return this.props.variant === 'view'
  }

  render() {
    const { classNames, conteudoContraido, conteudoExpandido, mostrarPin, variant } = this.props
    const { questaoProcessada, dragHandleProps, disableExpandir } = this.props
    return (
      <ProviderQuestaoNaProva questaoProcessada={questaoProcessada}>
        {({ estaExpandido, handleExpandirEContrair }) => {
          return (
            <ProviderActionsComunsNaProva
              onClick={{
                pinButton: questaoProcessada.toggleFixagem,
                expandirContrairButton: handleExpandirEContrair,
              }}
              estaExpandido={estaExpandido}
              questaoProcessada={questaoProcessada}
            >
              {({ editarButton, expandirContrairButton, excluirButton, addButton, pinButton, pinDisplay }) => {
                let actions
                if (this._isEdit) {
                  actions = []
                  if (!disableExpandir) actions.push(expandirContrairButton)
                  if (!questaoProcessada.isQuestaoDeBloco) actions.unshift(editarButton, addButton, excluirButton)
                  if (mostrarPin) actions.unshift(pinButton)
                }
                if (this._isView && !disableExpandir) actions = expandirContrairButton
                return (
                  <QuestaoNaProvaPadrao
                    variant={variant}
                    dragHandleProps={dragHandleProps}
                    questaoProcessada={questaoProcessada}
                    classNames={{ barraExpandir: classNames.barraExpandir }}
                    handleExpandirEContrair={handleExpandirEContrair}
                    estaExpandido={estaExpandido}
                    conteudoContraido={conteudoContraido}
                    actions={actions}
                    iconDisplay={this._isView && mostrarPin && questaoProcessada.fixa ? pinDisplay : null}
                    conteudoExpandido={conteudoExpandido}
                  />
                )
              }}
            </ProviderActionsComunsNaProva>
          )
        }}
      </ProviderQuestaoNaProva>
    )
  }
}
