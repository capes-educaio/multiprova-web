import PropTypes from 'prop-types'

const { shape, string, node, oneOf, bool } = PropTypes

export const propTypes = {
  questaoProcessada: PropTypes.object.isRequired,
  dragHandleProps: PropTypes.object,
  classNames: shape({ barraExpandir: string.isRequired }).isRequired,
  conteudoContraido: node,
  conteudoExpandido: node,
  variant: oneOf(['view', 'edit']),
  disableExpandir: bool,
  // redux state
  mostrarPin: PropTypes.bool.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
