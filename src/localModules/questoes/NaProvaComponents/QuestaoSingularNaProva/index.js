import { compose } from 'redux'
import { connect } from 'react-redux'
// import { withRouter } from 'react-router'

import { withStyles } from '@material-ui/core/styles'

import { mapStateToProps, mapDispatchToProps } from './redux'
import { style } from './style'
import { QuestaoSingularNaProvaComponent } from './QuestaoSingularNaProvaComponent'

export const QuestaoSingularNaProva = compose(
  // withRouter,
  withStyles(style),
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(QuestaoSingularNaProvaComponent)
