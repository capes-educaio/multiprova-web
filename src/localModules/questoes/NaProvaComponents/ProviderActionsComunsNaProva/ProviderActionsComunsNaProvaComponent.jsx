import React, { Component } from 'react'
import uid from 'uuid/v4'

import Add from '@material-ui/icons/Add'
import EditIcon from '@material-ui/icons/Edit'

import { updateUrlState } from 'localModules/urlState'
import { PinOnOffIcon } from 'localModules/icons/PinOnOffIcon'
import { ExpandMoreIcon } from 'localModules/icons/ExpandMoreIcon'
import { provaUtils } from 'localModules/provas/ProvaUtils'

import { BotaoExcluir } from 'common/BotaoExcluir'
import { MpIconButton } from 'common/MpIconButton'

import { propTypes } from './propTypes'
import { defaultProps } from './defaultProps'

export class ProviderActionsComunsNaProvaComponent extends Component {
  static propTypes = propTypes
  static defaultProps = defaultProps
  _id = uid()

  _openAddDialog = () => {
    const { questaoProcessada, value } = this.props
    const { indexDaQuestao, grupoIndex } = provaUtils.getQuestaoIndex({ value, questaoId: questaoProcessada.id })
    updateUrlState({
      hidden: {
        isOpenModal: true,
        indexQuestaoTriggerAdd: indexDaQuestao,
        indexGrupoTriggerAdd: grupoIndex,
        addWasTriggeredPorUmaQuestao: true,
      },
    })
  }

  _openAddDialogCriterio = () => {
    const { questaoProcessada, value } = this.props
    const indexDaQuestao = provaUtils.getCriterioQuestaoIndex({ prova: value, questao: questaoProcessada })
    updateUrlState({
      hidden: {
        isOpenModal: true,
        modalType: 'selecaoCriterio',
        indexQuestaoTriggerAdd: indexDaQuestao,
        indexGrupoTriggerAdd: 0,
        addWasTriggeredPorUmaQuestao: true,
      },
    })
  }

  _editCriterio = isToUpdateCriterio => {
    const { questaoProcessada, value } = this.props
    const indexDaQuestao = provaUtils.getCriterioQuestaoIndex({ prova: value, questao: questaoProcessada })
    updateUrlState({
      hidden: {
        isOpenModal: true,
        modalType: 'selecaoCriterio',
        indexQuestaoTriggerAdd: indexDaQuestao,
        indexGrupoTriggerAdd: 0,
        addWasTriggeredPorUmaQuestao: true,
        isToUpdateCriterio,
      },
    })
  }

  _excluirQuestaoDaProva = questao => () => {
    const { value, updateProvaValue } = this.props
    if (provaUtils.isProvaDinamica) updateProvaValue(provaUtils.excluirCriterioDaProva({ prova: value, questao }))
    else updateProvaValue(provaUtils.excluirQuestaoDaProva({ prova: value, questao }))
  }
  get _editarQuestaoDaProva() {
    const { location, value, selectValorProvaNaVolta } = this.props
    return provaUtils.irParaEditarQuestao({ location, selectValorProvaNaVolta, value })
  }

  render() {
    const { strings, onClick, questaoProcessada, estaExpandido, tooltips, stringExcluir } = this.props
    const { fixa } = questaoProcessada
    const { classes, children } = this.props
    const pinDisplay = (
      <span className={classes.icon} key={'pinDisplay' + this._id}>
        <PinOnOffIcon on={fixa} />
      </span>
    )
    const pinButton = (
      <MpIconButton
        key={'pinButton' + this._id}
        tooltip={tooltips.pinButton ? tooltips.pinButton : strings.provaFixarQuestao}
        color="fundoCinza"
        onClick={onClick.pinButton}
        IconComponent={PinOnOffIcon}
        iconProps={{ on: fixa }}
      />
    )
    const addButton = (
      <MpIconButton
        key={'addButton' + this._id}
        tooltip={
          tooltips.addButton
            ? tooltips.addButton
            : provaUtils.isProvaDinamica
              ? strings.adicionarCriterioAbaixo
              : strings.provaAddQuestaoAbaixo
        }
        onClick={provaUtils.isProvaDinamica ? this._openAddDialogCriterio : this._openAddDialog}
        IconComponent={Add}
        color="fundoCinza"
      />
    )
    const stringDesejaExcluir = provaUtils.isProvaDinamica
      ? strings.desejaExcluirCriterio
      : strings.desejaExcluirQuestaoDaProva
    const excluirButton = (
      <BotaoExcluir
        key={'excluirButton' + this._id}
        variant="mp"
        instancia={questaoProcessada}
        excluir={this._excluirQuestaoDaProva}
        stringExcluir={stringExcluir ? stringExcluir : stringDesejaExcluir}
        buttonProps={{
          tooltip: tooltips.excluirButton
            ? tooltips.excluirButton
            : provaUtils.isProvaDinamica
              ? strings.provaExcluirCriterio
              : strings.provaExcluirQuestao,
        }}
      />
    )
    const expandirContrairButton = (
      <MpIconButton
        key={'expandirContrairButton' + this._id}
        tooltip={estaExpandido ? strings.contrair : strings.expandir}
        onClick={onClick.expandirContrairButton}
        IconComponent={ExpandMoreIcon}
        iconProps={{ estaExpandido }}
        color="fundoCinza"
      />
    )
    const editarButton = (
      <MpIconButton
        key={'editarButton' + this._id}
        tooltip={provaUtils.isProvaDinamica ? strings.editarCriterio : strings.editarQuestao}
        onClick={
          provaUtils.isProvaDinamica ? () => this._editCriterio(true) : this._editarQuestaoDaProva(questaoProcessada.id)
        }
        IconComponent={EditIcon}
        color="fundoCinza"
      />
    )
    return children({ expandirContrairButton, excluirButton, addButton, pinButton, pinDisplay, editarButton })
  }
}
