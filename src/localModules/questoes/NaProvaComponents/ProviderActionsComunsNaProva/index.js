import { ProviderActionsComunsNaProvaComponent } from './ProviderActionsComunsNaProvaComponent'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import { withStyles } from '@material-ui/core/styles'

import { mapStateToProps, mapDispatchToProps } from './redux'
import { style } from './style'

export const ProviderActionsComunsNaProva = compose(
  withStyles(style),
  withRouter,
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(ProviderActionsComunsNaProvaComponent)
