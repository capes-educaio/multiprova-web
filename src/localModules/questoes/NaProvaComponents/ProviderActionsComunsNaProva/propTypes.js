import PropTypes from 'prop-types'

const { shape, func, string, object } = PropTypes

export const propTypes = {
  children: PropTypes.func.isRequired,
  onClick: shape({ pinButton: func, expandirContrairButton: func }),
  tooltips: shape({ pinButton: string, addButton: string, excluirButton: string }),
  stringExcluir: string,
  // redux state
  value: object.isRequired,
  updateProvaValue: func.isRequired,
  // redux actions
  selectValorProvaNaVolta: func.isRequired,
  // strings
  strings: object.isRequired,
  // style
  classes: object.isRequired,
}
