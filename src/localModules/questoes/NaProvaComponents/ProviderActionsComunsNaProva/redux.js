import { bindActionCreators } from 'redux'
import { actions } from 'utils/actions'

export function mapStateToProps(state) {
  return {
    strings: state.strings,
    value: state.ProviderProva__value,
    updateProvaValue: state.ProviderProva__updateProvaValue,
  }
}

export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      selectValorProvaNaVolta: actions.select.valorProvaNaVolta,
    },
    dispatch,
  )
}
