import { store } from 'utils/store'
import { EMPTY_EDITOR } from 'utils/Editor'
import { enumAnoEscolar } from 'utils/enumAnoEscolar'

export const montarValorInicialComum = ({
  enunciado,
  elaborador,
  dificuldade,
  tagIds,
  status,
  anoEscolar,
  tipo,
  publico,
}) => {
  const listaTags = store.getState().listaTags
  let tagsProcessadas
  if (tagIds && Array.isArray(listaTags)) {
    tagsProcessadas = tagIds.map(tag => {
      const id = listaTags.find(tagDaLista => tagDaLista.id === tag)
      if (id) return id
      return []
    })
  } else if (tagIds) {
    tagsProcessadas = []
    console.error('Falha ao processar tags.')
  } else {
    tagsProcessadas = []
  }
  const statusId = status !== undefined ? status.id : ''
  return {
    enunciado: enunciado ? enunciado : EMPTY_EDITOR,
    elaborador: elaborador ? elaborador : '',
    dificuldade,
    tags: tagsProcessadas,
    statusId,
    anoEscolar: anoEscolar ? anoEscolar : enumAnoEscolar.ensinoSuperiorBasico,
    tipo,
    publico,
  }
}
