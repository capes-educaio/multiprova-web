import { enumStatusQuestao } from 'utils/enumStatusQuestao'

export const buscaStatusUsandoId = statusId => {
  let status
  Object.keys(enumStatusQuestao).forEach(key => {
    if (enumStatusQuestao[key].id === statusId) status = enumStatusQuestao[key]
  })
  return status
}
