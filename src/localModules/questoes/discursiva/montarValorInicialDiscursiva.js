import { montarValorInicialComum } from '../montarValorInicialComum'

export const montarValorInicial = dados => {
  const { discursiva } = dados
  discursiva.numeroDeLinhas = String(discursiva.numeroDeLinhas)
  return {
    ...montarValorInicialComum(dados),
    discursiva,
  }
}
