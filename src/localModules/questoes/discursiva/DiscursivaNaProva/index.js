import { compose } from 'redux'
import { connect } from 'react-redux'
// import { withRouter } from 'react-router'

import { withStyles } from '@material-ui/core/styles'

import { mapStateToProps, mapDispatchToProps } from './redux'
import { style } from './style'
import { DiscursivaNaProvaComponent } from './DiscursivaNaProvaComponent'

export const DiscursivaNaProva = compose(
  // withRouter,
  withStyles(style),
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(DiscursivaNaProvaComponent)
