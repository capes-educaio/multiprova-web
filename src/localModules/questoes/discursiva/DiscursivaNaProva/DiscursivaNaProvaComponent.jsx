import React, { Component } from 'react'
import { MpParser } from 'common/MpParser'

import { QuestaoSingularNaProva } from 'localModules/questoes/NaProvaComponents'

import { propTypes } from './propTypes'

export class DiscursivaNaProvaComponent extends Component {
  static propTypes = propTypes

  render() {
    const { classes, questaoProcessada, dragHandleProps, variant } = this.props
    return (
      <QuestaoSingularNaProva
        questaoProcessada={questaoProcessada}
        dragHandleProps={dragHandleProps}
        variant={variant}
        classNames={{ barraExpandir: classes.barraExpandir }}
        conteudoContraido={<MpParser>{questaoProcessada.enunciado}</MpParser>}
        disableExpandir
      />
    )
  }
}
