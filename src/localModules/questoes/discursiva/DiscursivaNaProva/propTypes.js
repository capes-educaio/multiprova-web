import PropTypes from 'prop-types'

export const propTypes = {
  questaoProcessada: PropTypes.object.isRequired,
  dragHandleProps: PropTypes.object,
  variant: PropTypes.oneOf(['view', 'edit']).isRequired,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
