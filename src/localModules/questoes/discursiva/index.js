import { DiscursivaNaProva } from './DiscursivaNaProva'

export { getValorVazio } from './getValorVazioDiscursiva'
export { montarQuestaoParaEnviar } from './montarQuestaoParaEnviarDiscursiva'
export { montarValorInicial } from './montarValorInicialDiscursiva'
export { DiscursivaFormPage as Component } from './DiscursivaFormPage'
export const urlMatches = 'criar-discursiva'
export const VerNaProvaComponent = DiscursivaNaProva
export const EditarNaProvaComponent = DiscursivaNaProva
