import { EMPTY_EDITOR } from 'utils/Editor'

import { getValorVazioComum } from '../getValorVazioComum'
import { enumTipoQuestao } from 'utils/enumTipoQuestao'

export const getValorVazio = () => {
  const discursiva = {
    numeroDeLinhas: '',
    expectativaDeResposta: EMPTY_EDITOR,
  }
  return { tipo: enumTipoQuestao.tipoQuestaoDiscursiva, discursiva, ...getValorVazioComum() }
}
