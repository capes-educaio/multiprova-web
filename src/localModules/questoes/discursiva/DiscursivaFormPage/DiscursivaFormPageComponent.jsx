import React, { Component } from 'react'

import { QuestaoFormPagePadrao } from '../../QuestaoFormPagePadrao'
import { DiscursivaForm } from '../DiscursivaForm'

import { propTypes } from './propTypes'

export class DiscursivaFormPageComponent extends Component {
  static propTypes = propTypes

  render() {
    return <QuestaoFormPagePadrao {...this.props} FormComponent={DiscursivaForm} />
  }
}
