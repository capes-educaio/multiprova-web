import PropTypes from 'prop-types'

const { oneOfType, arrayOf, element } = PropTypes
export const propTypes = {
  botoesExtras: oneOfType([element, arrayOf(element)]),
  valor: PropTypes.object,
  dadosMontados: PropTypes.object,
  carregando: PropTypes.bool.isRequired,
  isModoPreviewLigado: PropTypes.bool.isRequired,
  isEdicao: PropTypes.bool.isRequired,
  handleValorChange: PropTypes.func.isRequired,
  ativarModoVisualizacao: PropTypes.func.isRequired,
  ativarModoEdicao: PropTypes.func.isRequired,
  editarQuestao: PropTypes.func.isRequired,
  cadastrarQuestao: PropTypes.func.isRequired,
  voltarParaPaginaDeOrigem: PropTypes.func.isRequired,
}
