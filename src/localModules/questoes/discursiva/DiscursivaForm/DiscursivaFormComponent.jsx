import React, { Component } from 'react'
import uid from 'uuid/v4'

import { SimpleReduxForm, CampoSlate, CampoObjeto, CampoText } from 'localModules/ReduxForm'

import { QuestaoCamposComuns } from '../../QuestaoCamposComuns'
import { CampoSubQuestaoBody } from '../../CampoSubQuestaoBody'

import { propTypes } from './propTypes'

export class DiscursivaFormComponent extends Component {
  static propTypes = propTypes
  _formId = uid()
  _discursivaId = uid()

  render() {
    const { valor, strings, onRef, onValid, onChange, variant, id, accessor, wrapperProps, label, classes } = this.props

    const discursiva = (
      <CampoObjeto key={this._discursivaId} id={'discursiva-' + this._discursivaId} accessor="discursiva">
        <CampoText
          id={'discursiva__numeroDeLinhas-' + this._formId}
          accessor="numeroDeLinhas"
          required
          label={strings.numeroDeLinhas}
          textFieldProps={{
            className: classes.numeroDeLinhas,
            InputProps: { inputProps: { min: 0 } },
            InputLabelProps: {
              shrink: true,
            },
            type: 'number',
          }}
        />
        <span className={classes.legenda}>({strings.dicaNumeroDeLinhas})</span>
        <CampoSlate
          id={'discursiva__expectativaDeResposta-' + this._formId}
          accessor="expectativaDeResposta"
          required
          inputProps={{ placeholder: strings.expectativaDeResposta, rows: 5 }}
        />
      </CampoObjeto>
    )
    return (
      <QuestaoCamposComuns>
        {({ campoEnunciado, campoDificuldade, campoElaborador, campoTag, campoStatus, campoAnoEscolar }) => {
          if (variant === 'dentroDeBloco')
            return (
              <CampoObjeto
                id={id}
                label={label}
                wrapperProps={wrapperProps}
                accessor={accessor}
                BodyComponent={CampoSubQuestaoBody}
              >
                {campoEnunciado}
                {discursiva}
                {campoAnoEscolar}
                {campoDificuldade}
                {campoElaborador}
                {campoTag}
              </CampoObjeto>
            )
          return (
            <SimpleReduxForm id={this._formId} valor={valor} onRef={onRef} onChange={onChange} onValid={onValid}>
              {campoStatus}
              {campoEnunciado}
              {discursiva}
              {campoAnoEscolar}
              {campoDificuldade}
              {campoElaborador}
              {campoTag}
            </SimpleReduxForm>
          )
        }}
      </QuestaoCamposComuns>
    )
  }
}
