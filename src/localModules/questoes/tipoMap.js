import * as multiplaEscolha from './multiplaEscolha'
import * as bloco from './bloco'
import * as discursiva from './discursiva'
import * as vouf from './vouf'
import * as associacaoDeColunas from './associacaoDeColunas'
import * as redacao from './redacao'
import * as criterio from './criterios'

export const tipoMap = {
  multiplaEscolha,
  bloco,
  discursiva,
  vouf,
  associacaoDeColunas,
  redacao,
  criterio,
}
