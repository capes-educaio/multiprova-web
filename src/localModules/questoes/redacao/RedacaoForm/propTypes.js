import PropTypes from 'prop-types'

export const propTypes = {
  valor: PropTypes.object,
  onValid: PropTypes.func,
  onChange: PropTypes.func,
  onRef: PropTypes.func,
  // style
  classes: PropTypes.object.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
}
