export const style = theme => ({
  numeroDeLinhas: {
    width: '250px',
  },
  legenda: {
    fontSize: 12,
    color: theme.palette.cinzaPadrao,
    fontStyle: 'italic',
    marginBottom: 10,
    display: 'block',
  },
})
