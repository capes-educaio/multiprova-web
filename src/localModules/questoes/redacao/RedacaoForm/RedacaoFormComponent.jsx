import React, { Component } from 'react'
import uid from 'uuid/v4'

import { SimpleReduxForm, CampoSlate, CampoObjeto, CampoText } from 'localModules/ReduxForm'

import { QuestaoCamposComuns } from '../../QuestaoCamposComuns'

import { propTypes } from './propTypes'

export class RedacaoFormComponent extends Component {
  static propTypes = propTypes
  _formId = uid()
  _redacaoId = uid()

  render() {
    const { valor, strings, onRef, onValid, onChange, classes } = this.props
    const tema = (
      <CampoSlate
        id={'redacao_tema-' + this._formId}
        accessor="tema"
        required
        inputProps={{ placeholder: strings.tema, rows: 3, id: 'form-questao-tema' }}
      />
    )
    const campoPropostaDeRedacao = (
      <CampoSlate
        id={'enunciado-' + this._formId}
        accessor="enunciado"
        inputProps={{ placeholder: strings.propostaDeRedacao, rows: 3, id: 'form-questao-proposta' }}
        required
      />
    )
    const redacao = (
      <CampoObjeto key={this._redacaoId} id={'redacao-' + this._redacaoId} accessor="redacao">
        <CampoSlate
          id={'redacao_instrucoes-' + this._formId}
          accessor="instrucoes"
          required
          inputProps={{ placeholder: strings.instrucoes, rows: 3, id: 'form-questao-instrucoes' }}
        />
        <CampoText
          id={'redacao__numeroDeLinhas-' + this._formId}
          accessor="numeroDeLinhas"
          required
          label={strings.numeroDeLinhas}
          textFieldProps={{
            className: classes.numeroDeLinhas,
            InputProps: { inputProps: { min: 0, max: 200 }, id: 'form-questao-numeroDeLinhas' },
            InputLabelProps: {
              shrink: true,
            },
            type: 'number',
          }}
        />
        <span className={classes.legenda}>({strings.dicaNumeroDeLinhas})</span>
        <CampoSlate
          id={'redacao__expectativaDeResposta-' + this._formId}
          accessor="expectativaDeResposta"
          required
          inputProps={{ placeholder: strings.expectativaDeResposta, rows: 3, id: 'form-questao-resposta' }}
        />
      </CampoObjeto>
    )
    return (
      <QuestaoCamposComuns>
        {({ campoDificuldade, campoElaborador, campoTag, campoStatus, campoAnoEscolar }) => {
          return (
            <SimpleReduxForm id={this._formId} valor={valor} onRef={onRef} onChange={onChange} onValid={onValid}>
              {campoStatus}
              {tema}
              {campoPropostaDeRedacao}
              {redacao}
              {campoAnoEscolar}
              {campoDificuldade}
              {campoElaborador}
              {campoTag}
            </SimpleReduxForm>
          )
        }}
      </QuestaoCamposComuns>
    )
  }
}
