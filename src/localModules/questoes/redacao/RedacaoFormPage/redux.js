import { bindActionCreators } from 'redux'

export function mapStateToProps(state) {
  return {}
}

export function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch)
}
