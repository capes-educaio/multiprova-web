import { RedacaoFormPageComponent } from './RedacaoFormPageComponent'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'

import { mapStateToProps, mapDispatchToProps } from './redux'

export const RedacaoFormPage = compose(
  withRouter,
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(RedacaoFormPageComponent)
