import React, { Component } from 'react'

import { QuestaoFormPagePadrao } from '../../QuestaoFormPagePadrao'
import { RedacaoForm } from '../RedacaoForm'

import { propTypes } from './propTypes'

export class RedacaoFormPageComponent extends Component {
  static propTypes = propTypes

  render() {
    return <QuestaoFormPagePadrao {...this.props} FormComponent={RedacaoForm} hideBotoes={['preview']} />
  }
}
