import { montarValorInicialComum } from '../montarValorInicialComum'

export const montarValorInicial = dados => {
  const { redacao } = dados
  redacao.numeroDeLinhas = String(redacao.numeroDeLinhas)
  const tema = redacao.tema
  delete redacao.tema
  return {
    ...montarValorInicialComum(dados),
    redacao,
    tema,
  }
}
