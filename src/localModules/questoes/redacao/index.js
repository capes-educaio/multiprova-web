import { RedacaoNaProva } from './RedacaoNaProva'

export { getValorVazio } from './getValorVazioRedacao'
export { montarQuestaoParaEnviar } from './montarQuestaoParaEnviarRedacao'
export { montarValorInicial } from './montarValorInicialRedacao'
export { RedacaoFormPage as Component } from './RedacaoFormPage'
export const VerNaProvaComponent = RedacaoNaProva
export const EditarNaProvaComponent = RedacaoNaProva
export const urlMatches = 'criar-redacao'
