import { compose } from 'redux'
import { connect } from 'react-redux'

import { withStyles } from '@material-ui/core/styles'

import { mapStateToProps, mapDispatchToProps } from './redux'
import { style } from './style'
import { RedacaoNaProvaComponent } from './RedacaoNaProvaComponent'

export const RedacaoNaProva = compose(
  withStyles(style),
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(RedacaoNaProvaComponent)
