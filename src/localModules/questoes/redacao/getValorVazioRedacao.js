import { EMPTY_EDITOR } from 'utils/Editor'

import { getValorVazioComum } from '../getValorVazioComum'

export const getValorVazio = () => {
  const redacao = {
    numeroDeLinhas: '',
    instrucoes: EMPTY_EDITOR,
    expectativaDeResposta: EMPTY_EDITOR,
  }
  return { tipo: 'redacao', tema: EMPTY_EDITOR, redacao, ...getValorVazioComum() }
}
