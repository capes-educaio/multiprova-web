import { montarQuestaoParaEnviarComum } from '../montarQuestaoParaEnviarComum'

export const montarQuestaoParaEnviar = dados => {
  let { redacao } = JSON.parse(JSON.stringify(dados))
  const { tema } = JSON.parse(JSON.stringify(dados))
  redacao.numeroDeLinhas = parseInt(redacao.numeroDeLinhas)
  redacao.tema = tema
  delete dados.tema
  return { redacao, ...montarQuestaoParaEnviarComum(dados) }
}
