import { bindActionCreators } from 'redux'
import { actions } from 'utils/actions'

export function mapStateToProps(state) {
  return {
    strings: state.strings,
    usuarioAtual: state.usuarioAtual,
    questoesSelecionadasDicionario: state.questoesSelecionadasDicionario,
    listaTags: state.listaTags,
  }
}

export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      selectListaTags: actions.select.listaTags,
      selectQuestoesSelecionadasDicionario: actions.select.questoesSelecionadasDicionario,
      listaDialogPush: actions.push.listaDialog,
    },
    dispatch,
  )
}
