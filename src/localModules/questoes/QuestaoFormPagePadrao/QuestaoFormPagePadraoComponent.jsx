import React, { Component } from 'react'
import Divider from '@material-ui/core/Divider'
import Tooltip from '@material-ui/core/Tooltip'

import Send from '@material-ui/icons/Send'
import Visibility from '@material-ui/icons/Visibility'
import EditIcon from '@material-ui/icons/Edit'
import ExportIcon from 'localModules/icons/ExportIcon'
import LockIcon from '@material-ui/icons/Lock'
import Public from '@material-ui/icons/Public'

import { appConfig } from 'appConfig'

import { PaginaPaper } from 'common/PaginaPaper'
import { TituloDaPagina } from 'common/TituloDaPagina'
import { VisualizacaoQuestao } from 'common/VisualizacaoQuestao'
import { MpButton } from 'common/MpButton'
import { questaoUtils } from 'localModules/questoes/utils/QuestaoUtils'
import { setListaTags } from 'utils/compositeActions'

import { propTypes } from './propTypes'
import { defaultProps } from './defaultProps'

export class QuestaoFormPagePadraoComponent extends Component {
  static propTypes = propTypes
  static defaultProps = defaultProps
  _formRef
  _salvarButtonRef

  state = {
    isValid: false,
    botaoPrivarQuestao: this.props.valor.publico,
    buttonDisabled: false,
  }

  _salvarOnClick = async () => {
    this.setState({ buttonDisabled: true })
    let questaoCadastrada = {}
    const { editarQuestao, cadastrarQuestao, isEdicao } = this.props
    const { isValid } = this.state
    if (!isValid) this._mostrarErros()
    else if (isEdicao) questaoCadastrada = await editarQuestao()
    else questaoCadastrada = await cadastrarQuestao()
    await setListaTags()

    if (this._salvarButtonRef && this._salvarButtonRef.endLoad) {
      this._salvarButtonRef.endLoad()
    }
    if (questaoCadastrada) {
      setTimeout(() => this.setState({ buttonDisabled: false }), 1000)
    }
  }

  _handleOnFormValid = isValid => this.setState({ isValid })

  _mostrarErros = () => {
    if (this._formRef && this._formRef.showError) this._formRef.showError()
    else console.error('O formRef não está setado ou não tem método showError.')
  }

  openDialogTornarPrivada = () => {
    const { listaDialogPush, strings } = this.props
    const dialogConfig = {
      titulo: strings.privarQuestao,
      texto: strings.desejaTornarPrivadaQuestao,
      actions: [
        {
          texto: strings.sim,
          onClick: this.privarQuestaoUsuario,
        },
        {
          texto: strings.nao,
        },
      ],
    }
    listaDialogPush(dialogConfig)
  }

  privarQuestaoUsuario = () => {
    this.props.valor.publico = false
    this.setState({ botaoPrivarQuestao: false })
  }

  render() {
    const {
      ativarModoEdicao,
      voltarParaPaginaDeOrigem,
      FormComponent,
      botoesExtras,
      hideBotoes,
      classes,
      strings,
      valor,
      isModoPreviewLigado,
      handleValorChange,
      ativarModoVisualizacao,
      dadosMontados,
      isEdicao,
    } = this.props
    const { isValid, botaoPrivarQuestao, buttonDisabled } = this.state
    const esconderNaVisualizacao = { display: isModoPreviewLigado ? 'none' : 'block' }
    const hidePreview = hideBotoes.includes('preview')
    return (
      <PaginaPaper>
        <TituloDaPagina>
          <div className={classes.editIcone}>
            {isEdicao ? strings.editarQuestao : strings.cadastrarQuestao}
            {botaoPrivarQuestao && (
              <Tooltip title={strings.questaoPublica}>
                <Public className={classes.iconePublic} color="primary" />
              </Tooltip>
            )}
          </div>
          {botaoPrivarQuestao && (
            <div className={classes.dispBotoes}>
              <MpButton
                buttonProps={{ variant: 'contained' }}
                onClick={this.openDialogTornarPrivada}
                IconComponent={LockIcon}
                variant="material"
              >
                {strings.privarQuestao}
              </MpButton>
            </div>
          )}
        </TituloDaPagina>
        <Divider />
        <div className={classes.secao} style={esconderNaVisualizacao}>
          <FormComponent
            valor={valor}
            onValid={this._handleOnFormValid}
            onChange={handleValorChange}
            onRef={ref => (this._formRef = ref)}
          />
        </div>
        {isModoPreviewLigado && (
          <VisualizacaoQuestao
            dados={dadosMontados}
            tipo={valor.tipo}
            tipoTemplate={'geral'}
            onFechar={ativarModoEdicao}
          />
        )}
        <Divider />
        <MpButton
          id="salvar-questao"
          classes={{ root: classes.botaoSalvar }}
          buttonProps={{ variant: 'contained', color: 'primary' }}
          onClick={this._salvarOnClick}
          IconComponent={Send}
          loadOnClick={isValid}
          onRef={ref => (this._salvarButtonRef = ref)}
          variant="material"
          disabled={buttonDisabled}
        >
          {strings.salvar}
        </MpButton>
        {!appConfig.projeto.isEducaio && (
          <MpButton
            id="exportar-questao"
            onClick={() => questaoUtils.exportarQuestao(valor)}
            className={classes.botaoSecundario}
            IconComponent={ExportIcon}
            variant="material"
            classes={{ icon: classes.icone }}
          >
            {strings.exportar}
          </MpButton>
        )}
        {!hidePreview && isModoPreviewLigado && (
          <MpButton
            id="editar-questao"
            onClick={ativarModoEdicao}
            className={classes.botaoSecundario}
            IconComponent={EditIcon}
            variant="material"
          >
            {strings.editar}
          </MpButton>
        )}
        {!hidePreview && !isModoPreviewLigado && (
          <MpButton
            id="visualizar-questao"
            onClick={ativarModoVisualizacao}
            className={classes.botaoSecundario}
            IconComponent={Visibility}
            variant="material"
          >
            {strings.visualizar}
          </MpButton>
        )}
        {botoesExtras}
        <MpButton
          id="sair-da-edicao-questao"
          onClick={voltarParaPaginaDeOrigem}
          className={classes.botaoSecundario}
          variant="material"
        >
          {strings.sairDaEdicao}
        </MpButton>
      </PaginaPaper>
    )
  }
}
