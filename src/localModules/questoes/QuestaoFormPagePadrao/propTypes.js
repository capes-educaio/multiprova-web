import PropTypes from 'prop-types'

const { oneOfType, arrayOf, element, oneOf } = PropTypes
export const propTypes = {
  FormComponent: PropTypes.any.isRequired,
  botoesExtras: oneOfType([element, arrayOf(element)]),
  hideBotoes: arrayOf(oneOf(['preview'])),
  // vindos de QuestaoComponent
  valor: PropTypes.object,
  dadosMontados: PropTypes.object,
  carregando: PropTypes.bool.isRequired,
  isModoPreviewLigado: PropTypes.bool.isRequired,
  isEdicao: PropTypes.bool.isRequired,
  handleValorChange: PropTypes.func.isRequired,
  ativarModoVisualizacao: PropTypes.func.isRequired,
  ativarModoEdicao: PropTypes.func.isRequired,
  editarQuestao: PropTypes.func.isRequired,
  cadastrarQuestao: PropTypes.func.isRequired,
  voltarParaPaginaDeOrigem: PropTypes.func.isRequired,
  // style
  classes: PropTypes.object.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
  // redux actions
  listaDialogPush: PropTypes.func.isRequired,
}
