import base64Image from 'base64-img'

import { uploadFile } from 'utils/uploadFile'
import { replaceAsync } from 'utils/strings'

const URL = 'url'
const BASE_64 = 'base64'

export const requisitarBase64 = url => {
  return new Promise(resolve => {
    base64Image.requestBase64(url, (err, res, base64) => {
      if (err) throw new Error('Erro ao obter a base64 da imagem')
      resolve(base64)
    })
  })
}

const base64ParaArquivo = base64 => {
  const arr = base64.split(','),
    mime = arr[0].match(/:(.*?);/)[1],
    bstr = atob(arr[1])
  let n = bstr.length
  const u8arr = new Uint8Array(n)
  while (n--) {
    u8arr[n] = bstr.charCodeAt(n)
  }
  return new File([u8arr], 'arquivo', { type: mime })
}

const converter = async (str, tipo) => {
  return replaceAsync(str, /<img[^>]+src\s*=\\\s*"([^\\">]+)/g, async (imagem, src) =>
    imagem.replace(
      src,
      tipo === BASE_64 ? await requisitarBase64(src) : await uploadFile(base64ParaArquivo(src), 'image'),
    ),
  )
}

export const imagensParaUrlQuestao = str => converter(str, URL)

export const imagensParaBase64Questao = str => converter(str, BASE_64)
