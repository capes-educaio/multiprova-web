import FileSaver from 'file-saver'

import { enumTipoQuestao } from 'utils/enumTipoQuestao'
import { store } from 'utils/store'
import { setUrlState } from 'localModules/urlState'
import { imagensParaBase64Questao } from './conversaoImagensQuestao'
import { get } from 'api'

class QuestaoUtils {
  stepInicialEdicao = 0

  get rotas() {
    return {
      [enumTipoQuestao.tipoQuestaoBloco]: '/questao/criar-bloco',
      [enumTipoQuestao.tipoQuestaoDiscursiva]: '/questao/criar-discursiva',
      [enumTipoQuestao.tipoQuestaoMultiplaEscolha]: '/questao/criar-multipla-escolha',
      [enumTipoQuestao.tipoQuestaoVerdadeiroOuFalso]: '/questao/criar-vouf',
      [enumTipoQuestao.tipoQuestaoAssociacaoDeColunas]: '/questao/criar-associacao-de-colunas',
      [enumTipoQuestao.tipoQuestaoRedacao]: '/questao/criar-redacao',
      estatisticasQuestoes: id => `/usuarios/${id}/estatisticas-questoes`,
    }
  }

  irParaEditarQuestao = location => questaoId => () => {
    const pathname = `/questao/${questaoId}`
    const deveVoltarParaEsseUrlState = {
      pathname: location.pathname,
      hidden: { ...location.state.hidden },
    }
    setUrlState({
      pathname,
      hidden: { deveVoltarParaEsseUrlState, activeStep: this.stepInicialEdicao },
    })
  }

  irParaAddQuestao = location => tipoQuestao => () => {
    const pathname = this.rotas[tipoQuestao]

    const deveVoltarParaEsseUrlState = {
      pathname: location.pathname,
      hidden: { ...location.state.hidden },
    }
    setUrlState({
      pathname,
      hidden: { deveVoltarParaEsseUrlState, activeStep: this.stepInicialEdicao },
    })
  }
  getQuestaoProcessada = questao => {
    const questaoProcessada = { ...questao }
    if (typeof questaoProcessada.multiplaEscolha.alternativas[0] === 'object') {
      questaoProcessada.multiplaEscolha.alternativas = questaoProcessada.multiplaEscolha.alternativas.map(
        alternativa => alternativa.texto,
      )
    }
    return questaoProcessada
  }

  exportarQuestao = async questao => {
    try {
      const { strings } = store.getState()
      let dadosDaQuestao = Object.assign({}, questao)
      if (dadosDaQuestao.status) dadosDaQuestao.statusId = dadosDaQuestao.status.id
      delete dadosDaQuestao.criadoPor
      delete dadosDaQuestao.dataCadastro
      delete dadosDaQuestao.dataUltimaAlteracao
      delete dadosDaQuestao.id
      delete dadosDaQuestao.status
      delete dadosDaQuestao.tagIds
      delete dadosDaQuestao.usuarioId
      delete dadosDaQuestao.open
      if (dadosDaQuestao.multiplaEscolha) {
        dadosDaQuestao = this.getQuestaoProcessada(dadosDaQuestao)
      } else if (dadosDaQuestao.bloco) {
        dadosDaQuestao.bloco.questoes = dadosDaQuestao.bloco.questoes.map(questao => this.getQuestaoProcessada(questao))
      }
      const questaoASerExportada = await imagensParaBase64Questao(JSON.stringify(dadosDaQuestao))
      const nomeDoArquivo = strings.questao
      const contentType = 'application/json;charset=utf-8;'
      let arquivo = new File([questaoASerExportada], nomeDoArquivo, { type: contentType })
      FileSaver.saveAs(arquivo)
    } catch (e) {
      console.error(e)
    }
  }

  getEstatisticasQuestoes = idUsuario => {
    return get(this.rotas.estatisticasQuestoes(idUsuario))
  }
}

export const questaoUtils = new QuestaoUtils()
