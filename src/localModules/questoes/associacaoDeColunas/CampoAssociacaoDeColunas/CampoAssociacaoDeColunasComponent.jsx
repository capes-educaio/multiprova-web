import React, { Component } from 'react'

import { CampoArray, CampoObjeto } from 'localModules/ReduxForm'

import { CampoItem } from '../CampoItem'

import { store } from 'utils/store'

import { propTypes } from './propTypes'

export class CampoAssociacaoDeColunasComponent extends Component {
  static propTypes = propTypes

  updateB = () => {
    const { id, refs } = this.props
    if (refs['colunaB-' + id]) refs['colunaB-' + id].forceUpdate()
  }

  reordenarB = indice => {
    const { id, refs, values } = this.props
    refs['colunaB-' + id].state.fields.forEach((fieldId, index, array) => {
      const { rotulo } = values[fieldId]
      if (rotulo === indice + 1) refs['rotulo-' + fieldId]._onChange(' ')
      if (rotulo > indice + 1) refs['rotulo-' + fieldId]._onChange(rotulo - 1)
    })
    this.updateB()
  }

  _getFieldPropsColunaA = () => {
    const { id } = this.props
    return {
      updateB: this.updateB,
      reordenarB: this.reordenarB,
      idColunaPai: 'colunaA-' + id,
      isColunaA: true,
    }
  }

  _getFieldPropsColunaB = () => {
    const { id } = this.props
    return {
      idColunaPai: 'colunaB-' + id,
      isColunaA: false,
      tamanhoColunaA: store.getState().SimpleReduxForm__values['colunaA-' + id].length,
    }
  }

  render() {
    const { strings, campoArrayProps, id, classes, accessor } = this.props
    const colunaA = (
      <CampoArray
        id={'colunaA-' + id}
        accessor={'colunaA'}
        label={<div className={classes.label}>{strings.coluna + ' A'}</div>}
        FieldComponent={CampoItem}
        getFieldProps={this._getFieldPropsColunaA}
        {...campoArrayProps}
        wrapperProps={{ className: classes.colunaResponsiva }}
      />
    )
    const colunaB = (
      <CampoArray
        id={'colunaB-' + id}
        accessor={'colunaB'}
        label={<div className={classes.label}>{strings.coluna + ' B'}</div>}
        FieldComponent={CampoItem}
        getFieldProps={this._getFieldPropsColunaB}
        {...campoArrayProps}
        wrapperProps={{ className: classes.colunaResponsiva }}
      />
    )
    return (
      <CampoObjeto
        key={id}
        id={id}
        accessor={accessor}
        wrapperProps={{ className: classes.associacaoDeColunasResponsiva }}
      >
        {colunaA}
        {colunaB}
      </CampoObjeto>
    )
  }
}
