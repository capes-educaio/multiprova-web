export const style = theme => ({
  label: {
    marginTop: 10,
  },
  colunaResponsiva: {
    [theme.breakpoints.down('sm')]: {
      width: '100%',
    },
    [theme.breakpoints.up('md')]: {
      width: '50%',
    },
  },
  associacaoDeColunasResponsiva: {
    [theme.breakpoints.down('sm')]: {
      display: 'block',
    },
    [theme.breakpoints.up('md')]: {
      display: 'flex',
    },
  },
})
