import { CampoAssociacaoDeColunasComponent } from './CampoAssociacaoDeColunasComponent'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'

import { mapStateToProps, mapDispatchToProps } from './redux'
import { style } from './style'

export const CampoAssociacaoDeColunas = compose(
  withStyles(style),
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(CampoAssociacaoDeColunasComponent)
