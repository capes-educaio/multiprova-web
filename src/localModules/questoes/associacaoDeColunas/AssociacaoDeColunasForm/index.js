import { AssociacaoDeColunasFormComponent } from './AssociacaoDeColunasFormComponent'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'

import { mapStateToProps, mapDispatchToProps } from './redux'
import { style } from './style'

export const AssociacaoDeColunasForm = compose(
  withStyles(style),
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(AssociacaoDeColunasFormComponent)

AssociacaoDeColunasForm.hasFields = true
