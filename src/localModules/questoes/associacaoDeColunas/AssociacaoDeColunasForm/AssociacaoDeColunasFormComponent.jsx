import React, { Component } from 'react'
import uid from 'uuid/v4'

import { SimpleReduxForm, CampoObjeto } from 'localModules/ReduxForm'

import { QuestaoCamposComuns } from '../../QuestaoCamposComuns'
import { CampoSubQuestaoBody } from '../../CampoSubQuestaoBody'

import { propTypes } from './propTypes'
import { CampoAssociacaoDeColunas } from '../CampoAssociacaoDeColunas'

export class AssociacaoDeColunasFormComponent extends Component {
  static propTypes = propTypes
  _formId = uid()

  render() {
    const { valor, onRef, onValid, onChange, variant, id, accessor, wrapperProps, label } = this.props
    const associacaoDeColunas = (
      <CampoAssociacaoDeColunas id={'associacaoDeColunas' + this._formId} accessor={'associacaoDeColunas'} />
    )
    return (
      <QuestaoCamposComuns>
        {({ campoEnunciado, campoDificuldade, campoElaborador, campoTag, campoStatus, campoAnoEscolar }) => {
          if (variant === 'dentroDeBloco')
            return (
              <CampoObjeto
                id={id}
                label={label}
                wrapperProps={wrapperProps}
                accessor={accessor}
                BodyComponent={CampoSubQuestaoBody}
              >
                {campoEnunciado}
                {associacaoDeColunas}
                {campoAnoEscolar}
                {campoDificuldade}
                {campoElaborador}
                {campoTag}
              </CampoObjeto>
            )
          return (
            <SimpleReduxForm id={this._formId} valor={valor} onRef={onRef} onChange={onChange} onValid={onValid}>
              {campoStatus}
              {campoEnunciado}
              {associacaoDeColunas}
              {campoAnoEscolar}
              {campoDificuldade}
              {campoElaborador}
              {campoTag}
            </SimpleReduxForm>
          )
        }}
      </QuestaoCamposComuns>
    )
  }
}
