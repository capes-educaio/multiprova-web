import PropTypes from 'prop-types'

export const propTypes = {
  accessor: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  id: PropTypes.string,
  tamanhocolunaA: PropTypes.number,
  index: PropTypes.number,
  campoSelectProps: PropTypes.object,
  hidden: PropTypes.bool,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
