import React, { Component } from 'react'

import MenuItem from '@material-ui/core/MenuItem'

import { CampoSelect } from 'localModules/ReduxForm/CampoSelect'

import { propTypes } from './propTypes'

export class CampoRotuloComponent extends Component {
  static propTypes = propTypes

  render() {
    const { id, accessor, tamanhoColunaA, isColunaA, index, classes, hidden } = this.props
    const range = Array.from({ length: tamanhoColunaA - 1 }, (value, key) => key + 1)
    const opcoes = [' '].concat(range)
    if (hidden) return <div id={id} accessor={accessor} className={classes.hidden} />
    else if (isColunaA)
      return (
        <MenuItem className={classes.rotuloFixo} id={id} accessor={accessor} value={index + 1}>
          {index + 1 + '.'}
        </MenuItem>
      )
    else
      return (
        <CampoSelect id={id} accessor={accessor}>
          {opcoes.map(valor => (
            <MenuItem value={valor} key={valor}>
              {valor}
            </MenuItem>
          ))}
        </CampoSelect>
      )
  }
}
