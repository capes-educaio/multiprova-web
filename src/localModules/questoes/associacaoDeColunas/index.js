import { AssociacaoDeColunasNaProva } from './AssociacaoDeColunasNaProva'

export { getValorVazio } from './getValorVazioAssociacaoDeColunas'
export { montarQuestaoParaEnviar } from './montarQuestaoParaEnviarAssociacaoDeColunas'
export { montarValorInicial } from './montarValorInicialAssociacaoDeColunas'
export { AssociacaoDeColunasFormPage as Component } from './AssociacaoDeColunasFormPage'
export const VerNaProvaComponent = AssociacaoDeColunasNaProva
export const EditarNaProvaComponent = AssociacaoDeColunasNaProva
export const urlMatches = 'criar-associacao-de-colunas'