import { AssociacaoDeColunasFormPageComponent } from './AssociacaoDeColunasFormPageComponent'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'

import { mapStateToProps, mapDispatchToProps } from './redux'

export const AssociacaoDeColunasFormPage = compose(
  withRouter,
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(AssociacaoDeColunasFormPageComponent)
