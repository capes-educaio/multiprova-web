import React, { Component } from 'react'

import { QuestaoFormPagePadrao } from '../../QuestaoFormPagePadrao'
import { AssociacaoDeColunasForm } from '../AssociacaoDeColunasForm'

import { propTypes } from './propTypes'

export class AssociacaoDeColunasFormPageComponent extends Component {
  static propTypes = propTypes

  render() {
    return <QuestaoFormPagePadrao {...this.props} FormComponent={AssociacaoDeColunasForm} />
  }
}
