import { montarValorInicialComum } from '../montarValorInicialComum'
import { getItemVazio } from './getItemVazio'

export const montarValorInicial = dados => {
  const { associacaoDeColunas } = dados
  associacaoDeColunas.colunaA.push(getItemVazio())
  associacaoDeColunas.colunaB.push(getItemVazio())
  return {
    ...montarValorInicialComum(dados),
    associacaoDeColunas,
  }
}
