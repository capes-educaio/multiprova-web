import React, { Component } from 'react'
import Grid from '@material-ui/core/Grid'
import { MpParser } from 'common/MpParser'
import { ItemAssociacao } from 'common/ItemAssociacao'

import { QuestaoSingularNaProva } from 'localModules/questoes/NaProvaComponents'

import { propTypes } from './propTypes'

export class AssociacaoDeColunasNaProvaComponent extends Component {
  static propTypes = propTypes

  render() {
    const { classes, questaoProcessada, dragHandleProps, variant } = this.props
    const conteudoItemAssociacao = (
      <Grid container spacing={24}>
        <Grid item xs={6}>
          {questaoProcessada.associacaoDeColunas.colunaA.map((item, index) => (
            <ItemAssociacao key={index} item={item} />
          ))}
        </Grid>
        <Grid item xs={6}>
          {questaoProcessada.associacaoDeColunas.colunaB.map((item, index) => (
            <ItemAssociacao key={index} item={item} mostrarResposta={false} />
          ))}
        </Grid>
      </Grid>
    )
    return (
      <QuestaoSingularNaProva
        questaoProcessada={questaoProcessada}
        dragHandleProps={dragHandleProps}
        variant={variant}
        classNames={{ barraExpandir: classes.barraExpandir }}
        conteudoContraido={<MpParser>{questaoProcessada.enunciado}</MpParser>}
        conteudoExpandido={conteudoItemAssociacao}
      />
    )
  }
}
