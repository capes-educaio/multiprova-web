import React, { Component } from 'react'

import DeleteIcon from '@material-ui/icons/DeleteOutline'

import { store } from 'utils/store'

import { MpIconButton } from 'common/MpIconButton'

import { CampoObjeto, CampoSlate } from 'localModules/ReduxForm'

import { CampoRotulo } from '../CampoRotulo'
import { getItemVazio } from '../getItemVazio'

import { propTypes } from './propTypes'

export class CampoItemComponent extends Component {
  static propTypes = propTypes
  _deveImpedirAdd

  get _tamanho() {
    const { idColunaPai } = this.props
    return store.getState().SimpleReduxForm__values[idColunaPai].length
  }

  _addArray = () => {
    if (this._deveImpedirAdd) {
      this._deveImpedirAdd = false
      return
    }
    const { idColunaPai, refs, updateB, isColunaA, id } = this.props
    const index = this.props.accessor
    const isUltimoCampo = index + 1 === this._tamanho
    if (isUltimoCampo && refs[idColunaPai]) {
      refs[idColunaPai].addField(getItemVazio())
      if (isColunaA) updateB()
    }
    refs[`conteudo-${id}`].forceUpdate()
  }

  _deleteField = () => {
    const { idColunaPai, refs, isColunaA, reordenarB } = this.props
    const index = this.props.accessor
    if (this._tamanho - 2 === index) this._deveImpedirAdd = true
    if (refs[idColunaPai]) {
      refs[idColunaPai].deleteField(index)
      if (isColunaA) reordenarB(index)
    }
  }

  render() {
    const { id, accessor, classes, isColunaA, tamanhoColunaA, strings } = this.props
    const propsRotulo = {
      isColunaA: isColunaA,
      index: accessor,
      tamanhoColunaA: tamanhoColunaA,
      hidden: accessor + 1 === this._tamanho,
    }

    return (
      <CampoObjeto id={id} accessor={accessor} wrapperProps={{ className: classes.root }}>
        <CampoRotulo id={`rotulo-${id}`} accessor="rotulo" {...propsRotulo} />
        <CampoSlate
          id={`conteudo-${id}`}
          accessor="conteudo"
          wrapperProps={{ className: classes.texto }}
          onFocus={this._addArray}
          inputProps={{
            placeholder: accessor + 1 === this._tamanho ? strings.novoItem : null,
            floatingToolbar: true,
            adornment: {
              align: 'right',
              onEvent: 'hover',
              element:
                accessor + 1 === this._tamanho ? null : (
                  <MpIconButton
                    className={classes.excluirButton}
                    onClick={this._deleteField}
                    IconComponent={DeleteIcon}
                  />
                ),
            },
          }}
        />
      </CampoObjeto>
    )
  }
}
