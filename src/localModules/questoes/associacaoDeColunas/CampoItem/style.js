export const style = theme => ({
  root: {
    display: 'flex',
    alignItems: 'center',
    flexWrap: 'wrap',
    width: 'calc(100% - 10px)',
    marginTop: 10,
    marginRight: 10,
  },
  texto: {
    flexGrow: 1,
    marginLeft: 10,
    maxWidth: 'calc(100% - 60px)',
  },
  excluirButton: {
    marginLeft: 0,
    borderRadius: 5,
  },
})
