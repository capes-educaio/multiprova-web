import PropTypes from 'prop-types'

export const propTypes = {
  accessor: PropTypes.number.isRequired,
  id: PropTypes.string,
  isColunaA: PropTypes.bool,
  tamanhocolunaA: PropTypes.number,
  idColunaPai: PropTypes.string,
  updateB: PropTypes.func,
  reordenarB: PropTypes.func,
  // redux state
  refs: PropTypes.object.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
