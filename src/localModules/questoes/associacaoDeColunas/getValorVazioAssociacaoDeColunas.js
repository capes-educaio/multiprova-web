import { getValorVazioComum } from '../getValorVazioComum'
import { enumTipoQuestao } from 'utils/enumTipoQuestao'
import { getItemVazio } from './getItemVazio'

export const getValorVazio = () => {
  const associacaoDeColunas = {
    colunaA: [getItemVazio(), getItemVazio(), getItemVazio()],
    colunaB: [getItemVazio(), getItemVazio(), getItemVazio(), getItemVazio(), getItemVazio()],
  }
  return { tipo: enumTipoQuestao.tipoQuestaoAssociacaoDeColunas, associacaoDeColunas, ...getValorVazioComum() }
}
