import { montarQuestaoParaEnviarComum } from '../montarQuestaoParaEnviarComum'

export const montarQuestaoParaEnviar = dados => {
  let { associacaoDeColunas } = JSON.parse(JSON.stringify(dados))
  let { colunaA, colunaB } = associacaoDeColunas
  if (colunaA[colunaA.length - 1].conteudo === '<p></p>') {
    colunaA.pop()
  }
  if (colunaB[colunaB.length - 1].conteudo === '<p></p>') {
    colunaB.pop()
  }
  colunaA.forEach((value, index, array) => {
    colunaA[index].rotulo = index + 1
  })
  colunaB.forEach((value, index, array) => {
    colunaB[index].rotulo = parseInt(colunaB[index].rotulo)
  })
  associacaoDeColunas.colunaA = colunaA
  associacaoDeColunas.colunaB = colunaB
  return { associacaoDeColunas, ...montarQuestaoParaEnviarComum(dados) }
}
