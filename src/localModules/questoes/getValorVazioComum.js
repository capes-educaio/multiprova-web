import { store } from 'utils/store'
import { EMPTY_EDITOR } from 'utils/Editor'
import { enumStatusQuestao } from 'utils/enumStatusQuestao'
import { enumAnoEscolar } from 'utils/enumAnoEscolar'

export const getValorVazioComum = () => ({
  enunciado: EMPTY_EDITOR,
  elaborador: store.getState().usuarioAtual.data.nome,
  dificuldade: 2,
  tags: [],
  statusId: enumStatusQuestao.elaboracao.id,
  anoEscolar: enumAnoEscolar.ensinoSuperiorBasico,
  publico: false,
})
