import React, { Component } from 'react'
import { Alternativa } from 'common/Alternativa'
import { MpParser } from 'common/MpParser'

import { QuestaoSingularNaProva } from 'localModules/questoes/NaProvaComponents'

import { propTypes } from './propTypes'

export class MultiplaEscolhaNaProvaComponent extends Component {
  static propTypes = propTypes

  render() {
    const { classes, questaoProcessada, dragHandleProps, variant } = this.props
    return (
      <QuestaoSingularNaProva
        questaoProcessada={questaoProcessada}
        dragHandleProps={dragHandleProps}
        variant={variant}
        classNames={{ barraExpandir: classes.barraExpandir }}
        conteudoContraido={<MpParser>{questaoProcessada.enunciado}</MpParser>}
        conteudoExpandido={questaoProcessada.multiplaEscolha.alternativas.map(alternativa => (
          <Alternativa key={alternativa.letra} alternativa={alternativa} />
        ))}
      />
    )
  }
}
