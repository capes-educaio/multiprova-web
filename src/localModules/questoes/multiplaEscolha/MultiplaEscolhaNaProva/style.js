export const style = theme => {
  const barraBloco = '#949494'
  return {
    barraExpandir: {
      backgroundColor: barraBloco,
    },
    enunciado: {
      wordWrap: 'break-word',
      display: 'flex',
      flexWrap: 'wrap',
      padding: '0px 30px 20px 30px',
    },
  }
}
