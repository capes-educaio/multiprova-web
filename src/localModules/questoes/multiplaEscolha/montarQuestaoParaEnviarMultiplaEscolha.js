import { montarQuestaoParaEnviarComum } from '../montarQuestaoParaEnviarComum'

import { converterAlternativas } from './converterAlternativas'

export const montarQuestaoParaEnviar = dados => {
  let { multiplaEscolha } = JSON.parse(JSON.stringify(dados))
  let { alternativas, alternativasPorLinha } = multiplaEscolha
  if (alternativas[alternativas.length - 1] === '<p></p>') {
    alternativas.pop()
  }
  alternativas = converterAlternativas(alternativas)
  multiplaEscolha.alternativas = alternativas
  if (alternativasPorLinha === 'todas') {
    multiplaEscolha.alternativasPorLinha = alternativas.length
  }
  return { multiplaEscolha, ...montarQuestaoParaEnviarComum(dados) }
}
