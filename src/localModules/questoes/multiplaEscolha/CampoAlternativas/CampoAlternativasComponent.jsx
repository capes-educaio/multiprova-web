import React, { Component } from 'react'

import IconButton from '@material-ui/core/IconButton'

import DeleteIcon from '@material-ui/icons/DeleteOutline'
import DoneIcon from '@material-ui/icons/Done'

import { store } from 'utils/store'

import { CampoArray, CampoSlate } from 'localModules/ReduxForm'

import { propTypes } from './propTypes'

export class CampoAlternativasComponent extends Component {
  static propTypes = propTypes
  _deveImpedirAdd

  get _tamanho() {
    return store.getState().SimpleReduxForm__values[this.props.id].length
  }

  _addArray = index => () => {
    if (this._deveImpedirAdd && !(index + 1 === this._tamanho)) {
      this._deveImpedirAdd = false
      return
    }
    const { refs, id } = this.props
    const isUltimoCampo = index + 1 === this._tamanho
    if (isUltimoCampo && refs[id]) refs[id].addField('<p></p>')
    return
  }

  _deleteField = index => () => {
    const { refs, id } = this.props
    if (this._tamanho - 2 === index) this._deveImpedirAdd = true
    if (refs[id]) refs[id].deleteField(index)
  }

  _getAlternativaProp = index => {
    const { strings, classes } = this.props

    const otherIcon =
      index !== this._tamanho - 1 ? (
        <IconButton className={classes.deleteButton} onClick={this._deleteField(index)}>
          <DeleteIcon fontSize="small" />
        </IconButton>
      ) : null

    return {
      inputProps: {
        placeholder: index === this._tamanho - 1 ? strings.novaAlternativa : strings.alfabeto[index] + ')',
        floatingToolbar: true,
        adornment: {
          align: 'right',
          onEvent: index !== 0 ? 'hover' : false,
          element: index === 0 ? <DoneIcon style={{ color: 'mediumseagreen' }} /> : otherIcon,
        },
      },
      onFocus: this._addArray(index),
    }
  }

  render() {
    const { strings, campoArrayProps, id, accessor, classes } = this.props
    return (
      <CampoArray
        id={id}
        accessor={accessor}
        label={strings.alternativas}
        FieldComponent={CampoSlate}
        wrapperProps={{ className: classes.alternativas }}
        getFieldProps={this._getAlternativaProp}
        {...campoArrayProps}
      />
    )
  }
}
