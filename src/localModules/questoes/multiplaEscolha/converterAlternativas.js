import { converterAlternativa } from './converterAlternativa'

export const converterAlternativas = alternativas => {
  return alternativas.map(converterAlternativa)
}
