import { EMPTY_EDITOR } from 'utils/Editor'

import { montarValorInicialComum } from '../montarValorInicialComum'

export const montarValorInicial = dados => {
  const { multiplaEscolha } = dados
  let { alternativasPorLinha, alternativas } = multiplaEscolha
  if (alternativasPorLinha === alternativas.length) {
    alternativasPorLinha = 'todas'
  }
  multiplaEscolha.alternativas = alternativas.map(alternativa => alternativa.texto)
  multiplaEscolha.alternativas.push(EMPTY_EDITOR)
  multiplaEscolha.alternativasPorLinha = alternativasPorLinha
  return {
    ...montarValorInicialComum(dados),
    multiplaEscolha,
  }
}
