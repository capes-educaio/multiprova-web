import React, { Component } from 'react'

import { QuestaoFormPagePadrao } from '../../QuestaoFormPagePadrao'
import { MultiplaEscolhaForm } from '../MultiplaEscolhaForm'

import { propTypes } from './propTypes'

export class MultiplaEscolhaFormPageComponent extends Component {
  static propTypes = propTypes

  render() {
    return <QuestaoFormPagePadrao {...this.props} FormComponent={MultiplaEscolhaForm} />
  }
}
