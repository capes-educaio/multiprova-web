export const style = theme => ({
  secao: {
    padding: '20px 20px 20px 20px',
  },
  carregando: {
    display: 'flex',
    justifyContent: 'center',
  },
  questao: {
    color: theme.palette.primary.main,
    fontFamily: 'Roboto, Helvetica, Arial, sans-serif',
    padding: '20px 20px',
    '& .label': {
      margin: '0 0 10px 0',
      color: theme.palette.primary.main,
      fontSize: '16px',
      display: 'inline-block',
    },
    '& > div:not(:last-child)': {
      marginBottom: '20px',
    },
    '& .displayNone': {
      display: 'none',
    },
    '& .mensagemValidacao': {
      display: 'flex',
      justifyContent: 'flex-end',
      color: 'indianred',
      fontSize: '14px',
    },
  },
  botaoSalvar: {
    margin: '20px 0 20px 20px',
  },
  botaoAddQuestao: {
    position: 'sticky',
    bottom: 10,
    left: 9999,
    margin: '0 20px 0 0',
    zIndex: 1000,
  },
})
