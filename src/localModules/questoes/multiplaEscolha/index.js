import { MultiplaEscolhaNaProva } from './MultiplaEscolhaNaProva'

export { getValorVazio } from './getValorVazioMultiplaEscolha'
export { montarQuestaoParaEnviar } from './montarQuestaoParaEnviarMultiplaEscolha'
export { montarValorInicial } from './montarValorInicialMultiplaEscolha'
export { MultiplaEscolhaFormPage as Component } from './MultiplaEscolhaFormPage'
export const VerNaProvaComponent = MultiplaEscolhaNaProva
export const EditarNaProvaComponent = MultiplaEscolhaNaProva
export const urlMatches = 'criar-multipla-escolha'
