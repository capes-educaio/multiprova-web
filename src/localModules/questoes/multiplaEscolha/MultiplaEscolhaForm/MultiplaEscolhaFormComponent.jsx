import React, { Component } from 'react'
import uid from 'uuid/v4'

import MenuItem from '@material-ui/core/MenuItem'

import { SimpleReduxForm, CampoSelect, CampoObjeto, CampoTag } from 'localModules/ReduxForm'

import { QuestaoCamposComuns } from '../../QuestaoCamposComuns'
import { CampoSubQuestaoBody } from '../../CampoSubQuestaoBody'

import { CampoAlternativas } from '../CampoAlternativas'

import { propTypes } from './propTypes'

export class MultiplaEscolhaFormComponent extends Component {
  static propTypes = propTypes
  _formId = uid()
  _alternativasId = uid()
  _multiplaEscolhaId = uid()
  _alternativasPorLinhaId = uid()

  render() {
    const { valor, strings, onRef, onValid, onChange, variant, id, accessor, wrapperProps, label } = this.props

    const campoMultiplaEscolha = (
      <CampoObjeto
        key={this._multiplaEscolhaId}
        id={'multiplaEscolha' + this._multiplaEscolhaId}
        accessor="multiplaEscolha"
      >
        <CampoAlternativas id={'alternativas' + this._alternativasId} accessor="alternativas" />
        <CampoSelect
          id={'alternativasPorLinha' + this._alternativasPorLinhaId}
          accessor="alternativasPorLinha"
          helperText={strings.alternativasPorLinha}
        >
          <MenuItem value={1}>{strings.quantidadeAlternativasPorLinha[0]}</MenuItem>
          <MenuItem value={2}>{strings.quantidadeAlternativasPorLinha[1]}</MenuItem>
          <MenuItem value={3}>{strings.quantidadeAlternativasPorLinha[2]}</MenuItem>
          <MenuItem value="todas">{strings.quantidadeAlternativasPorLinha[3]}</MenuItem>
        </CampoSelect>
      </CampoObjeto>
    )
    const campoTag = (
      <CampoTag
        key={this._tagId}
        id={'tags' + this._formId}
        accessor="tags"
        tagProps={{ placeholder: strings.adicionarTags }}
      />
    )
    return (
      <QuestaoCamposComuns>
        {({ campoEnunciado, campoDificuldade, campoElaborador, campoStatus, campoAnoEscolar, campoId }) => {
          if (variant === 'dentroDeBloco')
            return (
              <CampoObjeto
                id={id}
                label={label}
                wrapperProps={wrapperProps}
                accessor={accessor}
                BodyComponent={CampoSubQuestaoBody}
              >
                {campoId}
                {campoEnunciado}
                {campoMultiplaEscolha}
                {campoAnoEscolar}
                {campoDificuldade}
                {campoElaborador}
                {campoTag}
              </CampoObjeto>
            )
          return (
            <SimpleReduxForm id={this._formId} valor={valor} onRef={onRef} onChange={onChange} onValid={onValid}>
              {campoStatus}
              {campoEnunciado}
              {campoMultiplaEscolha}
              {campoAnoEscolar}
              {campoDificuldade}
              {campoElaborador}
              {campoTag}
            </SimpleReduxForm>
          )
        }}
      </QuestaoCamposComuns>
    )
  }
}
