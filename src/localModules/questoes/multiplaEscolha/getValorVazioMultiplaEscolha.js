import { EMPTY_EDITOR } from 'utils/Editor'
import { enumTipoQuestao } from 'utils/enumTipoQuestao'
import { getValorVazioComum } from '../getValorVazioComum'

const getAlternativas = quantAlternativas => new Array(quantAlternativas).fill(EMPTY_EDITOR)

export const getValorVazio = quantAlternativas => ({
  tipo: enumTipoQuestao.tipoQuestaoMultiplaEscolha,
  multiplaEscolha: {
    respostaCerta: 'a',
    alternativas: getAlternativas(quantAlternativas),
    alternativasPorLinha: 1,
  },
  ...getValorVazioComum(),
})
