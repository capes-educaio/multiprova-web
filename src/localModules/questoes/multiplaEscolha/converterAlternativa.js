import { ALFABETO } from 'utils/alfabeto'

export const converterAlternativa = (texto, index) => ({
  letra: ALFABETO[index],
  texto,
})
