import PropTypes from 'prop-types'

export const propTypes = {
  // redux state
  usuarioAtual: PropTypes.object.isRequired,
  // redux actions
  selectListaTags: PropTypes.func.isRequired,
  tipoBancoQuestoes: PropTypes.number,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
