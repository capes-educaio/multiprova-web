import { bindActionCreators } from 'redux'
import { actions } from 'utils/actions'

export function mapStateToProps(state) {
  return {
    strings: state.strings,
    usuarioAtual: state.usuarioAtual,
    tipoBancoQuestoes: state.tipoBancoQuestoes,
  }
}

export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      listaDialogPush: actions.push.listaDialog,
    },
    dispatch,
  )
}
