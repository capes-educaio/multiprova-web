import React from 'react'
import { shallow } from 'enzyme'

import { del } from 'api'
import { enumTipoQuestao } from 'utils/enumTipoQuestao'
import { QuestaoCardGerenciamentoQuestoesComponent } from '../QuestaoCardGerenciamentoQuestoesComponent'

jest.mock('api', () => ({
  del: jest.fn().mockResolvedValue({}),
}))

const questao = {
  id: '75cc4eff-6d71-4d55-821a-4ff6b8653695',
  dataCadastro: '2018-07-25T14:48:55.348Z',
  dataUltimaAlteracao: '2018-07-25T14:48:55.348Z',
  enunciado: '<p>Qual a metade de 2+2?</p>',
  dificuldade: 1,
  tagIds: [],
  tipo: enumTipoQuestao.tipoQuestaoMultiplaEscolha,
  multiplaEscolha: {
    respostaCerta: 'a',
    alternativas: [
      {
        texto: '<p>150 reais</p>',
        letra: 'a',
      },
      {
        texto: '<p>450 reais</p>',
        letra: 'b',
      },
    ],
  },
}

const defaultProps = {
  instancia: questao,
  atualizarCardDisplay: jest.fn(),
  estaInicialmenteExpandido: false,
  enunciado: questao.enunciado,
  indexColuna: 1,
  history: { push: jest.fn(), location: { pathname: '/' } },
  onRef: jest.fn(),
  contexto: {
    rotas: {
      back: {
        getExclusao: jest.fn(id => `/questoes/${id}`),
      },
    },
  },
  // redux actions
  listaDialogPush: jest.fn(),
  // style
  classes: {},
  // strings
  strings: {
    editar: '',
    excluir: '',
    cancelar: '',
    ok: '',
    desejaExcluirQuestao: '',
  },
}

describe('Testando <QuestaoCardGerenciamentoQuestoesComponent /> openDialogExcluir', () => {
  it('Testa se a função openDialogExcluir chama o push com idQuestao passado ', () => {
    const wrapper = shallow(<QuestaoCardGerenciamentoQuestoesComponent {...defaultProps} />)
    const spyhandleCloseMenu = jest.spyOn(wrapper.instance(), 'handleCloseMenu')

    wrapper.instance().excluirQuestaoUsuario = jest.fn()
    const ID_QUESTAO = 1
    const RESULTADO_ESPERADO = {
      titulo: '',
      texto: '',
      actions: [
        {
          id: 'confirm-dialog',
          texto: '',
          onClick: wrapper.instance().excluirQuestaoUsuario(ID_QUESTAO),
        },
        {
          id: 'cancel-dialog',
          texto: '',
        },
      ],
    }
    wrapper.instance().openDialogExcluir(ID_QUESTAO)()
    expect(spyhandleCloseMenu).toHaveBeenCalled()
    expect(wrapper.instance().props.listaDialogPush).toHaveBeenCalledWith(RESULTADO_ESPERADO)
    jest.clearAllMocks()
  })
})

describe('Testando <QuestaoCardGerenciamentoQuestoesComponent /> handleClickMenu', () => {
  it('Testa se a função handleClickMenu que troca o valor da variável anchorEl do state ', () => {
    const wrapper = shallow(<QuestaoCardGerenciamentoQuestoesComponent {...defaultProps} />)
    const VALOR_INICIAL = 'usuario'
    const VALOR_ESPERADO = 'questao'
    wrapper.setState({ anchorEl: VALOR_INICIAL })
    const event = { currentTarget: VALOR_ESPERADO }
    wrapper.instance().handleClickMenu(event)
    expect(wrapper.state('anchorEl')).toBe(VALOR_ESPERADO)
    expect(wrapper.state('anchorEl')).not.toBe(VALOR_INICIAL)
    jest.clearAllMocks()
  })
})

describe('Testando <QuestaoCardGerenciamentoQuestoesComponent /> handleCloseMenu', () => {
  it('Testa se a função handleCloseMenu que troca o valor da variável anchorEl do state para null ', () => {
    const wrapper = shallow(<QuestaoCardGerenciamentoQuestoesComponent {...defaultProps} />)
    const VALOR_INICIAL = 'usuario'
    wrapper.setState({ anchorEl: VALOR_INICIAL })
    wrapper.instance().handleCloseMenu()
    expect(wrapper.state('anchorEl')).toBeNull()
    expect(wrapper.state('anchorEl')).not.toBe(VALOR_INICIAL)
    jest.clearAllMocks()
  })
})

describe('Testando <QuestaoCardGerenciamentoQuestoesComponent /> handleCloseExcluir', () => {
  it('Testa se a função handleCloseExcluir chama handleCloseMenu e seta as variaveis de estado alertaExcluirQuestaoAberto=false e idQuestaoSelecionada=0 ', () => {
    const wrapper = shallow(<QuestaoCardGerenciamentoQuestoesComponent {...defaultProps} />)
    const spy = jest.spyOn(wrapper.instance(), 'handleCloseMenu')
    const CONFIRMA_EXCLUIR = false
    const ID_QUESTAO_RESET = 0
    wrapper.instance().handleCloseExcluir(CONFIRMA_EXCLUIR)
    expect(wrapper.state('idQuestaoSelecionada')).toBe(ID_QUESTAO_RESET)
    expect(wrapper.state('alertaExcluirQuestaoAberto')).toBeFalsy()
    expect(spy).toHaveBeenCalled()
    jest.clearAllMocks()
    spy.mockRestore()
  })
  it('Testa se a função handleCloseExcluir chama excluirQuestaoUsuario', () => {
    const wrapper = shallow(<QuestaoCardGerenciamentoQuestoesComponent {...defaultProps} />)
    const spy = jest.spyOn(wrapper.instance(), 'excluirQuestaoUsuario')
    const CONFIRMA_EXCLUIR = true
    wrapper.instance().handleCloseExcluir(CONFIRMA_EXCLUIR)
    expect(spy).toHaveBeenCalled()
    jest.clearAllMocks()
    spy.mockRestore()
  })
})

describe('Testando <QuestaoCardGerenciamentoQuestoesComponent /> handleClickExcluir', () => {
  it('Testa se a função handleClickExcluir seta as variaveis de estado alertaExcluirQuestaoAberto=true e idQuestaoSelecionada={id passado} ', () => {
    const wrapper = shallow(<QuestaoCardGerenciamentoQuestoesComponent {...defaultProps} />)
    const ID_QUESTAO_EXCLUIR = questao.id
    wrapper.instance().handleClickExcluir(ID_QUESTAO_EXCLUIR)
    expect(wrapper.state('idQuestaoSelecionada')).toBe(ID_QUESTAO_EXCLUIR)
    expect(wrapper.state('alertaExcluirQuestaoAberto')).toBeTruthy()
    jest.clearAllMocks()
  })
})

describe('Testando <QuestaoCardGerenciamentoQuestoesComponent /> excluirQuestaoUsuario', () => {
  it('Testa se a função excluirQuestaoUsuario chama a função atualizarPagina', async () => {
    const wrapper = shallow(<QuestaoCardGerenciamentoQuestoesComponent {...defaultProps} />)
    const spy = jest.spyOn(wrapper.instance(), 'atualizarPagina')
    await wrapper.instance().excluirQuestaoUsuario(questao.id)()
    expect(del).toHaveBeenCalledWith(`/questoes/${questao.id}`)
    expect(spy).toHaveBeenCalled()
    jest.clearAllMocks()
    spy.mockRestore()
  })
  it('Testa se a função excluirQuestaoUsuario chama a função handleCloseMenu', async () => {
    const wrapper = shallow(<QuestaoCardGerenciamentoQuestoesComponent {...defaultProps} />)
    const spy = jest.spyOn(wrapper.instance(), 'handleCloseMenu')
    await wrapper.instance().excluirQuestaoUsuario(questao.id)()
    expect(del).toHaveBeenCalledWith(`/questoes/${questao.id}`)
    expect(spy).toHaveBeenCalled()
    jest.clearAllMocks()
    spy.mockRestore()
  })
  it('Testa se a função excluirQuestaoUsuario chama o erro', async () => {
    const wrapper = shallow(<QuestaoCardGerenciamentoQuestoesComponent {...defaultProps} />)
    const spyHandleCloseMenu = jest.spyOn(wrapper.instance(), 'handleCloseMenu')
    const spyAtualizarPagina = jest.spyOn(wrapper.instance(), 'atualizarPagina')
    const ID_QUESTAO_ERRO = 0
    del.mockRejectedValue(true)
    await wrapper.instance().excluirQuestaoUsuario(ID_QUESTAO_ERRO)()
    expect(del).toHaveBeenCalledWith(`/questoes/${ID_QUESTAO_ERRO}`)
    expect(spyHandleCloseMenu).not.toHaveBeenCalled()
    expect(spyAtualizarPagina).not.toHaveBeenCalled()
    jest.clearAllMocks()
    spyHandleCloseMenu.mockRestore()
    spyAtualizarPagina.mockRestore()
  })
})
