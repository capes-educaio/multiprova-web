import PropTypes from 'prop-types'

const { shape, object } = PropTypes
export const propTypes = {
  instancia: PropTypes.object.isRequired,
  contexto: shape({
    rotas: object.isRequired,
    tipoVisualizacao: PropTypes.object.isRequired,
  }).isRequired,
  atualizarCardDisplay: PropTypes.object.isRequired,
  // redux state
  usuarioAtual: PropTypes.object.isRequired,
  // redux actions
  listaDialogPush: PropTypes.func.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
