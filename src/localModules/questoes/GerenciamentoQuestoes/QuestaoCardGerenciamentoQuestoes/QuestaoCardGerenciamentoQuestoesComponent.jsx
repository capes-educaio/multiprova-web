import React, { Component } from 'react'

import IconButton from '@material-ui/core/IconButton'
import Menu from '@material-ui/core/Menu'
import MenuItem from '@material-ui/core/MenuItem'
import MoreVertIcon from '@material-ui/icons/MoreVert'
import Delete from '@material-ui/icons/Delete'
import EditIcon from '@material-ui/icons/Edit'
import PublishIcon from '@material-ui/icons/Publish'
import ShareIcon from '@material-ui/icons/Share'
import LockIcon from '@material-ui/icons/Lock'
import PlayForWork from '@material-ui/icons/PlayForWork'
import Close from '@material-ui/icons/Close'
import VerticalAlignBottomIcon from '@material-ui/icons/VerticalAlignBottom'

import { appConfig } from 'appConfig'
import { Snackbar } from 'utils/Snackbar'

import { QuestaoCard } from 'common/QuestaoCard'
import { CompartilharQuestaoDialog } from 'common/CompartilharQuestaoDialog'
import { del, patch, get } from 'api'

import { questaoUtils } from 'localModules/questoes/utils/QuestaoUtils'
import ExportIcon from 'localModules/icons/ExportIconCardMenu'
import { bancoDeQuestoes } from 'utils/enumBancoDeQuestoes'

export class QuestaoCardGerenciamentoQuestoesComponent extends Component {
  idQuestao
  state = {
    anchorEl: null,
    openShareDialog: false,
    snackbarIsOpen: false,
  }

  get editarQuestao() {
    return questaoUtils.irParaEditarQuestao(this.props.location)
  }

  excluirQuestaoUsuario = idQuestao => () => {
    const { contexto } = this.props
    let url = contexto.rotas.back.getExclusao(idQuestao)
    del(url).then(() => {
      this.atualizarPagina()
      this.handleCloseMenu()
    })
  }

  publicarQuestaoUsuario = idQuestao => () => {
    const questaoPublica = { publico: true }
    try {
      patch(`questoes/${idQuestao}`, questaoPublica).then(() => {
        this.props.instancia.publico = true
        this.setState({ snackbarIsOpen: true })
      })
    } catch (error) {
      console.error(error)
    }
  }

  privarQuestaoUsuario = idQuestao => () => {
    const questaoPublica = { publico: false }
    try {
      patch(`questoes/${idQuestao}`, questaoPublica).then(() => {
        this.props.instancia.publico = false
        this.setState({ snackbarIsOpen: true })
      })
    } catch (error) {
      console.error(error)
    }
  }

  openDialogPublicar = idQuestao => () => {
    this.handleCloseMenu()
    const { listaDialogPush, strings } = this.props
    const dialogConfig = {
      titulo: strings.publicarQuestao,
      texto: strings.desejaPublicarQuestao,
      actions: [
        {
          texto: strings.sim,
          onClick: this.publicarQuestaoUsuario(idQuestao),
        },
        {
          texto: strings.nao,
        },
      ],
    }
    listaDialogPush(dialogConfig)
  }

  openDialogRejeitar = idQuestao => () => {
    this.handleCloseMenu()
    const { listaDialogPush, strings } = this.props
    const dialogConfig = {
      titulo: strings.rejeitarQuestao,
      texto: strings.desejaRejeitarQuestao,
      actions: [
        {
          texto: strings.sim,
          onClick: this.rejeitarQuestao(idQuestao),
        },
        {
          texto: strings.nao,
        },
      ],
    }
    listaDialogPush(dialogConfig)
  }

  openDialogReceber = idQuestao => () => {
    this.receberQuestao(idQuestao).then(questaoId => {
      this.handleCloseMenu()
      const { listaDialogPush, strings } = this.props
      const dialogConfig = {
        titulo: strings.receberQuestao,
        texto: strings.desejaEditarQuestao,
        actions: [
          {
            texto: strings.sim,
            onClick: this.editarQuestao(questaoId),
          },
          {
            texto: strings.agoraNao,
          },
        ],
      }
      listaDialogPush(dialogConfig)
    })
  }

  openDialogTornarPrivada = idQuestao => () => {
    this.handleCloseMenu()
    const { listaDialogPush, strings } = this.props
    const dialogConfig = {
      titulo: strings.privarQuestao,
      texto: strings.desejaTornarPrivadaQuestao,
      actions: [
        {
          texto: strings.sim,
          onClick: this.privarQuestaoUsuario(idQuestao),
        },
        {
          texto: strings.nao,
        },
      ],
    }
    listaDialogPush(dialogConfig)
  }

  openDialogExcluir = idQuestao => async () => {
    this.handleCloseMenu()
    const { listaDialogPush, strings } = this.props
    const dialogStrings = {
      titulo: strings.excluir,
      texto: strings.desejaExcluirQuestao,
    }

    const dialogConfig = {
      ...dialogStrings,
      actions: [
        {
          id: 'confirm-dialog',
          texto: strings.ok,
          onClick: this.excluirQuestaoUsuario(idQuestao),
        },
        {
          id: 'cancel-dialog',
          texto: strings.cancelar,
        },
      ],
    }
    listaDialogPush(dialogConfig)
  }

  copiarQuestao = id => {
    const { atualizarCardDisplay } = this.props.contexto
    return new Promise((resolve, reject) => {
      get(`questoes/${id}/copiar`)
        .then(res => {
          resolve(res.data.data.id)
          atualizarCardDisplay()
        })
        .catch(() => {
          reject()
        })
    })
  }

  openDialogCopiar = idQuestao => () => {
    this.copiarQuestao(idQuestao).then(id => {
      this.handleCloseMenu()
      const { listaDialogPush, strings } = this.props
      const dialogConfig = {
        titulo: strings.copiarQuestao,
        texto: strings.questaoCopiada,
        actions: [
          {
            texto: strings.sim,
            onClick: this.editarQuestao(id),
          },
          {
            texto: strings.agoraNao,
          },
        ],
      }
      listaDialogPush(dialogConfig)
    })
  }

  atualizarPagina = () => this.props.atualizarCardDisplay()

  handleClickMenu = event => {
    this.setState({ anchorEl: event.currentTarget })
  }

  handleClickExcluir = idQuestao => {
    this.setState({
      alertaExcluirQuestaoAberto: true,
      idQuestaoSelecionada: idQuestao,
    })
  }
  handleOpenShareDialog = () => {
    this.handleCloseMenu()
    this.setState({ openShareDialog: true })
  }
  handleCloseExcluir = confirmaExcluir => {
    if (confirmaExcluir !== true) {
      this.handleCloseMenu()
      this.setState({
        alertaExcluirQuestaoAberto: false,
        idQuestaoSelecionada: 0,
      })
    } else this.excluirQuestaoUsuario()
  }

  handleCloseMenu = () => {
    this.setState({ anchorEl: null })
  }

  snackbarShoudClose = () => {
    this.setState({ snackbarIsOpen: false })
  }

  receberQuestao = id => {
    const { atualizarCardDisplay } = this.props.contexto
    return new Promise((resolve, reject) => {
      get(`questoes-cedidas/${id}/aceitar`)
        .then(res => {
          resolve(res.data.data.id)
          atualizarCardDisplay()
        })
        .catch(() => {
          reject()
        })
    })
  }

  rejeitarQuestao = id => () => {
    const { atualizarCardDisplay } = this.props.contexto
    get(`questoes-cedidas/${id}/rejeitar`).then(res => {
      atualizarCardDisplay()
    })
  }

  render() {
    const { classes, instancia, strings, tipoBancoQuestoes } = this.props
    const { anchorEl, snackbarIsOpen, openShareDialog } = this.state

    const menuHeader = (
      <div>
        <Snackbar
          variant="success"
          open={snackbarIsOpen}
          onClose={this.snackbarShoudClose}
          message={<span id="snackbar-sucesso">{strings.asAlteracoesForamSalvas}</span>}
          autoHideDuration={3000}
        />
        <IconButton onClick={this.handleClickMenu}>
          <MoreVertIcon />
        </IconButton>
        <div>
          <Menu
            anchorEl={anchorEl}
            getContentAnchorEl={null}
            anchorOrigin={{ vertical: 'center', horizontal: 'center' }}
            transformOrigin={{ vertical: 'top', horizontal: 'right' }}
            open={Boolean(anchorEl)}
            onClose={this.handleCloseMenu}
          >
            <MenuItem id="item-menu-editar" onClick={this.editarQuestao(instancia.id)}>
              <EditIcon className={classes.menuIcon} />
              <span>{strings.editar}</span>
            </MenuItem>
            {!appConfig.projeto.isEducaio && (
              <MenuItem id="item-menu-exportar" onClick={() => questaoUtils.exportarQuestao(instancia)}>
                <ExportIcon className={classes.menuIcon} />
                <span>{strings.exportar}</span>
              </MenuItem>
            )}
            {!instancia.publico && (
              <MenuItem onClick={this.openDialogPublicar(instancia.id)}>
                <PublishIcon className={classes.menuIcon} />
                <span>{strings.publicarQuestao}</span>
              </MenuItem>
            )}
            {instancia.publico && (
              <MenuItem onClick={this.openDialogTornarPrivada(instancia.id)}>
                <LockIcon className={classes.menuIcon} />
                <span>{strings.privarQuestao}</span>
              </MenuItem>
            )}
            {
              <MenuItem onClick={this.handleOpenShareDialog}>
                <ShareIcon className={classes.menuIcon} />
                <span>{strings.compartilhar}</span>
              </MenuItem>
            }

            <MenuItem id="item-menu-excluir" onClick={this.openDialogExcluir(instancia.id)}>
              <Delete className={classes.menuIcon} />
              <span>{strings.excluir}</span>
            </MenuItem>
          </Menu>
        </div>
      </div>
    )

    const menuHeaderQuestoesPublicas = (
      <div>
        <Snackbar
          variant="success"
          open={snackbarIsOpen}
          onClose={this.snackbarShoudClose}
          message={<span id="snackbar-sucesso">{strings.asAlteracoesForamSalvas}</span>}
          autoHideDuration={3000}
        />
        <IconButton onClick={this.handleClickMenu}>
          <MoreVertIcon />
        </IconButton>
        <div>
          <Menu
            anchorEl={anchorEl}
            getContentAnchorEl={null}
            anchorOrigin={{ vertical: 'center', horizontal: 'center' }}
            transformOrigin={{ vertical: 'top', horizontal: 'right' }}
            open={Boolean(anchorEl)}
            onClose={this.handleCloseMenu}
          >
            <MenuItem id="item-menu-copiar" onClick={this.openDialogCopiar(instancia.id)}>
              <VerticalAlignBottomIcon className={classes.menuIcon} />
              <span>{strings.copiarQuestao}</span>
            </MenuItem>
          </Menu>
        </div>
      </div>
    )

    const menuHeaderQuestoesCompartilhadas = (
      <div>
        <Snackbar
          variant="success"
          open={snackbarIsOpen}
          onClose={this.snackbarShoudClose}
          message={<span id="snackbar-sucesso">{strings.asAlteracoesForamSalvas}</span>}
          autoHideDuration={3000}
        />
        <IconButton onClick={this.handleClickMenu}>
          <MoreVertIcon />
        </IconButton>
        <div>
          <Menu
            anchorEl={anchorEl}
            getContentAnchorEl={null}
            anchorOrigin={{ vertical: 'center', horizontal: 'center' }}
            transformOrigin={{ vertical: 'top', horizontal: 'right' }}
            open={Boolean(anchorEl)}
            onClose={this.handleCloseMenu}
          >
            <MenuItem onClick={this.openDialogReceber(instancia.id)}>
              <PlayForWork className={classes.menuIcon} />
              <span>{strings.receberQuestao}</span>
            </MenuItem>
            <MenuItem onClick={this.openDialogRejeitar(instancia.id)}>
              <Close className={classes.menuIcon} />
              <span>{strings.rejeitarQuestao}</span>
            </MenuItem>
          </Menu>
        </div>
      </div>
    )

    const mapaHeaderTipoBancoDeQuestoes = {
      [`${bancoDeQuestoes.minhas}`]: menuHeader,
      [`${bancoDeQuestoes.publicas}`]: menuHeaderQuestoesPublicas,
      [`${bancoDeQuestoes.compartilhadas}`]: menuHeaderQuestoesCompartilhadas,
    }

    return (
      <span>
        {!!instancia.id && (
          <CompartilharQuestaoDialog
            id={instancia.id}
            open={openShareDialog}
            onClose={() => this.setState({ openShareDialog: false })}
          />
        )}
        <QuestaoCard questao={instancia} opcoes={mapaHeaderTipoBancoDeQuestoes[tipoBancoQuestoes] || menuHeader} />
      </span>
    )
  }
}
