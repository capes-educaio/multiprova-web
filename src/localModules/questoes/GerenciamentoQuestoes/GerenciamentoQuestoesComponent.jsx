import React, { Component } from 'react'

import { Button, Select, FormControl, InputLabel, MenuItem } from '@material-ui/core'

import { appConfig } from 'appConfig'
import { tipoMap } from 'localModules/questoes/tipoMap'
import { setUrlState } from 'localModules/urlState'
import { imagensParaUrlQuestao } from 'localModules/questoes/utils/conversaoImagensQuestao'

import { TituloDaPagina } from 'common/TituloDaPagina'
import { Busca } from 'common/Busca'
import { FormQuestaoFiltroPadrao } from 'common/FormQuestaoFiltroPadrao'
import { FormQuestaoFiltroExpandido } from 'common/FormQuestaoFiltroExpandido'
import { PaginaPaper } from 'common/PaginaPaper'
import { BotaoComMenu } from 'common/BotaoComMenu'
import { CardDisplay } from 'common/CardDisplay'
import { Importar } from 'common/Importar'

import { montarFiltroQuestao } from 'utils/montarFiltroQuestao'
import { setListaTags } from 'utils/compositeActions'
import { bancoDeQuestoes } from 'utils/enumBancoDeQuestoes'
import { enumTipoQuestao } from 'utils/enumTipoQuestao'

import { QuestaoCardGerenciamentoQuestoes } from './QuestaoCardGerenciamentoQuestoes'
import { propTypes } from './propTypes'
import { MenuListAddQuestao } from './MenuListAddQuestao'

export class GerenciamentoQuestoesComponent extends Component {
  static propTypes = propTypes

  cardDisplay

  constructor(props) {
    super(props)
    setListaTags()
  }

  componentDidUpdate() {
    const { carregando, selectCarregando, tipoBancoQuestoes } = this.props
    if (carregando && tipoBancoQuestoes === 2) {
      selectCarregando(false)
      this.handleChange({ target: { value: 2 } })
    }
  }

  get tipoBancoQuestoes() {
    const { pathname } = this.props.history.location
    const bancoDeQuestoesName = pathname.split('/')[2]
    const banco = bancoDeQuestoesName ? bancoDeQuestoes[bancoDeQuestoesName] : bancoDeQuestoes.minhas
    return banco
  }

  get rotasMinhasQuestoes() {
    const { usuarioAtual } = this.props
    return {
      back: {
        get: `/usuarios/${usuarioAtual.data.id}/questoes/`,
        count: `/usuarios/${usuarioAtual.data.id}/questoes/count`,
        getExclusao: id => `/questoes/${id}`,
        create: `/questoes/`,
      },
      front: {
        add: '/questao',
        getEdicao: id => `questao/${id}`,
      },
    }
  }

  get rotasQuestoesPublicas() {
    return {
      back: {
        get: `/questoes/questoes-publicas`,
        count: `/questoes/questoes-publicas/count`,
        getExclusao: id => `/questoes/${id}`,
        create: `/questoes/`,
      },
      front: {
        add: '/questao',
        getEdicao: id => `/questao/${id}`,
      },
    }
  }

  get rotasQuestoesCompartilhadas() {
    const { usuarioAtual } = this.props
    return {
      back: {
        get: `/usuarios/${usuarioAtual.data.id}/questoes-cedidas`,
        count: `/usuarios/${usuarioAtual.data.id}/questoes-cedidas/count`,
        getExclusao: id => `/questoes/${id}`,
        create: `/questoes/`,
      },
      front: {
        add: '/questao',
        getEdicao: id => `/questao/${id}`,
      },
    }
  }

  rotas = bancoOrigem => {
    const { tipoBancoQuestoes } = this.props
    const tipoVisualizacao = bancoOrigem !== undefined ? bancoOrigem : tipoBancoQuestoes
    switch (tipoVisualizacao) {
      case bancoDeQuestoes.publicas:
        return this.rotasQuestoesPublicas
      case bancoDeQuestoes.compartilhadas:
        return this.rotasQuestoesCompartilhadas
      default:
        return this.rotasMinhasQuestoes
    }
  }

  prepararQuestaoParaEnviar = async stringQuestao => {
    const questao = JSON.parse(await imagensParaUrlQuestao(stringQuestao))
    const { tipo } = questao
    delete questao.tags
    if (tipo === enumTipoQuestao.tipoQuestaoBloco) {
      const questoesSemTags = questao.bloco.questoes.map(questao => {
        delete questao.tags
        return questao
      })
      questao.bloco.questoes = questoesSemTags
    }
    if (tipoMap[tipo] && tipoMap[tipo].montarQuestaoParaEnviar) {
      return tipoMap[tipo].montarQuestaoParaEnviar(questao)
    } else {
      console.error('Falha ao procurar montarQuestaoParaEnviar em tipo:' + tipo)
      return
    }
  }

  atualizarCardDisplay = props => this.cardDisplay && this.cardDisplay.fetchInstancias(props)

  handleChange = event => {
    const rotas = this.rotas(event.target.value)
    const bancoDeQuestoesName = Object.keys(bancoDeQuestoes)[event.target.value]
    setUrlState({ pathname: `/questoes/${bancoDeQuestoesName}` })
    this.props.selectTipoBancoQuestoes(event.target.value)
    if (rotas) {
      this.atualizarCardDisplay({
        bancoDeQuestoesOrigem: event.target.value,
        quantidadeUrl: rotas.back.count,
        fetchInstanciasUrl: rotas.back.get,
        location: this.props.history.location,
      })
    }
  }

  selectBancoDeQuestoes = () => {
    const { strings } = this.props

    return (
      <FormControl>
        <InputLabel>{strings.bancoDeQuestoes}</InputLabel>
        <Select
          inputProps={{
            name: 'tipoVisualizacao',
          }}
          value={this.tipoBancoQuestoes}
          onChange={this.handleChange}
        >
          <MenuItem value={bancoDeQuestoes.minhas}>{strings.minhasQuestoes}</MenuItem>
          <MenuItem value={bancoDeQuestoes.publicas}>{strings.questoesPublicas}</MenuItem>
          <MenuItem value={bancoDeQuestoes.compartilhadas}>{strings.questoesCompartilhadas}</MenuItem>
        </Select>
      </FormControl>
    )
  }

  messagemListaVazia = bancoDeQuestoesOrigem => {
    const { strings } = this.props
    switch (bancoDeQuestoesOrigem) {
      case bancoDeQuestoes.publicas:
        return strings.naoExisteQuestoesPublicas
      case bancoDeQuestoes.compartilhadas:
        return strings.naoExisteQuestoesCompartilhadas
      default:
        return strings.naoEncontrouQuestao
    }
  }

  render() {
    const { strings, classes } = this.props
    const rotas = this.rotas(this.tipoBancoQuestoes)
    return (
      <div>
        <PaginaPaper>
          <TituloDaPagina>
            {this.selectBancoDeQuestoes()}
            <div className={classes.dispBotoes}>
              {!appConfig.projeto.isEducaio && (
                <div className={classes.botaoImportar}>
                  <Importar
                    processarArquivo={this.prepararQuestaoParaEnviar}
                    url={rotas.back.create}
                    aposImportacao={this.atualizarCardDisplay}
                    className={{ botaoImportar: classes.botaoSecundario }}
                  />
                </div>
              )}
              <BotaoComMenu menuList={<MenuListAddQuestao />}>
                <Button id="criar-questao" variant="contained" color="primary" aria-label="add">
                  {strings.criar}
                </Button>
              </BotaoComMenu>
            </div>
          </TituloDaPagina>
        </PaginaPaper>

        <Busca
          FormPadrao={FormQuestaoFiltroPadrao}
          FormExpandido={FormQuestaoFiltroExpandido}
          montarFiltro={montarFiltroQuestao}
        />

        <CardDisplay
          nadaCadastrado={this.messagemListaVazia(this.tipoBancoQuestoes)}
          quantidadeUrl={rotas.back.count}
          bancoDeQuestoesOrigem={this.tipoBancoQuestoes}
          fetchInstanciasUrl={rotas.back.get}
          CardComponent={QuestaoCardGerenciamentoQuestoes}
          quantidadePorPaginaInicial={12}
          quantidadePorPaginaOpcoes={[12, 24, 48, 96]}
          onRef={ref => (this.cardDisplay = ref)}
          contexto={{
            rotas: rotas,
            atualizarCardDisplay: this.atualizarCardDisplay,
          }}
        />
      </div>
    )
  }
}
