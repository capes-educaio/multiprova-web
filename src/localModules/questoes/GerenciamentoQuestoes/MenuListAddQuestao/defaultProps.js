import { appConfig } from 'appConfig'

const { tipoQuestoesHabilitados } = appConfig

export const defaultProps = {
  tipoQuestoesHabilitados,
}
