import React, { Component } from 'react'
import MenuItem from '@material-ui/core/MenuItem'
import MenuList from '@material-ui/core/MenuList'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import DiscursivaIcon from '@material-ui/icons/Subject'
import BlocoIcon from '@material-ui/icons/ViewAgenda'

import MultiplaEscolhaIcon from 'localModules/icons/MultiplaEscolhaIcon'
import VouFIcon from 'localModules/icons/VouFIcon'
import AssociacaoDeColunasIcon from 'localModules/icons/AssociacaoDeColunasIcon'
import RedacaoIcon from 'localModules/icons/RedacaoIcon'

import { questaoUtils } from 'localModules/questoes/utils/QuestaoUtils.js'

import { propTypes } from './propTypes'
import { defaultProps } from './defaultProps'

import { enumTipoQuestao } from 'utils/enumTipoQuestao'

export class MenuListAddQuestaoComponent extends Component {
  static propTypes = propTypes
  static defaultProps = defaultProps

  get addQuestao() {
    return questaoUtils.irParaAddQuestao(this.props.location)
  }

  render() {
    const { strings, classes, tipoQuestoesHabilitados } = this.props
    return (
      <MenuList>
        {tipoQuestoesHabilitados.multiplaEscolha && (
          <MenuItem
            id="item-menu-multipla-escolha"
            onClick={this.addQuestao(enumTipoQuestao.tipoQuestaoMultiplaEscolha)}
          >
            <ListItemIcon className={classes.icon}>
              <MultiplaEscolhaIcon width="24px" />
            </ListItemIcon>
            <ListItemText classes={{ primary: classes.itemText }} inset primary={strings.multiplaEscolha} />
          </MenuItem>
        )}
        {tipoQuestoesHabilitados.vouf && (
          <MenuItem onClick={this.addQuestao(enumTipoQuestao.tipoQuestaoVerdadeiroOuFalso)}>
            <ListItemIcon className={classes.icon}>
              <VouFIcon width="24px" />
            </ListItemIcon>
            <ListItemText classes={{ primary: classes.itemText }} inset primary={strings.vouf} />
          </MenuItem>
        )}
        {tipoQuestoesHabilitados.bloco && (
          <MenuItem id="item-menu-bloco" onClick={this.addQuestao(enumTipoQuestao.tipoQuestaoBloco)}>
            <ListItemIcon className={classes.icon}>
              <BlocoIcon color="primary" />
            </ListItemIcon>
            <ListItemText classes={{ primary: classes.itemText }} inset primary={strings.bloco} />
          </MenuItem>
        )}
        {tipoQuestoesHabilitados.discursiva && (
          <MenuItem onClick={this.addQuestao(enumTipoQuestao.tipoQuestaoDiscursiva)}>
            <ListItemIcon className={classes.icon}>
              <DiscursivaIcon color="primary" />
            </ListItemIcon>
            <ListItemText classes={{ primary: classes.itemText }} inset primary={strings.discursiva} />
          </MenuItem>
        )}
        {tipoQuestoesHabilitados.associacaoDeColunas && (
          <MenuItem onClick={this.addQuestao(enumTipoQuestao.tipoQuestaoAssociacaoDeColunas)}>
            <ListItemIcon className={classes.icon}>
              <AssociacaoDeColunasIcon width="24px" />
            </ListItemIcon>
            <ListItemText
              classes={{ primary: classes.itemText }}
              inset
              primary={strings.tipoQuestaoAssociacaoDeColunas}
            />
          </MenuItem>
        )}
        {tipoQuestoesHabilitados.redacao && (
          <MenuItem id="item-menu-redacao" onClick={this.addQuestao(enumTipoQuestao.tipoQuestaoRedacao)}>
            <ListItemIcon className={classes.icon}>
              <RedacaoIcon width="24px" />
            </ListItemIcon>
            <ListItemText classes={{ primary: classes.itemText }} inset primary={strings.redacao} />
          </MenuItem>
        )}
      </MenuList>
    )
  }
}
