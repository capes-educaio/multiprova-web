import PropTypes from 'prop-types'

export const propTypes = {
  tipoQuestoesHabilitados: PropTypes.object.isRequired,
  // redux state
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
