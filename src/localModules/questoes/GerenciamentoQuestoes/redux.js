import { bindActionCreators } from 'redux'
import { actions } from 'utils/actions'

export function mapStateToProps(state) {
  return {
    strings: state.strings,
    usuarioAtual: state.usuarioAtual,
    tipoBancoQuestoes: state.tipoBancoQuestoes,
    carregando: state.carregando,
  }
}

export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      selectListaTags: actions.select.listaTags,
      selectTipoBancoQuestoes: actions.select.tipoBancoQuestoes,
      selectCarregando: actions.select.carregando,
    },
    dispatch,
  )
}
