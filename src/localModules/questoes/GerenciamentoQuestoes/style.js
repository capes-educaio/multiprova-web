export const style = theme => ({
  dispBotoes: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  botaoImportar: {
    marginRight: '5px',
  },
  botaoSecundario: {
    width: '100%',
    display: 'flex',
    padding: '0px 8px',
    alignItems: 'center',
    justifyContent: 'center',
  },
})
