import PropTypes from 'prop-types'

export const propTypes = {
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.element), PropTypes.element]),
  id: PropTypes.string.isRequired,
  isSortable: PropTypes.bool,
  label: PropTypes.string,
  helperText: PropTypes.string,
  showError: PropTypes.bool,
  errors: PropTypes.array,
  DragHandleComponent: PropTypes.any.isRequired,
  value: PropTypes.object.isRequired,
  accessor: PropTypes.number.isRequired,
  // redux state
  refs: PropTypes.object.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
