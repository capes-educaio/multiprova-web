import React, { Component } from 'react'
import classnames from 'classnames'

import { MpParser } from 'common/MpParser'

import FormHelperText from '@material-ui/core/FormHelperText'
import Collapse from '@material-ui/core/Collapse'
import IconButton from '@material-ui/core/IconButton'
import Typography from '@material-ui/core/Typography'
import Visibility from '@material-ui/icons/Visibility'
import EditIcon from '@material-ui/icons/Edit'

import ExpandMoreIcon from '@material-ui/icons/ExpandMore'

import { EMPTY_EDITOR } from 'utils/Editor'

import { VisualizacaoQuestao } from 'common/VisualizacaoQuestao'
import { BotaoExcluir } from 'common/BotaoExcluir'

import { tipoMap } from '../tipoMap'

import { propTypes } from './propTypes'

export class CampoSubQuestaoBodyComponent extends Component {
  static propTypes = propTypes

  _dadosMontados

  state = {
    modoVisualizacao: false,
    estaExpandido: true,
  }

  _toggleExpandir = () => this.setState({ estaExpandido: !this.state.estaExpandido })

  _ativarmodoVisualizacao = () => {
    const { value } = this.props
    this._dadosMontados = tipoMap[value.tipo].montarQuestaoParaEnviar(value)
    this.setState({ estaExpandido: true, modoVisualizacao: true })
  }

  _ativarModoEdicao = () => {
    this.setState({ estaExpandido: false, modoVisualizacao: false })
  }

  _deletarCampoAtual = () => () => {
    const { refs, accessor } = this.props
    if (refs['bloco__questoes']) refs['bloco__questoes'].deleteField(accessor)
  }

  _deleteField = index => () => {
    const { refs, id } = this.props
    if (this._tamanho - 2 === index) this._deveImpedirAdd = true
    if (refs[id]) refs[id].deleteField(index)
  }

  render() {
    const { modoVisualizacao, estaExpandido } = this.state
    const { label, classes, children, DragHandleComponent, value, strings } = this.props

    const esconderNaVisualizacao = { display: modoVisualizacao ? 'none' : 'block' }
    let erroEnunciado
    if (value.enunciado === EMPTY_EDITOR) erroEnunciado = 'Enunciado não preenchido.'
    return (
      <div className={classes.root}>
        <div className={classes.header}>
          <DragHandleComponent wrapperClassName={classes.dragHandle}>
            <Typography className={classes.label}>{label}</Typography>
          </DragHandleComponent>
          <div>
            {modoVisualizacao ? (
              <IconButton onClick={this._ativarModoEdicao}>
                <EditIcon />
              </IconButton>
            ) : (
              <IconButton onClick={this._ativarmodoVisualizacao}>
                <Visibility />
              </IconButton>
            )}
            <IconButton
              className={classnames(classes.expand, {
                [classes.expandOpen]: estaExpandido,
              })}
              onClick={this._toggleExpandir}
              aria-expanded={estaExpandido}
            >
              <ExpandMoreIcon />
            </IconButton>
            <BotaoExcluir
              excluir={this._deletarCampoAtual}
              buttonClass={classes.botaoDelete}
              stringExcluir={strings.deleteQuestaoDoBloco}
            />
          </div>
        </div>
        <Collapse
          in={!estaExpandido}
          classes={{
            wrapper: classes.minimizadoWrapper,
            container: erroEnunciado ? null : classes.conteudoMinimizado,
          }}
        >
          {erroEnunciado ? (
            <FormHelperText classes={{ root: classes.conteudoMinimizadoErro }} error>
              {erroEnunciado}
            </FormHelperText>
          ) : (
            <MpParser>{value.enunciado}</MpParser>
          )}
        </Collapse>
        <Collapse
          in={estaExpandido}
          classes={{
            entered: classes.visibleCollapse,
          }}
        >
          <div className={classes.campos} style={esconderNaVisualizacao} id={'subquestao-bloco'}>
            {children}
          </div>
          {modoVisualizacao && (
            <VisualizacaoQuestao
              dados={this._dadosMontados}
              tipo={value.tipo}
              tipoTemplate={'geral'}
              onFechar={this._ativarModoEdicao}
            />
          )}
        </Collapse>
      </div>
    )
  }
}
