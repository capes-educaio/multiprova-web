export const style = theme => ({
  root: {
    border: `1px solid ${theme.palette.cinzaAlto}`,
    borderRadius: 4,
  },
  campos: {
    padding: '20px',
    [theme.breakpoints.down('sm')]: {
      padding: '10px 10px 15px 10px',
    },
  },
  header: {
    display: 'flex',
    alignItems: 'center',
    backgroundColor: theme.palette.fundo.cinza,
    borderBottom: `1px solid ${theme.palette.cinzaAlto}`,
    height: 48,
  },
  dragHandle: {
    flexGrow: 1,
    display: 'flex',
    alignItems: 'center',
    height: '100%',
  },
  conteudoMinimizado: {
    maxHeight: '72px',
    padding: '0 10px',
    position: 'relative',
    '&:after': {
      content: '""',
      position: 'absolute',
      top: '32px',
      right: '0',
      width: '100%',
      height: '40px',
      background: `-webkit-linear-gradient(transparent, ${theme.palette.fundo.cinza})`,
    },
    '& p': {
      margin: '10px 0 0 0',
    },
    overflow: 'hidden',
  },
  minimizadoWrapper: {
    minHeight: '40px',
  },
  conteudoMinimizadoErro: {
    padding: '5px 10px 10px 10px',
  },
  expand: {
    transform: 'rotate(0deg)',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
    margin: 'auto 0 auto auto',
  },
  botaoDelete: {},
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  visibleCollapse: {
    overflow: 'visible',
  },
  label: {
    marginLeft: 13,
    padding: '2px 0',
  },
})
