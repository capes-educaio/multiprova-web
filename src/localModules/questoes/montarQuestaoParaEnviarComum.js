import { removerCamposDesnecessariosDeTagsNoCadastro } from './removerCamposDesnecessariosDeTagsNoCadastro'
import { buscaStatusUsandoId } from './buscaStatusUsandoId'

export const montarQuestaoParaEnviarComum = dados => {
  const { enunciado, tipo, elaborador, dificuldade, tags, statusId, anoEscolar, publico } = JSON.parse(
    JSON.stringify(dados),
  )
  const tagsVirtuais = removerCamposDesnecessariosDeTagsNoCadastro(tags)
  const status = buscaStatusUsandoId(statusId)
  const camposComuns = { enunciado, tipo, elaborador, dificuldade, tagsVirtuais, status, anoEscolar }
  return { ...camposComuns, publico }
}
