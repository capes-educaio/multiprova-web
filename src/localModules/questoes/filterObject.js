export const filterObject = ({ include, except }) => data => {
  let filteredData = JSON.parse(JSON.stringify(data))
  if (Array.isArray(include)) {
    const novoComuns = {}
    include.forEach(accessor => {
      novoComuns[accessor] = filteredData[accessor]
    })
    filteredData = novoComuns
  }
  if (Array.isArray(except)) {
    except.forEach(accessor => {
      delete filteredData[accessor]
    })
  }
  return filteredData
}
