export const removerCamposDesnecessariosDeTagsNoCadastro = tagsVirtuais => {
  if (!Array.isArray(tagsVirtuais)) return []
  return tagsVirtuais.map(({ id, nome, isNew }) => ({ id, nome, isNew }))
}
