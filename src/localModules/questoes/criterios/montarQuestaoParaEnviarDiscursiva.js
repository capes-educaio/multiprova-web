import { montarQuestaoParaEnviarComum } from '../montarQuestaoParaEnviarComum'

export const montarQuestaoParaEnviar = dados => {
  let { discursiva } = JSON.parse(JSON.stringify(dados))
  discursiva.numeroDeLinhas = parseInt(discursiva.numeroDeLinhas)
  return { discursiva, ...montarQuestaoParaEnviarComum(dados) }
}
