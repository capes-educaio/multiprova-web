export const style = theme => {
  const barraBloco = '#949494'
  return {
    barraExpandir: {
      backgroundColor: barraBloco,
    },
    tags: {
      display: 'flex',
      alignItems: 'center',
      '& > :first-child': {
        marginBottom: '10px',
      },
    },
    tagsTodos: {
      display: 'flex',
      alignItems: 'center',
    },
  }
}
