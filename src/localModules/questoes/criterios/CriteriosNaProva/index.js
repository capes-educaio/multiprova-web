import { compose } from 'redux'
import { connect } from 'react-redux'

import { withStyles } from '@material-ui/core/styles'

import { mapStateToProps, mapDispatchToProps } from './redux'
import { style } from './style'
import { CriteriosNaProvaComponent } from './CriteriosNaProvaComponent'

export const CriteriosNaProva = compose(
  withStyles(style),
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(CriteriosNaProvaComponent)
