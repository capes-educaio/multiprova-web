import React, { Component, Fragment } from 'react'
import { MpParser } from 'common/MpParser'

import { QuestaoSingularNaProva } from 'localModules/questoes/NaProvaComponents'
import { DisplayTags } from 'common/DisplayTags'

import { enumTipoQuestao } from 'utils/enumTipoQuestao'

import { propTypes } from './propTypes'

export class CriteriosNaProvaComponent extends Component {
  static propTypes = propTypes

  _getTipoString = tipo => {
    const { strings } = this.props
    switch (tipo) {
      case enumTipoQuestao.tipoQuestaoMultiplaEscolha:
        return strings.multiplaEscolha
      case enumTipoQuestao.tipoQuestaoVerdadeiroOuFalso:
        return strings.vouf
      case enumTipoQuestao.tipoQuestaoDiscursiva:
        return strings.discursiva
      case enumTipoQuestao.tipoQuestaoAssociacaoDeColunas:
        return strings.associacaoDeColunas
      default:
        return `Tipo ${tipo} não encontrado.`
    }
  }

  _getCriterios = () => {
    const { questaoProcessada, strings } = this.props
    const { criterios } = questaoProcessada

    let stringCriterios = !!criterios.tipo
      ? `${strings.tipo}: ${this._getTipoString(criterios.tipo)}<br>`
      : `${strings.tipo}: ${strings.todos}<br>`

    stringCriterios += !!criterios.dificuldade
      ? `${strings.dificuldade}: ${criterios.dificuldade}<br>`
      : `${strings.dificuldade}: ${strings.todas}<br>`

    stringCriterios += !!criterios.anoEscolar
      ? `${strings.anoEscolar}: ${criterios.anoEscolar}`
      : `${strings.anoEscolar}: ${strings.todos}`

    return stringCriterios
  }

  get _tagIds() {
    const { questaoProcessada } = this.props
    const tagIds = []
    if (questaoProcessada.criterios.tagIds) {
      questaoProcessada.criterios.tags.map(tag => tagIds.push(tag.id))
    }
    return tagIds
  }

  render() {
    const { classes, questaoProcessada, dragHandleProps, variant, strings } = this.props

    return (
      <QuestaoSingularNaProva
        questaoProcessada={questaoProcessada}
        dragHandleProps={dragHandleProps}
        variant={variant}
        classNames={{ barraExpandir: classes.barraExpandir }}
        conteudoContraido={
          <Fragment>
            <MpParser>{this._getCriterios()}</MpParser>
            {questaoProcessada.criterios.tagIds ? (
              <div className={classes.tags}>
                <MpParser>{`${strings.tags}: `}</MpParser>
                <DisplayTags>{questaoProcessada.criterios.tagIds}</DisplayTags>
              </div>
            ) : (
              <div className={classes.tagsTodos}>
                <MpParser>{`${strings.tags}: `}</MpParser>
                <MpParser>{strings.todos}</MpParser>
              </div>
            )}
          </Fragment>
        }
        disableExpandir
      />
    )
  }
}
