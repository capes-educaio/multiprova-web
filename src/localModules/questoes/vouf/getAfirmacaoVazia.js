import { EMPTY_EDITOR } from 'utils/Editor'

import { enumLetra } from './enumLetra'

export const getAfirmacaoVazia = () => ({
  texto: EMPTY_EDITOR,
  letra: enumLetra.d,
})
