import React, { Component } from 'react'

import DeleteIcon from '@material-ui/icons/DeleteOutline'

import { store } from 'utils/store'

import { MpIconButton } from 'common/MpIconButton'

import { CampoObjeto, CampoSlate } from 'localModules/ReduxForm'

import { CampoLetra } from '../CampoLetra'
import { getAfirmacaoVazia } from '../getAfirmacaoVazia'

import { propTypes } from './propTypes'

export class CampoAfirmacaoComponent extends Component {
  static propTypes = propTypes
  _deveImpedirAdd

  get _tamanho() {
    const { idCampoAfrimacoesPai } = this.props
    return store.getState().SimpleReduxForm__values[idCampoAfrimacoesPai].length
  }

  _addArray = () => {
    if (this._deveImpedirAdd) {
      this._deveImpedirAdd = false
      return
    }
    const { idCampoAfrimacoesPai, refs } = this.props
    const index = this.props.accessor
    const isUltimoCampo = index + 1 === this._tamanho
    if (isUltimoCampo && refs[idCampoAfrimacoesPai]) refs[idCampoAfrimacoesPai].addField(getAfirmacaoVazia())
    return
  }

  _deleteField = () => {
    const { idCampoAfrimacoesPai, refs } = this.props
    const index = this.props.accessor
    if (this._tamanho - 2 === index) this._deveImpedirAdd = true
    if (refs[idCampoAfrimacoesPai]) {
      refs[idCampoAfrimacoesPai].deleteField(index)
    }
  }

  render() {
    const { id, accessor, classes, strings } = this.props
    const isUltimo = accessor + 1 === this._tamanho
    return (
      <CampoObjeto id={id} accessor={accessor} wrapperProps={{ className: classes.root }}>
        <CampoLetra id={`vouf__afirmacoes__letra-${id}`} hidden={isUltimo} accessor="letra" />
        <CampoSlate
          id={`vouf__afirmacoes__texto-${id}`}
          accessor="texto"
          wrapperProps={{ className: classes.texto }}
          onFocus={this._addArray}
          inputProps={{
            placeholder: isUltimo ? strings.novaAfirmacao : null,
            floatingToolbar: true,
            adornment: {
              align: 'right',
              onEvent: 'hover',
              element: isUltimo ? null : (
                <MpIconButton
                  className={classes.excluirButton}
                  onClick={this._deleteField}
                  IconComponent={DeleteIcon}
                />
              ),
            },
          }}
        />
      </CampoObjeto>
    )
  }
}
