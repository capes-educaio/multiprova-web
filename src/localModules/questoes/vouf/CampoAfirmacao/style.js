export const style = theme => ({
  root: {
    display: 'flex',
    alignItems: 'center',
    marginTop: 10,
  },
  texto: {
    width: 'calc(100% - 58px)',
    marginLeft: 10,
  },
  excluirButton: {
    marginLeft: 0,
    borderRadius: 5,
  },
})
