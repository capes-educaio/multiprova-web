import React, { Component } from 'react'
import { Afirmacao } from 'common/Afirmacao'
import { MpParser } from 'common/MpParser'

import { QuestaoSingularNaProva } from 'localModules/questoes/NaProvaComponents'

import { propTypes } from './propTypes'

export class VOuFNaProvaComponent extends Component {
  static propTypes = propTypes

  render() {
    const { classes, questaoProcessada, dragHandleProps, variant } = this.props
    return (
      <QuestaoSingularNaProva
        questaoProcessada={questaoProcessada}
        dragHandleProps={dragHandleProps}
        variant={variant}
        classNames={{ barraExpandir: classes.barraExpandir }}
        conteudoContraido={<MpParser>{questaoProcessada.enunciado}</MpParser>}
        conteudoExpandido={questaoProcessada.vouf.afirmacoes.map((afirmacao, index) => (
          <Afirmacao key={index} afirmacao={afirmacao} mostrarResposta={false} />
        ))}
      />
    )
  }
}
