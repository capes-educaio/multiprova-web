import { getValorVazioComum } from '../getValorVazioComum'
import { enumTipoQuestao } from 'utils/enumTipoQuestao'
import { getAfirmacaoVazia } from './getAfirmacaoVazia'

export const getValorVazio = () => {
  const vouf = {
    afirmacoes: [getAfirmacaoVazia(), getAfirmacaoVazia()],
  }
  return { tipo: enumTipoQuestao.tipoQuestaoVerdadeiroOuFalso, vouf, ...getValorVazioComum() }
}
