import React, { Component } from 'react'

import { QuestaoFormPagePadrao } from '../../QuestaoFormPagePadrao'
import { VOuFForm } from '../VOuFForm'

import { propTypes } from './propTypes'

export class VOuFFormPageComponent extends Component {
  static propTypes = propTypes

  render() {
    return <QuestaoFormPagePadrao {...this.props} FormComponent={VOuFForm} />
  }
}
