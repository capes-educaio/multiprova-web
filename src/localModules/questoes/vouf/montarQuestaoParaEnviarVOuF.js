import { montarQuestaoParaEnviarComum } from '../montarQuestaoParaEnviarComum'

export const montarQuestaoParaEnviar = dados => {
  let { vouf } = JSON.parse(JSON.stringify(dados))
  if (vouf.afirmacoes[vouf.afirmacoes.length - 1].texto === '<p></p>') {
    vouf.afirmacoes.pop()
  }
  return { vouf, ...montarQuestaoParaEnviarComum(dados) }
}
