import React, { Component } from 'react'

import MenuItem from '@material-ui/core/MenuItem'

import { CampoSelect } from 'localModules/ReduxForm/CampoSelect'

import { enumLetra } from '../enumLetra'

import { propTypes } from './propTypes'

export class CampoLetraComponent extends Component {
  static propTypes = propTypes

  render() {
    const { campoSelectProps, id, accessor, classes, hidden } = this.props
    return (
      <CampoSelect
        wrapperProps={{ className: hidden ? classes.hidden : '' }}
        id={id}
        accessor={accessor}
        {...campoSelectProps}
      >
        <MenuItem value={enumLetra.d}>{enumLetra.d}</MenuItem>
        <MenuItem value={enumLetra.v}>{enumLetra.v}</MenuItem>
        <MenuItem value={enumLetra.f}>{enumLetra.f}</MenuItem>
      </CampoSelect>
    )
  }
}
