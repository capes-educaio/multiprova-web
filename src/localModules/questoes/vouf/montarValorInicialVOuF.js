import { montarValorInicialComum } from '../montarValorInicialComum'

import { getAfirmacaoVazia } from './getAfirmacaoVazia'

export const montarValorInicial = dados => {
  const { vouf } = dados
  vouf.afirmacoes.push(getAfirmacaoVazia())
  return {
    ...montarValorInicialComum(dados),
    vouf,
  }
}
