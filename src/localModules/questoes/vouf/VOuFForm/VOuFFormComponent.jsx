import React, { Component } from 'react'
import uid from 'uuid/v4'

import { SimpleReduxForm, CampoObjeto } from 'localModules/ReduxForm'

import { QuestaoCamposComuns } from '../../QuestaoCamposComuns'
import { CampoSubQuestaoBody } from '../../CampoSubQuestaoBody'

import { propTypes } from './propTypes'
import { CampoAfirmacoes } from '../CampoAfirmacoes'
import { campoAfimacoesId } from '../campoAfimacoesId'

export class VOuFFormComponent extends Component {
  static propTypes = propTypes
  _formId = uid()

  render() {
    const { valor, onRef, onValid, onChange, variant, id, accessor, wrapperProps, label } = this.props
    const vouf = (
      <CampoObjeto key={'vouf-' + this._formId} id={'vouf-' + this._formId} accessor="vouf">
        <CampoAfirmacoes id={campoAfimacoesId + this._formId} accessor="afirmacoes" />
      </CampoObjeto>
    )
    return (
      <QuestaoCamposComuns>
        {({ campoEnunciado, campoDificuldade, campoElaborador, campoTag, campoStatus, campoAnoEscolar }) => {
          if (variant === 'dentroDeBloco')
            return (
              <CampoObjeto
                id={id}
                label={label}
                wrapperProps={wrapperProps}
                accessor={accessor}
                BodyComponent={CampoSubQuestaoBody}
              >
                {campoEnunciado}
                {vouf}
                {campoAnoEscolar}
                {campoDificuldade}
                {campoElaborador}
                {campoTag}
              </CampoObjeto>
            )
          return (
            <SimpleReduxForm id={this._formId} valor={valor} onRef={onRef} onChange={onChange} onValid={onValid}>
              {campoStatus}
              {campoEnunciado}
              {vouf}
              {campoAnoEscolar}
              {campoDificuldade}
              {campoElaborador}
              {campoTag}
            </SimpleReduxForm>
          )
        }}
      </QuestaoCamposComuns>
    )
  }
}
