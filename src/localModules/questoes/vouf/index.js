import { VOuFNaProva } from './VOuFNaProva'

export { getValorVazio } from './getValorVazioVOuF'
export { montarQuestaoParaEnviar } from './montarQuestaoParaEnviarVOuF'
export { montarValorInicial } from './montarValorInicialVOuF'
export { VOuFFormPage as Component } from './VOuFFormPage'
export const urlMatches = 'criar-vouf'
export const VerNaProvaComponent = VOuFNaProva
export const EditarNaProvaComponent = VOuFNaProva
