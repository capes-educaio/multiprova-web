import React, { Component } from 'react'

import { CampoArray } from 'localModules/ReduxForm'

import { CampoAfirmacao } from '../CampoAfirmacao'

import { propTypes } from './propTypes'

export class CampoAfirmacoesComponent extends Component {
  static propTypes = propTypes

  _getIdCampoAfrimacoesPai = () => {
    return { idCampoAfrimacoesPai: this.props.id }
  }

  render() {
    const { strings, campoArrayProps, id, accessor, classes } = this.props
    return (
      <CampoArray
        id={id}
        accessor={accessor}
        label={<div className={classes.label}>{strings.afirmacoes}</div>}
        FieldComponent={CampoAfirmacao}
        getFieldProps={this._getIdCampoAfrimacoesPai}
        {...campoArrayProps}
      />
    )
  }
}
