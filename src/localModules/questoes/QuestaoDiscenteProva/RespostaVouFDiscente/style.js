export const style = theme => ({
  orderedList: {
    fontFamily: '"Roboto", "Helvetica", "Arial", "sans-serif"',
    padding: '0 20px',
  },
  afirmacao: {
    display: 'list-item',
    paddingBottom: '5px',
  },
  select: {
    margin: '10px 20px',
    maxWidth: '40px',
  },
  content: {
    display: 'flex',
    alignItems: 'center',
  },
  acerto: {
    color: theme.palette.success.dark,
    paddingRight: '5px',
  },
  erro: {
    color: theme.palette.warning.dark,
    paddingRight: '5px',
  },
  painelDeCorrecao: {
    paddingLeft: '10px',
  },
})
