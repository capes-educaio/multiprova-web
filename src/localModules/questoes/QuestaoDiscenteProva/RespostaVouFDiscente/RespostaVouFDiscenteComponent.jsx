import React from 'react'

import Check from '@material-ui/icons/Check'
import Clear from '@material-ui/icons/Clear'
import MenuItem from '@material-ui/core/MenuItem'
import Select from '@material-ui/core/Select'
import Typography from '@material-ui/core/Typography'

import { enumLetra } from 'localModules/questoes/vouf/enumLetra'

import { MpParser } from 'common/MpParser'

import { enumTipoQuestao } from 'utils/enumTipoQuestao'

import { propTypes } from './propTypes'

export class RespostaVouFDiscenteComponent extends React.Component {
  static propTypes = propTypes
  timeout = null

  handleChange = afirmacaoIndex => event => {
    const resposta = event.target.value
    const { questao, repository } = this.props
    const data = {
      grupoIndex: questao.grupoIndex,
      respostaCandidato: resposta,
      afirmacaoIndex: afirmacaoIndex,
    }

    if (questao.bloco) {
      data.tipo = enumTipoQuestao.tipoQuestaoBloco
      data.subtipo = enumTipoQuestao.tipoQuestaoVerdadeiroOuFalso
      data.questaoIndex = questao.bloco.questaoIndex
      data.subquestaoIndex = questao.questaoIndex
    } else {
      data.tipo = enumTipoQuestao.tipoQuestaoVerdadeiroOuFalso
      data.questaoIndex = questao.questaoIndex
    }

    const instancia = this.props.instanciaProva
    const afirmacao = questao.vouf.afirmacoes[afirmacaoIndex]
    if (questao.vouf && resposta !== afirmacao.respostaCandidato) {
      afirmacao.respostaCandidato = resposta
      this.props.updateInstanciaProva(instancia)
      repository
        .salvarResposta(instancia, data)
        .then(res => {
          this._confirmarResposta(questao, res, instancia, afirmacaoIndex)
        })
        .catch(e => {
          this._confirmarResposta(questao, { data: { confirmacao: data } }, instancia, afirmacaoIndex)
        })
    }
  }

  _confirmarResposta = (questao, resposta, instancia, afirmacaoIndex) => {
    const confirmacao = resposta.data.confirmacao
    questao.vouf.afirmacoes[afirmacaoIndex].dataRespostaCandidato = confirmacao.dataRespostaCandidato
    questao.vouf.afirmacoes[afirmacaoIndex].respostaCandidato = confirmacao.respostaCandidato
    this.props.updateInstanciaProva(instancia)
  }

  render() {
    const { questao, classes, strings, disabled, readMode, painelCorrecao } = this.props
    return (
      <ol className={classes.orderedList}>
        {readMode || painelCorrecao
          ? questao.vouf.afirmacoes.map((afirmacao, index) => {
            const marcouCorreto = afirmacao.respostaCandidato === afirmacao.letra
            return (
              <li key={'afirmacao' + index} className={classes.afirmacao}>
                <div className={classes.content}>
                  {painelCorrecao || readMode ? (
                    <div className={classes.content}>
                      <Typography className={marcouCorreto ? classes.acerto : classes.erro}>
                        <strong>{afirmacao.respostaCandidato || enumLetra.d}</strong>
                      </Typography>
                      <Typography>
                        <strong>{' / ' + afirmacao.letra}</strong>
                      </Typography>
                    </div>
                  ) : (
                    <Select
                      className={classes.select}
                      value={afirmacao.respostaCandidato || ''}
                      onChange={this.handleChange(index)}
                      disabled={disabled}
                    >
                      <MenuItem value="" />
                      <MenuItem value="V">{strings.verdadeiroMinificado}</MenuItem>
                      <MenuItem value="F">{strings.falsoMinificado}</MenuItem>
                    </Select>
                  )}
                  <div className={classes.painelDeCorrecao}>
                    <MpParser>{afirmacao.texto}</MpParser>
                  </div>
                  {marcouCorreto ? <Check className={classes.acerto} /> : <Clear className={classes.erro} />}
                </div>
              </li>
            )
          })
          : questao.vouf.afirmacoes.map((afirmacao, index) => (
            <li key={'afirmacao' + index} className={classes.afirmacao}>
              <div className={classes.content}>
                <Select
                  className={classes.select}
                  value={afirmacao.respostaCandidato || ''}
                  onChange={this.handleChange(index)}
                  disabled={disabled}
                >
                  <MenuItem value="" />
                  <MenuItem value="V">{strings.verdadeiroMinificado}</MenuItem>
                  <MenuItem value="F">{strings.falsoMinificado}</MenuItem>
                </Select>
                <MpParser>{afirmacao.texto}</MpParser>
              </div>
            </li>
          ))}
      </ol>
    )
  }
}
