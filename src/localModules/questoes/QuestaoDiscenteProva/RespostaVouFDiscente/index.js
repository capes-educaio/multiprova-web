import { compose } from 'redux'
import { connect } from 'react-redux'

import { withStyles } from '@material-ui/core/styles'

import { mapStateToProps, mapDispatchToProps } from './redux'
import { style } from './style'
import { RespostaVouFDiscenteComponent } from './RespostaVouFDiscenteComponent'

export const RespostaVouFDiscente = compose(
  withStyles(style),
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(RespostaVouFDiscenteComponent)
