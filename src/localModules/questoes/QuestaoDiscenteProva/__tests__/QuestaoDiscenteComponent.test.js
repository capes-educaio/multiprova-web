import Enzyme, { mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import { MemoryRouter } from 'react-router-dom'
import { Provider } from 'react-redux'

import { QuestaoDiscenteComponent } from '../QuestaoDiscenteComponent'

import React from 'react'

import configureStore from 'redux-mock-store'

import { ptBr } from 'utils/strings/ptBr'

const mockStore = configureStore()

export const store = mockStore({
  usuarioAtual: {
    data: {
      id: 1,
    },
  },
  strings: ptBr,
  browser: {},
})

Enzyme.configure({ adapter: new Adapter() })

jest.mock('@material-ui/core/CircularProgress', () => props => <div>{props.children}</div>)
jest.mock('@material-ui/core/CardContent', () => props => <div>{props.children}</div>)
jest.mock('@material-ui/core/Snackbar', () => props => <div>{props.children}</div>)

jest.mock('utils/enumTipoQuestao', () => ({
  enumTipoQuestao: {
    tipoQuestaoBloco: 'bloco',
    tipoQuestaoDiscursiva: 'discursiva',
    tipoQuestaoMultiplaEscolha: 'multiplaEscolha',
    tipoQuestaoVerdadeiroOuFalso: 'vouf',
    tipoQuestaoAssociacaoDeColunas: 'associacaoDeColunas',
    tipoQuestaoRedacao: 'redacao',
  },
}))

jest.mock('../RespostaMultiplaEscolhaDiscente', () => ({
  RespostaMultiplaEscolhaDiscente: props => <div>{props.children}</div>,
}))
jest.mock('../RespostaDiscursivaDiscente', () => ({
  RespostaDiscursivaDiscente: props => <div>{props.children}</div>,
}))
jest.mock('../RespostaAssociacaoDeColunasDiscente', () => ({
  RespostaAssociacaoDeColunasDiscente: props => <div>{props.children}</div>,
}))
jest.mock('../RespostaVouFDiscente', () => ({
  RespostaVouFDiscente: props => <div>{props.children}</div>,
}))
jest.mock('../CabecalhoBloco', () => ({
  CabecalhoBloco: props => <div>{props.children}</div>,
}))
jest.mock('common/SnackbarContent', () => ({
  SnackbarContent: props => <div>{props.children}</div>,
}))

jest.mock('common/MpParser', () => ({
  MpParser: props => <div>{props.children}</div>,
}))

const defaultProps = {
  classes: {},
  strings: ptBr,
  questaoValue: {},
  questaoSelecionada: 1,
  usuarioAtual: {
    data: {
      id: 1,
    },
    isDocente: jest.fn(),
  },
}

describe('QuestaoDiscenteComponent unit tests', () => {
  test('Monta', () => {
    const component = mount(
      <MemoryRouter>
        <div>
          <Provider store={store}>
            <QuestaoDiscenteComponent {...defaultProps} />
          </Provider>
          ,
        </div>
      </MemoryRouter>,
    )

    component.unmount()
  })
})
