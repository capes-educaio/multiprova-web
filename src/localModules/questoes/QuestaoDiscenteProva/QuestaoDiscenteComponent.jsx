import React, { Fragment, Component } from 'react'

import CircularProgress from '@material-ui/core/CircularProgress'
import CardContent from '@material-ui/core/CardContent'

import { propTypes } from './propTypes'
import { MpParser } from 'common/MpParser'

import { enumTipoQuestao } from 'utils/enumTipoQuestao'
import { RespostaMultiplaEscolhaDiscente } from './RespostaMultiplaEscolhaDiscente'
import { RespostaDiscursivaDiscente } from './RespostaDiscursivaDiscente'
import { RespostaAssociacaoDeColunasDiscente } from './RespostaAssociacaoDeColunasDiscente'
import { RespostaVouFDiscente } from './RespostaVouFDiscente'
import { CabecalhoBloco } from './CabecalhoBloco'
import Snackbar from '@material-ui/core/Snackbar'
import { SnackbarContent } from 'common/SnackbarContent'
import { Subtitulo } from 'common/Subtitulo'
import { ObservacaoDeCorrecao } from 'localModules/questoes/QuestaoDiscenteProva/ObservacaoDeCorrecao'
import { InstanciamentoRepository } from 'repository/InstanciamentoRepository'
import { arredondar } from 'utils/arredondar.js'

export class QuestaoDiscenteComponent extends Component {
  static propTypes = propTypes
  constructor(props) {
    super(props)
    this.state = {
      carregando: false,
      snackBarAutoHideTime: null,
      snackbarProps: {},
      snackbarFlag: false,
    }
    this.repository = new InstanciamentoRepository()
  }

  componentDidMount() {
    this.repository.synchronize.ligar()
  }

  componentWillMount() {
    this.repository.synchronize.desligar()
  }

  handleCloseSnackbar = (event, reason) => {
    if (reason === 'clickaway') return
    this.setState({ snackbarFlag: false })
  }

  handleSuccessSnackbar = (message = this.props.strings.respostaEnviada) =>
    this.setState({
      snackBarAutoHideTime: 3000,
      snackbarProps: { message, variant: 'success' },
      snackbarFlag: true,
    })
  handleErrorSnackbar = (message = this.props.strings.erroEnvioResposta, tryAgain = false, onTryAgain) =>
    this.setState({
      snackBarAutoHideTime: null,
      snackbarProps: { showIcon: false, tryAgain, onTryAgain, message, variant: 'error' },
      snackbarFlag: true,
    })

  _getNumeroPrimeiraEUltimaQuestoesBloco = questao => {
    const { bloco } = questao
    const { questoes } = bloco
    if (questoes)
      return {
        inicial: questoes[0].numeroNaInstancia,
        final: questoes[questoes.length - 1].numeroNaInstancia,
      }
    return {
      inicial: 0,
      final: 0,
    }
  }

  _substituirPalavrasChavePorNumerosNoEnunciadodoBloco = questao => {
    const { inicial, final } = this._getNumeroPrimeiraEUltimaQuestoesBloco(questao)
    questao.enunciado = questao.enunciado.replace('#inicial', inicial)
    questao.enunciado = questao.enunciado.replace('#final', final)
    questao.bloco.fraseInicial = questao.bloco.fraseInicial.replace('#inicial', inicial)
    questao.bloco.fraseInicial = questao.bloco.fraseInicial.replace('#final', final)
    return questao
  }

  render() {
    const { carregando, snackbarFlag, snackbarProps, snackBarAutoHideTime } = this.state
    const { classes, questaoSelecionada, disabled, readMode, strings, usuarioAtual } = this.props
    let { questaoValue } = this.props
    const painelCorrecao = usuarioAtual.isDocente()
    if (questaoValue.tipo === enumTipoQuestao.tipoQuestaoBloco)
      questaoValue = this._substituirPalavrasChavePorNumerosNoEnunciadodoBloco(questaoValue)
    const childrenProps = {
      handleSuccessSnackbar: this.handleSuccessSnackbar,
      handleErrorSnackbar: this.handleErrorSnackbar,
      handleCloseSnackbar: this.handleCloseSnackbar,
      questao: questaoValue,
      disabled,
      readMode,
      repository: this.repository,
      painelCorrecao,
    }
    return (
      <Fragment>
        <Snackbar
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'center',
          }}
          open={snackbarFlag}
          autoHideDuration={snackBarAutoHideTime}
          onClose={this.handleCloseSnackbar}
        >
          <SnackbarContent onClose={this.handleCloseSnackbar} {...snackbarProps} />
        </Snackbar>
        {questaoValue.bloco && <CabecalhoBloco questao={questaoValue.bloco} />}
        <div className={classes.secao} tabIndex="0">
          {carregando && (
            <div className={classes.carregando}>
              <CircularProgress />
            </div>
          )}
          {!carregando && questaoValue && (
            <div key={questaoValue.questaoMatrizId}>
              <div className={classes.enunciado}>
                <MpParser>{questaoValue.enunciado}</MpParser>
                <span className={classes.notaQuestao}>
                  ({strings.valorDaQuestao}: {arredondar(questaoValue.valor, 2).replace('.', ',')})
                </span>
              </div>

              <CardContent className={classes.cardExpandedContent}>
                {questaoValue.tipo === enumTipoQuestao.tipoQuestaoMultiplaEscolha && (
                  <RespostaMultiplaEscolhaDiscente {...childrenProps} />
                )}
                {questaoValue.tipo === enumTipoQuestao.tipoQuestaoDiscursiva && (
                  <Fragment>
                    {painelCorrecao && <Subtitulo>{strings.respostaDoAluno}</Subtitulo>}
                    <RespostaDiscursivaDiscente
                      numeroDaQuestao={questaoSelecionada + 1}
                      {...childrenProps}
                      isUpdating={snackbarFlag}
                    />
                  </Fragment>
                )}
                {questaoValue.tipo === enumTipoQuestao.tipoQuestaoAssociacaoDeColunas && (
                  <RespostaAssociacaoDeColunasDiscente {...childrenProps} />
                )}
                {questaoValue.tipo === enumTipoQuestao.tipoQuestaoVerdadeiroOuFalso && (
                  <RespostaVouFDiscente {...childrenProps} />
                )}
              </CardContent>

              {readMode && (
                <div className={classes.espacoAcima}>
                  <Subtitulo>{strings.observacoesDaCorrecao}</Subtitulo>
                  <ObservacaoDeCorrecao observacao={questaoValue.observacao} />
                </div>
              )}
            </div>
          )}
        </div>
      </Fragment>
    )
  }
}
