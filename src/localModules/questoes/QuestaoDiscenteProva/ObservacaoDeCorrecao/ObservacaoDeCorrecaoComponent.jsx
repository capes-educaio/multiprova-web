import { MpEditor } from 'common/MpEditor'
import React, { Component } from 'react'
import { propTypes } from './propTypes'
import { EMPTY_EDITOR } from 'utils/Editor'

export class ObservacaoDeCorrecaoComponent extends Component {
  static propTypes = propTypes

  render() {
    const { strings, observacao } = this.props
    const conteudo = !!!observacao || observacao === EMPTY_EDITOR ? strings.nenhumaObservacao : observacao
    const childProps = {
      html: conteudo,
      onFocus: () => {
        if (this.props.onFocus) this.props.onFocus()
      },
      onBlur: () => {
        if (this.props.onBlur) this.props.onBlur()
      },
      rows: 10,
      onChange: this.handleChange,
      disabled: true,
      readOnly: true,
    }
    return <MpEditor {...childProps} />
  }
}
