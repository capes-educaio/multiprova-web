import PropTypes from 'prop-types'

export const propTypes = {
  observacao: PropTypes.string,
  isAlternativaCorreta: PropTypes.bool,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
