export const style = theme => ({
  cardExpandedContent: {
    boxSizing: 'border-box',
    wordWrap: 'break-word',
    width: '100%',
    padding: 0,
    textAlign: 'justify',
  },
  carregando: {
    display: 'flex',
    justifyContent: 'center',
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -20,
    marginLeft: -20,
  },
  paper: {
    padding: 8,
  },
  secao: {
    paddingTop: 20,
  },
  enunciado: {
    maxWidth: '100%',
    paddingBottom: 10,
    textAlign: 'justify',
  },
  espacoAcima: { marginTop: 50 },
  notaQuestao: {
    fontSize: 12,
    color: theme.palette.cinzaPadrao,
    fontStyle: 'italic',
  },
})
