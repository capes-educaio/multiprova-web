import PropTypes from 'prop-types'

export const propTypes = {
  updateInstanciaProva: PropTypes.func.isRequired,
  handleSuccessSnackbar: PropTypes.func.isRequired,
  handleErrorSnackbar: PropTypes.func.isRequired,
  handleCloseSnackbar: PropTypes.func.isRequired,
  questao: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
  // strings
  strings: PropTypes.object,
}
