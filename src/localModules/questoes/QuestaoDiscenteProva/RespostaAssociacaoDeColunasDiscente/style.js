export const style = theme => ({
  itemColunaA: {
    display: 'flex',
    margin: '5px 0px 0px 0px',
  },
  itemColunaB: {
    display: 'flex',
    margin: '5px 0px 0px 0px',
  },
  colunaResponsiva: {
    padding: '20px',
    [theme.breakpoints.down('sm')]: {
      width: '100%',
    },
    [theme.breakpoints.up('md')]: {
      width: '50%',
    },
  },
  select: {
    margin: '10px 20px',
    maxWidth: '40px',
  },
  textoColuna: {
    padding: '30px 20px',
    fontSize: '18px',
  },
  label: {
    border: `1px solid ${theme.palette.cinzaDusty}`,
    borderRadius: '5px',
    padding: '10px 10px',
    margin: '10px 0px',
  },
  numbers: {
    padding: '10px 10px',
    display: 'flex',
    alignItems: 'center',
  },
  display: {
    [theme.breakpoints.down('sm')]: {
      display: 'block',
    },
    [theme.breakpoints.up('md')]: {
      display: 'flex',
    },
  },
  acerto: {
    paddingTop: '20px',
    color: theme.palette.success.dark,
  },
  erro: {
    paddingTop: '20px',
    color: theme.palette.warning.dark,
  },
  rotuloEsperado: {
    alignItems: 'center',
    marginRight: '10px',
  },
  rotuloCerto: {
    color: theme.palette.success.dark,
    margin: '5px 10px 5px 5px',
  },
  rotuloErrado: {
    color: theme.palette.warning.dark,
    margin: '5px 10px 5px 5px',
  },
  gabarito: {
    paddingLeft: '25px',
    fontSize: '14px',
  },
  painelCorrecao: {
    display: 'flex',
    alignItems: 'center',
  },
})
