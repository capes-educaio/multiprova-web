import React from 'react'

import { MenuItem, Select, Typography } from '@material-ui/core'
import FormControlLabel from '@material-ui/core/FormControlLabel'

import Check from '@material-ui/icons/Check'
import Clear from '@material-ui/icons/Clear'

import { MpParser } from 'common/MpParser'

import { enumLetra } from 'localModules/questoes/vouf/enumLetra'

import { enumTipoQuestao } from 'utils/enumTipoQuestao'

import { propTypes } from './propTypes'

export class RespostaAssociacaoDeColunasDiscenteComponent extends React.Component {
  static propTypes = propTypes

  handleChange = index => event => {
    const { questao, repository } = this.props
    const resposta = {
      grupoIndex: questao.grupoIndex,
      afirmacaoIndex: index,
      respostaCandidato: String(event.target.value),
    }

    if (questao.bloco) {
      resposta.tipo = enumTipoQuestao.tipoQuestaoBloco
      resposta.subtipo = enumTipoQuestao.tipoQuestaoAssociacaoDeColunas
      resposta.questaoIndex = questao.bloco.questaoIndex
      resposta.subquestaoIndex = questao.questaoIndex
    } else {
      resposta.tipo = enumTipoQuestao.tipoQuestaoAssociacaoDeColunas
      resposta.questaoIndex = questao.questaoIndex
    }

    const instancia = this.props.instanciaProva
    if (questao.associacaoDeColunas) {
      repository
        .salvarResposta(instancia, resposta)
        .then(res => {
          this._confirmarResposta(questao, res, instancia, index)
        })
        .catch(e => {
          this._confirmarResposta(questao, { data: { confirmacao: resposta } }, instancia, index)
        })
    }
  }

  _confirmarResposta = (questao, resposta, instancia, itemIndex) => {
    const confirmacao = resposta.data.confirmacao
    questao.associacaoDeColunas.colunaB[itemIndex].dataRespostaCandidato = confirmacao.dataRespostaCandidato
    questao.associacaoDeColunas.colunaB[itemIndex].respostaCandidato = confirmacao.respostaCandidato
    this.props.updateInstanciaProva(instancia)
  }

  render() {
    const { questao, classes, strings, disabled, readMode, painelCorrecao } = this.props
    const colunaA = (
      <div className={classes.colunaResponsiva}>
        <Typography className={classes.textoColuna} variant="h5" color="primary">
          {strings.coluna + ' A'}
        </Typography>
        {questao.associacaoDeColunas.colunaA.map((item, index) => (
          <div key={'colunaA-item-' + index} className={classes.itemColunaA}>
            <Typography className={classes.numbers}>{item.rotulo + '. '}</Typography>
            <MpParser typographyProps={{ className: classes.label }}>{item.conteudo}</MpParser>
          </div>
        ))}
      </div>
    )

    const range = Array.from({ length: questao.associacaoDeColunas.colunaA.length }, (value, key) => key + 1)
    const opcoescolunaB = [''].concat(range)
    const colunaB = (
      <div className={classes.colunaResponsiva}>
        <Typography className={classes.textoColuna} variant="h5" color="primary">
          {strings.coluna + ' B'}
        </Typography>
        {questao.associacaoDeColunas.colunaB.map((item, index) => {
          const marcouCorreto = item.rotuloEsperado === item.respostaCandidato

          return (
            <div key={'colunaB-item-' + index} className={classes.itemColunaB}>
              {readMode || painelCorrecao ? (
                marcouCorreto ? (
                  <Check className={classes.acerto} />
                ) : (
                  <Clear className={classes.erro} />
                )
              ) : (
                <div />
              )}

              {(painelCorrecao || readMode) && (
                <div className={classes.painelCorrecao}>
                  <Typography className={marcouCorreto ? classes.rotuloCerto : classes.rotuloErrado} variant="h6">
                    {questao.associacaoDeColunas.colunaB[index].respostaCandidato || enumLetra.d}
                  </Typography>
                  <Typography className={classes.rotuloEsperado} variant="h6">
                    {' / ' + questao.associacaoDeColunas.colunaB[index].rotuloEsperado}
                  </Typography>
                  <MpParser typographyProps={{ className: classes.label }}>{item.conteudo}</MpParser>
                </div>
              )}

              {!(painelCorrecao || readMode) && (
                <FormControlLabel
                  control={
                    <Select
                      className={classes.select}
                      onChange={this.handleChange(index)}
                      value={questao.associacaoDeColunas.colunaB[index].respostaCandidato || ''}
                      disabled={disabled}
                    >
                      {opcoescolunaB.map(valor => (
                        <MenuItem value={valor} key={valor}>
                          {valor}
                        </MenuItem>
                      ))}
                    </Select>
                  }
                  label={<MpParser typographyProps={{ className: classes.label }}>{item.conteudo}</MpParser>}
                />
              )}
            </div>
          )
        })}
      </div>
    )
    return (
      <div className={classes.display} data-cy="associacaoDeColunas">
        {colunaA}
        {colunaB}
      </div>
    )
  }
}
