import React from 'react'
import { MpEditor } from 'common/MpEditor'
import { EMPTY_EDITOR } from 'utils/Editor'
import { Subtitulo } from 'common/Subtitulo'

import { propTypes } from './propTypes'

import { Typography } from '@material-ui/core'
import DoneAllIcon from '@material-ui/icons/DoneAll'
import CircularProgress from '@material-ui/core/CircularProgress'

import { enumTipoQuestao } from 'utils/enumTipoQuestao'
export class RespostaDiscursivaDiscenteComponent extends React.Component {
  static propTypes = propTypes
  timeout = null

  _salvarDiscursiva = (questao, texto, resposta) => {
    const { instanciaProva, repository } = this.props
    questao.discursiva.respostaCandidato = texto
    this.props.updateInstanciaProva(instanciaProva)
    clearTimeout(this.timeout)
    repository
      .salvarResposta(instanciaProva, resposta)
      .then(res => {
        this.props.handleSuccessSnackbar(this.props.strings.respostaDaQuestaoEnviada(this.props.numeroDaQuestao))
        setTimeout(() => this.props.handleCloseSnackbar(), 1000)
        this.confirmarResposta(res)
      })
      .catch(e => {
        this.confirmarResposta({ data: { confirmacao: resposta } })
      })
  }

  handleChange = event => {
    const texto = event
    const { questao } = this.props
    const resposta = {
      grupoIndex: questao.grupoIndex,
      respostaCandidato: texto,
    }

    if (questao.bloco) {
      resposta.tipo = enumTipoQuestao.tipoQuestaoBloco
      resposta.subtipo = enumTipoQuestao.tipoQuestaoDiscursiva
      resposta.questaoIndex = questao.bloco.questaoIndex
      resposta.subquestaoIndex = questao.questaoIndex
    } else {
      resposta.tipo = enumTipoQuestao.tipoQuestaoDiscursiva
      resposta.questaoIndex = questao.questaoIndex
    }

    if (!!questao.discursiva) {
      this._salvarDiscursiva(questao, texto, resposta)
    }
  }
  confirmarResposta = resposta => {
    const { questao } = this.props
    const instancia = this.props.instanciaProva
    const confirmacao = resposta.data.confirmacao
    questao.discursiva.dataRespostaCandidato = confirmacao.dataRespostaCandidato
    this.props.updateInstanciaProva(instancia)
  }

  render() {
    const { questao, disabled, readMode, strings, classes, isUpdating } = this.props

    const props = {
      html: questao.discursiva.respostaCandidato || EMPTY_EDITOR,
      onFocus: () => {
        if (this.props.onFocus) this.props.onFocus()
      },
      onBlur: () => {
        if (this.props.onBlur) this.props.onBlur()
      },
      rows: 10,
      onChange: this.handleChange,
      disabled: disabled,
      readOnly: disabled,
    }
    const propsExpectativa = readMode
      ? {
        html: questao.discursiva.expectativaDeResposta || strings.docenteNaoRegistrouUmaExpectativa,
        onFocus: () => {
          if (this.props.onFocus) this.props.onFocus()
        },
        onBlur: () => {
          if (this.props.onBlur) this.props.onBlur()
        },
        rows: 10,
        onChange: this.handleChange,
        disabled: disabled,
        readOnly: disabled,
      }
      : null
    const iconeDiscursiva = isUpdating ? (
      <CircularProgress size={15} thickness={1.8} />
    ) : (
      <Typography className={classes.statusResposta}>
        <DoneAllIcon />
      </Typography>
    )
    return (
      <React.Fragment>
        <MpEditor {...props} />
        {readMode && (
          <div className={classes.espacoAcima}>
            <Subtitulo>{strings.expectativaDeResposta}</Subtitulo>
            <MpEditor {...propsExpectativa} />
          </div>
        )}
        {!disabled && iconeDiscursiva}
      </React.Fragment>
    )
  }
}
