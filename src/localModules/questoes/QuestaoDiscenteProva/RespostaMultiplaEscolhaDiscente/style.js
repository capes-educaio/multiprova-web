export const style = theme => ({
  radio: {
    '&$checked': {
      color: theme.palette.defaultLink || '#26a69a',
    },
  },
  checked: {},
})
