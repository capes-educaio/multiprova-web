import { compose } from 'redux'
import { connect } from 'react-redux'

import { withStyles } from '@material-ui/core/styles'

import { mapStateToProps, mapDispatchToProps } from './redux'
import { style } from './style'
import { RespostaMultiplaEscolhaDiscenteComponent } from './RespostaMultiplaEscolhaDiscenteComponent'

export const RespostaMultiplaEscolhaDiscente = compose(
  withStyles(style),
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(RespostaMultiplaEscolhaDiscenteComponent)
