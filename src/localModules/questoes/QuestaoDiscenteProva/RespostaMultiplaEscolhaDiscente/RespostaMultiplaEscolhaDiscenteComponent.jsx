import FormControlLabel from '@material-ui/core/FormControlLabel'
import Radio from '@material-ui/core/Radio'
import RadioGroup from '@material-ui/core/RadioGroup'
import { Alternativa } from 'common/Alternativa'
import React from 'react'
import { enumTipoQuestao } from 'utils/enumTipoQuestao'
import { AlternativaVistaDeProva } from '../AlternativaVistaDeProva'
import { propTypes } from './propTypes'

export class RespostaMultiplaEscolhaDiscenteComponent extends React.Component {
  static propTypes = propTypes

  _handleCheck = e => {
    this._handleChange(e.target.value)
  }

  _handleChange = newVal => {
    const { questao, repository } = this.props
    const instancia = this.props.instanciaProva
    const resposta = {
      grupoIndex: questao.grupoIndex,
      respostaCandidato: newVal,
    }

    if (questao.bloco) {
      resposta.tipo = enumTipoQuestao.tipoQuestaoBloco
      resposta.subtipo = enumTipoQuestao.tipoQuestaoMultiplaEscolha
      resposta.questaoIndex = questao.bloco.questaoIndex
      resposta.subquestaoIndex = questao.questaoIndex
    } else {
      resposta.tipo = enumTipoQuestao.tipoQuestaoMultiplaEscolha
      resposta.questaoIndex = questao.questaoIndex
    }

    const value = questao.multiplaEscolha.respostaCandidato
    if (newVal === value) resposta.respostaCandidato = ''

    repository
      .salvarResposta(instancia, resposta)
      .then(res => {
        this._confirmarResposta(questao, res, instancia)
      })
      .catch(e => {
        this._confirmarResposta(questao, { data: { confirmacao: resposta } }, instancia)
      })
  }

  _uncheckRadio = e => {
    const { questao } = this.props
    const cleanRadio = ''
    if (questao.multiplaEscolha.respostaCandidato === e.target.value) this._handleChange(cleanRadio)
  }

  _confirmarResposta = (questao, resposta, instancia) => {
    const confirmacao = resposta.data.confirmacao
    questao.multiplaEscolha.dataRespostaCandidato = confirmacao.dataRespostaCandidato
    questao.multiplaEscolha.respostaCandidato = confirmacao.respostaCandidato
    this.props.updateInstanciaProva(instancia)
  }

  render() {
    const { questao, classes, disabled, readMode, painelCorrecao } = this.props
    return (
      <RadioGroup
        aria-label="MultiplaEscolha"
        name="multiplaescolha"
        value={questao.multiplaEscolha.respostaCandidato}
        onChange={this._handleCheck}
      >
        {readMode || painelCorrecao
          ? questao.multiplaEscolha.alternativas.map((alternativa, index) => {
            const foiMarcada = alternativa.letraNaInstancia === questao.multiplaEscolha.respostaCandidato
            const isAlternativaCorreta = alternativa.letraNaInstancia === questao.multiplaEscolha.respostaCerta
            return (
              <FormControlLabel
                key={'alternativa' + index}
                value={alternativa.letraNaInstancia}
                control={<Radio disabled={disabled} classes={{ root: classes.radio, checked: classes.checked }} />}
                label={
                  <AlternativaVistaDeProva
                    key={alternativa.letraNaInstancia}
                    alternativa={alternativa}
                    foiMarcada={foiMarcada}
                    isAlternativaCorreta={isAlternativaCorreta}
                  />
                }
              />
            )
          })
          : questao.multiplaEscolha.alternativas.map((alternativa, index) => {
            return (
              <FormControlLabel
                key={'alternativa' + index}
                value={alternativa.letraNaInstancia}
                control={<Radio disabled={disabled} classes={{ root: classes.radio, checked: classes.checked }} />}
                label={<Alternativa key={alternativa.letraNaInstancia} alternativa={alternativa} />}
              />
            )
          })}
      </RadioGroup>
    )
  }
}
