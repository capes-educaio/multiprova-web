import { bindActionCreators } from 'redux'
import { actions } from 'utils/actions'

export function mapStateToProps(state) {
  return {
    strings: state.strings,
    instanciaProva: state.instanciaProva,
  }
}

export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      updateInstanciaProva: actions.update.instanciaProva,
    },
    dispatch,
  )
}
