const camposComuns = theme => ({
  display: 'flex',

  flexGrow: '1',
})
export const style = theme => ({
  cardExpandedContent: {
    wordWrap: 'break-word',
  },
  expand: {
    transform: 'rotate(0deg)',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
    marginLeft: 'auto',
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  actions: {
    display: 'flex',
    alignSelf: 'flex-end',
    flexGrow: '1',
  },
  enunciado: {
    ...camposComuns(theme),
    padding: '0px 20px 10px 20px',
  },
  card: {
    border: `1px solid ${theme.palette.cinzaMedio}`,
    borderRadius: '5px',
    margin: '25px 25px 0px 25px',
  },
  secao: {
    display: 'flex',
    alignItems: 'center',
    paddingLeft: '20px',
    flexGrow: '1',
  },
  fraseInicial: {
    ...camposComuns(theme),
  },
})
