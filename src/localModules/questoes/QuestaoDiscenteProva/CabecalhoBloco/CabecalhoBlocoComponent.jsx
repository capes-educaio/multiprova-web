import React from 'react'
import classnames from 'classnames'

import { propTypes } from './propTypes'
import { MpParser } from 'common/MpParser'

import Collapse from '@material-ui/core/Collapse'
import IconButton from '@material-ui/core/IconButton'

import ExpandMoreIcon from '@material-ui/icons/ExpandMore'

export class CabecalhoBlocoComponent extends React.Component {
  static defaultProps = {
    estaInicialmenteExpandido: true,
  }
  static propTypes = propTypes
  constructor(props) {
    super(props)
    this.state = {
      estaExpandido: props.estaInicialmenteExpandido,
    }
  }

  handleExpandirEContrair = event => {
    if (event) {
      event.stopPropagation()
    }
    const { estaExpandido } = this.state
    this.setState({ estaExpandido: !estaExpandido })
  }

  render() {
    const { classes, questao } = this.props
    const { estaExpandido } = this.state
    return (
      <div className={classes.card}>
        <div className={classes.secao}>
          <MpParser typographyProps={{ className: classes.fraseInicial }}>{questao.fraseInicial}</MpParser>
          <div className={classes.actions}>
            <IconButton
              size="small"
              className={classnames(classes.expand, {
                [classes.expandOpen]: estaExpandido,
              })}
              onClick={this.handleExpandirEContrair}
              aria-expanded={estaExpandido}
              aria-label="Show more"
            >
              <ExpandMoreIcon />
            </IconButton>
          </div>
        </div>
        <Collapse in={estaExpandido} timeout="auto" unmountOnExit>
          <MpParser typographyProps={{ className: classes.enunciado }}>{questao.enunciado}</MpParser>
        </Collapse>
      </div>
    )
  }
}
