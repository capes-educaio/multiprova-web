import { compose } from 'redux'
import { connect } from 'react-redux'

import { withStyles } from '@material-ui/core/styles'

import { mapStateToProps, mapDispatchToProps } from './redux'
import { style } from './style'
import { CabecalhoBlocoComponent } from './CabecalhoBlocoComponent'

export const CabecalhoBloco = compose(
  withStyles(style),
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(CabecalhoBlocoComponent)
