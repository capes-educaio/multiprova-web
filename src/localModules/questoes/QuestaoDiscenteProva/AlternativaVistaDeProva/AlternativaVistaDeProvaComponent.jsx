import Check from '@material-ui/icons/Check'
import Clear from '@material-ui/icons/Clear'
import { Alternativa } from 'common/Alternativa'
import React, { Component } from 'react'
import { propTypes } from './propTypes'

export class AlternativaVistaDeProvaComponent extends Component {
  static propTypes = propTypes

  render() {
    const { alternativa, classes, foiMarcada, isAlternativaCorreta } = this.props
    return (
      <div className={classes.linhaDeAlternativa}>
        <div className={classes.minimum}>
          {isAlternativaCorreta && <Check className={classes.acerto} />}
          {foiMarcada && !isAlternativaCorreta && <Clear className={classes.erro} />}
        </div>
        <Alternativa alternativa={alternativa} />
      </div>
    )
  }
}
