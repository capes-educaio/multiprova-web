import PropTypes from 'prop-types'

export const propTypes = {
  alternativa: PropTypes.object.isRequired,
  foiMarcada: PropTypes.bool,
  isAlternativaCorreta: PropTypes.bool,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
