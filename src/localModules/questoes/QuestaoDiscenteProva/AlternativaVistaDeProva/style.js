export const style = theme => ({
  linhaDeAlternativa: {
    display: 'flex',
    flexDirection: 'row',
  },
  acerto: {
    color: theme.palette.success.dark,
  },
  erro: {
    color: theme.palette.warning.dark,
  },
  minimum: {
    minWidth: '25px',
    display: 'flex',
    justifyContent: 'center',
  },
})
