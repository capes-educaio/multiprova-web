import { tipoMap } from '../tipoMap'
import { montarValorInicialComum } from '../montarValorInicialComum'

import { filtrarCamposComunsAusentes } from './filtrarCamposComunsAusentes'

export const montarValorInicial = dados => {
  const { bloco } = dados
  const questoesProcessadas = bloco.questoes.map(value => ({
    id: value.id,
    ...tipoMap[value.tipo].montarValorInicial(value),
  }))
  return {
    ...filtrarCamposComunsAusentes(montarValorInicialComum(dados)),
    fraseInicial: bloco.fraseInicial,
    bloco: { questoes: questoesProcessadas },
  }
}
