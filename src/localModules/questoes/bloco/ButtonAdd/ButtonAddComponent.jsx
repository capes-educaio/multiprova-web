import React, { Component } from 'react'
import Fab from '@material-ui/core/Fab'
import PlaylistAdd from '@material-ui/icons/PlaylistAdd'
import MenuItem from '@material-ui/core/MenuItem'
import { BotaoComMenu } from 'common/BotaoComMenu'

import ListItemIcon from '@material-ui/core/ListItemIcon'

import { tipoMap } from 'localModules/questoes/tipoMap'

import MultiplaEscolhaIcon from 'localModules/icons/MultiplaEscolhaIcon'
import DiscursivaIcon from '@material-ui/icons/Subject'
import uid from 'uuid/v4'
import VouFIcon from 'localModules/icons/VouFIcon'
import AssociacaoDeColunasIcon from 'localModules/icons/AssociacaoDeColunasIcon'
import { enumTipoQuestao } from 'utils/enumTipoQuestao'

import { propTypes } from './propTypes'
import { MenuList } from '@material-ui/core'

export class ButtonAddComponent extends Component {
  static propTypes = propTypes

  _addQuestao = tipo => {
    const { refs } = this.props
    const { context } = this
    const quantAlternativas = context ? 1 : 5
    if (refs['bloco__questoes'])
      refs['bloco__questoes'].addField({ id: uid(), ...tipoMap[tipo].getValorVazio(quantAlternativas) })
  }

  render() {
    const { classes, strings } = this.props

    const propsPopover = {
      anchorOrigin: {
        vertical: 'top',
        horizontal: 'left',
      },
      transformOrigin: {
        vertical: 'bottom',
        horizontal: 'right',
      },
    }
    const menuAddQuestoes = (
      <MenuList>
        <MenuItem onClick={() => this._addQuestao(enumTipoQuestao.tipoQuestaoMultiplaEscolha)}>
          <ListItemIcon className={classes.icon}>
            <MultiplaEscolhaIcon width="20px" />
          </ListItemIcon>
          {strings.multiplaEscolha}
        </MenuItem>
        <MenuItem onClick={() => this._addQuestao(enumTipoQuestao.tipoQuestaoVerdadeiroOuFalso)}>
          <ListItemIcon className={classes.icon}>
            <VouFIcon width="20px" />
          </ListItemIcon>
          {strings.tipoQuestaoVerdadeiroOuFalso}
        </MenuItem>
        <MenuItem onClick={() => this._addQuestao(enumTipoQuestao.tipoQuestaoAssociacaoDeColunas)}>
          <ListItemIcon className={classes.icon}>
            <AssociacaoDeColunasIcon width="20px" />
          </ListItemIcon>
          {strings.tipoQuestaoAssociacaoDeColunas}
        </MenuItem>
        <MenuItem onClick={() => this._addQuestao(enumTipoQuestao.tipoQuestaoDiscursiva)}>
          <ListItemIcon className={classes.icon}>
            <DiscursivaIcon color="primary" />
          </ListItemIcon>
          {strings.discursiva}
        </MenuItem>
      </MenuList>
    )
    return (
      <React.Fragment>
        <BotaoComMenu
          propsPopover={propsPopover}
          menuList={menuAddQuestoes}
          classes={{ classeBotao: classes.botaoAddQuestao }}
        >
          <Fab color="primary">
            <PlaylistAdd />
          </Fab>
        </BotaoComMenu>
      </React.Fragment>
    )
  }
}
