import React, { Component, Fragment } from 'react'

import { QuestaoFormPagePadrao } from '../../QuestaoFormPagePadrao'
import { BlocoForm } from '../BlocoForm'
import { ButtonAdd } from '../ButtonAdd'

import { propTypes } from './propTypes'

export class BlocoFormPageComponent extends Component {
  static propTypes = propTypes

  render() {
    const { botoesExtras } = this.props

    return (
      <QuestaoFormPagePadrao
        {...this.props}
        FormComponent={BlocoForm}
        botoesExtras={
          <Fragment>
            <ButtonAdd />
            {botoesExtras}
          </Fragment>
        }
      />
    )
  }
}
