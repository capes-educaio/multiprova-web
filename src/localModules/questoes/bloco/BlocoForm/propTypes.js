import PropTypes from 'prop-types'

export const propTypes = {
  valor: PropTypes.object,
  onValid: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  onRef: PropTypes.func.isRequired,
  // style
  classes: PropTypes.object.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
}
