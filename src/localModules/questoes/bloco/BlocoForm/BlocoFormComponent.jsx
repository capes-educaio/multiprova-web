import React, { Component } from 'react'

import { SimpleReduxForm, CampoObjeto, CampoSlate, CampoArray } from 'localModules/ReduxForm'

import { QuestaoCamposComuns } from '../../QuestaoCamposComuns'

import { propTypes } from './propTypes'

import { QuestaoInternaBlocoForm } from '../QuestaoInternaBlocoForm'

export class BlocoFormComponent extends Component {
  static propTypes = propTypes

  render() {
    const { valor, onRef, onValid, onChange, strings, classes } = this.props
    return (
      <QuestaoCamposComuns>
        {({ campoEnunciado, campoStatus }) => {
          return (
            <SimpleReduxForm id="bloco" valor={valor} onRef={onRef} onChange={onChange} onValid={onValid}>
              {campoStatus}
              <span className={classes.inicialFinal}>({strings.inicialFinal})</span>
              <CampoSlate
                classes={{ wrapper: classes.fraseInicial }}
                id="bloco__fraseInicial"
                required
                accessor="fraseInicial"
                inputProps={{
                  placeholder: strings.fraseInicial,
                  floatingToolbar: true,
                  editorProps: { id: 'form-questao-frase-inicial' },
                }}
              />
              {campoEnunciado}
              <CampoObjeto id="bloco__bloco" accessor="bloco">
                <CampoArray
                  id={'bloco__questoes'}
                  FieldComponent={QuestaoInternaBlocoForm}
                  getFieldProps={index => {
                    return {
                      label: String(index + 1),
                      variant: 'dentroDeBloco',
                      wrapperProps: { className: classes.questaoDoBloco },
                    }
                  }}
                  accessor="questoes"
                  sortable
                />
              </CampoObjeto>
            </SimpleReduxForm>
          )
        }}
      </QuestaoCamposComuns>
    )
  }
}
