export const style = theme => ({
  questaoDoBloco: {
    marginTop: 20,
  },
  fraseInicial: { margin: '10px 0' },
  inicialFinal: {
    fontSize: 12,
    color: theme.palette.cinzaPadrao,
    fontStyle: 'italic',
  },
})
