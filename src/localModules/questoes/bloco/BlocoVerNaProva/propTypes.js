import PropTypes from 'prop-types'

export const propTypes = {
  questaoProcessada: PropTypes.object.isRequired,
  // redux state
  mostrarPin: PropTypes.bool.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
