import React, { Component } from 'react'

import { MpParser } from 'common/MpParser'

import { QuestaoNaProvaVer } from 'localModules/provas/QuestaoNaProvaVer'

import { QuestaoNaProvaPadrao, ProviderActionsComunsNaProva, ProviderQuestaoNaProva } from '../../NaProvaComponents'

import { propTypes } from './propTypes'

export class BlocoVerNaProvaComponent extends Component {
  static propTypes = propTypes

  render() {
    const { classes, questaoProcessada, mostrarPin } = this.props
    return (
      <ProviderQuestaoNaProva questaoProcessada={questaoProcessada}>
        {({ estaExpandido, questoesDoBloco, handleExpandirEContrair }) => (
          <ProviderActionsComunsNaProva
            onClick={{
              expandirContrairButton: handleExpandirEContrair,
            }}
            estaExpandido={estaExpandido}
            questaoProcessada={questaoProcessada}
          >
            {({ expandirContrairButton, pinDisplay }) => {
              return (
                <QuestaoNaProvaPadrao
                  questaoProcessada={questaoProcessada}
                  classNames={{ barraExpandir: classes.barraExpandir }}
                  handleExpandirEContrair={handleExpandirEContrair}
                  estaExpandido={estaExpandido}
                  conteudoContraido={<MpParser>{questaoProcessada.bloco.fraseInicial}</MpParser>}
                  actions={expandirContrairButton}
                  iconDisplay={mostrarPin && questaoProcessada.fixa ? pinDisplay : null}
                  conteudoExpandido={questoesDoBloco.map((questaoDoBloco, index) => (
                    <QuestaoNaProvaVer
                      key={questaoDoBloco.id}
                      index={index}
                      questaoProcessada={questaoDoBloco}
                      mostrarPin={mostrarPin}
                    />
                  ))}
                />
              )
            }}
          </ProviderActionsComunsNaProva>
        )}
      </ProviderQuestaoNaProva>
    )
  }
}
