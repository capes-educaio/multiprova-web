import { compose } from 'redux'
import { connect } from 'react-redux'
// import { withRouter } from 'react-router'

import { withStyles } from '@material-ui/core/styles'

import { mapStateToProps, mapDispatchToProps } from './redux'
import { BlocoVerNaProvaComponent } from './BlocoVerNaProvaComponent'
import { style } from './style'

export const BlocoVerNaProva = compose(
  // withRouter,
  withStyles(style),
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(BlocoVerNaProvaComponent)
