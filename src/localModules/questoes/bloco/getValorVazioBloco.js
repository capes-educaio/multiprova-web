import uid from 'uuid/v4'
import { EMPTY_EDITOR } from 'utils/Editor'
import { enumTipoQuestao } from 'utils/enumTipoQuestao'

import { getValorVazioComum } from '../getValorVazioComum'
import * as multiplaEscolha from '../multiplaEscolha'

import { filtrarCamposComunsAusentes } from './filtrarCamposComunsAusentes'

export const getValorVazio = quantAlternativas => {
  return {
    tipo: enumTipoQuestao.tipoQuestaoBloco,
    fraseInicial: EMPTY_EDITOR,
    bloco: {
      questoes: [{ ...multiplaEscolha.getValorVazio(quantAlternativas), id: uid() }],
    },
    ...filtrarCamposComunsAusentes(getValorVazioComum()),
  }
}
