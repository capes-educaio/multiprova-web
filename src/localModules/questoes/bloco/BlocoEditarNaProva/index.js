import { compose } from 'redux'
import { connect } from 'react-redux'
// import { withRouter } from 'react-router'

import { withStyles } from '@material-ui/core/styles'

import { mapStateToProps, mapDispatchToProps } from './redux'
import { BlocoEditarNaProvaComponent } from './BlocoEditarNaProvaComponent'
import { style } from './style'

export const BlocoEditarNaProva = compose(
  // withRouter,
  withStyles(style),
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(BlocoEditarNaProvaComponent)
