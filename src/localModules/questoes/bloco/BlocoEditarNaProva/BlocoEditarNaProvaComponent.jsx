import React, { Component } from 'react'
import { Droppable, Draggable } from 'react-beautiful-dnd'

import { MpParser } from 'common/MpParser'

import { QuestaoNaProvaPadrao, ProviderActionsComunsNaProva, ProviderQuestaoNaProva } from '../../NaProvaComponents'

import { QuestaoNaProvaEditar } from 'localModules/provas/QuestaoNaProvaEditar'

import { propTypes } from './propTypes'

export class BlocoEditarNaProvaComponent extends Component {
  static propTypes = propTypes

  render() {
    const { questaoProcessada, classes, dragHandleProps, mostrarPin, strings } = this.props
    const { toggleFixagem } = questaoProcessada
    return (
      <ProviderQuestaoNaProva questaoProcessada={questaoProcessada}>
        {({ estaExpandido, questoesDoBloco, handleExpandirEContrair }) => {
          return (
            <ProviderActionsComunsNaProva
              onClick={{
                pinButton: toggleFixagem,
                expandirContrairButton: handleExpandirEContrair,
              }}
              tooltips={{
                pinButton: strings.provaFixarQuestaoBloco,
                addButton: strings.provaAddQuestaoAbaixoBloco,
                excluirButton: strings.provaExcluirQuestaoBloco,
              }}
              stringExcluir={strings.desejaExcluirQuestaoDaProva}
              estaExpandido={estaExpandido}
              questaoProcessada={questaoProcessada}
            >
              {({ expandirContrairButton, excluirButton, addButton, pinButton, editarButton }) => {
                const actions = [editarButton, addButton, excluirButton, expandirContrairButton]
                if (mostrarPin) actions.unshift(pinButton)
                return (
                  <QuestaoNaProvaPadrao
                    dragHandleProps={dragHandleProps}
                    questaoProcessada={questaoProcessada}
                    classNames={{ barraExpandir: classes.barraExpandir }}
                    handleExpandirEContrair={handleExpandirEContrair}
                    estaExpandido={estaExpandido}
                    conteudoContraido={
                      <React.Fragment>
                        <MpParser typographyProps={{ className: classes.fraseInicial }}>
                          {questaoProcessada.bloco.fraseInicial}
                        </MpParser>
                        <MpParser>{questaoProcessada.enunciado}</MpParser>
                      </React.Fragment>
                    }
                    actions={actions}
                    conteudoExpandido={
                      <Droppable type={'bloco' + questaoProcessada.id} droppableId={questaoProcessada.id}>
                        {provided => (
                          <div className={classes.fullWidth} ref={provided.innerRef} {...provided.droppableProps}>
                            {questoesDoBloco.map((questaoDoBloco, index) => (
                              <Draggable
                                key={questaoDoBloco.id}
                                draggableId={questaoDoBloco.id}
                                index={index}
                                type={'bloco' + questaoProcessada.id}
                              >
                                {provided => (
                                  <div
                                    className={classes.fullWidth}
                                    ref={provided.innerRef}
                                    key={questaoDoBloco.id}
                                    {...provided.draggableProps}
                                  >
                                    <QuestaoNaProvaEditar
                                      index={index}
                                      questaoProcessada={questaoDoBloco}
                                      dragHandleProps={provided.dragHandleProps}
                                    />
                                  </div>
                                )}
                              </Draggable>
                            ))}
                            {provided.placeholder}
                          </div>
                        )}
                      </Droppable>
                    }
                  />
                )
              }}
            </ProviderActionsComunsNaProva>
          )
        }}
      </ProviderQuestaoNaProva>
    )
  }
}
