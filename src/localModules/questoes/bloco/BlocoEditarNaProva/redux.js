import { bindActionCreators } from 'redux'
// import { actions } from 'utils/actions'

export function mapStateToProps(state) {
  return {
    strings: state.strings,
    mostrarPin: state.ProviderProva__mostrarPin,
  }
}

export function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch)
}
