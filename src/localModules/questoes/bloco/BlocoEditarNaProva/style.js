export const style = theme => {
  const barraBloco = '#33bb43'
  return {
    barraExpandir: {
      backgroundColor: barraBloco,
    },
    fullWidth: {
      width: '100%',
    },
    enunciado: {
      wordWrap: 'break-word',
      display: 'flex',
      flexWrap: 'wrap',
      padding: '0px 30px 20px',
    },
    fraseInicial: {
      marginBottom: '10px',
    },
  }
}
