import { filterObject } from '../filterObject'

import { camposComunsAusentes } from './camposComunsAusentes'

export const filtrarCamposComunsAusentes = filterObject({ except: camposComunsAusentes })
