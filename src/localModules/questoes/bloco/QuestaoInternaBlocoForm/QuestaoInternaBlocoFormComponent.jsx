import React, { Component } from 'react'

import { propTypes } from './propTypes'

import { store } from 'utils/store'

import { MultiplaEscolhaForm } from '../../multiplaEscolha/MultiplaEscolhaForm'
import { DiscursivaForm } from '../../discursiva/DiscursivaForm'
import { VOuFForm } from '../../vouf/VOuFForm'
import { AssociacaoDeColunasForm } from '../../associacaoDeColunas/AssociacaoDeColunasForm'

export class QuestaoInternaBlocoFormComponent extends Component {
  static propTypes = propTypes

  render() {
    const dicionarioQuestoesForm = {
      multiplaEscolha: MultiplaEscolhaForm,
      discursiva: DiscursivaForm,
      vouf: VOuFForm,
      associacaoDeColunas: AssociacaoDeColunasForm,
    }
    const index = this.props.accessor
    const tipo = store.getState().SimpleReduxForm__values['bloco__questoes'][index].tipo
    const FormularioQuestao = dicionarioQuestoesForm[tipo]
    return <FormularioQuestao {...this.props} />
  }
}
