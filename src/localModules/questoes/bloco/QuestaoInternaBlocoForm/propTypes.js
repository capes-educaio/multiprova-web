import PropTypes from 'prop-types'

export const propTypes = {
  valor: PropTypes.object,
  onValid: PropTypes.func,
  onChange: PropTypes.func,
  onRef: PropTypes.func,
  variant: PropTypes.oneOf(['dentroDeBloco']),
  // variant dentroDeBloco
  accessor: PropTypes.number,
  wrapperProps: PropTypes.object,
  id: PropTypes.string,
  label: PropTypes.string,
  // style
  classes: PropTypes.object.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
}
