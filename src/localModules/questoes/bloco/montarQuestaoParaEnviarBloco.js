import { tipoMap } from '../tipoMap'
import { montarQuestaoParaEnviarComum } from '../montarQuestaoParaEnviarComum'

import { filtrarCamposComunsAusentes } from './filtrarCamposComunsAusentes'

export const montarQuestaoParaEnviar = dados => {
  const { bloco, fraseInicial } = dados
  const questoesProcessadas = bloco.questoes.map(questao => {
    const montarQuestaoParaEnviar = tipoMap[questao.tipo] ? tipoMap[questao.tipo].montarQuestaoParaEnviar : null
    if (montarQuestaoParaEnviar) {
      const questaoParaEnviar = montarQuestaoParaEnviar(questao)
      delete questaoParaEnviar.status
      delete questaoParaEnviar.publico
      questaoParaEnviar.id = questao.id
      return questaoParaEnviar
    } else {
      console.error('Tipo map não tem montarQuestaoParaEnviar do tipo especificado. Tipo: ' + questao.tipo)
      return null
    }
  })
  return {
    ...filtrarCamposComunsAusentes(montarQuestaoParaEnviarComum(dados)),
    bloco: { fraseInicial, questoes: questoesProcessadas },
  }
}
