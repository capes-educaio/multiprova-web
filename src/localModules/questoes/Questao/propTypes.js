import PropTypes from 'prop-types'

const { objectOf, shape, string, func } = PropTypes

export const propTypes = {
  tipoMap: objectOf(
    shape({
      urlMatches: string.isRequired,
      getValorVazio: func.isRequired,
      montarValorInicial: func.isRequired,
      montarQuestaoParaEnviar: func.isRequired,
      Component: PropTypes.any.isRequired,
    }),
  ).isRequired,
  // redux state
  usuarioAtual: PropTypes.object.isRequired,
  questoesSelecionadasDicionario: PropTypes.object.isRequired,
  listaTags: PropTypes.array,
  valorProvaNaVolta: PropTypes.object,
  // redux actions
  selectListaTags: PropTypes.func.isRequired,
  selectQuestoesSelecionadasDicionario: PropTypes.func.isRequired,
  selectStepsValidation: PropTypes.func.isRequired,
  selectValorProvaNaVolta: PropTypes.func.isRequired,
  // style
  classes: PropTypes.object.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
}
