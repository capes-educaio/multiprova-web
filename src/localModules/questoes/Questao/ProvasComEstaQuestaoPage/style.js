export const style = theme => ({
  botoesInferiores: {
    margin: '5px',
    [theme.breakpoints.down(480)]: {
      flexGrow: 1,
    },
  },
  tituloProva: {
    fontWeight: '540',
    fontFamily: 'Roboto',
  },
})
