import React, { Component } from 'react'
import classnames from 'classnames'

import Checkbox from '@material-ui/core/Checkbox'
import Typography from '@material-ui/core/Typography'
import Divider from '@material-ui/core/Divider'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import Button from '@material-ui/core/Button'

import SendIcon from '@material-ui/icons/Send'
import ArrowBackwardIcon from '@material-ui/icons/ArrowBack'

import { PaginaPaper } from 'common/PaginaPaper'
import { TituloDaPagina } from 'common/TituloDaPagina'

import { propTypes } from './propTypes'

export class ProvasComEstaQuestaoPageComponent extends Component {
  static propTypes = propTypes

  render() {
    const {
      provas,
      classes,
      handleChange,
      strings,
      voltarParaPaginaDeOrigem,
      salvarQuestao,
      irParaPassoAnterior,
    } = this.props
    return (
      <PaginaPaper>
        <TituloDaPagina>{strings.provasComEstaQuestao}</TituloDaPagina>
        <Divider />
        <List>
          {provas.map((prova, index) => {
            return (
              <ListItem key={'prova' + index} className={classnames(classes.paperProva, 'prova')}>
                <Checkbox
                  checked={prova.deveTerEssaQuestao}
                  onChange={e => {
                    handleChange(index, e)
                    this.forceUpdate()
                  }}
                  value={prova.id}
                />
                <div>
                  <div className={classes.tituloProva}>{prova.titulo}</div>
                  <Typography>{prova.descricao}</Typography>
                </div>
              </ListItem>
            )
          })}
        </List>
        <Divider />
        <Button
          id="testButtonSalvar"
          onClick={salvarQuestao}
          className={classes.botoesInferiores}
          variant="contained"
          color="primary"
        >
          <SendIcon />
          {strings.salvar}
        </Button>
        <Button onClick={irParaPassoAnterior} className={classes.botoesInferiores}>
          <ArrowBackwardIcon />
          {strings.editarQuestao}
        </Button>
        <Button onClick={voltarParaPaginaDeOrigem} className={classes.botoesInferiores}>
          {strings.sairDaEdicao}
        </Button>
      </PaginaPaper>
    )
  }
}
