import PropTypes from 'prop-types'

export const propTypes = {
  irParaPassoAnterior: PropTypes.func.isRequired,
  mostrarErros: PropTypes.func.isRequired,
  isCurrentStepValid: PropTypes.bool.isRequired,
  voltarParaPaginaDeOrigem: PropTypes.func.isRequired,
  salvarQuestao: PropTypes.func.isRequired,
  provas: PropTypes.array.isRequired,
  handleChange: PropTypes.func.isRequired,
  // redux state
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
