import { bindActionCreators } from 'redux'
import { actions } from 'utils/actions'

export function mapStateToProps(state) {
  return {
    strings: state.strings,
    usuarioAtual: state.usuarioAtual,
    questoesSelecionadasDicionario: state.questoesSelecionadasDicionario,
    listaTags: state.listaTags,
    stepsValidation: state.StepperSimples__stepsValidation,
    stepsShowError: state.StepperSimples__stepsShowError,
    valorProvaNaVolta: state.valorProvaNaVolta,
  }
}

export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      selectListaTags: actions.select.listaTags,
      selectQuestoesSelecionadasDicionario: actions.select.questoesSelecionadasDicionario,
      selectStepsValidation: actions.select.StepperSimples__stepsValidation,
      selectStepsShowError: actions.select.StepperSimples__stepsShowError,
      selectValorProvaNaVolta: actions.select.valorProvaNaVolta,
    },
    dispatch,
  )
}
