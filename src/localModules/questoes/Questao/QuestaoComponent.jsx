import React, { Component } from 'react'

import CircularProgress from '@material-ui/core/CircularProgress'
import Divider from '@material-ui/core/Divider'

import { TituloDaPagina } from 'common/TituloDaPagina'
import { PaginaPaper } from 'common/PaginaPaper'
import { AlertaDeConfirmacao } from 'common/AlertaDeConfirmacao'

import { setUrlState, replaceState } from 'localModules/urlState'

import { get, post, patch } from 'api'

import { Snackbar } from 'utils/Snackbar'
import { setListaTags } from 'utils/compositeActions'
import { saoIguaisDeepEqual } from 'utils/compararObjetos'
import { enumTipoQuestao } from 'utils/enumTipoQuestao'

import { propTypes } from './propTypes'
import { defaultProps } from './defaultProps'

export class QuestaoComponent extends Component {
  static propTypes = propTypes
  static defaultProps = defaultProps

  _dadosMontados
  _tipo
  _isEdicao = true
  _questaoAtualId
  _foiChamadoFetchQuestaoParaEdicao = false
  _sairSemSalvar = false

  constructor(props, context) {
    super(props, context)
    const { tipoMap } = props
    setListaTags()
    const pathEnd = props.match.params.id
    const quantAlternativas = context ? 1 : 5
    for (let key in tipoMap) {
      if (tipoMap[key].urlMatches === pathEnd) {
        this._tipo = key
        this._isEdicao = false
      }
    }
    if (this._isEdicao) this._questaoAtualId = pathEnd
    this.state = {
      carregando: this._isEdicao,
      valor: this._isEdicao ? null : tipoMap[this._tipo].getValorVazio(quantAlternativas),
      valorAntigo: {},
      isModoPreviewLigado: false,
      provas: [],
      snackbarIsOpen: false,
      valorFoiModificado: false,
      alertaAberto: false,
    }
  }

  componentDidMount = () => {
    this.props.selectStepsValidation(Array(2).fill(true))
    this.props.selectStepsShowError(Array(2).fill(false))
    if (this._isEdicao && this.props.listaTags) this._fetchQuestaoParaEdicao()
  }

  componentDidUpdate = prevProps => {
    const { listaTags } = this.props
    if (this._isEdicao && !this._foiChamadoFetchQuestaoParaEdicao && !!listaTags) {
      this._fetchQuestaoParaEdicao()
    }
  }

  componentWillUnmount = () => this.props.selectListaTags(null)

  get rotasNormais() {
    const { usuarioAtual } = this.props
    const acesso = id => `/questoes/${id}`
    return {
      back: {
        get: `/usuarios/${usuarioAtual.data.id}/questoes/`,
        post: `/questoes/`,
        getExclusao: acesso,
        patch: acesso,
      },
      front: {
        add: '/questao',
        gerenciamento: '/questoes',
        editar: id => `questao/${id}`,
      },
    }
  }

  get rotas() {
    return this.rotasNormais
  }

  get _Component() {
    return this.props.tipoMap[this._tipo].Component
  }

  get _deveVoltarParaProva() {
    const { deveVoltarParaEsseUrlState } = this.props.location.state.hidden
    return (
      deveVoltarParaEsseUrlState &&
      (deveVoltarParaEsseUrlState.pathname.split('/')[3] === 'prova' ||
        deveVoltarParaEsseUrlState.pathname.split('/')[1] === 'prova')
    )
  }

  _fetchQuestaoParaEdicao = () => {
    setListaTags()
    this._foiChamadoFetchQuestaoParaEdicao = true
    const options = {
      filter: {
        include: ['tags'],
        where: { id: `${this._questaoAtualId}` },
      },
    }
    const url = this.rotas.back.get
    get(url, options).then(this._setValorParaEdicao)
  }

  _montarQuestaoParaEnviar = data => {
    return this.props.tipoMap[this._tipo].montarQuestaoParaEnviar(data)
  }

  _montarValorInicial = data => this.props.tipoMap[this._tipo].montarValorInicial(data)

  _setValorParaEdicao = async response => {
    const data = response.data[0]
    if (this._verificaSeAchou(data)) {
      this._tipo = data.tipo
      const valor = await this._montarValorInicial(data)
      this.setState({
        valor,
        valorAntigo: JSON.parse(JSON.stringify(valor)),
        carregando: false,
      })
    }
  }

  _verificaSeAchou = questao => {
    if (!questao) {
      replaceState({ pathname: '/404' })
      return false
    }
    return true
  }

  _cadastrarQuestao = async () => {
    const { valor } = this.state
    const data = this._montarQuestaoParaEnviar(valor)
    const url = this.rotas.back.post
    const { data: questao } = await post(url, data)
    this._atualizarQuestaoEmTodasAsProvas(questao)
    if (this._deveVoltarParaProva) this._adicionarQuestaoProvaNaVolta(questao)
    this._questaoAtualId = questao.id
    this.setState({
      snackbarIsOpen: true,
      valorFoiModificado: false,
      valorAntigo: JSON.parse(JSON.stringify(valor)),
    })
    this._isEdicao = true
    this._foiChamadoFetchQuestaoParaEdicao = false
    return questao
  }

  _editarQuestao = async () => {
    const { valor, provas } = this.state
    const { valorProvaNaVolta } = this.props
    const data = this._montarQuestaoParaEnviar(valor)
    delete data.tipo
    const url = this.rotas.back.patch(this._questaoAtualId)
    const verbo = patch
    const { data: questao } = await verbo(url, data)
    this._atualizarQuestaoEmTodasAsProvas(questao)
    if (this._deveVoltarParaProva && valorProvaNaVolta && Array.isArray(provas)) {
      const valorAtualProvaModificada = provas.filter(({ id }) => id === valorProvaNaVolta.id)
      if (
        valorAtualProvaModificada[0] &&
        valorProvaNaVolta.questaoIds.length > valorAtualProvaModificada[0].questaoIds.length
      ) {
        this._retirarQuestaoProvaNaVolta(questao)
      } else {
        this._atualizarQuestaoProvaNaVolta(questao)
      }
    }
    this.setState({
      snackbarIsOpen: true,
      valorFoiModificado: false,
      valorAntigo: JSON.parse(JSON.stringify(valor)),
    })
    this._foiChamadoFetchQuestaoParaEdicao = false
    return questao
  }

  _adicionarQuestaoProvaNaVolta = questao => {
    const { selectValorProvaNaVolta, valorProvaNaVolta } = this.props
    const { grupos } = valorProvaNaVolta
    grupos[grupos.length - 1].questoes.push(questao)
    valorProvaNaVolta.questaoIds.push(questao.id)
    valorProvaNaVolta.questoes.push(questao)
    selectValorProvaNaVolta({ ...valorProvaNaVolta })
  }

  _retirarQuestaoProvaNaVolta = questao => {
    const { selectValorProvaNaVolta, valorProvaNaVolta } = this.props
    const {
      grupos: gruposAntigos = [],
      questaoIds: questaoIdsAntigas = [],
      questoes: questoesAntigas = [],
    } = valorProvaNaVolta
    const { id } = questao

    const grupos = gruposAntigos.map(grupo => {
      const { questoes = [] } = grupo
      return {
        ...grupo,
        questoes: questoes.filter(questao => questao.id !== id),
      }
    })

    const questaoIds = questaoIdsAntigas.filter(questaoId => questaoId !== id)
    const questoes = questoesAntigas.filter(questao => questao.id !== id)

    selectValorProvaNaVolta({ ...valorProvaNaVolta, grupos, questaoIds, questoes })
  }

  _atualizarQuestaoProvaNaVolta = questao => {
    const { valorProvaNaVolta, selectValorProvaNaVolta } = this.props
    const { grupos } = valorProvaNaVolta

    let grupoIndexDaQuestao, questaoIndexDaQuestao
    grupos.some((grupo, grupoIndex) => {
      return grupo.questoes.some(({ id }, questaoIndex) => {
        if (questao.id === id) {
          ;[questaoIndexDaQuestao, grupoIndexDaQuestao] = [questaoIndex, grupoIndex]
          return true
        } else {
          return false
        }
      })
    })
    const questoesIndex = valorProvaNaVolta.questoes.findIndex(({ id }) => questao.id === id)
    if (grupoIndexDaQuestao > -1 && questaoIndexDaQuestao > -1 && questoesIndex > -1) {
      grupos[grupoIndexDaQuestao].questoes[questaoIndexDaQuestao] = questao
      valorProvaNaVolta.questoes[questoesIndex] = questao
      selectValorProvaNaVolta({ ...valorProvaNaVolta })
    } else {
      console.error(`Questao (${questao.id}) não achada na prova.`)
      console.error('Valor da prova:', valorProvaNaVolta)
    }
  }

  _atualizarQuestaoEmTodasAsProvas = questao => {
    const idQuestao = questao.id
    const { provas } = this.state
    const questaoNogrupo = {
      questaoId: idQuestao,
      fixa: false,
    }
    let provasSemDeveTer = []
    let bloco = []
    if (questao.bloco) {
      questao.bloco.questoes.forEach(questaoDoBloco => {
        return bloco.push({ questaoId: questaoDoBloco.id, fixa: false })
      })
      questaoNogrupo.bloco = bloco
    }

    provas.forEach(prova => {
      let { deveTerEssaQuestao, ...restoDaProva } = prova
      if (this._deveAtualizarProva(prova)) {
        if (deveTerEssaQuestao) {
          restoDaProva.questaoIds.push(idQuestao)
          restoDaProva.grupos[prova.grupos.length - 1].questoes.push(questaoNogrupo)
        } else {
          restoDaProva.questaoIds.splice(prova.questaoIds.indexOf(idQuestao), 1)
          restoDaProva.grupos[prova.grupos.length - 1].questoes.splice(
            restoDaProva.grupos[prova.grupos.length - 1].questoes.indexOf(questaoNogrupo),
            1,
          )
        }
      }
      provasSemDeveTer.push(restoDaProva)
    })
  }

  _guardarQuestaoParaSerUsadaNaListaDeVolta = questao => {
    const { questoesSelecionadasDicionario } = this.props
    questoesSelecionadasDicionario[questao.id] = questao
    this.props.selectQuestoesSelecionadasDicionario(questoesSelecionadasDicionario)
  }

  _selecionarQuestaoNaListaDeVolta = (deveVoltarParaEsseUrlState, questao) => {
    this._guardarQuestaoParaSerUsadaNaListaDeVolta(questao)
    deveVoltarParaEsseUrlState.hidden.idsCardsSelecionados.push(questao.id)
    return deveVoltarParaEsseUrlState
  }

  _voltarParaPaginaDeOrigem = () => {
    if (this.state.valorFoiModificado && !this._sairSemSalvar) {
      this.abrirAvisoCancelar()
    } else {
      const { location } = this.props
      let { deveVoltarParaEsseUrlState } = location.state.hidden
      if (deveVoltarParaEsseUrlState) setUrlState(deveVoltarParaEsseUrlState)
      else setUrlState({ pathname: this.rotas.front.gerenciamento })
    }
  }

  _handleValorChange = novoValor => {
    const { valor, valorAntigo, valorFoiModificado } = this.state

    if (!valorFoiModificado && !saoIguaisDeepEqual(valor, valorAntigo)) {
      this.setState({
        valorAntigo: JSON.parse(JSON.stringify(valor)),
        valor: novoValor,
        valorFoiModificado: true,
      })
    } else {
      this.setState({ valor: novoValor })
    }
  }

  _ativarModoVisualizacao = () => {
    const { valor } = this.state
    this._dadosMontados = this._montarQuestaoParaEnviar(valor)
    if (valor.tipo === enumTipoQuestao.tipoQuestaoBloco) {
      this._dadosMontados.bloco.questoes = this._dadosMontados.bloco.questoes.map(questaoArg => {
        const questao = { ...questaoArg }
        const propriedadesParaDeletar = ['dificuldade', 'elaborador', 'tagsVirtuais', 'anoEscolar']
        propriedadesParaDeletar.forEach(key => delete questao[key])
        return questao
      })
    }
    this.setState({ isModoPreviewLigado: true })
  }

  _ativarModoEdicao = () => this.setState({ isModoPreviewLigado: false })

  handleChangeProva = index => {
    let { provas } = this.state
    if (provas[index]) {
      provas[index].deveTerEssaQuestao = !provas[index].deveTerEssaQuestao
      this.setState({ provas })
    }
  }

  _deveAtualizarProva = prova => {
    const provaTinhaQuestao = prova.questaoIds.includes(this._questaoAtualId)
    return (provaTinhaQuestao && !prova.deveTerEssaQuestao) || (!provaTinhaQuestao && prova.deveTerEssaQuestao)
  }
  snackbarShoudClose = () => {
    this.setState({ snackbarIsOpen: false })
  }
  handleCloseCancelar = confirmaCancelar => {
    if (confirmaCancelar === true) {
      this._sairSemSalvar = true
      this._voltarParaPaginaDeOrigem()
    }
    this.setState({
      alertaAberto: false,
    })
  }
  abrirAvisoCancelar = () => this.setState({ alertaAberto: true })

  render() {
    const { classes, strings } = this.props
    const { valor, carregando, isModoPreviewLigado, snackbarIsOpen, alertaAberto } = this.state

    if (carregando)
      return (
        <PaginaPaper>
          <TituloDaPagina>{this._isEdicao ? strings.editarQuestao : strings.cadastrarQuestao}</TituloDaPagina>
          <Divider />
          <div className={classes.carregando}>
            <CircularProgress />
          </div>
        </PaginaPaper>
      )
    return (
      <div>
        <Snackbar
          variant="success"
          open={snackbarIsOpen}
          onClose={this.snackbarShoudClose}
          message={<span id="snackbar-sucesso">{strings.asAlteracoesForamSalvas}</span>}
          autoHideDuration={3000}
        />
        <AlertaDeConfirmacao
          alertaAberto={alertaAberto}
          descricaoSelected={strings.asAlteracoesNaoSalvasSeraoPerdidas}
          handleClose={this.handleCloseCancelar}
          stringNomeInstancia={strings.sairSemSalvarQuestao}
        />

        <this._Component
          valor={valor}
          dadosMontados={this._dadosMontados}
          carregando={carregando}
          isModoPreviewLigado={isModoPreviewLigado}
          isEdicao={this._isEdicao}
          handleValorChange={this._handleValorChange}
          ativarModoVisualizacao={this._ativarModoVisualizacao}
          ativarModoEdicao={this._ativarModoEdicao}
          editarQuestao={this._editarQuestao}
          cadastrarQuestao={this._cadastrarQuestao}
          voltarParaPaginaDeOrigem={this._voltarParaPaginaDeOrigem}
        />
      </div>
    )
  }
}
