import React, { Component } from 'react'
import uid from 'uuid/v4'

import { CampoText, CampoSlate, CampoSlider, CampoTag } from 'localModules/ReduxForm'

import { CampoQuestaoStatus } from './CampoQuestaoStatus'
import { CampoQuestaoAnoEscolar } from './CampoQuestaoAnoEscolar'
import { appConfig } from 'appConfig'
import { propTypes } from './propTypes'
import { getSliderCaption } from './getSliderCaption'

export class QuestaoCamposComunsComponent extends Component {
  static propTypes = propTypes
  _id = uid()
  _enunciadoId = uid()
  _dificuldadeId = uid()
  _elaboradorId = uid()
  _tagId = uid()
  _statusId = uid()
  _anoEscolarId = uid()

  render() {
    const { strings, children, classes } = this.props
    const campoAnoEscolar = (
      <CampoQuestaoAnoEscolar
        key={this._anoEscolarId}
        id={'enunciado-' + this._anoEscolarId}
        accessor="anoEscolar"
        required
      />
    )
    const campoEnunciado = (
      <CampoSlate
        key={this._enunciadoId}
        id={'enunciado-' + this._enunciadoId}
        accessor="enunciado"
        inputProps={{ placeholder: strings.enunciado, rows: 3, editorProps: { id: 'form-questao-enunciado' } }}
        required
      />
    )
    const campoDificuldade = (
      <CampoSlider
        classes={{ form: classes.form }}
        key={this._dificuldadeId}
        id={'dificuldade-' + this._dificuldadeId}
        accessor="dificuldade"
        label={strings.dificuldade}
        sliderProps={{
          min: 1,
          max: 3,
          step: 1,
          caption: getSliderCaption(strings),
        }}
      />
    )
    const campoElaborador = (
      <CampoText
        key={this._elaboradorId}
        id={'elaborador-' + this._elaboradorId}
        accessor="elaborador"
        label={strings.elaboradoPor}
        textFieldProps={{ style: { width: 300, maxWidth: '100%' } }}
      />
    )
    const campoId = (
      <CampoText classes={{ wrapper: classes.campoId }} key={'id-' + this._id} id={'id-' + this._id} accessor="id" />
    )
    const campoTag = (
      <CampoTag key={this._tagId} id={'tags'} accessor="tags" tagProps={{ placeholder: strings.adicionarTags }} />
    )
    const campoStatus = (
      <CampoQuestaoStatus key={this._statusId} id={'statusId-' + this._statusId} accessor="statusId" />
    )
    return children({
      campoEnunciado,
      campoDificuldade,
      campoElaborador: !appConfig.projeto.isEducaio ? campoElaborador : <div />,
      campoTag,
      campoStatus: !appConfig.projeto.isEducaio ? campoStatus : <div />,
      campoAnoEscolar,
      campoId,
    })
  }
}
