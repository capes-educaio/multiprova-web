export const getSliderCaption = strings => value => {
  if (value === 1) return strings.facil
  if (value === 2) return strings.medio
  if (value === 3) return strings.dificil
  return null
}
