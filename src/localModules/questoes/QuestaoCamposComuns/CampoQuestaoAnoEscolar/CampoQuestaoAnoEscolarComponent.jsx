import React, { Component } from 'react'

import MenuItem from '@material-ui/core/MenuItem'

import { enumAnoEscolar } from 'utils/enumAnoEscolar'

import { CampoSelect } from 'localModules/ReduxForm/CampoSelect'

import { propTypes } from './propTypes'

export class CampoQuestaoAnoEscolarComponent extends Component {
  static propTypes = propTypes

  render() {
    const { strings, campoSelectProps, id, accessor, required } = this.props
    return (
      <CampoSelect
        id={id}
        accessor={accessor}
        helperText={strings.anoEscolar}
        required={required}
        {...campoSelectProps}
      >
        <MenuItem value={enumAnoEscolar.ensinoSuperiorAvancado}>{strings.ensinoSuperiorAvancado}</MenuItem>
        <MenuItem value={enumAnoEscolar.ensinoSuperiorBasico}>{strings.ensinoSuperiorBasico}</MenuItem>
        <MenuItem value={enumAnoEscolar.terceiroAnoEnsinoMedio}>{strings.terceiroAnoEnsinoMedio}</MenuItem>
        <MenuItem value={enumAnoEscolar.segundoAnoEnsinoMedio}>{strings.segundoAnoEnsinoMedio}</MenuItem>
        <MenuItem value={enumAnoEscolar.primeiroAnoEnsinoMedio}>{strings.primeiroAnoEnsinoMedio}</MenuItem>
        <MenuItem value={enumAnoEscolar.ensinoFundamental2}>{strings.ensinoFundamental2}</MenuItem>
        <MenuItem value={enumAnoEscolar.ensinoFundamental1}>{strings.ensinoFundamental1}</MenuItem>
      </CampoSelect>
    )
  }
}
