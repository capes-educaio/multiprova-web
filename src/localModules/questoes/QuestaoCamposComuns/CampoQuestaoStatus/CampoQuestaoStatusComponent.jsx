import React, { Component } from 'react'

import MenuItem from '@material-ui/core/MenuItem'

import { enumStatusQuestao } from 'utils/enumStatusQuestao'

import { CampoSelect } from 'localModules/ReduxForm/CampoSelect'

import { propTypes } from './propTypes'

export class CampoQuestaoStatusComponent extends Component {
  static propTypes = propTypes

  render() {
    const { strings, campoSelectProps, id, accessor } = this.props
    return (
      <CampoSelect id={id} accessor={accessor} helperText={strings.statusQuestao} {...campoSelectProps}>
        <MenuItem value={enumStatusQuestao.elaboracao.id}>{enumStatusQuestao.elaboracao.texto}</MenuItem>
        <MenuItem value={enumStatusQuestao.primeiraRevisaoPedagogica.id}>
          {enumStatusQuestao.primeiraRevisaoPedagogica.texto}
        </MenuItem>
        <MenuItem value={enumStatusQuestao.validacaoDaRevisaoPedagogica.id}>
          {enumStatusQuestao.validacaoDaRevisaoPedagogica.texto}
        </MenuItem>
        <MenuItem value={enumStatusQuestao.revisaoDeConteudo.id}>{enumStatusQuestao.revisaoDeConteudo.texto}</MenuItem>
        <MenuItem value={enumStatusQuestao.revisaoDeLinguagem.id}>
          {enumStatusQuestao.revisaoDeLinguagem.texto}
        </MenuItem>
        <MenuItem value={enumStatusQuestao.validacaoDaRevisaoDeLinguagem.id}>
          {enumStatusQuestao.validacaoDaRevisaoDeLinguagem.texto}
        </MenuItem>
        <MenuItem value={enumStatusQuestao.liberadoParaImpressao.id}>
          {enumStatusQuestao.liberadoParaImpressao.texto}
        </MenuItem>
      </CampoSelect>
    )
  }
}
