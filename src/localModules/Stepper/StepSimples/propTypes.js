import PropTypes from 'prop-types'

export const propTypes = {
  children: PropTypes.func.isRequired,
  stepIndex: PropTypes.number.isRequired,
  // redux state
  stepsValidation: PropTypes.arrayOf(PropTypes.bool).isRequired,
  // router
  location: PropTypes.object.isRequired,
  // strings
  string: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
