import { Component } from 'react'

import { updateUrlState } from 'localModules/urlState'

export class StepSimplesComponent extends Component {
  _irParaPassoAnterior = () => updateUrlState({ hidden: { activeStep: this.props.stepIndex - 1 } })

  _irParaProximoPasso = () => updateUrlState({ hidden: { activeStep: this.props.stepIndex + 1 } })

  render() {
    const { stepsValidation, stepIndex, children, location } = this.props
    const { activeStep } = location.state.hidden
    const isCurrentStepValid = stepsValidation[stepIndex]
    let allStepsAreValid = false
    if (Array.isArray(stepsValidation)) {
      allStepsAreValid = !stepsValidation.some(isValid => isValid === false)
    }
    if (activeStep !== stepIndex) return null
    return children({
      isCurrentStepValid,
      allStepsAreValid,
      irParaPassoAnterior: this._irParaPassoAnterior,
      irParaProximoPasso: this._irParaProximoPasso,
    })
  }
}
