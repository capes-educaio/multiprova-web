import { bindActionCreators } from 'redux'
import { actions } from 'utils/actions'

export function mapStateToProps(state) {
  return {
    strings: state.strings,
    stepsValidation: state.StepperSimples__stepsValidation,
    stepsShowError: state.StepperSimples__stepsShowError,
  }
}

export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      selectStepsValidation: actions.select.StepperSimples__stepsValidation,
      selectStepsShowError: actions.select.StepperSimples__stepsShowError,
    },
    dispatch,
  )
}
