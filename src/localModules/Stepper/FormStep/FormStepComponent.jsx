import React, { Component } from 'react'
import { StepSimples } from '../StepSimples'

export class FormStepComponent extends Component {
  _formRef

  _setFormRef = ref => (this._formRef = ref)

  _mostrarErros = () => {
    if (this._formRef && this._formRef.showError) this._formRef.showError()
    else console.error('Algum problema com a ref do form.')
    this._showErrorStep()
  }

  _handleStepOnValid = isValid => {
    const { stepsValidation, selectStepsValidation, stepIndex } = this.props
    stepsValidation[stepIndex] = isValid
    selectStepsValidation([...stepsValidation])
  }

  _showErrorStep = () => {
    const { stepsShowError, selectStepsShowError, stepIndex } = this.props
    stepsShowError[stepIndex] = true
    selectStepsShowError([...stepsShowError])
  }

  render() {
    const { stepIndex, children } = this.props
    return (
      <StepSimples stepIndex={stepIndex}>
        {({ isCurrentStepValid, irParaPassoAnterior, irParaProximoPasso }) =>
          children({
            isCurrentStepValid,
            mostrarErros: this._mostrarErros,
            setFormRef: this._setFormRef,
            irParaPassoAnterior,
            irParaProximoPasso,
            showErrorStep: this._showErrorStep,
            handleOnValid: this._handleStepOnValid,
          })
        }
      </StepSimples>
    )
  }
}
