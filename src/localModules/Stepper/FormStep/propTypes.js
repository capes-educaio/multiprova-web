import PropTypes from 'prop-types'

export const propTypes = {
  children: PropTypes.func.isRequired,
  showErrorStep: PropTypes.func.isRequired,
  stepIndex: PropTypes.number.isRequired,
  // redux state
  stepsValidation: PropTypes.arrayOf(PropTypes.bool).isRequired,
  stepsShowError: PropTypes.arrayOf(PropTypes.bool).isRequired,
  // redux actions
  selectStepsValidation: PropTypes.func.isRequired,
  selectStepsShowError: PropTypes.func.isRequired,
  // strings
  string: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
