export const style = theme => ({
  root: {
    padding: '10px',
  },
  stepLabel: {
    userSelect: 'none',
  },
  label: {
    '& > *': {
      marginTop: '5px !important',
    },
  },
  clickableLabel: {
    cursor: 'pointer',
  },
  disabledLabel: {
    opacity: '0.5',
    cursor: 'not-allowed',
  },
  iconContainer: {
    opacity: '0.5',
  },
})
