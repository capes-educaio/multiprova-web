import PropTypes from 'prop-types'

export const propTypes = {
  activeStep: PropTypes.number.isRequired,
  steps: PropTypes.arrayOf(PropTypes.shape({ label: PropTypes.string.isRequired })).isRequired,
  beforeStepChange: PropTypes.func, // tem que retornar uma promise
  onStepChange: PropTypes.func,
  // redux state
  stepsValidation: PropTypes.arrayOf(PropTypes.bool).isRequired,
  stepsShowError: PropTypes.arrayOf(PropTypes.bool).isRequired,
  // redux actions
  selectStepsValidation: PropTypes.func.isRequired,
  selectStepsShowError: PropTypes.func.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
