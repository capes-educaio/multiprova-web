import React, { Component } from 'react'

import Stepper from '@material-ui/core/Stepper'
import Step from '@material-ui/core/Step'
import StepLabel from '@material-ui/core/StepLabel'

import { updateUrlState } from 'localModules/urlState'

import { propTypes } from './propTypes'
import { getInitialStore } from './getInitialStore'

// singleton
export class StepperSimplesComponent extends Component {
  static propTypes = propTypes

  constructor(props) {
    super(props)
    if (props.onRef) props.onRef(this)
  }

  componentWillUnmount = () => {
    if (this.props.onRef) this.props.onRef(null)
    const initialStore = getInitialStore()
    this.props.selectStepsValidation(initialStore.StepperSimples__stepsValidation)
    this.props.selectStepsShowError(initialStore.StepperSimples__stepsShowError)
  }

  _onClickLabel = index => () => {
    const { onChange, activeStep, stepsShowError, selectStepsShowError } = this.props
    stepsShowError[activeStep] = true
    selectStepsShowError([...stepsShowError])
    if (onChange) onChange(index)()
    else this.irParaPasso(index)
  }

  irParaPasso = async newStep => {
    const { onStepChange, activeStep, beforeStepChange } = this.props
    if (onStepChange) onStepChange({ activeStep, newStep })
    if (beforeStepChange) {
      const { mudarStep } = await beforeStepChange({ activeStep, newStep })
      if (mudarStep) updateUrlState({ hidden: { activeStep: newStep } })
    } else updateUrlState({ hidden: { activeStep: newStep } })
  }

  render() {
    const { classes, activeStep, steps, stepsValidation, stepsShowError } = this.props
    return (
      <Stepper activeStep={activeStep} nonLinear alternativeLabel classes={{ root: classes.root }} color="secondary">
        {stepsValidation &&
          steps.map((step, index) => {
            const stepProps = {
              id: step.id,
              key: index,
              className: classes.step,
            }
            const labelProps = {
              className: `${classes.stepLabel} ${
                activeStep !== index && stepsValidation[index] ? classes.clickableLabel : ''
              }`,
              children: step.label,
              classes: {
                labelContainer: classes.label,
              },
            }
            const isCurrentStep = activeStep === index
            const isValidatedStep = stepsValidation[index]
            const allPreviousSteps = stepsValidation.slice(0, index === 0 ? 0 : index - 1)
            const isValidAllPreviousSteps = allPreviousSteps.every(isValid => isValid)
            const clickableLabel = !isCurrentStep && isValidAllPreviousSteps && stepsValidation[activeStep]
            if (clickableLabel) {
              stepProps.onClick = this._onClickLabel(index)
              labelProps.classes.iconContainer = classes.iconContainer
            }
            const disabledLabel = !clickableLabel && !isCurrentStep
            if (disabledLabel) labelProps.className = `${labelProps.className} ${classes.disabledLabel}`
            const stepError = activeStep === index && !isValidatedStep
            const showError = stepError && stepsShowError[activeStep]
            if (showError) labelProps.error = true
            else if (step.icon) labelProps.icon = step.icon
            return (
              <Step {...stepProps}>
                <StepLabel {...labelProps} />
              </Step>
            )
          })}
        {!stepsValidation && null}
      </Stepper>
    )
  }
}
