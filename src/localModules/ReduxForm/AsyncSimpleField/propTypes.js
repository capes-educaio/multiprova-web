import PropTypes from 'prop-types'

export const propTypes = {
  children: PropTypes.func.isRequired,
  id: PropTypes.string.isRequired,
  accessor: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  setShowError: PropTypes.func.isRequired,
  label: PropTypes.string,
  helperText: PropTypes.string,
  validar: PropTypes.any,
  validadeWith: PropTypes.arrayOf(PropTypes.string),
  required: PropTypes.bool,
  emptyValue: PropTypes.any,
  mensagensDeErro: PropTypes.object,
  // redux state
  values: PropTypes.object.isRequired,
  refs: PropTypes.object.isRequired,
  sortables: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired,
  showError: PropTypes.object.isRequired,
  // redux actions
  updateValues: PropTypes.func.isRequired,
  updateRefs: PropTypes.func.isRequired,
  updateShowError: PropTypes.func.isRequired,
  updateErrors: PropTypes.func.isRequired,
  deleteRefs: PropTypes.func.isRequired,
  deleteValues: PropTypes.func.isRequired,
  deleteErrors: PropTypes.func.isRequired,
  deleteShowError: PropTypes.func.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
