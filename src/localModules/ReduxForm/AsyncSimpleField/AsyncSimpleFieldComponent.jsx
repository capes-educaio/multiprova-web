import { Component } from 'react'
import uid from 'uuid/v4'

import { propTypes } from './propTypes'

export class AsyncSimpleFieldComponent extends Component {
  static propTypes = propTypes
  invalid = false
  _initialValue

  state = {
    loading: false,
    lastValidation: null,
  }

  showError = () => this._updateShowError(true)

  hideError = () => this._updateShowError(true)

  constructor(props) {
    super(props)
    this.state = { ownErrors: [] }
    this._checkId()
    props.updateRefs({ [props.id]: this })
    props.setShowError(this.showError)
    props.updateErrors({ [props.id]: [] })
    this._initialValue = props.values[props.id]
  }

  componentDidMount = () => {
    const { values, id } = this.props
    this._handleValidate(values[id])
  }

  shouldComponentUpdate = next => {
    const { values, errors, showError, validateWith } = this.props
    const { id } = next
    if (values[id] !== next.values[id]) return true
    else if (errors[id] !== next.errors[id]) return true
    else if (showError[id] !== next.showError[id]) return true
    else if (Array.isArray(validateWith)) {
      return validateWith.some(id => values[id] !== next.values[id])
    }
    return false
  }

  componentWillUnmount = () => {
    this.props.deleteRefs(this.props.id)
    this.props.deleteValues(this.props.id)
    this.props.deleteErrors(this.props.id)
    this.props.deleteShowError(this.props.id)
    this.props.setShowError(() => console.error('AsyncSimpleField não está montado.'))
  }

  get isSortable() {
    const { sortables, id } = this.props
    return Boolean(sortables[id])
  }

  _updateShowError = value => {
    const { updateShowError, id } = this.props
    updateShowError({ [id]: value })
  }

  _onBlur = () => {
    const { values, id } = this.props
    this.showError()
    this._handleValidate(values[id])
  }

  _checkId = () => {
    const { id, refs, accessor } = this.props
    if (id && refs[id]) {
      this.invalid = true
      console.error(
        `Todos os campos devem ter um id único. Id duplicado: ${id};
        Accessor do campo problemático: ${accessor}`,
      )
    }
  }

  _handleValidate = value => {
    const { validar } = this.props
    const validationId = uid()
    this.setState({ loading: true, lastValidation: validationId })
    const errors = this._defaultValidate(value)
    if (validar) {
      validar(value, errors)
        .then(asyncErrors => this._updateErrors(validationId, [...errors, ...asyncErrors]))
        .catch(() => this._updateErrors(validationId, [{ code: 'async_validate_fail' }]))
    } else this._updateErrors(validationId, errors)
  }

  _defaultValidate = value => {
    const { required, emptyValue, strings, mensagensDeErro } = this.props
    const requiredNotMet = required && (value === null || value === undefined || value === emptyValue)
    if (requiredNotMet) {
      return [
        {
          code: 'campo_obrigatorio',
          message: mensagensDeErro['campo_obrigatorio']
            ? mensagensDeErro['campo_obrigatorio']
            : strings.campoObrigatorio,
        },
      ]
    } else return []
  }

  _updateErrors = (validationId, errors) => {
    const { id, updateErrors } = this.props
    const { lastValidation } = this.state
    if (validationId === lastValidation) {
      this.setState({ loading: false })
      updateErrors({ [id]: errors })
    }
  }

  _onChange = newValue => {
    const { updateValues, id } = this.props
    updateValues({ [id]: newValue })
  }

  render() {
    let { label, showError, id, children, errors, required, values } = this.props
    const { loading } = this.state
    if (this.invalid || values[id] === undefined) return null
    if (label && required) label = label + '*'
    return children({
      label,
      showError: showError[id],
      errors: errors[id],
      onFocus: this.hideError,
      onBlur: this._onBlur,
      onChange: this._onChange,
      isSortable: this.isSortable,
      value: values[id],
      loading,
      initialValue: this._initialValue,
    })
  }
}
