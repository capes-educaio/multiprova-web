import React, { Component } from 'react'

import { ImageInput } from 'common/ImageInput'

import { SimpleField } from '../SimpleField'

import { propTypes } from './propTypes'
import { defaultProps } from './defaultProps'

export class CampoImagemComponent extends Component {
  static propTypes = propTypes
  static defaultProps = defaultProps
  showError

  render() {
    const { id, label, helperText, validar, WrapperComponent, required, mensagensDeErro } = this.props
    const { validateWith, classes, inputProps, wrapperProps, accessor } = this.props
    return (
      <WrapperComponent className={classes.wrapper} {...wrapperProps}>
        <SimpleField
          id={id}
          accessor={accessor}
          label={label}
          validar={validar}
          validateWith={validateWith}
          required={required}
          emptyValue=""
          mensagensDeErro={mensagensDeErro}
          setShowError={showError => {
            this.showError = showError
          }}
        >
          {({ label, showError, errors, onFocus, onBlur, onChange, isSortable, value, setInputRef }) => {
            const hasError = Array.isArray(errors) && errors.length > 0
            const errorMessage = showError && hasError ? errors[0].message : helperText
            const props = {
              ...inputProps,
              onFocus,
              onBlur,
              error: showError && hasError,
              errorMessage,
            }
            return <ImageInput {...props} value={value} onChange={onChange} label={label} />
          }}
        </SimpleField>
      </WrapperComponent>
    )
  }
}
