import React, { Component } from 'react'
import uid from 'uuid/v4'

import { values } from '../values'
import { fatherFieldMap } from '../fatherFieldMap'
import { fatherFormMap } from '../fatherFormMap'
import { formFieldMap } from '../formFieldMap'

import { propTypes } from './propTypes'
import { defaultProps } from './defaultProps'

export class CampoArrayComponent extends Component {
  static propTypes = propTypes
  static defaultProps = defaultProps
  isField = true
  _invalid

  validate = () => {
    const value = values[this.props.id]
    if (Array.isArray(value)) {
      const errors = this._validate(values[this.props.id])
      if (Array.isArray(errors)) this._updateErrors(errors)
    }
  }

  addField = (value, atPosition, withThisId) => {
    const { updateValues } = this.props
    const { fields } = this.state
    const position = typeof atPosition === 'number' ? atPosition : fields.length
    const id = typeof withThisId === 'string' ? withThisId : uid()
    const formId = fatherFormMap[this.props.id]
    fatherFormMap[id] = formId
    formFieldMap[formId].push(id)
    fatherFieldMap[id] = this.props.id
    values[id] = value
    values[this.props.id].splice(position, 0, value)
    fields.splice(position, 0, id)
    updateValues({ [this.props.id]: values[this.props.id], [id]: value })
    this.setState({ fields: [...fields] })
    this._updateFather({ childId: this.props.id })
  }

  moveField = ({ oldIndex, newIndex }) => {
    const { updateValues, id } = this.props
    const { fields } = this.state
    const movingField = fields[oldIndex]
    fields.splice(oldIndex, 1)
    fields.splice(newIndex, 0, movingField)
    const movingValue = values[id][oldIndex]
    values[id].splice(oldIndex, 1)
    values[id].splice(newIndex, 0, movingValue)
    updateValues({ [id]: [...values[id]] })
    this._updateFather({ childId: id })
    this.setState({ fields: [...fields] })
  }

  deleteField = index => {
    const { updateValues, id } = this.props
    const { fields } = this.state
    const deletingField = fields[index]
    fields.splice(index, 1)
    values[id].splice(index, 1)
    updateValues({ [id]: [...values[id]], [deletingField]: undefined })
    this._updateFather({ childId: id })
    this.setState({ fields: [...fields] })
  }

  showError = () => {
    const { updateShowError, id, refs } = this.props
    updateShowError({ [id]: true })
    this.state.fields.forEach(id => {
      if (refs[id] && refs[id].isField && refs[id].showError) refs[id].showError()
    })
  }

  constructor(props) {
    super(props)
    if (!Array.isArray(values[props.id])) {
      const fatherId = fatherFieldMap[props.id]
      fatherFormMap[props.id] = fatherFormMap[fatherId]
      const fatherValue = values[fatherId]
      const initialValue = fatherValue[props.accessor]
      values[props.id] = initialValue
      props.updateValues({ [props.id]: initialValue })
    }
    const childrenIds = this._initiateFields()
    this.state = { ownErrors: [], fields: childrenIds }
    this._checkId()
    props.updateRefs({ [props.id]: this })
    const errors = this._validate(props.values[props.id])
    props.updateErrors({ [props.id]: errors })
    if (props.setShowError) props.setShowError(this.showError)
    if (!Array.isArray(values[props.id])) {
      console.warn('Faltando values em campo de id: ' + props.id)
      this._invalid = true
      return
    }
    return
  }

  shouldComponentUpdate = (propsNext, stateNext) => {
    if (this._invalid) return false
    const { id } = this.props
    if (this.state.fields !== stateNext.fields) return true
    else if (this.props.errors[id] !== propsNext.errors[id]) return true
    else if (this.props.showError[id] !== propsNext.showError[id]) return true
    else if (this.props.values[id].length !== propsNext.values[id].length) return true
    return false
  }

  componentWillUnmount = () => {
    this.props.updateRefs({ [this.props.id]: null })
    this.props.updateValues({ [this.props.id]: undefined })
    if (this.props.setShowError) this.props.setShowError(() => console.error('CampoArray está desmontado.'))
  }

  _initiateFields = () => {
    const { id, updateValues } = this.props
    const formId = fatherFormMap[id]
    return values[id].map(value => {
      const childId = uid()
      fatherFieldMap[childId] = id
      fatherFormMap[childId] = formId
      formFieldMap[formId].push(childId)
      values[childId] = value
      updateValues({ [childId]: value })
      return childId
    })
  }

  _checkId = () => {
    const { id, refs, accessor } = this.props
    if (id && refs[id]) {
      console.warn(
        `Todos os campos devem ter um id único. Id duplicado: ${id};
        Accessor do campo problemático: ${accessor}`,
      )
    }
  }

  _getErrorMessage = error => {
    const { mensagensDeErro } = this.props
    if (mensagensDeErro && mensagensDeErro[error.code]) return mensagensDeErro[error.code]
    else if (error.message) return error.message
    else return null
  }

  _getParsedErrors = errors =>
    errors.map(error => ({
      code: error.code,
      message: this._getErrorMessage(error),
    }))

  _updateErrors = ownErrors => {
    const { updateErrors, id } = this.props
    updateErrors({ [id]: [...ownErrors] })
  }

  _validate = value => {
    const { validar } = this.props
    if (validar) {
      const validation = validar(value)
      return this._getParsedErrors(validation.erros)
    }
    return []
  }

  _updateFather = ({ ref, childId }) => {
    if (!ref) ref = this.props.refs[fatherFieldMap[this.props.id]]
    const { updateValues, refs } = this.props
    const { id } = ref.props
    const { accessor } = refs[childId].props
    values[id][accessor] = values[childId]
    const fatherId = fatherFieldMap[id]
    if (!ref) if (fatherId) this._updateFather({ ref: refs[fatherId], childId: id })
    if (Array.isArray(values[id])) updateValues({ [id]: [...values[id]] })
    else updateValues({ [id]: { ...values[id] } })
    if (ref.validate) ref.validate()
    else console.error('Tem que existir um método validate nessa ref.')
  }

  render() {
    if (this._invalid) return null
    let { label, helperText, showError, id, WrapperComponent, BodyComponent, errors, wrapperProps } = this.props
    const { required, sortable, SortableWrapperComponent, SortableFieldWrapperComponent } = this.props
    const { FieldComponent, getFieldProps } = this.props
    const { fields } = this.state
    if (label && required) label = label + '*'
    const campoArray = (
      <WrapperComponent {...wrapperProps}>
        <BodyComponent label={label} helperText={helperText} showError={showError[id]} errors={errors[id]}>
          {fields.map((fieldId, index) => {
            const field = (
              <FieldComponent
                sortable={sortable}
                {...getFieldProps(index, fieldId)}
                key={sortable ? null : fieldId}
                id={fieldId}
                accessor={index}
              />
            )
            if (sortable)
              return (
                <SortableFieldWrapperComponent index={index} key={fieldId}>
                  {field}
                </SortableFieldWrapperComponent>
              )
            return field
          })}
        </BodyComponent>
      </WrapperComponent>
    )
    if (sortable) {
      return (
        <SortableWrapperComponent useDragHandle onSortEnd={this.moveField}>
          {campoArray}
        </SortableWrapperComponent>
      )
    }
    return campoArray
  }
}
