import React from 'react'
import { SortableContainer, SortableElement } from 'react-sortable-hoc'

import { MultipleFieldBody } from '../MultipleFieldBody'

const Wrapper = props => <div {...props} />

export const defaultProps = {
  WrapperComponent: Wrapper,
  SortableWrapperComponent: SortableContainer(Wrapper),
  SortableFieldWrapperComponent: SortableElement(Wrapper),
  BodyComponent: MultipleFieldBody,
  getFieldProps: () => ({}),
}
