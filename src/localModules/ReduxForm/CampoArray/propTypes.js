import PropTypes from 'prop-types'

export const propTypes = {
  FieldComponent: PropTypes.any.isRequired,
  getFieldProps: PropTypes.func.isRequired,
  id: PropTypes.string.isRequired,
  accessor: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  label: PropTypes.node,
  helperText: PropTypes.string,
  validar: PropTypes.func,
  validadeWith: PropTypes.arrayOf(PropTypes.string),
  WrapperComponent: PropTypes.any,
  BodyComponent: PropTypes.any,
  required: PropTypes.bool,
  setShowError: PropTypes.func,
  wrapperProps: PropTypes.object,
  // redux state
  sortables: PropTypes.object.isRequired,
  values: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired,
  refs: PropTypes.object.isRequired,
  showError: PropTypes.object.isRequired,
  // redux actions
  updateRefs: PropTypes.func.isRequired,
  updateValues: PropTypes.func.isRequired,
  updateErrors: PropTypes.func.isRequired,
  updateShowError: PropTypes.func.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
