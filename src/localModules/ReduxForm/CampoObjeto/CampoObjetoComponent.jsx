import React, { Component } from 'react'

import { values } from '../values'
import { fatherFieldMap } from '../fatherFieldMap'
import { fatherFormMap } from '../fatherFormMap'
import { DragHandle } from '../DragHandle'

import { propTypes } from './propTypes'
import { defaultProps } from './defaultProps'

export class CampoObjetoComponent extends Component {
  static propTypes = propTypes
  static defaultProps = defaultProps
  shouldUpdate
  isField = true

  validate = () => {
    this.shouldUpdate = true
    const value = values[this.props.id]
    if (typeof value === 'object') {
      const errors = this._validate(values[this.props.id])
      if (Array.isArray(errors)) this._updateErrors(errors)
    }
  }

  showError = () => {
    const { updateShowError, id, refs } = this.props
    updateShowError({ [id]: true })
    this.childrenIds.forEach(id => {
      if (refs[id] && refs[id].isField && refs[id].showError) refs[id].showError()
    })
  }

  updateValues = newProps => {
    const { updateValues, id, refs, accessor } = this.props
    const fatherId = fatherFieldMap[id]
    if (refs[fatherId]) {
      refs[fatherId].updateValues({ [accessor]: newProps[id] })
      refs[fatherId].validate()
    }
    Object.assign(values, newProps)
    updateValues(newProps)
  }

  constructor(props) {
    super(props)
    this.state = { ownErrors: [] }
    this._checkId()
    this._updateFatherMap(this.childrenIds)
    props.updateRefs({ [props.id]: this })
    const errors = this._validate(props.values[props.id])
    props.updateErrors({ [props.id]: errors })
    if (props.setShowError) props.setShowError(this.showError)
    if (typeof values[props.id] !== 'object') {
      const fatherId = fatherFieldMap[props.id]
      fatherFormMap[props.id] = fatherFormMap[fatherId]
      const fatherValue = values[fatherId]
      const initialValue = fatherValue[props.accessor]
      values[props.id] = initialValue
      props.updateValues({ [props.id]: initialValue })
    }
    if (typeof values[props.id] !== 'object') {
      console.warn('Faltando values em campo de id: ' + props.id)
      this._invalid = true
      return
    }
    return
  }

  shouldComponentUpdate = propsNext => {
    if (this._invalid) return false
    const { id } = this.props
    if (this.props.errors[id] !== propsNext.errors[id]) return true
    else if (this.props.showError[id] !== propsNext.showError[id]) return true
    else if (this.props.label !== propsNext.label) return true
    else if (this.props.children !== propsNext.children) return true
    return false
  }

  componentWillUnmount = () => {
    this.props.deleteRefs(this.props.id)
    this.props.deleteValues(this.props.id)
    this.props.deleteErrors(this.props.id)
    this.props.deleteShowError(this.props.id)
    if (this.props.setShowError) this.props.setShowError(() => console.error('CampoObjeto está desmontado.'))
  }

  get children() {
    const { children } = this.props
    if (Array.isArray(children)) return children
    else return [children]
  }

  get childrenIds() {
    return this.children.map(children => children.props.id)
  }

  get childrenProps() {
    return this.children.map(children => children.props)
  }

  _checkId = () => {
    const { id, refs, accessor } = this.props
    if (id && refs[id]) {
      console.warn(
        `Todos os campos devem ter um id único. Possível id duplicado: ${id};
        Accessor do campo problemático: ${accessor}`,
      )
    }
  }

  _updateFatherMap = childrenIds => childrenIds.forEach(id => (fatherFieldMap[id] = this.props.id))

  _hasSomeChildrenValueChanged = (values, valuesPrev) => this.childrenIds.some(id => values[id] !== valuesPrev[id])

  _hasSomeChildrenErrorsChanged = (erros, errosPrev) => this.childrenIds.some(id => erros[id] !== errosPrev[id])

  _updateValue = () => {
    const { values, id } = this.props
    const value = {}
    this.childrenProps.forEach(childProps => {
      value[childProps.accessor] = values[childProps.id]
    })
    this.updateValues({ [id]: value })
    return value
  }

  _getErrorMessage = error => {
    const { mensagensDeErro } = this.props
    if (mensagensDeErro && mensagensDeErro[error.code]) return mensagensDeErro[error.code]
    else if (mensagensDeErro) return error.message
    else return null
  }

  _getParsedErrors = errors =>
    errors.map(error => ({
      code: error.code,
      message: this._getErrorMessage(error),
    }))

  _updateErrors = ownErrors => {
    const { updateErrors, id } = this.props
    updateErrors({ [id]: [...ownErrors] })
  }

  _validate = value => {
    const { validar } = this.props
    if (validar) {
      const validation = validar(value)
      return this._getParsedErrors(validation.erros)
    }
    return []
  }

  render() {
    if (this._invalid) return null
    let { label, helperText, children, showError, id, WrapperComponent, BodyComponent, errors, required } = this.props
    const { wrapperProps, accessor } = this.props
    if (label && required) label = label + '*'
    return (
      <WrapperComponent {...wrapperProps}>
        <BodyComponent
          id={id}
          accessor={accessor}
          isSortable={this.isSortable}
          DragHandleComponent={DragHandle}
          label={label}
          helperText={helperText}
          showError={showError[id]}
          errors={errors[id]}
          value={values[id]}
        >
          {children}
        </BodyComponent>
      </WrapperComponent>
    )
  }
}
