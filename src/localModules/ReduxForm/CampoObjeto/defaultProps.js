import React from 'react'

import { MultipleFieldBody } from '../MultipleFieldBody'

export const defaultProps = {
  WrapperComponent: props => <div {...props} />,
  BodyComponent: MultipleFieldBody,
}
