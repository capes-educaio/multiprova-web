import { bindActionCreators } from 'redux'
import { actions } from 'utils/actions'

export function mapStateToProps(state) {
  return {
    strings: state.strings,
    fields: state.SimpleReduxForm__fields,
    values: state.SimpleReduxForm__values,
    errors: state.SimpleReduxForm__errors,
  }
}

export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      updateValues: actions.update.SimpleReduxForm__values,
    },
    dispatch,
  )
}
