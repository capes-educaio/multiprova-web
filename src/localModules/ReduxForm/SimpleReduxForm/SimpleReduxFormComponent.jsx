import React, { Component } from 'react'

import { CampoObjeto } from '../CampoObjeto'
import { values } from '../values'
import { fatherFormMap } from '../fatherFormMap'
import { formFieldMap } from '../formFieldMap'

import { propTypes } from './propTypes'
import { defaultProps } from './defaultProps'

export class SimpleReduxFormComponent extends Component {
  static propTypes = propTypes
  static defaultProps = defaultProps
  showError

  constructor(props) {
    super(props)
    formFieldMap[props.id] = []
    this._setInitialValue()
    if (props.onRef) props.onRef(this)
  }

  componentDidUpdate = propsPrev => {
    const { errors, id, onValid, onChange } = this.props
    if (onChange && this.props.values[id] !== propsPrev.values[id]) onChange(values[id])
    if (onValid && errors !== propsPrev.errors) {
      const hasError = Boolean(errors[id] && errors[id].length)
      const someFieldHasError = this._someFieldHasError()
      const isValid = !hasError && !someFieldHasError
      onValid(isValid)
    }
  }

  componentWillUnmount = () => {
    if (this.props.onRef) this.props.onRef(() => console.error('Component SimpleReduxForm está desmontado'))
  }

  _someFieldHasError = () =>
    formFieldMap[this.props.id].some(fieldId => this.props.errors[fieldId] && this.props.errors[fieldId].length)

  _getChildren(children) {
    if (Array.isArray(children)) return children
    else return [children]
  }

  _setInitialValue = () => {
    const { valor, id, children } = this.props
    this._updateValues({ [id]: valor })
    this._setChidrenValue(this._getChildren(children), valor)
  }

  _setChidrenValue = (children, valorInicial) => {
    children.forEach(child => {
      formFieldMap[this.props.id].push(child.props.id)
      fatherFormMap[child.props.id] = this.props.id
      this._updateValues({ [child.props.id]: valorInicial[child.props.accessor] })
      if (child.type.hasFields) {
        this._setChidrenValue(this._getChildren(child.props.children), valorInicial[child.props.accessor])
      }
    })
  }

  _updateValues = newProps => {
    const { updateValues } = this.props
    updateValues(newProps)
    Object.assign(values, newProps)
  }

  render() {
    const { values, id, WrapperComponent, onSubmit, title } = this.props
    if (!values[id]) return null
    return (
      <form onSubmit={onSubmit} title={title}>
        <CampoObjeto
          setShowError={showError => {
            this.showError = showError
          }}
          accessor={id}
          WrapperComponent={WrapperComponent}
          {...this.props}
        />
      </form>
    )
  }
}
