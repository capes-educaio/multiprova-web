import PropTypes from 'prop-types'

export const propTypes = {
  onChange: PropTypes.func,
  valor: PropTypes.object.isRequired,
  id: PropTypes.string.isRequired,
  children: PropTypes.oneOfType([PropTypes.element, PropTypes.arrayOf(PropTypes.element)]).isRequired,
  accessor: PropTypes.string,
  onValid: PropTypes.func,
  onSubmit: PropTypes.func,
  mensagensDeErro: PropTypes.object,
  validar: PropTypes.func,
  onRef: PropTypes.func,
  WrapperComponent: PropTypes.any,
  wrapperProps: PropTypes.object,
  // redux state
  values: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired,
  // redux actions
  updateValues: PropTypes.func.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
