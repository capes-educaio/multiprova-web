import PropTypes from 'prop-types'

export const propTypes = {
  accessor: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  required: PropTypes.bool,
  mensagensDeErro: PropTypes.object,
  validar: PropTypes.func,
  validateWith: PropTypes.arrayOf(PropTypes.string),
  WrapperComponent: PropTypes.any,
  textFieldProps: PropTypes.object,
  onEnter: PropTypes.func,
  onFocus: PropTypes.func,
  onBlur: PropTypes.func,
  onKeyPress: PropTypes.func,
  label: PropTypes.string,
  wrapperProps: PropTypes.object,
  type: PropTypes.string,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
