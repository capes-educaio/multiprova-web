import React, { Component } from 'react'

import TextField from '@material-ui/core/TextField'
import InputAdornment from '@material-ui/core/InputAdornment'

import Visibility from '@material-ui/icons/Visibility'
import VisibilityOff from '@material-ui/icons/VisibilityOff'

import { SimpleField } from '../SimpleField'
import { DragHandle } from '../DragHandle'

import { propTypes } from './propTypes'
import { defaultProps } from './defaultProps'

export class CampoPasswordComponent extends Component {
  static propTypes = propTypes
  static defaultProps = defaultProps
  showError
  _simpleFieldRef

  state = {
    mostrarSenha: false,
  }

  _onChange = onChange => e => {
    const { value } = e.target
    onChange(value)
  }

  _handleOnKeyPress = e => {
    const { onKeyPress, onEnter } = this.props
    if (e.key === 'Enter' && onEnter) {
      e.preventDefault()
      onEnter()
    }
    if (onKeyPress) onKeyPress(e)
  }

  _toggleMostrarSenha = () => {
    this.setState({ mostrarSenha: !this.state.mostrarSenha }, () => {
      if (this._simpleFieldRef) this._simpleFieldRef.forceUpdate()
    })
  }

  render() {
    let { textFieldProps, id, label, helperText, validar, WrapperComponent, required, mensagensDeErro } = this.props
    const { classes, validateWith, wrapperProps, accessor, sortable } = this.props
    const { mostrarSenha } = this.state
    const passwordProps = {
      type: mostrarSenha ? 'text' : 'password',
      InputProps: {
        autoComplete: 'current-password',
        endAdornment: (
          <InputAdornment position="end" onClick={this._toggleMostrarSenha} className={classes.iconButton}>
            {mostrarSenha ? <VisibilityOff className={classes.icon} /> : <Visibility className={classes.icon} />}
          </InputAdornment>
        ),
      },
    }
    textFieldProps = { ...textFieldProps, ...passwordProps }
    return (
      <WrapperComponent className={classes.wrapper} {...wrapperProps}>
        {sortable && <DragHandle />}
        <SimpleField
          onRef={ref => (this._simpleFieldRef = ref)}
          id={id}
          accessor={accessor}
          label={label}
          validar={validar}
          validateWith={validateWith}
          required={required}
          emptyValue=""
          mensagensDeErro={mensagensDeErro}
          setShowError={showError => {
            this.showError = showError
          }}
        >
          {({ label, showError, errors, onFocus, onBlur, onChange, value, setInputRef }) => {
            const hasError = Array.isArray(errors) && errors.length > 0
            const message = showError && hasError ? errors[0].message : helperText
            return (
              <TextField
                id={id}
                {...textFieldProps}
                label={label}
                error={showError && hasError}
                helperText={message}
                onFocus={() => {
                  onFocus()
                  if (this.props.onFocus) this.props.onFocus()
                }}
                onBlur={() => {
                  onBlur()
                  if (this.props.onBlur) this.props.onBlur()
                }}
                onChange={this._onChange(onChange)}
                value={value}
                inputRef={setInputRef}
                onKeyPress={this._handleOnKeyPress}
              />
            )
          }}
        </SimpleField>
      </WrapperComponent>
    )
  }
}
