export const style = theme => ({
  wrapper: { margin: '0 0 5px 0' },
  icon: {
    height: 20,
  },
  iconButton: {
    color: 'rgba(0, 0, 0, 0.5)',
    '& :hover': {
      color: 'rgba(0, 0, 0, 1)',
    },
  },
})
