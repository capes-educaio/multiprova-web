import React, { Component } from 'react'

import { InputTag } from 'common/InputTag'

import { SimpleField } from '../SimpleField'

import { propTypes } from './propTypes'
import { defaultProps } from './defaultProps'

export class CampoTagComponent extends Component {
  static propTypes = propTypes
  static defaultProps = defaultProps
  showError

  render() {
    const { tagProps, id, label, validar, WrapperComponent, required, mensagensDeErro, classes } = this.props
    const { validateWith, wrapperProps, accessor } = this.props
    return (
      <WrapperComponent className={classes.wrapper} {...wrapperProps}>
        <SimpleField
          id={id}
          accessor={accessor}
          label={label}
          validar={validar}
          validateWith={validateWith}
          required={required}
          emptyValue=""
          mensagensDeErro={mensagensDeErro}
          setShowError={showError => {
            this.showError = showError
          }}
        >
          {({ label, showError, errors, onFocus, onBlur, onChange, isSortable, value, setInputRef }) => {
            const props = {
              ...tagProps,
              valor: value,
              onChange,
            }
            return <InputTag {...props} />
          }}
        </SimpleField>
      </WrapperComponent>
    )
  }
}
