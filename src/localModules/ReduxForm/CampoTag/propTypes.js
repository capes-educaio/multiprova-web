import PropTypes from 'prop-types'

export const propTypes = {
  accessor: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  required: PropTypes.bool,
  mensagensDeErro: PropTypes.object,
  validar: PropTypes.func,
  validateWith: PropTypes.arrayOf(PropTypes.string),
  WrapperComponent: PropTypes.any,
  tagProps: PropTypes.object,
  label: PropTypes.string,
  wrapperProps: PropTypes.object,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
