import React, { Component } from 'react'

import TextField from '@material-ui/core/TextField'

import { store } from 'utils/store'

import { SimpleField } from '../SimpleField'
import { DragHandle } from '../DragHandle'

import { propTypes } from './propTypes'
import { defaultProps } from './defaultProps'

export class CampoTextComponent extends Component {
  static propTypes = propTypes
  static defaultProps = defaultProps
  showError

  constructor(props) {
    super(props)
    this.state = {
      value: store.getState().SimpleReduxForm__values[props.id],
    }
  }

  _onChange = e => {
    this.setState({ value: e.target.value })
    if (this.props.onChange) this.props.onChange(e.target.value)
  }

  _setValue = value => this.setState({ value })

  _handleOnKeyPress = e => {
    const { onKeyPress, onEnter } = this.props
    if (e.key === 'Enter' && onEnter) {
      e.preventDefault()
      onEnter()
    }
    if (onKeyPress) onKeyPress(e)
  }

  render() {
    const { textFieldProps, id, label, helperText, validar, WrapperComponent, required, mensagensDeErro } = this.props
    const { classes, validateWith, wrapperProps, accessor, sortable } = this.props
    return (
      <WrapperComponent className={classes.wrapper} {...wrapperProps}>
        {sortable && <DragHandle />}
        <SimpleField
          id={id}
          accessor={accessor}
          label={label}
          validar={validar}
          validateWith={validateWith}
          required={required}
          emptyValue=""
          mensagensDeErro={mensagensDeErro}
          setShowError={showError => {
            this.showError = showError
          }}
        >
          {({ label, showError, errors, onFocus, onBlur, onChange, setInputRef, value }) => {
            const hasError = Array.isArray(errors) && errors.length > 0
            const message = showError && hasError ? errors[0].message : helperText
            return (
              <TextField
                id={id}
                {...textFieldProps}
                label={label}
                error={showError && hasError}
                helperText={message}
                onFocus={() => {
                  this._setValue(value)
                  onFocus()
                  if (this.props.onFocus) this.props.onFocus()
                }}
                onBlur={() => {
                  onChange(this.state.value)
                  this._setValue(null)
                  onBlur()
                  if (this.props.onBlur) this.props.onBlur()
                }}
                onChange={this._onChange}
                value={typeof this.state.value === 'string' ? this.state.value : value}
                inputRef={setInputRef}
                onKeyPress={this._handleOnKeyPress}
              />
            )
          }}
        </SimpleField>
      </WrapperComponent>
    )
  }
}
