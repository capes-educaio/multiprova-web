import React, { Component } from 'react'
import uid from 'uuid/v4'

import Select from '@material-ui/core/Select'
import FormControl from '@material-ui/core/FormControl'
import InputLabel from '@material-ui/core/InputLabel'
import FormHelperText from '@material-ui/core/FormHelperText'

import { SimpleField } from '../SimpleField'

import { propTypes } from './propTypes'
import { defaultProps } from './defaultProps'

export class CampoSelectComponent extends Component {
  static propTypes = propTypes
  static defaultProps = defaultProps
  showError
  _inputKey = uid()

  render() {
    const { selectProps, id, label, helperText, validar, WrapperComponent, required, mensagensDeErro } = this.props
    const { validateWith, classes, children, wrapperProps, accessor } = this.props
    return (
      <WrapperComponent className={classes.wrapper} {...wrapperProps}>
        <SimpleField
          id={id}
          accessor={accessor}
          label={label}
          validar={validar}
          validateWith={validateWith}
          required={required}
          emptyValue=""
          mensagensDeErro={mensagensDeErro}
          setShowError={showError => {
            this.showError = showError
          }}
        >
          {({ label, showError, errors, onFocus, onBlur, onChange, isSortable, value, setInputRef }) => {
            const hasError = Array.isArray(errors) && errors.length > 0
            const message = showError && hasError ? errors[0].message : helperText
            const props = {
              key: this._inputKey,
              ...selectProps,
              value,
              onChange: event => onChange(event.target.value),
              onFocus,
              onBlur,
              error: showError && hasError,
            }
            return (
              <FormControl className={classes.field}>
                {helperText && <FormHelperText>{helperText}</FormHelperText>}
                {label && <InputLabel>{label}</InputLabel>}
                <Select {...props}>{children}</Select>
                {showError && hasError && message && <FormHelperText error>{message}</FormHelperText>}
              </FormControl>
            )
          }}
        </SimpleField>
      </WrapperComponent>
    )
  }
}
