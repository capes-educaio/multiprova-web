import PropTypes from 'prop-types'

const { oneOfType, element, arrayOf } = PropTypes

export const propTypes = {
  accessor: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  children: oneOfType([element, arrayOf(element)]).isRequired,
  required: PropTypes.bool,
  mensagensDeErro: PropTypes.object,
  validar: PropTypes.func,
  validateWith: PropTypes.arrayOf(PropTypes.string),
  WrapperComponent: PropTypes.any,
  selectProps: PropTypes.object,
  onEnter: PropTypes.func,
  onKeyPress: PropTypes.func,
  label: PropTypes.string,
  wrapperProps: PropTypes.object,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
