import React, { Component } from 'react'

import Typography from '@material-ui/core/Typography'

import { RichTextEditor } from 'common/RichTextEditor'

import { SimpleField } from '../SimpleField'
import { DragHandle } from '../DragHandle'

import { propTypes } from './propTypes'
import { defaultProps } from './defaultProps'

export class CampoRichTextComponent extends Component {
  static propTypes = propTypes
  static defaultProps = defaultProps
  showError

  render() {
    const { inputProps, id, label, helperText, validar, WrapperComponent, required, mensagensDeErro } = this.props
    const { classes, validateWith, wrapperProps, accessor, sortable } = this.props
    return (
      <WrapperComponent className={classes.wrapper} {...wrapperProps}>
        {sortable && <DragHandle className={classes.dragHandle} />}
        {sortable && <Typography className={classes.label}>{label}</Typography>}
        <SimpleField
          id={id}
          accessor={accessor}
          label={label}
          validar={validar}
          validateWith={validateWith}
          required={required}
          emptyValue=""
          mensagensDeErro={mensagensDeErro}
          setShowError={showError => {
            this.showError = showError
          }}
        >
          {({ label, showError, errors, onFocus, onBlur, onChange, value, setInputRef }) => {
            const hasError = Array.isArray(errors) && errors.length > 0
            const message = showError && hasError ? errors[0].message : helperText
            return (
              <RichTextEditor
                {...inputProps}
                label={sortable ? null : label}
                error={showError && hasError}
                helperText={message}
                onFocus={() => {
                  onFocus()
                  if (this.props.onFocus) this.props.onFocus()
                }}
                onBlur={() => {
                  onBlur()
                  if (this.props.onBlur) this.props.onBlur()
                }}
                onChange={onChange}
                default={value}
                inputRef={setInputRef}
              />
            )
          }}
        </SimpleField>
      </WrapperComponent>
    )
  }
}
