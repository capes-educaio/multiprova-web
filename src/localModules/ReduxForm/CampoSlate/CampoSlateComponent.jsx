import React, { Component } from 'react'
import Typography from '@material-ui/core/Typography'

import { MpEditor } from 'common/MpEditor'

import { SimpleField } from '../SimpleField'
import { DragHandle } from '../DragHandle'

import { propTypes } from './propTypes'
import { defaultProps } from './defaultProps'

const INITIAL_VALUE = '<p></p>'

export class CampoSlateComponent extends Component {
  static propTypes = propTypes
  static defaultProps = defaultProps
  showError

  getClasses = classesInputProps => {
    const { classes } = this.props

    if (!classesInputProps) return { editor: classes.editorComperve }
    else {
      return {
        ...classesInputProps,
        root: classesInputProps.root && classesInputProps.root,
        editor: classesInputProps.editor ? classesInputProps.editor : classes.editorComperve,
      }
    }
  }

  render() {
    const { inputProps, id, helperText, validar, WrapperComponent, required, mensagensDeErro } = this.props
    const { classes, validateWith, wrapperProps, accessor, sortable } = this.props
    return (
      <WrapperComponent className={classes.wrapper} {...wrapperProps}>
        {sortable && <DragHandle className={classes.dragHandle} />}
        <SimpleField
          id={id}
          accessor={accessor}
          validar={validar}
          validateWith={validateWith}
          required={required}
          emptyValue={INITIAL_VALUE}
          mensagensDeErro={mensagensDeErro}
          setShowError={showError => {
            this.showError = showError
          }}
        >
          {({ label, showError, errors, onFocus, onBlur, onChange, value, setInputRef }) => {
            const hasError = Array.isArray(errors) && errors.length > 0
            const message = showError && hasError ? errors[0].message : helperText
            const props = {
              ...inputProps,
              html: value,
              onChange,
              onFocus: () => {
                onFocus()
                if (this.props.onFocus) this.props.onFocus()
              },
              onBlur: () => {
                onBlur()
                if (this.props.onBlur) this.props.onBlur()
              },
              helperText: message,
              error: hasError,
              required,
            }
            return (
              <Typography headlineMapping={{ body1: 'div' }} variant="body1">
                <MpEditor {...props} />
              </Typography>
            )
          }}
        </SimpleField>
      </WrapperComponent>
    )
  }
}
