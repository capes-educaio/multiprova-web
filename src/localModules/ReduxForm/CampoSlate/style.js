export const style = theme => ({
  wrapper: { margin: '0 0 5px 0' },
  dragHandle: {
    margin: '13px 10px 0 0',
  },
  label: {
    margin: '13px 10px 0 0',
  },
  editorComperve: {
    fontFamily: 'Arial, Helvetica, sans-serif',
    fontSize: '10pt',
    letterSpacing: '0.7px',
    fontWeight: '500',
  },
})
