export const style = theme => ({
  root: {
    '&:hover': {
      cursor: 'grab',
    },
  },
  icon: { margin: '0 10px' },
})
