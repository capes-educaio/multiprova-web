import { DragHandleComponent } from './DragHandleComponent'
import { compose } from 'redux'
import { connect } from 'react-redux'
// import { withRouter } from 'react-router'
import { withStyles } from '@material-ui/core/styles'

import { SortableHandle } from 'react-sortable-hoc'

import { mapStateToProps, mapDispatchToProps } from './redux'
import { style } from './style'

export const DragHandle = compose(
  withStyles(style),
  SortableHandle,
  // withRouter,
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(DragHandleComponent)
