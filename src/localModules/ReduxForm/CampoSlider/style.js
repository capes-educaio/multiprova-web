export const style = theme => ({
  form: {},
  wrapper: { margin: '15px 0 10px 0' },
  sliderContainer: {
    display: 'flex',
    alignItems: 'center',
  },
  slider: {
    width: '100%',
    margin: '0 15px 0 5px',
  },
  label: {
    fontSize: '0.75em',
  },
})
