import React, { Component } from 'react'

import Slider from '@material-ui/lab/Slider'
import FormControl from '@material-ui/core/FormControl'
import FormHelperText from '@material-ui/core/FormHelperText'
import FormLabel from '@material-ui/core/FormLabel'
import Typography from '@material-ui/core/Typography'

import { SimpleField } from '../SimpleField'

import { propTypes } from './propTypes'
import { defaultProps } from './defaultProps'

export class CampoSliderComponent extends Component {
  static propTypes = propTypes
  static defaultProps = defaultProps
  showError

  render() {
    const { sliderProps, id, label, helperText, validar, WrapperComponent, required, mensagensDeErro } = this.props
    const { classes, validateWith, wrapperProps, accessor } = this.props
    return (
      <WrapperComponent className={classes.wrapper} {...wrapperProps}>
        <SimpleField
          id={id}
          accessor={accessor}
          label={label}
          validar={validar}
          validateWith={validateWith}
          required={required}
          emptyValue=""
          mensagensDeErro={mensagensDeErro}
          setShowError={showError => {
            this.showError = showError
          }}
        >
          {({ label, showError, errors, onFocus, onBlur, onChange, isSortable, value, setInputRef }) => {
            const hasError = Array.isArray(errors) && errors.length > 0
            const message = showError && hasError ? errors[0].message : helperText
            const { caption, max, min, step } = sliderProps
            const props = {
              max,
              min,
              step,
              value,
              onChange: (e, novoValor) => {
                if (value !== novoValor) onChange(novoValor)
              },
            }
            const captionValue = caption ? caption(value) : null
            return (
              <FormControl className={classes.form}>
                {label && <FormLabel component="legend" className={classes.label}>{label}</FormLabel>}
                <div className={classes.sliderContainer}>
                  <div className={classes.slider}>
                    <Slider {...props} />
                  </div>
                  {captionValue && <Typography variant="caption">{captionValue}</Typography>}
                </div>
                {showError && hasError && message && <FormHelperText error>{message}</FormHelperText>}
              </FormControl>
            )
          }}
        </SimpleField>
      </WrapperComponent>
    )
  }
}
