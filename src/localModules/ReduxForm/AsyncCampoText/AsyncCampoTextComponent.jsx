import React, { Component } from 'react'

import TextField from '@material-ui/core/TextField'

import { AsyncSimpleField } from '../AsyncSimpleField'

import { propTypes } from './propTypes'
import { defaultProps } from './defaultProps'
import { ValidarUrlAdornment } from './ValidarUrlAdornment'

export class AsyncCampoTextComponent extends Component {
  static propTypes = propTypes
  static defaultProps = defaultProps
  showError

  constructor(props) {
    super(props)
    this.state = {
      value: null,
    }
  }

  _onChange = onChange => e => {
    const { value } = e.target
    this.setState({ value })
    onChange(value)
  }

  render() {
    const { textFieldProps, id, label, helperText, validar, WrapperComponent, required, mensagensDeErro } = this.props
    const { classes, validadeWith, wrapperProps, accessor } = this.props
    const emptyValue = ''
    return (
      <WrapperComponent className={classes.wrapper} {...wrapperProps}>
        <AsyncSimpleField
          id={id}
          accessor={accessor}
          label={label}
          validar={validar}
          validadeWith={validadeWith}
          required={required}
          emptyValue={emptyValue}
          mensagensDeErro={mensagensDeErro}
          setShowError={showError => {
            this.showError = showError
          }}
        >
          {({ label, showError, errors, onFocus, onBlur, onChange, isSortable, value, loading, initialValue }) => {
            const hasError = Array.isArray(errors) && errors.length > 0
            const message = showError && hasError ? errors[0].message : helperText
            return (
              <TextField
                {...textFieldProps}
                label={label}
                error={showError && hasError}
                helperText={message}
                onFocus={onFocus}
                onBlur={onBlur}
                onChange={this._onChange(onChange)}
                value={value}
                InputProps={{
                  endAdornment: (
                    <ValidarUrlAdornment estaCarregando={loading} isValid={!hasError} showError={showError} />
                  ),
                }}
              />
            )
          }}
        </AsyncSimpleField>
      </WrapperComponent>
    )
  }
}
