import PropTypes from 'prop-types'

export const propTypes = {
  // padrões
  accessor: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  required: PropTypes.bool,
  mensagensDeErro: PropTypes.object,
  validar: PropTypes.func,
  validadeWith: PropTypes.arrayOf(PropTypes.string),
  CampoWrapper: PropTypes.any,
  textFieldProps: PropTypes.object,
  wrapperProps: PropTypes.object,
  // redux state
  values: PropTypes.object.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
