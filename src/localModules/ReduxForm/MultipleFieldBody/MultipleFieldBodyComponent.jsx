import React, { Component } from 'react'
import uid from 'uuid/v4'

import FormHelperText from '@material-ui/core/FormHelperText'
import FormLabel from '@material-ui/core/FormLabel'

import { DragHandle } from 'common/DragHandle'

import { propTypes } from './propTypes'

export class MultipleFieldBodyComponent extends Component {
  static propTypes = propTypes
  _id = uid()

  render() {
    const { sortable, label, helperText, showError, errors, classes, children } = this.props
    const elements = []
    if (sortable && !label) elements.push(<DragHandle key={'handle' + this._id} />)
    if (!sortable && label)
      elements.push(
        <FormLabel className={classes.label} key={'label' + this._id} focused>
          {label}
        </FormLabel>,
      )
    if (sortable && label)
      elements.push(
        <FormLabel className={classes.label} key={'labelHandle' + this._id} focused>
          <DragHandle /> {label}
        </FormLabel>,
      )
    if (helperText) elements.push(<FormHelperText key={'helperText' + this._id}>{helperText}</FormHelperText>)
    if (showError && errors[0] && errors[0].message)
      elements.push(
        <FormHelperText key={'error' + this._id}>
          {helperText} error>{errors[0].message}
        </FormHelperText>,
      )
    if (label) elements.push(<div key={'labelDiv' + this._id} className={classes.labelDiv} />)
    return [...elements, children]
  }
}
