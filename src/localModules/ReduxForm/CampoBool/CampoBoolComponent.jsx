import React, { Component } from 'react'

import Switch from '@material-ui/core/Switch'
import Checkbox from '@material-ui/core/Checkbox'
import FormControlLabel from '@material-ui/core/FormControlLabel'

import { SimpleField } from '../SimpleField'

import { propTypes } from './propTypes'
import { defaultProps } from './defaultProps'

export class CampoBoolComponent extends Component {
  static propTypes = propTypes
  static defaultProps = defaultProps
  showError

  render() {
    const { boolProps, tipo, id, label, validar, WrapperComponent, required, mensagensDeErro } = this.props
    const { classes, validateWith, wrapperProps, accessor } = this.props
    return (
      <WrapperComponent className={classes.wrapper} {...wrapperProps}>
        <SimpleField
          id={id}
          accessor={accessor}
          label={label}
          validar={validar}
          validateWith={validateWith}
          required={required}
          emptyValue=""
          mensagensDeErro={mensagensDeErro}
          setShowError={showError => {
            this.showError = showError
          }}
        >
          {({ label, showError, errors, onFocus, onBlur, onChange, isSortable, value, setInputRef }) => {
            const props = {
              ...boolProps,
              checked: value,
              onChange: (e, checked) => onChange(checked),
            }
            return (
              <FormControlLabel
                control={tipo === 'switch' ? <Switch {...props} /> : <Checkbox {...props} />}
                label={label}
              />
            )
          }}
        </SimpleField>
      </WrapperComponent>
    )
  }
}
