import { bindActionCreators } from 'redux'
import { actions } from 'utils/actions'

export function mapStateToProps(state) {
  return {
    strings: state.strings,
    sortables: state.SimpleReduxForm__sortables,
    values: state.SimpleReduxForm__values,
    errors: state.SimpleReduxForm__errors,
    refs: state.SimpleReduxForm__refs,
    showError: state.SimpleReduxForm__showError,
  }
}

export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      updateRefs: actions.update.SimpleReduxForm__refs,
      updateValues: actions.update.SimpleReduxForm__values,
      updateErrors: actions.update.SimpleReduxForm__errors,
      updateShowError: actions.update.SimpleReduxForm__showError,
      deleteRefs: actions.deleteProp.SimpleReduxForm__refs,
      deleteValues: actions.deleteProp.SimpleReduxForm__values,
      deleteErrors: actions.deleteProp.SimpleReduxForm__errors,
      deleteShowError: actions.deleteProp.SimpleReduxForm__showError,
    },
    dispatch,
  )
}
