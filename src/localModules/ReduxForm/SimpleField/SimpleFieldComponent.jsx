import { Component } from 'react'

import { values } from '../values'
import { fatherFieldMap } from '../fatherFieldMap'
import { fatherFormMap } from '../fatherFormMap'

import { propTypes } from './propTypes'

export class SimpleFieldComponent extends Component {
  static propTypes = propTypes
  isField = true
  _inputRef
  _invalid

  focus = () => {
    if (this._inputRef && this._inputRef.focus) this._inputRef.focus()
  }

  showError = () => {
    const { updateShowError, id } = this.props
    updateShowError({ [id]: true })
  }

  hideError = () => {
    const { updateShowError, id } = this.props
    updateShowError({ [id]: false })
  }

  constructor(props) {
    super(props)
    if (typeof values[props.id] === 'undefined') {
      const fatherId = fatherFieldMap[props.id]
      fatherFormMap[props.id] = fatherFormMap[fatherId]
      const fatherValue = values[fatherId]
      const initialValue = fatherValue[props.accessor]
      values[props.id] = initialValue
      props.updateValues({ [props.id]: initialValue })
    }
    this.state = { ownErrors: [] }
    this._checkId()
    props.updateRefs({ [props.id]: this })
    this._validate(values[props.id])
    props.setShowError(this.showError)
    if (props.onRef) props.onRef(this)
    if (typeof values[props.id] === 'undefined') {
      console.warn('Faltando values em campo de id: ' + props.id)
      this._invalid = true
      return
    }
    return
  }

  componentDidUpdate = propsPrev => {
    const { values, validateWith } = this.props
    let shouldValidade = false
    if (Array.isArray(validateWith)) {
      shouldValidade = validateWith.some(id => values[id] !== propsPrev.values[id])
    }
    if (shouldValidade) this._validate(values[this.props.id])
  }

  shouldComponentUpdate = next => {
    if (this._invalid) return false
    const { values, errors, showError, validateWith, label, children } = this.props
    const { id } = next
    if (values[id] !== next.values[id]) return true
    else if (errors[id] !== next.errors[id]) return true
    else if (showError[id] !== next.showError[id]) return true
    else if (children !== next.children) return true
    else if (label !== next.label) return true
    else if (Array.isArray(validateWith)) {
      return validateWith.some(id => values[id] !== next.values[id])
    }
    return false
  }

  componentWillUnmount = () => {
    this.props.deleteRefs(this.props.id)
    this.props.deleteValues(this.props.id)
    this.props.deleteErrors(this.props.id)
    this.props.deleteShowError(this.props.id)
    this.props.setShowError(() => console.error('SimpleField não está montado.'))
    if (this.props.onRef) this.props.onRef(null)
  }

  _setInputRef = inputRef => (this._inputRef = inputRef)

  _checkId = () => {
    const { id, refs, accessor } = this.props
    if (id && refs[id]) {
      console.warn(
        `Todos os campos devem ter um id único. Possível id duplicado: ${id};
        Accessor do campo problemático: ${accessor}`,
      )
    }
  }

  _validate = value => {
    const { validar, updateErrors, id, required, emptyValue, strings, mensagensDeErro } = this.props
    let ownErrors = []
    const requiredNotMet = required && (value === null || value === undefined || value === emptyValue)
    if (requiredNotMet) {
      ownErrors.push({
        code: 'campo_obrigatorio',
        message: mensagensDeErro['campo_obrigatorio'] ? mensagensDeErro['campo_obrigatorio'] : strings.campoObrigatorio,
      })
    }
    if (validar) {
      const validation = validar(value)
      validation.erros.forEach(erro => {
        if (mensagensDeErro[erro.code]) erro.message = mensagensDeErro[erro.code]
      })
      ownErrors = [...ownErrors, ...validation.erros]
    }
    updateErrors({ [id]: ownErrors })
  }

  _onChange = newValue => {
    this._updateValues(newValue)
    this._validate(newValue)
  }

  _updateValues = newValue => {
    const { updateValues, id, refs } = this.props
    const fatherId = fatherFieldMap[id]
    values[id] = newValue
    if (fatherId) this._updateFather({ ref: refs[fatherId], childId: id })
    updateValues({ [id]: values[id] })
  }

  _updateFather = ({ ref, childId }) => {
    const { updateValues, refs } = this.props
    const { id } = ref.props
    const { accessor } = refs[childId].props
    if (values[id]) values[id][accessor] = values[childId]
    const fatherId = fatherFieldMap[id]
    if (fatherId) this._updateFather({ ref: refs[fatherId], childId: id })
    if (Array.isArray(values[id])) updateValues({ [id]: [...values[id]] })
    else updateValues({ [id]: { ...values[id] } })
    if (ref.validate) ref.validate()
    else console.error('Tem que existir um método validate nessa ref.')
  }

  render() {
    if (this._invalid) return null
    let { label, showError, id, children, errors, required, sortable } = this.props
    if (values[id] === undefined) {
      console.error(`Campo de id ${id} está com valor undefined.`)
      return null
    }
    if (label && required) label = label + '*'
    return children({
      label,
      showError: showError[id],
      errors: errors[id],
      onFocus: this.hideError,
      onBlur: this.showError,
      onChange: this._onChange,
      sortable: sortable,
      value: values[id],
      setInputRef: this._setInputRef,
    })
  }
}
