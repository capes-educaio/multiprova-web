import { INITIAL_VALUE } from './utils'

const emptyFunc = () => {}

export const defaultProps = {
  html: INITIAL_VALUE,
  rows: 1,
  floatingToolbar: false,
  justifyDefault: true,
  spellCheck: true,
  readOnly: false,
  adornment: { align: 'right', onEvent: false },
  placeholder: '',
  autoFocus: false,
  onChange: emptyFunc,
  onFocus: emptyFunc,
  onBlur: emptyFunc,
  hidden: [],
  upload: false,
}
