import React from 'react'
import Button from '@material-ui/core/Button'
import Tooltip from '@material-ui/core/Tooltip'

import { propTypes } from './propTypes'

export const ButtonComponent = ({ children, classes, active, onClick, disabled, tooltip }) => {
  const style = active ? { backgroundColor: 'rgba(0, 0, 0, 0.1)' } : {}
  return (
    <Tooltip title={tooltip}>
      <Button style={style} classes={{ root: classes.root }} onMouseDown={onClick} disabled={disabled}>
        {children}
      </Button>
    </Tooltip>
  )
}

ButtonComponent.propTypes = propTypes

ButtonComponent.defaultProps = {
  disabled: false,
  active: false,
}
