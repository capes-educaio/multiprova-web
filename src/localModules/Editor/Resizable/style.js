import { FOCUS_COLOR } from '../utils'

export const style = () => ({
  root: {
    '&.focused': {
      outline: `2px solid ${FOCUS_COLOR}`,
      '& > $rect': {
        opacity: 1,
      },
    },
  },
  rect: {
    position: 'absolute',
    opacity: 0,
    width: 7,
    height: 7,
    backgroundColor: FOCUS_COLOR,
  },
  rect1: {
    top: -4,
    left: -4,
  },
  rect2: {
    top: -4,
    right: -4,
  },
  rect3: {
    bottom: -4,
    left: -4,
  },
  rect4: {
    bottom: -4,
    right: -4,
  },
  dimensions: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    background: FOCUS_COLOR,
    color: 'white',
    fontSize: '8pt',
    borderRadius: 3,
    padding: 2,
    whiteSpace: 'nowrap',
    userSelect: 'none',
  },
})
