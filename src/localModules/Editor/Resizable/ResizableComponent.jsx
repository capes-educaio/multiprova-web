import React, { Component } from 'react'
import Resizable from 're-resizable'
import classnames from 'classnames'
import { ResizeListener } from 'react-resize-listener'

import Fade from '@material-ui/core/Fade'

import { propTypes } from './propTypes'

export class ResizableComponent extends Component {
  static propTypes = propTypes
  static defaultProps = {
    dimensions: false,
    lockAspectRatio: true,
    adaptive: true,
    autoResize: true,
    minHeight: 10,
    minWidth: 10,
    depth: 1,
  }

  ref = React.createRef()
  timeoutWindowResize
  timeoutDimensions
  timeoutAutoResize
  parentBounds
  isUpdating = false
  isResizing = false

  constructor(props) {
    super(props)
    this.state = {
      maxWidth: null,
      size: props.size,
      showDimensions: false,
    }
  }

  componentDidMount() {
    const { dimensions, adaptive, depth } = this.props
    if (adaptive) {
      this.parentBounds = this.parent(this.ref.current.resizable, depth)
      this.setDimensions()
      window.addEventListener('resize', this.onWindowResize)
    }
    if (dimensions) this.showDimensions()
    else this.disableContentEditableResizable()
  }

  componentWillUnmount() {
    clearTimeout(this.timeoutWindowResize)
    clearTimeout(this.timeoutDimensions)
    clearTimeout(this.timeoutAutoResize)
    if (this.props.adaptive) {
      window.removeEventListener('resize', this.onWindowResize)
    }
  }

  componentDidUpdate(prevProps) {
    const { isFocused, dimensions, adaptive, depth, size } = this.props
    const { width, height } = size
    if (width !== prevProps.size.width || height !== prevProps.size.height) this.isUpdating = false
    if (depth !== prevProps.depth) this.parentBounds = this.parent(this.ref.current.resizable, depth)
    if (adaptive && !this.isUpdating) this.setDimensions()
    if (dimensions && isFocused && !prevProps.isFocused) this.showDimensions()
    this.disableContentEditableResizable()
  }

  parent = (element, depth) => {
    if (depth) return this.parent(element.parentElement, depth - 1)
    else return element
  }

  setDimensions = () => {
    const { size, dimensions } = this.props
    const { width } = size
    const maxWidth = this.parentBounds.clientWidth
    if (width > maxWidth) {
      const newSize = { width: maxWidth, height: this.heightRatio(maxWidth) }
      this.setState({ size: newSize })
      if (dimensions) this.showDimensions()
      this.updateSize(newSize)
    }
    if (this.state.maxWidth !== maxWidth) this.setState({ maxWidth })
  }

  updateSize = size => {
    const { width, height } = this.props.size
    if (size.width !== width || size.height !== height) {
      this.props.onResize(size)
      this.isUpdating = true
    }
  }

  onResizeListener = () => {
    const { autoResize, lockAspectRatio, size } = this.props
    if (!autoResize || this.isResizing) return
    const { offsetWidth, offsetHeight } = this.ref.current.resizable
    const { width, height } = size
    if (offsetWidth !== width || offsetHeight !== height) {
      clearTimeout(this.timeoutAutoResize)
      this.timeoutAutoResize = setTimeout(() => {
        const newSize = { width: offsetWidth, height: lockAspectRatio ? this.heightRatio(offsetWidth) : offsetHeight }
        if (!this.isResizing) this.setState({ size: newSize })
        this.updateSize(newSize)
      }, 500)
    }
  }

  disableContentEditableResizable = () => {
    const contents = [...this.ref.current.resizable.lastElementChild.children]
    contents.forEach(content => {
      content.contentEditable = false
    })
  }

  onWindowResize = () => {
    clearTimeout(this.timeoutWindowResize)
    this.timeoutWindowResize = setTimeout(this.setDimensions, 200)
  }

  onResizeStop = (e, direction, ref, d) => {
    const { width, height } = this.props.size
    this.updateSize({
      width: width + d.width,
      height: height + d.height,
    })
    this.isResizing = false
  }

  onResize = (e, direction, ref, d) => {
    const { width, height } = this.props.size
    this.setState({
      size: {
        width: width + d.width,
        height: height + d.height,
      },
    })
    this.showDimensions()
  }

  onResizeStart = () => {
    this.isResizing = true
    const { onResizeStart } = this.props
    if (onResizeStart) onResizeStart()
  }

  showDimensions = () => {
    if (!this.state.showDimensions) this.setState({ showDimensions: true })
    clearTimeout(this.timeoutDimensions)
    this.timeoutDimensions = setTimeout(() => {
      this.setState({ showDimensions: false })
    }, 2000)
  }

  heightRatio(newWidth) {
    const { width, height } = this.props.size
    const factor = width / height
    return Math.round(newWidth / factor)
  }

  get enable() {
    const { isFocused, lockAspectRatio } = this.props
    return {
      top: isFocused && !lockAspectRatio,
      right: isFocused && !lockAspectRatio,
      bottom: isFocused && !lockAspectRatio,
      left: isFocused && !lockAspectRatio,
      topRight: isFocused,
      bottomRight: isFocused,
      bottomLeft: isFocused,
      topLeft: isFocused,
    }
  }

  get resizableProps() {
    const { size, dimensions, lockAspectRatio, adaptive, minHeight, minWidth } = this.props
    const { maxWidth } = this.state
    const props = {
      size,
      enable: this.enable,
      onResizeStop: this.onResizeStop,
      onResizeStart: this.onResizeStart,
      lockAspectRatio,
      minHeight,
      minWidth,
    }
    if (adaptive && maxWidth !== null) props.maxWidth = maxWidth
    if (dimensions) props.onResize = this.onResize
    return props
  }

  render() {
    const { isFocused, dimensions, lockAspectRatio, classes, children } = this.props
    const { showDimensions } = this.state
    const { width, height } = this.state.size
    return (
      <Resizable ref={this.ref} className={classnames(classes.root, { focused: isFocused })} {...this.resizableProps}>
        {children}
        {lockAspectRatio &&
          [1, 2, 3, 4].map(number => (
            <div key={number} className={classnames(classes.rect, classes[`rect${number}`])} />
          ))}
        {dimensions && (
          <Fade
            in={showDimensions}
            onEntered={this.setTimeoutDimensions}
            timeout={{
              enter: 300,
              exit: 300,
            }}
          >
            <span className={classes.dimensions}>
              {width} x {height}
            </span>
          </Fade>
        )}
        <ResizeListener onResize={this.onResizeListener} />
      </Resizable>
    )
  }
}
