import React, { Component } from 'react'
import { Editor } from 'slate-react'
import classnames from 'classnames'

import Typography from '@material-ui/core/Typography'
import Collapse from '@material-ui/core/Collapse'

import { Toolbar } from './Toolbar'
import { serializer } from './serializer'
import { propTypes } from './propTypes'
import { INITIAL_VALUE, TAB } from './utils'
import * as plugins from './plugins'
import { defaultProps } from './defaultProps'

export class EditorComponent extends Component {
  static propTypes = propTypes
  static defaultProps = defaultProps

  ref = React.createRef()
  editor
  serialized
  deserialized
  lastArrowKey

  constructor(props) {
    super(props)
    const { html } = props
    this.serialized = html
    this.deserialized = serializer.deserialize(html)
    this.state = {
      value: this.deserialized,
      showAdornment: false,
      showLabel: !this.isEmpty,
      focused: false,
    }
  }

  componentDidMount() {
    const { readOnly, adornment } = this.props
    if (readOnly) return
    if (adornment.onEvent) this.addAdornmentListeners()
  }

  componentDidUpdate(prevProps) {
    if (this.props.placeholder !== prevProps.placeholder && this.serialized === INITIAL_VALUE) {
      setTimeout(this.updatePlaceholder, 500)
    }
  }

  onRef = editor => {
    if (!editor) return
    editor.moveToEndOfDocument()
    this.editor = editor
  }

  updatePlaceholder = () => {
    this.editor.insertText(' ')
    this.editor.deleteBackward(1)
  }

  addAdornmentListeners = () => {
    const { onEvent } = this.props.adornment
    if (onEvent === 'hover') {
      this.ref.current.addEventListener('mouseenter', () => {
        this.setState({ showAdornment: true })
      })
      this.ref.current.addEventListener('mouseleave', () => {
        this.setState({ showAdornment: false })
      })
    }
  }

  onChange = ({ value }) => {
    this.setState({ value })
    const { focused } = this.state
    if (value.selection.isFocused !== focused) {
      if (focused) this.onBlur()
      else this.onFocus()
      this.setState({ focused: !focused })
    }
    this.handleChange(value)
  }

  handleChange = ({ anchorInline }) => {
    if (anchorInline && anchorInline.type === TAB) {
      switch (this.lastArrowKey) {
        case 'left':
          this.editor.moveBackward(1)
          break
        case 'right':
          this.editor.moveForward(1)
          break
        default:
          this.editor.moveForward(1)
      }
    }
  }

  onKeyDown = event => {
    if (event.key === 'ArrowLeft') this.lastArrowKey = 'left'
    else if (event.key === 'ArrowRight') this.lastArrowKey = 'right'
  }

  onBlur = () => {
    this.serialized = serializer.serialize(this.state.value)
    this.props.onChange(this.serialized)
    this.setState({ focused: false, showLabel: !this.isEmpty })
    this.props.onBlur()
  }

  onFocus = () => {
    this.setState({ showLabel: true, focused: true })
    this.props.onFocus()
  }

  upload = async (file, node) => {
    try {
      const { upload } = this.props
      if (!upload) throw new Error('Método de upload indefinido')
      const src = await upload(file, node.type)
      const data = node.data.toJS()
      this.editor.setNodeByKey(node.key, { data: { ...data, src, loading: false } })
    } catch (error) {
      throw error
    }
  }

  get isEmpty() {
    return this.serialized === INITIAL_VALUE
  }

  get editorClasses() {
    const { classes, adornment } = this.props
    return classnames(classes.editor, {
      rightAdornment: adornment.element && adornment.align === 'right',
      leftAdornment: adornment.element && adornment.align === 'left',
    })
  }

  get toolbarProps() {
    const { classes, floatingToolbar, upload, hidden } = this.props
    const { focused } = this.state

    return {
      editor: this.editor,
      withUpload: !!upload,
      upload: this.upload,
      hidden,
      classes: {
        root: classnames(classes.toolbar, {
          [classes.floatingToolbar]: /* floatingToolbar */ false,
          [classes.invisibleToolbar]: floatingToolbar && !focused,
        }),
      },
    }
  }

  get editorProps() {
    const { placeholder, rows, spellCheck, required, autoFocus, id, justifyDefault } = this.props
    const { onChange } = this
    const { value } = this.state
    const labelText = placeholder + (required ? '*' : '')

    return {
      id: id,
      ref: this.onRef,
      value,
      style: { minHeight: `${rows * 20 + 20}px` },
      placeholder: placeholder ? labelText : null,
      onChange,
      ...plugins,
      spellCheck,
      autoFocus,
      justifyDefault,
    }
  }

  get readOnlyEditor() {
    const { renderMark, renderNode } = plugins
    const props = {
      value: this.deserialized,
      renderNode,
      renderMark,
    }
    return <Editor {...props} readOnly />
  }

  render() {
    const { classes, placeholder, helperText, error, adornment, readOnly, required, label, className } = this.props
    const { showAdornment } = this.state
    const labelText = placeholder + (required ? '*' : '')
    const showLabel = this.state.showLabel || (label && !this.isEmpty) || Boolean(helperText)
    const DESABILITAR_ANIMACAO_PLACEHOLDER = true
    if (readOnly) return this.readOnlyEditor
    return (
      <div className={classes.root} ref={this.ref}>
        {label && (
          <Typography variant="caption" gutterBottom>
            {label}
          </Typography>
        )}
        {(helperText || placeholder) && (
          <Collapse in={showLabel || DESABILITAR_ANIMACAO_PLACEHOLDER}>
            <Typography variant="caption" gutterBottom color={helperText && error ? 'error' : 'default'}>
              {helperText ? helperText : label ? labelText : null}
            </Typography>
          </Collapse>
        )}
        <div className={classnames(classes.container, { [className]: Boolean(className) })}>
          {this.editor && <Toolbar {...this.toolbarProps} />}
          <div className={this.editorClasses} onKeyDown={this.onKeyDown}>
            <Editor {...this.editorProps} {...this.props.editorProps} />
            {(showAdornment || !adornment.onEvent) && <div className={classes.adornment}>{adornment.element}</div>}
            <div style={{ clear: 'both' }} />
          </div>
        </div>
      </div>
    )
  }
}
