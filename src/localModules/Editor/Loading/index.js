import { withStyles } from '@material-ui/core/styles'

import { LoadingComponent } from './LoadingComponent'
import { style } from './style'

export const Loading = withStyles(style)(LoadingComponent)
