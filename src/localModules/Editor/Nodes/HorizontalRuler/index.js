import { withStyles } from '@material-ui/core/styles'

import { HorizontalRulerComponent } from './HorizontalRulerComponent'
import { style } from './style'

export const HorizontalRuler = withStyles(style)(HorizontalRulerComponent)
