import { FOCUS_COLOR, DOTTED, SOLID, DASHED, DOUBLE } from '../../utils'

export const style = () => ({
  root: {
    borderTopColor: 'black',
    [`&.${SOLID}`]: {
      borderTopStyle: 'solid',
    },
    [`&.${DOTTED}`]: {
      borderTopStyle: 'dotted',
    },
    [`&.${DASHED}`]: {
      borderTopStyle: 'dashed',
    },
    [`&.${DOUBLE}`]: {
      borderTopStyle: 'double',
      borderTopWidth: 3,
    },
    '&.focused': {
      borderTopColor: FOCUS_COLOR,
    },
  },
})
