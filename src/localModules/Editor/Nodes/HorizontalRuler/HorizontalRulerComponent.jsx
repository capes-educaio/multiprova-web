import React from 'react'
import classnames from 'classnames'

import { propTypes } from './propTypes'

export const HorizontalRulerComponent = ({ lineStyle, classes, isFocused }) => (
  <hr className={classnames(classes.root, lineStyle, { focused: isFocused })} />
)

HorizontalRulerComponent.propTypes = propTypes
