import PropTypes from 'prop-types'

export const propTypes = {
  classes: PropTypes.object.isRequired,
  lineStyle: PropTypes.string.isRequired,
  isFocused: PropTypes.bool.isRequired,
}
