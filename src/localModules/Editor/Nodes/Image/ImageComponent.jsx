import React from 'react'

import { propTypes } from './propTypes'

export const ImageComponent = ({ classes, src }) => <img className={classes.root} src={src} alt="imagem" />

ImageComponent.propTypes = propTypes
