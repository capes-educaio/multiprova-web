import { withStyles } from '@material-ui/core/styles'

import { ImageComponent } from './ImageComponent'
import { style } from './style'

export const Image = withStyles(style)(ImageComponent)
