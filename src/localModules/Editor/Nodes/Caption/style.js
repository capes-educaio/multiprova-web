import { ALIGN_LEFT, ALIGN_CENTER, ALIGN_RIGHT, JUSTIFY } from '../../utils'

export const style = () => ({
  root: {
    margin: 0,
    lineHeight: '0.7',
    marginBottom: '10px',
    [`&.${ALIGN_LEFT}`]: {
      textAlign: 'left',
    },
    [`&.${ALIGN_CENTER}`]: {
      textAlign: 'center',
    },
    [`&.${ALIGN_RIGHT}`]: {
      textAlign: 'right',
    },
    [`&.${JUSTIFY}`]: {
      textAlign: 'justify',
    },
  },
})
