import { withStyles } from '@material-ui/core/styles'

import { CaptionComponent } from './CaptionComponent'
import { style } from './style'

export const Caption = withStyles(style)(CaptionComponent)
