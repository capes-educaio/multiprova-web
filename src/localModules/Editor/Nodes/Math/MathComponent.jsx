import React, { Component } from 'react'
import { InlineMath, BlockMath } from 'react-katex'
import classnames from 'classnames'

import { propTypes } from './propTypes'

export class MathComponent extends Component {
  static propTypes = propTypes

  ref = React.createRef()

  componentDidUpdate() {
    const selector = this.props.block ? '.katex-display' : '.base'
    const outlineElement = this.ref.current.querySelector(selector)
    if (outlineElement) outlineElement.id = 'focusOutline'
  }

  render() {
    const { value, block, isFocused, classes } = this.props
    const Component = block ? BlockMath : InlineMath
    return (
      <div ref={this.ref} className={classnames(classes.root, { focused: isFocused })}>
        {value && <Component math={value} errorColor={'#cc0000'} />}
      </div>
    )
  }
}
