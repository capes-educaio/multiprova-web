import { FOCUS_COLOR } from '../../utils'

export const style = () => ({
  root: {
    display: 'inline-block',
    '& .katex-display': {
      margin: '5px 0',
    },

    '&.focused': {
      '& #focusOutline': {
        outline: `2px solid ${FOCUS_COLOR}`,
      },
    },
  },
})
