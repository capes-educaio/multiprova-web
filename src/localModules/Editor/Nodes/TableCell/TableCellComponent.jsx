import React, { Component } from 'react'
import classnames from 'classnames'

import { propTypes } from './propTypes'
import { Resizable } from '../../Resizable'
import { INITIAL_TABLE_CELL_HEIGHT, INITIAL_TABLE_CELL_WIDTH, TABLE, updateToolbarPosition } from '../../utils'

export class TableCellComponent extends Component {
  static propTypes = propTypes

  componentDidUpdate(prevProps) {
    const { isFocused, editor, node } = this.props
    const hasNode = editor.value.document.hasNode(node.key)
    if (hasNode && isFocused && !prevProps.isFocused) this.updateTableCellsSize()
  }

  updateTableCellsSize = () => {
    const { editor, node } = this.props
    const { value } = editor
    const table = value.document.getClosest(node.key, node => node.type === TABLE)
    const path = value.document.getPath(node.key)
    const columnIndex = path.get(path.size - 1)
    const columnCells = [...table.nodes].map(row => row.nodes.get(columnIndex))
    const rowCells = [...value.document.getParent(node.key).nodes]

    const getCellData = property => cell => cell.data.get(property)
    const cellsHeight = rowCells.map(getCellData('height'))
    const cellsWidth = columnCells.map(getCellData('width'))
    const higherHeight = Math.max(...cellsHeight)
    const higherWidth = Math.max(...cellsWidth)
    this.setSize({ height: higherHeight, width: higherWidth })

    const isntThisCell = cell => cell.key !== node.key
    const otherRowCells = rowCells.filter(isntThisCell)
    const otherColumnCells = columnCells.filter(isntThisCell)
    const setCells = (cells, property) => {
      cells.forEach(cell => {
        editor.setNodeByKey(cell.key, { data: { ...cell.data.toJS(), ...property } })
      })
    }
    setCells(otherRowCells, { height: INITIAL_TABLE_CELL_HEIGHT })
    setCells(otherColumnCells, { width: INITIAL_TABLE_CELL_WIDTH })
  }

  setSize = size => {
    const { editor, node } = this.props
    editor.setNodeByKey(node.key, { data: { ...node.data.toJS(), ...size } })
    updateToolbarPosition()
  }

  get resizableProps() {
    const { isFocused, node } = this.props
    const { width, height } = node.data.toJS()
    return {
      size: { width, height },
      isFocused,
      adaptive: false,
      autoResize: isFocused,
      lockAspectRatio: false,
      onResize: this.setSize,
      minHeight: INITIAL_TABLE_CELL_HEIGHT,
      minWidth: INITIAL_TABLE_CELL_WIDTH,
    }
  }

  get verticalAlign() {
    return this.props.node.data.get('verticalAlign').replace('vertical-align-', '')
  }

  render() {
    const { children, classes, attributes } = this.props
    const { verticalAlign } = this
    return (
      <Resizable
        {...this.resizableProps}
        {...attributes}
        classes={{
          root: classnames(classes.root, {
            [classes.verticalAlignMiddle]: verticalAlign === 'middle',
            [classes.verticalAlignBottom]: verticalAlign === 'bottom',
          }),
        }}
      >
        <div className={classes.invisible}>&#8205;</div>
        {children}
      </Resizable>
    )
  }
}
