import { withStyles } from '@material-ui/core/styles'

import { TableCellComponent } from './TableCellComponent'
import { style } from './style'

export const TableCell = withStyles(style)(TableCellComponent)
export { tableCellAction } from './tableCellAction'
