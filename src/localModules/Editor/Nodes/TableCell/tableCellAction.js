import { findDOMNode } from 'slate-react'

import {
  initialRow,
  initialCell,
  ADD_ROW_BELOW,
  ADD_ROW_ABOVE,
  ADD_COLUMN_LEFT,
  ADD_COLUMN_RIGHT,
  REMOVE_ROW,
  REMOVE_COLUMN,
  VERTICAL_ALIGN_BOTTOM,
  VERTICAL_ALIGN_MIDDLE,
  VERTICAL_ALIGN_TOP,
  removeResizeBase,
  updateToolbarPosition,
} from '../../utils'
import { addNode } from '../../Node/addNode'

export const tableCellAction = (table, editor, action) => {
  const { value } = editor
  const cell = value.document.getParent(value.anchorBlock.key)
  const path = value.document.getPath(cell.key)
  const focusOnCell = () => {
    editor.moveToEndOfNode(cell)
  }
  const columnIndex = path.get(path.size - 1)
  const rowIndex = path.get(path.size - 2)
  const cellsSize = table.nodes.get(0).nodes.size
  const setCellNode = properties => {
    editor.setNodeByKey(cell.key, { data: { ...cell.data.toJS(), ...properties } })
  }
  switch (action) {
    case ADD_ROW_BELOW:
      addNode(table, editor, rowIndex + 1, initialRow(cellsSize))
      focusOnCell()
      updateToolbarPosition()
      break
    case ADD_ROW_ABOVE:
      addNode(table, editor, rowIndex, initialRow(cellsSize))
      focusOnCell()
      updateToolbarPosition()
      break
    case ADD_COLUMN_LEFT:
      ;[...table.nodes].forEach(row => {
        addNode(row, editor, columnIndex, initialCell())
      })
      focusOnCell()
      break
    case ADD_COLUMN_RIGHT:
      ;[...table.nodes].forEach(row => {
        addNode(row, editor, columnIndex + 1, initialCell())
      })
      const firstRowElement = findDOMNode(table.nodes.get(0))
      const isLastCell = cellsSize === columnIndex + 1
      if (isLastCell && firstRowElement.children.length !== 1)
        [...table.nodes].forEach(row => {
          removeResizeBase(findDOMNode(row))
        })
      focusOnCell()
      break
    case REMOVE_ROW: {
      const isLastRow = rowIndex === table.nodes.size - 1
      const rowIndexFocus = isLastRow ? rowIndex - 1 : rowIndex + 1
      editor
        .removeNodeByKey(table.nodes.get(rowIndex).key)
        .moveToEndOfNode(table.nodes.get(rowIndexFocus).nodes.get(columnIndex))
      break
    }
    case REMOVE_COLUMN: {
      ;[...table.nodes].forEach(row => {
        editor.removeNodeByKey(row.nodes.get(columnIndex).key).moveToEndOfNode(table.nodes.get(0).nodes.get(0))
      })
      const isLastColumn = columnIndex === cellsSize - 1
      const columnIndexFocus = isLastColumn ? columnIndex - 1 : columnIndex + 1
      editor.moveToEndOfNode(table.nodes.get(rowIndex).nodes.get(columnIndexFocus))
      break
    }
    case VERTICAL_ALIGN_BOTTOM:
      setCellNode({ verticalAlign: VERTICAL_ALIGN_BOTTOM })
      break
    case VERTICAL_ALIGN_MIDDLE:
      setCellNode({ verticalAlign: VERTICAL_ALIGN_MIDDLE })
      break
    case VERTICAL_ALIGN_TOP:
      setCellNode({ verticalAlign: VERTICAL_ALIGN_TOP })
      break
    default:
      console.warn('Tipo de ação desconhecido na célula da tabela')
  }
}
