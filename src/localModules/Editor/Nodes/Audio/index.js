import { withStyles } from '@material-ui/core/styles'

import { AudioComponent } from './AudioComponent'
import { style } from './style'

export const Audio = withStyles(style)(AudioComponent)
