import { FOCUS_COLOR } from '../../utils'

export const style = () => ({
  root: {
    '&.focused': {
      outline: `2px solid ${FOCUS_COLOR}`,
    },
  },
})
