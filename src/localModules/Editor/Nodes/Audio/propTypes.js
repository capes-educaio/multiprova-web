import PropTypes from 'prop-types'

export const propTypes = {
  classes: PropTypes.object.isRequired,
  src: PropTypes.string.isRequired,
  focus: PropTypes.func.isRequired,
  focused: PropTypes.bool.isRequired,
}
