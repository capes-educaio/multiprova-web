import { ALIGN_LEFT, ALIGN_CENTER, ALIGN_RIGHT, JUSTIFY } from '../../utils'

export const style = () => ({
  root: {
    [`&.${ALIGN_LEFT}`]: {
      textAlign: 'left',
    },
    [`&.${ALIGN_CENTER}`]: {
      textAlign: 'center',
    },
    [`&.${ALIGN_RIGHT}`]: {
      textAlign: 'right',
    },
    [`&.${JUSTIFY}`]: {
      textAlign: 'justify',
    },
  },
})
