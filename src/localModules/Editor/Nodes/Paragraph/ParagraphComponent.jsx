import React, { Component } from 'react'
import classnames from 'classnames'

import { propTypes } from './propTypes'
import { ALIGN_LEFT, ALIGN_RIGHT, ALIGN_CENTER, JUSTIFY, PARAGRAPH } from '../../utils'

export class ParagraphComponent extends Component {
  static propTypes = propTypes

  ref = React.createRef()

  componentDidMount() {
    const { editor, node } = this.props
    const hasAlignment = value => [ALIGN_CENTER, ALIGN_RIGHT].includes(value.data.get('align'))
    const previousSibling = editor.value.document.getPreviousSibling(node.key)
    const isInitialParagraph = !hasAlignment(node) && !previousSibling
    const isPreviousSiblingAlignedParagraph =
      previousSibling && previousSibling.type === PARAGRAPH && hasAlignment(previousSibling)
    const { justifyDefault } = editor.props
    if (justifyDefault && (isInitialParagraph || !isPreviousSiblingAlignedParagraph)) {
      setTimeout(() => {
        editor.setNodeByKey(node.key, { data: { ...node.data.toJS(), align: JUSTIFY } })
      }, 100)
    }
  }

  componentDidUpdate(prevProps) {
    const { editor, node } = this.props
    const { align } = node.data.toJS()
    const isEmptyEditor = editor.value.blocks.size === 1 && !node.text
    const placeholder = this.ref.current.querySelector('[contenteditable=false]')
    if (placeholder && isEmptyEditor) {
      if ([ALIGN_RIGHT, ALIGN_CENTER].includes(align) && placeholder.style.display !== 'none')
        placeholder.style.display = 'none'
      else if (![ALIGN_RIGHT, ALIGN_CENTER].includes(align) && placeholder.style.display !== 'inline-block')
        placeholder.style.display = 'inline-block'
    }
  }

  get classes() {
    const { classes, node } = this.props
    const { align } = node.data.toJS()
    return classnames(classes.root, { [ALIGN_LEFT]: !align, [align]: !!align })
  }

  render() {
    const { children, attributes } = this.props
    return (
      <div ref={this.ref} {...attributes} className={this.classes}>
        {children}
      </div>
    )
  }
}
