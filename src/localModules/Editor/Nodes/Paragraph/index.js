import { withStyles } from '@material-ui/core/styles'

import { ParagraphComponent } from './ParagraphComponent'
import { style } from './style'

export const Paragraph = withStyles(style)(ParagraphComponent)
