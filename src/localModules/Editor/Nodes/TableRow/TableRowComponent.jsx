import React from 'react'

import { propTypes } from './propTypes'

export const TableRowComponent = ({ children, classes, attributes }) => (
  <div className={classes.root} {...attributes}>
    {children}
  </div>
)

TableRowComponent.propTypes = propTypes
