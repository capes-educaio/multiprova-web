import { withStyles } from '@material-ui/core/styles'

import { TableRowComponent } from './TableRowComponent'
import { style } from './style'

export const TableRow = withStyles(style)(TableRowComponent)
