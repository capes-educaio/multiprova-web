import { withStyles } from '@material-ui/core/styles'

import { VideoComponent } from './VideoComponent'
import { style } from './style'

export const Video = withStyles(style)(VideoComponent)
