import React, { Component } from 'react'

import { propTypes } from './propTypes'

export class VideoComponent extends Component {
  static propTypes = propTypes

  render() {
    const { classes, src } = this.props
    return (
      <video className={classes.root} controls>
        <source src={src} />
      </video>
    )
  }
}
