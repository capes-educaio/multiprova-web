import React from 'react'
import classnames from 'classnames'
import { HIDDEN } from '../../utils'

export const TableComponent = ({ classes, children, border, isFocused, attributes }) => {
  const rootClasses = classnames(classes.root, border, { opaqueBorder: isFocused && border === HIDDEN })
  return (
    <div {...attributes} className={rootClasses}>
      {children}
    </div>
  )
}
