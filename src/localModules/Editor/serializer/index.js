import htmlSerializer from 'slate-html-serializer'

import { serialize } from './serialize'
import { deserialize } from './deserialize'

export const serializer = new htmlSerializer({ rules: [{ serialize, deserialize }] })
