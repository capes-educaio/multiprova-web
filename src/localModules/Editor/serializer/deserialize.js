import {
  BLOCK,
  INLINE,
  MARK,
  MATH,
  IMAGE,
  TAB,
  NODE_TAGS,
  NODE_CLASSES,
  MARK_TAGS,
  TABLE,
  TABLE_ROW,
  TABLE_CELL,
  INITIAL_TABLE_CELL_WIDTH,
  INITIAL_TABLE_CELL_HEIGHT,
  VERTICAL_ALIGN_TOP,
  PARAGRAPH,
  ALIGN_LEFT,
  CONTAINER,
  HORIZONTAL_RULER,
  AUDIO,
  VIDEO,
  CAPTION,
  INITIAL_CONTAINER_WIDTH,
  INITIAL_CONTAINER_HEIGHT,
  TEX,
} from '../utils'

const pixelToNumber = value => Number(value.replace('px', ''))

export const deserialize = (el, next) => {
  const nodeTagType = NODE_TAGS[el.tagName.toLowerCase()]
  if (nodeTagType) {
    const nodeTagProperties = {
      object: BLOCK,
      type: nodeTagType,
      nodes: next(el.childNodes),
    }
    switch (nodeTagType) {
      case PARAGRAPH: {
        const align = el.classList ? el.classList[0] : ALIGN_LEFT
        return {
          ...nodeTagProperties,
          data: { align },
        }
      }
      case HORIZONTAL_RULER:
        return {
          ...nodeTagProperties,
          data: { lineStyle: el.classList[0] },
        }
      case AUDIO: {
        return {
          ...nodeTagProperties,
          data: {
            src: el.src,
            align: el.classList[0],
            loading: false,
          },
        }
      }
      case VIDEO: {
        const { src, width, height } = el
        return {
          ...nodeTagProperties,
          data: {
            src,
            width,
            height,
            align: el.classList[0],
            loading: false,
          },
        }
      }
      default:
        return nodeTagProperties
    }
  }
  const markType = MARK_TAGS[el.tagName.toLowerCase()]
  if (markType) {
    return {
      object: MARK,
      type: markType,
      nodes: next(el.childNodes),
    }
  }
  const nodeClassType = el.classList && NODE_CLASSES[el.classList[0]]
  if (nodeClassType) {
    const nodeClassProperties = {
      type: nodeClassType,
      nodes: next(el.childNodes),
    }
    switch (nodeClassType) {
      case TEX:
      case MATH: {
        const isBlock = [...el.classList].includes(BLOCK)
        const delimiter = isBlock ? '/block_tex/' : '/inline_tex/'
        return {
          ...nodeClassProperties,
          object: isBlock ? BLOCK : INLINE,
          data: {
            value: el.innerHTML.replace(new RegExp(delimiter, 'g'), ''),
            block: isBlock,
          },
        }
      }
      case IMAGE: {
        const { src, width, height } = el.firstChild
        return {
          ...nodeClassProperties,
          object: BLOCK,
          data: {
            src,
            width,
            height,
            align: el.classList[1],
            loading: false,
          },
        }
      }
      case CAPTION:
        const align = el.classList ? el.classList[1] : ALIGN_LEFT
        return {
          ...nodeClassProperties,
          object: BLOCK,
          data: {
            align,
          },
        }
      case TABLE:
        return {
          ...nodeClassProperties,
          object: BLOCK,
          data: {
            align: el.classList[1],
            border: el.classList[2],
          },
        }
      case TABLE_ROW:
        return {
          ...nodeClassProperties,
          object: BLOCK,
        }
      case TABLE_CELL: {
        const { width, height } = el.style
        const verticalAlign = el.classList[1]
        return {
          ...nodeClassProperties,
          object: BLOCK,
          data: {
            width: width ? pixelToNumber(width) : INITIAL_TABLE_CELL_WIDTH,
            height: height ? pixelToNumber(height) : INITIAL_TABLE_CELL_HEIGHT,
            verticalAlign: verticalAlign ? verticalAlign : VERTICAL_ALIGN_TOP,
          },
        }
      }
      case TAB:
        return {
          ...nodeClassProperties,
          object: INLINE,
        }
      case CONTAINER: {
        const { width, height } = el.firstChild.style
        return {
          ...nodeClassProperties,
          object: BLOCK,
          data: {
            align: el.classList[1],
            border: el.classList[2],
            width: width ? pixelToNumber(width) : INITIAL_CONTAINER_WIDTH,
            height: height ? pixelToNumber(height) : INITIAL_CONTAINER_HEIGHT,
          },
        }
      }
      default:
        return
    }
  }
  return
}
