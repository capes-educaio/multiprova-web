import React from 'react'
import classnames from 'classnames'

import {
  BLOCK,
  INLINE,
  MARK,
  PARAGRAPH,
  SUB,
  SOB,
  BOLD,
  ITALIC,
  UNDERLINE,
  CODE,
  BULLETED_LIST,
  NUMBERED_LIST,
  LIST_ITEM,
  BLOCK_QUOTE,
  MATH,
  IMAGE,
  TABLE,
  TAB,
  TABLE_ROW,
  TABLE_CELL,
  INITIAL_TABLE_CELL_WIDTH,
  INITIAL_TABLE_CELL_HEIGHT,
  VERTICAL_ALIGN_TOP,
  ALIGN_LEFT,
  CONTAINER,
  HORIZONTAL_RULER,
  AUDIO,
  VIDEO,
  CAPTION,
  TEX,
} from '../utils'

export const serialize = ({ object, type, data }, children) => {
  if (object === BLOCK || object === INLINE || object === MARK) {
    const dataJS = data.toJS()
    switch (type) {
      case SOB:
        return <sup>{children}</sup>
      case SUB:
        return <sub>{children}</sub>
      case BOLD:
        return <strong>{children}</strong>
      case ITALIC:
        return <em>{children}</em>
      case UNDERLINE:
        return <u>{children}</u>
      case CODE:
        return <code>{children}</code>
      case PARAGRAPH: {
        const props = {}
        const { align } = dataJS
        if (align && align !== ALIGN_LEFT) props.className = align
        return <p {...props}>{children}</p>
      }
      case CAPTION: {
        const { align } = dataJS
        return (
          <div className={classnames(type, { [align]: align && align !== ALIGN_LEFT })}>
            <small>{children}</small>
          </div>
        )
      }
      case BLOCK_QUOTE:
        return <blockquote>{children}</blockquote>
      case BULLETED_LIST:
        return <ul>{children}</ul>
      case LIST_ITEM:
        return <li>{children}</li>
      case NUMBERED_LIST:
        return <ol>{children}</ol>
      case IMAGE: {
        const { src, width, height, align, loading } = dataJS
        if (loading) return null
        return (
          <div className={classnames(type, align)}>
            <img {...{ src, width, height }} alt="imagem" />
          </div>
        )
      }
      case AUDIO: {
        const { src, align, loading } = dataJS
        if (loading) return null
        return <audio className={classnames(align)} {...{ src }} />
      }
      case VIDEO: {
        const { src, width, height, align, loading } = dataJS
        if (loading) return null
        return <video className={classnames(align)} {...{ src, width, height }} />
      }
      case TEX:
      case MATH: {
        const { value, block } = dataJS
        const delimiter = block ? '/block_tex/' : '/inline_tex/'
        return (
          <span className={classnames(type, { block })}>
            {delimiter}
            {value}
            {delimiter}
          </span>
        )
      }
      case TABLE: {
        const { border, align } = dataJS
        return (
          <div className={classnames(type, align, border)}>
            <div>{children}</div>
          </div>
        )
      }
      case TABLE_ROW:
        return <div className={type}>{children}</div>
      case TABLE_CELL: {
        const { width, height, verticalAlign } = dataJS
        const style = {}
        if (width !== INITIAL_TABLE_CELL_WIDTH) style.width = width
        if (height !== INITIAL_TABLE_CELL_HEIGHT) style.height = height
        return (
          <div className={classnames(type, { [verticalAlign]: verticalAlign !== VERTICAL_ALIGN_TOP })} {...{ style }}>
            {children}
          </div>
        )
      }
      case TAB:
        return <span className={type} />
      case CONTAINER: {
        const { border, align, width, height } = dataJS
        const style = { width, height }
        return (
          <div className={classnames(type, align, border)}>
            <div {...{ style }}>{children}</div>
          </div>
        )
      }
      case HORIZONTAL_RULER: {
        const { lineStyle } = dataJS
        return <hr className={lineStyle} />
      }
      default:
        return null
    }
  }
  return null
}
