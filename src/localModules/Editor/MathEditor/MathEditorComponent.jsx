import React, { Component } from 'react'
import { InlineMath } from 'react-katex'

import Send from '@material-ui/icons/Send'
import Cancel from '@material-ui/icons/Cancel'

import { Tabs } from './Tabs'
import { Button } from '../Button'
import texCommands from '../utils/texCommands'
import { propTypes } from './propTypes'

export class MathEditorComponent extends Component {
  static propTypes = propTypes
  mathField

  constructor(props) {
    super(props)
    this.ref = React.createRef()
  }

  componentDidMount = () => {
    this.createMathField()
  }

  createMathField = () => {
    const { value, classes } = this.props
    const input = this.ref.current.querySelector('.' + classes.mathEditorInput)
    const that = this
    const config = {
      spaceBehavesLikeTab: false,
      handlers: {
        enter() {
          that.onChange()
        },
      },
    }
    const MQ = window.MathQuill.getInterface(2)
    this.mathField = MQ.MathField(input, config)
    if (value) {
      this.mathField.latex(value)
    }
    this.mathField.focus()
  }

  command = ({ type, value, keystrokes }) => _ => {
    const { mathField } = this
    switch (type) {
      case 'cmd':
        mathField.cmd(value)
        break
      case 'write':
        mathField.write(value)
        break
      case 'typed':
        mathField.typedText(value)
        break
      default:
        console.warn('Comando mathQuill desconhecido')
    }
    if (keystrokes) {
      keystrokes.forEach(k => mathField.keystroke(k))
    }
    mathField.focus()
  }

  onChange = event => {
    if (event) event.preventDefault()
    const value = this.mathField.latex()
    this.props.onChange({ value })
  }

  get commandTabs() {
    return texCommands.map((commandTypes, index) => (
      <div key={index}>
        {commandTypes.map((command, index) => (
          <div key={index} className={this.props.classes.command} onClick={this.command(command)}>
            <InlineMath math={command.icon} />
          </div>
        ))}
      </div>
    ))
  }

  render() {
    const { classes, onCancel } = this.props
    return (
      <div ref={this.ref} className={classes.root}>
        <Tabs labels={['Básicos', 'Cálculo', 'Símbolos']}>{this.commandTabs}</Tabs>
        <div
          onClick={event => {
            event.stopPropagation()
          }}
          className={classes.inputContainer}
        >
          <span className={classes.mathEditorInput} />
          <Button onClick={this.onChange} tooltip="OK">
            <Send />
          </Button>
          <Button onClick={onCancel} tooltip="Cancelar">
            <Cancel />
          </Button>
        </div>
      </div>
    )
  }
}
