import { withStyles } from '@material-ui/core/styles'

import { MathEditorComponent } from './MathEditorComponent'
import { style } from './style'

export const MathEditor = withStyles(style)(MathEditorComponent)
