export const style = () => ({
  root: {
    display: 'inline-flex',
    flexDirection: 'column',
    backgroundColor: 'white',
    zIndex: 1,
    border: '1px solid lightgray',
    borderRadius: '5px',
    boxShadow: '1px 1px 10px lightgray',
    maxWidth: 500,

    '& .mq-math-mode': {
      // Solução do bug que faz sumir parte do expoente da raiz
      '& .mq-root-block': {
        padding: '10px 2px',
      },
      // Correção da altura do expoente da raiz
      '& .sup.mq-nthroot': {
        verticalAlign: '0.4em',
      },
    },
  },

  inputContainer: {
    padding: '0 0px 5px 5px',
    display: 'flex',
  },

  mathEditorInput: {
    maxWidth: '435px',
    padding: '4px 5px 0',
    flexGrow: 1,
    borderRadius: '5px !important',
    borderColor: 'lightgray !important',
    '&.mq-focused': {
      boxShadow: 'none',
    },
  },

  command: {
    display: 'inline-flex',
    justifyContent: 'center',
    alignItems: 'center',
    minWidth: '30px',
    cursor: 'default',
    border: '1px solid transparent',
    borderRadius: '2px',
    margin: '3px',
    padding: '3px',
    '&:hover': {
      borderColor: 'lightblue',
    },
  },
})
