import { TabsComponent } from './TabsComponent'

import { withStyles } from '@material-ui/core/styles'

import { style } from './style'

export const Tabs = withStyles(style, { withTheme: true })(TabsComponent)
