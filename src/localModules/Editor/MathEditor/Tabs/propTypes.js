import PropTypes from 'prop-types'

export const propTypes = {
  labels: PropTypes.arrayOf(PropTypes.string),
  containers: PropTypes.arrayOf(PropTypes.node),
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
}
