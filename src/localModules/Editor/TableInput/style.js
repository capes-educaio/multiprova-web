export const style = () => ({
  root: {
    padding: 5,
    position: 'absolute',
    top: -1,
    right: -1,
    background: 'rgb(240,240,240)',
    zIndex: 1,
    border: '1px solid rgb(200,200,200)',
    borderRadius: 5,
  },
  table: {
    borderCollapse: 'collapse',
    width: 150,
    height: 150,
    padding: 5,
  },
  cell: {
    border: '1px solid #eee',
    background: 'white',
    cursor: 'pointer',
    '&.selected': {
      background: 'rgb(150,150,150)',
    },
  },
  textField: { width: 60, height: 30 },
  inputs: {
    display: 'flex',
    justifyContent: 'space-around',
    marginBottom: 5,
  },
  buttons: {
    display: 'flex',
    justifyContent: 'flex-end',
    marginTop: 5,
  },
})
