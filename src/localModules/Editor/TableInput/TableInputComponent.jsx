import React, { Component } from 'react'
import classnames from 'classnames'
import { propTypes } from './propTypes'

import Collapse from '@material-ui/core/Collapse'
import TextField from '@material-ui/core/TextField'
import Send from '@material-ui/icons/Send'
import Cancel from '@material-ui/icons/Cancel'

import { Button } from '../Button'

export class TableInputComponent extends Component {
  static propTypes = propTypes
  maxDimension = 5
  state = {
    showGrid: true,
    row: 0,
    cell: 0,
  }

  onEnterCell = (row, cell) => () => this.setState({ row, cell })

  onChangeInput = type => ({ target }) => {
    const value = Number(target.value)
    const { row, cell } = this.state
    const showGrid = (type === 'row' && Math.max(value, cell) <= 20) || (type === 'cell' && Math.max(value, row) <= 20)
    if (value > this.maxDimension && value <= 20) this.maxDimension = value
    this.setState({ [type]: value, showGrid })
  }

  onChange = () => {
    const { row, cell } = this.state
    this.props.onChange(row, cell)
  }

  inputProps = type => ({
    value: this.state[type],
    classes: { root: this.props.classes.textField },
    inputProps: { max: 100, min: 1 },
    InputProps: { onChange: this.onChangeInput(type) },
    type: 'number',
  })

  render() {
    const { classes, onCancel } = this.props
    return (
      <div className={classes.root}>
        <div className={classes.inputs}>
          <TextField {...this.inputProps('row')} />
          <TextField {...this.inputProps('cell')} />
        </div>
        <Collapse in={this.state.showGrid}>
          <table className={classes.table}>
            <tbody>
              {Array(this.maxDimension)
                .fill([...Array(this.maxDimension).keys()].map(i => i + 1))
                .map((row, rowIndex) => (
                  <tr key={rowIndex} className={classes.row}>
                    {row.map(cell => {
                      const rowNumber = rowIndex + 1
                      return (
                        <td
                          key={cell}
                          className={classnames(classes.cell, {
                            selected: this.state.row >= rowNumber && this.state.cell >= cell,
                          })}
                          onMouseEnter={this.onEnterCell(rowNumber, cell)}
                          onClick={this.onChange}
                        />
                      )
                    })}
                  </tr>
                ))}
            </tbody>
          </table>
        </Collapse>
        <div className={classes.buttons}>
          <Button onClick={this.onChange} tooltip='OK'>
            <Send />
          </Button>
          <Button onClick={onCancel} tooltip='Cancelar'>
            <Cancel />
          </Button>
        </div>
      </div>
    )
  }
}
