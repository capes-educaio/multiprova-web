export const toggleData = (name, values, node, editor) => {
  const data = node.data.toJS()
  const index = values.indexOf(data[name])
  const newValue = index === values.length - 1 ? values[0] : values[index + 1]
  editor.setNodeByKey(node.key, { data: { ...data, [name]: newValue } })
}
