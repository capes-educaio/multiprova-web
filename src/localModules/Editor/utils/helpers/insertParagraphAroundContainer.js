import { PARAGRAPH } from '../constants'

export const insertParagraphBelowContainer = (container, editor) => insertParagraph(container, 1, editor)
export const insertParagraphAboveContainer = (container, editor) => insertParagraph(container, 0, editor)

const insertParagraph = (node, position, editor) => {
  const parent = editor.value.document.getParent(node.key)
  const nodeIndex = parent.getPath(node.key).get(0)
  const paragraphIndex = nodeIndex + position
  editor
    .insertNodeByKey(parent.key, paragraphIndex, { object: 'block', type: PARAGRAPH })
    .moveToStartOfNode(editor.value.document.getNode(parent.key).nodes.get(paragraphIndex))
}
