export const getVideoData = async file => {
  const localSrc = URL.createObjectURL(file)
  const resolution = await new Promise(resolve => {
    const video = document.createElement('video')
    video.addEventListener('loadedmetadata', () => {
      resolve({
        height: video.videoHeight,
        width: video.videoWidth,
      })
    })
    video.onerror = () => {
      throw new Error('Erro ao obter os dados do vídeo')
    }
    video.src = localSrc
  })
  return { localSrc, ...resolution }
}
