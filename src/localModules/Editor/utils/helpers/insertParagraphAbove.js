import { PARAGRAPH } from '../constants'

export const insertParagraphAbove = editor => {
  const { value } = editor
  const { anchorBlock } = value
  if (anchorBlock) {
    const parent = value.document.getParent(anchorBlock.key)
    const anchorBlockIndex = parent.getPath(anchorBlock.key).get(0)
    editor
      .insertNodeByKey(parent.key, anchorBlockIndex, { object: 'block', type: PARAGRAPH })
      .moveToStartOfPreviousBlock()
  }
}
