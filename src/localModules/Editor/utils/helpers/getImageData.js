export const getImageData = async file => {
  const reader = new FileReader()
  reader.readAsDataURL(file)
  const localSrc = await new Promise(resolve => {
    reader.onload = () => {
      resolve(reader.result)
    }
    reader.onerror = () => {
      throw new Error('Erro ao obter os dados da imagem')
    }
  })
  const dimensions = await new Promise(resolve => {
    var image = new Image()
    image.onload = () => {
      const { width, height } = image
      resolve({ width, height })
    }
    image.onerror = () => {
      throw new Error('Erro ao obter os dados da imagem')
    }
    image.src = localSrc
  })
  return { localSrc, ...dimensions }
}
