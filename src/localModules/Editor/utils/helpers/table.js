import { Block } from 'slate'

import {
  TABLE_ROW,
  TABLE_CELL,
  TABLE,
  PARAGRAPH,
  ALIGN_CENTER,
  VERTICAL_ALIGN_TOP,
  INITIAL_TABLE_CELL_WIDTH,
  INITIAL_TABLE_CELL_HEIGHT,
  SOLID,
} from '../constants'

const cell = {
  object: 'block',
  type: TABLE_CELL,
  nodes: [{ object: 'block', type: PARAGRAPH }],
  data: {
    width: INITIAL_TABLE_CELL_WIDTH,
    height: INITIAL_TABLE_CELL_HEIGHT,
    verticalAlign: VERTICAL_ALIGN_TOP,
  },
}

const row = cellsSize => ({
  object: 'block',
  type: TABLE_ROW,
  nodes: Array(cellsSize).fill(cell),
})

export const initialTable = (rows, columns) =>
  Block.fromJSON({
    type: TABLE,
    nodes: Array(rows).fill(row(columns)),
    data: {
      align: ALIGN_CENTER,
      border: SOLID,
    },
  })

export const initialRow = cellSize => Block.fromJSON(row(cellSize))
export const initialCell = () => Block.fromJSON(cell)
