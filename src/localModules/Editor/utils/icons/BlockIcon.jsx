import React from 'react'

export const BlockIcon = props => {
  return (
    <svg viewBox="0 0 24 24" {...props} className={props.classes.root}>
      <rect x="3" y="3" width="18" height="2" rx="0.5" ry="0.5" fill="#828282" />
      <rect x="3" y="19" width="18" height="2" rx="0.5" ry="0.5" fill="#828282" />
      <rect x="5" y="7" width="14" height="10" rx="1" ry="1" fill="#828282" />
      <path d="M0,0H24V24H0Z" fill="none" />
    </svg>
  )
}
