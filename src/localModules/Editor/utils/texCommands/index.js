import { basicos } from './basicos'
import { calculo } from './calculo'
import { simbolos } from './simbolos'

const texCommands = [basicos, calculo, simbolos]

export default texCommands
