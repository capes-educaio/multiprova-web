export const calculo = [
  {
    type: 'cmd',
    value: '\\sum',
    icon: '\\sum',
  },
  {
    type: 'cmd',
    value: '\\prod',
    icon: '\\prod',
  },
  {
    type: 'write',
    value: '\\lim_{x\\to\\infty}',
    icon: '\\lim_{x\\to\\infty}',
  },
  {
    type: 'write',
    value: '\\lim_{x\\to\\infty}',
    icon: '\\lim_{x\\to n}',
    keystrokes: ['Left', 'Backspace'],
  },
  {
    type: 'cmd',
    value: '\\int',
    icon: '\\int',
  },
  {
    type: 'write',
    value: '\\int_{}^{}',
    icon: '\\int_a^b',
    keystrokes: ['Left', 'Left'],
  },
  {
    type: 'typed',
    value: '\\oint_V',
    icon: '\\oint_V',
    keystrokes: ['Backspace'],
  },
]
