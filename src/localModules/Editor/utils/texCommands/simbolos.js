export const simbolos = [
  {
    type: 'write',
    value: '\\alpha',
    icon: '\\alpha',
  },
  {
    type: 'write',
    value: '\\beta',
    icon: '\\beta',
  },
  {
    type: 'write',
    value: '\\gamma',
    icon: '\\gamma',
  },
  {
    type: 'write',
    value: '\\Gamma',
    icon: '\\Gamma',
  },
  {
    type: 'write',
    value: '\\delta',
    icon: '\\delta',
  },
  {
    type: 'write',
    value: '\\Delta',
    icon: '\\Delta',
  },
  {
    type: 'write',
    value: '\\epsilon',
    icon: '\\epsilon',
  },
  {
    type: 'write',
    value: '\\zeta',
    icon: '\\zeta',
  },
  {
    type: 'write',
    value: '\\eta',
    icon: '\\eta',
  },
  {
    type: 'write',
    value: '\\theta',
    icon: '\\theta',
  },
  {
    type: 'write',
    value: '\\Theta',
    icon: '\\Theta',
  },
  {
    type: 'write',
    value: '\\iota',
    icon: '\\iota',
  },
  {
    type: 'write',
    value: '\\kappa',
    icon: '\\kappa',
  },
  {
    type: 'write',
    value: '\\lambda',
    icon: '\\lambda',
  },
  {
    type: 'write',
    value: '\\Lambda',
    icon: '\\Lambda',
  },
  {
    type: 'write',
    value: '\\mu',
    icon: '\\mu',
  },
  {
    type: 'write',
    value: '\\nu',
    icon: '\\nu',
  },
  {
    type: 'write',
    value: '\\xi',
    icon: '\\xi',
  },
  {
    type: 'write',
    value: '\\Xi',
    icon: '\\Xi',
  },
  {
    type: 'write',
    value: '\\epsilon',
    icon: '\\epsilon',
  },
  {
    type: 'write',
    value: '\\pi',
    icon: '\\pi',
  },
  {
    type: 'write',
    value: '\\Pi',
    icon: '\\Pi',
  },
  {
    type: 'write',
    value: '\\rho',
    icon: '\\rho',
  },
  {
    type: 'write',
    value: '\\sigma',
    icon: '\\sigma',
  },
  {
    type: 'write',
    value: '\\Sigma',
    icon: '\\Sigma',
  },
  {
    type: 'write',
    value: '\\tau',
    icon: '\\tau',
  },
  {
    type: 'write',
    value: '\\upsilon',
    icon: '\\upsilon',
  },
  {
    type: 'write',
    value: '\\phi',
    icon: '\\phi',
  },
  {
    type: 'write',
    value: '\\Phi',
    icon: '\\Phi',
  },
  {
    type: 'write',
    value: '\\chi',
    icon: '\\chi',
  },
  {
    type: 'write',
    value: '\\psi',
    icon: '\\psi',
  },
  {
    type: 'write',
    value: '\\Psi',
    icon: '\\Psi',
  },
  {
    type: 'write',
    value: '\\omega',
    icon: '\\omega',
  },
  {
    type: 'write',
    value: '\\Omega',
    icon: '\\Omega',
  },
  {
    type: 'write',
    value: '\\leftarrow',
    icon: '\\leftarrow',
  },
  {
    type: 'write',
    value: '\\rightarrow',
    icon: '\\rightarrow',
  },
  {
    type: 'write',
    value: '\\uparrow',
    icon: '\\uparrow',
  },
  {
    type: 'write',
    value: '\\downarrow',
    icon: '\\downarrow',
  },
  {
    type: 'write',
    value: '\\leftrightarrow',
    icon: '\\leftrightarrow',
  },
  {
    type: 'write',
    value: '\\updownarrow',
    icon: '\\updownarrow',
  },
  {
    type: 'write',
    value: '\\longleftarrow',
    icon: '\\longleftarrow',
  },
  {
    type: 'write',
    value: '\\longrightarrow',
    icon: '\\longrightarrow',
  },
  {
    type: 'write',
    value: '\\longleftrightarrow',
    icon: '\\longleftrightarrow',
  },
  {
    type: 'write',
    value: '\\Leftarrow',
    icon: '\\Leftarrow',
  },
  {
    type: 'write',
    value: '\\Rightarrow',
    icon: '\\Rightarrow',
  },
  {
    type: 'write',
    value: '\\Uparrow',
    icon: '\\Uparrow',
  },
  {
    type: 'write',
    value: '\\Downarrow',
    icon: '\\Downarrow',
  },
  {
    type: 'write',
    value: '\\Leftrightarrow',
    icon: '\\Leftrightarrow',
  },
  {
    type: 'write',
    value: '\\Updownarrow',
    icon: '\\Updownarrow',
  },
  { type: 'write', value: '\\Longleftarrow', icon: '\\Longleftarrow' },
  {
    type: 'write',
    value: '\\Longrightarrow',
    icon: '\\Longrightarrow',
  },
  {
    type: 'write',
    value: '\\Longleftrightarrow',
    icon: '\\Longleftrightarrow',
  },
  {
    type: 'write',
    value: '\\leftharpoonup',
    icon: '\\leftharpoonup',
  },
  {
    type: 'write',
    value: '\\rightharpoonup',
    icon: '\\rightharpoonup',
  },
  {
    type: 'write',
    value: '\\leftharpoondown',
    icon: '\\leftharpoondown',
  },
  {
    type: 'write',
    value: '\\rightharpoondown',
    icon: '\\rightharpoondown',
  },
  {
    type: 'write',
    value: '\\rightleftharpoons',
    icon: '\\rightleftharpoons',
  },
  {
    type: 'write',
    value: '\\mapsto',
    icon: '\\mapsto',
  },
]
