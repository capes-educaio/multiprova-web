import { insertParagraphAbove, insertParagraphAboveContainer } from '../helpers'
import { TABLE, CONTAINER } from '../constants'

export const removeNode = (node, editor) => {
  const { type, key, object } = node
  const isContainer = [TABLE, CONTAINER].includes(type)
  if (object === 'block' && editor.value.blocks.size === 1) {
    if (isContainer) insertParagraphAboveContainer(node, editor)
    else insertParagraphAbove(editor)
  }
  editor.removeNodeByKey(key).focus()
}
