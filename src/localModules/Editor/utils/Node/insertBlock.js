import { PARAGRAPH, TABLE } from '../constants'

export const insertBlock = (block, editor, autoFocus = true) => {
  const { anchorBlock } = editor.focus().value
  const { value } = editor
  const { end } = value.selection
  const isLastBlock = !value.document.getNextSibling(anchorBlock.key) && end.isAtEndOfNode(anchorBlock)
  if (isLastBlock) {
    editor.insertBlock(PARAGRAPH).moveBackward(1)
  }
  editor.insertBlock(block)
  const blockKey = editor.value.anchorBlock
  if (!autoFocus) editor.moveForward(1)
  if (block.type === TABLE) {
    const { value } = editor
    const table = value.document.getClosest(value.anchorBlock.key, node => node.type === TABLE)
    const firstCell = table.nodes.get(0).nodes.get(0)
    editor.moveToEndOfNode(firstCell)
  }
  if (anchorBlock) {
    const { type, key, nodes, text } = anchorBlock
    const isEmptyParagraph = type === PARAGRAPH && nodes.size === 1 && !text
    if (isEmptyParagraph) editor.removeNodeByKey(key)
  }
  return blockKey
}
