import { Map } from 'immutable'

import { removeNode } from './removeNode'
import { insertBlock } from './insertBlock'

export const toggleNodeType = (node, editor) => {
  const { key, type } = node
  const data = node.data.toJS()
  const { block } = data
  const properties = {
    key,
    data: Map({ ...data, block: !block }),
    type,
  }
  removeNode(node, editor)
  if (block) editor.insertInline(properties)
  else insertBlock(properties, editor)
}
