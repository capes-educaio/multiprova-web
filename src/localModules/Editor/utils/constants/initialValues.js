export const INITIAL_TABLE_CELL_WIDTH = 35
export const INITIAL_TABLE_CELL_HEIGHT = 21

export const INITIAL_CONTAINER_WIDTH = 300
export const INITIAL_CONTAINER_HEIGHT = 24
