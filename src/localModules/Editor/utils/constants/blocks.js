import {
  PARAGRAPH,
  BOLD,
  ITALIC,
  UNDERLINE,
  SOB,
  SUB,
  CODE,
  BULLETED_LIST,
  NUMBERED_LIST,
  LIST_ITEM,
  BLOCK_QUOTE,
  MATH,
  TEX,
  IMAGE,
  TABLE,
  TABLE_ROW,
  TABLE_CELL,
  TAB,
  CONTAINER,
  HORIZONTAL_RULER,
  VIDEO,
  AUDIO,
  CAPTION,
} from './editor'

export const ALIGN_CENTER = 'align-center'
export const ALIGN_LEFT = 'align-left'
export const ALIGN_RIGHT = 'align-right'
export const JUSTIFY = 'justify'
export const BORDER = 'border'
export const DOTTED = 'dotted'
export const VERTICAL_ALIGN_TOP = 'vertical-align-top'
export const VERTICAL_ALIGN_MIDDLE = 'vertical-align-middle'
export const VERTICAL_ALIGN_BOTTOM = 'vertical-align-bottom'
export const SOLID = 'solid'
export const DASHED = 'dashed'
export const DOUBLE = 'double'
export const HIDDEN = 'hidden'
export const LINE_STYLE = 'lineStyle'

export const EDIT = 'edit'
export const REMOVE = 'remove'

export const NODE_TAGS = {
  blockquote: BLOCK_QUOTE,
  p: PARAGRAPH,
  ul: BULLETED_LIST,
  ol: NUMBERED_LIST,
  li: LIST_ITEM,
  hr: HORIZONTAL_RULER,
  video: VIDEO,
  audio: AUDIO,
}

export const NODE_CLASSES = {
  image: IMAGE,
  math: MATH,
  table: TABLE,
  'table-row': TABLE_ROW,
  'table-cell': TABLE_CELL,
  tex: TEX,
  tab: TAB,
  container: CONTAINER,
  caption: CAPTION,
}

export const MARK_TAGS = {
  em: ITALIC,
  strong: BOLD,
  u: UNDERLINE,
  sup: SOB,
  sub: SUB,
  code: CODE,
}
