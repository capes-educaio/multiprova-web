import {
  isBoldHotkey,
  isItalicHotkey,
  isUnderlinedHotkey,
  isCodeHotkey,
  BOLD,
  ITALIC,
  UNDERLINE,
  CODE,
  TABLE_CELL,
  isTabHotkey,
  TAB,
  IMAGE,
  PARAGRAPH,
  TEX,
  MATH,
  isParagraphAboveHotkey,
  insertParagraphAbove,
} from '../utils'

export const onKeyDown = (event, editor, next) => {
  let mark
  if (isBoldHotkey(event)) mark = BOLD
  else if (isItalicHotkey(event)) mark = ITALIC
  else if (isUnderlinedHotkey(event)) mark = UNDERLINE
  else if (isCodeHotkey(event)) mark = CODE
  if (mark) {
    editor.toggleMark(mark)
    event.preventDefault()
    return next()
  }

  if (isTabHotkey(event)) {
    event.preventDefault()
    editor.insertInline(TAB).moveForward(1)
    return next()
  }

  const { value } = editor
  const { document, anchorBlock, selection } = value
  if (anchorBlock) {
    const anchorBlockParent = document.getParent(anchorBlock.key)
    if (anchorBlockParent.type === TABLE_CELL) {
      switch (event.key) {
        case 'Backspace':
          if (anchorBlockParent.nodes.size === 1 && !anchorBlock.text) event.preventDefault()
          else return next()
          break
        case 'Delete': {
          if (
            !document.getNextSibling(anchorBlock.key) &&
            selection.isCollapsed &&
            selection.end.offset === anchorBlock.text.length
          ) {
            event.preventDefault()
          } else return next()
          break
        }
        default:
          break
      }
    }

    const shouldPreventEmptyEditor =
      value.blocks.size === 1 &&
      [IMAGE, TEX, MATH].includes(anchorBlock.type) &&
      ['Backspace', 'Delete'].includes(event.key)
    if (shouldPreventEmptyEditor) {
      editor.insertBlock(PARAGRAPH)
      editor.removeNodeByKey(anchorBlock.key).focus()
      event.preventDefault()
    }

    const isAnchorBlockAddParagraph = [IMAGE, TEX, MATH].includes(anchorBlock.type)
    const fireParagraphAbove = isParagraphAboveHotkey(event) && isAnchorBlockAddParagraph
    const fireParagraphBelow = event.key === ' ' && isAnchorBlockAddParagraph
    if (fireParagraphAbove) {
      insertParagraphAbove(editor)
      event.preventDefault()
    } else if (fireParagraphBelow) {
      editor.insertBlock(PARAGRAPH)
      event.preventDefault()
    }
  }

  return next()
}
