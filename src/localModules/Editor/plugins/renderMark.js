import React from 'react'
import { BOLD, ITALIC, UNDERLINE, CODE, SOB, SUB } from '../utils'

export const renderMark = (props, editor, next) => {
  const { children, mark, attributes } = props

  switch (mark.type) {
    case SOB:
      return <sup {...attributes}>{children}</sup>
    case SUB:
      return <sub {...attributes}>{children}</sub>
    case BOLD:
      return <strong {...attributes}>{children}</strong>
    case CODE:
      return <code {...attributes}>{children}</code>
    case ITALIC:
      return <em {...attributes}>{children}</em>
    case UNDERLINE:
      return <u {...attributes}>{children}</u>
    default:
      return next()
  }
}
