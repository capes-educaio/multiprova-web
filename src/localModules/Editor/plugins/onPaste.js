export const onPaste = (event, editor) => {
  const text = event.clipboardData.getData('Text')
  if (text) editor.insertText(text)
}
