import React from 'react'

import { Node } from '../Node'
import { TableCell, TableRow, Paragraph, Caption } from '../Nodes'
import {
  BULLETED_LIST,
  NUMBERED_LIST,
  LIST_ITEM,
  BLOCK_QUOTE,
  MATH,
  IMAGE,
  TABLE,
  TABLE_ROW,
  TABLE_CELL,
  TEX,
  TAB,
  PARAGRAPH,
  CONTAINER,
  HORIZONTAL_RULER,
  VIDEO,
  AUDIO,
  CAPTION,
} from '../utils'

export const renderNode = (props, editor, next) => {
  const { attributes, children, node } = props
  switch (node.type) {
    case PARAGRAPH:
      return <Paragraph {...props} />
    case CAPTION:
      return <Caption {...props} />
    case BLOCK_QUOTE:
      return <blockquote {...attributes}>{children}</blockquote>
    case BULLETED_LIST:
      return <ul {...attributes}>{children}</ul>
    case LIST_ITEM:
      return <li {...attributes}>{children}</li>
    case NUMBERED_LIST:
      return <ol {...attributes}>{children}</ol>
    case TABLE_ROW:
      return <TableRow {...props} />
    case TABLE_CELL:
      return <TableCell {...props} />
    case TEX:
    case MATH:
    case IMAGE:
    case VIDEO:
    case AUDIO:
    case TABLE:
    case CONTAINER:
    case HORIZONTAL_RULER:
      return <Node {...props} />
    case TAB:
      return <span {...attributes} style={{ paddingLeft: '32px' }} />
    default:
      return next()
  }
}
