import { HORIZONTAL_RULER, MATH, TEX, TAB, IMAGE, VIDEO, AUDIO } from '../utils'

const isVoid = true

export const schema = {
  blocks: {
    [MATH]: { isVoid },
    [TEX]: { isVoid },
    [IMAGE]: { isVoid },
    [VIDEO]: { isVoid },
    [AUDIO]: { isVoid },
    [HORIZONTAL_RULER]: { isVoid },
  },
  inlines: {
    [MATH]: { isVoid },
    [TEX]: { isVoid },
    [TAB]: { isVoid },
  },
}
