import PropTypes from 'prop-types'

import { VIDEO, AUDIO } from './utils'

export const propTypes = {
  html: PropTypes.string,
  onChange: PropTypes.func,
  onFocus: PropTypes.func,
  onBlur: PropTypes.func,
  rows: PropTypes.number,
  placeholder: PropTypes.string,
  readOnly: PropTypes.bool,
  floatingToolbar: PropTypes.bool,
  speelCheck: PropTypes.bool,
  adornment: PropTypes.shape({
    align: PropTypes.oneOf(['left', 'right']),
    onEvent: PropTypes.oneOf([false, 'hover', 'focus', 'blur']),
    element: PropTypes.element,
  }),
  error: PropTypes.bool,
  helperText: PropTypes.string,
  requerid: PropTypes.bool,
  label: PropTypes.node,
  classes: PropTypes.object.isRequired,
  upload: ({ upload, hidden }, propName, componentName) => {
    if (!upload && (!hidden.includes(VIDEO) || !hidden.includes(AUDIO)))
      return new Error(
        `The prop \`${propName}\` is required in \`${componentName}\` if prop \`hidden\` does not contain video and audio`,
      )
  },
  hidden: PropTypes.arrayOf(PropTypes.string).isRequired,
  editorProps: PropTypes.object,
}
