export const style = () => ({
  root: {
    width: '100%',
  },
  container: {
    position: 'relative',
    background: 'white',
    borderRadius: '5px',
    border: '1px solid rgba(0, 0, 0, 0.2)',
  },
  editor: {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'row',
    '&.leftAdornment': {
      flexDirection: 'row-reverse',
      width: 'calc(100% - 40px)',
      '& > :first-child': {
        paddingLeft: 0,
      },
    },
    '&.rightAdornment > :first-child': {
      width: 'calc(100% - 40px)',
      paddingRight: 0,
    },
    '& > :first-child': {
      padding: '10px',
      width: '100%',
      boxSizing: 'border-box',
    },
  },
  toolbar: {},
  floatingToolbar: {
    position: 'absolute',
    top: '-27px',
    left: '-1px',
    backgroundColor: 'white',
    border: '1px solid rgba(0,0,0,0.2)',
    borderRadius: '5px 5px 5px 0px',
    padding: '0px 5px',
  },
  invisibleToolbar: { display: 'none' },
  helperText: {
    fontSize: 12,
  },
  adornment: {
    '& > *': {
      width: 35,
      height: 35,
    },
  },
})
