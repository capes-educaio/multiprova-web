import React, { Component } from 'react'
import { Map } from 'immutable'

import {
  getImageData,
  PARAGRAPH,
  BULLETED_LIST,
  NUMBERED_LIST,
  LIST_ITEM,
  MATH,
  IMAGE,
  ALIGN_CENTER,
  TABLE,
  initialTable,
  TEX,
  TAB,
  BLOCK,
  PARAGRAPH_BELOW,
  PARAGRAPH_ABOVE,
  insertParagraphAbove,
  MARK,
  DATA,
  ALIGN_LEFT,
  ALIGN_RIGHT,
  JUSTIFY,
  updateToolbarPosition,
  CONTAINER,
  HORIZONTAL_RULER,
  SOLID,
  insertBlock,
  VIDEO,
  AUDIO,
  CAPTION,
  getVideoData,
  INITIAL_CONTAINER_WIDTH,
  INITIAL_CONTAINER_HEIGHT,
} from '../utils'

import { MathEditor } from '../MathEditor'
import { TableInput } from '../TableInput'
import { Button } from '../Button'
import { actions } from './actions'

export class ToolbarComponent extends Component {
  ref = React.createRef()
  fileInput
  blockInput

  constructor(props) {
    super(props)
    this.state = {
      showBlockInput: false,
    }
  }

  componentDidMount() {
    this.fileInput = this.ref.current.querySelector('input[type="file"]')
  }

  onClickMark = type => event => {
    event.preventDefault()
    this.props.editor.toggleMark(type)
  }

  onClickData = data => event => {
    event.preventDefault()
    const { editor } = this.props
    const isAlign = [ALIGN_CENTER, ALIGN_LEFT, ALIGN_RIGHT, JUSTIFY].includes(data)
    const ALIGNABLE_TYPES = [MATH, AUDIO, VIDEO, IMAGE, TEX]
    if (isAlign) {
      const alignableBlocks = editor.value.blocks.filter(
        block =>
          [PARAGRAPH, CAPTION].includes(block.type) || (ALIGNABLE_TYPES.includes(block.type) && data !== JUSTIFY),
      )
      alignableBlocks.forEach(block => {
        editor.setNodeByKey(block.key, { data: { ...block.data.toJS(), align: data } })
        if (ALIGNABLE_TYPES.includes(block.type)) updateToolbarPosition()
      })
    }
  }

  hasBlock = type => {
    const { value } = this.props.editor
    return value.blocks.some(node => node.type === type)
  }

  onClickBlock = type => event => {
    event.preventDefault()
    const { editor, classes } = this.props
    const { value } = editor
    const { document } = value
    switch (type) {
      case IMAGE:
      case AUDIO:
      case VIDEO:
        this.fileInput.accept = type === VIDEO ? `${type}/mp4` : type === AUDIO ? `${type}/mp3` : `${type}/*`
        this.fileInput.click()
        return
      case MATH:
        this.blockInput = (
          <MathEditor
            classes={{ root: classes.mathEditor }}
            onChange={this.insertMath}
            onCancel={() => {
              this.setState({ showBlockInput: false })
            }}
          />
        )
        this.setState({ showBlockInput: true })
        return
      case TEX:
        insertBlock({ type, data: Map({ value: '', block: true, edit: true }) }, editor)
        return
      case TABLE:
        this.blockInput = (
          <TableInput
            onChange={this.insertTable}
            onCancel={() => {
              this.setState({ showBlockInput: false })
            }}
          />
        )
        this.setState({ showBlockInput: true })
        return
      case TAB:
        editor.insertInline(TAB).moveForward(1)
        return
      case PARAGRAPH_BELOW:
        editor.insertBlock(PARAGRAPH)
        return
      case PARAGRAPH_ABOVE: {
        insertParagraphAbove(editor)
        return
      }
      case CONTAINER: {
        insertBlock(
          {
            type,
            data: Map({
              border: SOLID,
              align: ALIGN_CENTER,
              width: INITIAL_CONTAINER_WIDTH,
              height: INITIAL_CONTAINER_HEIGHT,
            }),
            nodes: [{ object: 'block', type: PARAGRAPH }],
          },
          editor,
        )
        return
      }
      case HORIZONTAL_RULER: {
        insertBlock({ type, data: Map({ lineStyle: SOLID }) }, editor, false)
        return
      }
      case CAPTION: {
        const { anchorBlock } = value
        if (anchorBlock.type === CAPTION && !anchorBlock.text) {
          editor.removeNodeByKey(anchorBlock.key)
          editor.insertBlock(PARAGRAPH)
        }
        const isMedia = [IMAGE, AUDIO, VIDEO].includes(anchorBlock.type)
        if (!isMedia) return
        const align = value.anchorBlock.data.get('align')
        insertBlock({ type, data: Map({ align }) }, editor)
        return
      }
      default:
        break
    }
    if (type !== BULLETED_LIST && type !== NUMBERED_LIST) {
      const isActive = this.hasBlock(type)
      const isList = this.hasBlock(LIST_ITEM)
      if (isList)
        editor
          .setBlocks(isActive ? PARAGRAPH : type)
          .unwrapBlock(BULLETED_LIST)
          .unwrapBlock(NUMBERED_LIST)
      else editor.setBlocks(isActive ? PARAGRAPH : type)
    } else {
      const isList = this.hasBlock(LIST_ITEM)
      const isType = value.blocks.some(block => {
        return !!document.getClosest(block.key, parent => parent.type === type)
      })
      if (isList && isType)
        editor
          .setBlocks(PARAGRAPH)
          .unwrapBlock(BULLETED_LIST)
          .unwrapBlock(NUMBERED_LIST)
      else if (isList) editor.unwrapBlock(type === BULLETED_LIST ? NUMBERED_LIST : BULLETED_LIST).wrapBlock(type)
      else editor.setBlocks(LIST_ITEM).wrapBlock(type)
    }
  }

  insertMath = ({ value }) => {
    this.setState({ showBlockInput: false }, () => {
      if (value !== '') {
        const { editor } = this.props
        editor.focus()
        const isVoidAnchorBlock = [MATH, TEX, IMAGE, VIDEO, AUDIO].includes(editor.value.anchorBlock.type)
        if (isVoidAnchorBlock) {
          editor.insertBlock(PARAGRAPH)
        }
        editor.insertInline({ type: MATH, data: Map({ value, block: false }) })
      }
    })
  }

  insertFile = async () => {
    const { withUpload, editor, upload } = this.props
    try {
      const file = this.fileInput.files[0]
      let data
      const type = file.type.split('/')[0]
      switch (type) {
        case IMAGE:
          const imageData = await getImageData(file)
          data = { ...imageData, loading: withUpload }
          break
        case VIDEO:
          const videoData = await getVideoData(file)
          data = { ...videoData, loading: true }
          break
        case AUDIO:
          data = { localSrc: URL.createObjectURL(file), loading: true }
          break
        default:
          throw new Error('Tipo de arquivo desconhecido')
      }
      const insertedBlock = insertBlock({ type, data: Map({ ...data, align: ALIGN_CENTER }) }, editor)
      if (withUpload) upload(file, insertedBlock)
    } catch (error) {
      console.error('Erro ao inserir ou enviar o arquivo', error)
    }
    this.fileInput.value = ''
  }

  insertTable = (...dimensions) => {
    this.setState({ showBlockInput: false }, () => {
      insertBlock(initialTable(...dimensions), this.props.editor)
    })
  }

  renderButton = ({ formatting, type, Icon, tooltip }) => {
    const { classes, editor, hidden } = this.props
    if (hidden.includes(type)) return null
    const { activeMarks, blocks, anchorBlock } = editor.value
    const buttonProperties = () => {
      switch (formatting) {
        case MARK:
          return { isActive: activeMarks.some(item => item.type === type), onClick: this.onClickMark }
        case BLOCK:
          return { isActive: blocks.some(item => item.type === type), onClick: this.onClickBlock }
        case DATA:
          return {
            isActive: !!anchorBlock && Object.values(anchorBlock.data.toJS()).includes(type),
            onClick: this.onClickData,
          }
        default:
          console.warn('Ação inválida no toolbar do Editor')
          return { isActive: false, onClick: () => () => {} }
      }
    }
    const { isActive, onClick } = buttonProperties()
    return (
      <Button key={type} active={isActive} onClick={onClick(type)} tooltip={tooltip}>
        <Icon classes={{ root: classes.icon }} />
      </Button>
    )
  }

  render() {
    const { showBlockInput } = this.state
    return (
      <div ref={this.ref}>
        <div className={this.props.classes.root}>{actions.map(this.renderButton)}</div>
        {showBlockInput && this.blockInput}
        <input type="file" style={{ display: 'none' }} onChange={this.insertFile} />
      </div>
    )
  }
}
