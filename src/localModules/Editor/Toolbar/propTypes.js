import PropTypes from 'prop-types'

export const propTypes = {
  classes: PropTypes.object.isRequired,
  activeMarks: PropTypes.array.isRequired,
  activeBlocks: PropTypes.array.isRequired,
  onClickMark: PropTypes.func.isRequired,
  onClickBlock: PropTypes.func.isRequired,
  upload: PropTypes.func.isRequired,
  withUpload: PropTypes.bool.isRequired,
}
