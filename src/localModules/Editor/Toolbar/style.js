export const style = () => ({
  root: {
    display: 'flex',
    justifyContent: 'flex-end',
    left: '0px !important',
    flexWrap: 'wrap',
    borderBottom: '2px solid rgba(0, 0, 0, 0.15)',
    padding: 5,
  },
  button: {
    minWidth: 0,
    minHeight: 0,
    padding: '2px 5px',
    color: 'rgba(0, 0, 0, 0.45)',
    '&:not(:last-child)': {
      marginRight: '2px',
    },
    '&:hover': {
      backgroundColor: 'rgba(0, 0, 0, 0.05)',
    },
  },
  icon: {
    width: '24px',
  },
  mathEditor: {
    position: 'absolute',
    right: 0,
    top: 0,
    zIndex: 2,
  },
})
