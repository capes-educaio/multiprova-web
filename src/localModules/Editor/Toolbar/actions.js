import FormatBold from '@material-ui/icons/FormatBold'
import FormatItalic from '@material-ui/icons/FormatItalic'
import FormatUnderlined from '@material-ui/icons/FormatUnderlined'
import Code from '@material-ui/icons/Code'
import FormatListBulleted from '@material-ui/icons/FormatListBulleted'
import FormatListNumbered from '@material-ui/icons/FormatListNumbered'
import FormatQuote from '@material-ui/icons/FormatQuote'
import Functions from '@material-ui/icons/Functions'
import MonetizationOn from '@material-ui/icons/MonetizationOn'
import InsertPhoto from '@material-ui/icons/InsertPhoto'
import BorderAll from '@material-ui/icons/BorderAll'
import KeyboardTab from '@material-ui/icons/KeyboardTab'
import FormatAlignLeft from '@material-ui/icons/FormatAlignLeft'
import FormatAlignRight from '@material-ui/icons/FormatAlignRight'
import FormatAlignJustify from '@material-ui/icons/FormatAlignJustify'
import FormatAlignCenter from '@material-ui/icons/FormatAlignCenter'
import CheckBoxOutlineBlank from '@material-ui/icons/CheckBoxOutlineBlank'
import Remove from '@material-ui/icons/Remove'
import Movie from '@material-ui/icons/Movie'
import MusicVideo from '@material-ui/icons/MusicVideo'
import Subtitles from '@material-ui/icons/Subtitles'

import { SuperscriptIcon, SubscriptIcon } from 'localModules/Editor/utils/icons'

import {
  MARK,
  BLOCK,
  BOLD,
  ITALIC,
  UNDERLINE,
  SOB,
  SUB,
  CODE,
  BULLETED_LIST,
  NUMBERED_LIST,
  BLOCK_QUOTE,
  TAB,
  MATH,
  IMAGE,
  TABLE,
  TEX,
  ParagraphAboveIcon,
  ParagraphBelowIcon,
  PARAGRAPH_BELOW,
  PARAGRAPH_ABOVE,
  DATA,
  ALIGN_LEFT,
  ALIGN_CENTER,
  ALIGN_RIGHT,
  JUSTIFY,
  CONTAINER,
  HORIZONTAL_RULER,
  AUDIO,
  VIDEO,
  CAPTION,
} from '../utils'

export const actions = [
  { formatting: MARK, type: BOLD, Icon: FormatBold, tooltip: 'Negrito' },
  { formatting: MARK, type: ITALIC, Icon: FormatItalic, tooltip: 'Itálico' },
  { formatting: MARK, type: UNDERLINE, Icon: FormatUnderlined, tooltip: 'Sublinhado' },
  { formatting: MARK, type: SOB, Icon: SuperscriptIcon, tooltip: 'Sobrescrito' },
  { formatting: MARK, type: SUB, Icon: SubscriptIcon, tooltip: 'Subescrito' },
  { formatting: MARK, type: CODE, Icon: Code, tooltip: 'Código' },
  { formatting: DATA, type: ALIGN_LEFT, Icon: FormatAlignLeft, tooltip: 'À esquerda' },
  { formatting: DATA, type: ALIGN_CENTER, Icon: FormatAlignCenter, tooltip: 'Centralizar' },
  { formatting: DATA, type: ALIGN_RIGHT, Icon: FormatAlignRight, tooltip: 'À direita' },
  { formatting: DATA, type: JUSTIFY, Icon: FormatAlignJustify, tooltip: 'Justificar' },
  { formatting: BLOCK, type: TAB, Icon: KeyboardTab, tooltip: 'Recuo' },
  { formatting: BLOCK, type: BLOCK_QUOTE, Icon: FormatQuote, tooltip: 'Citação' },
  { formatting: BLOCK, type: BULLETED_LIST, Icon: FormatListBulleted, tooltip: 'Lista' },
  { formatting: BLOCK, type: NUMBERED_LIST, Icon: FormatListNumbered, tooltip: 'Enumeração' },
  { formatting: BLOCK, type: CAPTION, Icon: Subtitles, tooltip: 'Legenda' },
  { formatting: BLOCK, type: PARAGRAPH_BELOW, Icon: ParagraphBelowIcon, tooltip: 'Parágrafo abaixo' },
  { formatting: BLOCK, type: PARAGRAPH_ABOVE, Icon: ParagraphAboveIcon, tooltip: 'Parágrafo acima' },
  { formatting: BLOCK, type: HORIZONTAL_RULER, Icon: Remove, tooltip: 'Linha' },
  { formatting: BLOCK, type: CONTAINER, Icon: CheckBoxOutlineBlank, tooltip: 'Grupo' },
  { formatting: BLOCK, type: MATH, Icon: Functions, tooltip: 'Equações e símbolos' },
  { formatting: BLOCK, type: TEX, Icon: MonetizationOn, tooltip: 'Latex' },
  { formatting: BLOCK, type: TABLE, Icon: BorderAll, tooltip: 'Tabela' },
  { formatting: BLOCK, type: IMAGE, Icon: InsertPhoto, tooltip: 'Imagem' },
  { formatting: BLOCK, type: AUDIO, Icon: MusicVideo, tooltip: 'Áudio' },
  { formatting: BLOCK, type: VIDEO, Icon: Movie, tooltip: 'Vídeo' },
]
