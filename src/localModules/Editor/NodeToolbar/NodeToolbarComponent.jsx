import React, { Component } from 'react'
import classnames from 'classnames'

import Popper from '@material-ui/core/Popper'
import Fade from '@material-ui/core/Fade'
import Paper from '@material-ui/core/Paper'

import { Button } from '../Button'
import { propTypes } from './propTypes'
import { tableCellAction } from '../Nodes/TableCell'
import {
  displayAction,
  removeAction,
  editAction,
  tableCellActions,
  borderAction,
  containerActions,
  HorizontalRulerAction,
} from './actions'
import {
  REMOVE,
  EDIT,
  BLOCK,
  BORDER,
  TABLE,
  REMOVE_COLUMN,
  REMOVE_ROW,
  TABLE_CELL,
  PARAGRAPH_ABOVE,
  PARAGRAPH_BELOW,
  CONTAINER,
  ALIGN_CENTER,
  ALIGN_RIGHT,
  ALIGN_LEFT,
  insertParagraphBelowContainer,
  insertParagraphAboveContainer,
  HORIZONTAL_RULER,
  LINE_STYLE,
  removeNode,
  toggleNodeType,
  DOTTED,
  SOLID,
  HIDDEN,
  DASHED,
  DOUBLE,
  toggleData,
} from '../utils'

const INVALID_ACTION = 'Ação inválida no toolbar flutuante do Editor'

export class NodeToolbarComponent extends Component {
  static propTypes = propTypes
  isTable
  isContainer

  constructor(props) {
    super(props)
    const { node } = props
    this.isTable = node.type === TABLE
    this.isContainer = [TABLE, CONTAINER].includes(node.type)
  }

  get actives() {
    const { node, editor } = this.props
    const { value } = editor
    const { align, block } = node.data.toJS()
    const actives = []
    if (align) actives.push(align)
    if (block) actives.push(BLOCK)
    if (this.isTable && value.anchorBlock) {
      const cellVerticalAlign = value.document.getParent(value.anchorBlock.key).data.get('verticalAlign')
      actives.push(cellVerticalAlign)
    }
    return actives
  }

  get disabled() {
    const { node, editor } = this.props
    const { value } = editor
    const disabled = []
    if (this.isTable && value.anchorBlock) {
      const cell = value.document.getParent(value.anchorBlock.key)
      if (cell && cell.type === TABLE_CELL) {
        const isLoneColumn = value.document.getParent(cell.key).nodes.size === 1
        const isLoneRow = node.nodes.size === 1
        if (isLoneColumn) disabled.push(REMOVE_COLUMN)
        if (isLoneRow) disabled.push(REMOVE_ROW)
      }
    }
    return disabled
  }

  get actions() {
    const { node } = this.props
    const { data } = node
    const isHorizontalRuler = node.type === HORIZONTAL_RULER
    const withEdit = data.has('value')
    const withDisplay = data.has('block')
    const primary = [removeAction]
    const secondary = []
    if (this.isTable) secondary.push(...tableCellActions)
    if (withDisplay) primary.unshift(displayAction)
    if (this.isContainer) primary.unshift(borderAction, ...containerActions)
    if (withEdit) primary.unshift(editAction)
    if (isHorizontalRuler) primary.unshift(HorizontalRulerAction)
    return [primary, secondary]
  }

  containerAction = action => {
    const { editor, node, reopen, setNode } = this.props
    switch (action) {
      case PARAGRAPH_BELOW:
        insertParagraphBelowContainer(node, editor)
        break
      case PARAGRAPH_ABOVE: {
        insertParagraphAboveContainer(node, editor)
        break
      }
      case ALIGN_CENTER:
      case ALIGN_RIGHT:
      case ALIGN_LEFT:
        setNode({ align: action })
        reopen()
        break
      default:
        console.warn(INVALID_ACTION)
    }
  }

  command = action => {
    const { node, editor, edit } = this.props
    switch (action) {
      case REMOVE:
        removeNode(node, editor)
        break
      case EDIT:
        edit()
        break
      default:
        console.warn(INVALID_ACTION)
    }
  }

  toggle = action => {
    const { node, editor } = this.props
    switch (action) {
      case BLOCK:
        toggleNodeType(node, editor)
        break
      case BORDER: {
        if ([TABLE, CONTAINER].includes(node.type)) toggleData(BORDER, [SOLID, DOTTED, HIDDEN], node, editor)
        break
      }
      case LINE_STYLE:
        toggleData(LINE_STYLE, [SOLID, DASHED, DOUBLE, DOTTED], node, editor)
        break
      default:
        console.warn(INVALID_ACTION)
    }
  }

  onChange = action => {
    const { node, editor } = this.props
    const { command, toggle, tableCell, container } = action
    if (!!command) this.command(command)
    else if (!!toggle) this.toggle(toggle)
    else if (this.isTable && !!tableCell) tableCellAction(node, editor, tableCell)
    else if (this.isContainer && !!container) this.containerAction(container)
  }

  render() {
    const { classes, popperProps } = this.props
    return (
      <Popper {...popperProps} transition>
        {({ TransitionProps }) => (
          <Fade {...TransitionProps} timeout={350}>
            <div className={classnames(classes.toolbar, 'toolbar')}>
              <Paper>
                <div className={classes.actionGroups}>
                  {this.actions.map(type =>
                    !type.length ? null : (
                      <div key={type}>
                        {type.map(({ action, Icon, tooltip }) => {
                          const actionValue = Object.values(action)[0]
                          const isActive = this.actives.includes(actionValue)
                          const isDisabled = this.disabled.includes(actionValue)
                          return isDisabled ? null : (
                            <Button
                              key={actionValue}
                              active={isActive}
                              tooltip={tooltip}
                              onClick={event => {
                                event.preventDefault()
                                if (!!action.toggle || !isActive) this.onChange(action)
                              }}
                            >
                              <Icon classes={{ root: classes.icon }} />
                            </Button>
                          )
                        })}
                      </div>
                    ),
                  )}
                </div>
              </Paper>
            </div>
          </Fade>
        )}
      </Popper>
    )
  }
}
