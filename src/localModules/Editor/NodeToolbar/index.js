import { withStyles } from '@material-ui/core/styles'

import { NodeToolbarComponent } from './NodeToolbarComponent'
import { style } from './style'

export const NodeToolbar = withStyles(style)(NodeToolbarComponent)
