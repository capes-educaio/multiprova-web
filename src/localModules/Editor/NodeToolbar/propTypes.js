import PropTypes from 'prop-types'

export const propTypes = {
  classes: PropTypes.object.isRequired,
  popperProps: PropTypes.object.isRequired,
  editor: PropTypes.object.isRequired,
  node: PropTypes.object.isRequired,
  reopen: PropTypes.func.isRequired,
  edit: PropTypes.func.isRequired,
  setNode: PropTypes.func.isRequired,
}
