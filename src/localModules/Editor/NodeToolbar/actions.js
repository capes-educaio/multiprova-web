import FormatAlignLeft from '@material-ui/icons/FormatAlignLeft'
import FormatAlignCenter from '@material-ui/icons/FormatAlignCenter'
import FormatAlignRight from '@material-ui/icons/FormatAlignRight'
import Delete from '@material-ui/icons/Delete'
import Edit from '@material-ui/icons/Edit'
import VerticalAlignBottom from '@material-ui/icons/VerticalAlignBottom'
import VerticalAlignTop from '@material-ui/icons/VerticalAlignTop'
import VerticalAlignCenter from '@material-ui/icons/VerticalAlignCenter'
import LineStyle from '@material-ui/icons/LineStyle'
import BorderStyle from '@material-ui/icons/BorderStyle'

import {
  EDIT,
  REMOVE,
  BLOCK,
  BORDER,
  ALIGN_LEFT,
  ALIGN_CENTER,
  ALIGN_RIGHT,
  ADD_ROW_BELOW,
  ADD_ROW_ABOVE,
  ADD_COLUMN_LEFT,
  ADD_COLUMN_RIGHT,
  REMOVE_ROW,
  REMOVE_COLUMN,
  VERTICAL_ALIGN_TOP,
  VERTICAL_ALIGN_MIDDLE,
  VERTICAL_ALIGN_BOTTOM,
  PARAGRAPH_BELOW,
  PARAGRAPH_ABOVE,
  BlockIcon,
  AddColumnLeftIcon,
  AddColumnRightIcon,
  AddRowAboveIcon,
  AddRowBelowIcon,
  RemoveColumnIcon,
  RemoveRowIcon,
  ParagraphBelowIcon,
  ParagraphAboveIcon,
  LINE_STYLE,
} from '../utils'

export const removeAction = { action: { command: REMOVE }, Icon: Delete, tooltip: 'Excluir' }
export const displayAction = { action: { toggle: BLOCK }, Icon: BlockIcon, tooltip: 'Em bloco' }
export const editAction = { action: { command: EDIT }, Icon: Edit, tooltip: 'Editar' }
export const borderAction = { action: { toggle: BORDER }, Icon: BorderStyle, tooltip: 'Estilo' }
export const tableCellActions = [
  { action: { tableCell: VERTICAL_ALIGN_TOP }, Icon: VerticalAlignTop, tooltip: 'Alinhar acima' },
  { action: { tableCell: VERTICAL_ALIGN_MIDDLE }, Icon: VerticalAlignCenter, tooltip: 'Alinhar no meio' },
  { action: { tableCell: VERTICAL_ALIGN_BOTTOM }, Icon: VerticalAlignBottom, tooltip: 'Alinha abaixo' },
  { action: { tableCell: ADD_ROW_BELOW }, Icon: AddRowAboveIcon, tooltip: 'Linha abaixo' },
  { action: { tableCell: ADD_ROW_ABOVE }, Icon: AddRowBelowIcon, tooltip: 'Linha acima' },
  { action: { tableCell: ADD_COLUMN_LEFT }, Icon: AddColumnLeftIcon, tooltip: 'Coluna à esquerda' },
  { action: { tableCell: ADD_COLUMN_RIGHT }, Icon: AddColumnRightIcon, tooltip: 'Coluna à direita' },
  { action: { tableCell: REMOVE_ROW }, Icon: RemoveRowIcon, tooltip: 'Excluir linha' },
  { action: { tableCell: REMOVE_COLUMN }, Icon: RemoveColumnIcon, tooltip: 'Excluir coluna' },
]
export const containerActions = [
  { action: { container: ALIGN_LEFT }, Icon: FormatAlignLeft, tooltip: 'Alinhar à esquerda' },
  { action: { container: ALIGN_CENTER }, Icon: FormatAlignCenter, tooltip: 'Centralizar' },
  { action: { container: ALIGN_RIGHT }, Icon: FormatAlignRight, tooltip: 'Alinhar à direita' },
  { action: { container: PARAGRAPH_BELOW }, Icon: ParagraphBelowIcon, tooltip: 'Parágrafo abaixo' },
  { action: { container: PARAGRAPH_ABOVE }, Icon: ParagraphAboveIcon, tooltip: 'Parágrafo acima' },
]
export const HorizontalRulerAction = { action: { toggle: LINE_STYLE }, Icon: LineStyle, tooltip: 'Estilo' }
