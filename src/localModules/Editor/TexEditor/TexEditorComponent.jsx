import React, { Component } from 'react'

import Popper from '@material-ui/core/Popper'
import TextField from '@material-ui/core/TextField'

import { propTypes } from './propTypes'
import { isShiftEnterHotkey } from '../utils'

export class TexEditorComponent extends Component {
  static propTypes = propTypes

  ref = React.createRef()
  timeout

  constructor(props) {
    super(props)
    this.state = { value: props.value, open: true }
  }

  reopen = () => {
    const { open } = this.state
    if (open) {
      this.setState({ open: false }, () => {
        this.setState({ open: true })
      })
    }
  }

  onChange = event => {
    this.setState({ value: event.target.value })
  }

  onBlur = () => {
    clearTimeout(this.timeout)
    this.props.onExit(this.state.value)
  }

  sendValue = () => {
    const { value } = this.state
    this.props.onChange(value)
    this.reopen()
  }

  onKeyDown = event => {
    clearTimeout(this.timeout)
    if ((!isShiftEnterHotkey(event) && event.key === 'Enter') || event.key === 'Escape') {
      event.preventDefault()
      this.prop.onExit(event.target.value)
    } else {
      this.timeout = setTimeout(this.sendValue, 1000)
    }
  }

  render() {
    const { classes, popperProps } = this.props
    const { value, open } = this.state
    return (
      <Popper
        {...popperProps}
        open={open}
        keepMounted
        modifiers={{
          flip: { enabled: false },
          preventOverflow: {
            enabled: true,
            boundariesElement: 'scrollParent',
          },
        }}
      >
        <div ref={this.ref} className={classes.root}>
          <TextField
            value={value}
            onChange={this.onChange}
            onBlur={this.onBlur}
            onKeyDown={this.onKeyDown}
            variant="outlined"
            rows={3}
            rowsMax={15}
            autoFocus
            multiline
            fullWidth
            InputProps={{ classes: { root: classes.textField, notchedOutline: classes.notchedOutline } }}
          />
        </div>
      </Popper>
    )
  }
}
