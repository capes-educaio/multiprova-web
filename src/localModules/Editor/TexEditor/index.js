import { withStyles } from '@material-ui/core/styles'

import { TexEditorComponent } from './TexEditorComponent'
import { style } from './style'

export const TexEditor = withStyles(style)(TexEditorComponent)
