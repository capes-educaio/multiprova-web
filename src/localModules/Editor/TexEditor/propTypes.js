import PropTypes from 'prop-types'

export const propTypes = {
  value: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  onExit: PropTypes.func.isRequired,
  classes: PropTypes.object.isRequired,
}
