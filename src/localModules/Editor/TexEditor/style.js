import { FOCUS_COLOR } from '../utils'

export const style = () => ({
  root: { background: 'white', width: 320 },
  textField: { fontFamily: 'monospace' },
  notchedOutline: {
    borderWidth: '1px',
    borderColor: `${FOCUS_COLOR} !important`,
  },
})
