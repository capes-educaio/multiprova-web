import { Map } from 'immutable'

import { BLOCK } from '../utils'

export const addNode = ({ key, type, nodes, data }, editor, ...insert) => {
  editor.replaceNodeByKey(key, {
    key,
    type,
    object: BLOCK,
    nodes: nodes.insert(...insert),
    data: Map(data.toJS()),
  })
}
