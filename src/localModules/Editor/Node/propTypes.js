import PropTypes from 'prop-types'

export const propTypes = {
  classes: PropTypes.object.isRequired,
  isFocused: PropTypes.bool.isRequired,
  node: PropTypes.any,
  editor: PropTypes.any,
}
