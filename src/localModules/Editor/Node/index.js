import { withStyles } from '@material-ui/core/styles'

import { NodeComponent } from './NodeComponent'
import { style } from './style'

export const Node = withStyles(style)(NodeComponent)
