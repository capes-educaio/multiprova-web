import React, { Component } from 'react'
import classnames from 'classnames'

import { NodeToolbar } from '../NodeToolbar'
import { Loading } from '../Loading'
import { MathEditor } from '../MathEditor'
import { TexEditor } from '../TexEditor'
import {
  MATH,
  IMAGE,
  TABLE,
  ALIGN_CENTER,
  TEX,
  CONTAINER,
  HORIZONTAL_RULER,
  removeNode,
  VIDEO,
  AUDIO,
  SOLID,
  DOTTED,
  HIDDEN,
} from '../utils'
import { Resizable } from '../Resizable'
import { Image, Math, Table, Video, Audio } from '../Nodes'
import { propTypes } from './propTypes'
import { HorizontalRuler } from '../Nodes/HorizontalRuler'

export class NodeComponent extends Component {
  static propTypes = propTypes

  ref = React.createRef()

  state = {
    anchorEl: null,
    openToolbar: true,
    blockEditor: false,
  }

  componentDidMount() {
    const isAlignable = this.hasData('align')
    const nodeElement = this.ref.current
    this.setState({ anchorEl: isAlignable ? nodeElement.firstElementChild : nodeElement }, () => {
      if (this.data.edit) this.edit()
      else this.reopenToolbar()
    })
  }

  get data() {
    return this.props.node.data.toJS()
  }

  hasData = property => this.props.node.data.has(property)

  reopenToolbar = () => {
    const { openToolbar } = this.state
    if (openToolbar) {
      this.setState({ openToolbar: false })
      setTimeout(() => {
        this.setState({ openToolbar: true })
      }, 200)
    }
  }

  edit = () => {
    const { classes, node, editor } = this.props
    const { anchorEl } = this.state
    const { value, edit } = this.data
    const props = { value }
    switch (node.type) {
      case MATH:
        editor.blur()
        props.onChange = properties => {
          this.setNode(properties)
          this.setState({ blockEditor: false })
          editor.focus()
        }
        props.onCancel = () => {
          this.setState({ blockEditor: false })
        }
        this.blockEditor = <MathEditor {...props} classes={{ root: classes.mathEditor }} />
        break
      case TEX:
        editor.blur()
        props.onChange = value => {
          this.setNode({ value })
        }
        props.onExit = value => {
          if (!value) removeNode(node, editor)
          else {
            this.setNode(edit ? { value, edit: false } : { value })
            this.setState({ blockEditor: false, openToolbar: true })
            editor.focus()
          }
        }
        this.blockEditor = <TexEditor {...props} popperProps={{ anchorEl }} />
        this.setState({ openToolbar: false })
        break
      default:
        console.warn('Bloco do Editor não editável')
        this.blockEditor = null
        return
    }
    this.setState({ blockEditor: true })
  }

  setNode = properties => {
    const { editor, node } = this.props
    editor.setNodeByKey(node.key, { data: { ...this.data, ...properties } })
  }

  get node() {
    const { node, children, isFocused, editor, classes } = this.props
    const resizableProps = ({ width, height, align }) => ({
      size: { width, height },
      isFocused,
      onResize: size => {
        this.setNode(size)
        this.reopenToolbar()
      },
      depth: align === ALIGN_CENTER ? 1 : 2,
    })
    switch (node.type) {
      case IMAGE: {
        const { src, localSrc, width, height, align, loading } = this.data
        return (
          <Resizable {...resizableProps({ width, height, align })} dimensions>
            <Loading active={loading}>
              <Image src={localSrc ? localSrc : src} />
            </Loading>
          </Resizable>
        )
      }
      case VIDEO: {
        const { src, localSrc, width, height, align, loading } = this.data
        return (
          <Resizable {...resizableProps({ width, height, align })} dimensions>
            <Loading active={loading}>
              <Video src={localSrc ? localSrc : src} />
            </Loading>
          </Resizable>
        )
      }
      case AUDIO: {
        const { src, localSrc, loading } = this.data
        return (
          <Loading active={loading} classes={{ root: classes.audioLoading }}>
            <Audio
              src={localSrc ? localSrc : src}
              focus={() => {
                editor.focus().moveToStartOfNode(node)
              }}
              focused={isFocused}
            />
          </Loading>
        )
      }
      case MATH:
      case TEX: {
        const { value, block } = this.data
        return <Math {...{ value, block, isFocused }} />
      }
      case TABLE: {
        const { border } = this.data
        return <Table {...{ border, isFocused }}>{children}</Table>
      }
      case CONTAINER: {
        const { border, width, height, align } = this.data
        return (
          <div style={{ display: 'table' }}>
            <div style={{ display: 'table-row' }}>
              <Resizable
                classes={{
                  root: classnames(classes.container, {
                    solid: border === SOLID,
                    dotted: border === DOTTED,
                    hidden: border === HIDDEN,
                  }),
                }}
                {...resizableProps({ width, height, align })}
                lockAspectRatio={false}
                minHeight={24}
                minWidth={30}
                adaptive={false}
              >
                {children}
              </Resizable>
            </div>
          </div>
        )
      }
      case HORIZONTAL_RULER: {
        const { lineStyle } = this.data
        return <HorizontalRuler {...{ lineStyle, isFocused }} />
      }
      default:
        console.warn('Bloco personalizado do Editor desconhecido')
        return null
    }
  }

  get wrapperClasses() {
    const { classes } = this.props
    const { align, block } = this.data
    return classnames(classes.wrapper, {
      [ALIGN_CENTER]: this.hasData('block') && block,
      [align]: !!align,
    })
  }

  get rootClasses() {
    const { classes, isFocused, node } = this.props
    const { block } = this.data
    return classnames(classes.root, {
      inline: !this.hasData('block') ? false : !block,
      focused: isFocused,
      blockMargin: [IMAGE, TABLE, AUDIO, VIDEO, CONTAINER].includes(node.type),
      void: [IMAGE, MATH, TEX, HORIZONTAL_RULER].includes(node.type),
    })
  }

  render() {
    const { isFocused, node, editor, attributes } = this.props
    const { openToolbar, anchorEl, blockEditor } = this.state
    const hasAlign = this.hasData('align')
    const hasBlock = this.hasData('block')
    return (
      <div ref={this.ref} {...attributes} className={this.rootClasses}>
        {hasAlign || (hasBlock && this.data.block) ? <div className={this.wrapperClasses}>{this.node}</div> : this.node}
        <NodeToolbar
          popperProps={{ open: isFocused && openToolbar, anchorEl }}
          node={node}
          editor={editor}
          reopen={this.reopenToolbar}
          edit={this.edit}
          setNode={this.setNode}
        />
        {blockEditor && this.blockEditor}
      </div>
    )
  }
}
