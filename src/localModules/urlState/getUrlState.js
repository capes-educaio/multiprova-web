import { history } from 'utils/history'

export const getUrlState = () => {
  const { state, pathname } = history.location
  const hidden = state ? state.hidden : undefined
  const string = state ? state.string : undefined
  return { pathname, hidden, string }
}
