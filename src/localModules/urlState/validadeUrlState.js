import { validarUrlState } from './validarUrlState'
import { replaceState } from './replaceState'

export const validadeUrlState = history => {
  const { isValid, defaultState } = validarUrlState(history.location)
  if (!isValid) {
    console.warn('Sobreescrevendo urlState com padrão.')
    replaceState(defaultState)
  }
  return isValid
}
