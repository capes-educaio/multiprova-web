import { history } from 'utils/history'

var retornoLogin = null

export const setRetornoLogin = () => {
  if (history.length > 0) {
    retornoLogin = { ...history.location }
  } else retornoLogin = null
}

export const goToRetornoLogin = () => {
  history.push(retornoLogin)
}

export const getRetornoLogin = () => {
  return retornoLogin
}

export const clearRetornoLogin = () => (retornoLogin = null)
