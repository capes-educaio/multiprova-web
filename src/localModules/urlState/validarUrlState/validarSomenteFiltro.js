export const validarSomenteFiltro = ({ hidden, string }) => {
  let validation = {
    isValid: true,
    defaultState: {
      hidden: {},
      string: {},
    },
  }
  const hiddenPodeIncluirSomente = ['filtro', 'inputsDoFiltro']
  for (let prop in hidden) {
    if (!hiddenPodeIncluirSomente.includes(prop)) validation.isValid = false
  }
  for (let temAlgoDentro in string) validation.isValid = !temAlgoDentro
  return validation
}
