import { getEmptyUrlState } from './getEmptyUrlState'
import { validarFormComPassos } from './validarFormComPassos'
import { validarStateVazio } from './validarStateVazio'
import { validarUrlQuestoes } from './validarUrlQuestoes'
import { validarSomenteFiltro } from './validarSomenteFiltro'

export const validarUrlState = location => {
  let { state, pathname } = location
  let validation = {
    isValid: true,
    defaultState: getEmptyUrlState(),
  }
  if (typeof state !== 'object' || typeof state.hidden !== 'object' || typeof state.string !== 'object') {
    validation.isValid = false
    return validation
  }
  const rootPath = pathname.split('/')[1]
  switch (rootPath) {
    case 'questoes':
      validation = validarSomenteFiltro(state)
      break
    case 'provas':
      validation = validarSomenteFiltro(state)
      break
    case 'turmas':
      validation = validarSomenteFiltro(state)
      break
    case 'eventos':
      validation = validarSomenteFiltro(state)
      break
    case 'lixeira':
      validation = validarSomenteFiltro(state)
      break
    case 'usuarios':
      validation = validarSomenteFiltro(state)
      break
    case 'tags':
      validation = validarSomenteFiltro(state)
      break
    case 'prova':
      validation = validarFormComPassos(state, 3)
      break
    case 'questao':
      validation = validarUrlQuestoes(state)
      break
    default:
      validation = validarStateVazio(state)
  }
  return validation
}
