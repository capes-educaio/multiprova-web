export const validarStateVazio = ({ hidden, string }) => {
  let validation = {
    isValid: true,
    defaultState: {
      hidden: {},
      string: {},
    },
  }
  for (let temAlgoDentro in hidden) validation.isValid = !temAlgoDentro
  for (let temAlgoDentro in string) validation.isValid = !temAlgoDentro
  return validation
}
