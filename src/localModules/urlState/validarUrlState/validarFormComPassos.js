export const validarFormComPassos = ({ hidden }, quantidadePassos) => {
  let validation = {
    isValid: true,
    defaultState: {
      hidden: {
        activeStep: 0,
        isOpenModal: false,
        idsCardsSelecionados: [],
      },
      string: {},
    },
  }
  const { isOpenModal, activeStep, idsCardsSelecionados } = hidden
  if (typeof isOpenModal !== 'boolean') validation.isValid = false
  else if (activeStep < 0 || activeStep > quantidadePassos - 1) validation.isValid = false
  else if (!Array.isArray(idsCardsSelecionados)) validation.isValid = false
  return validation
}
