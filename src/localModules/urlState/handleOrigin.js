import { history } from 'utils/history'

var originLocation = null

export const registerOrigin = () => {
  if (history.length > 0) {
    originLocation = { ...history.location }
    delete originLocation.key
  } else originLocation = null
}

export const goToOrigin = () => {
  if (originLocation) {
    history.push(originLocation)
  }
  return originLocation
}

export const getOrigin = () => {
  return originLocation
}

export const clearOrigin = () => (originLocation = null)
