import qs from 'qs'

import { history } from 'utils/history'

import { changeUrlState } from './changeUrlState'

export const updateUrlState = ({ pathname, hidden, string }) => {
  let { state } = history.location
  if (!state) state = {}
  const newUrl = {
    pathname: history.location.pathname,
    state: {
      hidden: state.hidden,
      string: state.string,
    },
  }
  if (pathname) newUrl.pathname = pathname
  if (hidden) newUrl.state.hidden = { ...state.hidden, ...hidden }
  if (string) {
    newUrl.state.string = { ...state.string, ...string }
    newUrl.search = qs.stringify(newUrl.state.string)
  }
  changeUrlState(newUrl)
}
