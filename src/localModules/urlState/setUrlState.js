import qs from 'qs'

import { history } from 'utils/history'

import { changeUrlState } from './changeUrlState'

export const setUrlState = ({ pathname, hidden, string }) => {
  const newUrl = {
    pathname: pathname ? pathname : history.location.pathname,
    state: {
      hidden: hidden,
      string: string,
    },
  }
  if (hidden === null || hidden === undefined) newUrl.state.hidden = {}
  if (string === null || string === undefined) newUrl.state.string = {}
  else newUrl.search = qs.stringify(newUrl.state.string)
  changeUrlState(newUrl)
}
