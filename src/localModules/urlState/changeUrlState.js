import { history } from 'utils/history'

import { listenUrlStateChange } from './listenUrlStateChange'
import { getUrlState } from './getUrlState'

export const changeUrlState = async newUrlState => {
  try {
    const oldUrlState = getUrlState()
    await listenUrlStateChange({ newUrlState, oldUrlState })
    history.push(newUrlState)
    return
  } catch (error) {
    return
  }
}
