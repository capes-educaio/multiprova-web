import PropTypes from 'prop-types'

export const propTypes = {
  children: PropTypes.node,
  accessor: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  label: PropTypes.node,
  helperText: PropTypes.string,
  validar: PropTypes.func,
  required: PropTypes.bool,
  onCampoObjetoContext: PropTypes.func,
  // style
  classes: PropTypes.object.isRequired,
}
