import React from 'react'
import { CampoObjetoComponent } from './CampoObjetoComponent'
import { compose } from 'redux'
import { withStyles } from '@material-ui/core/styles'

import { withCampoContext } from '../Campo/context'
import { Campo } from '../Campo'

import { style } from './style'
import { withCampoObjetoContext } from './context'

export const CampoObjeto = props => (
  <Campo defaultInitialValue={{}} {...props}>
    <CampoObjetoWrapped {...props} />
  </Campo>
)

export const CampoObjetoWrapped = compose(
  withStyles(style),
  withCampoObjetoContext,
  withCampoContext,
)(CampoObjetoComponent)
