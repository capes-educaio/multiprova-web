import React, { Component } from 'react'

import { initiateContextState } from 'utils/context'

import { CampoFrame } from '../CampoFrame'

import { CampoObjetoContext } from './context'
import { propTypes } from './propTypes'

export class CampoObjetoComponent extends Component {
  static propTypes = propTypes

  constructor(props) {
    super(props)
    const contextInitialValue = {
      updateValue: this._updateValue,
      value: props.campoContext.getInitialValue(),
      setValue: this._setValue,
    }
    initiateContextState(this, contextInitialValue)
    props.campoContext.validate(contextInitialValue.value, false)
    if (props.onCampoObjetoContext) props.onCampoObjetoContext(this.state.context)
  }

  componentWillUnmount = () => {
    this._valueUpdater.unsubscribe()
  }

  componentWillUnmount = () => {
    if (this.props.onCampoObjetoContext) this.props.onCampoObjetoContext(null)
  }

  _setValue = async value => {
    const { onChange, campoObjetoContext } = this.props
    value = Array.isArray(value) ? [...value] : { ...value }
    await this._updateContext({ value })
    if (onChange) onChange(value)
    if (campoObjetoContext) campoObjetoContext.updateValue(this.props.accessor, value)
    this.props.campoContext.validate(value)
  }

  _updateValue = async (accessor, valueField) => {
    this.state.context.value[accessor] = valueField
    this._setValue(this.state.context.value)
  }

  render() {
    return (
      <CampoObjetoContext.Provider value={this.state.context}>
        <CampoFrame {...this.props} />
      </CampoObjetoContext.Provider>
    )
  }
}
