import React from 'react'
import { withContext } from 'utils/context'

export const CampoObjetoContext = React.createContext()
export const withCampoObjetoContext = withContext(CampoObjetoContext, 'campoObjetoContext')
