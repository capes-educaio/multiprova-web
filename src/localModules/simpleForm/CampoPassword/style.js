export const style = theme => ({
  icon: {
    height: 20,
  },
  iconButton: {
    color: 'rgba(0, 0, 0, 0.5)',
    '& :hover': {
      color: 'rgba(0, 0, 0, 1)',
    },
  },
})
