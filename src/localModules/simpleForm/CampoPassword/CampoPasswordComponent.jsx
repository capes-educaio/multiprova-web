import React, { Component } from 'react'

import InputAdornment from '@material-ui/core/InputAdornment'

import Visibility from '@material-ui/icons/Visibility'
import VisibilityOff from '@material-ui/icons/VisibilityOff'

import { CampoText } from '../CampoText'

import { propTypes } from './propTypes'

export class CampoPasswordComponent extends Component {
  static propTypes = propTypes

  state = { mostrarSenha: false }

  _toggleMostrarSenha = () => this.setState({ mostrarSenha: !this.state.mostrarSenha })

  render() {
    const { classes, ...otherProps } = this.props
    const { mostrarSenha } = this.state
    const passwordProps = {
      type: mostrarSenha ? 'text' : 'password',
      InputProps: {
        autoComplete: 'current-password',
        endAdornment: (
          <InputAdornment position="end" onClick={this._toggleMostrarSenha} className={classes.iconButton}>
            {mostrarSenha ? <VisibilityOff className={classes.icon} /> : <Visibility className={classes.icon} />}
          </InputAdornment>
        ),
      },
    }
    return <CampoText {...otherProps} {...passwordProps} />
  }
}
