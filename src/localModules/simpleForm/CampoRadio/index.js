import React from 'react'
import { CampoRadioComponent } from './CampoRadioComponent'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'

import { mapStateToProps, mapDispatchToProps } from './redux'
import { style } from './style'
import { withCampoContext } from '../Campo/context'
import { withCampoObjetoContext } from '../CampoObjeto/context'
import { withSimpleFormContext } from '../SimpleForm/context'
import { Campo } from '../Campo'

export const CampoRadio = props => (
  <Campo {...props}>
    <CampoRadioWrapped {...props} />
  </Campo>
)

const CampoRadioWrapped = compose(
  withStyles(style),
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withCampoContext,
  withCampoObjetoContext,
  withSimpleFormContext,
)(CampoRadioComponent)
