import React, { Component } from 'react'

import { FormControl, InputLabel, FormHelperText, RadioGroup } from '@material-ui/core'
import { propTypes } from './propTypes'

export class CampoRadioComponent extends Component {
  static propTypes = propTypes

  constructor(props) {
    super(props)
    this.state = {
      value: props.campoContext.getInitialValue(),
    }
  }

  _onChange = event => {
    const { value } = event.target
    this.setState({ value })
    this.props.campoContext.onChange(value)
  }

  render() {
    const { classes, children, campoContext, campoObjetoContext, simpleFormContext, ...otherProps } = this.props
    return (
      <FormControl className={classes.field}>
        {campoContext.getHelperOrErrorText() && (
          <FormHelperText error={campoContext.getIsErrorShown() && campoContext.getHasError()}>
            {campoContext.getHelperOrErrorText()}
          </FormHelperText>
        )}
        {campoContext.getLabel() && <InputLabel>{campoContext.getLabel()}</InputLabel>}
        <RadioGroup {...otherProps} onChange={this._onChange} value={this.state.value}>
          {children}
        </RadioGroup>
      </FormControl>
    )
  }
}
