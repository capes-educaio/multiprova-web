import PropTypes from 'prop-types'

export const propTypes = {
  children: PropTypes.func,
  elements: PropTypes.func,
  extraField: PropTypes.bool,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
