import React, { Component, Fragment } from 'react'
import uid from 'uuid/v4'
import { SortableContainer, SortableElement } from 'react-sortable-hoc'

import { propTypes } from './propTypes'

const Wrapper = ({ children }) => <div>{children}</div>

const SortContainer = SortableContainer(Wrapper)
const SortElement = SortableElement(Wrapper)

const Container = ({ children, sortable, onSortEnd }) => {
  if (sortable)
    return (
      <SortContainer useDragHandle onSortEnd={onSortEnd}>
        {children}
      </SortContainer>
    )
  else return children
}

const Elements = ({ value, array, keys, sortable, elements }) => {
  return keys.map((key, index) => {
    const element = elements({
      arrayValue: value,
      elementValue: value[index],
      index,
      length: value.length,
      array,
      isLast: index === value.length - 1,
    })
    if (sortable) {
      return (
        <SortElement index={index} key={key}>
          {element}
        </SortElement>
      )
    } else {
      return <Fragment key={key}>{element}</Fragment>
    }
  })
}

export class CampoArrayComponent extends Component {
  static propTypes = propTypes
  _keys

  constructor(props) {
    super(props)
    this._keys = props.campoObjetoContext.value.map(() => uid())
    if (props.extraField) this._keys.push(uid())
  }

  get _array() {
    return {
      delete: index => {
        const { campoObjetoContext } = this.props
        this._keys.splice(index, 1)
        campoObjetoContext.value.splice(index, 1)
        campoObjetoContext.setValue(campoObjetoContext.value)
      },
      push: elementValue => {
        const { campoObjetoContext } = this.props
        campoObjetoContext.value.push(elementValue)
        this._keys.push(uid())
        campoObjetoContext.setValue(campoObjetoContext.value)
      },
      add: (index, element) => {
        const { campoObjetoContext } = this.props
        campoObjetoContext.value.splice(index, 0, element)
        this._keys.splice(index, 0, uid())
        campoObjetoContext.setValue(campoObjetoContext.value)
      },
      move: (from, to) => {
        const { campoObjetoContext } = this.props
        if (to >= campoObjetoContext.value.length) return
        const element = campoObjetoContext.value[from]
        const key = this._keys[from]
        campoObjetoContext.value.splice(from, 1)
        this._keys.splice(from, 1)
        campoObjetoContext.value.splice(to, 0, element)
        this._keys.splice(to, 0, key)
        campoObjetoContext.setValue(campoObjetoContext.value)
        return
      },
    }
  }

  render() {
    let { elements, campoObjetoContext, children, extraField, sortable, className } = this.props
    if (!Array.isArray(campoObjetoContext.value)) {
      console.warn('CampoArray precisa ter um array como valor. Valor recebido: ', campoObjetoContext.value)
      return null
    }
    const value = extraField ? [...campoObjetoContext.value, null] : campoObjetoContext.value
    return (
      <div className={className}>
        <Container sortable={sortable} onSortEnd={({ oldIndex, newIndex }) => this._array.move(oldIndex, newIndex)}>
          {children &&
            children({
              arrayValue: campoObjetoContext.value,
              length: campoObjetoContext.value.length,
              array: this._array,
              elements: (
                <Elements value={value} elements={elements} array={this._array} keys={this._keys} sortable={sortable} />
              ),
            })}
          {!children && (
            <Elements value={value} elements={elements} array={this._array} keys={this._keys} sortable={sortable} />
          )}
        </Container>
      </div>
    )
  }
}
