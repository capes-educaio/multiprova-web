import React from 'react'
import { CampoArrayComponent } from './CampoArrayComponent'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'

import { CampoObjeto } from '../CampoObjeto'
import { withCampoObjetoContext } from '../CampoObjeto/context'

import { mapStateToProps, mapDispatchToProps } from './redux'
import { style } from './style'

export const CampoArray = props => (
  <CampoObjeto {...props}>
    <CampoArrayWrapped {...props} />
  </CampoObjeto>
)

export const CampoArrayWrapped = compose(
  withStyles(style),
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withCampoObjetoContext,
)(CampoArrayComponent)
