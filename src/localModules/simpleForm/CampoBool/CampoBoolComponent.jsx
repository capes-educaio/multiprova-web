import React, { Component } from 'react'

import Switch from '@material-ui/core/Switch'
import Checkbox from '@material-ui/core/Checkbox'
import FormControlLabel from '@material-ui/core/FormControlLabel'

import { propTypes } from './propTypes'

export class CampoBoolComponent extends Component {
  static propTypes = propTypes

  constructor(props) {
    super(props)
    this.state = {
      value: props.campoContext.getInitialValue(),
    }
  }

  _onChange = (event, checked) => {
    this.setState({ value: checked })
    this.props.campoContext.onChange(checked)
  }

  render() {
    const { campoContext, campoObjetoContext, tipo, ...otherProps } = this.props
    const boolProps = { onChange: this._onChange, checked: Boolean(this.state.value), ...otherProps }
    return (
      <FormControlLabel
        label={campoContext.getLabel()}
        control={tipo === 'switch' ? <Switch {...boolProps} /> : <Checkbox {...boolProps} />}
      />
    )
  }
}
