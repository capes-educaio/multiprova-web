import React from 'react'
import { CampoBoolComponent } from './CampoBoolComponent'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'

import { mapStateToProps, mapDispatchToProps } from './redux'
import { style } from './style'
import { withCampoContext } from '../Campo/context'
import { withCampoObjetoContext } from '../CampoObjeto/context'
import { Campo } from '../Campo'

export const CampoBool = props => (
  <Campo {...props}>
    <CampoBoolWrapped {...props} />
  </Campo>
)

const CampoBoolWrapped = compose(
  withStyles(style),
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withCampoContext,
  withCampoObjetoContext,
)(CampoBoolComponent)
