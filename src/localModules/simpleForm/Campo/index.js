import { CampoComponent } from './CampoComponent'
import { compose } from 'redux'
import { withStyles } from '@material-ui/core/styles'

import { style } from './style'
import { withCampoObjetoContext } from '../CampoObjeto/context'
import { withSimpleFormContext } from '../SimpleForm/context'

export const Campo = compose(
  withStyles(style),
  withCampoObjetoContext,
  withSimpleFormContext,
)(CampoComponent)
