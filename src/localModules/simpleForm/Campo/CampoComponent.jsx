import React, { Component } from 'react'
import uid from 'uuid/v4'

import { initiateContextState } from 'utils/context'
import { esperar } from 'utils/esperar'

import { CampoContext } from './context'
import { propTypes } from './propTypes'

export class CampoComponent extends Component {
  static propTypes = propTypes
  static defaultProps = {
    defaultInitialValue: '',
    strings: {
      campoObrigatorio: 'Campo obrigatório',
    },
  }

  _fieldId = uid()

  _updateInterval = 300

  _valueUpdater = {
    _lastUpdateTime: Date.now(),
    _valueBuffer: null,
    _unsubscribed: false,
    _updateQueued: false,
    _clearBuffer: () => {
      const buffer = this._valueUpdater._valueBuffer
      this._valueUpdater._valueBuffer = null
      return buffer
    },
    _update: value => {
      if (this._valueUpdater._unsubscribed) return
      this._valueUpdater._lastUpdateTime = Date.now()
      if (value === undefined) value = this._valueUpdater._clearBuffer()
      const { campoObjetoContext } = this.props
      if (this.props.onChange) this.props.onChange(value)
      if (campoObjetoContext) return campoObjetoContext.updateValue(this.props.accessor, value)
      return
    },
    _queueUpdate: async value => {
      const timeSinceLastUpdate = Date.now() - this._valueUpdater._lastUpdateTime
      this._valueUpdater._valueBuffer = value
      if (!this._valueUpdater._updateQueued) {
        this._valueUpdater._updateQueued = true
        const timeLeftForNextUpdate = this._updateInterval - timeSinceLastUpdate
        await esperar(timeLeftForNextUpdate)
        this._valueUpdater._update()
        this._valueUpdater._updateQueued = false
      }
    },
    requestUpdate: async value => {
      const timeSinceLastUpdate = Date.now() - this._valueUpdater._lastUpdateTime
      if (timeSinceLastUpdate > this._updateInterval) this._valueUpdater._update(value)
      else this._valueUpdater._queueUpdate(value)
    },
    unsubscribe: () => (this._valueUpdater._unsubscribed = true),
  }

  constructor(props) {
    super(props)
    this._isValid = true
    const contextInitialValue = {
      isErrorShown: false,
      erros: [],
      getInitialValue: this._getInitialValue,
      getHasError: this._getHasError,
      getIsErrorShown: this._getIsErrorShown,
      getHelperOrErrorText: this._getHelperOrErrorText,
      getErrorText: this._getErrorText,
      getLabel: this._getLabel,
      validate: this._validate,
      onChange: this._onChange,
      clear: null,
      id: this._fieldId,
    }
    initiateContextState(this, contextInitialValue)
  }

  _getInitialValue = () => {
    const { campoObjetoContext, initialValue, accessor, defaultInitialValue } = this.props
    let value
    if (initialValue) {
      if (campoObjetoContext) value = campoObjetoContext.updateValue(accessor, initialValue)
      else value = initialValue
    } else if (campoObjetoContext && campoObjetoContext.value[accessor]) value = campoObjetoContext.value[accessor]
    else value = defaultInitialValue
    this._validate(value, false)
    return value
  }

  _getHasError = () => {
    return this.state.context.erros.length > 0
  }

  _getIsErrorShown = () => {
    return (
      this.state.context.isErrorShown || (this.props.simpleFormContext && this.props.simpleFormContext.isErrorShown)
    )
  }

  _getHelperOrErrorText = () => {
    if (this._getIsErrorShown() && this._getHasError()) return this._getErrorText()
    else return this.props.helperText
  }

  _getErrorText = () => {
    const { mensagensDeErro, strings } = this.props
    if (this._getHasError()) {
      const [error] = this.state.context.erros
      if (mensagensDeErro && mensagensDeErro[error.code]) return mensagensDeErro[error.code]
      else if (error.message) return error.message
      else if (strings[error.code]) return strings[error.code]
      else return ''
    } else return null
  }

  _getLabel = () => {
    let label
    if (this.props.label) label = this.props.label
    else label = ''
    return this.props.required ? `${label}*` : label
  }

  _validate = async (value, isErrorShown = true) => {
    let erros = []
    let isValid = true
    if (this.props.required) {
      const isValueEmpty = value === null || value === undefined || value === '' || value === '<p></p>'
      if (isValueEmpty) {
        erros.push({ code: 'campoObrigatorio' })
        isValid = false
      }
    }
    if (this.props.validar && erros.length < 1) {
      const validacao = await this.props.validar(value)
      erros = [...erros, ...validacao.erros]
      isValid = validacao.sucesso || isValid
    }
    const isValidChanged = isValid !== this._isValid
    const isErrorShownChanged = isErrorShown !== this.state.isErrorShown
    const errosChanged = JSON.stringify(erros) !== JSON.stringify(this.state.context.erros)
    this._isValid = isValid
    if ((this.props.required || this.props.validar) && (isErrorShownChanged || errosChanged)) {
      await this._updateContext({ erros, isErrorShown })
    }
    if (this.props.simpleFormContext && isValidChanged) {
      this.props.simpleFormContext.updateFieldValidity(this._fieldId, isValid)
    }
  }

  _onChange = value => this._valueUpdater.requestUpdate(value)

  _setErrorRef = ref => {
    if (!this.props.simpleFormContext) return
    if (ref) this.props.simpleFormContext.errorRefs[this._fieldId] = ref
    else delete this.props.simpleFormContext.errorRefs[this._fieldId]
    return
  }

  render() {
    return (
      <CampoContext.Provider value={this.state.context}>
        {this._getIsErrorShown() && this._getHasError() && (
          <div className={this.props.classes.error} ref={this._setErrorRef} />
        )}
        {this.props.children}
      </CampoContext.Provider>
    )
  }
}
