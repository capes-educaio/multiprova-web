import React from 'react'
import { withContext } from 'utils/context'

export const CampoContext = React.createContext()
export const withCampoContext = withContext(CampoContext, 'campoContext')
