import PropTypes from 'prop-types'

export const propTypes = {
  accessor: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  required: PropTypes.bool,
  label: PropTypes.string,
  mensagensDeErro: PropTypes.object,
  onChange: PropTypes.func,
  validar: PropTypes.func,
  onFocus: PropTypes.func,
  onBlur: PropTypes.func,
  onEnter: PropTypes.func,
  onKeyPress: PropTypes.func,
  async: PropTypes.bool,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
