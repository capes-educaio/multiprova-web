import React, { Component } from 'react'
import classnames from 'classnames'

import TextField from '@material-ui/core/TextField'

import { ValidarUrlAdornment } from './ValidarUrlAdornment'
import { propTypes } from './propTypes'

export class CampoTextComponent extends Component {
  static propTypes = propTypes

  constructor(props) {
    super(props)
    this.state = {
      value: props.campoContext.getInitialValue(),
      loading: false,
    }
    props.simpleFormContext.registerFieldClear(props.campoContext.id, () => this.setState({ value: '' }))
  }

  componentWillUnmount = () => {
    this.props.simpleFormContext.registerFieldClear(this.props.campoContext.id)
  }

  _onChange = event => {
    const { value } = event.target
    this.setState({ value })
    this.props.campoContext.onChange(value)
  }

  _handleOnKeyPress = e => {
    const { onKeyPress, onEnter } = this.props
    if (e.key === 'Enter' && onEnter) {
      e.preventDefault()
      onEnter()
    }
    if (onKeyPress) onKeyPress(e)
  }

  _onFocus = () => {
    if (this.props.onFocus) this.props.onFocus()
  }

  _onBlur = async () => {
    if (this.props.onBlur) this.props.onBlur()
    if (this.props.async) {
      this.setState({ loading: true })
      await this.props.campoContext.validate(this.state.value)
      this.setState({ loading: false })
    } else this.props.campoContext.validate(this.state.value)
  }

  render() {
    let {
      classes,
      id,
      campoContext,
      InputProps,
      async,
      campoObjetoContext,
      mensagensDeErro,
      validar,
      required,
      simpleFormContext,
      onEnter,
      ...otherProps
    } = this.props
    if (!InputProps) InputProps = {}
    if (async)
      InputProps = {
        ...InputProps,
        endAdornment: (
          <ValidarUrlAdornment
            estaCarregando={this.state.loading}
            isValid={!campoContext.getHasError()}
            showError={campoContext.getIsErrorShown()}
          />
        ),
      }
    return (
      <TextField
        variant="outlined"
        fullWidth
        FormHelperTextProps={{ classes: { root: classes.helperText } }}
        {...otherProps}
        id={id}
        className={classnames(classes.root, { [classes.ajustWidth]: InputProps.endAdornment })}
        label={campoContext.getLabel()}
        error={campoContext.getIsErrorShown() && campoContext.getHasError()}
        helperText={campoContext.getHelperOrErrorText()}
        onFocus={this._onFocus}
        onBlur={this._onBlur}
        onChange={this._onChange}
        value={this.state.value}
        onKeyPress={this._handleOnKeyPress}
        InputProps={InputProps}
      />
    )
  }
}
