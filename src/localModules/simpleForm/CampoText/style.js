export const style = theme => ({
  root: {
    margin: '25px 0',
    display: 'block',
    width: '100%',
  },
  ajustWidth: { width: 'calc(100% - 12px)' },
  helperText: { position: 'absolute' },
})
