import React, { Component } from 'react'

import { InputTag } from 'common/InputTag'

import { esperar } from 'utils/esperar'

import { propTypes } from './propTypes'

export class CampoTagComponent extends Component {
  static propTypes = propTypes
  _valorInicial

  constructor(props) {
    super(props)
    this._valorInicial = props.campoContext.getInitialValue()
  }

  _handleOnKeyPress = async e => {
    const { onKeyPress, onEnter } = this.props
    if (onKeyPress) onKeyPress(e)
    if (e.key === 'Enter' && onEnter) {
      await esperar(300)
      onEnter()
    }
  }

  render() {
    const { campoContext, simpleFormContext, ...otherProps } = this.props
    const props = {
      valorInicial: this._valorInicial ? this._valorInicial : [],
      onChange: campoContext.onChange,
    }
    return (
      <InputTag
        getClear={clear => simpleFormContext.registerFieldClear(campoContext.id, clear)}
        {...otherProps}
        {...props}
        handleKeyPress={this._handleOnKeyPress}
      />
    )
  }
}
