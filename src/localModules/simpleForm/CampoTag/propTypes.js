import PropTypes from 'prop-types'

export const propTypes = {
  accessor: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  required: PropTypes.bool,
  mensagensDeErro: PropTypes.object,
  validar: PropTypes.func,
  label: PropTypes.string,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
