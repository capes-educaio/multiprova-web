import React, { Component } from 'react'
import uid from 'uuid/v4'

import Select from '@material-ui/core/Select'
import FormControl from '@material-ui/core/FormControl'
import InputLabel from '@material-ui/core/InputLabel'
import FormHelperText from '@material-ui/core/FormHelperText'

import { propTypes } from './propTypes'

export class CampoSelectComponent extends Component {
  static propTypes = propTypes
  _inputKey = uid()

  constructor(props) {
    super(props)
    this.state = {
      value: props.campoContext.getInitialValue(),
    }
  }

  _onChange = event => {
    const { value } = event.target
    this.setState({ value })
    this.props.campoContext.onChange(value)
  }

  render() {
    const {
      classes,
      children,
      campoContext,
      campoObjetoContext,
      simpleFormContext,
      helperText,
      ...otherProps
    } = this.props
    const props = {
      key: this._inputKey,
      ...otherProps,
      value: this.state.value,
      onChange: this._onChange,
      error: campoContext.getIsErrorShown() && campoContext.getHasError(),
    }
    return (
      <FormControl className={classes.field}>
        {campoContext.getHelperOrErrorText() && (
          <FormHelperText error={campoContext.getIsErrorShown() && campoContext.getHasError()}>
            {campoContext.getHelperOrErrorText()}
          </FormHelperText>
        )}
        {campoContext.getLabel() && <InputLabel>{campoContext.getLabel()}</InputLabel>}
        <Select {...props}>{children}</Select>
      </FormControl>
    )
  }
}
