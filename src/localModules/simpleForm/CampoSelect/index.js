import React from 'react'
import { CampoSelectComponent } from './CampoSelectComponent'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'

import { mapStateToProps, mapDispatchToProps } from './redux'
import { style } from './style'
import { withCampoContext } from '../Campo/context'
import { withCampoObjetoContext } from '../CampoObjeto/context'
import { withSimpleFormContext } from '../SimpleForm/context'
import { Campo } from '../Campo'

export const CampoSelect = props => (
  <Campo {...props}>
    <CampoSelectWrapped {...props} />
  </Campo>
)

const CampoSelectWrapped = compose(
  withStyles(style),
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withCampoContext,
  withCampoObjetoContext,
  withSimpleFormContext,
)(CampoSelectComponent)
