import PropTypes from 'prop-types'

export const propTypes = {
  accessor: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  required: PropTypes.bool,
  label: PropTypes.string,
  mensagensDeErro: PropTypes.object,
  onChange: PropTypes.func,
  helperText: PropTypes.node,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
