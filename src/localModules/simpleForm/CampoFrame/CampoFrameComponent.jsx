import React, { Component, Fragment } from 'react'

import FormHelperText from '@material-ui/core/FormHelperText'
import FormLabel from '@material-ui/core/FormLabel'

import { DragHandle } from 'common/DragHandle'

import { propTypes } from './propTypes'

export class CampoFrameComponent extends Component {
  static propTypes = propTypes

  render() {
    const {
      campoContext,
      dragHandle,
      label,
      helperText,
      classes,
      children,
      disableFrame,
      validar,
      required,
    } = this.props
    if (disableFrame) return children
    else
      return (
        <Fragment>
          {dragHandle && !label && <DragHandle />}
          {label && (
            <FormLabel className={classes.label} focused>
              {dragHandle && <DragHandle />}
              {campoContext.getLabel()}
            </FormLabel>
          )}
          {helperText && <FormHelperText>{helperText}</FormHelperText>}
          {(validar || required) && (
            <FormHelperText error>
              {campoContext.getIsErrorShown() && campoContext.getHasError() ? campoContext.getHelperOrErrorText() : ' '}
            </FormHelperText>
          )}
          {children}
        </Fragment>
      )
  }
}
