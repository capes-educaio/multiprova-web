import PropTypes from 'prop-types'

export const propTypes = {
  children: PropTypes.oneOfType([PropTypes.element, PropTypes.arrayOf(PropTypes.element)]).isRequired,
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  helperText: PropTypes.string,
  required: PropTypes.bool,
  index: PropTypes.number,
  dragHandle: PropTypes.bool,
  errors: PropTypes.array,
  showError: PropTypes.bool,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
