import { CampoFrameComponent } from './CampoFrameComponent'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'

import { withCampoContext } from '../Campo/context'
import { withSimpleFormContext } from '../SimpleForm/context'

import { mapStateToProps, mapDispatchToProps } from './redux'
import { style } from './style'

export const CampoFrame = compose(
  withStyles(style),
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withCampoContext,
  withSimpleFormContext,
)(CampoFrameComponent)
