import React, { Component } from 'react'
import Typography from '@material-ui/core/Typography'

import { MpEditor } from 'common/MpEditor'

import { propTypes } from './propTypes'

export class CampoSlateComponent extends Component {
  static propTypes = propTypes

  constructor(props) {
    super(props)
    this.state = {
      value: props.campoContext.getInitialValue(),
    }
  }

  _onChange = value => {
    this.setState({ value })
    this.props.campoContext.onChange(value)
    this.props.campoContext.validate(value)
  }

  _handleOnKeyPress = e => {
    const { onKeyPress, onEnter } = this.props
    if (e.key === 'Enter' && onEnter) {
      e.preventDefault()
      onEnter()
    }
    if (onKeyPress) onKeyPress(e)
  }

  _onFocus = () => {
    if (this.props.onFocus) this.props.onFocus()
  }

  _onBlur = async () => {
    if (this.props.onBlur) this.props.onBlur()
  }

  render() {
    let {
      classes,
      id,
      campoContext,
      InputProps,
      campoObjetoContext,
      simpleFormContext,
      shouldAddDragHandle,
      editorClasses,
      children,
      ...otherProps
    } = this.props
    const props = {
      html: this.state.value,
      onChange: this._onChange,
      onFocus: this._onFocus,
      onBlur: this._onBlur,
      helperText: campoContext.getHelperOrErrorText(),
      error: campoContext.getIsErrorShown() && campoContext.getHasError(),
      campoObjetoContext,
      label: campoContext.getLabel(),
      classes: editorClasses,
    }
    const editor = <MpEditor {...otherProps} {...props} />
    return (
      <Typography headlineMapping={{ body1: 'div' }} variant="body1">
        {editor}
      </Typography>
    )
  }
}
