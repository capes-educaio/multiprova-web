import React from 'react'
import { CampoSlateComponent } from './CampoSlateComponent'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import { withStyles } from '@material-ui/core/styles'

import { mapStateToProps, mapDispatchToProps } from './redux'
import { style } from './style'
import { withCampoContext } from '../Campo/context'
import { withCampoObjetoContext } from '../CampoObjeto/context'
import { withSimpleFormContext } from '../SimpleForm/context'
import { Campo } from '../Campo'

export const CampoSlate = props => (
  <Campo {...props}>
    <CampoSlateWrapped {...props} />
  </Campo>
)

const CampoSlateWrapped = compose(
  withStyles(style),
  withRouter,
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withCampoContext,
  withCampoObjetoContext,
  withSimpleFormContext,
)(CampoSlateComponent)
