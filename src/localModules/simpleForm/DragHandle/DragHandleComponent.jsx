import React, { Component } from 'react'
import classnames from 'classnames'

import DragIndicatorIcon from '@material-ui/icons/DragIndicator'

import { propTypes } from './propTypes'

export class DragHandleComponent extends Component {
  static propTypes = propTypes

  render() {
    const { className, wrapperClassName, children, classes } = this.props
    return (
      <span className={classnames(classes.root, { [wrapperClassName]: Boolean(wrapperClassName) })}>
        {children}
        <DragIndicatorIcon color="disabled" className={classnames(classes.icon, { [className]: Boolean(className) })} />
      </span>
    )
  }
}
