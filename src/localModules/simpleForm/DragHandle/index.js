import { DragHandleComponent } from './DragHandleComponent'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'

import { SortableHandle } from 'react-sortable-hoc'

import { mapStateToProps, mapDispatchToProps } from './redux'
import { style } from './style'

export const DragHandle = compose(
  withStyles(style),
  SortableHandle,
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(DragHandleComponent)
