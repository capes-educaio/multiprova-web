export const style = theme => ({
  sliderContainer: {
    display: 'flex',
    alignItems: 'center',
  },
  slider: {
    width: '100%',
    margin: '0 15px 0 5px',
  },
})
