import React, { Component } from 'react'

import Slider from '@material-ui/lab/Slider'
import FormControl from '@material-ui/core/FormControl'
import FormHelperText from '@material-ui/core/FormHelperText'
import FormLabel from '@material-ui/core/FormLabel'
import Typography from '@material-ui/core/Typography'

import { propTypes } from './propTypes'

export class CampoSliderComponent extends Component {
  static propTypes = propTypes

  constructor(props) {
    super(props)
    this.state = {
      value: props.campoContext.getInitialValue(),
    }
  }

  _onChange = (event, value) => {
    this.setState({ value })
    this.props.campoContext.onChange(value)
  }

  render() {
    const { classes, campoContext, caption, campoObjetoContext, simpleFormContext, ...otherProps } = this.props
    const captionValue = caption ? caption(this.state.value) : null
    return (
      <FormControl className={classes.form}>
        {campoContext.getLabel() && <FormLabel focused>{campoContext.getLabel()}</FormLabel>}
        <div className={classes.sliderContainer}>
          <div className={classes.slider}>
            <Slider {...otherProps} value={this.state.value ? this.state.value : 0} onChange={this._onChange} />
          </div>
          {captionValue && <Typography variant="caption">{captionValue}</Typography>}
        </div>
        {campoContext.getHelperOrErrorText() && (
          <FormHelperText error={campoContext.getIsErrorShown() && campoContext.getHasError()}>
            {campoContext.getHelperOrErrorText()}
          </FormHelperText>
        )}
      </FormControl>
    )
  }
}
