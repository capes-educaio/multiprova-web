import React, { Component } from 'react'

import { initiateContextState } from 'utils/context'

import { CampoObjeto } from '../CampoObjeto'

import { SimpleFormContext } from './context'
import { propTypes } from './propTypes'

export class SimpleFormComponent extends Component {
  static propTypes = propTypes

  _invalidFields = new Set()
  _fieldsClear = {}

  constructor(props) {
    super(props)
    const contextInitialValue = {
      isErrorShown: false,
      showError: this.showError,
      updateFieldValidity: this._updateFieldValidity,
      isValid: true,
      errorRefs: {},
      registerFieldClear: this._registerFieldClear,
      clearFields: this._clearFields,
    }
    initiateContextState(this, contextInitialValue)
    if (props.onRef) props.onRef(this)
  }

  componentWillUnmount = () => {
    if (this.props.onRef) this.props.onRef(() => console.error('Component SimpleReduxForm está desmontado'))
  }

  _registerFieldClear = (fieldId, clear) => {
    if (clear) this._fieldsClear[fieldId] = clear
    else delete this._fieldsClear[fieldId]
  }

  _clearFields = () => {
    for (let id in this._fieldsClear) this._fieldsClear[id] && this._fieldsClear[id]()
  }

  showError = async () => {
    await this._updateContext({ isErrorShown: true })
    const errorPositions = Object.keys(this.state.context.errorRefs).map(id => {
      return this.state.context.errorRefs[id].offsetTop
    })
    const topMostError = Math.min.apply(Math, errorPositions)
    window.scrollTo({
      top: topMostError - 148,
      behavior: 'smooth',
    })
  }

  _updateFieldValidity = (fieldId, isValid) => {
    if (isValid) this._invalidFields.delete(fieldId)
    else this._invalidFields.add(fieldId)
    const isFormValid = this._invalidFields.size < 1
    if (isFormValid !== this.state.context.isValid) {
      if (this.props.onValid) this.props.onValid(isFormValid)
      this._updateContext({ isValid: isFormValid })
    }
  }

  render() {
    return (
      <SimpleFormContext.Provider value={this.state.context}>
        <CampoObjeto {...this.props} accessor="form" />
      </SimpleFormContext.Provider>
    )
  }
}
