import PropTypes from 'prop-types'

export const propTypes = {
  children: PropTypes.node,
  onValid: PropTypes.func,
  // style
  classes: PropTypes.object.isRequired,
}
