import { SimpleFormComponent } from './SimpleFormComponent'
import { compose } from 'redux'
import { withStyles } from '@material-ui/core/styles'
import { style } from './style'

export const SimpleForm = compose(withStyles(style))(SimpleFormComponent)
