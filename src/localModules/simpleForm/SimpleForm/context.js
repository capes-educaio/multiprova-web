import React from 'react'
import { withContext } from 'utils/context'

export const SimpleFormContext = React.createContext()
export const withSimpleFormContext = withContext(SimpleFormContext, 'simpleFormContext')
