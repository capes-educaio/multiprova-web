import React from 'react'
import { shallow } from 'enzyme'

export const mountTest = (Component, name) => arrayOfProps => {
  describe(`Testando mount de ${name ? name : Component.displayName}`, () => {
    if (!Array.isArray(arrayOfProps)) {
      console.error('A funcão precisa receber um array com cada elemento sendo um "props" a ser testado.')
      return
    }
    arrayOfProps.forEach((props, index) => {
      it(`props ${index}`, () => {
        const wrapper = shallow(<Component {...props} />)
        wrapper.unmount()
      })
    })
    return
  })
}
