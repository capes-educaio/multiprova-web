import { ProvaAplicacaoCriarComponent } from './ProvaAplicacaoCriarComponent'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'

import { mapStateToProps, mapDispatchToProps } from './redux'
import { style } from './style'

export const ProvaAplicacaoCriar = compose(
  withStyles(style),
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(ProvaAplicacaoCriarComponent)
