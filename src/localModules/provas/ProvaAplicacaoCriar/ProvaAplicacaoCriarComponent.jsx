import React, { Component } from 'react'

import { ProvaAplicacaoComum } from 'localModules/provas/ProvaAplicacaoComum'
import { provaUtils } from 'localModules/provas/ProvaUtils'
import { appConfig } from 'appConfig'
import CircularProgress from '@material-ui/core/CircularProgress'

import { propTypes } from './propTypes'

export class ProvaAplicacaoCriarComponent extends Component {
  static propTypes = propTypes

  constructor(props) {
    super(props)
    this.state = {
      prova: null,
      valorInicial: this._getValorInicialAplicacao(),
      carregando: false,
    }
  }
  componentWillMount = async () => {
    await this.setState({ carregando: true })

    this._usuarioAtualId = this.props.usuarioAtual.data.id
    this._provaAtualId = this.props.match.params.id
    await provaUtils
      .getProva({ usuarioAtualId: this._usuarioAtualId, provaAtualId: this._provaAtualId })
      .then(response => {
        let prova = response.data
        if (prova.dadosAplicacao) {
          const { tipoAplicacao, papel, virtual, participantes = [] } = prova.dadosAplicacao

          if (virtual) virtual.duracaoDaProva = String(virtual.duracaoDaProva)

          const valorInicial = { tipoAplicacao, papel, virtual, participantes }
          this.setState({ valorInicial, prova })
        }
        this.setState({ carregando: false })
      })
      .catch(e => console.error(e))
  }

  _getValorInicialAplicacao = () => {
    if (appConfig.projeto.isEducaio)
      return { tipoAplicacao: 'virtual', virtual: provaUtils.objetoTipoAplicaoVazio.virtual, participantes: [] }
    else return { tipoAplicacao: 'papel', papel: provaUtils.objetoTipoAplicaoVazio.papel, participantes: [] }
  }

  render() {
    const { strings, classes } = this.props
    const { prova, valorInicial, carregando } = this.state

    const circularProgress = (
      <div className={classes.carregando}>
        <CircularProgress />
      </div>
    )

    return carregando ? (
      circularProgress
    ) : (
      <ProvaAplicacaoComum titulo={strings.aplicarProva} prova={prova} valorInicial={valorInicial} />
    )
  }
}
