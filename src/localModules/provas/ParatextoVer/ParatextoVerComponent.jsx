import React, { Component, Fragment } from 'react'
import classnames from 'classnames'
import Typography from '@material-ui/core/Typography'
import Card from '@material-ui/core/Card'
import IconButton from '@material-ui/core/IconButton'

import ExpandMoreIcon from '@material-ui/icons/ExpandMore'

import { MpParser } from 'common/MpParser'

import { propTypes } from './propTypes'

export class ParatextoVerComponent extends Component {
  static defaultProps = {
    estaInicialmenteExpandido: false,
  }

  static propTypes = propTypes

  constructor(props) {
    super(props)
    this.state = {
      estaExpandido: props.estaInicialmenteExpandido,
    }
  }

  componentDidMount = () => {
    const { onRef } = this.props
    if (onRef) onRef(this)
  }

  componentWillUnmount = () => {
    const { onRef } = this.props
    if (onRef) onRef(null)
  }

  expandir = event => {
    if (event) event.stopPropagation()
    this.setState({ estaExpandido: true })
  }

  contrair = event => {
    if (event) event.stopPropagation()
    this.setState({ estaExpandido: false })
  }

  handleExpandirEContrair = event => {
    if (event) {
      event.stopPropagation()
    }
    const { estaExpandido } = this.state
    this.setState({ estaExpandido: !estaExpandido })
  }

  render() {
    const { classes, conteudo, inicial, strings } = this.props
    const { estaExpandido } = this.state
    const placeholder = inicial ? strings.paratextoInicial : strings.paratextoFinal
    return (
      <Card className={classes.card}>
        <Fragment>
          <Typography variant="caption" className={classes.placeholder}>
            {placeholder}
          </Typography>
          <div className={classnames(classes.cardContent, { [classes.cardAltura]: !estaExpandido })}>
            <MpParser>{conteudo ? conteudo : ''}</MpParser>
          </div>
        </Fragment>
        {conteudo ? (
          <div className={classes.rodape}>
            <div className={classes.actions}>
              <IconButton
                size="small"
                className={classnames(classes.expand, {
                  [classes.expandOpen]: estaExpandido,
                })}
                onClick={this.handleExpandirEContrair}
                aria-expanded={estaExpandido}
                aria-label="Show more"
              >
                <ExpandMoreIcon />
              </IconButton>
            </div>
          </div>
        ) : null}
      </Card>
    )
  }
}
