import { fade } from '@material-ui/core/styles/colorManipulator'

export const style = theme => ({
  card: {
    backgroundColor: theme.palette.gelo,
    paddingTop: '20px',
  },

  expandOpen: {
    transform: 'rotate(180deg)',
  },
  expand: {
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
    marginLeft: 'auto',
  },
  actions: {
    display: 'flex',
    alignSelf: 'flex-end',
    flexGrow: '1',
  },
  tag: {
    display: 'flex',
    paddingLeft: '20px',
    flexGrow: '1',
  },
  rodape: {
    display: 'flex',
    alignItems: 'center',
  },

  cardExpandedContent: {
    wordWrap: 'break-word',
    padding: '5px 20px 20px 20px',
  },
  cardContent: {
    textAlign: 'justify',
    display: 'flex',
    justifyContent: 'space-between',
    wordWrap: 'break-word',
    padding: '0px 20px 0px 20px',
    '& > *': {
      width: '100%',
      overflow: 'hidden',
    },
  },
  cardAltura: {
    maxHeight: '200px',
    overflow: 'hidden',
    position: 'relative',
    '&:after': {
      content: '""',
      position: 'absolute',
      zIndex: 1,
      top: '160px',
      right: '0',
      width: '100%',
      height: '40px',
      background: `-webkit-linear-gradient(transparent, ${theme.palette.gelo})`,
    },
  },
  buttonGroup: {
    backgroundColor: theme.palette.gelo,
  },
  cardTop: {
    backgroundColor: 'red',
    display: 'flex',
    marginLeft: '10px',
    padding: '5px 10px 5px 5px',
    minHeight: '30px',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    '& > :last-child': {
      flexShrink: '0',
      width: 'fit-content',
    },
    '& > *': {
      margin: 0,
      padding: 0,
      width: '100%',
    },
    '& > * > :last-child': {
      margin: 0,
      padding: 0,
    },
  },
  root: {
    width: '100%',
    flexDirection: 'column',
    display: 'flex',
    backgroundColor: fade(theme.palette.common.black, 0.05),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.black, 0.1),
    },
  },

  placeholder: {
    color: theme.palette.secondary.contrastText,
    fontWeight: 'bold',
    marginLeft: '20px',
    marginBottom: '5px',
  },
})
