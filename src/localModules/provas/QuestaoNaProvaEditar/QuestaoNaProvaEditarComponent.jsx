import React, { Component } from 'react'

import { tipoMap } from 'localModules/questoes/tipoMap'

import { propTypes } from './propTypes'

export class QuestaoNaProvaEditarComponent extends Component {
  static propTypes = propTypes

  _variant = 'edit'

  render() {
    const { questaoProcessada } = this.props
    const { tipo } = questaoProcessada
    const EditarNaProvaComponent = tipoMap[tipo] ? tipoMap[tipo].EditarNaProvaComponent : null
    if (EditarNaProvaComponent) return <EditarNaProvaComponent {...this.props} variant={this._variant} />
    else {
      console.error(`Tipo de questão "${tipo}" não tem EditarNaProvaComponent em tipoMap`)
      return null
    }
  }
}
