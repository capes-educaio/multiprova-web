import { setUrlState, getUrlState } from 'localModules/urlState'
import { post, get, patch } from 'api'
import { store } from 'utils/store'
import { enumTipoProva } from 'utils/enumTipoProva'
import { enumTipoQuestao } from 'utils/enumTipoQuestao'
import { questaoUtils } from 'localModules/questoes/utils/QuestaoUtils'
import { dispatchToStore } from 'utils/compositeActions/dispatchToStore'

class ProvaUtils {
  quatidadeStep = 2
  stepInicialEdicao = 0
  stepInicialCriacao = 0
  urlVisualizacao

  get stepValidInicialNaEdicao() {
    return Array(this.quatidadeStep).fill(true)
  }

  get stepValidInicialNaCriacao() {
    return [true, true]
  }

  get stepsShowErrorInicial() {
    return Array(this.quatidadeStep).fill(false)
  }

  get _nextDay() {
    const nextDay = new Date()
    nextDay.setDate(nextDay.getDate() + 1)
    return nextDay
  }

  get _virtualVazio() {
    return {
      dataInicioProva: new Date(this._nextDay.setHours(0, 0, 0)),
      dataTerminoProva: new Date(this._nextDay.setHours(23, 59, 59, 999)),
      duracaoDaProva: 60,
    }
  }

  get _papelVazio() {
    return {
      dataAplicacao: new Date(this._nextDay.setHours(0, 0, 0)),
      dataTerminoProva: new Date(this._nextDay.setHours(23, 59, 59, 999)),
    }
  }

  get objetoTipoAplicaoVazio() {
    return { virtual: this._virtualVazio, papel: this._papelVazio }
  }

  get provaTipo() {
    const urlState = getUrlState()
    return urlState.hidden.provaTipo || enumTipoProva.convencional
  }

  get rotas() {
    return this._rotasNormais
  }

  get _rotasNormais() {
    const { usuarioAtual } = store.getState()
    const usuarioId = usuarioAtual.data.id
    return {
      front: {
        add: '/prova',
        getEdicao: provaId => `/prova/${provaId}`,
        getVisualizacao: provaId => `/prova/view/${provaId}`,
        getAplicacao: provaId => `/prova/aplicar/${provaId}`,
        getPainel: provaId => `/prova/${provaId}/painel`,
        getEdicaoAplicao: provaId => `/prova/aplicar-edit/${provaId}`,
        provas: { pathname: '/provas' },
        voltarParaProvas: () => setUrlState({ pathname: '/provas' }),
      },
      back: {
        addQuestao: `/questoes/`,
        getVisualizacaoDeUma: provaId => `provas/${provaId}`,
        get: `/usuarios/${usuarioId}/provas/`,
        count: `/usuarios/${usuarioId}/provas/count`,
        getExclusao: provaId => `/provas/${provaId}`,
        criacao: `/provas/`,
        visualizacao: `/provas/`,
        getEdicao: provaId => `/provas/${provaId}`,
        getResumo: provaId => `/provas/${provaId}/resumo`,
        getMatrixDeCorrecao: provaId => `/provas/${provaId}/matrix-de-correcao`,
        correcao: (instanciaId, indexQuestao) => `/instanciamentos/${instanciaId}/correcao/${indexQuestao}`,
        visualizacaoQuestoes: `/usuarios/${usuarioId}/questoes`,
        countQuestoes: `/usuarios/${usuarioId}/questoes/count`,
        getAplicacao: provaId => `/provas/aplicar/${provaId}`,
        getRevercaoDeAplicacao: provaId => `/provas/reverter-aplicacao/${provaId}`,
        getPreview: provaId => `/provas/preview/${provaId}`,
        previewProva: `provas/preview`,
        toggleVistaDeProva: id => `provas/${id}/vista-de-prova`,
        estatisticasAcademicas: '/usuarios/estatisticas-academicas',
        estatisticasProvasAplicadas: '/usuarios/estatisticas-provas-aplicadas',
        estatisticasQuestoes: '/usuarios/estatisticas-questoes',
        estatisticasInstancias: id => `/usuarios/${id}/estatisticas-instancias`,
        estatisticasProvas: id => `/usuarios/${id}/estatisticas-provas`,
        estatisticasDiscentes: id => `/usuarios/${id}/estatisticas-provas-discente/`,
        acumuladoDasInstancias: id => `/usuarios/${id}/acumulado-das-instancias/`,
      },
    }
  }

  getUrlVisualizacao = () => {
    return this.rotas.back.previewProva
  }

  processarProvaParaPreview = dados => {
    const prova = JSON.parse(JSON.stringify(dados))
    const {
      titulo,
      template,
      tema,
      nomeProfessor,
      instituicao,
      grupos = [],
      tipoEmbaralhamento,
      tipoProva,
      dinamica,
      sistemaDeNotasDaProva,
      valor,
      outros,
    } = prova

    if (this.isProvaDinamica) {
      dinamica.map(item => delete item.id)
      dinamica.map(item => delete item.numPrimeiraQuestao)
      dinamica.map(item => delete item.numUltimaQuestao)
    }

    let novaProva = {
      titulo,
      template,
      tema,
      nomeProfessor,
      instituicao,
      grupos,
      tipoEmbaralhamento,
      tipoProva,
      dinamica,
      sistemaDeNotasDaProva,
      valor,
      outros,
    }
    const gruposProcessados = novaProva.grupos.map(grupo => {
      const questoes = grupo.questoes.map(questao => {
        const {
          anoEscolar,
          dificuldade,
          elaborador,
          enunciado,
          id,
          status,
          tagsVirtuais,
          tipo,
          bloco,
          multiplaEscolha,
          redacao,
          discursiva,
          vouf,
          associacaoDeColunas,
          fixa,
        } = questao

        let novaQuestao = {
          anoEscolar,
          dificuldade,
          elaborador,
          enunciado,
          id,
          status,
          tagsVirtuais,
          tipo,
          bloco,
          multiplaEscolha,
          redacao,
          discursiva,
          associacaoDeColunas,
          vouf,
          fixa,
        }
        Object.keys(novaQuestao).forEach(key => novaQuestao[key] === undefined && delete novaQuestao[key])
        return novaQuestao
      })
      return { ...grupo, questoes }
    })
    novaProva.grupos = gruposProcessados
    return novaProva
  }

  updateProva = (provaId, dados) => {
    const prova = this._processarProvaParaEnviar(dados)
    return this._updateProvaNormal(provaId, prova)
  }

  _updateProvaNormal = (provaId, dados) => {
    const url = this.rotas.back.getEdicao(provaId)
    return patch(url, dados)
  }

  _processarProvaParaEnviar = dados => {
    const prova = JSON.parse(JSON.stringify(dados))
    delete prova.dataCadastro
    delete prova.dataUltimaAlteracao
    delete prova.id
    delete prova.questaoIds
    delete prova.status
    return prova
  }

  enumStatusProva = {
    elaboracao: 'Em elaboração',
    prontaParaAplicacao: 'Pronta para aplicação',
    aplicada: 'Aplicada',
    emAplicacao: 'Em aplicação',
    resolucaoIniciada: 'Resolução iniciada',
  }

  enumIdStatusProva = {
    'Em elaboração': 'el',
    'Pronta para aplicação': 'pa',
    Aplicada: 'ap',
    'Em aplicação': 'ea',
    'Resolução iniciada': 'ri',
  }

  aplicar = ({ provaId, dadosAplicacao }) => {
    const url = this.rotas.back.getAplicacao(provaId)
    return post(url, dadosAplicacao)
  }

  reverter = ({ provaId }) => {
    const url = this.rotas.back.getRevercaoDeAplicacao(provaId)
    return post(url)
  }

  getProva = ({ usuarioAtualId, provaAtualId }) => get(this.rotas.back.getVisualizacaoDeUma(provaAtualId))

  getResumo = ({ provaAtualId }) => {
    dispatchToStore('select', 'carregando', true)
    get(this.rotas.back.getResumo(provaAtualId))
      .then(response => {
        dispatchToStore('select', 'resumo', response.data.resumoProva)
        dispatchToStore('select', 'carregando', false)
      })
      .catch(e => console.error(e))
  }

  getMatrixDeCorrecao = async ({ provaAtualId, tipoProva }) => {
    dispatchToStore('select', 'carregando', true)
    get(this.rotas.back.getMatrixDeCorrecao(provaAtualId))
      .then(response => {
        if (tipoProva === enumTipoProva.convencional) {
          let sequence = []
          for (let m of response.data.matrixDeCorrecao) {
            m.questoes = this.adapterQuestoes(m.questoes)
            if (sequence.length > 0) {
              m.questoes = this.sortQuestoesBySequence(m.questoes, sequence)
            } else {
              sequence = m.questoes.map(q => q.questaoMatrizId)
            }
          }
        }
        dispatchToStore('select', 'matrixDeCorrecao', response.data.matrixDeCorrecao)
        dispatchToStore('select', 'carregando', false)
      })
      .catch(e => console.error(e))
  }

  updateCorrecao = ({ instanciaId, idQuestao, dados }) => {
    const url = this.rotas.back.correcao(instanciaId, idQuestao)
    return post(url, { data: dados })
  }

  adapterQuestoes = questoes => {
    let _questoes = []
    for (let questao of questoes) {
      if (questao.tipo === 'bloco') {
        for (let q of questao.bloco.questoes) {
          q.bloco = {
            questaoMatrizId: questao.questaoMatrizId,
            enunciado: questao.enunciado,
            fraseInicial: questao.bloco.fraseInicial,
          }
          _questoes.push(q)
        }
      } else _questoes.push(questao)
    }
    return _questoes
  }

  sortQuestoesBySequence = (questoes, sequence) => {
    let qts = {}
    let _questoes = []

    for (let q of questoes) {
      qts[q.questaoMatrizId] = q
    }

    for (let s of sequence) {
      _questoes.push(qts[s])
    }

    return _questoes
  }

  getGrupos = ({ gruposComValor }) => {
    return gruposComValor.map(grupoComValores => {
      let { questoes, nome, id, paratextoInicial, paratextoFinal } = JSON.parse(JSON.stringify(grupoComValores))
      questoes = grupoComValores.questoes.map(this._getQuestaoOuBloco)
      return { questoes, nome, id, paratextoInicial, paratextoFinal }
    })
  }

  _getQuestaoOuBloco = questaoComValor => {
    if (questaoComValor.tipo === enumTipoQuestao.tipoQuestaoBloco) {
      const bloco = questaoComValor.bloco.questoes.map(this._getQuestao)
      return { ...this._getQuestao(questaoComValor), bloco }
    } else {
      return this._getQuestao(questaoComValor)
    }
  }

  _getQuestao = questaoComValor => {
    const { id, fixa, peso, valor } = questaoComValor
    return { questaoId: id, fixa: Boolean(fixa), peso: Number(peso), valor: Number(valor) }
  }

  getComGruposValores = ({ questoes, grupos }) => {
    return grupos.map(grupo => {
      const { id, nome, paratextoInicial, paratextoFinal } = grupo
      const questoesComValores = []
      const grupoComValores = { id, nome, questoes: questoesComValores, paratextoInicial, paratextoFinal }
      grupo.questoes.forEach(questaoNoGrupo => {
        const questao = questoes.find(q => questaoNoGrupo.questaoId === q.id)
        if (questao) {
          questao.fixa = questaoNoGrupo.fixa
          questao.peso = questaoNoGrupo.peso
          questao.valor = questaoNoGrupo.valor
          if (questao.tipo === enumTipoQuestao.tipoQuestaoBloco) {
            questoesComValores.push(this._getBlocoReordenado({ questaoBloco: questao, questaoNoGrupo }))
          } else {
            questoesComValores.push(questao)
          }
        }
      })
      grupoComValores.questoes = questoesComValores
      return grupoComValores
    })
  }

  grupoTemParatexto = grupo => {
    if (grupo.paratextoInicial) {
      return true
    }
    if (grupo.paratextoFinal) {
      return true
    }
    return false
  }

  get isProvaDinamica() {
    return provaUtils.provaTipo === enumTipoProva.dinamica
  }

  _getBlocoReordenado = ({ questaoBloco, questaoNoGrupo }) => {
    const novaQuestaoBloco = JSON.parse(JSON.stringify(questaoBloco))
    novaQuestaoBloco.bloco.questoes = []
    questaoNoGrupo.bloco.forEach(({ questaoId, fixa, peso, valor }) => {
      const questaoDoBlocoComValor = questaoBloco.bloco.questoes.find(questao => questao.id === questaoId)
      if (questaoDoBlocoComValor) {
        novaQuestaoBloco.bloco.questoes.push({ ...questaoDoBlocoComValor, fixa, peso, valor })
      }
    })
    questaoBloco.bloco.questoes.forEach(questao => {
      const questaoSemPosicao = questaoNoGrupo.bloco.every(({ questaoId }) => questao.id !== questaoId)
      if (questaoSemPosicao) {
        novaQuestaoBloco.bloco.questoes.push({ ...questao, fixa: false, peso: 1, valor: 1 })
      }
    })
    return novaQuestaoBloco
  }

  voltarParaProvas = () => setUrlState({ pathname: '/provas' })

  irParaAplicarProva = idProva => {
    setUrlState({ pathname: this.rotas.front.getAplicacao(idProva) })
  }

  irParaEditarAplicacao = idProva => {
    setUrlState({ pathname: this.rotas.front.getEdicaoAplicao(idProva) })
  }

  getCriterioQuestaoIndex = ({ prova, questao }) => {
    return prova.dinamica.findIndex(item => item.id === questao.id)
  }

  excluirCriterioDaProva = ({ prova, questao }) => {
    if (this.isProvaDinamica) {
      const index = this.getCriterioQuestaoIndex({ prova, questao })
      if (index >= 0) prova.dinamica.splice(index, 1)
    }
    return prova
  }

  excluirQuestaoDaProva = ({ prova, questao }) => {
    const value = JSON.parse(JSON.stringify(prova))
    const { indexDaQuestao, indexDaQuestaoNoBloco, grupoIndex } = this.getQuestaoIndex({
      value,
      questaoId: questao.id,
    })
    if (typeof indexDaQuestao !== 'number') return
    const questoesDaGrupo = value.grupos[grupoIndex].questoes
    if (indexDaQuestaoNoBloco >= 0) {
      questoesDaGrupo[indexDaQuestao].bloco.questoes.splice(indexDaQuestaoNoBloco, 1)
    } else {
      questoesDaGrupo.splice(indexDaQuestao, 1)
      value.questoes = value.questoes.filter(({ id }) => id !== questao.id)
    }
    value.grupos[grupoIndex].questoes = questoesDaGrupo
    return value
  }

  getQuestaoIndex = ({ value, questaoId, questoes }) => {
    const grupoIndex = value.grupos.findIndex(this._grupoTemQuestao(questaoId))
    if (grupoIndex < 0) {
      console.error('Questão não foi achada na prova: + ' + questaoId)
      return {}
    }
    const questoesDoGrupo = value.grupos[grupoIndex].questoes
    let indexDaQuestao = questoesDoGrupo.findIndex(this._questaoTemId(questaoId))
    let indexDaQuestaoNoBloco
    if (questoesDoGrupo[indexDaQuestao].tipo === enumTipoQuestao.tipoQuestaoBloco) {
      indexDaQuestaoNoBloco = questoesDoGrupo[indexDaQuestao].bloco.questoes.findIndex(this._questaoTemId(questaoId))
    }
    return { indexDaQuestao, indexDaQuestaoNoBloco, grupoIndex }
  }

  _grupoTemQuestao = questaoId => grupo => grupo.questoes.some(this._questaoTemId(questaoId))

  _questaoTemId = questaoId => questao => {
    const tem = questao.id === questaoId
    let temEmQuestaoNoBlco = false
    if (questao.tipo === enumTipoQuestao.tipoQuestaoBloco)
      temEmQuestaoNoBlco = questao.bloco.questoes.some(q => q.id === questaoId)
    return tem || temEmQuestaoNoBlco
  }

  irParaCriarQuestao = ({ location, selectValorProvaNaVolta, value }) => tipoQuestaoString => () => {
    selectValorProvaNaVolta(value)
    const deveVoltarParaEsseUrlState = {
      pathname: location.pathname,
      hidden: { ...location.state.hidden, estaVoltandoDaCriacaoDeQuestoes: true },
    }
    const pathname = `/questao/criar-${tipoQuestaoString}`
    setUrlState({
      pathname,
      hidden: { deveVoltarParaEsseUrlState, activeStep: questaoUtils.stepInicialEdicao },
    })
  }

  irParaEditarQuestao = ({ location, selectValorProvaNaVolta, value }) => questaoId => () => {
    const pathname = `/questao/${questaoId}`
    selectValorProvaNaVolta(value)
    const deveVoltarParaEsseUrlState = {
      pathname: location.pathname,
      hidden: { ...location.state.hidden, estaVoltandoDaEdicaoDeQuestoes: true },
    }
    setUrlState({
      pathname,
      hidden: { deveVoltarParaEsseUrlState, activeStep: questaoUtils.stepInicialEdicao },
    })
  }

  validarNumeroMaiorQueZero = value => {
    const erros = []
    let sucesso = true
    if (value < 1) {
      sucesso = false
      erros.push({ code: 'menor_que_um' })
    }
    return { sucesso, erros }
  }

  estatisticasAcademicas = () => {
    return get(this.rotas.back.estatisticasAcademicas)
  }

  estatisticasProvasAplicadas = () => {
    return get(this.rotas.back.estatisticasProvasAplicadas)
  }

  estatisticasQuestoes = () => {
    return get(this.rotas.back.estatisticasQuestoes)
  }

  findById = id => {
    return get(`/provas/${id}`)
  }

  getEstatisticasInstancias = idUsuario => {
    return get(this._rotasNormais.back.estatisticasInstancias(idUsuario))
  }

  getEstatisticasProvas = idUsuario => {
    return get(this._rotasNormais.back.estatisticasProvas(idUsuario))
  }

  getEstatisticasProvasDiscente = idUsuario => {
    return get(this._rotasNormais.back.estatisticasDiscentes(idUsuario))
  }

  getAcumuladoDasInstancias = idUsuario => {
    return get(this._rotasNormais.back.acumuladoDasInstancias(idUsuario))
  }
}

export const provaUtils = new ProvaUtils()
