import React, { Component } from 'react'

import Typography from '@material-ui/core/Typography'
import { Delete, Person } from '@material-ui/icons'

import { MpIconButton } from 'common/MpIconButton'
import { MpLista } from 'common/MpLista'

import { propTypes } from './propTypes'

export class ListaParticipantesAplicacaoComponent extends Component {
  static propTypes = propTypes

  excluirParticipanteSelecionado = (itemSelecionado, indexSelecionado) => {
    const { atualizarParticipantes } = this.props
    const participantes = Object.assign([], this.props.participantes)
    participantes.splice(indexSelecionado, 1)
    atualizarParticipantes(participantes)
  }
  get actionsDaListaDeUsuarios() {
    return this.props.disabled
      ? []
      : [
        <MpIconButton
          IconComponent={Delete}
          onClick={(item = 'erro ao pegar item', index) => {
            this.excluirParticipanteSelecionado(item, index)
          }}
        />,
      ]
  }

  render() {
    const { classes, participantes = [], strings, disabled } = this.props
    const icon = <Person className={classes.icon} />

    return (
      <React.Fragment>
        {participantes.length > 0 ? (
          <MpLista
            disabled={disabled}
            lista={participantes.map(el =>
              el.matricula ? `${el.matricula} - ${el.nome} (${el.email})` : `${el.nome} (${el.email})`,
            )}
            actions={this.actionsDaListaDeUsuarios}
            className={classes.root}
            icon={icon}
          />
        ) : (
          <Typography>{strings.nenhumParticipanteSelecionado}</Typography>
        )}
      </React.Fragment>
    )
  }
}
