import PropTypes from 'prop-types'

export const propTypes = {
  //lista de usuarios a ser listados
  participantes: PropTypes.array,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
