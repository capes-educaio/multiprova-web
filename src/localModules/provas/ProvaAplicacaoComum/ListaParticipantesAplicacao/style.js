export const style = theme => ({
  root: {
    padding: '10px',
    marginBottom: '10px',
  },
  icon: { fill: theme.palette.cinzaEscuro, marginRight: 10, fontSize: 20 },
})
