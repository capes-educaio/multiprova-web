import React, { Component, Fragment } from 'react'
import { propTypes } from './propTypes'
import TextField from '@material-ui/core/TextField'
import InputAdornment from '@material-ui/core/InputAdornment'
import { CalendarioInput } from 'common/CalendarioInput'

export class ProvaAplicacaoFormComponent extends Component {
  static propTypes = propTypes
  constructor(props) {
    super(props)
    this.state = {
      erroType: {
        dataInicioMenorQueHoje: false,
        dataFimMenorQueDataInicio: false,
        duracaoMenorQueHum: false,
        duracaoErroMessage: '',
        dataErroMessage: '',
      },
    }
  }

  componentDidMount = () => {
    const { onRef } = this.props
    if (onRef) onRef(this)
  }

  componentWillUnmount = () => {
    const { onRef } = this.props
    if (onRef) onRef(null)
  }

  onFormChangeData = (campo, isToCheckFields) => data => {
    this.onFormChange(data[0], campo)
    if (isToCheckFields) this.checkFields()
  }

  onFormChange = (novoValor, campo) => {
    const { valor } = this.props
    valor[campo] = novoValor
    this.props.onChange({ ...valor })
  }

  showError = () => {
    this.checkFields()
  }

  hideError = campo => {
    const { erroType } = this.state
    if (campo === 'duracao') {
      erroType.duracaoMenorQueHum = false
      erroType.duracaoErroMessage = ''
      this.setState({ erroType })
    }
    if (campo === 'dataInicio') {
      erroType.dataInicioMenorQueHoje = false
      erroType.dataFimMenorQueDataInicio = false
      erroType.dataErroMessage = ''
      this.setState({ erroType })
    }
    if (campo === 'dataFim') {
      erroType.dataFimMenorQueDataInicio = false
      erroType.dataErroMessage = ''
      this.setState({ erroType })
    }
  }

  checkFields = () => {
    const { isVirtual, strings, valor } = this.props
    const { dataAplicacao, dataInicioProva, dataTerminoProva, duracaoDaProva } = valor
    let erroType = {
      dataInicioMenorQueHoje: isVirtual ? new Date(dataInicioProva) < new Date() : new Date(dataAplicacao) < new Date(),
      dataFimMenorQueDataInicio: isVirtual
        ? new Date(dataTerminoProva) < new Date(dataInicioProva)
        : new Date(dataTerminoProva) < new Date(dataAplicacao),
      duracaoMenorQueHum: isVirtual ? !duracaoDaProva || duracaoDaProva < 1 : false,
    }

    if (erroType.duracaoMenorQueHum) {
      erroType.duracaoErroMessage = strings.maiorQueZero
    }

    if (erroType.dataInicioMenorQueHoje) {
      erroType.dataErroMessage = strings.dataMenorQueHoje
    }

    if (erroType.dataFimMenorQueDataInicio) {
      erroType.dataErroMessage = strings.dataFimMenorQueInicio
    }

    this.setState({ erroType })

    const isValid =
      !erroType.duracaoMenorQueHum && !erroType.dataInicioMenorQueHoje && !erroType.dataFimMenorQueDataInicio
    return isValid
  }

  get nomeDaVariavelInicio() {
    const { isVirtual } = this.props
    if (isVirtual) return 'dataInicioProva'
    else return 'dataAplicacao'
  }

  get classDataStyle() {
    const { erroType } = this.state
    const { classes } = this.props
    let flatClass = classes.flat
    let labelClass = classes.label
    if (erroType.dataFimMenorQueDataInicio || erroType.dataInicioMenorQueHoje) {
      flatClass += ' ' + classes.erroStyle
      labelClass += ' ' + classes.erroStyleLabel
    }
    return { flatClass, labelClass }
  }

  render() {
    const { classes, strings, isVirtual, valor, disabled } = this.props
    const { erroType } = this.state
    const duracaoDaProva = valor.duracaoDaProva
    const dataTerminoProva = valor.dataTerminoProva
    const value = valor[this.nomeDaVariavelInicio]

    return (
      <Fragment>
        <div className={classes.root}>
          <div className={classes.content}>
            <CalendarioInput
              value={value}
              inputName={this.nomeDaVariavelInicio}
              inputLabel={isVirtual ? strings.inicioJanela : strings.dataDeAplicacao}
              onFormChangeData={this.onFormChangeData(this.nomeDaVariavelInicio, false)}
              flatClass={this.classDataStyle.flatClass}
              labelClass={this.classDataStyle.labelClass}
              onClose={this.onFormChangeData(this.nomeDaVariavelInicio, true)}
              onFocus={() => this.hideError('dataInicio')}
              disabled={disabled}
              otherFlatpickrProps={{ minDate: 'today' }}
            />

            <CalendarioInput
              value={dataTerminoProva}
              inputName={'dataTerminoProva'}
              inputLabel={isVirtual ? strings.fimJanela : strings.dataTermino}
              onFormChangeData={this.onFormChangeData('dataTerminoProva', false)}
              flatClass={this.classDataStyle.flatClass}
              labelClass={this.classDataStyle.labelClass}
              onClose={this.onFormChangeData('dataTerminoProva', true)}
              onFocus={() => this.hideError('dataFim')}
              disabled={disabled}
              otherFlatpickrProps={{ minDate: 'today' }}
            />
          </div>
          {(erroType.dataFimMenorQueDataInicio || erroType.dataInicioMenorQueHoje) && (
            <label className={classes.labelErroMessage}>{erroType.dataErroMessage}</label>
          )}
        </div>
        {isVirtual && (
          <TextField
            disabled={disabled}
            error={erroType.duracaoMenorQueHum}
            className={classes.duracao}
            id="outlined-number"
            label={strings.duracaoDaProva}
            value={duracaoDaProva}
            onChange={e => this.onFormChange(e.target.value, 'duracaoDaProva')}
            type="number"
            InputLabelProps={{
              shrink: true,
            }}
            InputProps={{
              endAdornment: (
                <InputAdornment variant="filled" position="end">
                  {strings.minutosAbreviado}
                </InputAdornment>
              ),
              inputProps: { min: '1', step: '1' },
            }}
            margin="normal"
            variant="outlined"
            onBlur={this.checkFields}
            onFocus={() => this.hideError('duracao')}
            helperText={erroType.duracaoMenorQueHum ? erroType.duracaoErroMessage : ''}
            required
          />
        )}
      </Fragment>
    )
  }
}
