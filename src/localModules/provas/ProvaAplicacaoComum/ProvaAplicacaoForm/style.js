export const style = theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    marginBottom: '20px',
  },

  content: {
    marginBottom: '6px',
    display: 'flex',
    justifyContent: 'space-between',
    width: '450px',
  },

  labelErroMessage: {
    fontSize: 12,
    color: 'red',
    fontFamily: 'Roboto, sans-serif',
    marginLeft: 10,
  },

  input: {
    appearance: 'none',
    mozAppearance: 'none',
    webkitAppearance: 'none',
    border: 0,
    fontFamily: 'Roboto, sans-serif',
    fontSize: '16px',
    padding: '0px 0px 5px 0px',
    position: 'relative',
    zIndex: 1,
    width: 140,
    background: 'transparent',
    outline: 0,
  },

  erroStyle: {
    borderBottom: '0.1px solid red !important',
  },

  erroStyleLabel: {
    color: 'red !important',
  },

  flat: {
    display: 'flex',
    position: 'relative',
    borderBottom: '0.1px solid grey',
    background: theme.palette.cinza300,
    flexDirection: 'column',
    alignItems: 'flex-start',
    padding: '10px 10px 5px 15px',
    '&:hover': {
      background: theme.palette.cinzaMaisMedio,
    },
  },

  calendar: {
    display: 'flex',
    alignItems: 'flex-end',
  },

  button: {
    background: 'transparent',
    border: 0,
    cursor: 'pointer',
    outline: 0,
  },

  label: {
    fontSize: 11,
    color: theme.palette.cinza700,
    fontFamily: 'Roboto, sans-serif',
  },

  text: {
    fontFamily: 'Roboto, sans-serif',
  },

  duracao: {
    marginBottom: 30,
  },
})
