import PropTypes from 'prop-types'

export const propTypes = {
  valor: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired,
  isVirtual: PropTypes.bool.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
