import React, { Component, Fragment } from 'react'
import { Delete, GroupAdd, Print, ArrowBackIos } from '@material-ui/icons'
import { AlertaDeConfirmacao } from 'common/AlertaDeConfirmacao'
import { GeradorPDF } from 'common/GeradorPDF'
import { MpButton } from 'common/MpButton'
import { PaginaPaper } from 'common/PaginaPaper'
import { Subtitulo } from 'common/Subtitulo'
import { CancelarAplicacaoIcon } from 'localModules/icons/CancelarAplicacaoIcon'
import { provaUtils } from 'localModules/provas/ProvaUtils'
import { openDialogConfirm } from 'utils/compositeActions/'
import { showMessage, Snackbar } from 'utils/Snackbar'
import { CampoSelectTipoAplicacao } from './CampoSelectTipoAplicacao'
import { ListaParticipantesAplicacao } from './ListaParticipantesAplicacao'
import { ParticipantesDialog } from './ParticipantesDialog'
import { propTypes } from './propTypes'
import { ProvaAplicacaoForm } from './ProvaAplicacaoForm'
import { appConfig } from 'appConfig'

export class ProvaAplicacaoComumComponent extends Component {
  static propTypes = propTypes
  _valorIniciado = false
  _formRef
  _formVirtualRef
  _urlImpressao
  _geradorPDFRef
  _delayPersisteProvaNoBanco = 1000
  constructor(props) {
    super(props)
    this._provaAtualId = props.match.params.id
    this._usuarioAtualId = props.usuarioAtual.data.id
    this._urlImpressao = `/provas/imprimir-instancias/${this._provaAtualId}`
    if (props.valorInicial) this._valorIniciado = true
    this.state = {
      papelValorArmazenado: null,
      virtualValorArmazenado: null,
      snackbarIsOpen: false,
      alertaRemoverParticipantesAberto: false,
    }
    if (props.prova) {
      this._prova = props.prova
    } else {
      provaUtils
        .getProva({ usuarioAtualId: this._usuarioAtualId, provaAtualId: this._provaAtualId })
        .then(this._setProva)
        .catch(this._alertarNaoAchouProva)
    }
  }
  componentWillMount() {
    const valor = this.props.valorInicial
    this.setState({ valor })
  }

  componentDidUpdate = propsPrev => {
    const { valorInicial } = this.props

    if (!propsPrev.valorInicial && valorInicial && !this._valorIniciado) {
      this.setState({ valor: valorInicial })
      this._valorIniciado = true
    }
  }

  get _isPapel() {
    return this.state.valor.tipoAplicacao === 'papel'
  }

  get _isVirtual() {
    return this.state.valor.tipoAplicacao === 'virtual'
  }

  get _isElaboracao() {
    return this._prova === undefined || this._prova.status === provaUtils.enumStatusProva.elaboracao
  }

  get _emAplicacao() {
    return this._prova === undefined || this._prova.status === provaUtils.enumStatusProva.emAplicacao
  }

  _setProva = r => {
    this._prova = r.data
  }

  _alertarNaoAchouProva = e => console.error(e)

  _handleOnChangeTipoAplicacao = e => {
    const tipoAplicacaoNovo = e.target.value
    const tipoAplicacaoAtual = this.state.valor.tipoAplicacao
    if (tipoAplicacaoAtual !== tipoAplicacaoNovo) {
      const valorAtual = this.state.valor[tipoAplicacaoAtual]
      const valorArmazenado = this.state[tipoAplicacaoNovo + 'ValorArmazenado']
      const objetoTipoAplicacaoVazio = provaUtils.objetoTipoAplicaoVazio[tipoAplicacaoNovo]
      const novoValor = valorArmazenado ? valorArmazenado : objetoTipoAplicacaoVazio
      const participantes = this.state.valor.participantes
      if (novoValor) {
        this.setState({
          [tipoAplicacaoAtual + 'ValorArmazenado']: valorAtual,
          valor: {
            tipoAplicacao: tipoAplicacaoNovo,
            [tipoAplicacaoNovo]: novoValor,
            participantes,
          },
        })
      } else console.error('Não foi encontrado objeto para esse tipo de aplicação: ' + tipoAplicacaoNovo)
    }
  }

  _handleFormChange = novoValor => {
    const { tipoAplicacao } = this.state.valor
    this.setState({ valor: { ...this.state.valor, tipoAplicacao, [tipoAplicacao]: novoValor } })
  }

  _setFormRef = ref => (this._formRef = ref)

  _aplicar = () => {
    const { strings } = this.props
    const { valor } = this.state
    const formIsValid = this._formRef.checkFields()
    if (formIsValid && this._prova) {
      const dadosAplicacao = valor
      if (dadosAplicacao.virtual) dadosAplicacao.virtual.duracaoDaProva = Number(dadosAplicacao.virtual.duracaoDaProva)
      dadosAplicacao.grupos = this._prova.dinamica ? [] : provaUtils.getComGruposValores(this._prova)
      provaUtils
        .aplicar({ provaId: this._provaAtualId, dadosAplicacao })
        .then(() => {
          showMessage.success({ message: strings.provaProntaParaAplicacao })
          this._voltarParaProvas()
        })
        .catch(e => {
          showMessage.error({ message: strings.provaInvalida })
        })
    } else {
      if (this._formRef) this._formRef.showError()
      else console.error('Sem ref de form')
    }
  }

  _handleParticipantesGerenciados = participantesGerenciados => {
    this.setState({
      valor: {
        ...this.state.valor,
        participantes: [...participantesGerenciados],
      },
    })

    const dadosAplicacao = this.state.valor
    if (dadosAplicacao.virtual) dadosAplicacao.virtual.duracaoDaProva = Number(dadosAplicacao.virtual.duracaoDaProva)
    dadosAplicacao.participantes = [...participantesGerenciados]
    this._updateProva(dadosAplicacao)
  }

  _updateProva = dadosAplicacao => {
    provaUtils
      .updateProva(this._prova.id, { dadosAplicacao: dadosAplicacao })
      .then(this.setState({ snackbarIsOpen: true }))
      .catch(e => console.error(e))
  }

  _salvar = () => {
    const { valor } = this.state
    const formIsValid = this._formRef.checkFields()
    if (formIsValid && this._prova) {
      const dadosAplicacao = valor
      if (dadosAplicacao.virtual) dadosAplicacao.virtual.duracaoDaProva = Number(dadosAplicacao.virtual.duracaoDaProva)
      this._updateProva(dadosAplicacao)
    } else {
      if (this._formRef) this._formRef.showError()
      else console.error('Sem ref de form')
    }
  }

  _voltarParaProvas = () => {
    provaUtils.voltarParaProvas()
  }

  _irParaEditarAplicacao = () => {
    provaUtils.irParaEditarAplicacao(this._provaAtualId)
  }

  _irParaAplicarProvas = () => {
    provaUtils.irParaAplicarProva(this._provaAtualId)
  }

  _reverter = () => {
    provaUtils
      .reverter({ provaId: this._provaAtualId })
      .then(() =>
        setTimeout(() => {
          this._irParaAplicarProvas()
        }, this._delayPersisteProvaNoBanco),
      )
      .catch(e => console.error(e))
  }

  snackbarShouldClose = () => {
    this.setState({ snackbarIsOpen: false })
  }
  _fecharDialogAdicionarParticipantes = () => {
    this.setState({ modalAdicionarParticipantesAberto: false })
  }
  _openParticipantesModal = () => {
    this.setState({ modalAdicionarParticipantesAberto: true })
  }
  get _showRemoverParticipantes() {
    return Array.isArray(this.state.valor.participantes) && this.state.valor.participantes.length > 0
  }
  _handleCloseAlertaRemoverParticipantes = confirma => {
    if (confirma) this._handleParticipantesGerenciados([])
    this.setState({ alertaRemoverParticipantesAberto: false })
  }
  _abrirConfirmacaoRemoverParticipantes = () => {
    this.setState({ alertaRemoverParticipantesAberto: true })
  }

  _checarCamposParaAbrirModal = () => {
    const { strings } = this.props
    const formIsValid = this._formRef.checkFields()
    if (formIsValid)
      openDialogConfirm({
        texto: strings.confirmarAplicacao,
        titulo: strings.aplicarProva,
        onConfirm: this._aplicar,
      })
  }

  render() {
    const { strings, titulo, disabled, classes } = this.props
    const { valor, snackbarIsOpen, modalAdicionarParticipantesAberto, alertaRemoverParticipantesAberto } = this.state
    const { tipoAplicacao } = valor
    const formProps = {
      valor: valor[tipoAplicacao],
      onChange: this._handleFormChange,
      isVirtual: this._isVirtual,
      disabled,
    }
    return (
      <PaginaPaper
        titulo={titulo}
        actions={[
          this._isElaboracao && (
            <MpButton
              key={0}
              variant="material"
              buttonProps={{ variant: 'contained', color: 'primary' }}
              marginRight
              onClick={this._checarCamposParaAbrirModal}
            >
              {strings.aplicarProva}
            </MpButton>
          ),
          !this._isElaboracao && (
            <MpButton
              key={1}
              IconComponent={CancelarAplicacaoIcon}
              variant="material"
              buttonProps={{ disabled: this._emAplicacao }}
              onClick={() =>
                openDialogConfirm({
                  texto: strings.cancelarInstancias,
                  titulo: strings.excluir,
                  onConfirm: this._reverter,
                })
              }
            >
              {strings.cancelarAplicacao}
            </MpButton>
          ),
          !this._isElaboracao && this._isPapel && (
            <MpButton key={2} IconComponent={Print} variant="material" onClick={() => this._geradorPDFRef.gerarPDF()}>
              {strings.gerarPdf}
            </MpButton>
          ),
          this._isElaboracao && (
            <MpButton key={3} defaultIcon="salvar" variant="material" marginRight onClick={this._salvar}>
              {strings.salvar}
            </MpButton>
          ),
          <MpButton
            key={4}
            IconComponent={ArrowBackIos}
            variant="material"
            onClick={provaUtils.rotas.front.voltarParaProvas}
          >
            {strings.voltar}
          </MpButton>,
        ]}
        secao
      >
        <Snackbar
          variant="success"
          open={snackbarIsOpen}
          onClose={this.snackbarShouldClose}
          message={strings.asAlteracoesForamSalvas}
          autoHideDuration={3000}
        />
        {!appConfig.projeto.isEducaio && (
          <Fragment>
            <Subtitulo>{strings.tipoAplicacao}</Subtitulo>

            <CampoSelectTipoAplicacao
              value={valor.tipoAplicacao}
              onChange={this._handleOnChangeTipoAplicacao}
              disabled={disabled}
            />
          </Fragment>
        )}
        <Subtitulo marginTop>
          {strings.configuracoes} - {strings[valor.tipoAplicacao]}
        </Subtitulo>
        <ProvaAplicacaoForm
          {...formProps}
          onRef={ref => {
            this._formRef = ref
          }}
        />
        <Subtitulo>{strings.participantes}</Subtitulo>
        <ListaParticipantesAplicacao
          participantes={valor.participantes}
          atualizarParticipantes={this._handleParticipantesGerenciados}
          disabled={disabled}
        />
        <div className={classes.buttons}>
          <MpButton
            IconComponent={GroupAdd}
            variant="material"
            buttonProps={{ variant: 'contained', color: 'primary', disabled: disabled }}
            marginRight
            onClick={this._openParticipantesModal}
          >
            {strings.gerenciarParticipantes}
          </MpButton>
          {this._showRemoverParticipantes && (
            <MpButton
              IconComponent={Delete}
              variant="material"
              buttonProps={{ variant: 'contained', disabled: disabled }}
              marginRight
              onClick={() => this._abrirConfirmacaoRemoverParticipantes()}
            >
              {strings.removerParticipantes}
            </MpButton>
          )}
        </div>

        {modalAdicionarParticipantesAberto && (
          <ParticipantesDialog
            open={modalAdicionarParticipantesAberto}
            onClose={this._fecharDialogAdicionarParticipantes}
            handleParticipantesGerenciados={this._handleParticipantesGerenciados}
            participantesJaSalvos={valor.participantes}
            abas={[
              { value: 'turma', label: strings.buscaPorTurma },
              { value: 'usuario', label: strings.buscaPorUsuario },
            ]}
          />
        )}
        <GeradorPDF
          stringGerandoPDF={strings.imprimindoProvas}
          url={this._urlImpressao}
          dados={this._prova}
          onRef={geradorPDFRef => {
            this._geradorPDFRef = geradorPDFRef
          }}
          salvarNoBanco={this._salvarNoBanco}
        />
        <AlertaDeConfirmacao
          alertaAberto={alertaRemoverParticipantesAberto}
          handleClose={this._handleCloseAlertaRemoverParticipantes}
          stringNomeInstancia={strings.desejaRemoverParticipantes}
        />
      </PaginaPaper>
    )
  }
}
