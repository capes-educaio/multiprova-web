export const style = theme => ({
  input: {
    width: 200,
    maxWidth: '100%',
  },
  select: {
    paddingTop: 10,
  },
})
