import React, { Component } from 'react'

import Select from '@material-ui/core/Select'
import FormControl from '@material-ui/core/FormControl'
import MenuItem from '@material-ui/core/MenuItem'
import FilledInput from '@material-ui/core/FilledInput'

import { propTypes } from './propTypes'

export class CampoSelectTipoAplicacaoComponent extends Component {
  static propTypes = propTypes

  render() {
    const { classes, value, selectProps, strings, onChange, disabled } = this.props
    return (
      <FormControl variant="filled">
        <Select
          classes={{ root: classes.input, select: classes.select }}
          value={value}
          onChange={onChange}
          disabled={Boolean(disabled)}
          {...selectProps}
          input={<FilledInput name="age" id="filled-age-native-simple" />}
        >
          <MenuItem value="papel">{strings.papel}</MenuItem>
          <MenuItem value="virtual">{strings.virtual}</MenuItem>
        </Select>
      </FormControl>
    )
  }
}
