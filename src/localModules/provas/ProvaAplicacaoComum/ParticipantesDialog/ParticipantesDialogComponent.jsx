import { Checkbox, MenuItem, Select, TextField, Tooltip, Typography } from '@material-ui/core'
import { get } from 'api'
import { ListaUsuariosBusca } from 'common/ListaUsuariosBusca'
import React, { Component } from 'react'

export class ParticipantesDialogComponent extends Component {
  state = {
    participantesDaBusca: [],
    participantesSelecionados: [],
    valorDigitadoBuscaUsuario: '',
    turmas: [],
    turmaId: '',
    checkboxMaster: false,
  }
  timerParaBuscaNaAPI = null

  componentWillMount = async () => {
    this.props.ligarLoading()
    this.setState({
      participantesSelecionados: this.props.participantesJaSalvos,
    })
    await this.buscarPorTurma()
    this.props.desligarLoading()
  }
  componentDidUpdate(prevProps) {
    if (prevProps.abaAtual !== this.props.abaAtual) {
      this.handleChangeAba(this.props.abaAtual)
    }
  }

  onClose = () => {
    const { handleParticipantesGerenciados } = this.props
    handleParticipantesGerenciados(this.state.participantesSelecionados)
  }

  handleChangeAba = novoValorAba => {
    this.props.ligarLoading()
    this.setState({ participantesDaBusca: [] })
    switch (novoValorAba) {
      case 'turma':
        this.buscarPorTurma()
        this.props.desligarLoading()
        break
      case 'usuario':
        this.props.desligarLoading()
        break
      default:
        console.warn('Aba não válida')
        break
    }
  }
  buscarPorTurma = async () => {
    const docenteId = this.props.usuarioAtual.data.id
    await get('/turmas/turmas-recentes', { docenteId }).then(res => {
      const turmas = res.data
      this.setState({
        turmas,
        turmaId: turmas[0] ? turmas[0].id : '',
      })
      this.atualizarParticipantesTurma(this.state.turmaId)
    })
  }

  buscarPorUsuario = async input => {
    if (!!input) {
      await get('/usuarios/usuarios-discentes', { value: input }).then(({ data = {} }) => {
        const participantesDaBusca = data.usuarios ? data.usuarios : []
        this.setState({
          participantesDaBusca,
          checkboxMaster: false,
        })
      })
    } else {
      this.setState({ participantesDaBusca: [] })
    }
  }
  handleChangeBuscaTurma = event => {
    if (this.state.turmaId !== event.target.value) this.setState({ turmaId: event.target.value })
    this.atualizarParticipantesTurma(event.target.value)
  }
  handleChangeTextoUsuario = event => {
    const { value } = event.target

    clearTimeout(this.timerParaBuscaNaAPI)
    this.props.ligarLoading()
    this.setState({ valorDigitadoBuscaUsuario: value })

    this.timerParaBuscaNaAPI = setTimeout(() => {
      this.buscarPorUsuario(value)
      this.props.desligarLoading()
    }, 300)
  }

  _ordenarUsuarioPorNome = participantesSelecionados => {
    participantesSelecionados.sort((a, b) => (a.nome < b.nome ? -1 : a.nome > b.nome ? 1 : 0))
    return participantesSelecionados
  }

  marcarUsuario = usuario => {
    const { participantesSelecionados } = this.state
    participantesSelecionados.push({
      nome: usuario.nome,
      matricula: usuario.matricula,
      email: usuario.email,
      id: usuario.id,
    })
    this.setState({ participantesSelecionados: this._ordenarUsuarioPorNome(participantesSelecionados) })
  }
  desmarcarUsuario = usuario => {
    const { participantesSelecionados } = this.state
    const temp = participantesSelecionados.findIndex(part => part.id === usuario.id)
    participantesSelecionados.splice(temp, 1)
    this.setState({ participantesSelecionados })
  }
  atualizarParticipantesTurma = turmaId => {
    this.state.turmas.forEach(turma => {
      if (turma.id === turmaId) {
        const todosParticipantes = turma.alunos ? turma.alunos : []
        this.setState({
          participantesDaBusca: todosParticipantes,
          checkboxMaster: false,
        })
      }
    })
  }
  removerRepeticaoDeParticipantes = participantes => {
    for (let i = 0; i < participantes.length; i++) {
      const temp = participantes.findIndex(part => part.id === participantes[i].id)
      if (temp !== i) participantes.splice(temp, 1)
    }
    return participantes
  }
  handleChangeAllCheckboxs = (event, newCheckedValue) => {
    const { participantesSelecionados, participantesDaBusca } = this.state

    let participantes
    if (newCheckedValue) {
      participantes = participantesSelecionados.concat(participantesDaBusca)
      participantes = this.removerRepeticaoDeParticipantes(participantes)
    } else
      participantes = participantesSelecionados.filter(el => {
        return participantesDaBusca.indexOf(el) < 0
      })

    this.setState({ checkboxMaster: newCheckedValue, participantesSelecionados: participantes })
  }

  render() {
    const { abaAtual, strings, classes } = this.props
    const {
      participantesDaBusca,
      valorDigitadoBuscaUsuario,
      participantesSelecionados,
      turmas,
      turmaId,
      checkboxMaster,
    } = this.state

    return (
      <div>
        {abaAtual === 'turma' && (
          <React.Fragment>
            {turmaId !== '' && (
              <div>
                <Select
                  value={turmaId}
                  onChange={this.handleChangeBuscaTurma}
                  inputProps={{
                    className: classes.input,
                  }}
                  fullWidth
                >
                  {turmas.map((turma, i) => (
                    <MenuItem key={i} value={turma.id}>
                      {turma.ano + '.' + turma.periodo + ' - ' + turma.nome + ' - ' + turma.numero}
                    </MenuItem>
                  ))}
                </Select>
                <Tooltip
                  placement="left"
                  title={checkboxMaster ? strings.desmarcarTodosParticipantes : strings.selecionarTodosParticipantes}
                >
                  <Checkbox
                    checked={checkboxMaster}
                    key={`AllCheckboxs`}
                    onChange={(event, newCheckedValue) => this.handleChangeAllCheckboxs(event, newCheckedValue)}
                    inputProps={{
                      'aria-label': 'primary checkbox',
                    }}
                  />
                </Tooltip>
              </div>
            )}
            {turmaId === '' && <Typography variant="subtitle2">{strings.naoEncontrouTurma}</Typography>}
          </React.Fragment>
        )}
        {abaAtual === 'usuario' && (
          <React.Fragment>
            <div className={classes.buscaPorUsuariosInput}>
              <TextField
                id="inputNomeMatriculaOuEmail"
                label={strings.nomeMatriculaOuEmail}
                value={valorDigitadoBuscaUsuario}
                onChange={event => this.handleChangeTextoUsuario(event)}
                className={classes.TextField}
                autoFocus={true}
              />
            </div>
          </React.Fragment>
        )}
        <ListaUsuariosBusca
          participantes={participantesDaBusca}
          marcarUsuario={this.marcarUsuario}
          desmarcarUsuario={this.desmarcarUsuario}
          participantesSelecionados={participantesSelecionados}
        />
      </div>
    )
  }
}
