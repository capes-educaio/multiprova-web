import { ParticipantesDialogComponent } from './ParticipantesDialogComponent'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'
import { style } from './style'
import { mapStateToProps, mapDispatchToProps } from './redux'

import { WithRadioTabsDialog } from 'common/WithRadioTabsDialog'

export const ParticipantesDialog = WithRadioTabsDialog(
  compose(
    connect(
      mapStateToProps,
      mapDispatchToProps,
      null,
      { forwardRef: true },
    ),
  )(withStyles(style)(ParticipantesDialogComponent)),
)
