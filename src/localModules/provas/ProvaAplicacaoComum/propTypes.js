import PropTypes from 'prop-types'

export const propTypes = {
  titulo: PropTypes.node,
  valorInicial: PropTypes.object,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
