import React, { Component } from 'react'

import { tipoMap } from 'localModules/questoes/tipoMap'

import { propTypes } from './propTypes'

export class QuestaoNaProvaVerComponent extends Component {
  static propTypes = propTypes

  _variant = 'view'

  render() {
    const { questaoProcessada } = this.props
    const { tipo } = questaoProcessada
    const VerNaProvaComponent = tipoMap[tipo] ? tipoMap[tipo].VerNaProvaComponent : null
    if (VerNaProvaComponent) return <VerNaProvaComponent {...this.props} variant={this._variant} />
    else {
      console.error(`Tipo de questão "${tipo}" não tem VerNaProvaComponent em tipoMap`)
      return null
    }
  }
}
