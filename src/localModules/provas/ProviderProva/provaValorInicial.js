import uid from 'uuid/v4'
import { provaUtils } from 'localModules/provas'
import { enumTemplate } from 'utils/enumTemplate'
import { enumTipoSistemaDeNotasDeProva } from 'utils/enumTipoSistemaDeNotasDeProva'

export const provaValorInicial = ({ usuarioAtual, strings }) => {
  const valorProva = {
    tipoProva: provaUtils.provaTipo,
    descricao: strings.descricaoNovaProva,
    titulo: strings.novaProva,
    template: enumTemplate.provaGeral,
    tema: '',
    instituicao: '',
    dataAplicacao: '',
    nomeProfessor: usuarioAtual.data.nome,
    tagsVirtuais: [],
    tipoEmbaralhamento: 2,
    status: provaUtils.enumStatusProva.elaboracao,
    questoes: [],
    questaoIds: [],
  }

  let tipoProva = undefined
  if (!provaUtils.isProvaDinamica) {
    tipoProva = {
      grupos: [
        {
          id: uid(),
          nome: '',
          questoes: [],
        },
      ],
      valor: 0,
      sistemaDeNotasDaProva: enumTipoSistemaDeNotasDeProva.valorEmQuestoes,
    }
  } else {
    tipoProva = {
      sistemaDeNotasDaProva: enumTipoSistemaDeNotasDeProva.valorEmProvaEPesosEmQuestoes,
      valor: 10,
      dinamica: [],
    }
  }

  return { ...valorProva, ...tipoProva }
}
