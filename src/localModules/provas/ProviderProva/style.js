export const style = theme => ({
  secao: {
    padding: '20px 20px 20px 20px',
  },
  carregando: {
    display: 'flex',
    justifyContent: 'center',
    position: 'absolute',
    top: '50%',
    left: '50%',
  },
})
