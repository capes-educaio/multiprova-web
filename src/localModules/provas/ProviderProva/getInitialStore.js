export const getInitialStore = () => ({
  ProviderProva__isGruposHabilitados: false,
  ProviderProva__mostrarPin: false,
  ProviderProva__value: null,
  ProviderProva__confirmarUnificarGrupos: () => {
    console.warn('ProviderProva__confirmarUnificarGrupos não foi setado ainda.')
  },
  ProviderProva__updateProvaValue: () => console.warn('ProviderProva__updateProvaValue não foi setado ainda.'),
  ProviderProva__salvarNoBanco: () => console.warn('ProviderProva__salvarNoBanco não foi setado ainda.'),
})
