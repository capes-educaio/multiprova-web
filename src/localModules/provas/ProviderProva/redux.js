import { bindActionCreators } from 'redux'
import { actions } from 'utils/actions'

export function mapStateToProps(state) {
  return {
    usuarioAtual: state.usuarioAtual,
    strings: state.strings,
    valorProvaNaVolta: state.valorProvaNaVolta,
    mostrarPin: state.ProviderProva__mostrarPin,
    value: state.ProviderProva__value,
    stepsValidation: state.StepperSimples__stepsValidation,
    listaTags: state.listaTags,
  }
}

export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      selectListaTags: actions.select.listaTags,
      listaDialogPush: actions.push.listaDialog,
      selectStepsValidation: actions.select.StepperSimples__stepsValidation,
      selectValue: actions.select.ProviderProva__value,
      selectConfirmarUnificarGrupos: actions.select.ProviderProva__confirmarUnificarGrupos,
      selectMostrarPin: actions.select.ProviderProva__mostrarPin,
      selectUpdateProvaValue: actions.select.ProviderProva__updateProvaValue,
      selectSalvarNoBanco: actions.select.ProviderProva__salvarNoBanco,
      selectValorProvaNaVolta: actions.select.valorProvaNaVolta,
    },
    dispatch,
  )
}
