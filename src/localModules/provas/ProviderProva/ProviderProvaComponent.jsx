import React, { Component } from 'react'
import uid from 'uuid/v4'

import CircularProgress from '@material-ui/core/CircularProgress'

import { setUrlState } from 'localModules/urlState'
import { provaUtils } from 'localModules/provas'

import { get, post } from 'api'

import { setListaTags } from 'utils/compositeActions'
import { initiateContextState } from 'utils/context'
import { showMessage, Snackbar } from 'utils/Snackbar'
import { enumSeloQuestao } from 'utils/enumSeloQuestao'
import { openDialogAviso } from 'utils/compositeActions/openDialogAviso'
import { esperar } from 'utils/esperar'

import { provaValorInicial } from './provaValorInicial'
import { propTypes } from './propTypes'
import { getInitialStore } from './getInitialStore'
import { ProviderProvaContext } from './context'

export class ProviderProvaComponent extends Component {
  static propTypes = propTypes

  _provaAtualId
  _valorInicialSetado

  constructor(props) {
    super(props)
    if (props.onRef) props.onRef(this)
    setListaTags()
    this._provaAtualId = props.match.params.id
    const jaEstaCriada = Boolean(this._provaAtualId)
    this.state = {
      carregando: Boolean(this._provaAtualId) && !this._estaVotandoDeQuestao,
      snackbarIsOpen: false,
    }
    let valorInicial
    if (this._estaVotandoDeQuestao) {
      valorInicial = props.valorProvaNaVolta
      props.selectValorProvaNaVolta(null)
    } else {
      valorInicial = provaValorInicial({ usuarioAtual: props.usuarioAtual, strings: props.strings })
    }
    props.selectValue(valorInicial)
    this._valorInicialSetado = !jaEstaCriada
    props.selectConfirmarUnificarGrupos(this._confirmarUnificarGrupos)
    props.selectUpdateProvaValue(this._updateProvaValue)
    props.selectSalvarNoBanco(this._salvarNoBanco)
    if (this._estaVotandoDeQuestao) this._atualizarMostrarPin(props.valorProvaNaVolta)
    if (props.onRef) props.onRef(this)
    const contextInitialValue = {
      jaEstaCriada,
      salvarNoBanco: this._salvarNoBanco,
      isGruposHabilitados: this._checarGruposDevemEstarHabilitados(valorInicial),
      handleVoltarDeProva: this._handleVoltarDeProva,
    }

    initiateContextState(this, contextInitialValue)
  }

  componentDidMount = () => {
    if (this.state.context.jaEstaCriada) {
      this.props.selectStepsValidation(provaUtils.stepValidInicialNaEdicao)
      if (!this._estaVotandoDeQuestao) this._fetchProva()
    } else {
      this.props.selectStepsValidation(provaUtils.stepValidInicialNaCriacao)
    }
  }

  componentWillUnmount = () => {
    const initialStore = getInitialStore()
    this.props.selectMostrarPin(initialStore.ProviderProva__mostrarPin)
    this.props.selectValue(initialStore.ProviderProva__value)
    this.props.selectConfirmarUnificarGrupos(initialStore.ProviderProva__confirmarUnificarGrupos)
    this.props.selectUpdateProvaValue(initialStore.ProviderProva__updateProvaValue)
    this.props.selectSalvarNoBanco(initialStore.ProviderProva__salvarNoBanco)
    if (this.props.onRef) this.props.onRef(null)
  }

  get _estaVotandoDeQuestao() {
    const { valorProvaNaVolta } = this.props
    return (this._estaVoltandoDaEdicaoDeQuestoes || this._estaVoltandoDaCriacaoDeQuestoes) && valorProvaNaVolta
  }

  get _isView() {
    return this.props.location.pathname.split('/')[2] === 'view'
  }

  get _temMaisDeUmGrupo() {
    return this.props.value.grupos.length > 1
  }

  get _estaVoltandoDaCriacaoDeQuestoes() {
    return this.props.location.state.hidden.estaVoltandoDaCriacaoDeQuestoes
  }
  get _estaVoltandoDaEdicaoDeQuestoes() {
    return this.props.location.state.hidden.estaVoltandoDaEdicaoDeQuestoes
  }

  _fetchProva = async () => {
    const url = provaUtils.rotas.back.getVisualizacaoDeUma(this._provaAtualId)
    const { data: prova } = await get(url)
    if (prova) this._setProvaState(prova)
    else setUrlState({ pathname: '/pagNaoEncontrada/prova' })
  }

  _getTagsVirtuaisAPartirDeListaTags = tagIds => {
    const { listaTags } = this.props
    const tagsVirtuais = []
    tagIds.forEach(tagId => {
      const tag = listaTags.find(({ id }) => id === tagId)
      if (tag) tagsVirtuais.push(tag)
    })
    return tagsVirtuais
  }

  _checarGruposDevemEstarHabilitados = prova => {
    return prova && prova.grupos && (prova.grupos.length > 1 || provaUtils.grupoTemParatexto(prova.grupos[0]))
  }

  _setProvaState = async prova => {
    if (this._isView) {
      if (prova.dadosAplicacao) prova.grupos = prova.dadosAplicacao.grupos
      else console.error('deve ter dadosAplicacao para ver prova')
    } else {
      if (!provaUtils.isProvaDinamica) {
        prova.grupos = provaUtils.getComGruposValores(prova)
        prova.grupos.forEach(grupo => {
          if (!grupo.id) grupo.id = uid()
        })
        if (!this._valorInicialSetado) {
          this._updateContext({ isGruposHabilitados: this._checarGruposDevemEstarHabilitados(prova) })
        }
      }
      prova.tagsVirtuais = prova.tags

      delete prova.tags
      if (prova.dataAplicacao) prova.dataAplicacao = prova.dataAplicacao.substring(0, 10)
      else prova.dataAplicacao = ''
      if (provaUtils.isProvaDinamica) {
        prova.dinamica = prova.dinamica.map(item => ({ id: Math.random(), ...item }))
      }
    }
    this._updateProvaValue(prova)
    this.setState({ carregando: false })
  }

  _salvar = () => (this.state.context.jaEstaCriada ? this._editar() : this._cadastrar())

  get _valorParaSalvar() {
    const value = JSON.parse(JSON.stringify(this.props.value))
    delete value.id
    delete value.dataCadastro
    delete value.dataUltimaAlteracao
    delete value.tagIds
    delete value.questoes
    delete value.questaoIds

    const { valor } = value
    if (valor !== undefined) value.valor = Number(valor)
    delete value.quantidadePrevistaDeQuestoes

    value.tagsVirtuais = value.tagsVirtuais
      ? value.tagsVirtuais.map(({ id, nome, isNew }) => ({ id, nome, isNew }))
      : []
    if (value.dataAplicacao === '') delete value.dataAplicacao
    if (provaUtils.isProvaDinamica) {
      value.dinamica = value.dinamica.map(item => {
        delete item.id
        delete item.numPrimeiraQuestao
        delete item.numUltimaQuestao
        return item
      })
    } else {
      value.grupos = provaUtils.getGrupos({ gruposComValor: value.grupos })
    }
    return value
  }

  _editar = () => {
    const { ...prova } = this._valorParaSalvar
    return provaUtils.updateProva(this._provaAtualId, prova)
  }

  _cadastrar = async () => {
    const { data: prova } = await post(provaUtils.rotas.back.criacao, this._valorParaSalvar)
    this._provaAtualId = prova.id
    this._updateContext({ jaEstaCriada: true })
  }

  _processarQuestaoImportadaSalva = (questaoNaProva, questaoCriada) => {
    const novaQuestao = { ...questaoCriada }
    const saoBlocosValidos =
      questaoNaProva.bloco &&
      questaoCriada.bloco &&
      Array.isArray(questaoNaProva.bloco.questoes) &&
      Array.isArray(questaoCriada.bloco.questoes)
    if (saoBlocosValidos) {
      for (let index in questaoNaProva.bloco.questoes) {
        novaQuestao.bloco.questoes[index].fixa = questaoNaProva.bloco.questoes[index].fixa
      }
    }
    return { ...novaQuestao, fixa: questaoNaProva.fixa }
  }

  _salvarQuestaoImportada = async questao => {
    const url = provaUtils.rotas.back.addQuestao
    const { data: novaQuestao } = await post(url, questao)
    return novaQuestao
  }

  _getQuestaoParaCadastrar = questao => {
    const questaoFormatada = { ...questao }
    const propriedadesParaRemover = ['fixa', 'numeroNaProva', 'id', 'deveSerCriadaAoSalvar']
    propriedadesParaRemover.forEach(propriedadeParaRemover => delete questaoFormatada[propriedadeParaRemover])
    if (questaoFormatada.bloco) {
      questaoFormatada.bloco.questoes = questaoFormatada.bloco.questoes.map(({ numeroNaProva, ...questao }) => questao)
    }
    return questaoFormatada
  }

  _cadastrarQuestoesImportadas = async () => {
    let { value } = this.props
    for (let grupoIndex in value.grupos) {
      const grupo = value.grupos[grupoIndex]
      for (let questaoIndex in grupo.questoes) {
        const questao = grupo.questoes[questaoIndex]
        if (questao.deveSerCriadaAoSalvar) {
          const questaoFormatada = this._getQuestaoParaCadastrar(questao)
          const questaoCriada = await this._salvarQuestaoImportada(questaoFormatada)
          grupo.questoes[questaoIndex] = this._processarQuestaoImportadaSalva(questao, questaoCriada)
        }
      }
    }
    this._updateProvaValue({ ...value })
    await esperar(1000)
  }

  _salvarNoBanco = async () => {
    try {
      if (!provaUtils.isProvaDinamica) await this._cadastrarQuestoesImportadas()
      await this._salvar()

      const { value = {}, strings } = this.props
      const { questoes = [] } = value
      const isValid = !questoes.some(({ selo }) => selo !== enumSeloQuestao.validada)
      if (!isValid)
        showMessage.warn({ message: <span id="snackbar-sucesso">{strings.provaSalvaComQuestoesInvalidas}</span> })
      else showMessage.success({ message: <span id="snackbar-sucesso">{strings.asAlteracoesForamSalvas}</span> })

      this._fetchProva()
    } catch (error) {
      const { strings } = this.props
      openDialogAviso({ texto: strings.falhaNaRequisicao, titulo: strings.erro })
    }
  }

  _unificarGrupos = (value, callback) => () => {
    const { selectValue } = this.props
    const { grupos } = value
    const novoGrupos = [grupos[0]]
    for (let index = 1; index < grupos.length; index++) {
      novoGrupos[0].questoes = [...novoGrupos[0].questoes, ...grupos[index].questoes]
    }
    value.grupos = novoGrupos
    selectValue(value)
    if (callback) callback()
  }

  _confirmarUnificarGrupos = callback => value => {
    if (value === undefined) value = this.props.value
    const { listaDialogPush, strings } = this.props
    const dialogConfig = {
      titulo: strings.confirmar,
      texto: strings.confirmarUnificarGrupos,
      actions: [{ texto: strings.ok, onClick: this._unificarGrupos(value, callback) }, { texto: strings.cancelar }],
    }
    listaDialogPush(dialogConfig)
  }

  _updateProvaValue = provaValue => {
    const { value, selectValue } = this.props
    const naoEmbaralhar = provaValue.tipoEmbaralhamento === 0
    const embaralhar = provaValue.tipoEmbaralhamento === 1
    const newValue = { ...value, ...provaValue }
    if ((naoEmbaralhar || embaralhar) && this._temMaisDeUmGrupo) this._confirmarUnificarGrupos()(newValue)
    else selectValue(newValue)
    this._atualizarMostrarPin(newValue)
    if (!this._valorInicialSetado) this._valorInicialSetado = true
    else if (!this._foiModificada) this._foiModificada = true
  }

  _atualizarMostrarPin = value => {
    const { selectMostrarPin, mostrarPin } = this.props
    const { tipoEmbaralhamento } = value
    const comPin = tipoEmbaralhamento === 2
    if (!mostrarPin && comPin && !provaUtils.isProvaDinamica) selectMostrarPin(true)
    else if (mostrarPin && !comPin) selectMostrarPin(false)
  }

  _snackbarShoudClose = () => {
    this.setState({ snackbarIsOpen: false })
  }

  _handleVoltarDeProva = () => this.props.urlContext.goBackFrom('prova/|prova$|questao/', provaUtils.rotas.front.provas)

  render() {
    const { carregando, snackbarIsOpen } = this.state
    const { value, classes, children, strings } = this.props
    return (
      <ProviderProvaContext.Provider value={this.state.context}>
        <div className={classes.secao}>
          {carregando && (
            <div className={classes.carregando}>
              <CircularProgress />
            </div>
          )}
          {!carregando && value && children}
          <Snackbar
            variant="success"
            open={snackbarIsOpen}
            onClose={this._snackbarShoudClose}
            message={<span id="snackbar-sucesso">{strings.asAlteracoesForamSalvas}</span>}
            autoHideDuration={3000}
          />
        </div>
      </ProviderProvaContext.Provider>
    )
  }
}
