import { ProviderProvaComponent } from './ProviderProvaComponent'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import { withStyles } from '@material-ui/core/styles'
import { withUrlContext } from 'common/UrlProvider/context'

import { mapStateToProps, mapDispatchToProps } from './redux'
import { style } from './style'

export const ProviderProva = compose(
  withStyles(style),
  withRouter,
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withUrlContext,
)(ProviderProvaComponent)
