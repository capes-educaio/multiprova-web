import React from 'react'
import { withContext } from 'utils/context'

export const ProviderProvaContext = React.createContext()
export const withProviderProvaContext = withContext(ProviderProvaContext, 'providerProvaContext')
