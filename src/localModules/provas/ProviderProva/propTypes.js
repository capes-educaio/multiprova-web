import PropTypes from 'prop-types'

export const propTypes = {
  onRef: PropTypes.func,
  // redux state
  usuarioAtual: PropTypes.object.isRequired,
  valorProvaNaVolta: PropTypes.object,
  value: PropTypes.object,
  stepsValidation: PropTypes.array,
  listaTags: PropTypes.array,
  // redux actions
  selectListaTags: PropTypes.func.isRequired,
  listaDialogPush: PropTypes.func.isRequired,
  selectStepsValidation: PropTypes.func.isRequired,
  selectConfirmarUnificarGrupos: PropTypes.func.isRequired,
  selectMostrarPin: PropTypes.func.isRequired,
  selectUpdateProvaValue: PropTypes.func.isRequired,
  selectSalvarNoBanco: PropTypes.func.isRequired,
  selectValue: PropTypes.func.isRequired,
  selectValorProvaNaVolta: PropTypes.func.isRequired,
  // router
  location: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
}
