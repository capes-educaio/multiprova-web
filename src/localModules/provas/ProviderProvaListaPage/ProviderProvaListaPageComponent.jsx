import { Component } from 'react'
import uid from 'uuid/v4'

import { propTypes } from './propTypes'
import { enumTipoQuestao } from 'utils/enumTipoQuestao'

export class ProviderProvaListaPageComponent extends Component {
  static propTypes = propTypes

  cardsRefs = {}

  _expandirTodasCards = () => {
    const { cardsRefs } = this
    for (let cardId in cardsRefs) {
      const cardRef = cardsRefs[cardId]
      cardRef.expandir()
    }
  }

  _contrairTodasCards = () => {
    const { cardsRefs } = this
    for (let cardId in cardsRefs) {
      const cardRef = cardsRefs[cardId]
      cardRef.contrair()
    }
  }

  _setCardsRefs = id => cardRef => {
    if (cardRef) this.cardsRefs[id] = cardRef
    else delete this.cardsRefs[id]
  }

  toggleFixagemQuestaoNoBloco = (indexGrupo, indexBloco, indexQuestao) => () => {
    const { value, updateProvaValue } = this.props
    const { grupos } = value
    const grupo = grupos[indexGrupo]
    const { questoes } = grupo
    const questaoBloco = questoes[indexBloco]
    const questaoDoBloco = questaoBloco.bloco.questoes
    const questao = questaoDoBloco[indexQuestao]
    questao.fixa = !questao.fixa
    updateProvaValue(value)
  }

  toggleFixagemQuestao = (indexGrupo, indexQuestao) => () => {
    const { value, updateProvaValue } = this.props
    const { grupos } = value
    const grupo = grupos[indexGrupo]
    const { questoes } = grupo
    const questao = questoes[indexQuestao]
    questao.fixa = !questao.fixa
    updateProvaValue(value)
  }

  somaPesoQuestoes = (prev, current) => {
    if (current.tipo === enumTipoQuestao.tipoQuestaoBloco) {
      const { questoes = [] } = current.bloco
      return prev + questoes.reduce(this.somaPesoQuestoes, 0)
    }
    const { peso = 1 } = current
    return prev + Number(peso)
  }

  changePesoNoBloco = (indexGrupo, indexBloco, indexQuestao) => peso => {
    const { value, updateProvaValue } = this.props
    const { grupos } = value
    const grupo = grupos[indexGrupo]
    const { questoes } = grupo
    const questaoBloco = questoes[indexBloco]
    const questaoDoBloco = questaoBloco.bloco.questoes
    const questao = questaoDoBloco[indexQuestao]
    questao.peso = peso
    const somaPesos = questoes.reduce(this.somaPesoQuestoes, 0)
    questao.valor = value.valor * (peso / somaPesos)
    updateProvaValue(value)
  }

  changePeso = (indexGrupo, indexQuestao) => peso => {
    const { value, updateProvaValue } = this.props
    const { grupos } = value
    const grupo = grupos[indexGrupo]
    const { questoes } = grupo
    const questao = questoes[indexQuestao]
    questao.peso = peso
    const somaPesos = questoes.reduce(this.somaPesoQuestoes, 0)
    questao.valor = value.valor * (peso / somaPesos)
    updateProvaValue(value)
  }
  somaValorQuestoes = (prev, current) => {
    if (current.tipo === enumTipoQuestao.tipoQuestaoBloco) {
      const { questoes = [] } = current.bloco
      return prev + questoes.reduce(this.somaValorQuestoes, 0)
    }
    const { valor = 0 } = current
    return prev + Number(valor)
  }
  changeValorNoBloco = (indexGrupo, indexBloco, indexQuestao) => valorDaQuestao => {
    const { value, updateProvaValue } = this.props
    const { grupos } = value
    const grupo = grupos[indexGrupo]
    const { questoes } = grupo
    const questaoBloco = questoes[indexBloco]
    const questaoDoBloco = questaoBloco.bloco.questoes
    const questao = questaoDoBloco[indexQuestao]
    questao.valor = valorDaQuestao
    const todasAsQuestoes = grupos.reduce((prev, current) => [...prev, ...current.questoes], [])
    value.valor = todasAsQuestoes.reduce(this.somaValorQuestoes, 0).toFixed(2)
    updateProvaValue(value)
  }
  changeValor = (indexGrupo, indexQuestao) => valorDaQuestao => {
    const { value, updateProvaValue } = this.props
    const { grupos } = value
    const grupo = grupos[indexGrupo]
    const { questoes } = grupo
    const questao = questoes[indexQuestao]
    questao.valor = valorDaQuestao
    const todasAsQuestoes = grupos.reduce((prev, current) => [...prev, ...current.questoes], [])
    value.valor = todasAsQuestoes.reduce(this.somaValorQuestoes, 0).toFixed(2)
    updateProvaValue(value)
  }

  _getQuestoesProcessadas = (contagem, indexGrupo, pertenceBlocoIndex) => (questao, indexQuestao) => {
    const { numeroNaProva } = contagem
    const questaoComNumero = { ...questao }
    let toggleFixagem
    let changePeso
    let changeValor
    if (!questaoComNumero.id) questaoComNumero.id = uid()
    if (questao.tipo === enumTipoQuestao.tipoQuestaoBloco) {
      questaoComNumero.numeroDoBloco = contagem.numeroDoBloco
      contagem.numeroDoBloco++
      questaoComNumero.bloco.questoes = questao.bloco.questoes.map(
        this._getQuestoesProcessadas(contagem, indexGrupo, indexQuestao),
      )
      toggleFixagem = this.toggleFixagemQuestao(indexGrupo, indexQuestao)
    } else if (typeof pertenceBlocoIndex === 'number') {
      changeValor = this.changeValorNoBloco(indexGrupo, pertenceBlocoIndex, indexQuestao)
      changePeso = this.changePesoNoBloco(indexGrupo, pertenceBlocoIndex, indexQuestao)
      toggleFixagem = this.toggleFixagemQuestaoNoBloco(indexGrupo, pertenceBlocoIndex, indexQuestao)
      contagem.numeroNaProva++
    } else {
      changeValor = this.changeValor(indexGrupo, indexQuestao)
      changePeso = this.changePeso(indexGrupo, indexQuestao)
      toggleFixagem = this.toggleFixagemQuestao(indexGrupo, indexQuestao)
      contagem.numeroNaProva++
    }

    return {
      ...questaoComNumero,
      changePeso,
      changeValor,
      numeroNaProva,
      onRef: this._setCardsRefs(questao.id),
      toggleFixagem,
    }
  }

  _getGrupoProcessado = contagem => (grupo, indexGrupo) => {
    const { numeroNaProva } = contagem
    const grupoProcessado = { ...grupo }
    grupoProcessado.onRef = this._setCardsRefs(grupoProcessado.id)
    grupoProcessado.questoes = grupo.questoes.map(this._getQuestoesProcessadas(contagem, indexGrupo))
    return { ...grupoProcessado, numeroNaProva }
  }

  _getGruposProcessados = () => {
    const { grupos } = this.props.value
    const contagem = {
      numeroNaProva: 1,
      numeroDoBloco: 1,
    }
    return grupos ? [...grupos.map(this._getGrupoProcessado(contagem))] : []
  }

  _reordenarQuestoesGrupo = ({ fonteIndexGrupo, destinoIndexGrupo, fonteIndexQuestao, destinoIndexQuestao }) => {
    const { value, updateProvaValue } = this.props
    const grupoFonte = value.grupos[fonteIndexGrupo]
    const grupoDestino = value.grupos[destinoIndexGrupo]
    const questaoMovida = grupoFonte.questoes.splice(fonteIndexQuestao, 1)
    if (questaoMovida[0]) grupoDestino.questoes.splice(destinoIndexQuestao, 0, questaoMovida[0])
    else console.error('Erro ao mover questão.')
    updateProvaValue(value)
  }

  _reordenarGrupos = (fonteIndex, destinoIndex) => {
    const { updateProvaValue, value } = this.props
    const { grupos } = value
    const [grupoMovido] = grupos.splice(fonteIndex, 1)
    grupos.splice(destinoIndex, 0, grupoMovido)
    updateProvaValue(value)
  }

  _reordenarQuestoesBloco = ({ questaoId, fonteIndex, destinoIndex }) => {
    const { updateProvaValue, value } = this.props
    const { indexDaQuestao, grupoIndex } = this._getQuestaoIndex(questaoId)
    const { questoes } = value.grupos[grupoIndex].questoes[indexDaQuestao].bloco
    const [questaoMovida] = questoes.splice(fonteIndex, 1)
    questoes.splice(destinoIndex, 0, questaoMovida)
    value.grupos[grupoIndex].questoes[indexDaQuestao].bloco.questoes = questoes
    updateProvaValue(value)
  }

  _reordenarCriterios = ({ criterioId, fonteIndex, destinoIndex }) => {
    const { updateProvaValue, value } = this.props
    const { dinamica } = value
    const [criterioMovido] = dinamica.splice(fonteIndex, 1)
    dinamica.splice(destinoIndex, 0, criterioMovido)
    updateProvaValue(value)
  }

  _getQuestaoIndex = questaoId => {
    const { value } = this.props
    const grupoIndex = value.grupos.findIndex(grupo => grupo.questoes.some(q => q.id === questaoId))
    const indexDaQuestao = value.grupos[grupoIndex].questoes.findIndex(q => q.id === questaoId)
    return { indexDaQuestao, grupoIndex }
  }

  _onDragEnd = result => {
    if (!result.destination) return
    const fonteIndexGrupo = Number(result.source.droppableId)
    const destinoIndexGrupo = Number(result.destination.droppableId)
    const fonteIndex = result.source.index
    const destinoIndex = result.destination.index
    if (result.type === 'questao')
      this._reordenarQuestoesGrupo({
        fonteIndexGrupo,
        destinoIndexGrupo,
        fonteIndexQuestao: fonteIndex,
        destinoIndexQuestao: destinoIndex,
      })
    else if (result.type === 'grupo') this._reordenarGrupos(fonteIndex, destinoIndex)
    else if (result.type.substring(0, 5) === 'bloco')
      this._reordenarQuestoesBloco({
        questaoId: result.source.droppableId,
        fonteIndex,
        destinoIndex,
      })
    else if (result.type === 'criterios')
      this._reordenarCriterios({
        criterioId: result.source.droppableId,
        fonteIndex,
        destinoIndex,
      })
    else console.error('Tipo de droppable não identificado: ' + result.type)
  }

  render() {
    const { children } = this.props
    return children({
      onDragEnd: this._onDragEnd,
      getGruposProcessados: this._getGruposProcessados,
      expandirTodasCards: this._expandirTodasCards,
      contrairTodasCards: this._contrairTodasCards,
    })
  }
}
