import PropTypes from 'prop-types'

export const propTypes = {
  children: PropTypes.func.isRequired,
  // redux state
  value: PropTypes.object.isRequired,
  updateProvaValue: PropTypes.func.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
