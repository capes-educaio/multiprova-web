import PropTypes from 'prop-types'

export const propTypes = {
  expandirTodasQuestoes: PropTypes.func,
  contrairTodasQuestoes: PropTypes.func.isRequired,
  // redux state
  value: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
}
