import React from 'react'

import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import ExpandLessIcon from '@material-ui/icons/ExpandLess'
import { enumTipoProva } from 'utils/enumTipoProva'

import { propTypes } from './propTypes'

import { Paper, Typography, IconButton } from '@material-ui/core'

export class CabecalhoProvaComponent extends React.Component {
  static propTypes = propTypes

  organizaData = dataString => {
    if (!dataString) {
      return ''
    } else {
      let dataSoComDiaMesAno = dataString.substring(0, 10)
      let partesDaData = dataSoComDiaMesAno.split('-')
      return `${partesDaData[2]}-${partesDaData[1]}-${partesDaData[0]}`
    }
  }

  get qtnQuestaoDinamica() {
    const { value } = this.props
    let qtn = 0
    value.dinamica.forEach(criterio => {
      qtn += criterio.criterios.quantidade
    })
    return qtn
  }

  get calcValorQuestaoDinamica() {
    const { value } = this.props
    return Number(value.valor / this.qtnQuestaoDinamica)
      .toFixed(2)
      .replace('.', ',')
  }

  render() {
    const { classes, value, strings, expandirTodasQuestoes, contrairTodasQuestoes } = this.props
    const { instituicao, titulo } = value
    const dataFormadata = this.organizaData(value.dataAplicacao)
    const hasCriterios = value.dinamica && value.dinamica.length > 0
    const isDinamica = value.tipoProva === enumTipoProva.dinamica

    const dataAplicacao = !(dataFormadata === '') && (
      <Typography align="center">
        {strings.dataDeAplicacao}: {dataFormadata}
      </Typography>
    )
    const nomeProfessor = !(value.nomeProfessor === '') && (
      <Typography align="center">
        {strings.professor}: {value.nomeProfessor}
      </Typography>
    )

    const cabecalhoProva = (
      <div className={classes.textoContainer}>
        <Typography align="center">{instituicao}</Typography>
        <Typography align="center">{titulo}</Typography>
        {dataAplicacao}
        {nomeProfessor}
        <Typography>
          {`${strings.valorDaProva}: `}
          {Number(value.valor)
            .toFixed(2)
            .replace('.', ',')}
        </Typography>

        {value.tipoProva === enumTipoProva.dinamica && hasCriterios ? (
          <Typography>{`${strings.valorDeCadaQuestao}: ${this.calcValorQuestaoDinamica}`}</Typography>
        ) : null}
      </div>
    )
    return (
      <Paper className={classes.disposicaoCabecalho}>
        {cabecalhoProva}
        {!isDinamica && (
          <div className={classes.botoesCabecalho}>
            <IconButton onClick={contrairTodasQuestoes}>
              <ExpandLessIcon />
            </IconButton>
            <IconButton onClick={expandirTodasQuestoes}>
              <ExpandMoreIcon />
            </IconButton>
          </div>
        )}
      </Paper>
    )
  }
}
