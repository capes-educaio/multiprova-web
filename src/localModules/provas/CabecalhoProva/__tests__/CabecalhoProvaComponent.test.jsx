import React from 'react'
import { CabecalhoProvaComponent } from '../CabecalhoProvaComponent'
import Enzyme from 'enzyme'
import { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import { ptBr } from 'utils/strings/ptBr'

Enzyme.configure({ adapter: new Adapter() })

const usuarioAtual = {
  data: {
    id: 1,
  },
}

const provaValue = {
  instituicao: '',
  titulo: '',
  professor: '',
  dataAplicacao: '',
  grupos: [],
}

const defaultProps = {
  value: provaValue,
  expandirTodasQuestoes: jest.fn(),
  contrairTodasQuestoes: jest.fn(),
  // style
  classes: {},
  // strings
  strings: ptBr,
  // redux state
  usuarioAtual,
}

describe('Testando <CabecalhoProvaComponent />', () => {
  it('Carrega o componente', () => {
    const wrapper = shallow(<CabecalhoProvaComponent {...defaultProps} />)
    expect(wrapper.length).toEqual(1)
    jest.clearAllMocks()
  })
})

describe('Testando <CabecalhoProvaComponent /> organizaData', () => {
  it('Deve chamar organizaData e retornar uma data válida organizada por dd-mm-yyyy', () => {
    const wrapper = shallow(<CabecalhoProvaComponent {...defaultProps} />)
    expect(wrapper.instance().organizaData('2018-07-18T20:40:43.300Z')).toBe('18-07-2018')
    jest.clearAllMocks()
  })
  it('Deve chamar organizaData e retornar string vazia caso receba data undefined', () => {
    const wrapper = shallow(<CabecalhoProvaComponent {...defaultProps} />)
    expect(wrapper.instance().organizaData(undefined)).toBe('')
    jest.clearAllMocks()
  })
})
