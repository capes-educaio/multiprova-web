export const style = theme => ({
  disposicaoCabecalho: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    padding: '10px 0',
    boxShadow: 'none',
  },
  textoContainer: {
    flexGrow: 1,
    padding: '10px 0 10px 20px',
  },
  botoesCabecalho: {
    display: 'flex',
    flexDirection: 'column',
  },
})
