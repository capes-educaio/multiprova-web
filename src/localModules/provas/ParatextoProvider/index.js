import { compose } from 'redux'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'

import { ParatextoProviderComponent } from './ParatextoProviderComponent'
import { mapStateToProps, mapDispatchToProps } from './redux'
import { style } from './style'

export const ParatextoProvider = compose(
  withStyles(style),
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(ParatextoProviderComponent)
