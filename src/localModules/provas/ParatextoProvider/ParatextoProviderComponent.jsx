import React, { Component } from 'react'
import classnames from 'classnames'

import EditIcon from '@material-ui/icons/Edit'
import DoneIcon from '@material-ui/icons/Done'

import { ParatextoVer } from 'localModules/provas/ParatextoVer'
import { MpEditor } from 'common/MpEditor'
import { MpButtonGroup } from 'common/MpButtonGroup'
import { MpIconButton } from 'common/MpIconButton'

import { propTypes } from './propTypes'

export class ParatextoProviderComponent extends Component {
  static defaultProps = {
    estaInicialmenteExpandido: false,
  }

  static propTypes = propTypes

  constructor(props) {
    super(props)
    this.state = {
      estaExpandido: props.estaInicialmenteExpandido,
      editParatexto: false,
    }
  }

  componentDidMount = () => {
    const { onRef } = this.props
    if (onRef) onRef(this)
  }

  componentWillUnmount = () => {
    const { onRef } = this.props
    if (onRef) onRef(null)
  }

  toggleEditParatexto = () => {
    const { editParatexto } = this.state
    this.setState({ editParatexto: !editParatexto })
  }

  render() {
    const { conteudo, classes, strings, salvarParatexto, inicial } = this.props
    const { editParatexto } = this.state
    return (
      <div className={classnames(classes.mostrarAcoesOnHover, classes.root)}>
        <div className={editParatexto ? classes.buttonMenuAnchorEdit : classes.buttonMenuAnchorVer}>
          <div />
          <div className={classes.buttonGroupInNotTouch}>
            <MpButtonGroup className={classes.buttonGroup}>
              <MpIconButton
                color={editParatexto ? 'darkBlue' : 'fundoCinza'}
                IconComponent={editParatexto ? DoneIcon : EditIcon}
                onClick={this.toggleEditParatexto}
                tooltip={editParatexto ? strings.salvarPartexto : strings.editarParatexto}
                for={'edit-titulo-etapa'}
              />
            </MpButtonGroup>
          </div>
        </div>

        {editParatexto ? (
          <MpEditor html={conteudo ? conteudo : '<p></p>'} onChange={salvarParatexto} autoFocus />
        ) : (
          <ParatextoVer inicial={inicial} conteudo={conteudo} />
        )}
      </div>
    )
  }
}
