import { fade } from '@material-ui/core/styles/colorManipulator'

export const style = theme => ({
  card: {
    backgroundColor: theme.palette.gelo,
    paddingTop: '20px',
  },

  expandOpen: {
    transform: 'rotate(180deg)',
  },
  expand: {
    transform: 'rotate(0deg)',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
    marginLeft: 'auto',
  },
  actions: {
    display: 'flex',
    alignSelf: 'flex-end',
    flexGrow: '1',
  },
  tag: {
    display: 'flex',
    paddingLeft: '20px',
    flexGrow: '1',
  },
  rodape: {
    display: 'flex',
    alignItems: 'center',
  },

  cardExpandedContent: {
    wordWrap: 'break-word',
    padding: '5px 20px 20px 20px',
  },
  cardContent: {
    textAlign: 'justify',
    display: 'flex',
    justifyContent: 'space-between',
    wordWrap: 'break-word',
    padding: '0px 20px 0px 20px',
    '& > *': {
      width: '100%',
      overflow: 'hidden',
    },
  },
  buttonGroupInNotTouch: {
    opacity: '0',
    transition: '0.3s',
    alignItems: 'right',
  },
  mostrarAcoesOnHover: {
    '&:hover $buttonGroupInNotTouch': {
      opacity: '1',
    },
  },
  '@media (pointer: coarse)': {
    buttonGroupInNotTouch: {
      opacity: '1',
    },
  },
  buttonGroup: {
    backgroundColor: theme.palette.gelo,
    pointerEvents: 'auto',
  },
  buttonMenuAnchorVer: {
    pointerEvents: 'none',
    zIndex: '1',
    position: 'absolute',
    width: '100%',
    display: 'flex',
    top: '10%',
    right: '5px',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    '& > :last-child': {
      flexShrink: '0',
      width: 'fit-content',
    },
    '& > *': {
      margin: 0,
      padding: 0,
      width: '100%',
    },
    '& > * > :last-child': {
      margin: 0,
      padding: 0,
    },
  },
  buttonMenuAnchorEdit: {
    pointerEvents: 'none',
    zIndex: '1',
    position: 'absolute',
    width: '100%',
    display: 'flex',
    bottom: '8px',
    right: '5px',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    '& > :last-child': {
      flexShrink: '0',
      width: 'fit-content',
    },
    '& > *': {
      margin: 0,
      padding: 0,
      width: '100%',
    },
    '& > * > :last-child': {
      margin: 0,
      padding: 0,
    },
  },
  root: {
    position: 'relative',
    width: '100%',
    flexDirection: 'column',
    display: 'flex',
    backgroundColor: fade(theme.palette.common.black, 0.05),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.black, 0.1),
    },
  },
})
