import React, { Component, Fragment } from 'react'
import { propTypes } from './propTypes'

import { Button, MenuList, MenuItem, Select, InputLabel, FormControl } from '@material-ui/core'

import { enumTipoProva } from 'utils/enumTipoProva'
import { setListaTags } from 'utils/compositeActions'
import { dispatchToStore } from 'utils/compositeActions/dispatchToStore'
import { bancoDeProvas, bancoDeProvasId } from 'utils/enumBancoDeProvas'

import { setUrlState } from 'localModules/urlState'
import { provaUtils } from 'localModules/provas'

import { Busca } from 'common/Busca'
import { CardDisplay } from 'common/CardDisplay'
import { TituloDaPagina } from 'common/TituloDaPagina'
import { PaginaPaper } from 'common/PaginaPaper'
import { BotaoComMenu } from 'common/BotaoComMenu'

import { CardGerenciamentoProvas } from './CardGerenciamentoProvas'
import { ProvaFormFiltroExpandido } from './ProvaFormFiltroExpandido'
import { ProvaFormFiltroPadrao } from './ProvaFormFiltroPadrao'
import { montarFiltroProva } from './montarFiltroProva'

export class GerenciamentoProvasComponent extends Component {
  static propTypes = propTypes

  cardDisplay
  buscaRef

  constructor(props) {
    super(props)
    setListaTags()
  }

  _goToAdd = (tipo = enumTipoProva.convencional) => () =>
    setUrlState({
      pathname: provaUtils.rotas.front.add,
      hidden: {
        provaTipo: tipo,
        activeStep: provaUtils.stepInicialCriacao,
        isOpenModal: false,
        idsCardsSelecionados: [],
        stepsValidation: provaUtils.stepValidInicialNaCriacao,
        stepsShowError: provaUtils.stepsShowErrorInicial,
      },
    })

  _updateBancoDeProvaStore = () => {
    const { inputsDoFiltro } = this.props.location.state.hidden
    if (inputsDoFiltro && inputsDoFiltro.status)
      dispatchToStore('select', 'tipoBancoProvas', bancoDeProvasId[inputsDoFiltro.status])
    else dispatchToStore('select', 'tipoBancoProvas', bancoDeProvas.todasAsProvas)
  }

  componentDidMount = () => {
    this._updateBancoDeProvaStore()
  }

  rotas = () => {
    const { usuarioAtual } = this.props
    return {
      back: {
        get: `/usuarios/${usuarioAtual.data.id}/provas/`,
        count: `/usuarios/${usuarioAtual.data.id}/provas/count`,
        getExclusao: id => `/provas/${id}`,
        create: `/provas/`,
      },
      front: {
        add: '/prova',
        getEdicao: id => `prova/${id}`,
      },
    }
  }

  handleChange = event => {
    event.preventDefault()
    const rotas = this.rotas(event.target.value)
    if (rotas) {
      this.buscaRef.filtroRef.handleChangeStatus(event.target.value)
      this.buscaRef.onClickPesquisar()
    }
    dispatchToStore('select', 'tipoBancoProvas', event.target.value)
  }

  selectBancoDeStatus = () => {
    const { strings, classes } = this.props

    return (
      <FormControl>
        <InputLabel>{strings.filtroPorStatus}</InputLabel>
        <Select
          className={classes.widthSelect}
          inputProps={{
            name: 'tipoVisualizacao',
          }}
          value={this.props.tipoBancoProvas}
          onChange={this.handleChange}
        >
          <MenuItem value={bancoDeProvas.todasAsProvas}>{strings.todasAsProvas}</MenuItem>
          <MenuItem value={bancoDeProvas.emElaboracao}>{strings.emElaboracao}</MenuItem>
          <MenuItem value={bancoDeProvas.prontaParaAplicacao}>{strings.prontaParaAplicacao}</MenuItem>
          <MenuItem value={bancoDeProvas.emAplicacao}>{strings.emAplicacao}</MenuItem>
          <MenuItem value={bancoDeProvas.aplicada}>{strings.aplicada}</MenuItem>
          {/* <MenuItem value={bancoDeProvas.resolucaoIniciada}>{strings.resolucaoIniciada}</MenuItem> */}
        </Select>
      </FormControl>
    )
  }

  render() {
    const { strings } = this.props
    return (
      <Fragment>
        <PaginaPaper>
          <TituloDaPagina>
            {this.selectBancoDeStatus()}
            <BotaoComMenu
              menuList={
                <MenuList>
                  <MenuItem onClick={this._goToAdd(enumTipoProva.convencional)}>{strings.convencional}</MenuItem>
                  <MenuItem onClick={this._goToAdd(enumTipoProva.dinamica)}>{strings.dinamica}</MenuItem>
                </MenuList>
              }
            >
              <Button variant="contained" mini color="primary" aria-label="add">
                {strings.novaProva}
              </Button>
            </BotaoComMenu>
          </TituloDaPagina>
        </PaginaPaper>
        <Busca
          FormPadrao={ProvaFormFiltroPadrao}
          FormExpandido={ProvaFormFiltroExpandido}
          montarFiltro={montarFiltroProva}
          onRef={buscaRef => {
            this.buscaRef = buscaRef
          }}
          isFiltroDeProva
        />
        <CardDisplay
          CardComponent={CardGerenciamentoProvas}
          numeroDeColunas={{
            extraSmall: 1,
            small: 1,
            medium: 1,
            large: 1,
            extraLarge: 1,
          }}
          nadaCadastrado={strings.naoEncontrouProva}
          quantidadeUrl={provaUtils.rotas.back.count}
          fetchInstanciasUrl={provaUtils.rotas.back.get}
          quantidadePorPaginaInicial={12}
          quantidadePorPaginaOpcoes={[12, 24, 48, 96]}
          onRef={ref => (this.cardDisplay = ref)}
          contexto={{ rotas: provaUtils.rotas }}
        />
      </Fragment>
    )
  }
}
