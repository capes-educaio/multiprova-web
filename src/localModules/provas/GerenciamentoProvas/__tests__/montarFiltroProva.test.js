import { montarFiltroProva, getTags, getStatus } from '../montarFiltroProva'
import Enzyme from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

Enzyme.configure({ adapter: new Adapter() })

const EM_ELABORACAO = 'Em elaboração'
const RESULTADO_ESPERADO_VAZIO = {}
const TAGS = [{ id: 1 }, { id: 2 }, { id: 3 }]
const TEXTO_FILTRO_ESPERADO = '.*TESTE.*'
const TEXTO_FILTRO = 'TESTE'
const RESULTADO_ESPERADO_STATUS = { status: EM_ELABORACAO }
const RESULTADO_ESPERADO_TAG = {
  or: [{ and: [{ tagIds: 1 }, { tagIds: 2 }, { tagIds: 3 }] }],
}

const where = {
  and: [
    {
      or: [{ titulo: { regexp: TEXTO_FILTRO_ESPERADO } }, { descricao: { regexp: TEXTO_FILTRO_ESPERADO } }],
    },
    RESULTADO_ESPERADO_TAG,
    RESULTADO_ESPERADO_STATUS,
  ],
}
const include = {
  relation: 'tags',
}
const RESULTADO_ESPERADO_FILTRO = {
  include,
  where,
}

describe('Testando <montaFiltroQuestao.js />', () => {
  it('Retorna filtro tag ', () => {
    const filtroTag = getTags(TAGS)
    expect(filtroTag).toEqual(RESULTADO_ESPERADO_TAG)
  })
  it('Retorna filtro tag vazio', () => {
    const NAO_FILTRADO = undefined
    const filtroTag = getTags(NAO_FILTRADO)
    expect(filtroTag).toEqual(RESULTADO_ESPERADO_VAZIO)
  })
  it('Retorna filtro status ', () => {
    const filtroStatus = getStatus(EM_ELABORACAO)
    expect(filtroStatus).toEqual(RESULTADO_ESPERADO_STATUS)
  })
  it('Retorna filtro status vazio', () => {
    const NAO_FILTRADO = undefined
    const filtroStatus = getStatus(NAO_FILTRADO)
    expect(filtroStatus).toEqual(RESULTADO_ESPERADO_VAZIO)
  })

  it('Retorna filtro ', () => {
    const FILTROS = {
      tagsSelected: TAGS,
      textoFiltro: TEXTO_FILTRO,
      status: EM_ELABORACAO,
    }
    const filtroTag = montarFiltroProva(FILTROS)

    expect(filtroTag).toEqual(RESULTADO_ESPERADO_FILTRO)
  })
})
