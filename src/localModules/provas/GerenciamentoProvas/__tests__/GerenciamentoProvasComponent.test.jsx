import React from 'react'
import { shallow } from 'enzyme'

import { GerenciamentoProvasComponent } from '../GerenciamentoProvasComponent'

jest.mock('localModules/provas', () => ({
  provaUtils: {
    rotas: { front: {}, back: {} },
  },
}))

jest.mock('utils/compositeActions', () => ({
  setListaTags: jest.fn(),
}))

const defaultProps = {
  // redux state
  usuarioAtual: { data: {} },
  // strngs
  strings: {},
  // style
  classes: {},
  // router
  history: { location: { pathname: '' } },
  location: { pathname: '', state: { hidden: '' } },
}

describe('GerenciamentoProvasComponent', () => {
  it('Render e mount', () => {
    shallow(<GerenciamentoProvasComponent {...defaultProps} />)
  })
})
