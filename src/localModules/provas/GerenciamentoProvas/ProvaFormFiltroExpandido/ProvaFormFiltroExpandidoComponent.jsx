import React from 'react'

import TextField from '@material-ui/core/TextField'

import { propTypes } from './propTypes'

export class ProvaFormFiltroExpandidoComponent extends React.Component {
  static propTypes = propTypes

  handleChange = campo => event => {
    event.preventDefault()

    this.alterarValorDoForm(campo, event.target.value)
    this.setState({ [campo]: event.target.value })
  }

  handleChangeTags = tags => {
    this.alterarValorDoForm('tagsSelected', tags)
  }

  alterarValorDoForm = (campo, valor) => {
    const { onFormFiltroChange, valorDoForm } = this.props
    const value = valor.value
    valorDoForm[campo] = value ? value : valor
    onFormFiltroChange(valorDoForm)
  }

  render() {
    const { strings, valorDoForm, handleKeyPress, classes } = this.props
    const { textoFiltro } = valorDoForm
    return (
      <div>
        <div className={classes.pesquisar}>
          <TextField
            className={classes.textField}
            id="busca"
            value={textoFiltro}
            label={strings.buscarPorTexto}
            onChange={this.handleChange('textoFiltro')}
            onKeyPress={handleKeyPress}
            fullWidth
          />
        </div>
        <div className={classes.botoes}>{this.props.children}</div>
      </div>
    )
  }
}
