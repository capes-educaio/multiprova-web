import React, { Component } from 'react'

import { InsertChart, DoneAll, Clear, PlaylistAddCheck, Cancel, FileCopy } from '@material-ui/icons'
import VisibilityIcon from '@material-ui/icons/Visibility'
import EditIcon from '@material-ui/icons/Edit'
import AssignmentIcon from '@material-ui/icons/Assignment'
import DeleteIcon from '@material-ui/icons/Delete'
import { showMessage } from 'utils/Snackbar'

import { openDialogExcluir } from 'utils/compositeActions/openDialogExcluir'
import { openDialogConfirm } from 'utils/compositeActions/openDialogConfirm'

import { setUrlState, updateUrlState } from 'localModules/urlState'
import { provaUtils } from 'localModules/provas'
import { post, patch } from 'api'

import { ProvaCard } from 'common/ProvaCard'
import { AlertaDeConfirmacao } from 'common/AlertaDeConfirmacao'
import { MpIconButton } from 'common/MpIconButton'
import { MpButtonGroup } from 'common/MpButtonGroup'

import { propTypes } from './propTypes'

export class CardGerenciamentoProvasComponent extends Component {
  static propTypes = propTypes
  _opcoesRef

  state = {
    alerta: false,
  }

  get _estaEmElaboracao() {
    const { instancia } = this.props
    return instancia.status === provaUtils.enumStatusProva.elaboracao
  }

  get _estaProntaAplicacao() {
    const { instancia } = this.props
    return instancia.status === provaUtils.enumStatusProva.prontaParaAplicacao
  }

  get _aplicada() {
    const { instancia } = this.props
    return instancia.status === provaUtils.enumStatusProva.aplicada
  }

  get _emAplicacao() {
    const { instancia } = this.props
    return instancia.status === provaUtils.enumStatusProva.emAplicacao
  }

  get _notasPublicadas() {
    const { instancia } = this.props
    if (instancia.dadosAplicacao && instancia.dadosAplicacao.notasPublicadas)
      return instancia.dadosAplicacao.notasPublicadas
    else return false
  }
  get _vistaDeProvaEstaDisponivel() {
    return this._emAplicacao || this._aplicada || this._estaProntaAplicacao
  }

  _goToEdit = event => {
    this._opcoesRef && this._opcoesRef.close(event)
    const { instancia, contexto } = this.props
    updateUrlState({
      pathname: contexto.rotas.front.getEdicao(instancia.id),
      hidden: {
        activeStep: provaUtils.stepInicialEdicao,
        provaTipo: instancia.tipoProva,
        isOpenModal: false,
        idsCardsSelecionados: [],
        stepsValidation: provaUtils.stepValidInicialNaEdicao,
        stepsShowError: provaUtils.stepsShowErrorInicial,
      },
    })
  }

  _goToView = event => {
    this._opcoesRef && this._opcoesRef.close(event)
    const { instancia, contexto } = this.props
    updateUrlState({
      pathname: contexto.rotas.front.getVisualizacao(instancia.id),
      hidden: {
        activeStep: provaUtils.stepInicialEdicao,
        stepsValidation: provaUtils.stepValidInicialNaEdicao,
        stepsShowError: provaUtils.stepsShowErrorInicial,
      },
    })
  }

  _goToAplicar = event => {
    this._opcoesRef && this._opcoesRef.close(event)
    const { instancia, contexto } = this.props
    setUrlState({ pathname: contexto.rotas.front.getAplicacao(instancia.id) })
  }

  _goToAplicarEdit = event => {
    this._opcoesRef && this._opcoesRef.close(event)
    const { instancia, contexto } = this.props
    setUrlState({ pathname: contexto.rotas.front.getEdicaoAplicao(instancia.id) })
  }

  _goToPainelProva = event => {
    this._opcoesRef && this._opcoesRef.close(event)
    const { instancia, contexto } = this.props
    setUrlState({ pathname: contexto.rotas.front.getPainel(instancia.id) })
  }

  _handleExcluir = async event => {
    this._opcoesRef && this._opcoesRef.close(event)
    const {
      strings,
      instancia,
      atualizarCardDisplay,
      contexto: { rotas },
    } = this.props
    let texto = strings.desejaExcluirProva
    let titulo = strings.excluir
    let url = rotas.back.getExclusao(instancia.id)
    openDialogExcluir({ texto, titulo, url, resolve: () => atualizarCardDisplay(), reject: this._rejectExcluir })
  }

  _rejectExcluir = e => {
    console.error('Erro não tratado')
    console.error(e)
  }

  _publicarNotas = () => {
    const { instancia, atualizarCardDisplay, strings } = this.props
    const url = `/provas/${instancia.id}/publicar-notas`
    post(url).then(() => {
      atualizarCardDisplay()
      showMessage.success({ message: strings.notasPublicadas })
    })
  }

  _despublicarNotas = () => {
    const { instancia, atualizarCardDisplay, strings } = this.props
    const url = `/provas/${instancia.id}/despublicar-notas`
    post(url).then(() => {
      atualizarCardDisplay()
      showMessage.success({ message: strings.notasDespublicadas })
    })
  }

  _handleDuplicarProva = () => {
    const { strings, instancia, atualizarCardDisplay } = this.props
    const texto = strings.desejaDuplicarProva
    const titulo = strings.duplicarProva
    openDialogConfirm({
      texto,
      titulo,
      onConfirm: () =>
        post(`/provas/${instancia.id}/duplicar`).then(() => {
          atualizarCardDisplay()
          showMessage.success({ message: strings.provaDuplicada })
        }),
    })
  }

  _handleCloseAlerta = () => this.setState({ alerta: false })

  _handleClosePublicarDespublicar = confirma => {
    if (confirma === true && !this._notasPublicadas) this._publicarNotas()
    else if (confirma === true && this._notasPublicadas) this._despublicarNotas()
    this._handleCloseAlerta()
  }

  _handleAbrirAlerta = () => {
    this.setState({ alerta: true })
  }

  _toggleHabilitarVistaDeProva = async event => {
    this._opcoesRef && this._opcoesRef.close(event)
    const { atualizarCardDisplay, instancia, strings } = this.props
    const url = provaUtils.rotas.back.toggleVistaDeProva(instancia.id)
    patch(url, { vistaProvaHabilitada: !instancia.vistaProvaHabilitada })
      .then(() => {
        atualizarCardDisplay()
      })
      .catch(() => {
        showMessage.error({ message: strings.provaNaoAplicada })
      })
  }

  render() {
    const { instancia, strings } = this.props
    const { alerta } = this.state
    const deveMostrarEditar = this._estaEmElaboracao
    const { vistaProvaHabilitada } = instancia

    let opcoesArray = [
      {
        IconComponent: VisibilityIcon,
        onClick: this._goToView,
        tooltip: strings.visualizar,
        key: 'visualizar' + this._id,
        condition: this._estaProntaAplicacao || this._aplicada || this._emAplicacao,
      },
      {
        IconComponent: EditIcon,
        onClick: this._goToEdit,
        tooltip: strings.editar,
        key: 'editar' + this._id,
        condition: deveMostrarEditar,
      },
      {
        IconComponent: AssignmentIcon,
        onClick: this._goToAplicar,
        tooltip: strings.aplicarProva,
        key: 'aplicarProva' + this._id,
        condition: this._estaEmElaboracao,
      },
      {
        IconComponent: EditIcon,
        onClick: this._goToAplicarEdit,
        tooltip: strings.editarAplicacao,
        key: 'editarAplicacao' + this._id,
        condition: this._estaProntaAplicacao,
      },
      {
        IconComponent: FileCopy,
        onClick: this._handleDuplicarProva,
        tooltip: strings.duplicarProva,
        key: 'duplicar' + this._id,
        condition: true,
      },
      {
        IconComponent: DeleteIcon,
        onClick: this._handleExcluir,
        tooltip: strings.excluir,
        key: 'excluir' + this._id,
        condition: !this._aplicada && !this._emAplicacao,
      },
      {
        IconComponent: InsertChart,
        onClick: this._goToPainelProva,
        tooltip: strings.painelProva,
        key: 'painelProva' + this._id,
        condition: this._aplicada || this._emAplicacao || this._estaProntaAplicacao,
      },
      {
        IconComponent: DoneAll,
        onClick: this._handleAbrirAlerta,
        tooltip: strings.publicarNotas,
        key: 'publicarNotas' + this._id,
        condition: !this._notasPublicadas && this._aplicada,
      },
      {
        IconComponent: Clear,
        onClick: this._handleAbrirAlerta,
        tooltip: strings.despublicarNotas,
        key: 'despublicarNotas' + this._id,
        condition: this._notasPublicadas && this._aplicada,
      },
      {
        IconComponent: vistaProvaHabilitada ? Cancel : PlaylistAddCheck,
        onClick: this._toggleHabilitarVistaDeProva,
        tooltip: vistaProvaHabilitada ? strings.desativarVistaDeProva : strings.ativarVistaDeProva,
        key: 'despublicarNotas' + this._id,
        condition: this._vistaDeProvaEstaDisponivel,
      },
    ]

    const opcoes = (
      <div>
        <MpButtonGroup>
          {opcoesArray
            .filter(op => {
              return op.condition
            })
            .map(({ IconComponent, onClick, tooltip, iconProps, key }) => (
              <MpIconButton {...{ IconComponent, onClick, tooltip, iconProps, key }} color="fundoCinza" />
            ))}
        </MpButtonGroup>
      </div>
    )

    return (
      <div>
        <AlertaDeConfirmacao
          alertaAberto={alerta}
          descricaoSelected={this._notasPublicadas ? strings.confirmarDespublicacao : strings.confirmarPublicacao}
          handleClose={this._handleClosePublicarDespublicar}
          stringNomeInstancia={this._notasPublicadas ? strings.despublicarNotas : strings.publicarNotas}
        />
        <ProvaCard opcoes={opcoes} instancia={instancia} titulo={instancia.titulo} exibirDataUltimaAlteracao />
      </div>
    )
  }
}
