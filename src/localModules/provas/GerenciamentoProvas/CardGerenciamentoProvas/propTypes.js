import PropTypes from 'prop-types'

const { shape, object } = PropTypes

export const propTypes = {
  instancia: PropTypes.object.isRequired,
  atualizarCardDisplay: PropTypes.func.isRequired,
  contexto: shape({ rotas: object.isRequired }).isRequired,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
