import React from 'react'

import { InputTag } from 'common/InputTag'
import { provaUtils } from 'localModules/provas'
import { propTypes } from './propTypes'

import { bancoDeProvas } from 'utils/enumBancoDeProvas'

export class ProvaFormFiltroPadraoComponent extends React.Component {
  static propTypes = propTypes

  componentDidMount = async () => {
    const { onRef } = this.props
    if (onRef) onRef(this)
  }

  componentWillUnmount = () => {
    const { onRef } = this.props
    if (onRef) onRef(null)
  }

  handleChange = campo => event => {
    event.preventDefault()
    this.alterarValorDoForm(campo, event.target.value)
  }

  handleChangeStatus = status => {
    const value = status !== bancoDeProvas.todasAsProvas ? Object.values(provaUtils.enumStatusProva)[status] : 0
    this.alterarValorDoForm('status', value)
  }

  alterarValorDoForm = (campo, valor) => {
    const { onFormFiltroChange, valorDoForm } = this.props
    valorDoForm[campo] = valor
    onFormFiltroChange(valorDoForm)
    this.setState({ [campo]: valor })
  }

  limparFiltro = () => {
    const { onFormFiltroChange } = this.props
    const valorInicialTexto = ''
    const valorInicialStatus = 0
    const valorInicialTagsSelected = []
    const filtroLimpo = {
      textoFiltro: valorInicialTexto,
      tagsSelected: valorInicialTagsSelected,
      status: valorInicialStatus,
    }
    onFormFiltroChange(filtroLimpo)
  }

  handleChangeTags = tags => {
    this.alterarValorDoForm('tagsSelected', tags)
  }

  render() {
    const { valorDoForm, handleKeyPress } = this.props
    let { tagsSelected } = valorDoForm
    if (tagsSelected === undefined) tagsSelected = []

    return <InputTag onChange={this.handleChangeTags} valor={tagsSelected} handleKeyPress={handleKeyPress} />
  }
}
