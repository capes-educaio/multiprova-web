const getStatus = status => {
  let jsonStatus
  if (status !== 0) {
    jsonStatus = { status }
  } else {
    jsonStatus = {}
  }
  return jsonStatus
}

const getTags = tagsSelected => {
  let jsonTag
  if (typeof tagsSelected !== 'undefined' && tagsSelected.length > 0) {
    let jsonTagProva = { and: [] }
    jsonTag = { or: [] }
    tagsSelected.forEach(tag => {
      jsonTagProva.and.push({ tagIds: tag.id })
    })
    jsonTag.or.push(jsonTagProva)
  } else {
    jsonTag = {}
  }
  return jsonTag
}

export const montarFiltroProva = valorDoForm => {
  const { textoFiltro, tagsSelected, status } = valorDoForm
  const expressao = `.*${textoFiltro}.*`
  let where = {
    and: [
      {
        or: [{ titulo: { regexp: expressao } }, { descricao: { regexp: expressao } }],
      },
      getTags(tagsSelected),
      getStatus(status),
    ],
  }
  const include = {
    relation: 'tags',
  }
  
  return {
    include,
    where,
  }
}

if (process.env.NODE_ENV === 'test') {
  exports.getTags = getTags
  exports.getStatus = getStatus
}
