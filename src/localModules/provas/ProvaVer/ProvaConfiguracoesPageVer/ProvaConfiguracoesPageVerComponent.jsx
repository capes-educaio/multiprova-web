import React, { Component } from 'react'

import Button from '@material-ui/core/Button'
import Divider from '@material-ui/core/Divider'
import Typography from '@material-ui/core/Typography'

import ViewListIcon from '@material-ui/icons/ViewList'
import CancelIcon from '@material-ui/icons/Cancel'

import { StepSimples } from 'localModules/Stepper'
import { provaUtils } from 'localModules/provas/ProvaUtils'

import { PaginaPaper } from 'common/PaginaPaper'
import { CampoView } from 'common/CampoView'
import { DisplayTags } from 'common/DisplayTags'

import { enumTemplate } from 'utils/enumTemplate'

import { propTypes } from './propTypes'

export class ProvaConfiguracoesPageVerComponent extends Component {
  static propTypes = propTypes

  render() {
    const { strings, value, classes, stepIndex } = this.props
    return (
      <StepSimples stepIndex={stepIndex}>
        {({ isCurrentStepValid, irParaPassoAnterior }) => (
          <PaginaPaper>
            <div className={classes.form}>
              <Typography color="primary" variant="subtitle2">
                {strings.descricao}
              </Typography>
              <Divider className={classes.divider} />
              <CampoView label={strings.descricao}>{value.descricao}</CampoView>
              <Typography color="primary" variant="subtitle2" className={classes.title}>
                {strings.cabecalhoLayout}
              </Typography>
              <Divider className={classes.divider} />
              <CampoView className={classes.campoView} label={strings.templateProva}>
                {value.template === enumTemplate.provaGeral && strings.templatesTipos[0]}
                {value.template === enumTemplate.provaComperve && strings.templatesTipos[1]}
              </CampoView>
              <CampoView className={classes.campoView} label={strings.titulo}>
                {value.titulo}
              </CampoView>
              {value.tema && (
                <CampoView className={classes.campoView} label={strings.assunto}>
                  {value.tema}
                </CampoView>
              )}
              {value.instituicao && (
                <CampoView className={classes.campoView} label={strings.instituicao}>
                  {value.instituicao}
                </CampoView>
              )}
              {value.nomeProfessor && <CampoView label={strings.professor}>{value.nomeProfessor}</CampoView>}
              <Typography color="primary" variant="subtitle2" className={classes.title}>
                {strings.outrasConfiguracoes}
              </Typography>
              <Divider className={classes.divider} />
              <CampoView className={classes.campoView} label={strings.tipoEmbaralhamento}>
                {strings.tiposEmbaralhamento[value.tipoEmbaralhamento]}
              </CampoView>
              {value.tagIds.length > 0 && (
                <CampoView label={strings.tags} htmlChildren>
                  <DisplayTags>{value.tagIds}</DisplayTags>
                </CampoView>
              )}
            </div>
            <Divider />
            <Button classes={{ root: classes.buttonBack }} onClick={isCurrentStepValid ? irParaPassoAnterior : null}>
              <ViewListIcon className={classes.buttonIcon} />
              {strings.questoes}
            </Button>
            <Button onClick={provaUtils.rotas.front.voltarParaProvas}>
              <CancelIcon className={classes.buttonIcon} />
              {strings.cancelar}
            </Button>
          </PaginaPaper>
        )}
      </StepSimples>
    )
  }
}
