import PropTypes from 'prop-types'

export const propTypes = {
  stepIndex: PropTypes.number.isRequired,
  gerarPdf: PropTypes.func.isRequired,
  // redux state
  value: PropTypes.object.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
