export const style = {
  form: { padding: '20px' },
  buttonBack: {
    margin: '20px 10px 20px 20px',
  },
  campoView: {
    marginBottom: 10,
  },
  buttonIcon: {
    marginRight: 5,
  },
  divider: {
    margin: '10px 0 20px 0',
  },
  title: {
    marginTop: 30,
  },
  tags: {
    marginTop: 20,
  },
}
