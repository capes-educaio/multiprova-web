export const style = theme => ({
  form: { padding: '20px' },
  botoesInferiores: {
    marginTop: 3,
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'flex-end',
  },
  fillBotoesInferiores: {
    marginTop: 2,
    [theme.breakpoints.up(480)]: {
      flexGrow: 1,
    },
  },
  button: {
    marginTop: 2,
    [theme.breakpoints.down(480)]: {
      flexGrow: 1,
    },
  },
})
