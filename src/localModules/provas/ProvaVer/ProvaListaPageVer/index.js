import { ProvaListaPageVerComponent } from './ProvaListaPageVerComponent'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import { withStyles } from '@material-ui/core/styles'

import { withProviderProvaContext } from 'localModules/provas/ProviderProva/context'

import { mapStateToProps, mapDispatchToProps } from './redux'
import { style } from './style'

export const ProvaListaPageVer = compose(
  withStyles(style),
  withRouter,
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withProviderProvaContext,
)(ProvaListaPageVerComponent)
