import React, { Component } from 'react'

import Paper from '@material-ui/core/Paper'

import ArrowForwardIcon from '@material-ui/icons/ArrowForward'

import { CancelarIcon } from 'localModules/icons/CancelarIcon'

import { StepSimples } from 'localModules/Stepper'

import { CabecalhoProva } from 'localModules/provas/CabecalhoProva'
import { ProviderProvaListaPage } from 'localModules/provas/ProviderProvaListaPage'
import { provaUtils } from 'localModules/provas/ProvaUtils'

import { MpButton } from 'common/MpButton'

import { enumTipoProva } from 'utils/enumTipoProva'

import { ListaQuestoesGrupoVer } from '../ListaQuestoesGrupoVer'
import { ListaGruposVer } from '../ListaGruposVer'
import { ListaQuestoesCriteriosVer } from '../ListaQuestoesCriteriosVer'

import { propTypes } from './propTypes'

export class ProvaListaPageVerComponent extends Component {
  static propTypes = propTypes

  render() {
    const { strings, classes, value, stepIndex, providerProvaContext } = this.props
    const { isGruposHabilitados } = providerProvaContext
    const { tipoEmbaralhamento } = value
    const naoEmbaralhar = tipoEmbaralhamento === 0
    const embaralhar = tipoEmbaralhamento === 1
    const embaralharComRestricao = tipoEmbaralhamento === 2
    const grupoDefaultIndex = 0
    const isDinamica = value.tipoProva === enumTipoProva.dinamica
    return (
      <ProviderProvaListaPage>
        {({ getGruposProcessados, expandirTodasCards, contrairTodasCards }) => {
          const gruposProcessados = getGruposProcessados()
          const grupoDefault = gruposProcessados[grupoDefaultIndex]
          return (
            <StepSimples stepIndex={stepIndex}>
              {({ irParaProximoPasso }) => (
                <div>
                  <CabecalhoProva
                    provaValue={value}
                    expandirTodasQuestoes={expandirTodasCards}
                    contrairTodasQuestoes={contrairTodasCards}
                  />
                  {(naoEmbaralhar || embaralhar || !isGruposHabilitados) && !isDinamica && (
                    <ListaQuestoesGrupoVer
                      key={grupoDefaultIndex}
                      questoesProcessadas={grupoDefault.questoes}
                      grupoIndex={grupoDefaultIndex}
                      isGrupoUnico
                    />
                  )}
                  {embaralharComRestricao && isGruposHabilitados && !isDinamica && (
                    <ListaGruposVer gruposProcessados={gruposProcessados} />
                  )}
                  {isDinamica && <ListaQuestoesCriteriosVer dinamica={value.dinamica} />}
                  <div className={classes.botoesInferiores}>
                    <Paper elevation={0} className={classes.fillBotoesInferiores} />
                    <MpButton
                      className={classes.button}
                      IconComponent={CancelarIcon}
                      onClick={provaUtils.rotas.front.voltarParaProvas}
                      marginLeft
                    >
                      {strings.cancelar}
                    </MpButton>
                    <MpButton
                      className={classes.button}
                      IconComponent={ArrowForwardIcon}
                      onClick={irParaProximoPasso}
                      marginLeft
                    >
                      {strings.proximo}
                    </MpButton>
                  </div>
                </div>
              )}
            </StepSimples>
          )
        }}
      </ProviderProvaListaPage>
    )
  }
}
