import React from 'react'
import { shallow } from 'enzyme'

import { ListaQuestoesGrupoVer } from '../index'
import { unwrap } from '@material-ui/core/test-utils'

jest.mock('react-beautiful-dnd', () => ({
  Droppable: props => <div>{props.children}</div>,
}))

jest.mock('@material-ui/core/Typography', () => props => <div>{props.children}</div>)

jest.mock('@material-ui/core/Divider', () => props => <div>{props.children}</div>)

const defaultProps = {
  questoesProcessadas: [],
  onQuestoesChange: jest.fn(),
  onQuestaoChange: jest.fn(),
  excluirQuestaoDaProva: jest.fn(),
  grupoIndex: 0,
  mostrarPin: true,
  isGrupoUnico: true,
  // style
  classes: {},
  // strings
  strings: {},
  // router
  history: {
    location: {
      state: {},
    },
    push: jest.fn(),
  },
}

const ComponentNaked = unwrap(ListaQuestoesGrupoVer)

describe('BarraSuperiorComponent unit tests', () => {
  test('Monta', () => {
    const component = shallow(<ComponentNaked {...defaultProps} />)
    component.unmount()
  })
})
