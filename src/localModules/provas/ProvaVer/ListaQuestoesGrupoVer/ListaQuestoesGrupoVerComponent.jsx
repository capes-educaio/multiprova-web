import React, { Component } from 'react'

import Typography from '@material-ui/core/Typography'

import { propTypes } from './propTypes'

import { QuestaoNaProvaVer } from 'localModules/provas/QuestaoNaProvaVer'

export class ListaQuestoesGrupoVerComponent extends Component {
  static propTypes = propTypes

  render() {
    const { strings, classes, questoesProcessadas, mostrarPin, isGrupoUnico } = this.props
    const vazio = questoesProcessadas.length < 1
    return (
      <div className={classes.root}>
        {questoesProcessadas.map((questao, index) => (
          <QuestaoNaProvaVer key={questao.id} questaoProcessada={questao} mostrarPin={mostrarPin} />
        ))}
        {vazio && (
          <div>
            {isGrupoUnico ? (
              <Typography align="center" className={classes.nadaCadastrado}>
                {strings.nenhumaQuestaoProva}
              </Typography>
            ) : (
              <Typography align="center" className={classes.nadaCadastradoGrupo}>
                {strings.nenhumaQuestaoGrupo}
              </Typography>
            )}
          </div>
        )}
      </div>
    )
  }
}
