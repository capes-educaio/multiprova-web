import React, { Component } from 'react'

import { GrupoVer } from '../GrupoVer'

import { propTypes } from './propTypes'

export class ListaGruposVerComponent extends Component {
  static propTypes = propTypes

  render() {
    return this.props.gruposProcessados.map((grupo, index) => (
      <GrupoVer key={grupo.id} instancia={grupo} index={index} />
    ))
  }
}
