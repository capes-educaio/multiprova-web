import Proptypes from 'prop-types'

export const propTypes = {
  gruposProcessados: Proptypes.array.isRequired,
  // style
  classes: Proptypes.object.isRequired,
}
