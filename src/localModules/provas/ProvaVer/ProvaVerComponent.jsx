import React, { Component } from 'react'

import ViewList from '@material-ui/icons/ViewList'
import Settings from '@material-ui/icons/Settings'

import { StepperSimples } from 'localModules/Stepper'

import { GeradorPDF } from 'common/GeradorPDF'

import { ProviderProva } from '../ProviderProva'

import { ProvaConfiguracoesPageVer } from './ProvaConfiguracoesPageVer'
import { ProvaListaPageVer } from './ProvaListaPageVer'
import { propTypes } from './propTypes'
import { provaUtils } from '../ProvaUtils'

export class ProvaVerComponent extends Component {
  static propTypes = propTypes

  _gerarPdf = () => {
    if (this._geradorPDFRef) this._geradorPDFRef.gerarPDF()
  }

  render() {
    const { strings, location, match, value, classes } = this.props
    const { activeStep } = location.state.hidden
    const steps = [
      { label: strings.stepAdicaoQuestao, icon: <ViewList /> },
      { label: strings.stepConfiguracoesProva, icon: <Settings /> },
    ]
    const { id } = match.params
    return (
      <ProviderProva>
        <StepperSimples classes={{ root: classes.stepper }} activeStep={activeStep} steps={steps} />
        <ProvaListaPageVer stepIndex={0} gerarPdf={this._gerarPdf} />
        <ProvaConfiguracoesPageVer stepIndex={1} gerarPdf={this._gerarPdf} />

        <GeradorPDF
          stringGerandoPDF={strings.gerandoPdfProva}
          stringsSalvarAntesDeImprimir={strings.salvarProvaAntesDeImprimir}
          url={provaUtils.rotas.back.getPreview(id)}
          dados={value}
          onRef={geradorPDFRef => {
            this._geradorPDFRef = geradorPDFRef
          }}
        />
      </ProviderProva>
    )
  }
}
