import { compose } from 'redux'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'

import { withStyles } from '@material-ui/core/styles'

import { mapStateToProps, mapDispatchToProps } from './redux'
import { style } from './style'
import { ListaQuestoesCriteriosVerComponent } from './ListaQuestoesCriteriosVerComponent'

export const ListaQuestoesCriteriosVer = compose(
  withStyles(style),
  withRouter,
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(ListaQuestoesCriteriosVerComponent)
