import Proptypes from 'prop-types'

export const propTypes = {
  dinamica: Proptypes.array,
  // style
  classes: Proptypes.object.isRequired,
  // strings
  strings: Proptypes.object.isRequired,
}
