import React, { Component } from 'react'

import Typography from '@material-ui/core/Typography'

import { propTypes } from './propTypes'

import { QuestaoNaProvaVer } from 'localModules/provas/QuestaoNaProvaVer'

export class ListaQuestoesCriteriosVerComponent extends Component {
  static propTypes = propTypes

  render() {
    const { strings, classes, dinamica } = this.props
    const vazio = dinamica ? dinamica.length < 1 : 0
    let numQuestoesNaProva = 0
    return (
      <div className={classes.root}>
        {dinamica.map((conjuntoCriterios, index) => {
          const { criterios } = conjuntoCriterios
          conjuntoCriterios.numPrimeiraQuestao = numQuestoesNaProva + 1
          conjuntoCriterios.numUltimaQuestao = numQuestoesNaProva + Number(criterios.quantidade)
          numQuestoesNaProva += Number(criterios.quantidade)

          return <QuestaoNaProvaVer key={index} questaoProcessada={conjuntoCriterios} />
        })}
        {vazio && (
          <div>
            <Typography align="center" className={classes.nadaCadastrado}>
              {strings.nenhumCriterioProva}
            </Typography>
          </div>
        )}
      </div>
    )
  }
}
