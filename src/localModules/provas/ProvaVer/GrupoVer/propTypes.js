import PropTypes from 'prop-types'

export const propTypes = {
  instancia: PropTypes.object.isRequired,
  index: PropTypes.number.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
