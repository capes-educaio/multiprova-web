import React, { Component } from 'react'

import { Grupo } from 'localModules/provas/Grupo'

import { ListaQuestoesGrupoVer } from '../ListaQuestoesGrupoVer'

import { propTypes } from './propTypes'

export class GrupoVerComponent extends Component {
  static propTypes = propTypes

  render() {
    const { instancia, index } = this.props
    return <Grupo instancia={instancia} index={index} ListaQuestoesGrupoComponent={ListaQuestoesGrupoVer} />
  }
}
