import { compose } from 'redux'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'

import { withStyles } from '@material-ui/core/styles'

import { style } from './style'
import { mapStateToProps, mapDispatchToProps } from './redux'
import { ProvaVerComponent } from './ProvaVerComponent'

export const ProvaVer = compose(
  withStyles(style),
  withRouter,
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(ProvaVerComponent)
