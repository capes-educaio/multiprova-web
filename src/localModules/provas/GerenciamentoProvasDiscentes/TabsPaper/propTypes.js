import PropTypes from 'prop-types'

export const propTypes = {
  labels: PropTypes.arrayOf(PropTypes.string),
}
