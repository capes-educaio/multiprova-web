import { TabsPaperComponent } from './TabsPaperComponent'
import { compose } from 'redux'
import { connect } from 'react-redux'
// import { withRouter } from 'react-router'
import { withStyles } from '@material-ui/core/styles'

import { mapStateToProps, mapDispatchToProps } from './redux'
import { style } from './style'

export const TabsPaper = compose(
  withStyles(style, { withTheme: true }),
  // withRouter,
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(TabsPaperComponent)
