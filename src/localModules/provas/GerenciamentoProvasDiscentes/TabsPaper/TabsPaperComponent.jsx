import React, { Component } from 'react'

import { CardDisplay } from 'common/CardDisplay'
import { CardGerenciamentoProvasDiscentes } from 'localModules/provas/GerenciamentoProvasDiscentes/CardGerenciamentoProvasDiscentes'

import Paper from '@material-ui/core/Paper'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'

import { setUrlState, getUrlState } from 'localModules/urlState'

import { propTypes } from './propTypes'

export class TabsPaperComponent extends Component {
  static propTypes = propTypes
  cardDisplayRef

  constructor(props) {
    super(props)
    const { labels } = this.props

    const urlState = getUrlState()
    const pathname = urlState.pathname.split('/')

    if (pathname.length > 2) {
      const tab = pathname[2]
      const Tab = tab.charAt(0).toUpperCase() + tab.substring(1)
      const index = labels.findIndex(i => i === Tab)

      if (index !== -1) this.state = { tabIndex: index }
    }
    else {
      this.state = { tabIndex: 0 }
    }
  }

  handleChange = (event, tabIndex) => {
    const { labels, endpoints } = this.props
    setUrlState({ pathname: `/provas/${labels[tabIndex].toLowerCase()}` })
    this.setState({ tabIndex })
    this.cardDisplayRef.fetchInstancias({
      quantidadeUrl: endpoints[tabIndex]['count'],
      fetchInstanciasUrl: endpoints[tabIndex]['instancias'],
    })
  }

  handleChangeIndex = tabIndex => {
    this.setState({ tabIndex })
  }

  render() {
    const { labels, endpoints } = this.props
    const { tabIndex } = this.state

    return (
      <div>
        <Paper square>
          <Tabs value={tabIndex} onChange={this.handleChange} indicatorColor="primary" textColor="primary" variant="fullWidth">
            {labels.map((label, index) => (
              <Tab key={index} label={label} />
            ))}
          </Tabs>
        </Paper>
        <CardDisplay
          key="provas"
          onRef={ref => (this.cardDisplayRef = ref)}
          component="div"
          CardComponent={CardGerenciamentoProvasDiscentes}
          numeroDeColunas={{
            extraSmall: 1,
            small: 1,
            medium: 1,
            large: 1,
            extraLarge: 1,
          }}
          nadaCadastrado={endpoints[tabIndex]['nadaCadastrado']}
          quantidadeUrl={endpoints[tabIndex]['count']}
          fetchInstanciasUrl={endpoints[tabIndex]['instancias']}
          quantidadePorPaginaInicial={12}
          quantidadePorPaginaOpcoes={[12, 24, 48, 96]}
        />
      </div>
    )
  }
}
