import React from 'react'
import { shallow } from 'enzyme'

import { GerenciamentoProvasDiscentesComponent } from '../GerenciamentoProvasDiscentesComponent'

const defaultProps = {
  // redux state
  usuarioAtual: { data: {} },
  // strngs
  strings: {
    gerenciamentoProvasDiscentes: {
      titulo: 'titulo',
    },
  },
  // style
  classes: {},
}

describe('GerenciamentoProvasDiscenteComponent', () => {
  it('Render e mount', () => {
    shallow(<GerenciamentoProvasDiscentesComponent {...defaultProps} />)
  })
})
