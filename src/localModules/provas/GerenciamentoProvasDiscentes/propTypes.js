import PropTypes from 'prop-types'

export const propTypes = {
  // redux state
  usuarioAtual: PropTypes.object.isRequired,
  // strngs
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
