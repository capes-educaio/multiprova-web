import React, { Component } from 'react'

import { TituloDaPagina } from 'common/TituloDaPagina'
import { PaginaPaper } from 'common/PaginaPaper'
import { TabsPaperComponent } from './TabsPaper/TabsPaperComponent'

import { propTypes } from './propTypes'

export class GerenciamentoProvasDiscentesComponent extends Component {
  static propTypes = propTypes

  
  render() {
    const { usuarioAtual, strings } = this.props

    return (
      <div role="main">
        <PaginaPaper>
          <TituloDaPagina>{strings.gerenciamentoProvasDiscentes.titulo}</TituloDaPagina>
        </PaginaPaper>
        <TabsPaperComponent
          labels={[
            strings.gerenciamentoProvasDiscentes.abertas,
            strings.gerenciamentoProvasDiscentes.agendadas,
            strings.gerenciamentoProvasDiscentes.finalizadas,
          ]}
          endpoints={[
            {
              nadaCadastrado: strings.naoEncontrouProva,
              count: `/instanciamentos/by-candidato-por-estado/count?candidatoId=${usuarioAtual.data.id}&estado=abertas`,
              instancias: `/instanciamentos/by-candidato-por-estado?candidatoId=${usuarioAtual.data.id}&estado=abertas`,
            },
            {
              nadaCadastrado: strings.naoEncontrouProva,
              count: `/instanciamentos/by-candidato-por-estado/count?candidatoId=${usuarioAtual.data.id}&estado=agendadas`,
              instancias: `/instanciamentos/by-candidato-por-estado?candidatoId=${usuarioAtual.data.id}&estado=agendadas`,
            },
            {
              nadaCadastrado: strings.naoEncontrouProva,
              count: `/instanciamentos/by-candidato-por-estado/count?candidatoId=${usuarioAtual.data.id}&estado=finalizadas`,
              instancias: `/instanciamentos/by-candidato-por-estado?candidatoId=${usuarioAtual.data.id}&estado=finalizadas`,
            },
          ]}
        />
      </div>
    )
  }
}
