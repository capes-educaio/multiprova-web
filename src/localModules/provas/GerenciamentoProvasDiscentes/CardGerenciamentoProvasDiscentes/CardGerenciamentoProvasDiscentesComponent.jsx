import React, { Component } from 'react'

import { GerenciamentoCard } from 'common/GerenciamentoCard'
import { ProvaDiscenteCard } from 'common/ProvaDiscenteCard'

import { propTypes } from './propTypes'

export class CardGerenciamentoProvasDiscentesComponent extends Component {
  static propTypes = propTypes

  render() {
    const { instancia, atualizarCardDisplay, strings } = this.props
    return (
      <GerenciamentoCard
        role="list"
        instancia={instancia}
        atualizarCardDisplay={atualizarCardDisplay}
        rotaDaInstanciaFront="/prova"
        rotaDaInstanciaBack="/provas"
        stringDesejaExcluirInstancia={strings.desejaExcluirProva}
        CardComponent={ProvaDiscenteCard}
        goToEdit={this._goToEdit}
      />
    )
  }
}
