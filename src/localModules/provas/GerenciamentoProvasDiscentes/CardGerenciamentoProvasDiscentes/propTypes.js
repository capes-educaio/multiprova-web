import PropTypes from 'prop-types'

export const propTypes = {
  instancia: PropTypes.object.isRequired,
  atualizarCardDisplay: PropTypes.func.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
