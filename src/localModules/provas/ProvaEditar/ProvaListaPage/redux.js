import { bindActionCreators } from 'redux'

export function mapStateToProps(state) {
  return {
    strings: state.strings,
    stepsValidation: state.StepperSimples__stepsValidation,
    value: state.ProviderProva__value,
    salvarNoBanco: state.ProviderProva__salvarNoBanco,
    updateProvaValue: state.ProviderProva__updateProvaValue,
  }
}

export function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch)
}
