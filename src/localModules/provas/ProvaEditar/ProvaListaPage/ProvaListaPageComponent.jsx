import React, { Component } from 'react'
import { DragDropContext } from 'react-beautiful-dnd'

import ArrowForwardIcon from '@material-ui/icons/ArrowForward'
import SaveIcon from '@material-ui/icons/Save'
import Visibility from '@material-ui/icons/Visibility'

import { StepSimples } from 'localModules/Stepper'
import { ProviderProvaContext } from 'localModules/provas/ProviderProva/context'
import { provaUtils } from 'localModules/provas/ProvaUtils'
import { CabecalhoProva } from 'localModules/provas/CabecalhoProva'
import { ProviderProvaListaPage } from 'localModules/provas/ProviderProvaListaPage'

import { MpButton } from 'common/MpButton'
import { MpActionBar } from 'common/MpActionBar'
import { GeradorPDF } from 'common/GeradorPDF'

import { ListaQuestoesGrupo } from '../ListaQuestoesGrupo'
import { ListaQuestoesCriterios } from '../ListaQuestoesCriterios'
import { ListaGrupos } from '../ListaGrupos'

import { propTypes } from './propTypes'
import { MenuProva } from '../MenuProva/'

export class ProvaListaPageComponent extends Component {
  static propTypes = propTypes
  static contextType = ProviderProvaContext
  _geradorPDFRef
  _urlImpressao
  _salvarButtonRef

  constructor(props) {
    const { getUrlVisualizacao } = provaUtils
    super(props)
    this._urlImpressao = getUrlVisualizacao()
    this.state = { template: '', previewSemEmbaralhar: true }
  }

  _ajustarQuestoesESalvarNoBanco = async () => {
    const { salvarNoBanco } = this.props
    await salvarNoBanco()
    if (this._salvarButtonRef && this._salvarButtonRef.endLoad) {
      this._salvarButtonRef.endLoad()
    }
  }

  dadosPreview = () => {
    const { value } = this.props
    const { tipoEmbaralhamento, ...outrosAtributos } = value
    const { previewSemEmbaralhar } = this.state
    const { processarProvaParaPreview } = provaUtils
    let valueParaProcessar = {
      ...outrosAtributos,
      tipoEmbaralhamento: previewSemEmbaralhar ? 0 : tipoEmbaralhamento,
    }

    const prova = processarProvaParaPreview(valueParaProcessar)
    const opcoesInstanciamento = previewSemEmbaralhar ? { embaralharAlternativas: false } : null
    return { prova, opcoesInstanciamento }
  }

  _gerarPreview = ({ impedirEmbaralhamento }) => () => {
    this.setState({ previewSemEmbaralhar: impedirEmbaralhamento }, () => this._geradorPDFRef.gerarPDF())
  }

  render() {
    const { strings, classes, stepsValidation, stepIndex, value, providerProvaContext } = this.props
    const areAllStepsValid = stepsValidation.every(isValid => isValid)
    const { tipoEmbaralhamento } = value
    const naoEmbaralhar = tipoEmbaralhamento === 0
    const embaralhar = tipoEmbaralhamento === 1
    const embaralharComRestricao = tipoEmbaralhamento === 2
    const grupoDefaultIndex = 0
    const mostrarUmGrupo = naoEmbaralhar || embaralhar || !providerProvaContext.isGruposHabilitados
    const mostrarGrupos = embaralharComRestricao && providerProvaContext.isGruposHabilitados
    const { handleVoltarDeProva } = providerProvaContext
    return (
      <div>
        <ProviderProvaListaPage>
          {({ getGruposProcessados, onDragEnd, expandirTodasCards, contrairTodasCards }) => {
            const gruposProcessados = getGruposProcessados()
            const grupoDefault = gruposProcessados[grupoDefaultIndex]
            return (
              <StepSimples stepIndex={stepIndex}>
                {({ irParaProximoPasso }) => {
                  return (
                    <div>
                      <CabecalhoProva
                        expandirTodasQuestoes={expandirTodasCards}
                        contrairTodasQuestoes={contrairTodasCards}
                      />
                      <MenuProva />
                      <DragDropContext onDragEnd={onDragEnd}>
                        {provaUtils.isProvaDinamica && <ListaQuestoesCriterios dinamica={value.dinamica} />}
                        {!provaUtils.isProvaDinamica && mostrarUmGrupo && (
                          <ListaQuestoesGrupo
                            key={grupoDefaultIndex}
                            questoesProcessadas={grupoDefault.questoes}
                            grupoIndex={grupoDefaultIndex}
                            isGrupoUnico
                          />
                        )}
                        {!provaUtils.isProvaDinamica && mostrarGrupos && (
                          <ListaGrupos gruposProcessados={gruposProcessados} />
                        )}
                      </DragDropContext>
                      <MpActionBar className={classes.botoesInferiores}>
                        {areAllStepsValid && (
                          <MpButton
                            className={classes.button}
                            id="testMpButtonSalvar"
                            onClick={this._ajustarQuestoesESalvarNoBanco}
                            IconComponent={SaveIcon}
                            marginLeft
                            loadOnClick
                            onRef={ref => (this._salvarButtonRef = ref)}
                          >
                            {strings.salvar}
                          </MpButton>
                        )}
                        <MpButton
                          id="visualizar-prova"
                          className={classes.button}
                          IconComponent={Visibility}
                          tooltip={strings.questoesEmbaralhadas}
                          marginLeft
                          onClick={this._gerarPreview({ impedirEmbaralhamento: false })}
                        >
                          {strings.visualizar}
                        </MpButton>
                        {!provaUtils.isProvaDinamica && (
                          <MpButton
                            id="visualizar-prova"
                            className={classes.button}
                            IconComponent={Visibility}
                            marginLeft
                            tooltip={strings.questoesNaoEmbaralhadas}
                            onClick={this._gerarPreview({ impedirEmbaralhamento: true })}
                          >
                            {strings.visualizarSemEmbaralhamento}
                          </MpButton>
                        )}

                        <MpButton
                          className={classes.button}
                          IconComponent={ArrowForwardIcon}
                          onClick={irParaProximoPasso}
                          marginLeft
                        >
                          {strings.proximo}
                        </MpButton>

                        <MpButton
                          id="sair-da-edicao-prova-lista-page"
                          className={classes.button}
                          onClick={() => handleVoltarDeProva()}
                          marginLeft
                        >
                          {strings.sairDaEdicao}
                        </MpButton>
                      </MpActionBar>
                    </div>
                  )
                }}
              </StepSimples>
            )
          }}
        </ProviderProvaListaPage>
        <GeradorPDF
          stringGerandoPDF={strings.gerandoPreview}
          url={this._urlImpressao}
          dados={this.dadosPreview()}
          onRef={geradorPDFRef => {
            this._geradorPDFRef = geradorPDFRef
          }}
        />
      </div>
    )
  }
}
