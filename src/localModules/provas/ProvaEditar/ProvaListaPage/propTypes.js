import PropTypes from 'prop-types'

export const propTypes = {
  stepIndex: PropTypes.number.isRequired,
  // redux state
  stepsValidation: PropTypes.arrayOf(PropTypes.bool).isRequired,
  value: PropTypes.object.isRequired,
  salvarNoBanco: PropTypes.func.isRequired,
  updateProvaValue: PropTypes.func.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
