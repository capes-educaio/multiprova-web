import { ProvaListaPageComponent } from './ProvaListaPageComponent'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import { withStyles } from '@material-ui/core/styles'

import { withProviderProvaContext } from 'localModules/provas/ProviderProva/context'

import { mapStateToProps, mapDispatchToProps } from './redux'
import { style } from './style'

export const ProvaListaPage = compose(
  withStyles(style),
  withRouter,
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withProviderProvaContext,
)(ProvaListaPageComponent)
