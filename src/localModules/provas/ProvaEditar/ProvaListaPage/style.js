export const style = theme => ({
  form: { padding: '20px' },
  botoesInferiores: {
    marginTop: 5,
  },
  button: {
    [theme.breakpoints.down(480)]: {
      flexGrow: 1,
    },
  },
})
