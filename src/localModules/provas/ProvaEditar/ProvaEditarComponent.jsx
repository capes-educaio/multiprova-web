import React, { Component, Fragment } from 'react'

import ViewList from '@material-ui/icons/ViewList'
import Settings from '@material-ui/icons/Settings'

import { StepperSimples } from 'localModules/Stepper'

import { Dialog } from 'common/Dialog'
import { Modal } from 'common/Modal'

import { ProviderProva } from '../ProviderProva'

import { AdicionarQuestao } from './AdicionarQuestao'
import { SelecaoAutomatica } from './SelecaoAutomatica'
import { ProvaConfiguracoesPage } from './ProvaConfiguracoesPage'
import { ProvaListaPage } from './ProvaListaPage'
import { propTypes } from './propTypes'

export class ProvaEditarComponent extends Component {
  static propTypes = propTypes

  setModal = (type, editMode) => {
    switch (type) {
      case 'selecaoAutomatica':
        return <SelecaoAutomatica selecaoAutomatica={true} />
      case 'selecaoCriterio':
        return <SelecaoAutomatica selecaoAutomatica={false} editMode={editMode} />
      case 'adicionarQuestao':
        return <AdicionarQuestao />
      default:
        return <AdicionarQuestao />
    }
  }
  render() {
    const { strings, location, classes } = this.props
    const { isOpenModal, activeStep, modalType, addWasTriggeredPorUmaQuestao } = location.state.hidden
    const steps = [
      { id: 'questoes-stepper-prova', label: strings.stepAdicaoQuestao, icon: <ViewList /> },
      { id: 'configuracoes-stepper-prova', label: strings.stepConfiguracoesProva, icon: <Settings /> },
    ]
    return (
      <ProviderProva>
        <StepperSimples classes={{ root: classes.stepper }} activeStep={activeStep} steps={steps} />
        <Fragment>
          <ProvaListaPage stepIndex={0} />
          <ProvaConfiguracoesPage stepIndex={1} />
        </Fragment>
        {isOpenModal === false && <Dialog />}
        <Modal open={isOpenModal}>{this.setModal(modalType, addWasTriggeredPorUmaQuestao)}</Modal>
      </ProviderProva>
    )
  }
}
