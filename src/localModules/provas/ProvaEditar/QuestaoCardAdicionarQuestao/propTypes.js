import PropTypes from 'prop-types'

export const propTypes = {
  instancia: PropTypes.object.isRequired,
  contexto: PropTypes.shape({
    questoesDaProva: PropTypes.array.isRequired,
  }).isRequired,
  // redux state
  questoesSelecionadasDicionario: PropTypes.object.isRequired,
  value: PropTypes.object.isRequired,
  updateProvaValue: PropTypes.func.isRequired,
  // redux actions
  selectQuestoesSelecionadasDicionario: PropTypes.func.isRequired,
  // style
  classes: PropTypes.object.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
  // router
  location: PropTypes.object.isRequired,
}
