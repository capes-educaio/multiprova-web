import { bindActionCreators } from 'redux'
import { actions } from 'utils/actions'

export function mapStateToProps(state) {
  return {
    strings: state.strings,
    questoesSelecionadasDicionario: state.questoesSelecionadasDicionario,
    value: state.ProviderProva__value,
    updateProvaValue: state.ProviderProva__updateProvaValue,
  }
}

export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      selectQuestoesSelecionadasDicionario: actions.select.questoesSelecionadasDicionario,
    },
    dispatch,
  )
}
