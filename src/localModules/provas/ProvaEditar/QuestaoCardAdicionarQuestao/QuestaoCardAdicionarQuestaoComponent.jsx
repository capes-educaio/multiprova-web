import React, { Component } from 'react'

import classNames from 'classnames'

import { updateUrlState } from 'localModules/urlState'

import { provaUtils } from 'localModules/provas/ProvaUtils'

import { QuestaoCard } from 'common/QuestaoCard'
import { BotaoExcluir } from 'common/BotaoExcluir'

import { propTypes } from './propTypes'

export class QuestaoCardAdicionarQuestaoComponent extends Component {
  static propTypes = propTypes

  get estaInclusa() {
    const { instancia, contexto } = this.props
    return contexto.questoesDaProva.some(questao => questao.id === instancia.id)
  }

  get estaSelecionada() {
    const { location, instancia, questoesSelecionadasDicionario } = this.props
    const { idsCardsSelecionados } = location.state.hidden
    return idsCardsSelecionados.includes(instancia.id) && questoesSelecionadasDicionario[instancia.id]
  }

  selecionarParaInclusao = () => {
    const { location, selectQuestoesSelecionadasDicionario, questoesSelecionadasDicionario, instancia } = this.props
    const { idsCardsSelecionados } = location.state.hidden
    questoesSelecionadasDicionario[instancia.id] = instancia
    selectQuestoesSelecionadasDicionario(questoesSelecionadasDicionario)
    if (!idsCardsSelecionados.includes(instancia.id)) idsCardsSelecionados.push(instancia.id)
    updateUrlState({ hidden: { idsCardsSelecionados } })
  }

  removerSelecao = () => {
    const { location, instancia } = this.props
    const { idsCardsSelecionados } = location.state.hidden
    const index = idsCardsSelecionados.indexOf(instancia.id)
    idsCardsSelecionados.splice(index, 1)
    updateUrlState({ hidden: { idsCardsSelecionados } })
  }

  onCardClick = () => {
    if (this.estaInclusa) return
    if (this.estaSelecionada) {
      this.removerSelecao()
    } else {
      this.selecionarParaInclusao()
    }
    return
  }

  _excluirQuestaoDaProva = questao => () => {
    const { value, updateProvaValue } = this.props
    updateProvaValue(provaUtils.excluirQuestaoDaProva({ prova: value, questao }))
  }

  render() {
    const { classes, instancia, strings } = this.props
    let cardClass = classes.card
    if (this.estaInclusa) {
      cardClass += ' ' + classes.cardIncluso
    }
    if (this.estaSelecionada) {
      cardClass += ' ' + classes.cardSelecionado
    }
    const BotaoExcluirQuestaoDaProva = this.estaInclusa ? (
      <BotaoExcluir
        instancia={instancia}
        excluir={this._excluirQuestaoDaProva}
        stringExcluir={strings.desejaExcluirQuestaoDaProva}
      />
    ) : null
    return (
      <div className={classNames(cardClass, 'card-questao')} onClick={this.onCardClick}>
        <QuestaoCard
          id={'card-adicionar-questao-' + instancia.id}
          questao={instancia}
          opcoes={BotaoExcluirQuestaoDaProva}
        />
      </div>
    )
  }
}
