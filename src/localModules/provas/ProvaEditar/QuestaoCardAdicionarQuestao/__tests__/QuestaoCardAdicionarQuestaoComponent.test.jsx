import React from 'react'
import { shallow } from 'enzyme'

import { ptBr } from 'utils/strings/ptBr'
import { enumTipoQuestao } from 'utils/enumTipoQuestao'

import { QuestaoCardAdicionarQuestao } from '../index'
import { unwrap } from '@material-ui/core/test-utils'

const questao = {
  id: '75cc4eff-6d71-4d55-821a-4ff6b8653695',
  dataCadastro: '2018-07-25T14:48:55.348Z',
  dataUltimaAlteracao: '2018-07-25T14:48:55.348Z',
  enunciado: '',
  dificuldade: 2,
  tagIds: [],
  tipo: enumTipoQuestao.tipoQuestaoMultiplaEscolha,
  multiplaEscolha: {
    respostaCerta: 'a',
    alternativas: [
      {
        texto: '<p>150 reais</p>',
        letra: 'a',
      },
      {
        texto: '<p>450 reais</p>',
        letra: 'b',
      },
    ],
  },
}

const location = {
  state: {
    strings: {},
    hidden: {
      idsCardsSelecionados: [],
    },
  },
}

const defaultProps = {
  instancia: questao,
  contexto: {
    questoesDaProva: [],
  },
  onRef: jest.fn(),
  // style
  classes: {},
  // strings
  strings: ptBr,
  // redux state
  excluirQuestaoDaProva: jest.fn(),
  questoesSelecionadasDicionario: {},
  value: {},
  updateProvaValue: jest.fn(),
  // redux actions
  selectQuestoesSelecionadasDicionario: jest.fn(),
  // router
  history: {
    push: jest.fn(),
    location,
  },
  location,
}

const ComponentNaked = unwrap(QuestaoCardAdicionarQuestao)

describe('Testando <QuestaoCardAdicionarQuestao />', () => {
  it('Deve montar o componente', () => {
    const wrapper = shallow(<ComponentNaked {...defaultProps} />)
    wrapper.unmount()
  })
})
