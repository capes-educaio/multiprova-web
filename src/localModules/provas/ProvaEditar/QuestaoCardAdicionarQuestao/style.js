export const style = theme => ({
  card: {
    '&:hover': {
      boxShadow: '0px 2px 10px grey',
    },
    cursor: 'pointer',
    userSelect: 'none',
  },
  cardSelecionado: {
    outline: 'solid 2px chartreuse !important',
    boxShadow: 'none !important',
  },
  cardIncluso: {
    outline: 'none !important',
    boxShadow: 'none !important',
    opacity: '0.6',
  },
  menuOpcoes: {
    position: 'absolute',
  },
  menuContainer: {
    position: 'relative',
  },
  menuIcon: {
    marginRight: '3px',
    color: theme.palette.primary.main,
  },
})
