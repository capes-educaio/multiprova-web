import React, { Component } from 'react'
import uid from 'uuid/v4'

import Popover from '@material-ui/core/Popover'
import MenuItem from '@material-ui/core/MenuItem'
import MenuList from '@material-ui/core/MenuList'

import { propTypes } from './propTypes'

export class MenuListGruposComponent extends Component {
  static propTypes = propTypes

  state = {}

  _handleToggleHabilitarGrupos = () => {
    const { handleClose, value } = this.props
    handleClose()
    const { confirmarUnificarGrupos } = this.props
    if (value.grupos.length > 1) confirmarUnificarGrupos(this._toggleHabilitarGrupos)()
    else this._toggleHabilitarGrupos()
  }

  _toggleHabilitarGrupos = () => {
    const { isGruposHabilitados, updateContext } = this.props.providerProvaContext
    updateContext({ isGruposHabilitados: !isGruposHabilitados })
  }

  _adicionarGrupo = () => {
    const { providerProvaContext, handleClose, value, updateProvaValue } = this.props
    const { isGruposHabilitados } = providerProvaContext
    handleClose()
    if (!isGruposHabilitados) this._toggleHabilitarGrupos()
    value.grupos.push({ id: uid(), nome: '', questoes: [] })
    updateProvaValue(value)
  }

  render() {
    const { providerProvaContext, strings, open, anchorEl, handleClose, classes } = this.props
    const { isGruposHabilitados } = providerProvaContext
    return (
      <Popover
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{ vertical: 'bottom', horizontal: 'left' }}
        transformOrigin={{ vertical: 'top', horizontal: 'left' }}
      >
        <MenuList className={classes.menu}>
          <MenuItem className={classes.menuItem} onClick={this._handleToggleHabilitarGrupos}>
            {isGruposHabilitados ? strings.desabilitarGrupos : strings.habilitarGrupos}
          </MenuItem>
          <MenuItem className={classes.menuItem} onClick={this._adicionarGrupo} disabled={!isGruposHabilitados}>
            {strings.addGrupos}
          </MenuItem>
        </MenuList>
      </Popover>
    )
  }
}
