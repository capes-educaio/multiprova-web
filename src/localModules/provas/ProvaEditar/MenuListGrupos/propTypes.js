import PropTypes from 'prop-types'

export const propTypes = {
  // redux state
  strings: PropTypes.object.isRequired,
  confirmarUnificarGrupos: PropTypes.func.isRequired,
  value: PropTypes.object.isRequired,
  updateProvaValue: PropTypes.func.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
