import { MenuListGruposComponent } from './MenuListGruposComponent'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'

import { withProviderProvaContext } from 'localModules/provas/ProviderProva/context'

import { mapStateToProps, mapDispatchToProps } from './redux'
import { style } from './style'

export const MenuListGrupos = compose(
  withStyles(style),
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withProviderProvaContext,
)(MenuListGruposComponent)
