export const style = theme => ({
  menu: {
    padding: '0px',
  },
  menuItem: {
    borderRadius: '0px',
    borderStyle: 'solid',
    borderWidth: '0px 0px 1px 0px',
    borderColor: theme.palette.steelBlueContrast,
    padding: '5px 20px 5px 20px',
    fontSize: '15px',
  },
})
