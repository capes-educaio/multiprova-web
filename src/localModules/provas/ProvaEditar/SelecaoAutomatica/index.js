import { compose } from 'redux'
import { withStyles } from '@material-ui/core/styles'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'

import { mapStateToProps, mapDispatchToProps } from './redux'
import { style } from './style'
import { SelecaoAutomaticaComponent } from './SelecaoAutomaticaComponent'

export const SelecaoAutomatica = compose(
  withStyles(style),
  withRouter,
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(SelecaoAutomaticaComponent)
