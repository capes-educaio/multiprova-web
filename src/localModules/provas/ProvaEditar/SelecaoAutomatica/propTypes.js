import PropTypes from 'prop-types'

export const propTypes = {
  editMode: PropTypes.bool,
  indexGrupo: PropTypes.number.isRequired,
  selecaoAutomatica: PropTypes.bool.isRequired,
  // redux state
  usuarioAtual: PropTypes.object.isRequired,
  questoesSelecionadasDicionario: PropTypes.object.isRequired,
  value: PropTypes.object.isRequired,
  updateProvaValue: PropTypes.func.isRequired,
  // redux actions
  selectQuestoesSelecionadasDicionario: PropTypes.func.isRequired,
  selectValorProvaNaVolta: PropTypes.func.isRequired,
  // style
  classes: PropTypes.object.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
  // router
  location: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
}
