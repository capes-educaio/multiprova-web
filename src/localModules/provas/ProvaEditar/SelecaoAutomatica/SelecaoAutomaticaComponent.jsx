import React from 'react'
import { Typography } from '@material-ui/core'
import { SelecaoAutomaticaBusca } from 'common/SelecaoAutomaticaBusca'
import { FormQuestaoFiltroPadrao } from 'common/FormQuestaoFiltroPadrao'
import { FormQuestaoSelecaoAutomaticaExpandido } from 'common/FormQuestaoSelecaoAutomaticaExpandido'
import { Dialog } from 'common/Dialog'
import { AlertaDeConfirmacao } from 'common/AlertaDeConfirmacao'
import { updateUrlState } from 'localModules/urlState'
import { propTypes } from './propTypes'
import { defaultProps } from './defaultProps'
import { get } from 'api'
import { openDialogAviso } from 'utils/compositeActions/openDialogAviso'
import { enumTipoQuestao } from 'utils/enumTipoQuestao'
import { provaUtils } from '../../ProvaUtils'
import { enumSeloQuestao } from 'utils/enumSeloQuestao'

export class SelecaoAutomaticaComponent extends React.Component {
  static propTypes = propTypes
  static defaultProps = defaultProps

  _cardDisplayRef

  state = {
    anchorCriarQuestao: null,
    alertaDeConfirmacaoAberto: false,
    questoes: [],
  }

  _fecharModal = () =>
    updateUrlState({ hidden: { isOpenModal: false, selecaoAutomatica: false, isToUpdateCriterio: false } })

  _getTipo = tipo => {
    if (provaUtils.isProvaDinamica) return tipo ? { tipo } : { tipo: { neq: enumTipoQuestao.tipoQuestaoBloco } }
    else return tipo ? { tipo } : {}
  }

  _processarFiltroTags = tagsSelected => {
    return (
      tagsSelected &&
      tagsSelected.length > 0 && {
        and: tagsSelected.map(tag => ({
          tagIds: {
            inq: [tag.id],
          },
        })),
      }
    )
  }

  processarFiltroParaRequisicao = ({ dificuldade = false, anoEscolar = false, tipo = false, tagsSelected = [] }) => {
    const dificuldadeSpread = dificuldade ? { dificuldade } : {}
    const anoEscolarSpread = anoEscolar ? { anoEscolar } : {}
    const tipoSpread = this._getTipo(tipo)
    const tagsSpread = this._processarFiltroTags(tagsSelected)
    const selo = { selo: enumSeloQuestao.validada }
    return { ...dificuldadeSpread, ...anoEscolarSpread, ...tipoSpread, ...tagsSpread, ...selo }
  }

  processarNovoCriterio = ({
    quantidadeDeQuestoes = 1,
    dificuldade = false,
    anoEscolar = false,
    tipo = false,
    tagsSelected = [],
  }) => {
    const dificuldadeSpread = dificuldade ? { dificuldade } : {}
    const anoEscolarSpread = anoEscolar ? { anoEscolar } : {}
    const tipoSpread = tipo ? { tipo } : {}
    const tagsSpread = tagsSelected && tagsSelected.length ? { tagIds: tagsSelected.map(tag => tag.id) } : {}
    return {
      id: Math.random(),
      tipo: 'criterio',
      criterios: {
        ...dificuldadeSpread,
        ...anoEscolarSpread,
        ...tipoSpread,
        ...tagsSpread,
        quantidade: Number(quantidadeDeQuestoes),
      },
    }
  }

  _handleChangeOrAddCriterios = (criterios, cb) => {
    const { value, updateProvaValue, usuarioAtual, strings, location } = this.props
    const { indexQuestaoTriggerAdd, addWasTriggeredPorUmaQuestao, isToUpdateCriterio } = location.state.hidden
    const { quantidadeDeQuestoes, ...filtro } = criterios
    const filtroProcessado = this.processarFiltroParaRequisicao(filtro)
    get(`/usuarios/${usuarioAtual.data.id}/questoes/count`, { where: filtroProcessado })
      .then(({ data: { count } }) => {
        if (count >= quantidadeDeQuestoes) {
          const questaoCriterio = this.processarNovoCriterio({ ...filtro, quantidadeDeQuestoes })
          if (isToUpdateCriterio) {
            value.dinamica[indexQuestaoTriggerAdd] = questaoCriterio
          } else {
            if (!addWasTriggeredPorUmaQuestao) {
              value.dinamica.push(questaoCriterio)
            } else {
              value.dinamica.splice(indexQuestaoTriggerAdd + 1, 0, questaoCriterio)
            }
          }
          updateProvaValue({ ...value })
          this._fecharModal()
          cb && cb(true)
        } else {
          openDialogAviso({
            texto: strings.alertaCriteriosQuestoesInsuficiente(count),
            titulo: strings.erro,
          })
          cb && cb(false)
        }
      })
      .catch(erro => {
        console.error(erro)
        cb && cb(false)
      })
  }

  _validarResposta = (qtnRequerida, qtnDisponivel) => Number(qtnRequerida) === qtnDisponivel

  _handleAdicionarQuestoes = (filtro, cb) => {
    const { value, strings } = this.props
    const questoesSelecionadas = value.grupos.reduce((arr, act) => [...arr, ...act.questoes], []).map(({ id }) => id)
    const filtroProcessado = this.processarFiltroParaRequisicao(filtro)
    const { quantidadeDeQuestoes } = filtro
    get('questoes/selecao-automatica', { ...filtroProcessado, questoesSelecionadas, quantidadeDeQuestoes })
      .then(response => {
        const questoesAA = response.data
        if (questoesAA.length > 0) {
          const validar = this._validarResposta(filtro.quantidadeDeQuestoes, questoesAA.length)
          if (!validar) {
            this.setState({
              alertaDeConfirmacaoAberto: true,
              selecaoAutomatica: false,
              qtnDisponivel: questoesAA.length,
              questoes: questoesAA,
            })
            cb && cb(false)
          } else {
            this.adicionarQuestoes(questoesAA)
            cb && cb(true)
          }
        } else {
          openDialogAviso({
            texto: strings.alertaCriteriosQuestoesInsuficiente(questoesAA.length),
            titulo: strings.erro,
          })
          cb && cb(false)
        }
      })
      .catch(erro => {
        console.warn(erro)
        cb && cb(false)
      })
  }

  adicionarQuestoes = (questoes = []) => {
    const { value, updateProvaValue } = this.props
    const questoesDoGrupo = value.grupos[0].questoes
    questoes.forEach(questao => {
      questao.fixa = false
      if (questao.tipo === enumTipoQuestao.tipoQuestaoBloco)
        questao.bloco.questoes.forEach(quest => (quest.fixa = false))
      questoesDoGrupo.push(questao)
    })
    updateProvaValue(value)
    updateUrlState({ hidden: { idsCardsSelecionados: [] } })
    this._fecharModal()
  }

  _onCancelar = () => {
    updateUrlState({ hidden: { idsCardsSelecionados: [] } })
    this._fecharModal()
  }

  handleCloseConfirmarSelecao = button => {
    if (button === true) {
      this.adicionarQuestoes(this.state.questoes)
    }
    this.setState({ alertaDeConfirmacaoAberto: false })
    this._fecharModal()
  }

  render() {
    const { strings, classes, selecaoAutomatica = true, location, value, editMode = false } = this.props
    const { indexQuestaoTriggerAdd = 0, isToUpdateCriterio } = location.state.hidden
    const { alertaDeConfirmacaoAberto, qtnDisponivel } = this.state

    return (
      <div className={classes.principal}>
        <Typography variant="h6" gutterBottom>
          {selecaoAutomatica
            ? strings.adicionarQuestaoAutomaticamente
            : editMode
              ? strings.editarCriterio
              : strings.criarCriterio}
        </Typography>
        <SelecaoAutomaticaBusca
          editMode={editMode}
          criterios={isToUpdateCriterio && value.dinamica[indexQuestaoTriggerAdd].criterios}
          FormPadrao={FormQuestaoFiltroPadrao}
          FormExpandido={FormQuestaoSelecaoAutomaticaExpandido}
          adicionarQuestoes={selecaoAutomatica ? this._handleAdicionarQuestoes : this._handleChangeOrAddCriterios}
          onCancelar={this._onCancelar}
        />
        <Dialog />
        <AlertaDeConfirmacao
          alertaAberto={alertaDeConfirmacaoAberto}
          handleClose={this.handleCloseConfirmarSelecao}
          stringNomeInstancia={strings.selecaoAutomatica}
          descricaoSelected={strings.alertaSelecaoAutomaticaInsuficiente(qtnDisponivel)}
        />
      </div>
    )
  }
}
