import PropTypes from 'prop-types'

export const propTypes = {
  gruposProcessados: PropTypes.array.isRequired,
  instancia: PropTypes.object.isRequired,
  dragHandleProps: PropTypes.object.isRequired,
  index: PropTypes.number.isRequired,
  // redux state
  value: PropTypes.object.isRequired,
  updateProvaValue: PropTypes.func.isRequired,
  // redux state
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
