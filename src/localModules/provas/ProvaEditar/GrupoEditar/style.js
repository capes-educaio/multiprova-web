export const style = theme => ({
  buttonBorder: {
    borderStyle: 'solid',
    borderWidth: '0px 0px 0px 1px',
    borderColor: theme.palette.gelo,
  },
})
