import React, { Component } from 'react'
import uid from 'uuid/v4'

import { Add, Notes, DeleteSweep } from '@material-ui/icons'

import { updateUrlState } from 'localModules/urlState'
import { Grupo } from 'localModules/provas/Grupo'

import { MpIconButton } from 'common/MpIconButton'
import { BotaoExcluir } from 'common/BotaoExcluir'

import { ListaQuestoesGrupo } from '../ListaQuestoesGrupo'
import { provaUtils } from 'localModules/provas'

import { propTypes } from './propTypes'

export class GrupoEditarComponent extends Component {
  static propTypes = propTypes
  _id = uid()

  constructor(props) {
    super(props)
    this.state = {
      paratextosHabilitados: false,
    }
  }

  componentDidMount() {
    const { instancia } = this.props
    if (provaUtils.grupoTemParatexto(instancia)) {
      this.setState({ paratextosHabilitados: true })
    }
  }

  _addQuestao = index => () => {
    updateUrlState({
      hidden: {
        isOpenModal: true,
        indexQuestaoTriggerAdd: 0,
        indexGrupoTriggerAdd: index,
        addWasTriggeredPorUmaQuestao: false,
      },
    })
  }

  _habilitarParatextos = () => {
    if (this.state.paratextosHabilitados) {
      const { value, updateProvaValue, index } = this.props
      delete value.grupos[index].paratextoInicial
      delete value.grupos[index].paratextoFinal
      updateProvaValue(value)
    }
    this.setState({ paratextosHabilitados: !this.state.paratextosHabilitados })
  }

  _apagarGrupo = index => () => {
    const { value, updateProvaValue } = this.props
    const { grupos } = value
    if (index === 0 && grupos.length === 1) {
      console.log('Operação inválida, não é possível apagar o último grupo')
      return
    }
    value.grupos.splice(index, 1)
    updateProvaValue(value)
    return
  }

  _salvarParatextoInicial = conteudo => {
    const { value, updateProvaValue, index } = this.props
    value.grupos[index].paratextoInicial = conteudo
    updateProvaValue(value)
  }
  _salvarParatextoFinal = conteudo => {
    const { value, updateProvaValue, index } = this.props
    value.grupos[index].paratextoFinal = conteudo
    updateProvaValue(value)
  }

  render() {
    const { strings, classes, gruposProcessados, instancia, dragHandleProps, index } = this.props
    const { paratextosHabilitados } = this.state
    const { _salvarParatextoInicial, _salvarParatextoFinal } = this
    const ultimoGrupo = gruposProcessados.length === 1
    const actions = [
      <MpIconButton
        key={this._id + 'paratexto'}
        tooltip={paratextosHabilitados ? strings.desabilitarParatextos : strings.habilitarParatextos}
        onClick={() => this._habilitarParatextos()}
        IconComponent={paratextosHabilitados ? DeleteSweep : Notes}
        color="darkBlue"
        className={classes.buttonBorder}
      />,
      <MpIconButton
        key={this._id + 'add'}
        tooltip={strings.provaAddQuestaoGrupo}
        onClick={this._addQuestao(index)}
        IconComponent={Add}
        color="darkBlue"
        className={classes.buttonBorder}
      />,
    ]
    if (!ultimoGrupo)
      actions.unshift(
        <BotaoExcluir
          key={this._id + 'excluir'}
          variant="mp"
          color="darkBlue"
          instancia={index}
          excluir={this._apagarGrupo}
          stringExcluir={strings.desejaExcluirGrupo}
          className={classes.iconButton}
          buttonClass={classes.buttonBorder}
          buttonProps={{ tooltip: strings.provaExcluirGrupo }}
        />,
      )
    return (
      <Grupo
        instancia={instancia}
        dragHandleProps={dragHandleProps}
        index={index}
        ListaQuestoesGrupoComponent={ListaQuestoesGrupo}
        actions={actions}
        paratextosHabilitados={paratextosHabilitados}
        salvarParatextoInicial={_salvarParatextoInicial}
        salvarParatextoFinal={_salvarParatextoFinal}
      />
    )
  }
}
