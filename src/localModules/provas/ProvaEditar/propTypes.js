import PropTypes from 'prop-types'

export const propTypes = {
  // router
  location: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
}
