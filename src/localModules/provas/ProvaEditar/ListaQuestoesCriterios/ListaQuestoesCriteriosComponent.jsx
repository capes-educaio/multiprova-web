import React, { Component } from 'react'
import { Droppable, Draggable } from 'react-beautiful-dnd'

import Typography from '@material-ui/core/Typography'

import { propTypes } from './propTypes'

import { QuestaoNaProvaEditar } from 'localModules/provas/QuestaoNaProvaEditar'

export class ListaQuestoesCriteriosComponent extends Component {
  static propTypes = propTypes

  render() {
    const { strings, classes, dinamica } = this.props
    const { excluirQuestaoDaProva, definirQuestaoClicada } = this.props
    const vazio = dinamica ? dinamica.length < 1 : 0
    const idUnico = '0'
    let numQuestoesNaProva = 0
    return (
      <div className={classes.root}>
        <Droppable type="criterios" droppableId={idUnico}>
          {provided => (
            <div ref={provided.innerRef} {...provided.droppableProps}>
              {dinamica &&
                dinamica.map((conjuntoCriterios, index) => {
                  const { criterios } = conjuntoCriterios
                  conjuntoCriterios.numPrimeiraQuestao = numQuestoesNaProva + 1
                  conjuntoCriterios.numUltimaQuestao = numQuestoesNaProva + Number(criterios.quantidade)
                  numQuestoesNaProva += Number(criterios.quantidade)
                  return (
                    <Draggable
                      key={conjuntoCriterios.id}
                      draggableId={conjuntoCriterios.id}
                      index={index}
                      type="criterios"
                    >
                      {provided => (
                        <div ref={provided.innerRef} key={index} {...provided.draggableProps}>
                          <QuestaoNaProvaEditar
                            key={conjuntoCriterios.id}
                            questaoProcessada={conjuntoCriterios}
                            excluirQuestaoDaProva={excluirQuestaoDaProva}
                            definirQuestaoClicada={definirQuestaoClicada}
                            dragHandleProps={provided.dragHandleProps}
                          />
                        </div>
                      )}
                    </Draggable>
                  )
                })}
              {vazio && (
                <div>
                  <Typography align="center" className={classes.nadaCadastrado}>
                    {strings.nenhumaQuestaoProva}
                  </Typography>
                </div>
              )}
              {provided.placeholder}
            </div>
          )}
        </Droppable>
      </div>
    )
  }
}
