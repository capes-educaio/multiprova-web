export const style = theme => ({
  root: {},
  actions: {
    display: 'flex',
  },
  nadaCadastrado: {
    padding: 10,
    margin: '10px 0 8px 0',
    backgroundColor: theme.palette.gelo,
  },
  nadaCadastradoGrupo: {
    padding: 10,
    backgroundColor: theme.palette.gelo,
  },
})
