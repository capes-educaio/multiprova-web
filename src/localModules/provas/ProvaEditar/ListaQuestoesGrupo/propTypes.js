import Proptypes from 'prop-types'

export const propTypes = {
  questoesProcessadas: Proptypes.array,
  grupoIndex: Proptypes.number.isRequired,
  isGrupoUnico: Proptypes.bool,
  // style
  classes: Proptypes.object.isRequired,
  // strings
  strings: Proptypes.object.isRequired,
}
