import React, { Component } from 'react'
import { Droppable, Draggable } from 'react-beautiful-dnd'

import Typography from '@material-ui/core/Typography'

import { propTypes } from './propTypes'

import { QuestaoNaProvaEditar } from 'localModules/provas/QuestaoNaProvaEditar'

export class ListaQuestoesGrupoComponent extends Component {
  static propTypes = propTypes

  render() {
    const { strings, classes, questoesProcessadas, grupoIndex, isGrupoUnico } = this.props
    const { excluirQuestaoDaProva, definirQuestaoClicada } = this.props
    const vazio = questoesProcessadas.length < 1
    return (
      <div className={classes.root}>
        <Droppable type="questao" droppableId={String(grupoIndex)}>
          {provided => (
            <div ref={provided.innerRef} {...provided.droppableProps}>
              {questoesProcessadas.map((questao, index) => (
                <Draggable key={questao.id} draggableId={questao.id} index={index} type="questao">
                  {provided => (
                    <div ref={provided.innerRef} key={questao.id} {...provided.draggableProps}>
                      <QuestaoNaProvaEditar
                        key={questao.id}
                        questaoProcessada={questao}
                        excluirQuestaoDaProva={excluirQuestaoDaProva}
                        definirQuestaoClicada={definirQuestaoClicada}
                        dragHandleProps={provided.dragHandleProps}
                      />
                    </div>
                  )}
                </Draggable>
              ))}
              {vazio && (
                <div>
                  {isGrupoUnico ? (
                    <Typography align="center" className={classes.nadaCadastrado}>
                      {strings.nenhumaQuestaoProva}
                    </Typography>
                  ) : (
                    <Typography align="center" className={classes.nadaCadastradoGrupo}>
                      {strings.nenhumaQuestaoGrupo}
                    </Typography>
                  )}
                </div>
              )}
              {provided.placeholder}
            </div>
          )}
        </Droppable>
      </div>
    )
  }
}
