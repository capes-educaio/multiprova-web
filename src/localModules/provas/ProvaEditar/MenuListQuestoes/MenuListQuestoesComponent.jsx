import React, { Component } from 'react'

import Popover from '@material-ui/core/Popover'
import MenuItem from '@material-ui/core/MenuItem'
import MenuList from '@material-ui/core/MenuList'

import { provaUtils } from 'localModules/provas/ProvaUtils'

import { propTypes } from './propTypes'
import { defaultProps } from './defaultProps'

export class MenuListQuestoesComponent extends Component {
  static propTypes = propTypes
  static defaultProps = defaultProps

  get _irParaCriarQuestao() {
    const { location, selectValorProvaNaVolta, value } = this.props
    return provaUtils.irParaCriarQuestao({ location, selectValorProvaNaVolta, value })
  }

  render() {
    const { strings, open, anchorEl, handleClose, classes, tipoQuestoesHabilitados } = this.props
    const itemMap = {
      multiplaEscolha: (
        <MenuItem
          className={classes.menuItem}
          key="multiplaEscolha"
          onClick={this._irParaCriarQuestao('multipla-escolha')}
        >
          {strings.multiplaEscolha}
        </MenuItem>
      ),
      bloco: (
        <MenuItem className={classes.menuItem} key="bloco" onClick={this._irParaCriarQuestao('bloco')}>
          {strings.bloco}
        </MenuItem>
      ),
      vouf: (
        <MenuItem className={classes.menuItem} key="vouf" onClick={this._irParaCriarQuestao('vouf')}>
          {strings.vouf}
        </MenuItem>
      ),
      discursiva: (
        <MenuItem className={classes.menuItem} key="discursiva" onClick={this._irParaCriarQuestao('discursiva')}>
          {strings.discursiva}
        </MenuItem>
      ),
      associacaoDeColunas: (
        <MenuItem
          className={classes.menuItem}
          key="associacaodecolunas"
          onClick={this._irParaCriarQuestao('associacao-de-colunas')}
        >
          {strings.associacaoDeColunas}
        </MenuItem>
      ),
      redacao: (
        <MenuItem className={classes.menuItem} key="redacao" onClick={this._irParaCriarQuestao('redacao')}>
          {strings.redacao}
        </MenuItem>
      ),
    }
    const items = []
    Object.keys(itemMap).map(tipo => tipoQuestoesHabilitados[tipo] && items.push(itemMap[tipo]))
    return (
      <Popover
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
        transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      >
        <MenuList className={classes.menu}>{items}</MenuList>
      </Popover>
    )
  }
}
