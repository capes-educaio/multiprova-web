import PropTypes from 'prop-types'

export const propTypes = {
  tipoQuestoesHabilitados: PropTypes.object.isRequired,
  // redux state
  value: PropTypes.object.isRequired,
  // redux actions
  selectValorProvaNaVolta: PropTypes.func.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
