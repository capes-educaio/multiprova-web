import React, { Component } from 'react'
import { Droppable, Draggable } from 'react-beautiful-dnd'

import { GrupoEditar } from '../GrupoEditar'

import { propTypes } from './propTypes'

export class ListaGruposComponent extends Component {
  static propTypes = propTypes

  render() {
    const { gruposProcessados } = this.props
    return (
      <div>
        <Droppable type="grupo" droppableId="grupos">
          {provided => (
            <div ref={provided.innerRef} {...provided.droppableProps}>
              {gruposProcessados.map((grupo, index) => (
                <Draggable key={grupo.id} draggableId={grupo.id} index={index} type="grupo">
                  {provided => (
                    <div ref={provided.innerRef} key={grupo.id} {...provided.draggableProps}>
                      <GrupoEditar
                        gruposProcessados={gruposProcessados}
                        instancia={grupo}
                        dragHandleProps={provided.dragHandleProps}
                        index={index}
                      />
                    </div>
                  )}
                </Draggable>
              ))}
              {provided.placeholder}
            </div>
          )}
        </Droppable>
      </div>
    )
  }
}
