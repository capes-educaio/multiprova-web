import Proptypes from 'prop-types'

export const propTypes = {
  gruposProcessados: Proptypes.array.isRequired,
  // strings
  strings: Proptypes.object.isRequired,
  // style
  classes: Proptypes.object.isRequired,
}
