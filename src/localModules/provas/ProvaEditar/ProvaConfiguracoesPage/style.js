export const style = theme => ({
  form: { padding: '20px' },
  buttonSave: {},
  buttonBack: {},
  divider: {
    margin: '10px 0 20px 0',
  },
  title: {
    marginTop: 30,
  },
  input: {
    marginBottom: 20,
  },
  tags: {
    marginTop: 20,
  },
})
