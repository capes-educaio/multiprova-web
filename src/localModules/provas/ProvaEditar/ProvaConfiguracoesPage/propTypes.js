import PropTypes from 'prop-types'

export const propTypes = {
  stepIndex: PropTypes.number.isRequired,
  // redux state
  salvarNoBanco: PropTypes.func.isRequired,
  updateProvaValue: PropTypes.func.isRequired,
  value: PropTypes.object.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
