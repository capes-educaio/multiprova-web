import React, { Component, Fragment } from 'react'

import { FormControlLabel, Radio, RadioGroup, MenuItem, Divider, Typography, TextField } from '@material-ui/core'

import ViewListIcon from '@material-ui/icons/ViewList'
import SaveIcon from '@material-ui/icons/Save'

import { SimpleReduxForm, CampoTag, CampoSelect, CampoText } from 'localModules/ReduxForm'
import { FormStep } from 'localModules/Stepper'
import { provaUtils } from 'localModules/provas'
import { MpButton } from 'common/MpButton'

import { PaginaPaper } from 'common/PaginaPaper'
import { setListaTags } from 'utils/compositeActions'

import { enumTipoProva } from 'utils/enumTipoProva'
import { enumTipoSistemaDeNotasDeProva } from 'utils/enumTipoSistemaDeNotasDeProva'
import { enumTipoQuestao } from 'utils/enumTipoQuestao'

import { propTypes } from './propTypes'
export class ProvaComumConfiguracaoPageComponent extends Component {
  static propTypes = propTypes
  _form

  constructor(props) {
    super(props)

    this.state = {
      buttonDisabled: false,
      valorDasQuestoesChecked: props.value.sistemaDeNotasDaProva === enumTipoSistemaDeNotasDeProva.valorEmQuestoes,
    }
  }

  _validarCampoEmQuestao = (questoes, campo) => {
    if (!questoes) return false
    for (let questao of questoes) {
      const isQuestaoBloco = questao.tipo === enumTipoQuestao.tipoQuestaoBloco
      if (isQuestaoBloco && !this._validarCampoEmQuestao(questao.bloco.questoes, campo)) {
        return false
      } else if (questao[campo] !== undefined && questao[campo] !== 1) return false
    }
    return true
  }

  _updateProvaValue = value => {
    const { updateProvaValue } = this.props
    updateProvaValue(value)
    setListaTags()
  }

  _salvarNoBanco = () => {
    this.setState({ buttonDisabled: true })
    const { salvarNoBanco } = this.props
    salvarNoBanco()
    setTimeout(() => this.setState({ buttonDisabled: false }), 3000)
  }

  _contarQuestoes = questoes => {
    if (!questoes) return 0

    let qtdQuestoes = 0

    questoes.forEach(questao => {
      if (questao.tipo === enumTipoQuestao.tipoQuestaoBloco) qtdQuestoes += this._contarQuestoes(questao.bloco.questoes)
      else qtdQuestoes += 1
    })
    return qtdQuestoes
  }

  _atribuirValorEPeso(questoes, { valor, peso = null }) {
    if (!questoes) return
    questoes.forEach(questao => {
      if (questao.tipo === enumTipoQuestao.tipoQuestaoBloco)
        this._atribuirValorEPeso(questao.bloco.questoes, { valor, peso })
      else {
        if (!peso) delete questao.peso
        else questao.peso = peso

        questao.valor = valor
      }
    })
  }

  _atribuirValorEPesoAsQuestoesDosGrupos = (grupos, { valor, peso }) => {
    if (!grupos) return

    grupos.forEach(grupo => {
      this._atribuirValorEPeso(grupo.questoes, { valor, peso })
    })
  }

  _contarQuestoesDosGrupos = grupos => {
    if (!grupos) return 0
    let qtdQuestoes = 0
    grupos.forEach(grupo => {
      qtdQuestoes += this._contarQuestoes(grupo.questoes)
    })
    return qtdQuestoes
  }

  _mudarParaPesos = value => {
    const valorPadraoProva = 10
    const qtdQuestoes = this._contarQuestoesDosGrupos(value.grupos)
    const valorDasQuestoes = qtdQuestoes > 0 && valorPadraoProva / qtdQuestoes
    this._atribuirValorEPesoAsQuestoesDosGrupos(value.grupos, { valor: valorDasQuestoes, peso: 1 })
    value.valor = valorPadraoProva
  }

  _mudarParaValorEmQuestoes = value => {
    value.valor = this._contarQuestoesDosGrupos(value.grupos)
    this._atribuirValorEPesoAsQuestoesDosGrupos(value.grupos, { valor: 1 })
  }

  _mudarSistemaNotas = () => {
    const { value } = this.props
    const { valorDasQuestoesChecked } = this.state
    const mudarParaPesos = valorDasQuestoesChecked
    if (mudarParaPesos) {
      this._mudarParaPesos(value)
      this.setState({ valorDasQuestoesChecked: false })
      value.sistemaDeNotasDaProva = enumTipoSistemaDeNotasDeProva.valorEmProvaEPesosEmQuestoes
    } else {
      this._mudarParaValorEmQuestoes(value)
      this.setState({ valorDasQuestoesChecked: true })
      value.sistemaDeNotasDaProva = enumTipoSistemaDeNotasDeProva.valorEmQuestoes
    }
    this._updateProvaValue(value)
  }

  _manterSistemaAtual = () => {
    const { value } = this.props
    const { valorDasQuestoesChecked } = this.state
    if (valorDasQuestoesChecked) {
      value.sistemaDeNotasDaProva = enumTipoSistemaDeNotasDeProva.valorEmQuestoes
    } else {
      value.sistemaDeNotasDaProva = enumTipoSistemaDeNotasDeProva.valorEmProvaEPesosEmQuestoes
    }
    this._updateProvaValue(value)
  }

  openDialogAutorizarAlterarSistemaDeProva = atualSistemaDeNota => {
    const { listaDialogPush, strings } = this.props

    const valorEmQuestao = atualSistemaDeNota === enumTipoSistemaDeNotasDeProva.valorEmQuestoes
    const texto = valorEmQuestao ? strings.mudarSistemaDeNotaDeValorParaPeso : strings.mudarSistemaDeNotaDePesoParaValor

    const dialogConfig = {
      titulo: strings.mudarSistemaDeNota,
      texto: texto,
      actions: [
        {
          texto: strings.sim,
          onClick: this._mudarSistemaNotas,
        },
        {
          texto: strings.agoraNao,
          onClick: this._manterSistemaAtual,
        },
      ],
    }
    listaDialogPush(dialogConfig)
  }

  _validarCampoEmGrupos = campo => {
    const { value } = this.props
    const { grupos } = value
    if (!grupos) return true

    for (let grupo of grupos) {
      if (!this._validarCampoEmQuestao(grupo.questoes, campo)) return false
    }

    return true
  }

  _validarCampoEMudar = (campo, sistemaDeNotas) => {
    if (!this._validarCampoEmGrupos(campo)) {
      this.openDialogAutorizarAlterarSistemaDeProva(sistemaDeNotas)
    } else this._mudarSistemaNotas()
  }

  mudarParaValorEmQuestoes = () => {
    this._validarCampoEMudar('peso', enumTipoSistemaDeNotasDeProva.valorEmProvaEPesosEmQuestoes)
  }

  mudarParaValorEmProvaEPesosEmQuestoes = () => {
    this._validarCampoEMudar('valor', enumTipoSistemaDeNotasDeProva.valorEmQuestoes)
  }

  render() {
    const { strings, value, classes, stepIndex, providerProvaContext } = this.props
    const salvarNoBanco = this._salvarNoBanco
    const { buttonDisabled, valorDasQuestoesChecked } = this.state
    const { handleVoltarDeProva } = providerProvaContext
    const deveExibirValorDaProva = !valorDasQuestoesChecked

    return (
      <FormStep stepIndex={stepIndex}>
        {({ isCurrentStepValid, mostrarErros, setFormRef, irParaPassoAnterior, handleOnValid }) => (
          <PaginaPaper
            variant="mp"
            conteudo={
              <div className={classes.form}>
                <SimpleReduxForm id="provaConfiguracoes" valor={value} onValid={handleOnValid} onRef={setFormRef}>
                  <Typography color="primary" variant="subtitle2">
                    {strings.descricao}
                  </Typography>
                  <Divider className={classes.divider} />
                  <CampoText
                    id="descricao"
                    accessor="descricao"
                    required
                    label={strings.descricao}
                    textFieldProps={{
                      variant: 'outlined',
                      fullWidth: true,
                      multiline: true,
                      rowsMax: 4,
                    }}
                  />
                  <Typography color="primary" variant="subtitle2" className={classes.title}>
                    {strings.cabecalhoLayout}
                  </Typography>
                  <Divider className={classes.divider} />
                  <CampoText
                    id="titulo"
                    accessor="titulo"
                    required
                    label={strings.titulo}
                    wrapperProps={{ className: classes.input }}
                    textFieldProps={{
                      variant: 'outlined',
                      fullWidth: true,
                    }}
                  />
                  <CampoText
                    id="tema"
                    accessor="tema"
                    label={strings.assunto}
                    wrapperProps={{ className: classes.input }}
                    textFieldProps={{
                      variant: 'outlined',
                      fullWidth: true,
                    }}
                  />
                  <CampoText
                    id="instituicao"
                    accessor="instituicao"
                    label={strings.instituicao}
                    wrapperProps={{ className: classes.input }}
                    textFieldProps={{
                      variant: 'outlined',
                      fullWidth: true,
                    }}
                  />
                  <CampoText
                    id="nomeProfessor"
                    accessor="nomeProfessor"
                    label={strings.professor}
                    textFieldProps={{
                      variant: 'outlined',
                      fullWidth: true,
                    }}
                  />
                  <div>
                    <Typography color="primary" variant="subtitle2" className={classes.title}>
                      {strings.sistemasDeNotasDaProva}
                    </Typography>
                    <Divider className={classes.divider} />
                  </div>
                  {/* <CampoRadio
                    id="sistemaDeNotasDaProva"
                    accessor="sistemaDeNotasDaProva"
                    wrapperProps={{ className: classes.input }}
                  > */}
                  <RadioGroup
                    aria-label="sistemaDeNotasDaProva"
                    name="sistemaDeNotasDaProva"
                    value={value.sistemaDeNotasDaProva}
                  >
                    {value.tipoProva !== enumTipoProva.dinamica ? (
                      <FormControlLabel
                        value={enumTipoSistemaDeNotasDeProva.valorEmQuestoes}
                        control={<Radio onClick={this.mudarParaValorEmQuestoes} checked={valorDasQuestoesChecked} />}
                        label={strings.valorEmQuestoes}
                      />
                    ) : (
                      <div />
                    )}
                    <FormControlLabel
                      value={enumTipoSistemaDeNotasDeProva.valorEmProvaEPesosEmQuestoes}
                      control={
                        <Radio
                          onClick={this.mudarParaValorEmProvaEPesosEmQuestoes}
                          checked={!valorDasQuestoesChecked}
                        />
                      }
                      label={strings.valorEmProvaEPesosEmQuestoes}
                    />
                  </RadioGroup>
                  {deveExibirValorDaProva ? (
                    <TextField
                      value={value.valor}
                      inputProps={{ min: 0, step: 0.01 }}
                      onChange={({ target: { value: valor } }) => {
                        this._updateProvaValue({ ...value, valor })
                      }}
                      type="number"
                      variant="outlined"
                      label={strings.valorDaProva}
                    />
                  ) : (
                    <div />
                  )}

                  <Typography color="primary" variant="subtitle2" className={classes.title}>
                    {strings.outrasConfiguracoes}
                  </Typography>
                  <Divider className={classes.divider} />
                  {provaUtils.provaTipo !== enumTipoProva.dinamica ? (
                    <CampoSelect
                      id="tipoEmbaralhamento"
                      accessor="tipoEmbaralhamento"
                      wrapperProps={{ className: classes.input }}
                      helperText={strings.tipoEmbaralhamento}
                    >
                      <MenuItem value={0}>{strings.tiposEmbaralhamento[0]}</MenuItem>
                      <MenuItem value={1}>{strings.tiposEmbaralhamento[1]}</MenuItem>
                      <MenuItem value={2}>{strings.tiposEmbaralhamento[2]}</MenuItem>
                    </CampoSelect>
                  ) : (
                    <div />
                  )}
                  <CampoTag
                    id="tagsVirtuais"
                    accessor="tagsVirtuais"
                    tagProps={{ placeholder: strings.adicionarTags }}
                  />
                </SimpleReduxForm>
              </div>
            }
            actions={
              <Fragment>
                <MpButton
                  IconComponent={SaveIcon}
                  onClick={isCurrentStepValid ? salvarNoBanco : mostrarErros}
                  classes={{ root: classes.buttonSave }}
                  marginLeft
                  disabled={buttonDisabled}
                >
                  {strings.salvar}
                </MpButton>
                <MpButton
                  IconComponent={ViewListIcon}
                  classes={{ root: classes.buttonBack }}
                  onClick={isCurrentStepValid ? irParaPassoAnterior : mostrarErros}
                  marginLeft
                >
                  {strings.questoes}
                </MpButton>
                <MpButton marginLeft onClick={() => handleVoltarDeProva()}>
                  {strings.sairDaEdicao}
                </MpButton>
              </Fragment>
            }
          />
        )}
      </FormStep>
    )
  }
}
