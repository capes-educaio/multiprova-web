import { bindActionCreators } from 'redux'
import { actions } from 'utils/actions'

export function mapStateToProps(state) {
  return {
    strings: state.strings,
    salvarNoBanco: state.ProviderProva__salvarNoBanco,
    updateProvaValue: state.ProviderProva__updateProvaValue,
    value: state.ProviderProva__value,
    disabled: state.messages.length > 0,
  }
}

export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      listaDialogPush: actions.push.listaDialog,
    },
    dispatch,
  )
}
