import React, { Component } from 'react'

import { propTypes } from './propTypes'

import { ProvaComumConfiguracaoPageComponent } from './ProvaComumConfiguracaoPageComponent'

export class ProvaConfiguracoesPageComponent extends Component {
  static propTypes = propTypes

  render() {
    const tipoConfiguracao = 'capes'

    const provaConfiguracao = {
      capes: <ProvaComumConfiguracaoPageComponent {...this.props} />,
    }

    return provaConfiguracao[tipoConfiguracao]
  }
}
