import { ProvaConfiguracoesPageComponent } from './ProvaConfiguracoesPageComponent'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import { withStyles } from '@material-ui/core/styles'

import { withProviderProvaContext } from 'localModules/provas/ProviderProva/context'

import { mapStateToProps, mapDispatchToProps } from './redux'
import { style } from './style'

export const ProvaConfiguracoesPage = compose(
  withStyles(style),
  withRouter,
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withProviderProvaContext,
)(ProvaConfiguracoesPageComponent)
