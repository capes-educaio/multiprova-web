import React from 'react'

import { Typography, Button } from '@material-ui/core'
import CancelIcon from '@material-ui/icons/Cancel'
import PlaylistAddCheckIcon from '@material-ui/icons/PlaylistAddCheck'

import { Busca } from 'common/Busca'
import { FormQuestaoFiltroPadrao } from 'common/FormQuestaoFiltroPadrao'
import { FormQuestaoFiltroExpandido } from 'common/FormQuestaoFiltroExpandido'
import { CardDisplay } from 'common/CardDisplay'
import { Dialog } from 'common/Dialog'

import { montarFiltroQuestao } from 'utils/montarFiltroQuestao'
import { enumTipoSistemaDeNotasDeProva } from 'utils/enumTipoSistemaDeNotasDeProva'
import { enumTipoQuestao } from 'utils/enumTipoQuestao'

import { updateUrlState } from 'localModules/urlState'

import { provaUtils } from 'localModules/provas/ProvaUtils'

import { QuestaoCardAdicionarQuestao } from '../QuestaoCardAdicionarQuestao'

import { propTypes } from './propTypes'
import { defaultProps } from './defaultProps'

export class AdicionarQuestaoComponent extends React.Component {
  static propTypes = propTypes
  static defaultProps = defaultProps

  _cardDisplayRef

  _fecharModal = () => updateUrlState({ hidden: { isOpenModal: false } })

  _clearQuestoesSelecinadasDicionario = () => this.props.selectQuestoesSelecionadasDicionario({})

  _adicionarQuestoes = () => {
    const { location, questoesSelecionadasDicionario, value } = this.props
    const { updateProvaValue } = this.props
    const { idsCardsSelecionados, indexQuestaoTriggerAdd, addWasTriggeredPorUmaQuestao } = location.state.hidden
    const { indexGrupoTriggerAdd } = location.state.hidden
    const questoesDoGrupo = value.grupos[indexGrupoTriggerAdd].questoes
    idsCardsSelecionados.forEach(id => {
      const novaQuestao = questoesSelecionadasDicionario[id]
      if (novaQuestao) {
        novaQuestao.fixa = false
        novaQuestao.peso = 1
        novaQuestao.valor = 1
        if (novaQuestao.tipo === enumTipoQuestao.tipoQuestaoBloco)
          novaQuestao.bloco.questoes.forEach(questao => {
            questao.fixa = false
            questao.peso = 1
            questao.valor = 1
          })

        if (!addWasTriggeredPorUmaQuestao) {
          questoesDoGrupo.push(novaQuestao)
          value.questoes.push(novaQuestao)
        } else {
          questoesDoGrupo.splice(indexQuestaoTriggerAdd + 1, 0, novaQuestao)
        }
        if (value.sistemaDeNotasDaProva === enumTipoSistemaDeNotasDeProva.valorEmQuestoes) {
          if (novaQuestao.tipo === enumTipoQuestao.tipoQuestaoBloco) value.valor += novaQuestao.bloco.questoes.length
          else value.valor += 1
        }
      } else console.warn('Card está em idsCardsSelecionados, mas não está em -SelecionadasDicionario')
    })
    updateProvaValue(value)
    updateUrlState({ hidden: { idsCardsSelecionados: [] } })
    this._clearQuestoesSelecinadasDicionario()
    this._fecharModal()
  }

  _onCancelar = () => {
    updateUrlState({ hidden: { idsCardsSelecionados: [] } })
    this._clearQuestoesSelecinadasDicionario()
    this._fecharModal()
  }

  _getQuestoesDaProva = () => {
    const { value } = this.props
    const { grupos } = value
    let questoes = []
    grupos.forEach((grupo, indexGrupo) => {
      const questoesDoGrupo = grupo.questoes.map(questao => ({ ...questao, indexGrupo }))
      questoes = [...questoes, ...questoesDoGrupo]
    })
    return questoes
  }

  render() {
    const { strings, classes } = this.props
    return (
      <div className={classes.principal}>
        <Typography variant="h6" gutterBottom>
          {strings.questoes}
        </Typography>
        <Busca
          FormPadrao={FormQuestaoFiltroPadrao}
          FormExpandido={FormQuestaoFiltroExpandido}
          montarFiltro={montarFiltroQuestao}
        />
        <CardDisplay
          nadaCadastrado={strings.naoEncontrouQuestao}
          quantidadeUrl={provaUtils.rotas.back.countQuestoes}
          fetchInstanciasUrl={provaUtils.rotas.back.visualizacaoQuestoes}
          quantidadePorPaginaInicial={9}
          quantidadePorPaginaOpcoes={[9]}
          onRef={ref => {
            this._cardDisplayRef = ref
          }}
          CardComponent={QuestaoCardAdicionarQuestao}
          contexto={{ questoesDaProva: this._getQuestoesDaProva() }}
        />
        <Dialog />
        <div className={classes.acoesModal}>
          <Button id="adicionar-questao" variant="contained" color="primary" onClick={this._adicionarQuestoes}>
            <PlaylistAddCheckIcon />
            {strings.adicionar}
          </Button>
          <Button id="cancelar-adicao-questao" color="default" onClick={this._onCancelar}>
            <CancelIcon />
            {strings.cancelar}
          </Button>
        </div>
      </div>
    )
  }
}
