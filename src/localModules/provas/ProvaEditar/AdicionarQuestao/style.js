export const style = theme => ({
  principal: {
    '& .colunaInner': {
      backgroundColor: 'white',
    },
  },
  acoesModal: {
    display: 'flex',
    justifyContent: 'flex-start',
    marginTop: '20px',
    '& button:not(:last-child)': {
      marginRight: '10px',
    },
  },
  lista: {
    margin: '20px 0',
    '& > *:not(:last-child)': {
      marginBottom: '10px',
    },
  },
  criarQuestaoButton: {
    margin: '0 0 0 10px',
  },
})
