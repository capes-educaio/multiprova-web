import { bindActionCreators } from 'redux'
import { actions } from 'utils/actions'

export function mapStateToProps(state) {
  return {
    strings: state.strings,
    usuarioAtual: state.usuarioAtual,
    questoesSelecionadasDicionario: state.questoesSelecionadasDicionario,
    value: state.ProviderProva__value,
    updateProvaValue: state.ProviderProva__updateProvaValue,
  }
}

export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      selectQuestoesSelecionadasDicionario: actions.select.questoesSelecionadasDicionario,
      selectValorProvaNaVolta: actions.select.valorProvaNaVolta,
    },
    dispatch,
  )
}
