import React from 'react'
import { AdicionarQuestao } from '../index'
import { shallow } from 'enzyme'

import { ptBr } from 'utils/strings/ptBr'
import { enumTipoQuestao } from 'utils/enumTipoQuestao'

jest.mock('localModules/provas/ProvaUtils', () => ({
  provaUtils: {
    irParaCriarQuestao: () => jest.fn(),
    rotas: { back: {}, front: {} },
  },
}))

const usuarioAtual = {
  data: {
    id: 1,
  },
}

const questoes = [
  {
    id: '75cc4eff-6d71-4d55-821a-4ff6b8653695',
    dataCadastro: '2018-07-25T14:48:55.348Z',
    dataUltimaAlteracao: '2018-07-25T14:48:55.348Z',
    enunciado: '',
    dificuldade: 2,
    tipo: enumTipoQuestao.tipoQuestaoMultiplaEscolha,
    multiplaEscolha: {
      respostaCerta: 'a',
      alternativas: [
        {
          texto: '<p>150 reais</p>',
          letra: 'a',
        },
        {
          texto: '<p>450 reais</p>',
          letra: 'b',
        },
      ],
    },
  },
  {
    id: '00000000-6d71-4d55-821a-4ff6b8653695',
    dataCadastro: '2019-07-25T14:48:55.348Z',
    dataUltimaAlteracao: '2019-07-25T14:48:55.348Z',
    enunciado: '',
    dificuldade: 3,
    tipo: enumTipoQuestao.tipoQuestaoMultiplaEscolha,
    multiplaEscolha: {
      respostaCerta: 'a',
      alternativas: [
        {
          texto: '<p>10</p>',
          letra: 'a',
        },
        {
          texto: '<p>teste</p>',
          letra: 'b',
        },
      ],
    },
  },
  {
    id: '01caced3-5cae-47a0-80eb-b008f13366bf',
    dataCadastro: '2019-01-09T16:24:20.229Z',
    dataUltimaAlteracao: '2019-01-09T16:24:20.229Z',
    enunciado: '<p>Testando a criação de questão discursiva diretamente para a criação de prova:</p>',
    dificuldade: 2,
    tipo: enumTipoQuestao.tipoQuestaoDiscursiva,
    discursiva: {
      numeroDeLinhas: 5,
      expectativaDeResposta: '<p>Vamoooos dissertar!</p>',
    },
  },
  {
    id: '2fede4e6-cc77-4412-8c29-72b742ea333c',
    dataCadastro: '2019-01-09T13:19:14.466Z',
    dataUltimaAlteracao: '2019-01-09T13:19:14.466Z',
    enunciado: '<p>Assinale verdadeiro ou falso para as opções abaixo:</p>',
    dificuldade: 2,
    tipo: enumTipoQuestao.tipoQuestaoVerdadeiroOuFalso,
    vouf: {
      afirmacoes: [
        {
          texto: '<p>Baleia e Leão  são mamíferos.</p>',
          letra: 'V',
        },
        {
          texto: '<p>Sapo e Elefante  são mamíferos.</p>',
          letra: 'F',
        },
      ],
    },
  },
]

const match = {
  params: {
    id: '09233', // id da prova atual para todos os casos
  },
}

const defaultProps = {
  indexGrupo: 0,
  value: {
    grupos: [{ questoes }],
  },
  classes: {},
  strings: ptBr,
  usuarioAtual,
  history: {
    push: jest.fn(),
  },
  location: {},
  questoesDaProva: questoes,
  onQuestoesChange: jest.fn(),
  excluirQuestaoDaProva: jest.fn(),
  fecharModal: jest.fn(),
  questoesSelecionadasDicionario: {},
  selectQuestoesSelecionadasDicionario: jest.fn(),
  match,
  updateProvaValue: jest.fn(),
  selectValorProvaNaVolta: jest.fn(),
}

const localStorageMock = (function() {
  let store = {}
  return {
    getItem: function(key) {
      return store[key] || null
    },
    setItem: function(key, value) {
      if (value) store[key] = value.toString()
    },
    removeItem: function(key) {
      delete store[key]
    },
    clear: function() {
      store = {}
    },
  }
})()

Object.defineProperty(window, 'localStorage', {
  value: localStorageMock,
})

describe('Testando <AcionarQuestaoComponent />', () => {
  it('Carrega o componente', () => {
    const wrapper = shallow(<AdicionarQuestao {...defaultProps} />)
    expect(wrapper.length).toEqual(1)
    jest.clearAllMocks()
  })
})

// describe('Testando UNSAFE_componentWillMount do componente AdicionarQuestaoComponent', () => {
//   it('Testa se updateUrlState é chamada, passando um valor inicial de questoesSelecionadas', () => {
//     const wrapper = shallow(<AdicionarQuestaoComponent {...defaultProps} />)
//     const spy = jest.spyOn(wrapper.instance(), 'updateUrlState')
//     const RESULTADO_ESPERADO = { questoesSelecionadas: `[]` }
//     wrapper.instance().UNSAFE_componentWillMount()
//     expect(spy).toHaveBeenCalledWith(RESULTADO_ESPERADO)
//     jest.clearAllMocks()
//   })
// })
// describe('Testando <AdicionarQuestaoComponent /> updateUrlState', () => {
//   it('Testa se updateUrlState altera a url', () => {
//     const wrapper = shallow(<AdicionarQuestaoComponent {...defaultProps} />)
//     const spy = wrapper.instance().props.history.push
//     const RESULTADO_ESPERADO = { questoesSelecionadas: `["3919d87b-1589-4239-b6b1-2a039bcc14be"]` }
//     wrapper.instance().updateUrlState(RESULTADO_ESPERADO)
//     expect(wrapper.state('urlState')).toEqual(RESULTADO_ESPERADO)
//     expect(spy).toHaveBeenCalled()
//     jest.clearAllMocks()
//   })
// })

// describe('Testando <AdicionarQuestaoComponent /> get questoesSelecionadasDicionario', () => {
//   it('Testa se get questoesSelecionadasDicionario pega as questões do localstorage com um resutado esperado', () => {
//     const wrapper = shallow(<AdicionarQuestaoComponent {...defaultProps} />)
//     const spy = jest.spyOn(global.window.localStorage, 'getItem')
//     const provaAtualId = match.params.id
//     const RESULTADO_ESPERADO_EDITAR_PROVA = usuarioAtual.data.id + 'questoesSelecionadasDicionario' + provaAtualId
//     wrapper.instance().questoesSelecionadasDicionario
//     expect(spy).toBeCalledWith(RESULTADO_ESPERADO_EDITAR_PROVA)

//     const RESULTADO_ESPERADO_NOVA_PROVA = usuarioAtual.data.id + 'questoesSelecionadasDicionario'
//     wrapper.instance().edicao = null

//     wrapper.instance().questoesSelecionadasDicionario
//     expect(spy).toBeCalledWith(RESULTADO_ESPERADO_NOVA_PROVA)

//     jest.clearAllMocks()
//     spy.mockRestore()
//   })
// })

// describe('Testando <AdicionarQuestaoComponent /> set questoesSelecionadasDicionario', () => {
//   it('Testa se set questoesSelecionadasDicionario seta as questões do localstorage com um resutado esperado', () => {
//     const wrapper = shallow(<AdicionarQuestaoComponent {...defaultProps} />)
//     const spy = jest.spyOn(global.window.localStorage, 'setItem')
//     const provaAtualId = match.params.id
//     const RESULTADO_ESPERADO_EDITAR_PROVA = usuarioAtual.data.id + 'questoesSelecionadasDicionario' + provaAtualId
//     const RESULTADO_ESPERADO_EDITAR_PROVA_SEGUNDO_PARAMETRO = '{}'
//     wrapper.instance().questoesSelecionadasDicionario = 1
//     expect(spy).toBeCalledWith(RESULTADO_ESPERADO_EDITAR_PROVA, RESULTADO_ESPERADO_EDITAR_PROVA_SEGUNDO_PARAMETRO)

//     const RESULTADO_ESPERADO_NOVA_PROVA = usuarioAtual.data.id + 'questoesSelecionadasDicionario'
//     wrapper.instance().edicao = null

//     wrapper.instance().questoesSelecionadasDicionario = 1
//     expect(spy).toBeCalledWith(RESULTADO_ESPERADO_NOVA_PROVA, RESULTADO_ESPERADO_EDITAR_PROVA_SEGUNDO_PARAMETRO)

//     jest.clearAllMocks()
//     spy.mockRestore()
//   })
// })

// describe('Testando <AdicionarQuestaoComponent /> clearQuestoesSelecinadasDicionario', () => {
//   it('Testa se clearQuestoesSelecinadasDicionario limpa o localstorage', () => {
//     const wrapper = shallow(<AdicionarQuestaoComponent {...defaultProps} />)
//     const spy = jest.spyOn(global.window.localStorage, 'setItem')
//     const provaAtualId = match.params.id
//     const RESULTADO_ESPERADO_EDITAR_PROVA = usuarioAtual.data.id + 'questoesSelecionadasDicionario' + provaAtualId
//     const RESULTADO_ESPERADO_EDITAR_PROVA_SEGUNDO_PARAMETRO = null
//     wrapper.instance().clearQuestoesSelecinadasDicionario()
//     expect(spy).toBeCalledWith(RESULTADO_ESPERADO_EDITAR_PROVA, RESULTADO_ESPERADO_EDITAR_PROVA_SEGUNDO_PARAMETRO)

//     const RESULTADO_ESPERADO_NOVA_PROVA = usuarioAtual.data.id + 'questoesSelecionadasDicionario'
//     wrapper.instance().edicao = null

//     wrapper.instance().clearQuestoesSelecinadasDicionario()
//     expect(spy).toBeCalledWith(RESULTADO_ESPERADO_NOVA_PROVA, RESULTADO_ESPERADO_EDITAR_PROVA_SEGUNDO_PARAMETRO)

//     jest.clearAllMocks()
//     spy.mockRestore()
//   })
// })

// describe('Testando <AdicionarQuestaoComponent /> UNSAFE_componentWillReceiveProps', () => {
//   it('Testa se UNSAFE_componentWillReceiveProps se seta o estado com a nova props passada', () => {
//     const wrapper = shallow(<AdicionarQuestaoComponent {...defaultProps} />)
//     const HISTORY_VALORSEARCH_REPETIDO = {
//       history: {
//         push: jest.fn(),
//         location: {
//           search: {},
//         },
//       },
//     }
//     const NOVA_HISTORY = {
//       history: {
//         push: jest.fn(),
//         location: {
//           search: 'teste',
//         },
//       },
//     }
//     const RESULTADO_ESPERADO_VALORSEARCH_REPETIDO = {}
//     const RESULTADO_ESPERADO_NOVA_HISTORY = { teste: '' }
//     wrapper.instance().UNSAFE_componentWillReceiveProps(HISTORY_VALORSEARCH_REPETIDO)
//     expect(wrapper.instance().state.urlState).toEqual(RESULTADO_ESPERADO_VALORSEARCH_REPETIDO)

//     wrapper.instance().UNSAFE_componentWillReceiveProps(NOVA_HISTORY)
//     expect(wrapper.instance().state.urlState).toEqual(RESULTADO_ESPERADO_NOVA_HISTORY)

//     jest.clearAllMocks()
//   })
// })

// describe('Testando <AdicionarQuestaoComponent /> get questoesSelecionadas', () => {
//   it('Testa se get questoesSelecionadas pega as questões do estado', () => {
//     const wrapper = shallow(<AdicionarQuestaoComponent {...defaultProps} />)

//     const questoesSelecionadas = []
//     questoesSelecionadas.push(questoes[0].id)
//     const urlState = { questoesSelecionadas: JSON.stringify(questoesSelecionadas) }
//     const RESULTADO_ESPERADO = JSON.parse(urlState.questoesSelecionadas)
//     wrapper.instance().setState({ urlState })
//     const RESULTADO = wrapper.instance().questoesSelecionadas
//     expect(RESULTADO).toEqual(RESULTADO_ESPERADO)

//     jest.clearAllMocks()
//   })
// })

// describe('Testando <AdicionarQuestaoComponent /> adicionarQuestoes', () => {
//   it('Testa as questões selecionadas são passadas ao método onQuestoesChange corretamente', () => {
//     const wrapper = shallow(<AdicionarQuestaoComponent {...defaultProps} />)
//     const spy = wrapper.instance().props.onQuestoesChange
//     const RESULTADO_ESPERADO = []
//     RESULTADO_ESPERADO.push(questoes[0])
//     RESULTADO_ESPERADO.push(questoes[1])
//     wrapper.instance().adicionarQuestoes()
//     expect(spy).toBeCalledWith(RESULTADO_ESPERADO)
//     spy.mockRestore()
//     jest.clearAllMocks()
//   })

//   it('Testa o urlState é zerado', () => {
//     const wrapper = shallow(<AdicionarQuestaoComponent {...defaultProps} />)
//     const spy = jest.spyOn(wrapper.instance(), 'updateUrlState')
//     wrapper.instance().adicionarQuestoes()
//     const RESULTADO_ESPERADO = { questoesSelecionadas: undefined }
//     expect(spy).toBeCalledWith(RESULTADO_ESPERADO)
//     spy.mockRestore()
//     jest.clearAllMocks()
//   })

//   it('Testa se clearQuestoesSelecinadasDicionario e fecharModal são chamados', () => {
//     const wrapper = shallow(<AdicionarQuestaoComponent {...defaultProps} />)
//     const spyclear = jest.spyOn(wrapper.instance(), 'clearQuestoesSelecinadasDicionario')
//     const spyModal = wrapper.instance().props.fecharModal
//     wrapper.instance().adicionarQuestoes()
//     expect(spyclear).toBeCalled()
//     expect(spyModal).toBeCalled()
//     spyclear.mockRestore()
//     spyModal.mockRestore()
//     jest.clearAllMocks()
//   })
// })

// describe('Testando <AdicionarQuestaoComponent /> onCancelar', () => {
//   it('Testa se updateUrlState, clearQuestoesSelecinadasDicionario e fecharModal são chamados', () => {
//     const wrapper = shallow(<AdicionarQuestaoComponent {...defaultProps} />)
//     const spyclear = jest.spyOn(wrapper.instance(), 'clearQuestoesSelecinadasDicionario')
//     const spyUrlState = jest.spyOn(wrapper.instance(), 'updateUrlState')
//     const spyModal = wrapper.instance().props.fecharModal
//     const RESULTADO_ESPERADO = { questoesSelecionadas: undefined }

//     wrapper.instance().onCancelar()
//     expect(spyclear).toBeCalled()
//     expect(spyModal).toBeCalled()
//     expect(spyUrlState).toBeCalledWith(RESULTADO_ESPERADO)
//     spyclear.mockRestore()
//     spyModal.mockRestore()
//     spyUrlState.mockRestore()
//     jest.clearAllMocks()
//   })
// })

// describe('Testando <AdicionarQuestaoComponent /> irParaCriarQuestao', () => {
//   it('Testa se irParaCriarQuestao altera a url e volta para criar questão', () => {
//     const wrapper = shallow(<AdicionarQuestaoComponent {...defaultProps} />)
//     const spy = wrapper.instance().props.history.push
//     const RESULTADO_ESPERADO = { pathname: '/questao', search: 'backTo=%7B%22search%22%3A%7B%7D%7D' }
//     wrapper.instance().irParaCriarQuestao()
//     expect(spy).toHaveBeenCalledWith(RESULTADO_ESPERADO)
//     jest.clearAllMocks()
//     spy.mockRestore()
//   })
// })

// describe('Testando <AdicionarQuestaoComponent /> expandirTodasAsCards', () => {
//   it('Testa se expandirTodasAsCards chama cardDisplay.expandirTodas', () => {
//     const wrapper = shallow(<AdicionarQuestaoComponent {...defaultProps} />)
//     const cardDisplay = {
//       expandirTodas: jest.fn(),
//     }
//     wrapper.instance().cardDisplay = cardDisplay
//     const spy = wrapper.instance().cardDisplay.expandirTodas
//     wrapper.instance().expandirTodasAsCards()
//     expect(spy).toHaveBeenCalled()
//     jest.clearAllMocks()
//     spy.mockRestore()
//   })
// })

// describe('Testando <AdicionarQuestaoComponent /> contrairTodasAsCards', () => {
//   it('Testa se contrairTodasAsCards chama cardDisplay.expandirTodas', () => {
//     const wrapper = shallow(<AdicionarQuestaoComponent {...defaultProps} />)
//     const cardDisplay = {
//       contrairTodas: jest.fn(),
//     }
//     wrapper.instance().cardDisplay = cardDisplay
//     const spy = wrapper.instance().cardDisplay.contrairTodas
//     wrapper.instance().contrairTodasAsCards()
//     expect(spy).toHaveBeenCalled()
//     jest.clearAllMocks()
//     spy.mockRestore()
//   })
// })

// describe('Testando <AdicionarQuestaoComponent />, clique nos botões.', () => {
//   it('Deve simular o click no botão e chamar irParaCriarQuestao', () => {
//     const wrapperParent = mount(
//       <MemoryRouter>
//         <div>
//           <Provider store={store}>
//             <AdicionarQuestaoComponent {...defaultProps} />
//           </Provider>
//           ,
//         </div>
//       </MemoryRouter>,
//     )
//     const wrapper = wrapperParent.find('AdicionarQuestaoComponent')

//     const spy = jest.spyOn(wrapper.instance(), 'irParaCriarQuestao')
//     wrapper.instance().forceUpdate()
//     const BOTAO_NESSE_CENARIO = 0
//     const botao = wrapper.find('button').at(BOTAO_NESSE_CENARIO)
//     botao.simulate('click', spy)
//     expect(spy).toHaveBeenCalled()
//     spy.mockRestore()
//     jest.clearAllMocks()
//     wrapperParent.unmount()
//   })

//   it('Deve simular o click no botão questão e chamar expandirTodasAsCards', () => {
//     const wrapperParent = mount(
//       <MemoryRouter>
//         <div>
//           <Provider store={store}>
//             <AdicionarQuestaoComponent {...defaultProps} />
//           </Provider>
//           ,
//         </div>
//       </MemoryRouter>,
//     )
//     const wrapper = wrapperParent.find('AdicionarQuestaoComponent')

//     const spy = jest.spyOn(wrapper.instance(), 'expandirTodasAsCards')
//     wrapper.instance().forceUpdate()
//     const BOTAO_NESSE_CENARIO = 1
//     const botao = wrapper.find('button').at(BOTAO_NESSE_CENARIO)
//     botao.simulate('click', spy)
//     expect(spy).toHaveBeenCalled()
//     spy.mockRestore()
//     jest.clearAllMocks()
//     wrapperParent.unmount()
//   })

//   it('Deve simular o click no botão e chamar contrairTodasAsCards', () => {
//     const wrapperParent = mount(
//       <MemoryRouter>
//         <div>
//           <Provider store={store}>
//             <AdicionarQuestaoComponent {...defaultProps} />
//           </Provider>
//           ,
//         </div>
//       </MemoryRouter>,
//     )
//     const wrapper = wrapperParent.find('AdicionarQuestaoComponent')

//     const spy = jest.spyOn(wrapper.instance(), 'contrairTodasAsCards')
//     wrapper.instance().forceUpdate()
//     const BOTAO_NESSE_CENARIO = 2
//     const botao = wrapper.find('button').at(BOTAO_NESSE_CENARIO)
//     botao.simulate('click', spy)
//     expect(spy).toHaveBeenCalled()
//     spy.mockRestore()
//     jest.clearAllMocks()
//     wrapperParent.unmount()
//   })

//   it('Deve simular o click no botão e chamar onCancelar', () => {
//     const wrapperParent = mount(
//       <MemoryRouter>
//         <div>
//           <Provider store={store}>
//             <AdicionarQuestaoComponent {...defaultProps} />
//           </Provider>
//           ,
//         </div>
//       </MemoryRouter>,
//     )
//     const wrapper = wrapperParent.find('AdicionarQuestaoComponent')

//     const spy = jest.spyOn(wrapper.instance(), 'onCancelar')
//     wrapper.instance().forceUpdate()
//     const BOTAO_NESSE_CENARIO = 6
//     const botao = wrapper.find('button').at(BOTAO_NESSE_CENARIO)
//     botao.simulate('click', spy)
//     expect(spy).toHaveBeenCalled()
//     spy.mockRestore()
//     jest.clearAllMocks()
//     wrapperParent.unmount()
//   })

//   it('Deve simular o click no botão e chamar adicionarQuestoes', () => {
//     const wrapperParent = mount(
//       <MemoryRouter>
//         <div>
//           <Provider store={store}>
//             <AdicionarQuestaoComponent {...defaultProps} />
//           </Provider>
//           ,
//         </div>
//       </MemoryRouter>,
//     )
//     const wrapper = wrapperParent.find('AdicionarQuestaoComponent')

//     const spy = jest.spyOn(wrapper.instance(), 'adicionarQuestoes')
//     wrapper.instance().forceUpdate()
//     const BOTAO_NESSE_CENARIO = 7
//     const botao = wrapper.find('button').at(BOTAO_NESSE_CENARIO)
//     botao.simulate('click', spy)
//     expect(spy).toHaveBeenCalled()
//     spy.mockRestore()
//     jest.clearAllMocks()
//     wrapperParent.unmount()
//   })
// })
