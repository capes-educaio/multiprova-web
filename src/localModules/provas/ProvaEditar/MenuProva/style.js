export const style = theme => ({
  input: {
    display: 'none',
  },
  menu: {
    display: 'flex',
    justifyContent: 'flex-end',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: theme.palette.defaultBackground || theme.palette.steelBlue,
    height: '40px',
    margin: '10px 0px 0px 0px',
  },
  botoesMenu: {
    display: 'flex',
    flexDirection: 'column',
    backgroundColor: theme.palette.defaultBackground || theme.palette.darkBlue,
    color: theme.palette.defaultLink || theme.palette.steelBlueContrast,
    borderRadius: '0px',
    borderStyle: 'solid',
    borderWidth: '0px 0px 0px 1px',
    borderLeftColor: theme.palette.steelBlueContrast,
    textTransform: 'none',
    padding: '5px 10px 5px 10px',
    margin: '0px',
    fontFamily: 'Arial',
    height: '40px',
    boxShadow: '0px 0px 0px 0px',
    '&:hover': {
      backgroundColor: theme.palette.defaultBackground || theme.palette.steelBlue,
    },
  },
  leftIcon: {
    margin: '0px 5px 0px 0px',
  },
  label: {
    marginRight: 10,
  },
})
