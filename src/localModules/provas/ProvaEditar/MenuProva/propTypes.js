import PropTypes from 'prop-types'

export const propTypes = {
  // state redux
  value: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
}
