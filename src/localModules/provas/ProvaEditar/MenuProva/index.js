import { compose } from 'redux'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import { mapStateToProps, mapDispatchToProps } from './redux'
import { withStyles } from '@material-ui/core/styles'
import { style } from './style'
import { MenuProvaComponent } from './MenuProvaComponent'

export const MenuProva = compose(
  withStyles(style),
  withRouter,
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(MenuProvaComponent)
