import React, { Component } from 'react'
import uid from 'uuid/v4'

import { propTypes } from './propTypes'

import { MenuListGrupos } from '../MenuListGrupos'
import { MenuListQuestoes } from '../MenuListQuestoes'

import { tipoMap } from 'localModules/questoes/tipoMap'
import Button from '@material-ui/core/Button'
import GroupWork from '@material-ui/icons/GroupWork'
import Add from '@material-ui/icons/Add'
import QuestionAnswer from '@material-ui/icons/QuestionAnswer'
import { appConfig } from 'appConfig'

import { updateUrlState } from 'localModules/urlState'
import { ProviderProvaContext } from 'localModules/provas/ProviderProva/context'
import { provaUtils } from 'localModules/provas'
import { imagensParaUrlQuestao } from 'localModules/questoes/utils/conversaoImagensQuestao'
import { Importar } from 'common/Importar'

export class MenuProvaComponent extends Component {
  static propTypes = propTypes
  static contextType = ProviderProvaContext
  state = {
    openGrupos: false,
    openQuestoes: false,
  }

  handleClickButtonGrupos = () => this.setState({ openGrupos: true })

  handleClickButtonQuestoes = () => this.setState({ openQuestoes: true })

  addQuestao = () => {
    updateUrlState({
      hidden: {
        isOpenModal: true,
        modalType: 'adicionarQuestao',
        indexQuestaoTriggerAdd: 0,
        indexGrupoTriggerAdd: 0,
        addWasTriggeredPorUmaQuestao: false,
      },
    })
  }
  addSelecaoAutomatica = () => {
    updateUrlState({
      hidden: {
        isOpenModal: true,
        modalType: 'selecaoAutomatica',
        indexQuestaoTriggerAdd: 0,
        indexGrupoTriggerAdd: 0,
        addWasTriggeredPorUmaQuestao: false,
      },
    })
  }
  addSelecaoCriterio = () => {
    updateUrlState({
      hidden: {
        isOpenModal: true,
        modalType: 'selecaoCriterio',
        indexQuestaoTriggerAdd: 0,
        indexGrupoTriggerAdd: 0,
        addWasTriggeredPorUmaQuestao: false,
      },
    })
  }
  handleCloseGrupos = () => this.setState({ openGrupos: false })

  handleCloseQuestoes = () => this.setState({ openQuestoes: false })

  prepararQuestaoParaEnviar = async stringQuestao => {
    const questao = JSON.parse(await imagensParaUrlQuestao(stringQuestao))
    const { tipo } = questao
    if (tipoMap[tipo] && tipoMap[tipo].montarQuestaoParaEnviar) {
      return tipoMap[tipo].montarQuestaoParaEnviar(questao)
    } else {
      console.error('Falha ao procurar montarQuestaoParaEnviar em tipo:' + tipo)
      return
    }
  }

  processarQuestaoDaProva = questaoArg => {
    const questao = { ...questaoArg }
    questao.fixa = false
    delete questao.elaborador
    return questao
  }

  addQuestaoNaProva = async novaQuestao => {
    const { value, updateProvaValue } = this.props
    const ultimoGrupo = value.grupos.length - 1
    const questoesDoGrupo = value.grupos[ultimoGrupo].questoes
    novaQuestao = { ...this.processarQuestaoDaProva(novaQuestao), id: uid(), deveSerCriadaAoSalvar: true }
    questoesDoGrupo.push(novaQuestao)
    updateProvaValue(value)
  }

  render() {
    const { classes, strings, value } = this.props
    const { openGrupos, openQuestoes } = this.state
    const { tipoEmbaralhamento } = value
    return (
      <div className={classes.menu}>
        {!appConfig.projeto.isEducaio && !provaUtils.isProvaDinamica && tipoEmbaralhamento === 2 && (
          <div>
            <Button
              id="grupos-prova"
              className={classes.botoesMenu}
              buttonRef={node => {
                this.anchorEl1 = node
              }}
              variant="contained"
              onClick={this.handleClickButtonGrupos}
            >
              <GroupWork className={classes.leftIcon} />
              {strings.grupos}
            </Button>
            <MenuListGrupos open={openGrupos} anchorEl={this.anchorEl1} handleClose={this.handleCloseGrupos} />
          </div>
        )}
        {!appConfig.projeto.isEducaio && !provaUtils.isProvaDinamica && (
          <Importar
            id="importar-questao-prova"
            processarArquivo={this.prepararQuestaoParaEnviar}
            url={provaUtils.rotas.back.addQuestao}
            aposImportacao={this.addQuestaoNaProva}
            classes={{ botaoImportar: classes.botoesMenu }}
            // iconProps={{ SVGcolor: theme.palette.steelBlueContrast }}
            pularSalvamento
          />
        )}
        {!provaUtils.isProvaDinamica && (
          <Button
            id="adicionar-questao-prova"
            className={classes.botoesMenu}
            buttonRef={node => {
              this.anchorEl2 = node
            }}
            variant="contained"
            onClick={this.addQuestao}
          >
            <Add className={classes.leftIcon} />
            {strings.adicionarQuestao}
          </Button>
        )}
        {provaUtils.isProvaDinamica ? null : (
          <Button
            className={classes.botoesMenu}
            buttonRef={node => {
              this.anchorEl3 = node
            }}
            variant="contained"
            onClick={this.addSelecaoAutomatica}
          >
            <Add className={classes.leftIcon} />
            {strings.adicionarQuestaoAutomaticamente}
          </Button>
        )}
        {provaUtils.isProvaDinamica && (
          <Button
            className={classes.botoesMenu}
            buttonRef={node => {
              this.anchorEl3 = node
            }}
            onClick={this.addSelecaoCriterio}
            variant="contained"
          >
            <Add className={classes.leftIcon} />
            {strings.criarCriterio}
          </Button>
        )}
        {!provaUtils.isProvaDinamica && (
          <Button
            id="criar-questao-prova"
            className={classes.botoesMenu}
            buttonRef={node => {
              this.anchorEl4 = node
            }}
            variant="contained"
            onClick={this.handleClickButtonQuestoes}
          >
            <QuestionAnswer className={classes.leftIcon} />
            {strings.criarQuestao}
          </Button>
        )}
        <MenuListQuestoes open={openQuestoes} anchorEl={this.anchorEl4} handleClose={this.handleCloseQuestoes} />
      </div>
    )
  }
}
