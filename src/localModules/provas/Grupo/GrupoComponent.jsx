import React, { Component } from 'react'
import classnames from 'classnames'

import CardContent from '@material-ui/core/CardContent'

import DragIndicatorIcon from '@material-ui/icons/DragIndicator'

import { ParatextoProvider } from 'localModules/provas/ParatextoProvider'

import { propTypes } from './propTypes'

export class GrupoComponent extends Component {
  static propTypes = propTypes

  deveRenderizarParatextos = () => {
    const { instancia, paratextosHabilitados } = this.props
    return instancia.paratextoInicial || instancia.paratextoFinal || paratextosHabilitados
  }

  render() {
    const {
      classes,
      ListaQuestoesGrupoComponent,
      instancia,
      dragHandleProps,
      index,
      actions,
      salvarParatextoInicial,
      salvarParatextoFinal,
    } = this.props
    const isDraggable = Boolean(dragHandleProps)
    const { deveRenderizarParatextos } = this
    return (
      <div>
        <div className={classes.header}>
          <div className={classes.bordaGrupoEsquerda} />
          <div className={classes.dispMenuGrupos}>
            <div className={classes.grupoBox} key={index}>
              <div {...dragHandleProps} className={classes.dragHandle}>
                {isDraggable && <DragIndicatorIcon />}
                <div className={classnames(classes.grupoText, { [classes.ajusteDrag]: isDraggable })}>
                  {'GRUPO ' + (index + 1)}
                </div>
              </div>
              {actions &&
                actions.map((button, index) => (
                  <div key={index} className={classes.buttonGroup}>
                    {button}
                  </div>
                ))}
            </div>
          </div>
          <div className={classes.bordaGrupoDireita} />
        </div>
        <CardContent className={classes.cardExpandedContent}>
          {deveRenderizarParatextos() ? (
            <ParatextoProvider inicial conteudo={instancia.paratextoInicial} salvarParatexto={salvarParatextoInicial} />
          ) : null}
          <div className={classes.questoesNoGrupo}>
            <ListaQuestoesGrupoComponent key={index} questoesProcessadas={instancia.questoes} grupoIndex={index} />
          </div>
          {deveRenderizarParatextos() ? (
            <ParatextoProvider conteudo={instancia.paratextoFinal} salvarParatexto={salvarParatextoFinal} />
          ) : null}
        </CardContent>
      </div>
    )
  }
}
