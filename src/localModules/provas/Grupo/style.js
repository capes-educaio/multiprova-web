const getBorda = (theme, parentHeight) => ({
  borderStyle: 'dashed',
  borderColor: theme.palette.cinzaDusty,
  height: parentHeight / 2,
  flexGrow: 1,
})

export const style = theme => ({
  dragHandle: {
    display: 'flex',
    alignItems: 'center',
    flexGrow: 1,
    '& svg': {
      fill: theme.palette.gelo,
    },
    paddingLeft: 5,
  },
  grupoText: {
    display: 'flex',
    justifyContent: 'center',
    flexGrow: 1,
    color: theme.palette.gelo,
  },
  ajusteDrag: {
    marginRight: 24,
  },
  dispMenuGrupos: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  header: {
    display: 'flex',
    alignItems: 'flex-end',
    padding: '10px 0px',
  },
  bordaGrupoEsquerda: {
    ...getBorda(theme, 35),
    borderWidth: '1px 0px 0px 1px',
    borderRadius: '5px 0 0 0',
  },
  bordaGrupoDireita: {
    ...getBorda(theme, 35),
    borderWidth: '1px 1px 0px 0px',
    borderRadius: '0 5px 0 0',
  },
  grupoBox: {
    color: theme.palette.steelBlueContrast,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: theme.palette.steelBlue,
    borderRadius: '5px',
    width: '250px',
    fontFamily: 'Arial',
    fontSize: '15px',
    height: '35px',
    position: 'relative',
    overflow: 'hidden',
  },
  card: {
    display: 'flex',
  },
  action: {
    display: 'flex',
    justifyContent: 'center',
    margin: '3px 0',
  },
  cardExpandedContent: {
    wordWrap: 'break-word',
    padding: '0 0 0 0 !important',
    display: 'flex',
    width: '100%',
    flexDirection: 'column',
    '& > *': {
      marginTop: '10px',
    },
  },
  cardContent: {
    width: '100%',
  },
  questoesNoGrupo: {
    width: '100%',
    backgroundColor: theme.palette.gelo,
  },
  buttonGroup: {
    marginRight: 0,
  },
  iconButton: {
    borderRadius: '0px 5px 5px 0px',
  },
  buttonBorder: {
    borderStyle: 'solid',
    borderLeftColor: theme.palette.steelBlueContrast,
    borderWidth: '0px 0px 0px 1px',
  },
})
