import PropTypes from 'prop-types'

export const propTypes = {
  instancia: PropTypes.object.isRequired,
  index: PropTypes.number.isRequired,
  ListaQuestoesGrupoComponent: PropTypes.any.isRequired,
  dragHandleProps: PropTypes.object,
  actions: PropTypes.node,
  // redux state
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}
