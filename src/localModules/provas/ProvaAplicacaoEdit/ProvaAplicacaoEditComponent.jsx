import React, { Component } from 'react'

import { ProvaAplicacaoComum } from 'localModules/provas/ProvaAplicacaoComum'
import { provaUtils } from 'localModules/provas/ProvaUtils'

import { propTypes } from './propTypes'

export class ProvaAplicacaoEditComponent extends Component {
  static propTypes = propTypes

  state = {
    prova: null,
    valorInicial: null,
  }

  componentDidMount = () => {
    this._usuarioAtualId = this.props.usuarioAtual.data.id
    this._provaAtualId = this.props.match.params.id
    provaUtils
      .getProva({ usuarioAtualId: this._usuarioAtualId, provaAtualId: this._provaAtualId })
      .then(this._setValores)
      .catch(e => console.error(e))
  }

  _setValores = r => {
    let prova = r.data

    const { tipoAplicacao, papel, virtual, participantes = [] } = prova.dadosAplicacao

    if (virtual) virtual.duracaoDaProva = String(virtual.duracaoDaProva)

    const valorInicial = { tipoAplicacao, papel, virtual, participantes }
    this.setState({ valorInicial, prova })
  }

  render() {
    const { strings } = this.props
    const { prova, valorInicial } = this.state
    if (prova) {
      return <ProvaAplicacaoComum titulo={strings.editarAplicacao} prova={prova} valorInicial={valorInicial} disabled />
    } else {
      return null
    }
  }
}
