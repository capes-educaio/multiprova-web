import { deslogar } from '../utils/compositeActions/deslogar'
import { openDialogAviso } from 'utils/compositeActions/openDialogAviso'
import { store } from 'utils/store'

export const defaultCatch = (error, method, rota, data) => {
  if (error.message === 'Network Error') {
    const { strings } = store.getState()
    openDialogAviso({ texto: strings.falhaNaRequisicao, titulo: strings.erro })
  } else if (error.response) {
    const { status } = error.response
    if (status === 401) deslogar()
    if (process.env.NODE_ENV !== 'production') {
      console.error('defaultCatch response', error.response)
      if (error.response.data) {
        if (typeof error.response.data === 'object') {
          const messages = error.response.data
          for (let index in messages) {
            console.error('defaultCatch message ' + index, messages[index])
          }
        }
      }
    }
  }
}