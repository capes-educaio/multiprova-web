import axios from 'axios'

import { store } from 'utils/store'
import { cookies } from 'utils/cookies'

import { baseUrl } from './url'
import { defaultCatch } from './defaultCatch'

export const post = async (appendUrl, data) => {
  const url = baseUrl + appendUrl
  const thisPost = JSON.stringify({ url, data })
  if (thisPost === store.getState().lastPost) {
    console.log('Entrada duplicada ignorada.')
    return null
  }
  const authToken = cookies ? cookies.get('authToken') : undefined
  const tokenId = authToken ? authToken.id : undefined
  const headers = tokenId ? { Authorization: tokenId } : undefined
  const config = { headers }
  try {
    return await axios.post(url, data, config)
  } catch (error) {
    defaultCatch(error)
    throw error
  }
}
