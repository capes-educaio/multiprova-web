import { defaultCatch } from '../defaultCatch'

const error = {
  response: {
    data: {
      campo1: 'Erro do campo 1',
      campo2: 'Erro do campo 2',
    },
  },
}

describe('defaultCatch', () => {
  test('Processa erro com mensagens', () => {
    expect(defaultCatch(error)).toBe(undefined)
  })
  test('Processa erro fora do padrão', () => {
    expect(defaultCatch({})).toBe(undefined)
  })
})
