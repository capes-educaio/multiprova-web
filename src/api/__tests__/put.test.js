import axios from 'axios'

import { defaultCatch } from '../defaultCatch'
import { put } from '../put'

jest.mock('../defaultCatch', () => ({
  defaultCatch: jest.fn(),
}))
jest.mock('axios')
jest.mock('utils/store', () => ({
  store: {
    getState: () => ({
      authToken: 'authTokenTestValue',
    }),
  },
}))
jest.mock('../url', () => ({
  baseUrl: 'baseUrl/',
}))

describe('put.js tests', () => {
  test('Quando axios put é resolvido, put é resolvido', () => {
    axios.put = jest.fn().mockResolvedValueOnce('MockSucesso')
    put('a').then(r => {
      expect(axios.put).toBeCalled()
      expect(r).toBe('MockSucesso')
    })
  })
  test('Quando axios.put é rejeitado, put é rejeitado e defaultCatch é chamado', () => {
    axios.put = jest.fn().mockRejectedValueOnce(new Error('MockFalha'))
    expect(put('a')).rejects.toThrow('MockFalha')
    axios.put = jest.fn().mockRejectedValueOnce(new Error('MockFalha'))
    put('a').catch(() => {
      expect(defaultCatch).toBeCalled()
    })
  })
})
