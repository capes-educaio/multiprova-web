import axios from 'axios'

import { defaultCatch } from '../defaultCatch'
import { post } from '../post'

jest.mock('../defaultCatch', () => ({
  defaultCatch: jest.fn(),
}))
jest.mock('axios')
jest.mock('utils/store', () => ({
  store: {
    getState: () => ({
      authToken: 'authTokenTestValue',
    }),
  },
}))
jest.mock('../url', () => ({
  baseUrl: 'baseUrl/',
}))

describe('post.js tests', () => {
  test('Quando axios post é resolvido, post é resolvido', () => {
    axios.post = jest.fn().mockResolvedValueOnce('MockSucesso')
    post('a').then(r => {
      expect(axios.post).toBeCalled()
      expect(r).toBe('MockSucesso')
    })
  })
  test('Quando axios.post é rejeitado, post é rejeitado e defaultCatch é chamado', () => {
    axios.post = jest.fn().mockRejectedValueOnce(new Error('MockFalha'))
    expect(post('a')).rejects.toThrow('MockFalha')
    axios.post = jest.fn().mockRejectedValueOnce(new Error('MockFalha'))
    post('a').catch(() => {
      expect(defaultCatch).toBeCalled()
    })
  })
})
