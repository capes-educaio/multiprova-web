import axios from 'axios'

import { defaultCatch } from '../defaultCatch'
import { patch } from '../patch'

jest.mock('../defaultCatch', () => ({
  defaultCatch: jest.fn(),
}))
jest.mock('axios')
jest.mock('utils/store', () => ({
  store: {
    getState: () => ({
      authToken: 'authTokenTestValue',
    }),
  },
}))
jest.mock('../url', () => ({
  baseUrl: 'baseUrl/',
}))

describe('patch.js tests', () => {
  test('Quando axios.patch é resolvido, patch é resolvido', () => {
    axios.patch = jest.fn().mockResolvedValueOnce('MockSucesso')
    patch('a').then(r => {
      expect(axios.patch).toBeCalled()
      expect(r).toBe('MockSucesso')
    })
  })
  test('Quando axios.patch é rejeitado, patch é rejeitado e defaultCatch é chamado', () => {
    axios.patch = jest.fn().mockRejectedValueOnce(new Error('MockFalha'))
    expect(patch('a')).rejects.toThrow('MockFalha')
    axios.patch = jest.fn().mockRejectedValueOnce(new Error('MockFalha'))
    patch('a').catch(() => {
      expect(defaultCatch).toBeCalled()
    })
  })
})
