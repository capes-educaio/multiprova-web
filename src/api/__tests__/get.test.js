import axios from 'axios'

import { defaultCatch } from '../defaultCatch'
import { get } from '../get'

jest.mock('../defaultCatch', () => ({
  defaultCatch: jest.fn(),
}))
jest.mock('axios')
jest.mock('utils/store', () => ({
  store: {
    getState: () => ({
      authToken: 'authTokenTestValue',
    }),
  },
}))
jest.mock('../url', () => ({
  baseUrl: 'baseUrl/',
}))

describe('get.js tests', () => {
  test('Quando axios get é resolvido, get é resolvido', () => {
    axios.get = jest.fn().mockResolvedValueOnce('MockSucesso')
    get('a').then(r => {
      expect(axios.get).toBeCalled()
      expect(r).toBe('MockSucesso')
    })
  })
  test('Quando axios.get é rejeitado, get é rejeitado e defaultCatch é chamado', () => {
    axios.get = jest.fn().mockRejectedValueOnce(new Error('MockFalha'))
    expect(get('a')).rejects.toThrow('MockFalha')
    axios.get = jest.fn().mockRejectedValueOnce(new Error('MockFalha'))
    get('a').catch(() => {
      expect(defaultCatch).toBeCalled()
    })
  })
})
