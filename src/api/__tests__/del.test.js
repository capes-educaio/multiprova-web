import axios from 'axios'

import { defaultCatch } from '../defaultCatch'
import { del } from '../del'

jest.mock('../defaultCatch', () => ({
  defaultCatch: jest.fn(),
}))
jest.mock('axios')
jest.mock('utils/store', () => ({
  store: {
    getState: () => ({
      authToken: 'authTokenTestValue',
    }),
  },
}))
jest.mock('../url', () => ({
  baseUrl: 'baseUrl/',
}))

describe('del.js tests', () => {
  test('Quando axios delete é resolvido, del é resolvido', () => {
    axios.delete = jest.fn().mockResolvedValueOnce('MockSucesso')
    del('a').then(r => {
      expect(axios.delete).toBeCalled()
      expect(r).toBe('MockSucesso')
    })
  })
  test('Quando axios.delete é rejeitado, del é rejeitado e defaultCatch é chamado', () => {
    axios.delete = jest.fn().mockRejectedValueOnce(new Error('MockFalha'))
    expect(del('a')).rejects.toThrow('MockFalha')
    axios.delete = jest.fn().mockRejectedValueOnce(new Error('MockFalha'))
    del('a').catch(() => {
      expect(defaultCatch).toBeCalled()
    })
  })
})
