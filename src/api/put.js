import axios from 'axios'

import { cookies } from 'utils/cookies'
import { baseUrl } from './url'
import { defaultCatch } from './defaultCatch'

export const put = (appendUrl, data) => {
  const url = baseUrl + appendUrl
  const authToken = cookies ? cookies.get('authToken') : undefined
  const tokenId = authToken ? authToken.id : undefined
  const headers = tokenId ? { Authorization: tokenId } : undefined
  const config = { headers }
  return new Promise((resolve, reject) => {
    axios
      .put(url, data, config)
      .then(resolve)
      .catch(e => {
        defaultCatch(e, 'put', appendUrl, data)
        reject(e)
      })
  })
}
