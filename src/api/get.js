import axios from 'axios'

import { cookies } from 'utils/cookies'

import { baseUrl } from './url'
import { defaultCatch } from './defaultCatch'

export function get(appendUrl, params, tokenIdParam) {
  const url = baseUrl + appendUrl
  const authTokenCookie = cookies ? cookies.get('authToken') : undefined
  const tokenIdCookie = authTokenCookie ? authTokenCookie.id : undefined
  const headers =
    tokenIdCookie || tokenIdParam ? { Authorization: tokenIdParam ? tokenIdParam : tokenIdCookie } : undefined
  const config = { params, headers }
  return new Promise((resolve, reject) => {
    axios
      .get(url, config)
      .then(resolve)
      .catch(e => {
        defaultCatch(e)
        reject(e)
      })
  })
}
