const getDevelopmentUrl = () => {
  let baseWDev = window.location.hostname.replace('dev.', '')
  return `${window.location.protocol}//dev.api.${baseWDev}/api/`
}

const getProductionUrl = () => {
  const hostname = window.location.hostname
  if (hostname.startsWith('dev.')) {
    let baseWDev = window.location.hostname.replace('dev.', '')
    return `${window.location.protocol}//dev.api.${baseWDev}/api/`
  } else {
    return `${window.location.protocol}//api.${window.location.hostname}/api/`
  }
}

const mapNodeEnvToGetUrl = {
  local: () => 'http://localhost:3010/api/',
  production: getProductionUrl,
  development: getDevelopmentUrl,
  test: () => 'http://localhost:3010/api/',
}

const getBaseUrlByNodeEnv = () => {
  const getUrl = mapNodeEnvToGetUrl[process.env.NODE_ENV]
  if (getUrl) return getUrl()
  else console.error('getBaseUrlByNodeEnv: Variável de ambiente NODE_ENV desconhecida.', process.env.NODE_ENV)
  return
}

const getBaseUrl = () => {
  if (!process.env.REACT_APP_USA_DEV_API) return getBaseUrlByNodeEnv()
  switch (process.env.REACT_APP_VARIANT_CONFIG) {
    case 'picco':
      return `${window.location.protocol}//dev.api.picco.comperve.ufrn.br/api/`
    case 'educaio':
      return `${window.location.protocol}//dev.api.educaio.ufrn.br/api/`
    case 'multiprova':
      // return 'http://dev.api.multiprova3.ufrn.br/api/'
      return `${window.location.protocol}//dev.api.picco.comperve.ufrn.br/api/`
    default:
      return `${window.location.protocol}//dev.api.picco.comperve.ufrn.br/api/`
  }
}

export const baseUrl = getBaseUrl()
console.log(`URL API: ${baseUrl}`)
