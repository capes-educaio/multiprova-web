import { GenericRepository } from './GenericRepository'
import { addItemToCollection, getItem, removeItemFromCollection } from './localRepository'
import { post, put } from 'api'
export class InstanciamentoRepository extends GenericRepository {
  constructor() {
    super('instanciamentos')
  }

  get synchronize() {
    return {
      ligar: () => {
        window.addEventListener('online', this.synchronizeRespostas)
      },
      desligar: () => {
        window.removeEventListener('online', this.synchronizeRespostas)
      },
    }
  }

  async synchronizeRespostas() {
    const respostas = getItem('respostas')
    // salvar as questões do cache
    for (let item of respostas) {
      const { method, rota, data } = item

      if (method === 'put') {
        await put(rota, data)
      } else if (method === 'post') {
        await post(rota, data)
      }
      removeItemFromCollection('respostas', (el) => el.rota === rota)
    }
  }

  salvarResposta(instancia, resposta) {
    const rota = `${this.entityName}/${instancia.id}/salvar-resposta`
    return new Promise((resolve, reject) => {
      this.crud.update(rota, resposta).then(res => {
        resolve(res)
      }).catch(err => {
        addItemToCollection('respostas', { method: 'put', rota, data: resposta })
        reject(err)
      })
    })
  }
}
