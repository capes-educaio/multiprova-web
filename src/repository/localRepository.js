
function setItem(key, value) {
  if (typeof value === 'object') {
    localStorage.setItem(key, JSON.stringify(value))
  } else {
    localStorage.setItem(key, value)
  }
}

function getItem(key) {
  try {
    const value = localStorage.getItem(key)
    return JSON.parse(value)
  }
  catch (e) {
    return localStorage.getItem(key)
  }
}

function clear() {
  localStorage.clear()
}

function addItemToCollection(key, item) {
  const value = localStorage.getItem(key)
  if (value !== undefined && value !== null) {
    try {
      const collection = JSON.parse(value)
      collection.push(item)
      localStorage.setItem(key, JSON.stringify(collection))
    } catch (e) {
      throw new Error(`${key} is not a collection`)
    }
  } else {
    const collection = []
    collection.push(item)
    localStorage.setItem(key, JSON.stringify(collection))
  }
}

function removeItemFromCollection(key, predicao) {
  const value = localStorage.getItem(key)
  if (value !== undefined) {
    try {
      const collection = JSON.parse(value)
      const index = collection.findIndex(predicao)
      if (index !== -1) {
        collection.splice(index, 1)
        localStorage.setItem(key, JSON.stringify(collection))
      }
    } catch (e) {
      throw new Error(`${key} is not a collection`)
    }
  } else {
    throw new Error(`${key} is empty`)
  }
}

export {
  setItem,
  getItem,
  addItemToCollection,
  removeItemFromCollection,
  clear,
}