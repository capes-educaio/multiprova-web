import { setItem, getItem, addItemToCollection, removeItemFromCollection, clear } from '../localRepository'

let storage = {}

const localStorageMock = {
  getItem: (key) => { return storage[key] },
  setItem: (key, value) => { storage[key] = value },
  clear: () => { storage = {} },
}

global.localStorage = localStorageMock

describe('Testar utilitarios para localstorage', () => {

  it('set string value', () => {
    clear()
    setItem('nome', 'talison')
    expect(typeof getItem('nome')).toBe('string')
  })

  it('set object value', () => {
    clear()
    setItem('pessoa', { 'nome': 'talison' })
    expect(typeof getItem('pessoa')).toBe('object')
  })

  it('add item to collection', () => {
    clear()
    addItemToCollection('alunos', { 'nome': 'talison' })
    expect(getItem('alunos') instanceof Array).toBe(true)
  })

  it('add item to collection', () => {
    clear()
    addItemToCollection('alunos', { 'nome': 'talison' })
    addItemToCollection('alunos', { 'nome': 'jose' })
    addItemToCollection('alunos', { 'nome': 'rodrigo' })
    expect(getItem('alunos').length).toBe(3)
  })

  it('remove item from collection', () => {
    clear()
    addItemToCollection('alunos', { 'nome': 'talison' })
    addItemToCollection('alunos', { 'nome': 'jose' })
    addItemToCollection('alunos', { 'nome': 'rodrigo' })
    removeItemFromCollection('alunos', (it) => it.nome === 'talison')
    expect(getItem('alunos').length).toBe(2)
  })

})