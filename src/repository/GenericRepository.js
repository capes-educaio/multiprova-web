import { get, post, put, del } from 'api'

export class GenericRepository {

  constructor(entityName) {
    this.entityName = entityName
  }

  get crud() {
    return {
      create: (url, payload) => post(url, payload),
      update: (url, payload) => put(url, payload),
      delete: url => del(url),
      read: url => get(url),
    }
  }

  save(payload) {
    return post(this.entityName, payload)
  }

  update(payload) {
    return put(this.entityName, payload)
  }

  findAll() {
    return get(this.entityName)
  }

  findById(id) {
    return get(`${this.entityName}/${id}`)
  }

  delete(id) {
    return del(`${this.entityName}/${id}`)
  }
}
