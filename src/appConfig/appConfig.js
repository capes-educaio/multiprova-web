import { educaio } from './educaio'
import { defaultConfig } from './defaultConfig'

const variantConfigs = {
  educaio: educaio,
}

const getConfigVariantByHostname = () => {
  let { hostname } = window.location
  const variant = hostname.replace('dev.', '').split('.')[0]
  return variantConfigs[variant]
}

const getConfigVariantByEnv = () => {
  const variant = variantConfigs[process.env.REACT_APP_VARIANT_CONFIG]
  return variant ? variant : educaio
}

const getFullConfig = variant => {
  const config = { ...defaultConfig, ...variant }
  for (let key in config) {
    if (typeof defaultConfig[key] === 'object' && typeof variant[key] === 'object') {
      config[key] = { ...defaultConfig[key], ...variant[key] }
    }
  }
  return config
}

const getConfig = () => {
  let variant = getConfigVariantByHostname()
  if (!variant) variant = getConfigVariantByEnv()
  return getFullConfig(variant)
}

export const appConfig = getConfig()
