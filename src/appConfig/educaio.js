export const educaio = Object.freeze({
  projeto: {
    isEducaio: true,
  },
  strings: {
    nomeProjeto: 'EDUCA-IO',
  },
  tipoQuestoesHabilitados: {
    multiplaEscolha: true,
    bloco: false,
    discursiva: true,
    vouf: true,
    associacaoDeColunas: true,
    redacao: false,
  },
  modulosHabilitados: {
    notificacoes: true,
  },
})
