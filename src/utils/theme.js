import { createMuiTheme } from '@material-ui/core/styles'
import { blueGrey, green, amber, grey, blue, indigo, red, yellow, purple, orange } from '@material-ui/core/colors'

export const theme = createMuiTheme({
  typography: {
    useNextVariants: true,
  },
  palette: {
    primary: {
      light: '#54647b',
      main: '#293a4f',
      dark: '#001427',
      contrastText: '#fff',
    },
    secondary: {
      light: '#ffd75b',
      main: '#ffa626',
      dark: '#c67700',
      contrastText: '#000000',
    },
    success: { dark: green[600], contrastText: '#fff' },
    warning: { dark: amber[700], contrastText: '#fff' },
    error: { main: red[500], dark: red[700], contrastText: '#fff' },
    default: {
      light: blueGrey[200],
      medium: blueGrey[400],
      main: blueGrey[500],
      dark: blueGrey[900],
    },
    fundo: {
      amarelo: '#FFFDE7',
      indigo: '#E8EAF6',
      cinza: '#f5f5f5',
    },
    grafico: {
      grey: { light: grey[300], main: grey[400], dark: grey[900], transparent: 'rgb(110,110,110, 0.5)' },
      blue: { lighter: blue[100], light: blue[300], main: blue[500], dark: blue[900] },
      indigo: { light: indigo[300], main: indigo[500] },
      red: { light: red[300], middle: red[400], main: red[500] },
      yellow: { light: yellow[300], main: yellow[800], dark: yellow[900] },
      green: { light: green[500], main: green[700], dark: green[900] },
      purple: { light: purple[300], main: purple[500] },
    },
    card: {
      orange: { light: orange[400], main: orange[500], dark: orange[600] },
      blue: { light: blue[500], main: blue[700], dark: blue[900] },
    },
    amareloQueimado: '#f7a530',
    verdeEscuro: '#20800e',
    cinzaEscuro: '#434440',
    cinza300: '#e8e8e8',
    cinza700: grey[700],
    defaultGrey: 'rgba(0, 0, 0, 0.54)',
    darkGray: 'rgba(0, 0, 0, 0.87)',
    decorarCorreta: 'limegreen',
    gelo: '#ffffff',
    steelBlue: '#3d73b4',
    darkBlue: '#315c90',
    mediumBlue: '#359bd6',
    minimumBlue: '#add8e6',
    steelBlueContrast: '#d6deea',
    cinzaAlto: '#dfdfdf',
    cinzaMedio: '#d0d0d0',
    cinzaMaisMedio: '#d8d8d8',
    cinzaPadrao: '#808080',
    cinzaDusty: '#949494',
    magentaEscuro: '#4a1564',
  },
})
