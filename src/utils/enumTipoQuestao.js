export const enumTipoQuestao = {
  tipoQuestaoBloco: 'bloco',
  tipoQuestaoDiscursiva: 'discursiva',
  tipoQuestaoMultiplaEscolha: 'multiplaEscolha',
  tipoQuestaoVerdadeiroOuFalso: 'vouf',
  tipoQuestaoAssociacaoDeColunas: 'associacaoDeColunas',
  tipoQuestaoRedacao: 'redacao',
}
