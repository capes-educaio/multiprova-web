const getDificuldade = dificuldade => {
  let jsonDificuldade
  if (dificuldade) {
    jsonDificuldade = {
      or: [{ dificuldade: dificuldade }, { 'bloco.questoes.dificuldade': dificuldade }],
    }
  } else {
    jsonDificuldade = {}
  }
  return jsonDificuldade
}
const getStatus = status => {
  let jsonStatus
  if (status !== 0) {
    jsonStatus = { 'status.id': status }
  } else {
    jsonStatus = {}
  }
  return jsonStatus
}
const getAnoEscolar = anoEscolar => {
  let jsonAnoEscolar

  if (anoEscolar !== 0) {
    jsonAnoEscolar = { or: [{ anoEscolar: anoEscolar }, { 'bloco.questoes.anoEscolar': anoEscolar }] }
  } else {
    jsonAnoEscolar = {}
  }
  return jsonAnoEscolar
}

const getTipo = tipo => {
  let jsonTipo
  if (tipo !== 0) {
    jsonTipo = { tipo: tipo }
  } else {
    jsonTipo = {}
  }
  return jsonTipo
}

const getTags = tagsSelected => {
  let jsonTag
  if (typeof tagsSelected !== 'undefined' && tagsSelected.length > 0) {
    let jsonTagQuestao = { and: [] }
    let jsonTagBloco = { and: [] }
    jsonTag = { or: [] }
    tagsSelected.forEach(tag => {
      jsonTagQuestao.and.push({ tagIds: tag.id })
      jsonTagBloco.and.push({ 'bloco.questoes.tagIds': tag.id })
    })
    jsonTag.or.push(jsonTagQuestao)
    jsonTag.or.push(jsonTagBloco)
  } else {
    jsonTag = {}
  }
  return jsonTag
}

const getElaborador = elaborador => {
  let jsonElaborador
  if (!!elaborador) {
    jsonElaborador = { elaborador: elaborador }
  } else {
    jsonElaborador = {}
  }
  return jsonElaborador
}

export const montarFiltroQuestao = valorDoForm => {
  const { textoFiltro, dificuldade, tagsSelected, status, anoEscolar, tipo, elaborador } = valorDoForm
  const expressao = `.*${textoFiltro}.*`
  let where = {
    and: [
      {
        or: [
          { enunciado: { regexp: expressao } },
          { 'multiplaEscolha.alternativas.texto': { regexp: expressao } },
          { 'bloco.fraseInicial': { regexp: expressao } },
          { 'bloco.questoes.enunciado': { regexp: expressao } },
          { 'bloco.questoes.multiplaEscolha.alternativas.texto': { regexp: expressao } },
          { 'bloco.questoes.vouf.afirmacoes.texto': { regexp: expressao } },
          { 'bloco.questoes.discursiva.expectativaDeResposta': { regexp: expressao } },
          { 'vouf.afirmacoes.texto': { regexp: expressao } },
          { 'discursiva.expectativaDeResposta': { regexp: expressao } },
          { 'associacaoDeColunas.colunaA.conteudo': { regexp: expressao } },
          { 'associacaoDeColunas.colunaB.conteudo': { regexp: expressao } },
        ],
      },
      getDificuldade(dificuldade),
      getTags(tagsSelected),
      getStatus(status),
      getAnoEscolar(anoEscolar),
      getTipo(tipo),
      getElaborador(elaborador),
    ],
  }
  const include = {
    relation: 'tags',
  }
  return {
    include,
    where,
  }
}

if (process.env.NODE_ENV === 'test') {
  exports.getDificuldade = getDificuldade
  exports.getTags = getTags
  exports.getStatus = getStatus
  exports.getAnoEscolar = getAnoEscolar
  exports.getTipo = getTipo
  exports.getElaborador = getElaborador
}
