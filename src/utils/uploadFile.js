import { post, baseUrl } from 'api'

export const uploadFile = async (file, type) => {
  const url = `armazenamentos/${type}/`
  const data = new FormData()
  data.append('file', file)
  const { data: responseData } = await post(url + 'upload', data)
  const { name } = responseData.result.files.file[0]
  const src = `${baseUrl}${url}download/${name}`
  return src
}
