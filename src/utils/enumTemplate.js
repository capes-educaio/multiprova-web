export const enumTemplate = {
  multiplaEscolhaGeral: 'geral/previewMultiplaEscolha',
  blocoGeral: 'geral/previewBloco',
  provaGeral: 'geral/prova',
  provaGeralCandidato: 'geral/provaCandidato',
  multiplaEscolhaComperve: 'comperve/previewMultiplaEscolha',
  blocoComperve: 'comperve/previewBloco',
  provaComperve: 'comperve/prova',
}
