export const arredondar = (num, places) => {
  return parseFloat(num).toFixed(places)
}
