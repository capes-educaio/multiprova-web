import { get } from 'api'
import { baseUrl } from 'api/url'

import { cookies } from 'utils/cookies'
import { esperar } from 'utils/esperar'

const consultar = async (impressaoId, options) => {
  const numeroDeConsultas = 40
  const esperaEntreConsultas = 1000
  let consultasRestantes = numeroDeConsultas
  while (consultasRestantes > 0) {
    await esperar(esperaEntreConsultas)
    const { data: impressao } = await get(`/impressao/${impressaoId}`)
    if (impressao.status === 'PRONTO' || options.stop) return
    else consultasRestantes--
  }
  throw new Error(
    `Timeout. A impressao não ficou pronta depois de ${(numeroDeConsultas * esperaEntreConsultas) / 1000}s`,
  )
}

export const getUrlDownload = async (impressaoId, options = {}) => {
  await consultar(impressaoId, options)
  if (options.stop) return
  else return baseUrl + `impressao/download/${impressaoId}/?access_token=${cookies.get('authToken').id}`
}
