import React from 'react'

export const getUpdateContext = (thisComponent, contextKeys) => async (newContextPropertiesArg, callbackArg) => {
  const newContextProperties = { ...newContextPropertiesArg }
  return new Promise(resolve => {
    const callback = callbackArg ? callbackArg : resolve
    Object.keys(newContextProperties).forEach(contextPropertyKey => {
      if (!contextKeys.includes(contextPropertyKey)) {
        console.warn(
          `Falha em updateContext, a chave ${contextPropertyKey} não existe no contexto. 
          Chaves válidas: ${JSON.stringify(contextKeys)}`,
        )
        delete newContextProperties[contextPropertyKey]
      }
    })
    thisComponent._contextValue = { ...thisComponent._contextValue, ...newContextProperties }
    thisComponent.setState({ context: thisComponent._contextValue }, callback)
  })
}

export const initiateContextState = (thisComponent, initialState) => {
  const contextKeys = Object.keys(initialState)
  const contextValue = { ...initialState, updateContext: getUpdateContext(thisComponent, contextKeys) }
  thisComponent._contextValue = contextValue
  thisComponent._updateContext = contextValue.updateContext
  if (thisComponent.state && typeof thisComponent.state === 'object') thisComponent.state.context = contextValue
  else thisComponent.state = { context: contextValue }
}

export const withContext = (context, contextName) => Component => props => {
  return <context.Consumer>{value => <Component {...{ [contextName]: value, ...props }} />}</context.Consumer>
}
