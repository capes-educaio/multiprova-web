import { calculaTempoRestanteDaProva } from './calculaTempoRestanteDaProva'

const DATA_ATUAL = '2019-03-30T12:15:00.226Z'
const INICIO_DA_PROVA = '2019-03-30T12:00:00.226Z'
const DURACAO_DA_PROVA = 60
const RESULTADO_ESPERADO = { timestamp: 2700000, horas: 0, minutos: 45, segundos: 0 }

describe('Calculo de tempo da prova', () => {
  it('Calcula Tempo Restante Da Prova ', () => {
    const resultado = calculaTempoRestanteDaProva(DATA_ATUAL, INICIO_DA_PROVA, DURACAO_DA_PROVA)
    expect(resultado).toEqual(RESULTADO_ESPERADO)
  })
})
