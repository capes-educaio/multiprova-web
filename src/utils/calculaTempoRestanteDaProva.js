export const calculaTempoRestanteDaProva = (dataAtual, inicioDaProva, duracaoDaProva) => {
  let dAtual = Date.parse(dataAtual)
  let inicioProva = Date.parse(inicioDaProva)
  let duracao = duracaoDaProva * 60000

  let timestamp = duracao - (dAtual - inicioProva)

  let segundos = parseInt((timestamp / 1000) % 60)
  let minutos = parseInt((timestamp / 1000 - segundos) / 60) % 60
  let horas = parseInt(timestamp / 3600000)

  return { timestamp: timestamp, horas: horas, minutos, segundos }
}