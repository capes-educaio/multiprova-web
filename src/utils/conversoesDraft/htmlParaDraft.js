import { ContentState } from 'draft-js'
import htmlToDraft from 'html-to-draftjs'

import { TYPE_TEX, TYPE_IMG } from 'utils/RichText'

const atomicOptions = (type, content) => ({ type, data: { content }, mutability: 'IMMUTABLE' })

const removeTexBlockDelimeters = string => string.replace(/\$\$/g, '')

export const htmlParaDraft = html => {
  const blocksFromHtml = htmlToDraft(html, (nodeName, node) => {
    const isAtomicBlock = nodeName === 'figure'
    if (isAtomicBlock) {
      const atomicNode = node.firstChild
      switch (atomicNode.className) {
        case TYPE_TEX:
          return atomicOptions(TYPE_TEX, removeTexBlockDelimeters(atomicNode.innerHTML))
        case TYPE_IMG:
          return atomicOptions(TYPE_IMG, atomicNode.firstChild.src)
        default:
          console.error('Tipo de bloco desconhecido')
          return false
      }
    }
    return false
  })
  const { contentBlocks, entityMap } = blocksFromHtml
  return ContentState.createFromBlockArray(contentBlocks, entityMap)
}
