import React from 'react'
import { convertToHTML } from 'draft-convert'

import { TYPE_TEX, TYPE_IMG } from 'utils/RichText'

export const draftParaHtml = draft => {
  const options = {
    entityToHTML: ({ type, data }, originalText) => {
      switch (type) {
        case TYPE_TEX:
          return <span className={TYPE_TEX}>{`$$${data.content}$$`}</span>
        case TYPE_IMG:
          return (
            <div className={TYPE_IMG}>
              <img src={data.content} alt="" />
            </div>
          )
        default:
          console.error('Tipo de bloco desconhecido')
          return originalText
      }
    },
  }
  return convertToHTML(options)(draft)
}
