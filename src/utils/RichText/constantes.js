export const DRAFT_EMPTY = '<p></p>'

export const TYPE_TEX = 'TEX'
export const TYPE_IMG = 'IMAGE'
export const TEX_BLOCK = 'tex-block'
export const IMG_BLOCK = 'img-block'
