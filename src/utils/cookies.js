import Cookies from 'universal-cookie'

export const cookies = new Cookies()

export function removeAllCookies(key) {
  if (cookies.get(key)) {
    const cookieOptions = {
      path: '/',
    }
    cookies.remove(key, cookieOptions)
    removeAllCookies(key)
  }
}
