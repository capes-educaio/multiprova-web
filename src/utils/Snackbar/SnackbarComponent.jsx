import React, { Component } from 'react'

import Snackbar from '@material-ui/core/Snackbar'
import SnackbarContent from '@material-ui/core/SnackbarContent'
import IconButton from '@material-ui/core/IconButton'
import CloseIcon from '@material-ui/icons/Close'

export class SnackbarComponent extends Component {
  _close = () => {
    const { removeMessage } = this.props
    setTimeout(() => {
      removeMessage()
    }, 2500)
  }

  render() {
    const { open, message, variant, classes, onClose, autoHideDuration, vertical, horizontal } = this.props
    const { _close } = this
    this._variant = variant
    return (
      <Snackbar
        anchorOrigin={{
          vertical: vertical ? vertical : 'top',
          horizontal: horizontal ? horizontal : 'center',
        }}
        open={open}
        autoHideDuration={autoHideDuration ? autoHideDuration : 5000}
        onClose={onClose ? onClose : _close}
        ContentProps={{
          'aria-describedby': 'message-id',
        }}
      >
        <SnackbarContent
          className={classes[variant]}
          message={<span id="message-id">{message}</span>}
          action={[
            <IconButton key="close" aria-label="Close" color="inherit" className={classes.close} onClick={onClose}>
              <CloseIcon />
            </IconButton>,
          ]}
        />
      </Snackbar>
    )
  }
}
