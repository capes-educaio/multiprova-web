import { dispatchToStore } from 'utils/compositeActions/dispatchToStore'

export const showMessage = {
  success: ({ message }) => {
    dispatchToStore('push', 'messages', { message, variant: 'success' })
  },
  error: ({ message }) => {
    dispatchToStore('push', 'messages', { message, variant: 'error' })
  },
  warn: ({ message }) => {
    dispatchToStore('push', 'messages', { message, variant: 'warning' })
  },
}
