import { bindActionCreators } from 'redux'
import { actions } from 'utils/actions'

export function mapStateToProps(state) {
  return {
    strings: state.strings,
  }
}

export function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    removeMessage: actions.pop.messages,
  }, dispatch)
}
