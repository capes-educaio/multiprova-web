import { createStore } from 'utils/createStore'
import { rootReducer } from 'utils/rootReducer'

export const store = createStore(rootReducer)
