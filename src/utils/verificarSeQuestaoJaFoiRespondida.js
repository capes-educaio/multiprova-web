import { enumTipoQuestao } from './enumTipoQuestao'
import { EMPTY_EDITOR } from 'utils/Editor'

export const verificarSeQuestaoJaFoiRespondida = questao => {
  switch (questao.tipo) {
    case enumTipoQuestao.tipoQuestaoMultiplaEscolha:
      return verificarMultiplaEscolha(questao)
    case enumTipoQuestao.tipoQuestaoDiscursiva:
      return verificarDiscursiva(questao)
    case enumTipoQuestao.tipoQuestaoVerdadeiroOuFalso:
      return verificarVouF(questao)
    case enumTipoQuestao.tipoQuestaoAssociacaoDeColunas:
      return verificarAssociacaoDeColunas(questao)
    default:
      return false
  }
}

const verificarMultiplaEscolha = ({ multiplaEscolha }) =>
  !!multiplaEscolha.respostaCandidato && multiplaEscolha.respostaCandidato !== '?'

const verificarDiscursiva = ({ discursiva }) =>
  !!discursiva.respostaCandidato && discursiva.respostaCandidato !== EMPTY_EDITOR

const verificarVouF = ({ vouf }) => !vouf.afirmacoes.some(({ respostaCandidato }) => !respostaCandidato)

const verificarAssociacaoDeColunas = ({ associacaoDeColunas }) =>
  !associacaoDeColunas.colunaB.some(({ respostaCandidato }) => !respostaCandidato)
