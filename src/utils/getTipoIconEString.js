import DiscursivaIcon from '@material-ui/icons/Subject'
import BlocoIcon from 'localModules/icons/BlocoIcon'
import MultiplaEscolhaIcon from 'localModules/icons/MultiplaEscolhaIcon'
import VouFIcon from 'localModules/icons/VouFIcon'
import AssociacaoDeColunasIcon from 'localModules/icons/AssociacaoDeColunasIcon'
import RedacaoIcon from 'localModules/icons/RedacaoIcon'
import { enumTipoQuestao } from 'utils/enumTipoQuestao'

export const getTipoIconEString = (tipo, strings) => {
  switch (tipo) {
    case enumTipoQuestao.tipoQuestaoMultiplaEscolha:
      return { tipoString: strings.tipoQuestaoMultiplaEscolha, TipoIcon: MultiplaEscolhaIcon }
    case enumTipoQuestao.tipoQuestaoVerdadeiroOuFalso:
      return { tipoString: strings.tipoQuestaoVerdadeiroOuFalso, TipoIcon: VouFIcon }
    case enumTipoQuestao.tipoQuestaoDiscursiva:
      return { tipoString: strings.tipoQuestaoDiscursiva, TipoIcon: DiscursivaIcon }
    case enumTipoQuestao.tipoQuestaoBloco:
      return { tipoString: strings.tipoQuestaoBloco, TipoIcon: BlocoIcon }
    case enumTipoQuestao.tipoQuestaoRedacao:
      return { tipoString: strings.tipoQuestaoRedacao, TipoIcon: RedacaoIcon }
    case enumTipoQuestao.tipoQuestaoAssociacaoDeColunas:
      return { tipoString: strings.tipoQuestaoAssociacaoDeColunas, TipoIcon: AssociacaoDeColunasIcon }
    default:
      return { tipoString: undefined, TipoIcon: undefined }
  }
}
