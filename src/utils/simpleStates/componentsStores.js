import { getInitialStore as getProvaStore } from 'localModules/provas/ProviderProva/getInitialStore'
import { getInitialStore as getStepperSimplesStore } from 'localModules/Stepper/StepperSimples/getInitialStore'
import { getInitialStore as getTurmaSimpleStore } from 'App/Turma/getInitialStore'

export const componentsStores = {
  ...getProvaStore(),
  ...getStepperSimplesStore(),
  ...getTurmaSimpleStore(),
}
