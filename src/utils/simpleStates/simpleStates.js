import { componentsStores } from './componentsStores'
import { reduxStates as simpleReduxFormStates } from 'localModules/ReduxForm/reduxStates'
import { bancoDeQuestoes } from 'utils/enumBancoDeQuestoes'
import { bancoDeProvas } from 'utils/enumBancoDeProvas'
import { enumFiltroPorTempo } from 'utils/enumFiltroPorTempo'

export const simpleStates = {
  theme: 'light',
  usuarioAtual: null,
  instanciaProva: null,
  instanciaProvaReadOnly: { readMode: false },
  prova: null,
  notificacoes: [],
  questaoId: null,
  authToken: null,
  menuEsquerdoAberto: false,
  listaDialog: [],
  subscribeUrlState: false,
  listaTags: null,
  listaElaboradores: null,
  questoesSelecionadasDicionario: {},
  provasSelecionadasDicionario: {},
  filtro: {},
  valorProvaNaVolta: null,
  ...componentsStores,
  ...simpleReduxFormStates,
  tipoBancoQuestoes: bancoDeQuestoes.minhas,
  tipoBancoProvas: bancoDeProvas.todasAsProvas,
  tipoBancoTurmas: enumFiltroPorTempo.todas,
  resumo: [],
  matrixDeCorrecao: [],
  carregando: false,
  messages: [],
  urlParaEnviarEmail: window.location.origin + '/resgatar',
  isMobile: false,
}
