export const enumAnoEscolar = {
  ensinoSuperiorBasico: 'Ensino superior: basico',
  ensinoSuperiorAvancado: 'Ensino superior: avancado',
  terceiroAnoEnsinoMedio: '3o ano do ensino medio',
  segundoAnoEnsinoMedio: '2o ano do ensino medio',
  primeiroAnoEnsinoMedio: '1o ano do ensino medio',
  ensinoFundamental2: 'Ensino fundamental II',
  ensinoFundamental1: 'Ensino fundamental I',
}
