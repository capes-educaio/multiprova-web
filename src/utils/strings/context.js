import React from 'react'
import { withContext } from 'utils/context'

export const StringsContext = React.createContext()
export const withStringsContext = withContext(StringsContext, 'strings')
