import { ptBr } from './ptBr'
import { appConfig } from 'appConfig'

export { replaceAsync } from './replaceAsync'
export const languages = { ptBr: { ...ptBr, ...appConfig.strings } }
