export const composeDateTimeFromDateAndTime = (date, time) => {
  let dateTime
  if (date && time) {
    dateTime = `${date}T${time}`
  } else {
    dateTime = ''
  }

  return new Date(dateTime)
}
export const composeDateAndTimeFromDateTime = dateTime => {
  if (!dateTime) {
    return { data: '', horario: '' }
  }

  const time = new Date(dateTime)

  if (!time) {
    console.warn('Data invalida foi passada.')
    return { data: '', horario: '' }
  }
  const ano = time.getFullYear()
  const mes = ('0' + (time.getMonth() + 1)).slice(-2)
  const dia = ('0' + time.getDate()).slice(-2)
  const hora = ('0' + time.getHours()).slice(-2)
  const minuto = ('0' + time.getMinutes()).slice(-2)

  return { data: `${ano}-${mes}-${dia}`, horario: `${hora}:${minuto}` }
}
