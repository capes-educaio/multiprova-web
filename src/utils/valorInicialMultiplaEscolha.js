import { EMPTY_EDITOR } from 'utils/Editor'
import { enumStatusQuestao } from 'utils/enumStatusQuestao'
import { enumAnoEscolar } from 'utils/enumAnoEscolar'
import { enumTipoQuestao } from 'utils/enumTipoQuestao'

export const VALOR_INICIAL_MULTIPLA_ESCOLHA = {
  tipo: enumTipoQuestao.tipoQuestaoMultiplaEscolha,
  enunciado: EMPTY_EDITOR,
  multiplaEscolha: {
    alternativas: [EMPTY_EDITOR, EMPTY_EDITOR, EMPTY_EDITOR, EMPTY_EDITOR, EMPTY_EDITOR],
  },
  dificuldade: 2,
  tags: [],
  statusId: enumStatusQuestao.elaboracao.id,
  anoEscolar: enumAnoEscolar.ensinoSuperiorBasico,
}
