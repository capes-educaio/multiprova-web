import {
  montarFiltroQuestao,
  getDificuldade,
  getTags,
  getStatus,
  getAnoEscolar,
  getTipo,
  getElaborador,
} from './montarFiltroQuestao'
import Enzyme from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

Enzyme.configure({ adapter: new Adapter() })

const RESULTADO_ESPERADO_VAZIO = {}
const TAGS = [{ id: 1 }, { id: 2 }, { id: 3 }]
const FACIL = 1
const EM_ELABORACAO = 'el'
const TERCEIRO_ANO_ENSINO_MEDIO = '3o ano do ensino medio'
const V_OU_F = 'vouf'
const TEXTO_FILTRO_ESPERADO = '.*TESTE.*'
const TEXTO_FILTRO = 'TESTE'
const ELABORADOR = 'John Doe'
const RESULTADO_ESPERADO_DIFICULDADE = {
  or: [{ dificuldade: FACIL }, { 'bloco.questoes.dificuldade': FACIL }],
}
const RESULTADO_ESPERADO_ANO_ESCOLAR = {
  or: [
    { anoEscolar: TERCEIRO_ANO_ENSINO_MEDIO },
    { 'bloco.questoes.anoEscolar': TERCEIRO_ANO_ENSINO_MEDIO },
  ],
}
const RESULTADO_ESPERADO_TIPO = { 'tipo' : V_OU_F }
const RESULTADO_ESPERADO_STATUS = { 'status.id': EM_ELABORACAO }
const RESULTADO_ESPERADO_TAG = {
  or: [
    { and: [{ tagIds: 1 }, { tagIds: 2 }, { tagIds: 3 }] },
    {
      and: [
        { 'bloco.questoes.tagIds': 1 },
        { 'bloco.questoes.tagIds': 2 },
        { 'bloco.questoes.tagIds': 3 },
      ],
    },
  ],
}
const RESULTADO_ESPERADO_ELABORADOR = { 'elaborador': ELABORADOR }

const where = {
  and: [
    {
      or: [
        { enunciado: { regexp: TEXTO_FILTRO_ESPERADO } },
        { 'multiplaEscolha.alternativas.texto': { regexp: TEXTO_FILTRO_ESPERADO } },
        { 'bloco.fraseInicial': { regexp: TEXTO_FILTRO_ESPERADO } },
        { 'bloco.questoes.enunciado': { regexp: TEXTO_FILTRO_ESPERADO } },
        { 'bloco.questoes.multiplaEscolha.alternativas.texto': { regexp: TEXTO_FILTRO_ESPERADO } },
        { 'bloco.questoes.vouf.afirmacoes.texto': { regexp: TEXTO_FILTRO_ESPERADO } },
        { 'bloco.questoes.discursiva.expectativaDeResposta': { regexp: TEXTO_FILTRO_ESPERADO } },
        { 'vouf.afirmacoes.texto': { regexp: TEXTO_FILTRO_ESPERADO } },
        { 'discursiva.expectativaDeResposta': { regexp: TEXTO_FILTRO_ESPERADO } },
        { 'associacaoDeColunas.colunaA.conteudo': { regexp: TEXTO_FILTRO_ESPERADO } },
        { 'associacaoDeColunas.colunaB.conteudo': { regexp: TEXTO_FILTRO_ESPERADO } },
      ],
    },
    RESULTADO_ESPERADO_DIFICULDADE,
    RESULTADO_ESPERADO_TAG,
    RESULTADO_ESPERADO_STATUS,
    RESULTADO_ESPERADO_ANO_ESCOLAR,
    RESULTADO_ESPERADO_TIPO,
    RESULTADO_ESPERADO_ELABORADOR,
  ],
}
const include = {
  relation: 'tags',
}
const RESULTADO_ESPERADO_FILTRO = {
  include,
  where,
}

describe('Testando <montaFiltroQuestao.js />', () => {
  it('Retorna filtro dificuldade ', () => {
    const filtroDificuldade = getDificuldade(FACIL)
    expect(filtroDificuldade).toEqual(RESULTADO_ESPERADO_DIFICULDADE)
  })
  it('Retorna filtro dificuldade vazio', () => {
    const NAO_FILTRADO = undefined
    const filtroDificuldade = getDificuldade(NAO_FILTRADO)
    expect(filtroDificuldade).toEqual(RESULTADO_ESPERADO_VAZIO)
  })

  it('Retorna filtro tag ', () => {
    const filtroTag = getTags(TAGS)
    expect(filtroTag).toEqual(RESULTADO_ESPERADO_TAG)
  })
  it('Retorna filtro tag vazio', () => {
    const NAO_FILTRADO = undefined
    const filtroTag = getTags(NAO_FILTRADO)
    expect(filtroTag).toEqual(RESULTADO_ESPERADO_VAZIO)
  })
  it('Retorna filtro status ', () => {
    const filtroStatus = getStatus(EM_ELABORACAO)
    expect(filtroStatus).toEqual(RESULTADO_ESPERADO_STATUS)
  })
  it('Retorna filtro status vazio', () => {
    const NAO_FILTRADO = undefined
    const filtroStatus = getStatus(NAO_FILTRADO)
    expect(filtroStatus).toEqual(RESULTADO_ESPERADO_VAZIO)
  })
  it('Retorna ano escolar vazio', () => {
    const NAO_FILTRADO = 0
    const filtroStatus = getAnoEscolar(NAO_FILTRADO)
    expect(filtroStatus).toEqual(RESULTADO_ESPERADO_VAZIO)
  })
  it('Retorna ano escolar terceiro ano do ensino medio', () => {
    const filtroStatus = getAnoEscolar(TERCEIRO_ANO_ENSINO_MEDIO)
    expect(filtroStatus).toEqual(RESULTADO_ESPERADO_ANO_ESCOLAR)
  })

  it('Retorna filtro tipo ', () => {
    const filtroTipo = getTipo(V_OU_F)
    expect(filtroTipo).toEqual(RESULTADO_ESPERADO_TIPO)
  })
  it('Retorna filtro tipo vazio', () => {
    const NAO_FILTRADO = 0
    const filtroTipo = getTipo(NAO_FILTRADO)
    expect(filtroTipo).toEqual(RESULTADO_ESPERADO_VAZIO)
  })

  it('Retorna filtro elaborador ', () => {
    const filtroElaborador = getElaborador(ELABORADOR)
    expect(filtroElaborador).toEqual(RESULTADO_ESPERADO_ELABORADOR)
  })
  it('Retorna filtro elaborador vazio', () => {
    const NAO_FILTRADO = ''
    const filtroElaborador = getElaborador(NAO_FILTRADO)
    expect(filtroElaborador).toEqual(RESULTADO_ESPERADO_VAZIO)
  })

  it('Retorna filtro ', () => {
    const FILTROS = {
      tagsSelected: TAGS,
      dificuldade: FACIL,
      textoFiltro: TEXTO_FILTRO,
      status: EM_ELABORACAO,
      anoEscolar: TERCEIRO_ANO_ENSINO_MEDIO,
      tipo: V_OU_F,
      elaborador: ELABORADOR,
    }
    const filtroTag = montarFiltroQuestao(FILTROS)

    expect(filtroTag).toEqual(RESULTADO_ESPERADO_FILTRO)
  })
})
