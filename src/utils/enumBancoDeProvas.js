export const bancoDeProvas = {
  emElaboracao: 0,
  prontaParaAplicacao: 1,
  aplicada: 2,
  emAplicacao: 3,
  resolucaoIniciada: 4,
  todasAsProvas: 5,
}

export const bancoDeProvasId = {
  'Em elaboração': 0,
  'Pronta para aplicação': 1,
  Aplicada: 2,
  'Em aplicação': 3,
  'Resolução iniciada': 4,
}
