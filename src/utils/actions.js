import { createSimpleActions } from 'localModules/simpleRedux'
import { simpleStates } from 'utils/simpleStates'

export const actions = createSimpleActions(simpleStates)
