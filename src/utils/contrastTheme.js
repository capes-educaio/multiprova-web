import { createMuiTheme } from '@material-ui/core/styles'
import blueGrey from '@material-ui/core/colors/blueGrey'
import green from '@material-ui/core/colors/green'
import amber from '@material-ui/core/colors/amber'

const BLACK = '#000000'
const WHITE = '#FFFFFF'
const YELLOW = '#ffe500'

export const contrastTheme = createMuiTheme({
  overrides: {
    MuiFormHelperText: { root: { color: WHITE } },
    MuiButton: {
      root: { color: YELLOW, '&$disabled': { color: WHITE } },
      contained: { backgroundColor: YELLOW, color: BLACK },
      containedPrimary: { backgroundColor: YELLOW, color: BLACK },
    },
    MuiTablePagination: { root: { color: WHITE } },
    MuiIconButton: { root: { color: YELLOW, '&$disabled': { color: WHITE } } },
    MuiInputBase: { root: { color: WHITE } },
    MuiInput: {
      underline: {
        '&:before': { borderBottom: `1px solid ${WHITE}` },
        '&:after': { borderBottom: `2px solid ${YELLOW}` },
      },
    },
    MuiFormLabel: { root: { color: WHITE } },
    MuiSelect: { icon: { color: WHITE } },
    MuiStepLabel: {
      iconContainer: { color: YELLOW },
      label: { color: YELLOW, '&$alternativeLabel': { color: YELLOW } },
    },
    MuiTableCell: { body: { color: WHITE } },
    MuiList: { root: { border: `1px solid ${WHITE}` } },
    MuiMobileStepper: {
      root: { background: BLACK },
      dot: { backgroundColor: WHITE },
      dotActive: { backgroundColor: YELLOW },
    },
    MuiChip: { root: { backgroundColor: WHITE } },
    MuiTypography: {
      h6: { color: WHITE },
      subheading: { color: WHITE },
      caption: { color: WHITE },
      colorTextSecondary: { color: WHITE },
      body2: { color: WHITE },
      subtitle2: { color: WHITE },
    },
    MuiRadio: { root: { color: YELLOW }, colorSecondary: { '&$checked': { color: YELLOW } } },
    MuiCheckbox: { root: { color: YELLOW }, colorSecondary: { '&$checked': { color: YELLOW } } },

    MuiSvgIcon: { colorAction: { color: YELLOW } },
    MuiPaper: {
      root: {
        backgroundColor: BLACK,
      },
    },
    MuiMenuItem: { root: { color: YELLOW } },
    MuiOutlinedInput: { root: { '& $notchedOutline': { borderColor: WHITE + 'AA' } } },
  },
  typography: {
    useNextVariants: true,
  },
  palette: {
    defaultText: WHITE,
    defaultBackground: BLACK,
    defaultLink: YELLOW,
    primary: {
      light: WHITE,
      main: WHITE,
      dark: WHITE,
      contrastText: BLACK,
    },
    secondary: {
      light: BLACK,
      main: BLACK,
      dark: BLACK,
      contrastText: WHITE,
    },
    success: { dark: green[600] },
    warning: { dark: amber[700] },
    default: {
      light: blueGrey[200],
      main: blueGrey[500],
      dark: blueGrey[900],
    },
    fundo: {
      amarelo: '#FFFDE7',
      indigo: '#E8EAF6',
      cinza: '#f5f5f5',
    },
    amareloQueimado: '#f7a530',
    verdeEscuro: '#20800e',
    cinzaEscuro: '#434440',
    defaultGrey: 'rgba(0, 0, 0, 0.54)',
    darkGray: 'rgba(0, 0, 0, 0.87)',
    decorarCorreta: 'limegreen',
    gelo: '#ffffff',
    steelBlue: '#3d73b4',
    darkBlue: '#315c90',
    mediumBlue: '#359bd6',
    minimumBlue: '#add8e6',
    steelBlueContrast: '#d6deea',
    cinzaAlto: '#dfdfdf',
    cinzaMedio: '#d0d0d0',
    cinzaDusty: '#949494',
  },
})
