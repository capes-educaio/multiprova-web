import { verificarSeQuestaoJaFoiRespondida } from './verificarSeQuestaoJaFoiRespondida'
import { enumTipoQuestao } from 'utils/enumTipoQuestao'
import { enumAnoEscolar } from 'utils/enumAnoEscolar'

// multipla escolha
const questaoMultiplaEscolhaNaoRespondida = {
  dificuldade: 3,
  enunciado:
    '<p>Acerca das proposições filosóficas de Immanuel Kant em sua obra Fundamentação da Metafísica dos Costumes, considere as assertivas abaixo.</p><p></p><p>I. O valor moral de uma ação não depende da re…',
  fixa: false,
  multiplaEscolha: {
    alternativas: [
      {
        letraNaInstancia: 'a',
        texto: '<p> I, III, V e VI. </p>',
      },
      {
        letraNaInstancia: 'b',
        texto: '<p> II, III e V. </p>',
      },
      {
        letraNaInstancia: 'c',
        texto: '<p> II, IV e VI. </p>',
      },
      {
        letraNaInstancia: 'd',
        texto: '<p> I, III, IV e V. </p>',
      },
      {
        letraNaInstancia: 'e',
        texto: '<p> I, II, IV e VI. </p>',
      },
    ],
    alternativasPorLinha: 2,
  },
  tipo: enumTipoQuestao.tipoQuestaoMultiplaEscolha,
}
const questaoMultiplaEscolhaRespondida = {
  dificuldade: 3,
  enunciado:
    '<p>Acerca das proposições filosóficas de Immanuel Kant em sua obra Fundamentação da Metafísica dos Costumes, considere as assertivas abaixo.</p><p></p><p>I. O valor moral de uma ação não depende da re…',
  fixa: false,
  multiplaEscolha: {
    alternativas: [
      {
        letraNaInstancia: 'a',
        texto: '<p> I, III, V e VI. </p>',
      },
      {
        letraNaInstancia: 'b',
        texto: '<p> II, III e V. </p>',
      },
      {
        letraNaInstancia: 'c',
        texto: '<p> II, IV e VI. </p>',
      },
      {
        letraNaInstancia: 'd',
        texto: '<p> I, III, IV e V. </p>',
      },
      {
        letraNaInstancia: 'e',
        texto: '<p> I, II, IV e VI. </p>',
      },
    ],
    alternativasPorLinha: 2,
    respostaCandidato: 'c',
  },
  tipo: enumTipoQuestao.tipoQuestaoMultiplaEscolha,
}

// discursiva
const questaoDiscursivaNaoRespondida = {
  questaoMatrizId: '568c28fffc4be30d44d00989',
  numeroNaMatriz: 5,
  numeroNaInstancia: 1,
  dataCadastro: '2019-01-31T01:35:15.669Z',
  dataUltimaAlteracao: '2019-01-31T01:35:15.669Z',
  enunciado: '<p>Fale sobre ...</p>',
  dificuldade: 2,
  anoEscolar: enumAnoEscolar.ensinoSuperiorBasico,
  tipo: enumTipoQuestao.tipoQuestaoDiscursiva,
  status: {
    id: 'el',
    texto: 'Em elaboração',
  },
  discursiva: {
    numeroDeLinhas: 3,
    expectativaDeResposta: '<p>Esperava-se isso!</p>', // deve ser excluído ao enviar para o aluno
  },
  elaborador: 'ODocente SobrenomeDocente',
  usuarioId: 'dccc93c2-41db-4aa8-a415-f4712e9fce10',
  criadoPor: 'dccc93c2-41db-4aa8-a415-f4712e9fce10',
  isDeleted: false,
  ocultoLixeira: false,
  tagIds: ['4eec0cc1-9e1e-4141-a2ad-44ed3cd9c443'],
}
const questaoDiscursivaRespondida = {
  questaoMatrizId: '568c28fffc4be30d44d00989',
  numeroNaMatriz: 5,
  numeroNaInstancia: 1,
  dataCadastro: '2019-01-31T01:35:15.669Z',
  dataUltimaAlteracao: '2019-01-31T01:35:15.669Z',
  enunciado: '<p>Fale sobre ...</p>',
  dificuldade: 2,
  anoEscolar: enumAnoEscolar.ensinoSuperiorBasico,
  tipo: enumTipoQuestao.tipoQuestaoDiscursiva,
  status: {
    id: 'el',
    texto: 'Em elaboração',
  },
  discursiva: {
    respostaCandidato: 'texto aqui',
    dataRespostaCandidato: '2019-01-31T01:35:15.669Z',
    numeroDeLinhas: 3,
    expectativaDeResposta: '<p>Esperava-se isso!</p>', // deve ser excluído ao enviar para o aluno
  },
  elaborador: 'ODocente SobrenomeDocente',
  usuarioId: 'dccc93c2-41db-4aa8-a415-f4712e9fce10',
  criadoPor: 'dccc93c2-41db-4aa8-a415-f4712e9fce10',
  isDeleted: false,
  ocultoLixeira: false,
  tagIds: ['4eec0cc1-9e1e-4141-a2ad-44ed3cd9c443'],
}

// vouf
const questaoVouFNaoRespondida = {
  questaoMatrizId: '568c28fffc4be30d44d00989',
  numeroNaMatriz: 5,
  numeroNaInstancia: 1,
  dataCadastro: '2019-01-31T01:33:42.950Z',
  dataUltimaAlteracao: '2019-01-31T01:33:42.950Z',
  enunciado: '<p>Enunciado V ou F</p>',
  dificuldade: 2,
  anoEscolar: enumAnoEscolar.ensinoSuperiorBasico,
  tipo: enumTipoQuestao.tipoQuestaoVerdadeiroOuFalso,
  status: {
    id: 'el',
    texto: 'Em elaboração',
  },
  vouf: {
    afirmacoes: [
      {
        texto: '<p>Essa é verdadeira</p>',
      },
      {
        texto: '<p>Essa é falsa</p>',
        respostaCandidato: null,
        dataRespostaCandidato: null,
      },
      {
        texto: '<p>Essa é Falsa</p>',
      },
    ],
  },
}
const questaoVouFRespondida = {
  questaoMatrizId: '568c28fffc4be30d44d00989',
  numeroNaMatriz: 5,
  numeroNaInstancia: 1,
  dataCadastro: '2019-01-31T01:33:42.950Z',
  dataUltimaAlteracao: '2019-01-31T01:33:42.950Z',
  enunciado: '<p>Enunciado V ou F</p>',
  dificuldade: 2,
  anoEscolar: enumAnoEscolar.ensinoSuperiorBasico,
  tipo: enumTipoQuestao.tipoQuestaoVerdadeiroOuFalso,
  status: {
    id: 'el',
    texto: 'Em elaboração',
  },
  vouf: {
    afirmacoes: [
      {
        texto: '<p>Essa é verdadeira</p>',
        respostaCandidato: 'V',
        dataRespostaCandidato: '2019-01-31T01:33:42.950Z',
      },
      {
        texto: '<p>Essa é falsa</p>',
        respostaCandidato: 'V',
        dataRespostaCandidato: '2019-01-31T01:33:42.950Z',
      },
      {
        texto: '<p>Essa é Falsa</p>',
        respostaCandidato: 'V',
        dataRespostaCandidato: '2019-01-31T01:33:42.950Z',
      },
    ],
  },
}
const questaoVouFParcialmenteRespondida = {
  questaoMatrizId: '568c28fffc4be30d44d00989',
  numeroNaMatriz: 5,
  numeroNaInstancia: 1,
  dataCadastro: '2019-01-31T01:33:42.950Z',
  dataUltimaAlteracao: '2019-01-31T01:33:42.950Z',
  enunciado: '<p>Enunciado V ou F</p>',
  dificuldade: 2,
  anoEscolar: enumAnoEscolar.ensinoSuperiorBasico,
  tipo: enumTipoQuestao.tipoQuestaoVerdadeiroOuFalso,
  status: {
    id: 'el',
    texto: 'Em elaboração',
  },
  vouf: {
    afirmacoes: [
      {
        texto: '<p>Essa é verdadeira</p>',
      },
      {
        texto: '<p>Essa é falsa</p>',
        respostaCandidato: 'V',
        dataRespostaCandidato: '2019-01-31T01:33:42.950Z',
      },
      {
        texto: '<p>Essa é Falsa</p>',
        respostaCandidato: 'V',
        dataRespostaCandidato: '2019-01-31T01:33:42.950Z',
      },
    ],
  },
}

// associacao de colunas
const questaoAssociacaoDeColunasNaoRespondida = {
  questaoMatrizId: '568c28fffc4be30d44d00989',
  numeroNaMatriz: 5,
  numeroNaInstancia: 1,
  dataCadastro: '2019-01-31T01:34:50.641Z',
  dataUltimaAlteracao: '2019-01-31T01:34:50.641Z',
  enunciado: '<p>Associe as colunas</p>',
  dificuldade: 2,
  anoEscolar: enumAnoEscolar.ensinoSuperiorBasico,
  tipo: enumTipoQuestao.tipoQuestaoAssociacaoDeColunas,
  status: {
    id: 'el',
    texto: 'Em elaboração',
  },
  elaborador: 'ODocente SobrenomeDocente',
  associacaoDeColunas: {
    colunaA: [
      {
        conteudo: '<p>Pronome</p>',
        rotulo: 1,
      },
      {
        conteudo: '<p>Verbo</p>',
        rotulo: 2,
        posicaoMatriz: 0,
      },
    ],
    colunaB: [
      {
        conteudo: '<p>Ele</p>',
        respostaCandidato: null,
        dataRespostaCandidato: null,
      },
      {
        conteudo: '<p>Ir</p>',
        respostaCandidato: undefined,
        dataRespostaCandidato: undefined,
      },
      {
        conteudo: '<p>Ter</p>',
      },
      {
        conteudo: '<p>Nós</p>',
      },
    ],
  },
  usuarioId: 'dccc93c2-41db-4aa8-a415-f4712e9fce10',
  criadoPor: 'dccc93c2-41db-4aa8-a415-f4712e9fce10',
  isDeleted: false,
  ocultoLixeira: false,
  tagIds: ['4eec0cc1-9e1e-4141-a2ad-44ed3cd9c443'],
}
const questaoAssociacaoDeColunasRespondida = {
  questaoMatrizId: '568c28fffc4be30d44d00989',
  numeroNaMatriz: 5,
  numeroNaInstancia: 1,
  dataCadastro: '2019-01-31T01:34:50.641Z',
  dataUltimaAlteracao: '2019-01-31T01:34:50.641Z',
  enunciado: '<p>Associe as colunas</p>',
  dificuldade: 2,
  anoEscolar: enumAnoEscolar.ensinoSuperiorBasico,
  tipo: enumTipoQuestao.tipoQuestaoAssociacaoDeColunas,
  status: {
    id: 'el',
    texto: 'Em elaboração',
  },
  elaborador: 'ODocente SobrenomeDocente',
  associacaoDeColunas: {
    colunaA: [
      {
        conteudo: '<p>Pronome</p>',
        rotulo: 1,
      },
      {
        conteudo: '<p>Verbo</p>',
        rotulo: 2,
        posicaoMatriz: 0,
      },
    ],
    colunaB: [
      {
        conteudo: '<p>Ele</p>',
        respostaCandidato: 1,
        dataRespostaCandidato: '2019-01-31T01:34:50.641Z',
      },
      {
        conteudo: '<p>Ir</p>',
        respostaCandidato: 1,
        dataRespostaCandidato: '2019-01-31T01:34:50.641Z',
      },
      {
        conteudo: '<p>Ter</p>',
        respostaCandidato: 2,
        dataRespostaCandidato: '2019-01-31T01:34:50.641Z',
      },
      {
        conteudo: '<p>Nós</p>',
        respostaCandidato: 1,
        dataRespostaCandidato: '2019-01-31T01:34:50.641Z',
      },
    ],
  },
  usuarioId: 'dccc93c2-41db-4aa8-a415-f4712e9fce10',
  criadoPor: 'dccc93c2-41db-4aa8-a415-f4712e9fce10',
  isDeleted: false,
  ocultoLixeira: false,
  tagIds: ['4eec0cc1-9e1e-4141-a2ad-44ed3cd9c443'],
}
const questaoAssociacaoDeColunasParcialmenteRespondida = {
  questaoMatrizId: '568c28fffc4be30d44d00989',
  numeroNaMatriz: 5,
  numeroNaInstancia: 1,
  dataCadastro: '2019-01-31T01:34:50.641Z',
  dataUltimaAlteracao: '2019-01-31T01:34:50.641Z',
  enunciado: '<p>Associe as colunas</p>',
  dificuldade: 2,
  anoEscolar: enumAnoEscolar.ensinoSuperiorBasico,
  tipo: enumTipoQuestao.tipoQuestaoAssociacaoDeColunas,
  status: {
    id: 'el',
    texto: 'Em elaboração',
  },
  elaborador: 'ODocente SobrenomeDocente',
  associacaoDeColunas: {
    colunaA: [
      {
        conteudo: '<p>Pronome</p>',
        rotulo: 1,
      },
      {
        conteudo: '<p>Verbo</p>',
        rotulo: 2,
        posicaoMatriz: 0,
      },
    ],
    colunaB: [
      {
        conteudo: '<p>Ele</p>',
      },
      {
        conteudo: '<p>Ir</p>',
        respostaCandidato: null,
        dataRespostaCandidato: null,
      },
      {
        conteudo: '<p>Ter</p>',
        respostaCandidato: undefined,
        dataRespostaCandidato: undefined,
      },
      {
        conteudo: '<p>Nós</p>',
        respostaCandidato: 1,
        dataRespostaCandidato: '2019-01-31T01:34:50.641Z',
      },
    ],
  },
  usuarioId: 'dccc93c2-41db-4aa8-a415-f4712e9fce10',
  criadoPor: 'dccc93c2-41db-4aa8-a415-f4712e9fce10',
  isDeleted: false,
  ocultoLixeira: false,
  tagIds: ['4eec0cc1-9e1e-4141-a2ad-44ed3cd9c443'],
}
describe('Testando <verificarSeQuestaoJaFoiRespondida.js />', () => {
  // testes referentes ao tipo multipla escolha
  it('Verificando se a questao multipla escolha respondida retorna verdadeiro', () => {
    expect(verificarSeQuestaoJaFoiRespondida(questaoMultiplaEscolhaRespondida)).toBeTruthy()
  })
  it('Verificando se a questao multipla escolha não respondida retorna falso', () => {
    expect(verificarSeQuestaoJaFoiRespondida(questaoMultiplaEscolhaNaoRespondida)).toBeFalsy()
  })

  // testes referentes ao tipo discursiva
  it('Verificando se a questao discursiva respondida retorna verdadeiro', () => {
    expect(verificarSeQuestaoJaFoiRespondida(questaoDiscursivaRespondida)).toBeTruthy()
  })
  it('Verificando se a questao discursiva não respondida retorna falso', () => {
    expect(verificarSeQuestaoJaFoiRespondida(questaoDiscursivaNaoRespondida)).toBeFalsy()
  })

  // testes referentes ao tipo vouf
  it('Verificando se a questao vouf respondida retorna verdadeiro', () => {
    expect(verificarSeQuestaoJaFoiRespondida(questaoVouFRespondida)).toBeTruthy()
  })
  it('Verificando se a questao vouf não respondida retorna falso', () => {
    expect(verificarSeQuestaoJaFoiRespondida(questaoVouFNaoRespondida)).toBeFalsy()
  })
  it('Verificando se a questao vouf parcialmente respondida retorna falso', () => {
    expect(verificarSeQuestaoJaFoiRespondida(questaoVouFParcialmenteRespondida)).toBeFalsy()
  })

  // testes referentes ao tipo associacao de colunas
  it('Verificando se a questao associacao de colunas respondida retorna verdadeiro', () => {
    expect(verificarSeQuestaoJaFoiRespondida(questaoAssociacaoDeColunasRespondida)).toBeTruthy()
  })
  it('Verificando se a questao associacao de colunas não respondida retorna falso', () => {
    expect(verificarSeQuestaoJaFoiRespondida(questaoAssociacaoDeColunasNaoRespondida)).toBeFalsy()
  })
  it('Verificando se a questao associacao de colunas parcialmente respondida retorna falso', () => {
    expect(verificarSeQuestaoJaFoiRespondida(questaoAssociacaoDeColunasParcialmenteRespondida)).toBeFalsy()
  })
})
