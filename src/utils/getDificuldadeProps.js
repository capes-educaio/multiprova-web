import dificuldadeDificil from 'images/dificuldadeDificil.png'
import dificuldadeFacil from 'images/dificuldadeFacil.png'
import dificuldadeMedio from 'images/dificuldadeMedio.png'

export const getDificuldadeProps = (dificuldade, strings) => {
  let dificuldadeString, imagem
  if (dificuldade === 1) {
    dificuldadeString = strings.facil
    imagem = dificuldadeFacil
  }
  if (dificuldade === 2) {
    dificuldadeString = strings.medio
    imagem = dificuldadeMedio
  }
  if (dificuldade === 3) {
    dificuldadeString = strings.dificil
    imagem = dificuldadeDificil
  }
  return { dificuldadeString, imagem }
}
