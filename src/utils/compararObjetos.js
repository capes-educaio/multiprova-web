import { deepStrictEqual } from 'assert'

export const saoIguaisDeepEqual = (objeto1, objeto2) => {
  try {
    deepStrictEqual(objeto1, objeto2)
    return true
  } catch (e) {
    return false
  }
}
