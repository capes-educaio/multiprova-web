export const calculo = [
  {
    type: 'cmd',
    tex: '\\sum',
    icon: '\\sum',
  },
  {
    type: 'cmd',
    tex: '\\prod',
    icon: '\\prod',
  },
  {
    type: 'write',
    tex: '\\lim_{x\\to\\infty}',
    icon: '\\lim_{x\\to\\infty}',
  },
  {
    type: 'write',
    tex: '\\lim_{x\\to\\infty}',
    icon: '\\lim_{x\\to n}',
    keystrokes: ['Left', 'Backspace'],
  },
  {
    type: 'cmd',
    tex: '\\int',
    icon: '\\int',
  },
  {
    type: 'write',
    tex: '\\int_{}^{}',
    icon: '\\int_a^b',
    keystrokes: ['Left', 'Left'],
  },
  {
    type: 'typed',
    tex: '\\oint_V',
    icon: '\\oint_V',
    keystrokes: ['Backspace'],
  },
]
