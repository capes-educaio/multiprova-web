export const basicos = [
  {
    type: 'write',
    tex: '+',
    icon: '+',
  },
  {
    type: 'write',
    tex: '-',
    icon: '-',
  },
  {
    type: 'write',
    tex: '\\sqrt[]{}',
    icon: '\\sqrt[n]{\\phantom{1}}',
    keystrokes: ['Left', 'Left'],
  },
  {
    type: 'cmd',
    tex: '\\times',
    icon: '\\times',
  },
  {
    type: 'cmd',
    tex: '\\leq',
    icon: '\\leq',
  },
  {
    type: 'cmd',
    tex: '\\geq',
    icon: '\\geq',
  },
  {
    type: 'cmd',
    tex: '\\div',
    icon: '\\div',
  },
  {
    type: 'write',
    tex: '^2',
    icon: 'x^2',
  },
  {
    type: 'write',
    tex: '^3',
    icon: 'x^3',
  },
  {
    type: 'typed',
    tex: '^',
    icon: 'x^n',
  },
  {
    type: 'typed',
    tex: '_',
    icon: 'x_n',
  },
  {
    type: 'typed',
    tex: '=',
    icon: '=',
  },
  {
    type: 'typed',
    tex: '/',
    icon: '\\frac{x}{n}',
  },
  {
    type: 'cmd',
    tex: '\\sqrt',
    icon: '\\sqrt{\\phantom{1}}',
  },

  {
    type: 'cmd',
    tex: '\\pm',
    icon: '\\pm',
  },
  {
    type: 'typed',
    tex: '<',
    icon: '<',
  },

  {
    type: 'typed',
    tex: '>',
    icon: '>',
  },
]
