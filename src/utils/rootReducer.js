import { combineReducers } from 'redux'
import { createResponsiveStateReducer } from 'redux-responsive'

import { createSimpleReducers } from 'localModules/simpleRedux'
import { createStringsReducer } from 'localModules/strings'
import { languages } from 'utils/strings'
import { simpleStates } from './simpleStates'

const appReducer = combineReducers({
  browser: createResponsiveStateReducer({
    extraSmall: 600,
    small: 960,
    medium: 1280,
    large: 1920,
    extraLarge: 9000,
  }),
  strings: createStringsReducer(languages),
  ...createSimpleReducers(simpleStates),
})

export const rootReducer = (state, action) => {
  if (action.type === 'RESET_SIMPLE_STATES') {
    state = { ...state, ...simpleStates }
  }
  return appReducer(state, action)
}
