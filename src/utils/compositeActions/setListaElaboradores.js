import { get } from 'api'
import { dispatchToStore } from './dispatchToStore'

export const setListaElaboradores = () => {
  const url = `/questoes/elaboradores`
  get(url).then(r => {
    const listaElaboradores = r.data.map(elaborador => {
      return { value: elaborador, label: elaborador }
    })
    dispatchToStore('select', 'listaElaboradores', listaElaboradores)
  })
}
