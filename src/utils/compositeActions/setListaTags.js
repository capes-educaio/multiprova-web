import { get } from 'api'
import { store } from 'utils/store'

import { dispatchToStore } from './dispatchToStore'

export const setListaTags = () => {
  const usuarioAtual = store.getState().usuarioAtual
  const url = `/usuarios/${usuarioAtual.data.id}/tags`
  get(url).then(r => {
    const todasTagsDoUsuario = r.data
    dispatchToStore('select', 'listaTags', todasTagsDoUsuario)
  })
}
