import { dispatchToStore } from './dispatchToStore'
import { get } from 'api'

export const carregarNotificacoes = usuario => {
  const { id } = usuario._data
  get(`usuarios/${id}/notificacoes`).then(res => {
    dispatchToStore('select', 'notificacoes', res.data)
  })
}
