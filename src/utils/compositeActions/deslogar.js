import { setUrlState } from 'localModules/urlState'

import { removeAllCookies } from 'utils/cookies'

import { dispatchToStore } from './dispatchToStore'

export function deslogar() {
  removeAllCookies('authToken')
  dispatchToStore('select', 'usuarioAtual', null)
  dispatchToStore('select', 'notificacoes', [])
  dispatchToStore('select', 'tipoBancoQuestoes', null)
  setUrlState({ pathname: '/login' })
}
