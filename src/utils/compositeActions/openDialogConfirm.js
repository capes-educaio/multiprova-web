import { dispatchToStore } from './dispatchToStore'
import { store } from 'utils/store'

export const openDialogConfirm = ({ texto, titulo, onConfirm, onCancel }) => {
  const { strings } = store.getState()
  const dialogConfig = {
    titulo,
    texto,
    actions: [
      {
        id: 'confirm-dialog',
        texto: strings.ok,
        onClick: onConfirm,
      },
      {
        id: 'cancel-dialog',
        texto: strings.cancelar,
        onClick: onCancel,
      },
    ],
  }
  dispatchToStore('push', 'listaDialog', dialogConfig)
}
