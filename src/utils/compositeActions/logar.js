import { Usuario } from 'localModules/multiprovaEntidades'

import { cookies, removeAllCookies } from 'utils/cookies'

import { dispatchToStore } from './dispatchToStore'
import { mandarParaPaginaInicial } from './mandarParaPaginaInicial'
import { carregarNotificacoes } from './carregarNotificacoes'

export function logar({ authToken, userData, redirecionar = false }) {
  removeAllCookies('authToken')
  const cookieOptions = { path: '/' }
  cookies.set('authToken', authToken, cookieOptions)
  const usuario = new Usuario(userData)
  carregarNotificacoes(usuario)
  if (usuario.isValid()) {
    dispatchToStore('select', 'usuarioAtual', usuario)
    if (redirecionar) {
      mandarParaPaginaInicial(usuario)
    }
  } else console.error('Usuário inválido vindo do backend.', usuario.erros)
}
