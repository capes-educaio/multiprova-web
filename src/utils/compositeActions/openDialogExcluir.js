import { openDialogConfirm } from './openDialogConfirm'
import { store } from 'utils/store'

import { del } from 'api'

const excluir = ({ url, resolve, reject }) => () => {
  del(url)
    .then(resolve)
    .catch(reject)
}

export const openDialogExcluir = ({ texto, titulo, url, resolve, reject }) => {
  const { strings } = store.getState()
  openDialogConfirm({ texto, titulo: titulo ? titulo : strings.excluir, onConfirm: excluir({ url, resolve, reject }) })
}
