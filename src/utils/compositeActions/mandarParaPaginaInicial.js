import { setUrlState } from 'localModules/urlState'
import { openDialogAviso } from 'utils/compositeActions/openDialogAviso'
import { store } from 'utils/store'

import { appConfig } from 'appConfig'

export const mandarParaPaginaInicial = usuarioAtual => {
  if (usuarioAtual.isAdmin()) setUrlState(appConfig.homePath.admin)
  else if (usuarioAtual.isDocente()) setUrlState(appConfig.homePath.docente)
  else if (usuarioAtual.isDiscente()) setUrlState(appConfig.homePath.discente)
  else if (usuarioAtual.isGestor()) setUrlState(appConfig.homePath.gestor)
  else {
    const { strings } = store.getState()
    openDialogAviso({
      texto: `${strings.falhaInesperada} id: c35907b4-d0d4-423c-8c0e-78e3e25aa5be`,
      titulo: strings.erro,
    })
  }
}
