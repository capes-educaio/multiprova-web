import { store } from 'utils/store'

import { actions } from 'utils/actions'

export function dispatchToStore(actionName, stateName, value) {
  const simpleAction = actions[actionName][stateName]
  store.dispatch(simpleAction(value))
}
