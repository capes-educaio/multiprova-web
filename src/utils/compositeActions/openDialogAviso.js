import { dispatchToStore } from './dispatchToStore'
import { store } from 'utils/store'

export const openDialogAviso = ({ texto, titulo }) => {
  const { strings } = store.getState()
  const dialogConfig = {
    titulo,
    texto,
    actions: [
      {
        texto: strings.ok,
      },
    ],
  }
  dispatchToStore('push', 'listaDialog', dialogConfig)
}
