// Only works if you do not use Dates, functions, undefined, Infinity, RegExps, Maps, Sets, Blobs, FileLists,
// ImageDatas, sparse Arrays, Typed Arrays or other complex types within your object
export const clonarObjeto = objeto => JSON.parse(JSON.stringify(objeto))
