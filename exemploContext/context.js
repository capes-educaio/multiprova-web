import React from 'react'
import { withContext } from 'utils/context'

export const ExemploContext = React.createContext()
export const withExemploContext = withContext(ExemploContext, 'exemploContext')
