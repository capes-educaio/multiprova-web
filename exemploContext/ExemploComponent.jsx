import React, { Component } from 'react'

import { initiateContextState } from 'utils/context'
import { ExemploContext } from './context'

import { propTypes } from './propTypes'

export class ExemploComponent extends Component {
  static propTypes = propTypes

  constructor(props) {
    super(props)
    const contextInitialValue = {}
    initiateContextState(this, contextInitialValue)
  }

  render() {
    return <ExemploContext.Provider value={this.state.context}></ExemploContext.Provider>
  }
}
